<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Pipp_lib
{
	private $CI;
	private $id_aplikasi_otoritas_pipp = "75";


	function __construct()
	{
			$this->CI =& get_instance();
	}

	function fmt_tgl($str_tanggal)
	{
		return date("d/m/Y", strtotime($str_tanggal));
	}

	function str_nama_pelabuhan($id_pelabuhan)
	{
		$this->CI->load->model('mdl_pelabuhan');
		$result = '';
		$arr_result = array();
		if(empty($id_pelabuhan))
		{
			return "-";
		}
				if(strpos($id_pelabuhan, ",") !== FALSE)
				{
					$temp_arr = explode(",", $id_pelabuhan);
					foreach ($temp_arr as $id) {
						$nama_pelabuhan = kos($this->CI->mdl_pelabuhan->detail_pelabuhan($id, array('nama_pelabuhan'))->nama_pelabuhan);
						array_push($arr_result, $nama_pelabuhan);
					}
					$result = implode(",", $arr_result);
				}else{
					$result = kos($this->CI->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan, array('nama_pelabuhan'))->nama_pelabuhan);
				}
		return $result;
	}

	function bulan_ind($index =''){
		$mons = array(1 => "Januari",
					  2 => "Pebruari",
					  3 => "Maret",
					  4 => "April",
					  5 => "Mei",
					  6 => "Juni",
					  7 => "Juli",
					  8 => "Agustus",
					  9 => "September",
					  10 => "Oktober",
					  11 => "November",
					  12 => "Desember");
		if(empty($index)) {
			$date = getdate();
			$month = $date['mon'];
		}else{
			$month = $index;
		}


		$month_name = $mons[$month];

		return $month_name; // Displays the current month
	}

	function istilah_tujuan($istilah)
	{	
		$tujuan_berangkat = '';

			if($istilah=== 'angkut')
            {
                $tujuan_berangkat = 'Mengangkut Ikan';
            }elseif ($istilah === 'tangkap') {
                $tujuan_berangkat = 'Menangkap Ikan';
            }elseif ($istilah === 'docking') {
                $tujuan_berangkat = 'Docking';
            }
        return $tujuan_berangkat;
	}

	function info_is_admin($index)
	{
		$info_is_admin = array();
		$is_admin_session = $this->CI->session->userdata('is_admin') > 0 ? TRUE : FALSE;
		$is_super_admin = $this->CI->session->userdata('is_super_admin') > 0 ? TRUE : FALSE;
		$is_admin_otoritas = false;

		$arr_data_otoritas = $this->CI->session->userdata('data_otoritas_array');
		foreach ($arr_data_otoritas as $key) {
			if($key['id_aplikasi_otoritas'] === $this->id_aplikasi_otoritas_pipp)
			{
				if($key['id_grup_otoritas'] === "8")
				{
					$is_admin_otoritas = true;										
				}else{
					$is_admin_otoritas = false;
				}
			}
		}

		$info_is_admin = array('is_admin_otoritas' => $is_admin_otoritas,
								'is_super_admin' => $is_super_admin,
								'is_admin_session' => $is_admin_session
							   );


		return $info_is_admin[$index];
	}


	function info_admin_pipp($index)
	{
		$admin_otori = $this->info_is_admin('is_admin_otoritas');
		$admin_super = $this->info_is_admin('is_super_admin');
		$info_session = $index === 'id_pelabuhan_temp' ? '1' : 'Kendari';
		if($admin_super || $admin_otori)
		{
			if( $this->CI->session->userdata('id_pelabuhan_temp') !== FALSE )
			{
				$info_session =	$this->CI->session->userdata($index);			
			}
		}
		return $info_session;
	}

	function id_pelabuhan_pengguna()
	{
		$id_pelabuhan_pengguna = 0;
		$arr_data_otoritas = $this->CI->session->userdata('data_otoritas_array');
		foreach ($arr_data_otoritas as $key) {
			if($key['id_aplikasi_otoritas'] === $this->id_aplikasi_otoritas_pipp)
			{
				if(strpos( $key['id_pelabuhan_otoritas'], ',' ) === FALSE )
				{
					$id_pelabuhan_pengguna = $key['id_pelabuhan_otoritas'];										
				}else{
					$arr_plb = explode(",", $key['id_pelabuhan_otoritas'] );
					$id_pelabuhan_pengguna = $arr_plb[0];
				}
			}
		}
		return empty($id_pelabuhan_pengguna) ? 0 : $id_pelabuhan_pengguna;
	}

	function id_grup_pengguna()
	{
		$id_grup_pengguna = 0;
		$arr_data_otoritas = $this->CI->session->userdata('data_otoritas_array');
		foreach ($arr_data_otoritas as $key) {
			if($key['id_aplikasi_otoritas'] === $this->id_aplikasi_otoritas_pipp)
			{
				$id_grup_pengguna = $key['id_grup_otoritas'];
			}
		}
		return $id_grup_pengguna;
	}

	function url_query_encode($str)
	{		
		 $result ='';
		 $key = "abc123";
		  for($i=0; $i<strlen($str); $i++) {
		     $char = substr($str, $i, 1);
		     $keychar = substr($key, ($i % strlen($key))-1, 1);
		     $char = chr(ord($char)+ord($keychar));
		     $result.=$char;
		  }
		  return urlencode(base64_encode($result));
	}


	function url_query_decode($str)
	{
	     $str = base64_decode(urldecode($str));
		  $result = '';
		 $key = "abc123";
		  for($i=0; $i<strlen($str); $i++) {
		    $char = substr($str, $i, 1);
		    $keychar = substr($key, ($i % strlen($key))-1, 1);
		    $char = chr(ord($char)-ord($keychar));
		    $result.=$char;
		  }
		return $result;
	}

}
?>