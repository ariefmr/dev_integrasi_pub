<?php
	$config = Array(
			'global_config' => Array(
									'dev_mode' => TRUE
									),
			'form_titles'	=>Array(
									'h1'	=> 'Kedatangan Kapal',
									'h2'	=> 'Produksi & Harga',
									'h3'	=> 'Pemasaran Masuk Pelabuhan',
									'h5'	=> 'Keberangkatan Kapal',
									'b1'	=> 'Jasa dan Non Jasa Pelabuhan',	
									'b3'	=> 'Pemasaran Keluar Pelabuhan',
									'b4'	=> 'Usaha Pengolahan Ikan',
									'b2'	=> 'Pemasok Perbekalan',
									's1'	=> 'Industri di Pelabuhan',
									's6'	=> Array(	
													'a' => 'Data Umum Pelabuhan',
													'b' => 'Luas Daerah Kerja Perairan',
													'c' => 'Akses Pelabuhan / Pangkalan Perikanan',
													'd' => 'Transportasi Pelabuhan',
													'e' => 'Amenities Pelabuhan'
												),
									's7' 	=>  Array(	
													'a' => 'Breakwater',
													'b' => 'Revetment',
													'c' => 'Groin',
													'd' => 'Dermaga',
													'e' => 'Kolam Pelabuhan',
													'f' => 'Alur Pelayaran',
													'g' => 'Jalan',
													'h' => 'Penghubung',
													'i' => 'Pagar Keliling'
												),
									's8'	=> Array(	
													'a' => 'Pemasaran Hasil Perikanan',
													'b' => 'Navigasi Pelayaran dan Komunikasi',
													'c' => 'Sumur',
													'd' => 'Sungai',
													'e' => 'PDAM',
													'f' => 'Water Treatment dan Penampung Air ',
													'g' => 'Hydrant',
													'h' => 'Pabrik/Gudang Es',
													'i' => 'Mesin Penghancur Es',
													'j' => 'Mesin Genset/PLN',
													'k' => 'Rumah Genset',
													'l' => 'Layanan Bahan Bakar',
													'm' => 'Dock' ,
													'n' => 'Pemeliharaan Kapal dan Alat Penangkapan Ikan',
													'o' => 'Laboratorium Pembinaan dan Pengujian Hasil Mutu Perikanan',
													'p' => 'Penanganan dan Pengujian Hasil Mutu Perikanan',
													'q' => 'Perkantoran',
													'r' => 'Kendaraan',
													's' => 'Tempat Parkir',
													't' => 'Pengolahan Limbah'
												),
									's9'	=>	Array(	
													'a' => 'Pembinaan Nelayan',
													'b' => 'Pengelola Pelabuhan',
													'c' => 'Sosial dan Umum',
													'd' => 'Kios Iptek'
												),
									's10'	=> Array(	
													'a' => 'Kondisi Fisik Pelabuhan',
													'b' => 'Sungai'
													//'S10B - Geoteknik' => base_url('jurnal/S10B'),
													//'S10B - Hidrooseanografi' => base_url('jurnal/S10B'),
													//'S10C - Geoteknik' => base_url('jurnal/S10C'),
													//'S10D - Sedimentasi' => base_url('jurnal/S10D'),
													//'S10E - Banjir & Tsunami' => base_url('jurnal/S10E'),
													//'S10F - Topografi & Batimetri' => base_url('jurnal/S10F'),
													//'S10G - Sungai' => base_url('jurnal/S10G')
												),
									's11'	=> 'Kelembagaan UPT Pelabuhan',
									's11_detail'	=> 'Jurnal Data Detail Pegawai',
									's12'	=> 'Kelembagaan di Dalam Pelabuhan',
									's13'	=> 'Masyarakat Perikanan',
									'k5'	=> 'Monitoring K5'
									),
			'link_terkait'  => Array(
									'h1'	=> Array(	
													'Keberangkatan Kapal' => base_url('jurnal/H5'),
													'Produksi & Harga' => base_url('jurnal/H2')
												),
									'h2'	=> Array(	
													'Keberangkatan Kapal' => base_url('jurnal/H5'),
													'Kedatangan Kapal' => base_url('jurnal/H1')
												),
									'h3'	=> Array(	
													'Keluar Pelabuhan' => base_url('jurnal/B3')
												),
									'h5'	=> Array(	
													'Kedatangan Kapal' => base_url('jurnal/H1'),
													'Produksi & Harga' => base_url('jurnal/H2')
												),	
									'b3'	=> Array(	
													'Masuk Pelabuhan' => base_url('jurnal/H3')
												),	
									'b2'	=> Array(	
													'Pemasok Perbekalan' => base_url('jurnal/B2')
												),	
									'b4'	=> Array(	
													'Usaha Pengolahan Ikan' => base_url('jurnal/B4')
												),	
									'b1'	=> Array(	
													'Jasa dan Non Jasa Pelabuhan' => base_url('jurnal/B1')
												),
									's1'	=> Array(	
													'Industri di Pelabuhan' => base_url('jurnal/S1'),
													'Usaha Pengolahan Ikan' => base_url('jurnal/B4')
												),
									's6'	=> Array(	
													'Data Umum Pelabuhan' => base_url('jurnal/S6A'),
													'Luas Daerah Kerja Perairan' => base_url('jurnal/S6B'),
													'Akses Pelabuhan / Pangkalan Perikanan' => base_url('jurnal/S6C'),
													'Transportasi Pelabuhan' => base_url('jurnal/S6D'),
													'Amenities Pelabuhan' => base_url('jurnal/S6E')
												),
									's7' 	=>  Array(	
													'Breakwater' => base_url('jurnal/S7A'),
													'Revetment' => base_url('jurnal/S7B'),
													'Groin' => base_url('jurnal/S7C'),
													'Dermaga' => base_url('jurnal/S7D'),
													'Kolam Pelabuhan' => base_url('jurnal/S7E'),
													'Alur Pelayaran' => base_url('jurnal/S7F'),
													'Jalan' => base_url('jurnal/S7G'),
													'Penghubung' => base_url('jurnal/S7H'),
													'Pagar Keliling' => base_url('jurnal/S7I')
												),
									's8'	=> Array(	
													'Pemasaran Hasil Perikanan' => base_url('jurnal/S8A'),
													'Navigasi Pelayaran dan Komunikasi' => base_url('jurnal/S8B'),
													'Sumur' => base_url('jurnal/S8C'),
													'Sungai' => base_url('jurnal/S8D'),
													'PDAM' => base_url('jurnal/S8E'),
													'Water Treatment dan Penampung Air ' => base_url('jurnal/S8F'),
													'Hydrant' => base_url('jurnal/S8G'),
													'Pabrik/Gudang Es' => base_url('jurnal/S8H'),
													'Mesin Penghancur Es' => base_url('jurnal/S8I'),
													'Mesin Genset/PLN' => base_url('jurnal/S8J'),
													'Rumah Genset' => base_url('jurnal/S8K'),
													'Layanan Bahan Bakar' => base_url('jurnal/S8L'),
													'Dock' => base_url('jurnal/S8M'),
													'Pemeliharaan Kapal dan Alat Penangkapan Ikan' => base_url('jurnal/S8N'),
													'Laboratorium Pembinaan dan Pengujian Hasil Mutu Perikanan' => base_url('jurnal/S8O'),
													'Penanganan dan Pengujian Hasil Mutu Perikanan' => base_url('jurnal/S8P'),
													'Perkantoran' => base_url('jurnal/S8Q'),
													'Kendaraan' => base_url('jurnal/S8R'),
													'Tempat Parkir' => base_url('jurnal/S8S'),
													'Pengolahan Limbah' => base_url('jurnal/S8T')
												),
									's9'	=>	Array(	
													'Pembinaan Nelayan' => base_url('jurnal/S9A'),
													'Pengelola Pelabuhan' => base_url('jurnal/S9B'),
													'Sosial dan Umum' => base_url('jurnal/S9C'),
													'Kios Iptek' => base_url('jurnal/S9D')
												),
									's10'	=> Array(	
													'Geoteknik' => base_url('jurnal/S10A'),
													'Sungai' => base_url('jurnal/S10B')
													//'S10B - Geoteknik' => base_url('jurnal/S10B'),
													//'S10B - Hidrooseanografi' => base_url('jurnal/S10B'),
													//'S10C - Geoteknik' => base_url('jurnal/S10C'),
													//'S10D - Sedimentasi' => base_url('jurnal/S10D'),
													//'S10E - Banjir & Tsunami' => base_url('jurnal/S10E'),
													//'S10F - Topografi & Batimetri' => base_url('jurnal/S10F'),
													//'S10G - Sungai' => base_url('jurnal/S10G')
												),
									's11'	=> Array(	
													'Kelembagaan UPT Pelabuhan' => base_url('jurnal/S11')
												),
									's12'	=> Array(	
													'Kelembagaan di Dalam Pelabuhan' => base_url('jurnal/S12')
												),
									's13'	=> Array(	
													'Masyarakat Perikanan' => base_url('jurnal/S13')
												),
									'k5'	=> Array(	
													'Monitoring K5' => base_url('jurnal/k5')
												)
									),
	        'assets_paths' => Array(
	        						'misc_css' => base_url('assets/third_party/css'),
	        						'misc_js' => base_url('assets/third_party/js'),
	        						'misc_sounds' => base_url('assets/third_party/sounds'),
	        						'main_css' => base_url('assets/pipp/css'),
	        						'main_js' => base_url('assets/pipp/js'),
	        						'pipp_images' => base_url('assets/pipp/images'),
	        						'pipp_uploads' => base_url('assets/pipp/uploads'),
	        						'mockup_images' => base_url('assets/pipp/images/mockup'),
	        						'folder_ikan_dss' => 'http://integrasi.djpt.kkp.go.id/upload/ikan/images'
	        						),
	        'upload_rules' => Array(
	        						'upload_path' => 'assets/pipp/uploads',
	        						'allowed_types' => 'gif|jpg|png|jpeg',
									'max_size'	=> '2048',
									'max_width'  => '2048',
									'max_height'  => '1536'
	        						),
	        'public_access' => Array(
	        							'shti'
	        						),
	        'basic' => Array(
	        					'app_name' => 'PIPP',
	        					'copyright' => 'Copyright text here 2013',
	        					'owner' => 'PIPP'
	        					),
	        'menus' => Array(
	        					'Home' => base_url(),
	        					'Master' => Array(	
	        										'Pelabuhan' => base_url('mst_pelabuhan/daftar_pelabuhan_dss'),
	        										'Sumber Daya Ikan' => base_url('mst_jenis_ikan/daftar_ikan'),
	        										'Alat Tangkap' => base_url('mst_alat_tangkap/daftar_alat_tangkap')
	        									),
	        					'Form Entry' => Array(
	        											'Produksi' => Array(
	        																				'Keberangkatan Kapal' => base_url('entry/H5'),
	        																				'Kedatangan Kapal' => base_url('jurnal/H1'),
	        																				'Produksi dan Harga' => base_url('jurnal/H2')
	        																				//'Sampling (Panjang, Berat, Mutu)' => base_url('forms/view/S5')
	        																				),
	        											'Pemasaran' => Array(
	        																				'Masuk Pelabuhan' => base_url('entry/H3'),
	        																				'Keluar Pelabuhan' => base_url('entry/B3')
	        																				),
	        											'Perbekalan' => Array(
	        																				'Pemasok Perbekalan' => base_url('entry/B2')
	        																				),
	        											
	        											'Industri dan Jasa' => Array(		
	        																				'Industri di Pelabuhan' => base_url('entry/S1'),
	        																				'Usaha Pengolahan Ikan di Pelabuhan' => base_url('entry/B4'),
	        																				'Jasa dan Non Jasa Pelabuhan' => base_url('jurnal/B1')
	        																				)
	        											/*'Profil Pelabuhan' => Array(
	        																				'Data Umum' => base_url('jurnal/S6A'),
	        																				'Prasarana' => base_url('#'),
	        																				'Kelembagaan' => base_url('#')
	        																				//'S10 - Data Lingkungan Fisik Pelabuhan' => base_url('forms/view/S10')
	        																				)
	        											
	        											'Prasarana Pelabuhan' => Array(
	        																				//'B5 - Kondisi Fasilitas Di Pelabuhan' => base_url('forms/view/B5'),
	        																				//'B7 - Masalah Dan Upaya Di Pelabuhan' => base_url('forms/view/B7'),
	        																				//'S7 - Fasilitas Pokok' => base_url('forms/view/S7'),
	        																				//'S8 - Fasilitas Fungsional' => base_url('forms/view/S8'),
	        																				//'S9 - Fasilitas Penunjang' => base_url('forms/view/S9'),
	        																				),
	        											'Kelembagaan dan Tenaga Kerja di Pelabuhan' => Array(
	        																				//'S11 - Kelembagaan UPT Pelabuhan' => base_url('forms/view/S11'),
	        																				//'S12 - Kelembagaan di Dalam Pelabuhan' => base_url('forms/view/S12'),
	        																				//'S13 - Masyarakat Perikanan' => base_url('forms/view/S13')
	        																				)*/
        											),
								'Profil Pelabuhan' => Array(
															'Data Umum' => Array(
																				'Data Umum Pelabuhan' => base_url('jurnal/S6A'),
																				'Luas Daerah Kerja Perairan' => base_url('jurnal/S6B'),
																				'Akses Pelabuhan / Pangkalan Perikanan' => base_url('jurnal/S6C'),
																				'Transportasi Pelabuhan' => base_url('jurnal/S6D'),
																				'Amenities Pelabuhan' => base_url('jurnal/S6E')
																				//'S10 - Data Lingkungan Fisik Pelabuhan' => base_url('forms/view/S10')
																				),
															'Fasilitas Pokok' => Array(
																				'Breakwater' => base_url('jurnal/S7A'),
																				'Revetment' => base_url('jurnal/S7B'),
																				'Groin' => base_url('jurnal/S7C'),
																				'Dermaga' => base_url('jurnal/S7D'),
																				'Kolam Pelabuhan' => base_url('jurnal/S7E'),
																				'Alur Pelayaran' => base_url('jurnal/S7F'),
																				'Jalan' => base_url('jurnal/S7G'),
																				'Penghubung' => base_url('jurnal/S7H'),
																				'Pagar Keliling' => base_url('jurnal/S7I')
																				),
															'Fasilitas Fungsional' => Array(
																				'Pemasaran Hasil Perikanan' => base_url('jurnal/S8A'),
																				'Navigasi Pelayaran dan Komunikasi' => base_url('jurnal/S8B'),
																				'Sumur' => base_url('jurnal/S8C'),
																				'Sungai' => base_url('jurnal/S8D'),
																				'PDAM' => base_url('jurnal/S8E'),
																				'Water Treatment dan Penampung Air ' => base_url('jurnal/S8F'),
																				'Hydrant' => base_url('jurnal/S8G'),
																				'Pabrik/Gudang Es' => base_url('jurnal/S8H'),
																				'Mesin Penghancur Es' => base_url('jurnal/S8I'),
																				'Mesin Genset/PLN' => base_url('jurnal/S8J'),
																				'Rumah Genset' => base_url('jurnal/S8K'),
																				'Layanan Bahan Bakar' => base_url('jurnal/S8L'),
																				'Dock' => base_url('jurnal/S8M'),
																				'Pemeliharaan Kapal dan Alat Penangkapan Ikan' => base_url('jurnal/S8N'),
																				'Laboratorium Pembinaan dan Pengujian Hasil Mutu Perikanan' => base_url('jurnal/S8O'),
																				'Penanganan dan Pengujian Hasil Mutu Perikanan' => base_url('jurnal/S8P'),
																				'Perkantoran' => base_url('jurnal/S8Q'),
																				'Kendaraan' => base_url('jurnal/S8R'),
																				'Tempat Parkir' => base_url('jurnal/S8S'),
																				'Pengolahan Limbah' => base_url('jurnal/S8T')
																				),
															'Fasilitas Penunjang' => Array(
																				'Pembinaan Nelayan' => base_url('jurnal/S9A'),
																				'Pengelola Pelabuhan' => base_url('jurnal/S9B'),
																				'Sosial dan Umum' => base_url('jurnal/S9C'),
																				'Kios Iptek' => base_url('jurnal/S9D')
																				),
															'Data Lingkungan Fisik Pelabuhan' => Array(
																				'Geoteknik,  Banjir & Tsunami' => base_url('jurnal/S10A'),
																				//'S10B - Hidrooseanografi' => base_url('jurnal/S10B'),
																				//'S10C - Geoteknik' => base_url('jurnal/S10C'),
																				//'S10D - Sedimentasi' => base_url('jurnal/S10D'),
																				//'S10E - Banjir & Tsunami' => base_url('jurnal/S10E'),
																				//'S10F - Topografi & Batimetri' => base_url('jurnal/S10F'),
																				'Sungai' => base_url('jurnal/S10B')
																				),
															'Kelembagaan UPT Pelabuhan' => base_url('jurnal/S11'),
															'Kelembagaan di Dalam Pelabuhan' => base_url('jurnal/S12'),
															'Masyarakat Perikanan' => base_url('jurnal/S13'),
															'Monitoring K5' => base_url('entry/K5')
															//'S10 - Data Lingkungan Fisik Pelabuhan' => base_url('forms/view/S10')
														),
	        					'Berita' => base_url('berita/daftar'),
	        					'Laporan' => Array(
	        											'Data Umum Pelabuhan Perikanan' => base_url('report/situasional_pelabuhan/index'),
	        											'Data Jumlah Kapal' => base_url('report/operasional_kapal/index'),
	        											'Data Alat Tangkap' => base_url('report/alat_tangkap/index'),
	        											'Produksi dan Nilai Produksi Ikan' => base_url('report/produksi/index'),
	        											'Data Distribusi Ikan' => base_url('report/distribusi_ikan/index'),
	        											'Data Pendapatan Bulanan' => base_url('report/pendapatan/index'),
	        											'Data Kebutuhan BBM' => base_url('report/kebutuhan_bbm/index'),
	        											'Data Instansi dan Lembaga Di Pelabuhan' => base_url('report/instansi_lembaga/index'),
	        											'Penyalur Perbekalan di Pelabuhan Perikanan' => base_url('report/penyalur_perbekalan/index'),
	        											'Unit Usaha di Pelabuhan Perikanan' => base_url('report/unit_usaha/index'),
	        											'Tenaga Kerja di Pelabuhan Perikanan' => base_url('report/tenaga_kerja/index'),
	        											'Usaha Pengolahan di Pelabuhan Perikanan' => base_url('report/usaha_pengolahan/index')
	        											),
	        					'Pencarian' => Array(
	        										'Operasional' => base_url('pencarian/operasional'),
	        										'Fungsional' => base_url('pencarian/fungsional')
	        									),
	        					'admin_menu' => array(
	        										'Pengaturan' => base_url('admin/konfigurasi'),
													'Admin Website' => base_url('admin')
												)
        					),
			'list_report' => Array(
												'Data Umum Pelabuhan Perikanan' => '#',
												'Perkembangan Produksi dan Nilai Produksi Ikan' => 'report/laporan/nilai_produksi'
											),
			'kondisi_ikan' => Array(
								'1' => 'Segar',
                               	'2' => 'Beku',
                               	'3' => 'Hidup',
                               	'4' => 'Asin' ),

			'transportasi' => array(
								'Mobil' => 'Mobil',
                               	'Kapal Angkut' => 'Kapal Angkut'
                               )
	    );



?>