<form action="http://pippnew.djpt.kkp.go.id/dataentry/index.php/jurnal_b6/input" id="form_entry" method="post" accept-charset="utf-8"><div style="display:none">
<input type="hidden" name="id_pelabuhan" value="020301">
</div>
<div id="pilih_tanggal">
    <table class="tanggal">
        <tbody><tr>
            <td class="tengah"><label for="tahun">Tahun</label>
            <select size="1" name="tahun" id="opsi_tahun">
            <option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option>            </select></td>
            
           <td class="tengah"> <label for="bulan">Bulan</label>
            <select size="1" name="bulan" id="opsi_bulan">
            <option value="1">Januari</option><option value="2">Pebruari</option><option value="3">Maret</option><option value="4">April</option><option value="5">Mei</option><option value="6">Juni</option><option value="7">Juli</option><option value="8">Agustus</option><option value="9">September</option><option value="10">Oktober</option><option value="11">November</option><option value="12">Desember</option>            </select>
            
        </td></tr>    
    </tbody></table> 
    <br>
    <p id="tanggal_selected"></p>   
</div>
<table border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>No. </th><th>Jenis Ikan</th><th>Alat Tangkap</th><th>Koefisien Koreksi (%)</th></tr>
</thead>
<tbody>
<tr>
<td>1. </td><td><select name="id_species_1"><option value="0">Kosong</option><option value="3" title="Nama Lokal: AJI-AJI
 Nama Lokal Daerah: AJI-AJI
 Nama Indonesia: Albakora
 Nama Internasional: Albacore
 Nama Latin: Thunnus alalunga (Bonnaterre, 1788)" '="">Albakora</option><option value="51" title="Nama Lokal: ALU-ALU
 Nama Lokal Daerah: ALU-ALU
 Nama Indonesia: Alu-alu/ Manggilala/Pucul
 Nama Internasional: Great barracuda
 Nama Latin: Sphyraena barracuda (Walbaum, 1792)" '="">Alu-alu/ Manggilala/Pucul</option><option value="46" title="Nama Lokal: CINCARO
 Nama Lokal Daerah: CINCARO
 Nama Indonesia: Banyar/Kembung Lelaki
 Nama Internasional: Indian mackerel
 Nama Latin: Rastrelliger kanagurta" '="">Banyar/Kembung Lelaki</option><option value="57" title="Nama Lokal: BAWAL HITAM
 Nama Lokal Daerah: BAWAL HITAM
 Nama Indonesia: Bawal hitam
 Nama Internasional: Black pomfret
 Nama Latin: Parastromateus niger (Bloch, 1795)" '="">Bawal hitam</option><option value="58" title="Nama Lokal: BAWAL PUTIH
 Nama Lokal Daerah: BAWAL PUTIH
 Nama Indonesia: Bawal putih
 Nama Internasional: Silver pomfret
 Nama Latin: Pampus argenteus (Euphrasen, 1788)" '="">Bawal putih</option><option value="73" title="Nama Lokal: BELANAK
 Nama Lokal Daerah: BELANAK
 Nama Indonesia: Belanak
 Nama Internasional: Blue-spot mullet/ Blue-tail mullet
 Nama Latin: Valamugil seheli (Forssk�l, 1775)" '="">Belanak</option><option value="299" title="Nama Lokal: belanak
 Nama Lokal Daerah: belanak
 Nama Indonesia: Belanak
 Nama Internasional: Mangrove mullets
 Nama Latin: Mugil cephalus (Linnaeus, 1758)" '="">Belanak</option><option value="67" title="Nama Lokal: beloso
 Nama Lokal Daerah: beloso
 Nama Indonesia: Beloso/Buntut kerbo
 Nama Internasional: Greater lizardfish
 Nama Latin: Saurida tumbil (Bloch, 1795)" '="">Beloso/Buntut kerbo</option><option value="264" title="Nama Lokal: MATA BESAR
 Nama Lokal Daerah: MATA BESAR
 Nama Indonesia: Bentong
 Nama Internasional: Oxeye scad
 Nama Latin: Selar boops (Valenciennes, 1833)" '="">Bentong</option><option value="279" title="Nama Lokal: BIJI NANGKA
 Nama Lokal Daerah: BIJI NANGKA
 Nama Indonesia: Biji nangka
 Nama Internasional: Yellow-stripe goatfish
 Nama Latin: Upeneus vittatus (Forssk�l, 1775)" '="">Biji nangka</option><option value="179" title="Nama Lokal: Bijinangka
 Nama Lokal Daerah: Bijinangka
 Nama Indonesia: Bijinangka/Kuniran
 Nama Internasional: Goatfish
 Nama Latin: Upeneus moluccensis" '="">Bijinangka/Kuniran</option><option value="7" title="Nama Lokal: Tongkol
 Nama Lokal Daerah: Tongkol
 Nama Indonesia: Cakalang
 Nama Internasional: Skipjack tuna
 Nama Latin: Katsuwonus pelamis (Linnaeus, 1758)" '="">Cakalang</option><option value="18" title="Nama Lokal: CUCUT
 Nama Lokal Daerah: CUCUT
 Nama Indonesia: Cucut botol
 Nama Internasional: Longnose velvet dogfish
 Nama Latin: Centrocymnus crepidater" '="">Cucut botol</option><option value="225" title="Nama Lokal: CUMI-CUMI
 Nama Lokal Daerah: CUMI-CUMI
 Nama Indonesia: Cumi-cumi
 Nama Internasional: Common squids
 Nama Latin: Loligo spp" '="">Cumi-cumi</option><option value="96" title="Nama Lokal: EKOR KUNING
 Nama Lokal Daerah: EKOR KUNING
 Nama Indonesia: Ekor kuning
 Nama Internasional: Redbelly yellowtail fusilier
 Nama Latin: Caesio cuning (Bloch, 1791)" '="">Ekor kuning</option><option value="69" title="Nama Lokal: Gerot
 Nama Lokal Daerah: Gerot
 Nama Indonesia: Gerot-gerot
 Nama Internasional: Saddle grunt/ Spotted javelinfish
 Nama Latin: Pomadasys maculatum(Bloch, 1797)" '="">Gerot-gerot</option><option value="48" title="Nama Lokal: Golok-golok
 Nama Lokal Daerah: Golok-golok
 Nama Indonesia: Golok-golok
 Nama Internasional: Dorab wolf-herring
 Nama Latin: Chirocentrus dorab (Forssk�l, 1775)" '="">Golok-golok</option><option value="61" title="Nama Lokal: GULAMAH
 Nama Lokal Daerah: GULAMAH
 Nama Indonesia: Gulamah/Tigawaja
 Nama Internasional: Croackers
 Nama Latin: Nibea albiflora (Ricahardson, 1846)" '="">Gulamah/Tigawaja</option><option value="94" title="Nama Lokal: Baronang
 Nama Lokal Daerah: Baronang
 Nama Indonesia: Ikan beronang
 Nama Internasional: Orange-spotted spinefoot
 Nama Latin: Siganus guttatus (Bloch, 1787)" '="">Ikan beronang</option><option value="89" title="Nama Lokal: KAKAP MERAH
 Nama Lokal Daerah: KAKAP MERAH
 Nama Indonesia: Ikan kakap merah/Bambangan
 Nama Internasional: Red snappers
 Nama Latin: Lutjanus spp" '="">Ikan kakap merah/Bambangan</option><option value="72" title="Nama Lokal: LIDAH
 Nama Lokal Daerah: LIDAH
 Nama Indonesia: Ikan lidah
 Nama Internasional: Tongue soles
 Nama Latin: Cynoglossus spp, Pleuronectus spp." '="">Ikan lidah</option><option value="64" title="Nama Lokal: IKAN SEBELAH
 Nama Lokal Daerah: IKAN SEBELAH
 Nama Indonesia: Ikan sebelah (Terompa)
 Nama Internasional: Indian halibut/ Queensland halibut
 Nama Latin: Psettodes erumei (Schneider, 1801)" '="">Ikan sebelah (Terompa)</option><option value="90" title="Nama Lokal: KAKAP
 Nama Lokal Daerah: kakap
 Nama Indonesia: Kakap putih
 Nama Internasional: Barramundi, Giant sea perch
 Nama Latin: Lates calcarifer (Bloch)" '="">Kakap putih</option><option value="294" title="Nama Lokal: KAPAS-KAPAS
 Nama Lokal Daerah: KAPAS-KAPAS
 Nama Indonesia: Kapas-kapas
 Nama Internasional: Fals trevally
 Nama Latin: Lactarius lactarius (Bloch &amp; Schneider, 1801)" '="">Kapas-kapas</option><option value="276" title="Nama Lokal: KEMBUNG
 Nama Lokal Daerah: KEMBUNG
 Nama Indonesia: Kembung Perempuan
 Nama Internasional: Short-bodied mackerel
 Nama Latin: Rastrelliger brachysoma" '="">Kembung Perempuan</option><option value="296" title="Nama Lokal: pasir-pasir
 Nama Lokal Daerah: pasir-pasir
 Nama Indonesia: Kempar Pati
 Nama Internasional: Sharptooth jobfish
 Nama Latin: Pristipomoides typus (Bleeker, 1852)" '="">Kempar Pati</option><option value="87" title="Nama Lokal: Kerapu
 Nama Lokal Daerah: kerapu
 Nama Indonesia: Kerapu balong
 Nama Internasional: Honeycomb grouper
 Nama Latin: Epinephelus merra (Bloch, 1793)" '="">Kerapu balong</option><option value="286" title="Nama Lokal: KERAPU
 Nama Lokal Daerah: KERAPU
 Nama Indonesia: Kerapu karang
 Nama Internasional: Blue-lined seabass
 Nama Latin: Cephalophodis boenack (Bloch, 1790)" '="">Kerapu karang</option><option value="74" title="Nama Lokal: Kerong-kerong
 Nama Lokal Daerah: Kerong-kerong
 Nama Indonesia: Kerong-kerong
 Nama Internasional: Largescale terapon
 Nama Latin: Terapon theraps (Cuvier, 1829)" '="">Kerong-kerong</option><option value="70" title="Nama Lokal: KURISI
 Nama Lokal Daerah: KURISI
 Nama Indonesia: Kurisi
 Nama Internasional: Threadfin bream
 Nama Latin: Nemimterus spp" '="">Kurisi</option><option value="278" title="Nama Lokal: SENANGIN
 Nama Lokal Daerah: SENANGIN
 Nama Indonesia: Kuro/senangin
 Nama Internasional: Threadfins
 Nama Latin: Polynemus spp" '="">Kuro/senangin</option><option value="34" title="Nama Lokal: KUWE
 Nama Lokal Daerah: KUWE
 Nama Indonesia: Kwee
 Nama Internasional: Bigeye trevally
 Nama Latin: Caranx sexfaciatus (Quoy &amp; Gaimard,1825)" '="">Kwee</option><option value="269" title="Nama Lokal: kuwe
 Nama Lokal Daerah: kuwe
 Nama Indonesia: Kwee
 Nama Internasional: Tille trevally
 Nama Latin: Caranx tile (Cuvier, 1833)" '="">Kwee</option><option value="271" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang biru
 Nama Internasional: Mackerel scad
 Nama Latin: Decapterus macarellus (Cuvier, 1833)" '="">Layang biru</option><option value="38" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang deles
 Nama Internasional: Shortfin scad
 Nama Latin: Decapterus macrosoma (Bleeker, 1855)" '="">Layang deles</option><option value="31" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang/Benggol
 Nama Internasional: Indian scad
 Nama Latin: Decapterus. Ruselli (R�ppell, 1830)" '="">Layang/Benggol</option><option value="62" title="Nama Lokal: LAYUR
 Nama Lokal Daerah: CUALAI
 Nama Indonesia: Layur
 Nama Internasional: Hairtails
 Nama Latin: Trichiurus spp" '="">Layur</option><option value="42" title="Nama Lokal: LEMURU
 Nama Lokal Daerah: LEMURU
 Nama Indonesia: Lemuru
 Nama Internasional: Bali sardinella
 Nama Latin: Sardinella lemuru (Bleeker, 1853)" '="">Lemuru</option><option value="54" title="Nama Lokal: MANYUNG
 Nama Lokal Daerah: MANYUNG
 Nama Indonesia: Manyung
 Nama Internasional: Giant catfish
 Nama Latin: Arius thalassinus (R�ppell, 1837)" '="">Manyung</option><option value="133" title="Nama Lokal: LAYARAN
 Nama Lokal Daerah: LAYARAN
 Nama Indonesia: Moris layaran
 Nama Internasional: Horned coralfish
 Nama Latin: Heniochus permutatus" '="">Moris layaran</option><option value="80" title="Nama Lokal: Pari
 Nama Lokal Daerah: Pari
 Nama Indonesia: Pari burung
 Nama Internasional: Eaglerays
 Nama Latin: Myliobatus spp, Aetobatus spp. (Euphrasen, 1790) Aetomylaeus spp" '="">Pari burung</option><option value="285" title="Nama Lokal: pari
 Nama Lokal Daerah: pari
 Nama Indonesia: Pari kelelawar
 Nama Internasional: Devilrays, Mantarays
 Nama Latin: Mobula spp" '="">Pari kelelawar</option><option value="78" title="Nama Lokal: PARI
 Nama Lokal Daerah: PARI
 Nama Indonesia: Pari kembang/Pari macan
 Nama Internasional: Stingrays
 Nama Latin: Dasyatis spp" '="">Pari kembang/Pari macan</option><option value="66" title="Nama Lokal: PISANG-PISANG
 Nama Lokal Daerah: PISANG-PISANG
 Nama Indonesia: Peperek
 Nama Internasional: Slipmouths or Pony fishes
 Nama Latin: Leiognathus spp" '="">Peperek</option><option value="204" title="Nama Lokal: RAJUNGAN
 Nama Lokal Daerah: RAJUNGAN
 Nama Indonesia: Rajungan
 Nama Internasional: Swimming Crabs
 Nama Latin: Portunus pelagicus (Linnaeus, 1766)" '="">Rajungan</option><option value="262" title="Nama Lokal: SELAR
 Nama Lokal Daerah: SELAR
 Nama Indonesia: Selar kuning
 Nama Internasional: Black banded trevally
 Nama Latin: Seriolina nigrofasciata (R�ppell, 1829)" '="">Selar kuning</option><option value="263" title="Nama Lokal: selar
 Nama Lokal Daerah: selar
 Nama Indonesia: Selar kuning
 Nama Internasional: White mouth jack
 Nama Latin: Uraspis uraspis" '="">Selar kuning</option><option value="174" title="Nama Lokal: SEMBILANG
 Nama Lokal Daerah: SEMBILANG
 Nama Indonesia: Sembilang karang
 Nama Internasional: Striped catfish-eel
 Nama Latin: Plotosus lineatus" '="">Sembilang karang</option><option value="226" title="Nama Lokal: SOTONG
 Nama Lokal Daerah: SOTONG
 Nama Indonesia: Sotong
 Nama Internasional: Cuttle fish
 Nama Latin: Sepia Spp" '="">Sotong</option><option value="60" title="Nama Lokal: SWANGGI
 Nama Lokal Daerah: SWANGGI
 Nama Indonesia: Swanggi
 Nama Internasional: Purple-spotted bigeye
 Nama Latin: Priacanthus tayenus (Richardson, 1846)" '="">Swanggi</option><option value="37" title="Nama Lokal: TALANG-TALANG
 Nama Lokal Daerah: TALANG-TALANG
 Nama Indonesia: Talang-talang
 Nama Internasional: Talang queenfish
 Nama Latin: Scomberoides commersonnianus (Lacep�de, 1802)" '="">Talang-talang</option><option value="270" title="Nama Lokal: Talang-talang
 Nama Lokal Daerah: Talang-talang
 Nama Indonesia: Talang-talang
 Nama Internasional: Needlescale queenfish
 Nama Latin: Scomberoides tol (Cuvier, 1832)" '="">Talang-talang</option><option value="41" title="Nama Lokal: TAMBAN
 Nama Lokal Daerah: TAMBAN
 Nama Indonesia: Tembang
 Nama Internasional: Goldstripe sardinella
 Nama Latin: Sardinella gibbosa (Bleeker, 1849)" '="">Tembang</option><option value="273" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Deepbody sardinella
 Nama Latin: Sardinella brachysoma (Bleeker, 1852)" '="">Tembang</option><option value="274" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Fringescale sardinella
 Nama Latin: Sardinella fimbriata (Valenciennes, 1847)" '="">Tembang</option><option value="16" title="Nama Lokal: TENGGIRI
 Nama Lokal Daerah: TENGGIRI
 Nama Indonesia: Tenggiri
 Nama Internasional: Narrow-barred Spanish mackerel
 Nama Latin: Scomberomorus commerson (Lacep�de, 1800)" '="">Tenggiri</option><option value="17" title="Nama Lokal: tenggiri
 Nama Lokal Daerah: tenggiri
 Nama Indonesia: Tenggiri papan
 Nama Internasional: Indo-Pacific king mackerel
 Nama Latin: Scomberomorus guttatus (Bloch &amp; Schneider, 1801)" '="">Tenggiri papan</option><option value="39" title="Nama Lokal: TERI
 Nama Lokal Daerah: TERI
 Nama Indonesia: Teri
 Nama Internasional: Anchovies
 Nama Latin: Stolephorus spp." '="">Teri</option><option value="44" title="Nama Lokal: terubuk
 Nama Lokal Daerah: terubuk
 Nama Indonesia: Terubuk
 Nama Internasional: Hilsha shad
 Nama Latin: Tenualosa ilisha (Hamilton-Buchanan,1822)" '="">Terubuk</option><option value="35" title="Nama Lokal: TETENGKEK
 Nama Lokal Daerah: TETENGKEK
 Nama Indonesia: Tetengkek
 Nama Internasional: Torpedo scad
 Nama Latin: Megalaspis cordyla (Linnaeus, 1758)" '="">Tetengkek</option><option value="260" title="Nama Lokal: TONGKOL
 Nama Lokal Daerah: TONGKOL
 Nama Indonesia: Tongkol abu-abu
 Nama Internasional: Longtail tuna
 Nama Latin: Thunnus tonggol (Bleeker, 1851)" '="">Tongkol abu-abu</option><option value="302" title="Nama Lokal: UDANG DOGOL
 Nama Lokal Daerah: UDANG DOGOL
 Nama Indonesia: Udang  Dogol
 Nama Internasional: Endeavour prawn/ Shrimp red greasiback
 Nama Latin: Metapenaeus ensis, (de Haan, 1850), Metapenaeus monoceros, (Fabricius, 1798)" '="">Udang  Dogol</option><option value="301" title="Nama Lokal: udang windu
 Nama Lokal Daerah: udang windu
 Nama Indonesia: Udang  Windu
 Nama Internasional: Tiger prawn/ Brown tiger prawn
 Nama Latin: Penaeus esculentus (Haswell, 1879)" '="">Udang  Windu</option><option value="184" title="Nama Lokal: udang putih
 Nama Lokal Daerah: udang putih
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn/ White shrimp
 Nama Latin: Penaeus merguiensis (de Man, 1888 )" '="">Udang Jerbung/Udang putih</option><option value="187" title="Nama Lokal: udang jerubung
 Nama Lokal Daerah: udang jerubung
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn, Indian banana
 Nama Latin: Penaeus indicus (Milne Edwards, 1837)" '="">Udang Jerbung/Udang putih</option><option value="290" title="Nama Lokal: UDANG LAINNYA
 Nama Lokal Daerah: UDANG LAINNYA
 Nama Indonesia: Udang Lainnya
 Nama Internasional: Other shrimps
 Nama Latin: " '="">Udang Lainnya</option><option value="183" title="Nama Lokal: UDANG WINDU
 Nama Lokal Daerah: UDANG WINDU
 Nama Indonesia: Udang windu
 Nama Internasional: Jumbo tiger prawn/ shrimp, Giant tiger prawn/shrimp, Blue tiger prawn
 Nama Latin: Penaeus monodon (Fabricius, 1798)" '="">Udang windu</option></select></td><td><select name="id_alat_tangkap_1"><option value="1" title="Nama Internasional: Double rigs shrimp trawl">Pukat tarik udang ganda</option><option value="2" title="Nama Internasional: Otter Trawl">Otter Trawl</option><option value="3" title="Nama Internasional: Fish net">Pukat tarik ikan</option><option value="4" title="Nama Internasional: Pelagic danish seine">Payang (tmsk. Lampara)</option><option value="5" title="Nama Internasional: Demersal danish seine">Dogol (tmsk. Lampara dasar, cantrang)</option><option value="6" title="Nama Internasional: Beach seine">Pukat Pantai (Jaring arad)</option><option value="26" title="Nama Internasional: Other traps">Perangkap lainnya</option><option value="7" title="Nama Internasional: Purse seine">Pukat Cincin</option><option value="8" title="Nama Internasional: Drift gill nets">Jaring insang hanyut</option><option value="9" title="Nama Internasional: Encircling gill nets">Jaring insang lingkar</option><option value="10" title="Nama Internasional: Shrimp entangling gill net">Jaring klitik</option><option value="11" title="Nama Internasional: Set gill nets">Jaring insang tetap</option><option value="12" title="Nama Internasional: Trammel net">Jaring tiga lapis</option><option value="13" title="Nama Internasional: Boat/raft lift net">Bagan perahu/rakit</option><option value="14" title="Nama Internasional: Stationary lift net">Bagan tancap</option><option value="15" title="Nama Internasional: Scoop nets">Serok dan songko</option><option value="16" title="Nama Internasional: Other lift nets">Jaring Angkat Lainnya</option><option value="17" title="Nama Internasional: Tuna long line">Rawai tuna</option><option value="18" title="Nama Internasional: Other drift long line">Rawai hanyut lainnya selain rawai tuna</option><option value="19" title="Nama Internasional: Set long line">Rawai tetap</option><option value="20" title="Nama Internasional: Skipjack pole and line">Huhate</option><option value="21" title="Nama Internasional: Trowl lines">Pancing tonda</option><option value="22" title="Nama Internasional: Hand lines">Pancing ulur</option><option value="23" title="Nama Internasional: Guiding barrier">Sero (tmsk. Kelong)</option><option value="24" title="Nama Internasional: Stow net">Jermal</option><option value="25" title="Nama Internasional: Portable trap">Bubu (tmsk. Bubu ambai)</option><option value="27" title="Nama Internasional: Shell fish gears">Alat penangkap kerang</option><option value="28" title="Nama Internasional: Sea weed collectors">Alat pengumpul rumput laut</option><option value="29" title="Nama Internasional: Muroami">Muroami</option><option value="30" title="Nama Internasional: Cast net">Jala tebar</option><option value="31" title="Nama Internasional: ">Tidak Ada Alat Tangkap</option><option value="32" title="Nama Internasional: Beam trawl">Pukat tarik berbingkai</option><option value="33" title="Nama Internasional: Shore lift net">Anco</option><option value="34" title="Nama Internasional: Set bottom long line">Rawai dasar tetap</option><option value="35" title="Nama Internasional: Vertical line (incl. Vertical long line)">Pancing tegak</option><option value="36" title="Nama Internasional: Squid Jigger">Pancing cumi</option><option value="37" title="Nama Internasional: Other lines">Pancing lainnya</option><option value="38" title="Nama Internasional: Sea cucumber gears">Alat penangkap teripang (ladung)</option><option value="39" title="Nama Internasional: Crab gears">Alat penangkap kepiting</option><option value="40" title="Nama Internasional: Harpoon">Garpu dan Tombak</option><option value="41" title="Nama Internasional: Stern shrimp trawl">Pukat tarik udang tunggal</option><option value="42" title="Nama Internasional: ">Pukat cincin satu kapal</option><option value="43" title="Nama Internasional: ">Pukat Cincin Pelagis Besar dengan satu kapal</option><option value="44" title="Nama Internasional: ">Jaring lingkar tanpa tali kerut/Lampara</option><option value="45" title="Nama Internasional: ">Pukat hela dasar berpalang</option><option value="46" title="Nama Internasional: ">Pukat hela dasar berpapan</option><option value="47" title="Nama Internasional: ">Pukat hela dasar dua kapal</option><option value="48" title="Nama Internasional: ">Pukat hela dasar lainnya</option><option value="49" title="Nama Internasional: ">Pukat hela dasar udang</option><option value="50" title="Nama Internasional: ">Pukat hela pertengahan berpapan</option><option value="51" title="Nama Internasional: ">Puka hela pertengahan dua kapal</option><option value="52" title="Nama Internasional: ">Pukat hela pertengahan udang</option><option value="53" title="Nama Internasional: ">Pukat hela kembar berpapan</option><option value="54" title="Nama Internasional: ">Pukat hela lainnya</option><option value="55" title="Nama Internasional: ">Pukat dorong</option><option value="56" title="Nama Internasional: ">Boke Ami</option><option value="57" title="Nama Internasional: ">Jala jatuh berkapal</option><option value="58" title="Nama Internasional: ">Jala tebar lainnya</option><option value="59" title="Nama Internasional: ">Jaring liong bun</option><option value="60" title="Nama Internasional: ">Jaring insang oseanik</option><option value="61" title="Nama Internasional: tramel net">Jaring insang berlapis</option><option value="62" title="Nama Internasional: ">Jaring insang kombinasi dengan tramel net</option><option value="63" title="Nama Internasional: ">Jaring insang lainnya</option><option value="64" title="Nama Internasional: ">Set Net</option><option value="65" title="Nama Internasional: ">Bubu bersayap</option><option value="66" title="Nama Internasional: Long bag set net">Pukat labuh</option><option value="67" title="Nama Internasional: ">Togo</option><option value="68" title="Nama Internasional: ">Ambai</option><option value="69" title="Nama Internasional: ">Pengerih</option><option value="70" title="Nama Internasional: ">Perangkap ikan peloncat</option><option value="71" title="Nama Internasional: ">Seser</option><option value="72" title="Nama Internasional: ">Pancing berjoran</option><option value="73" title="Nama Internasional: ">Squid angling</option><option value="74" title="Nama Internasional: ">Huhate Mekanis</option><option value="75" title="Nama Internasional: ">Pancing layang-layang</option><option value="76" title="Nama Internasional: ">Ladung</option><option value="77" title="Nama Internasional: ">Panah</option><option value="78" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="79" title="Nama Internasional: ">Ladung</option><option value="80" title="Nama Internasional: ">Panah</option><option value="81" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="82" title="Nama Internasional: tes">Alat tangkap tes</option><option value="83" title="Nama Internasional: ">Pengangkut Ikan</option></select></td><td><input type="text" name="koef_koreksi_1" value="0" style="width: 180px;" title="Data yang di masukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td><select name="id_species_2"><option value="0">Kosong</option><option value="3" title="Nama Lokal: AJI-AJI
 Nama Lokal Daerah: AJI-AJI
 Nama Indonesia: Albakora
 Nama Internasional: Albacore
 Nama Latin: Thunnus alalunga (Bonnaterre, 1788)" '="">Albakora</option><option value="51" title="Nama Lokal: ALU-ALU
 Nama Lokal Daerah: ALU-ALU
 Nama Indonesia: Alu-alu/ Manggilala/Pucul
 Nama Internasional: Great barracuda
 Nama Latin: Sphyraena barracuda (Walbaum, 1792)" '="">Alu-alu/ Manggilala/Pucul</option><option value="46" title="Nama Lokal: CINCARO
 Nama Lokal Daerah: CINCARO
 Nama Indonesia: Banyar/Kembung Lelaki
 Nama Internasional: Indian mackerel
 Nama Latin: Rastrelliger kanagurta" '="">Banyar/Kembung Lelaki</option><option value="57" title="Nama Lokal: BAWAL HITAM
 Nama Lokal Daerah: BAWAL HITAM
 Nama Indonesia: Bawal hitam
 Nama Internasional: Black pomfret
 Nama Latin: Parastromateus niger (Bloch, 1795)" '="">Bawal hitam</option><option value="58" title="Nama Lokal: BAWAL PUTIH
 Nama Lokal Daerah: BAWAL PUTIH
 Nama Indonesia: Bawal putih
 Nama Internasional: Silver pomfret
 Nama Latin: Pampus argenteus (Euphrasen, 1788)" '="">Bawal putih</option><option value="73" title="Nama Lokal: BELANAK
 Nama Lokal Daerah: BELANAK
 Nama Indonesia: Belanak
 Nama Internasional: Blue-spot mullet/ Blue-tail mullet
 Nama Latin: Valamugil seheli (Forssk�l, 1775)" '="">Belanak</option><option value="299" title="Nama Lokal: belanak
 Nama Lokal Daerah: belanak
 Nama Indonesia: Belanak
 Nama Internasional: Mangrove mullets
 Nama Latin: Mugil cephalus (Linnaeus, 1758)" '="">Belanak</option><option value="67" title="Nama Lokal: beloso
 Nama Lokal Daerah: beloso
 Nama Indonesia: Beloso/Buntut kerbo
 Nama Internasional: Greater lizardfish
 Nama Latin: Saurida tumbil (Bloch, 1795)" '="">Beloso/Buntut kerbo</option><option value="264" title="Nama Lokal: MATA BESAR
 Nama Lokal Daerah: MATA BESAR
 Nama Indonesia: Bentong
 Nama Internasional: Oxeye scad
 Nama Latin: Selar boops (Valenciennes, 1833)" '="">Bentong</option><option value="279" title="Nama Lokal: BIJI NANGKA
 Nama Lokal Daerah: BIJI NANGKA
 Nama Indonesia: Biji nangka
 Nama Internasional: Yellow-stripe goatfish
 Nama Latin: Upeneus vittatus (Forssk�l, 1775)" '="">Biji nangka</option><option value="179" title="Nama Lokal: Bijinangka
 Nama Lokal Daerah: Bijinangka
 Nama Indonesia: Bijinangka/Kuniran
 Nama Internasional: Goatfish
 Nama Latin: Upeneus moluccensis" '="">Bijinangka/Kuniran</option><option value="7" title="Nama Lokal: Tongkol
 Nama Lokal Daerah: Tongkol
 Nama Indonesia: Cakalang
 Nama Internasional: Skipjack tuna
 Nama Latin: Katsuwonus pelamis (Linnaeus, 1758)" '="">Cakalang</option><option value="18" title="Nama Lokal: CUCUT
 Nama Lokal Daerah: CUCUT
 Nama Indonesia: Cucut botol
 Nama Internasional: Longnose velvet dogfish
 Nama Latin: Centrocymnus crepidater" '="">Cucut botol</option><option value="225" title="Nama Lokal: CUMI-CUMI
 Nama Lokal Daerah: CUMI-CUMI
 Nama Indonesia: Cumi-cumi
 Nama Internasional: Common squids
 Nama Latin: Loligo spp" '="">Cumi-cumi</option><option value="96" title="Nama Lokal: EKOR KUNING
 Nama Lokal Daerah: EKOR KUNING
 Nama Indonesia: Ekor kuning
 Nama Internasional: Redbelly yellowtail fusilier
 Nama Latin: Caesio cuning (Bloch, 1791)" '="">Ekor kuning</option><option value="69" title="Nama Lokal: Gerot
 Nama Lokal Daerah: Gerot
 Nama Indonesia: Gerot-gerot
 Nama Internasional: Saddle grunt/ Spotted javelinfish
 Nama Latin: Pomadasys maculatum(Bloch, 1797)" '="">Gerot-gerot</option><option value="48" title="Nama Lokal: Golok-golok
 Nama Lokal Daerah: Golok-golok
 Nama Indonesia: Golok-golok
 Nama Internasional: Dorab wolf-herring
 Nama Latin: Chirocentrus dorab (Forssk�l, 1775)" '="">Golok-golok</option><option value="61" title="Nama Lokal: GULAMAH
 Nama Lokal Daerah: GULAMAH
 Nama Indonesia: Gulamah/Tigawaja
 Nama Internasional: Croackers
 Nama Latin: Nibea albiflora (Ricahardson, 1846)" '="">Gulamah/Tigawaja</option><option value="94" title="Nama Lokal: Baronang
 Nama Lokal Daerah: Baronang
 Nama Indonesia: Ikan beronang
 Nama Internasional: Orange-spotted spinefoot
 Nama Latin: Siganus guttatus (Bloch, 1787)" '="">Ikan beronang</option><option value="89" title="Nama Lokal: KAKAP MERAH
 Nama Lokal Daerah: KAKAP MERAH
 Nama Indonesia: Ikan kakap merah/Bambangan
 Nama Internasional: Red snappers
 Nama Latin: Lutjanus spp" '="">Ikan kakap merah/Bambangan</option><option value="72" title="Nama Lokal: LIDAH
 Nama Lokal Daerah: LIDAH
 Nama Indonesia: Ikan lidah
 Nama Internasional: Tongue soles
 Nama Latin: Cynoglossus spp, Pleuronectus spp." '="">Ikan lidah</option><option value="64" title="Nama Lokal: IKAN SEBELAH
 Nama Lokal Daerah: IKAN SEBELAH
 Nama Indonesia: Ikan sebelah (Terompa)
 Nama Internasional: Indian halibut/ Queensland halibut
 Nama Latin: Psettodes erumei (Schneider, 1801)" '="">Ikan sebelah (Terompa)</option><option value="90" title="Nama Lokal: KAKAP
 Nama Lokal Daerah: kakap
 Nama Indonesia: Kakap putih
 Nama Internasional: Barramundi, Giant sea perch
 Nama Latin: Lates calcarifer (Bloch)" '="">Kakap putih</option><option value="294" title="Nama Lokal: KAPAS-KAPAS
 Nama Lokal Daerah: KAPAS-KAPAS
 Nama Indonesia: Kapas-kapas
 Nama Internasional: Fals trevally
 Nama Latin: Lactarius lactarius (Bloch &amp; Schneider, 1801)" '="">Kapas-kapas</option><option value="276" title="Nama Lokal: KEMBUNG
 Nama Lokal Daerah: KEMBUNG
 Nama Indonesia: Kembung Perempuan
 Nama Internasional: Short-bodied mackerel
 Nama Latin: Rastrelliger brachysoma" '="">Kembung Perempuan</option><option value="296" title="Nama Lokal: pasir-pasir
 Nama Lokal Daerah: pasir-pasir
 Nama Indonesia: Kempar Pati
 Nama Internasional: Sharptooth jobfish
 Nama Latin: Pristipomoides typus (Bleeker, 1852)" '="">Kempar Pati</option><option value="87" title="Nama Lokal: Kerapu
 Nama Lokal Daerah: kerapu
 Nama Indonesia: Kerapu balong
 Nama Internasional: Honeycomb grouper
 Nama Latin: Epinephelus merra (Bloch, 1793)" '="">Kerapu balong</option><option value="286" title="Nama Lokal: KERAPU
 Nama Lokal Daerah: KERAPU
 Nama Indonesia: Kerapu karang
 Nama Internasional: Blue-lined seabass
 Nama Latin: Cephalophodis boenack (Bloch, 1790)" '="">Kerapu karang</option><option value="74" title="Nama Lokal: Kerong-kerong
 Nama Lokal Daerah: Kerong-kerong
 Nama Indonesia: Kerong-kerong
 Nama Internasional: Largescale terapon
 Nama Latin: Terapon theraps (Cuvier, 1829)" '="">Kerong-kerong</option><option value="70" title="Nama Lokal: KURISI
 Nama Lokal Daerah: KURISI
 Nama Indonesia: Kurisi
 Nama Internasional: Threadfin bream
 Nama Latin: Nemimterus spp" '="">Kurisi</option><option value="278" title="Nama Lokal: SENANGIN
 Nama Lokal Daerah: SENANGIN
 Nama Indonesia: Kuro/senangin
 Nama Internasional: Threadfins
 Nama Latin: Polynemus spp" '="">Kuro/senangin</option><option value="34" title="Nama Lokal: KUWE
 Nama Lokal Daerah: KUWE
 Nama Indonesia: Kwee
 Nama Internasional: Bigeye trevally
 Nama Latin: Caranx sexfaciatus (Quoy &amp; Gaimard,1825)" '="">Kwee</option><option value="269" title="Nama Lokal: kuwe
 Nama Lokal Daerah: kuwe
 Nama Indonesia: Kwee
 Nama Internasional: Tille trevally
 Nama Latin: Caranx tile (Cuvier, 1833)" '="">Kwee</option><option value="271" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang biru
 Nama Internasional: Mackerel scad
 Nama Latin: Decapterus macarellus (Cuvier, 1833)" '="">Layang biru</option><option value="38" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang deles
 Nama Internasional: Shortfin scad
 Nama Latin: Decapterus macrosoma (Bleeker, 1855)" '="">Layang deles</option><option value="31" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang/Benggol
 Nama Internasional: Indian scad
 Nama Latin: Decapterus. Ruselli (R�ppell, 1830)" '="">Layang/Benggol</option><option value="62" title="Nama Lokal: LAYUR
 Nama Lokal Daerah: CUALAI
 Nama Indonesia: Layur
 Nama Internasional: Hairtails
 Nama Latin: Trichiurus spp" '="">Layur</option><option value="42" title="Nama Lokal: LEMURU
 Nama Lokal Daerah: LEMURU
 Nama Indonesia: Lemuru
 Nama Internasional: Bali sardinella
 Nama Latin: Sardinella lemuru (Bleeker, 1853)" '="">Lemuru</option><option value="54" title="Nama Lokal: MANYUNG
 Nama Lokal Daerah: MANYUNG
 Nama Indonesia: Manyung
 Nama Internasional: Giant catfish
 Nama Latin: Arius thalassinus (R�ppell, 1837)" '="">Manyung</option><option value="133" title="Nama Lokal: LAYARAN
 Nama Lokal Daerah: LAYARAN
 Nama Indonesia: Moris layaran
 Nama Internasional: Horned coralfish
 Nama Latin: Heniochus permutatus" '="">Moris layaran</option><option value="80" title="Nama Lokal: Pari
 Nama Lokal Daerah: Pari
 Nama Indonesia: Pari burung
 Nama Internasional: Eaglerays
 Nama Latin: Myliobatus spp, Aetobatus spp. (Euphrasen, 1790) Aetomylaeus spp" '="">Pari burung</option><option value="285" title="Nama Lokal: pari
 Nama Lokal Daerah: pari
 Nama Indonesia: Pari kelelawar
 Nama Internasional: Devilrays, Mantarays
 Nama Latin: Mobula spp" '="">Pari kelelawar</option><option value="78" title="Nama Lokal: PARI
 Nama Lokal Daerah: PARI
 Nama Indonesia: Pari kembang/Pari macan
 Nama Internasional: Stingrays
 Nama Latin: Dasyatis spp" '="">Pari kembang/Pari macan</option><option value="66" title="Nama Lokal: PISANG-PISANG
 Nama Lokal Daerah: PISANG-PISANG
 Nama Indonesia: Peperek
 Nama Internasional: Slipmouths or Pony fishes
 Nama Latin: Leiognathus spp" '="">Peperek</option><option value="204" title="Nama Lokal: RAJUNGAN
 Nama Lokal Daerah: RAJUNGAN
 Nama Indonesia: Rajungan
 Nama Internasional: Swimming Crabs
 Nama Latin: Portunus pelagicus (Linnaeus, 1766)" '="">Rajungan</option><option value="262" title="Nama Lokal: SELAR
 Nama Lokal Daerah: SELAR
 Nama Indonesia: Selar kuning
 Nama Internasional: Black banded trevally
 Nama Latin: Seriolina nigrofasciata (R�ppell, 1829)" '="">Selar kuning</option><option value="263" title="Nama Lokal: selar
 Nama Lokal Daerah: selar
 Nama Indonesia: Selar kuning
 Nama Internasional: White mouth jack
 Nama Latin: Uraspis uraspis" '="">Selar kuning</option><option value="174" title="Nama Lokal: SEMBILANG
 Nama Lokal Daerah: SEMBILANG
 Nama Indonesia: Sembilang karang
 Nama Internasional: Striped catfish-eel
 Nama Latin: Plotosus lineatus" '="">Sembilang karang</option><option value="226" title="Nama Lokal: SOTONG
 Nama Lokal Daerah: SOTONG
 Nama Indonesia: Sotong
 Nama Internasional: Cuttle fish
 Nama Latin: Sepia Spp" '="">Sotong</option><option value="60" title="Nama Lokal: SWANGGI
 Nama Lokal Daerah: SWANGGI
 Nama Indonesia: Swanggi
 Nama Internasional: Purple-spotted bigeye
 Nama Latin: Priacanthus tayenus (Richardson, 1846)" '="">Swanggi</option><option value="37" title="Nama Lokal: TALANG-TALANG
 Nama Lokal Daerah: TALANG-TALANG
 Nama Indonesia: Talang-talang
 Nama Internasional: Talang queenfish
 Nama Latin: Scomberoides commersonnianus (Lacep�de, 1802)" '="">Talang-talang</option><option value="270" title="Nama Lokal: Talang-talang
 Nama Lokal Daerah: Talang-talang
 Nama Indonesia: Talang-talang
 Nama Internasional: Needlescale queenfish
 Nama Latin: Scomberoides tol (Cuvier, 1832)" '="">Talang-talang</option><option value="41" title="Nama Lokal: TAMBAN
 Nama Lokal Daerah: TAMBAN
 Nama Indonesia: Tembang
 Nama Internasional: Goldstripe sardinella
 Nama Latin: Sardinella gibbosa (Bleeker, 1849)" '="">Tembang</option><option value="273" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Deepbody sardinella
 Nama Latin: Sardinella brachysoma (Bleeker, 1852)" '="">Tembang</option><option value="274" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Fringescale sardinella
 Nama Latin: Sardinella fimbriata (Valenciennes, 1847)" '="">Tembang</option><option value="16" title="Nama Lokal: TENGGIRI
 Nama Lokal Daerah: TENGGIRI
 Nama Indonesia: Tenggiri
 Nama Internasional: Narrow-barred Spanish mackerel
 Nama Latin: Scomberomorus commerson (Lacep�de, 1800)" '="">Tenggiri</option><option value="17" title="Nama Lokal: tenggiri
 Nama Lokal Daerah: tenggiri
 Nama Indonesia: Tenggiri papan
 Nama Internasional: Indo-Pacific king mackerel
 Nama Latin: Scomberomorus guttatus (Bloch &amp; Schneider, 1801)" '="">Tenggiri papan</option><option value="39" title="Nama Lokal: TERI
 Nama Lokal Daerah: TERI
 Nama Indonesia: Teri
 Nama Internasional: Anchovies
 Nama Latin: Stolephorus spp." '="">Teri</option><option value="44" title="Nama Lokal: terubuk
 Nama Lokal Daerah: terubuk
 Nama Indonesia: Terubuk
 Nama Internasional: Hilsha shad
 Nama Latin: Tenualosa ilisha (Hamilton-Buchanan,1822)" '="">Terubuk</option><option value="35" title="Nama Lokal: TETENGKEK
 Nama Lokal Daerah: TETENGKEK
 Nama Indonesia: Tetengkek
 Nama Internasional: Torpedo scad
 Nama Latin: Megalaspis cordyla (Linnaeus, 1758)" '="">Tetengkek</option><option value="260" title="Nama Lokal: TONGKOL
 Nama Lokal Daerah: TONGKOL
 Nama Indonesia: Tongkol abu-abu
 Nama Internasional: Longtail tuna
 Nama Latin: Thunnus tonggol (Bleeker, 1851)" '="">Tongkol abu-abu</option><option value="302" title="Nama Lokal: UDANG DOGOL
 Nama Lokal Daerah: UDANG DOGOL
 Nama Indonesia: Udang  Dogol
 Nama Internasional: Endeavour prawn/ Shrimp red greasiback
 Nama Latin: Metapenaeus ensis, (de Haan, 1850), Metapenaeus monoceros, (Fabricius, 1798)" '="">Udang  Dogol</option><option value="301" title="Nama Lokal: udang windu
 Nama Lokal Daerah: udang windu
 Nama Indonesia: Udang  Windu
 Nama Internasional: Tiger prawn/ Brown tiger prawn
 Nama Latin: Penaeus esculentus (Haswell, 1879)" '="">Udang  Windu</option><option value="184" title="Nama Lokal: udang putih
 Nama Lokal Daerah: udang putih
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn/ White shrimp
 Nama Latin: Penaeus merguiensis (de Man, 1888 )" '="">Udang Jerbung/Udang putih</option><option value="187" title="Nama Lokal: udang jerubung
 Nama Lokal Daerah: udang jerubung
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn, Indian banana
 Nama Latin: Penaeus indicus (Milne Edwards, 1837)" '="">Udang Jerbung/Udang putih</option><option value="290" title="Nama Lokal: UDANG LAINNYA
 Nama Lokal Daerah: UDANG LAINNYA
 Nama Indonesia: Udang Lainnya
 Nama Internasional: Other shrimps
 Nama Latin: " '="">Udang Lainnya</option><option value="183" title="Nama Lokal: UDANG WINDU
 Nama Lokal Daerah: UDANG WINDU
 Nama Indonesia: Udang windu
 Nama Internasional: Jumbo tiger prawn/ shrimp, Giant tiger prawn/shrimp, Blue tiger prawn
 Nama Latin: Penaeus monodon (Fabricius, 1798)" '="">Udang windu</option></select></td><td><select name="id_alat_tangkap_2"><option value="1" title="Nama Internasional: Double rigs shrimp trawl">Pukat tarik udang ganda</option><option value="2" title="Nama Internasional: Otter Trawl">Otter Trawl</option><option value="3" title="Nama Internasional: Fish net">Pukat tarik ikan</option><option value="4" title="Nama Internasional: Pelagic danish seine">Payang (tmsk. Lampara)</option><option value="5" title="Nama Internasional: Demersal danish seine">Dogol (tmsk. Lampara dasar, cantrang)</option><option value="6" title="Nama Internasional: Beach seine">Pukat Pantai (Jaring arad)</option><option value="26" title="Nama Internasional: Other traps">Perangkap lainnya</option><option value="7" title="Nama Internasional: Purse seine">Pukat Cincin</option><option value="8" title="Nama Internasional: Drift gill nets">Jaring insang hanyut</option><option value="9" title="Nama Internasional: Encircling gill nets">Jaring insang lingkar</option><option value="10" title="Nama Internasional: Shrimp entangling gill net">Jaring klitik</option><option value="11" title="Nama Internasional: Set gill nets">Jaring insang tetap</option><option value="12" title="Nama Internasional: Trammel net">Jaring tiga lapis</option><option value="13" title="Nama Internasional: Boat/raft lift net">Bagan perahu/rakit</option><option value="14" title="Nama Internasional: Stationary lift net">Bagan tancap</option><option value="15" title="Nama Internasional: Scoop nets">Serok dan songko</option><option value="16" title="Nama Internasional: Other lift nets">Jaring Angkat Lainnya</option><option value="17" title="Nama Internasional: Tuna long line">Rawai tuna</option><option value="18" title="Nama Internasional: Other drift long line">Rawai hanyut lainnya selain rawai tuna</option><option value="19" title="Nama Internasional: Set long line">Rawai tetap</option><option value="20" title="Nama Internasional: Skipjack pole and line">Huhate</option><option value="21" title="Nama Internasional: Trowl lines">Pancing tonda</option><option value="22" title="Nama Internasional: Hand lines">Pancing ulur</option><option value="23" title="Nama Internasional: Guiding barrier">Sero (tmsk. Kelong)</option><option value="24" title="Nama Internasional: Stow net">Jermal</option><option value="25" title="Nama Internasional: Portable trap">Bubu (tmsk. Bubu ambai)</option><option value="27" title="Nama Internasional: Shell fish gears">Alat penangkap kerang</option><option value="28" title="Nama Internasional: Sea weed collectors">Alat pengumpul rumput laut</option><option value="29" title="Nama Internasional: Muroami">Muroami</option><option value="30" title="Nama Internasional: Cast net">Jala tebar</option><option value="31" title="Nama Internasional: ">Tidak Ada Alat Tangkap</option><option value="32" title="Nama Internasional: Beam trawl">Pukat tarik berbingkai</option><option value="33" title="Nama Internasional: Shore lift net">Anco</option><option value="34" title="Nama Internasional: Set bottom long line">Rawai dasar tetap</option><option value="35" title="Nama Internasional: Vertical line (incl. Vertical long line)">Pancing tegak</option><option value="36" title="Nama Internasional: Squid Jigger">Pancing cumi</option><option value="37" title="Nama Internasional: Other lines">Pancing lainnya</option><option value="38" title="Nama Internasional: Sea cucumber gears">Alat penangkap teripang (ladung)</option><option value="39" title="Nama Internasional: Crab gears">Alat penangkap kepiting</option><option value="40" title="Nama Internasional: Harpoon">Garpu dan Tombak</option><option value="41" title="Nama Internasional: Stern shrimp trawl">Pukat tarik udang tunggal</option><option value="42" title="Nama Internasional: ">Pukat cincin satu kapal</option><option value="43" title="Nama Internasional: ">Pukat Cincin Pelagis Besar dengan satu kapal</option><option value="44" title="Nama Internasional: ">Jaring lingkar tanpa tali kerut/Lampara</option><option value="45" title="Nama Internasional: ">Pukat hela dasar berpalang</option><option value="46" title="Nama Internasional: ">Pukat hela dasar berpapan</option><option value="47" title="Nama Internasional: ">Pukat hela dasar dua kapal</option><option value="48" title="Nama Internasional: ">Pukat hela dasar lainnya</option><option value="49" title="Nama Internasional: ">Pukat hela dasar udang</option><option value="50" title="Nama Internasional: ">Pukat hela pertengahan berpapan</option><option value="51" title="Nama Internasional: ">Puka hela pertengahan dua kapal</option><option value="52" title="Nama Internasional: ">Pukat hela pertengahan udang</option><option value="53" title="Nama Internasional: ">Pukat hela kembar berpapan</option><option value="54" title="Nama Internasional: ">Pukat hela lainnya</option><option value="55" title="Nama Internasional: ">Pukat dorong</option><option value="56" title="Nama Internasional: ">Boke Ami</option><option value="57" title="Nama Internasional: ">Jala jatuh berkapal</option><option value="58" title="Nama Internasional: ">Jala tebar lainnya</option><option value="59" title="Nama Internasional: ">Jaring liong bun</option><option value="60" title="Nama Internasional: ">Jaring insang oseanik</option><option value="61" title="Nama Internasional: tramel net">Jaring insang berlapis</option><option value="62" title="Nama Internasional: ">Jaring insang kombinasi dengan tramel net</option><option value="63" title="Nama Internasional: ">Jaring insang lainnya</option><option value="64" title="Nama Internasional: ">Set Net</option><option value="65" title="Nama Internasional: ">Bubu bersayap</option><option value="66" title="Nama Internasional: Long bag set net">Pukat labuh</option><option value="67" title="Nama Internasional: ">Togo</option><option value="68" title="Nama Internasional: ">Ambai</option><option value="69" title="Nama Internasional: ">Pengerih</option><option value="70" title="Nama Internasional: ">Perangkap ikan peloncat</option><option value="71" title="Nama Internasional: ">Seser</option><option value="72" title="Nama Internasional: ">Pancing berjoran</option><option value="73" title="Nama Internasional: ">Squid angling</option><option value="74" title="Nama Internasional: ">Huhate Mekanis</option><option value="75" title="Nama Internasional: ">Pancing layang-layang</option><option value="76" title="Nama Internasional: ">Ladung</option><option value="77" title="Nama Internasional: ">Panah</option><option value="78" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="79" title="Nama Internasional: ">Ladung</option><option value="80" title="Nama Internasional: ">Panah</option><option value="81" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="82" title="Nama Internasional: tes">Alat tangkap tes</option><option value="83" title="Nama Internasional: ">Pengangkut Ikan</option></select></td><td><input type="text" name="koef_koreksi_2" value="0" style="width: 180px;" title="Data yang di masukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td><select name="id_species_3"><option value="0">Kosong</option><option value="3" title="Nama Lokal: AJI-AJI
 Nama Lokal Daerah: AJI-AJI
 Nama Indonesia: Albakora
 Nama Internasional: Albacore
 Nama Latin: Thunnus alalunga (Bonnaterre, 1788)" '="">Albakora</option><option value="51" title="Nama Lokal: ALU-ALU
 Nama Lokal Daerah: ALU-ALU
 Nama Indonesia: Alu-alu/ Manggilala/Pucul
 Nama Internasional: Great barracuda
 Nama Latin: Sphyraena barracuda (Walbaum, 1792)" '="">Alu-alu/ Manggilala/Pucul</option><option value="46" title="Nama Lokal: CINCARO
 Nama Lokal Daerah: CINCARO
 Nama Indonesia: Banyar/Kembung Lelaki
 Nama Internasional: Indian mackerel
 Nama Latin: Rastrelliger kanagurta" '="">Banyar/Kembung Lelaki</option><option value="57" title="Nama Lokal: BAWAL HITAM
 Nama Lokal Daerah: BAWAL HITAM
 Nama Indonesia: Bawal hitam
 Nama Internasional: Black pomfret
 Nama Latin: Parastromateus niger (Bloch, 1795)" '="">Bawal hitam</option><option value="58" title="Nama Lokal: BAWAL PUTIH
 Nama Lokal Daerah: BAWAL PUTIH
 Nama Indonesia: Bawal putih
 Nama Internasional: Silver pomfret
 Nama Latin: Pampus argenteus (Euphrasen, 1788)" '="">Bawal putih</option><option value="73" title="Nama Lokal: BELANAK
 Nama Lokal Daerah: BELANAK
 Nama Indonesia: Belanak
 Nama Internasional: Blue-spot mullet/ Blue-tail mullet
 Nama Latin: Valamugil seheli (Forssk�l, 1775)" '="">Belanak</option><option value="299" title="Nama Lokal: belanak
 Nama Lokal Daerah: belanak
 Nama Indonesia: Belanak
 Nama Internasional: Mangrove mullets
 Nama Latin: Mugil cephalus (Linnaeus, 1758)" '="">Belanak</option><option value="67" title="Nama Lokal: beloso
 Nama Lokal Daerah: beloso
 Nama Indonesia: Beloso/Buntut kerbo
 Nama Internasional: Greater lizardfish
 Nama Latin: Saurida tumbil (Bloch, 1795)" '="">Beloso/Buntut kerbo</option><option value="264" title="Nama Lokal: MATA BESAR
 Nama Lokal Daerah: MATA BESAR
 Nama Indonesia: Bentong
 Nama Internasional: Oxeye scad
 Nama Latin: Selar boops (Valenciennes, 1833)" '="">Bentong</option><option value="279" title="Nama Lokal: BIJI NANGKA
 Nama Lokal Daerah: BIJI NANGKA
 Nama Indonesia: Biji nangka
 Nama Internasional: Yellow-stripe goatfish
 Nama Latin: Upeneus vittatus (Forssk�l, 1775)" '="">Biji nangka</option><option value="179" title="Nama Lokal: Bijinangka
 Nama Lokal Daerah: Bijinangka
 Nama Indonesia: Bijinangka/Kuniran
 Nama Internasional: Goatfish
 Nama Latin: Upeneus moluccensis" '="">Bijinangka/Kuniran</option><option value="7" title="Nama Lokal: Tongkol
 Nama Lokal Daerah: Tongkol
 Nama Indonesia: Cakalang
 Nama Internasional: Skipjack tuna
 Nama Latin: Katsuwonus pelamis (Linnaeus, 1758)" '="">Cakalang</option><option value="18" title="Nama Lokal: CUCUT
 Nama Lokal Daerah: CUCUT
 Nama Indonesia: Cucut botol
 Nama Internasional: Longnose velvet dogfish
 Nama Latin: Centrocymnus crepidater" '="">Cucut botol</option><option value="225" title="Nama Lokal: CUMI-CUMI
 Nama Lokal Daerah: CUMI-CUMI
 Nama Indonesia: Cumi-cumi
 Nama Internasional: Common squids
 Nama Latin: Loligo spp" '="">Cumi-cumi</option><option value="96" title="Nama Lokal: EKOR KUNING
 Nama Lokal Daerah: EKOR KUNING
 Nama Indonesia: Ekor kuning
 Nama Internasional: Redbelly yellowtail fusilier
 Nama Latin: Caesio cuning (Bloch, 1791)" '="">Ekor kuning</option><option value="69" title="Nama Lokal: Gerot
 Nama Lokal Daerah: Gerot
 Nama Indonesia: Gerot-gerot
 Nama Internasional: Saddle grunt/ Spotted javelinfish
 Nama Latin: Pomadasys maculatum(Bloch, 1797)" '="">Gerot-gerot</option><option value="48" title="Nama Lokal: Golok-golok
 Nama Lokal Daerah: Golok-golok
 Nama Indonesia: Golok-golok
 Nama Internasional: Dorab wolf-herring
 Nama Latin: Chirocentrus dorab (Forssk�l, 1775)" '="">Golok-golok</option><option value="61" title="Nama Lokal: GULAMAH
 Nama Lokal Daerah: GULAMAH
 Nama Indonesia: Gulamah/Tigawaja
 Nama Internasional: Croackers
 Nama Latin: Nibea albiflora (Ricahardson, 1846)" '="">Gulamah/Tigawaja</option><option value="94" title="Nama Lokal: Baronang
 Nama Lokal Daerah: Baronang
 Nama Indonesia: Ikan beronang
 Nama Internasional: Orange-spotted spinefoot
 Nama Latin: Siganus guttatus (Bloch, 1787)" '="">Ikan beronang</option><option value="89" title="Nama Lokal: KAKAP MERAH
 Nama Lokal Daerah: KAKAP MERAH
 Nama Indonesia: Ikan kakap merah/Bambangan
 Nama Internasional: Red snappers
 Nama Latin: Lutjanus spp" '="">Ikan kakap merah/Bambangan</option><option value="72" title="Nama Lokal: LIDAH
 Nama Lokal Daerah: LIDAH
 Nama Indonesia: Ikan lidah
 Nama Internasional: Tongue soles
 Nama Latin: Cynoglossus spp, Pleuronectus spp." '="">Ikan lidah</option><option value="64" title="Nama Lokal: IKAN SEBELAH
 Nama Lokal Daerah: IKAN SEBELAH
 Nama Indonesia: Ikan sebelah (Terompa)
 Nama Internasional: Indian halibut/ Queensland halibut
 Nama Latin: Psettodes erumei (Schneider, 1801)" '="">Ikan sebelah (Terompa)</option><option value="90" title="Nama Lokal: KAKAP
 Nama Lokal Daerah: kakap
 Nama Indonesia: Kakap putih
 Nama Internasional: Barramundi, Giant sea perch
 Nama Latin: Lates calcarifer (Bloch)" '="">Kakap putih</option><option value="294" title="Nama Lokal: KAPAS-KAPAS
 Nama Lokal Daerah: KAPAS-KAPAS
 Nama Indonesia: Kapas-kapas
 Nama Internasional: Fals trevally
 Nama Latin: Lactarius lactarius (Bloch &amp; Schneider, 1801)" '="">Kapas-kapas</option><option value="276" title="Nama Lokal: KEMBUNG
 Nama Lokal Daerah: KEMBUNG
 Nama Indonesia: Kembung Perempuan
 Nama Internasional: Short-bodied mackerel
 Nama Latin: Rastrelliger brachysoma" '="">Kembung Perempuan</option><option value="296" title="Nama Lokal: pasir-pasir
 Nama Lokal Daerah: pasir-pasir
 Nama Indonesia: Kempar Pati
 Nama Internasional: Sharptooth jobfish
 Nama Latin: Pristipomoides typus (Bleeker, 1852)" '="">Kempar Pati</option><option value="87" title="Nama Lokal: Kerapu
 Nama Lokal Daerah: kerapu
 Nama Indonesia: Kerapu balong
 Nama Internasional: Honeycomb grouper
 Nama Latin: Epinephelus merra (Bloch, 1793)" '="">Kerapu balong</option><option value="286" title="Nama Lokal: KERAPU
 Nama Lokal Daerah: KERAPU
 Nama Indonesia: Kerapu karang
 Nama Internasional: Blue-lined seabass
 Nama Latin: Cephalophodis boenack (Bloch, 1790)" '="">Kerapu karang</option><option value="74" title="Nama Lokal: Kerong-kerong
 Nama Lokal Daerah: Kerong-kerong
 Nama Indonesia: Kerong-kerong
 Nama Internasional: Largescale terapon
 Nama Latin: Terapon theraps (Cuvier, 1829)" '="">Kerong-kerong</option><option value="70" title="Nama Lokal: KURISI
 Nama Lokal Daerah: KURISI
 Nama Indonesia: Kurisi
 Nama Internasional: Threadfin bream
 Nama Latin: Nemimterus spp" '="">Kurisi</option><option value="278" title="Nama Lokal: SENANGIN
 Nama Lokal Daerah: SENANGIN
 Nama Indonesia: Kuro/senangin
 Nama Internasional: Threadfins
 Nama Latin: Polynemus spp" '="">Kuro/senangin</option><option value="34" title="Nama Lokal: KUWE
 Nama Lokal Daerah: KUWE
 Nama Indonesia: Kwee
 Nama Internasional: Bigeye trevally
 Nama Latin: Caranx sexfaciatus (Quoy &amp; Gaimard,1825)" '="">Kwee</option><option value="269" title="Nama Lokal: kuwe
 Nama Lokal Daerah: kuwe
 Nama Indonesia: Kwee
 Nama Internasional: Tille trevally
 Nama Latin: Caranx tile (Cuvier, 1833)" '="">Kwee</option><option value="271" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang biru
 Nama Internasional: Mackerel scad
 Nama Latin: Decapterus macarellus (Cuvier, 1833)" '="">Layang biru</option><option value="38" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang deles
 Nama Internasional: Shortfin scad
 Nama Latin: Decapterus macrosoma (Bleeker, 1855)" '="">Layang deles</option><option value="31" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang/Benggol
 Nama Internasional: Indian scad
 Nama Latin: Decapterus. Ruselli (R�ppell, 1830)" '="">Layang/Benggol</option><option value="62" title="Nama Lokal: LAYUR
 Nama Lokal Daerah: CUALAI
 Nama Indonesia: Layur
 Nama Internasional: Hairtails
 Nama Latin: Trichiurus spp" '="">Layur</option><option value="42" title="Nama Lokal: LEMURU
 Nama Lokal Daerah: LEMURU
 Nama Indonesia: Lemuru
 Nama Internasional: Bali sardinella
 Nama Latin: Sardinella lemuru (Bleeker, 1853)" '="">Lemuru</option><option value="54" title="Nama Lokal: MANYUNG
 Nama Lokal Daerah: MANYUNG
 Nama Indonesia: Manyung
 Nama Internasional: Giant catfish
 Nama Latin: Arius thalassinus (R�ppell, 1837)" '="">Manyung</option><option value="133" title="Nama Lokal: LAYARAN
 Nama Lokal Daerah: LAYARAN
 Nama Indonesia: Moris layaran
 Nama Internasional: Horned coralfish
 Nama Latin: Heniochus permutatus" '="">Moris layaran</option><option value="80" title="Nama Lokal: Pari
 Nama Lokal Daerah: Pari
 Nama Indonesia: Pari burung
 Nama Internasional: Eaglerays
 Nama Latin: Myliobatus spp, Aetobatus spp. (Euphrasen, 1790) Aetomylaeus spp" '="">Pari burung</option><option value="285" title="Nama Lokal: pari
 Nama Lokal Daerah: pari
 Nama Indonesia: Pari kelelawar
 Nama Internasional: Devilrays, Mantarays
 Nama Latin: Mobula spp" '="">Pari kelelawar</option><option value="78" title="Nama Lokal: PARI
 Nama Lokal Daerah: PARI
 Nama Indonesia: Pari kembang/Pari macan
 Nama Internasional: Stingrays
 Nama Latin: Dasyatis spp" '="">Pari kembang/Pari macan</option><option value="66" title="Nama Lokal: PISANG-PISANG
 Nama Lokal Daerah: PISANG-PISANG
 Nama Indonesia: Peperek
 Nama Internasional: Slipmouths or Pony fishes
 Nama Latin: Leiognathus spp" '="">Peperek</option><option value="204" title="Nama Lokal: RAJUNGAN
 Nama Lokal Daerah: RAJUNGAN
 Nama Indonesia: Rajungan
 Nama Internasional: Swimming Crabs
 Nama Latin: Portunus pelagicus (Linnaeus, 1766)" '="">Rajungan</option><option value="262" title="Nama Lokal: SELAR
 Nama Lokal Daerah: SELAR
 Nama Indonesia: Selar kuning
 Nama Internasional: Black banded trevally
 Nama Latin: Seriolina nigrofasciata (R�ppell, 1829)" '="">Selar kuning</option><option value="263" title="Nama Lokal: selar
 Nama Lokal Daerah: selar
 Nama Indonesia: Selar kuning
 Nama Internasional: White mouth jack
 Nama Latin: Uraspis uraspis" '="">Selar kuning</option><option value="174" title="Nama Lokal: SEMBILANG
 Nama Lokal Daerah: SEMBILANG
 Nama Indonesia: Sembilang karang
 Nama Internasional: Striped catfish-eel
 Nama Latin: Plotosus lineatus" '="">Sembilang karang</option><option value="226" title="Nama Lokal: SOTONG
 Nama Lokal Daerah: SOTONG
 Nama Indonesia: Sotong
 Nama Internasional: Cuttle fish
 Nama Latin: Sepia Spp" '="">Sotong</option><option value="60" title="Nama Lokal: SWANGGI
 Nama Lokal Daerah: SWANGGI
 Nama Indonesia: Swanggi
 Nama Internasional: Purple-spotted bigeye
 Nama Latin: Priacanthus tayenus (Richardson, 1846)" '="">Swanggi</option><option value="37" title="Nama Lokal: TALANG-TALANG
 Nama Lokal Daerah: TALANG-TALANG
 Nama Indonesia: Talang-talang
 Nama Internasional: Talang queenfish
 Nama Latin: Scomberoides commersonnianus (Lacep�de, 1802)" '="">Talang-talang</option><option value="270" title="Nama Lokal: Talang-talang
 Nama Lokal Daerah: Talang-talang
 Nama Indonesia: Talang-talang
 Nama Internasional: Needlescale queenfish
 Nama Latin: Scomberoides tol (Cuvier, 1832)" '="">Talang-talang</option><option value="41" title="Nama Lokal: TAMBAN
 Nama Lokal Daerah: TAMBAN
 Nama Indonesia: Tembang
 Nama Internasional: Goldstripe sardinella
 Nama Latin: Sardinella gibbosa (Bleeker, 1849)" '="">Tembang</option><option value="273" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Deepbody sardinella
 Nama Latin: Sardinella brachysoma (Bleeker, 1852)" '="">Tembang</option><option value="274" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Fringescale sardinella
 Nama Latin: Sardinella fimbriata (Valenciennes, 1847)" '="">Tembang</option><option value="16" title="Nama Lokal: TENGGIRI
 Nama Lokal Daerah: TENGGIRI
 Nama Indonesia: Tenggiri
 Nama Internasional: Narrow-barred Spanish mackerel
 Nama Latin: Scomberomorus commerson (Lacep�de, 1800)" '="">Tenggiri</option><option value="17" title="Nama Lokal: tenggiri
 Nama Lokal Daerah: tenggiri
 Nama Indonesia: Tenggiri papan
 Nama Internasional: Indo-Pacific king mackerel
 Nama Latin: Scomberomorus guttatus (Bloch &amp; Schneider, 1801)" '="">Tenggiri papan</option><option value="39" title="Nama Lokal: TERI
 Nama Lokal Daerah: TERI
 Nama Indonesia: Teri
 Nama Internasional: Anchovies
 Nama Latin: Stolephorus spp." '="">Teri</option><option value="44" title="Nama Lokal: terubuk
 Nama Lokal Daerah: terubuk
 Nama Indonesia: Terubuk
 Nama Internasional: Hilsha shad
 Nama Latin: Tenualosa ilisha (Hamilton-Buchanan,1822)" '="">Terubuk</option><option value="35" title="Nama Lokal: TETENGKEK
 Nama Lokal Daerah: TETENGKEK
 Nama Indonesia: Tetengkek
 Nama Internasional: Torpedo scad
 Nama Latin: Megalaspis cordyla (Linnaeus, 1758)" '="">Tetengkek</option><option value="260" title="Nama Lokal: TONGKOL
 Nama Lokal Daerah: TONGKOL
 Nama Indonesia: Tongkol abu-abu
 Nama Internasional: Longtail tuna
 Nama Latin: Thunnus tonggol (Bleeker, 1851)" '="">Tongkol abu-abu</option><option value="302" title="Nama Lokal: UDANG DOGOL
 Nama Lokal Daerah: UDANG DOGOL
 Nama Indonesia: Udang  Dogol
 Nama Internasional: Endeavour prawn/ Shrimp red greasiback
 Nama Latin: Metapenaeus ensis, (de Haan, 1850), Metapenaeus monoceros, (Fabricius, 1798)" '="">Udang  Dogol</option><option value="301" title="Nama Lokal: udang windu
 Nama Lokal Daerah: udang windu
 Nama Indonesia: Udang  Windu
 Nama Internasional: Tiger prawn/ Brown tiger prawn
 Nama Latin: Penaeus esculentus (Haswell, 1879)" '="">Udang  Windu</option><option value="184" title="Nama Lokal: udang putih
 Nama Lokal Daerah: udang putih
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn/ White shrimp
 Nama Latin: Penaeus merguiensis (de Man, 1888 )" '="">Udang Jerbung/Udang putih</option><option value="187" title="Nama Lokal: udang jerubung
 Nama Lokal Daerah: udang jerubung
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn, Indian banana
 Nama Latin: Penaeus indicus (Milne Edwards, 1837)" '="">Udang Jerbung/Udang putih</option><option value="290" title="Nama Lokal: UDANG LAINNYA
 Nama Lokal Daerah: UDANG LAINNYA
 Nama Indonesia: Udang Lainnya
 Nama Internasional: Other shrimps
 Nama Latin: " '="">Udang Lainnya</option><option value="183" title="Nama Lokal: UDANG WINDU
 Nama Lokal Daerah: UDANG WINDU
 Nama Indonesia: Udang windu
 Nama Internasional: Jumbo tiger prawn/ shrimp, Giant tiger prawn/shrimp, Blue tiger prawn
 Nama Latin: Penaeus monodon (Fabricius, 1798)" '="">Udang windu</option></select></td><td><select name="id_alat_tangkap_3"><option value="1" title="Nama Internasional: Double rigs shrimp trawl">Pukat tarik udang ganda</option><option value="2" title="Nama Internasional: Otter Trawl">Otter Trawl</option><option value="3" title="Nama Internasional: Fish net">Pukat tarik ikan</option><option value="4" title="Nama Internasional: Pelagic danish seine">Payang (tmsk. Lampara)</option><option value="5" title="Nama Internasional: Demersal danish seine">Dogol (tmsk. Lampara dasar, cantrang)</option><option value="6" title="Nama Internasional: Beach seine">Pukat Pantai (Jaring arad)</option><option value="26" title="Nama Internasional: Other traps">Perangkap lainnya</option><option value="7" title="Nama Internasional: Purse seine">Pukat Cincin</option><option value="8" title="Nama Internasional: Drift gill nets">Jaring insang hanyut</option><option value="9" title="Nama Internasional: Encircling gill nets">Jaring insang lingkar</option><option value="10" title="Nama Internasional: Shrimp entangling gill net">Jaring klitik</option><option value="11" title="Nama Internasional: Set gill nets">Jaring insang tetap</option><option value="12" title="Nama Internasional: Trammel net">Jaring tiga lapis</option><option value="13" title="Nama Internasional: Boat/raft lift net">Bagan perahu/rakit</option><option value="14" title="Nama Internasional: Stationary lift net">Bagan tancap</option><option value="15" title="Nama Internasional: Scoop nets">Serok dan songko</option><option value="16" title="Nama Internasional: Other lift nets">Jaring Angkat Lainnya</option><option value="17" title="Nama Internasional: Tuna long line">Rawai tuna</option><option value="18" title="Nama Internasional: Other drift long line">Rawai hanyut lainnya selain rawai tuna</option><option value="19" title="Nama Internasional: Set long line">Rawai tetap</option><option value="20" title="Nama Internasional: Skipjack pole and line">Huhate</option><option value="21" title="Nama Internasional: Trowl lines">Pancing tonda</option><option value="22" title="Nama Internasional: Hand lines">Pancing ulur</option><option value="23" title="Nama Internasional: Guiding barrier">Sero (tmsk. Kelong)</option><option value="24" title="Nama Internasional: Stow net">Jermal</option><option value="25" title="Nama Internasional: Portable trap">Bubu (tmsk. Bubu ambai)</option><option value="27" title="Nama Internasional: Shell fish gears">Alat penangkap kerang</option><option value="28" title="Nama Internasional: Sea weed collectors">Alat pengumpul rumput laut</option><option value="29" title="Nama Internasional: Muroami">Muroami</option><option value="30" title="Nama Internasional: Cast net">Jala tebar</option><option value="31" title="Nama Internasional: ">Tidak Ada Alat Tangkap</option><option value="32" title="Nama Internasional: Beam trawl">Pukat tarik berbingkai</option><option value="33" title="Nama Internasional: Shore lift net">Anco</option><option value="34" title="Nama Internasional: Set bottom long line">Rawai dasar tetap</option><option value="35" title="Nama Internasional: Vertical line (incl. Vertical long line)">Pancing tegak</option><option value="36" title="Nama Internasional: Squid Jigger">Pancing cumi</option><option value="37" title="Nama Internasional: Other lines">Pancing lainnya</option><option value="38" title="Nama Internasional: Sea cucumber gears">Alat penangkap teripang (ladung)</option><option value="39" title="Nama Internasional: Crab gears">Alat penangkap kepiting</option><option value="40" title="Nama Internasional: Harpoon">Garpu dan Tombak</option><option value="41" title="Nama Internasional: Stern shrimp trawl">Pukat tarik udang tunggal</option><option value="42" title="Nama Internasional: ">Pukat cincin satu kapal</option><option value="43" title="Nama Internasional: ">Pukat Cincin Pelagis Besar dengan satu kapal</option><option value="44" title="Nama Internasional: ">Jaring lingkar tanpa tali kerut/Lampara</option><option value="45" title="Nama Internasional: ">Pukat hela dasar berpalang</option><option value="46" title="Nama Internasional: ">Pukat hela dasar berpapan</option><option value="47" title="Nama Internasional: ">Pukat hela dasar dua kapal</option><option value="48" title="Nama Internasional: ">Pukat hela dasar lainnya</option><option value="49" title="Nama Internasional: ">Pukat hela dasar udang</option><option value="50" title="Nama Internasional: ">Pukat hela pertengahan berpapan</option><option value="51" title="Nama Internasional: ">Puka hela pertengahan dua kapal</option><option value="52" title="Nama Internasional: ">Pukat hela pertengahan udang</option><option value="53" title="Nama Internasional: ">Pukat hela kembar berpapan</option><option value="54" title="Nama Internasional: ">Pukat hela lainnya</option><option value="55" title="Nama Internasional: ">Pukat dorong</option><option value="56" title="Nama Internasional: ">Boke Ami</option><option value="57" title="Nama Internasional: ">Jala jatuh berkapal</option><option value="58" title="Nama Internasional: ">Jala tebar lainnya</option><option value="59" title="Nama Internasional: ">Jaring liong bun</option><option value="60" title="Nama Internasional: ">Jaring insang oseanik</option><option value="61" title="Nama Internasional: tramel net">Jaring insang berlapis</option><option value="62" title="Nama Internasional: ">Jaring insang kombinasi dengan tramel net</option><option value="63" title="Nama Internasional: ">Jaring insang lainnya</option><option value="64" title="Nama Internasional: ">Set Net</option><option value="65" title="Nama Internasional: ">Bubu bersayap</option><option value="66" title="Nama Internasional: Long bag set net">Pukat labuh</option><option value="67" title="Nama Internasional: ">Togo</option><option value="68" title="Nama Internasional: ">Ambai</option><option value="69" title="Nama Internasional: ">Pengerih</option><option value="70" title="Nama Internasional: ">Perangkap ikan peloncat</option><option value="71" title="Nama Internasional: ">Seser</option><option value="72" title="Nama Internasional: ">Pancing berjoran</option><option value="73" title="Nama Internasional: ">Squid angling</option><option value="74" title="Nama Internasional: ">Huhate Mekanis</option><option value="75" title="Nama Internasional: ">Pancing layang-layang</option><option value="76" title="Nama Internasional: ">Ladung</option><option value="77" title="Nama Internasional: ">Panah</option><option value="78" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="79" title="Nama Internasional: ">Ladung</option><option value="80" title="Nama Internasional: ">Panah</option><option value="81" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="82" title="Nama Internasional: tes">Alat tangkap tes</option><option value="83" title="Nama Internasional: ">Pengangkut Ikan</option></select></td><td><input type="text" name="koef_koreksi_3" value="0" style="width: 180px;" title="Data yang di masukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td><select name="id_species_4"><option value="0">Kosong</option><option value="3" title="Nama Lokal: AJI-AJI
 Nama Lokal Daerah: AJI-AJI
 Nama Indonesia: Albakora
 Nama Internasional: Albacore
 Nama Latin: Thunnus alalunga (Bonnaterre, 1788)" '="">Albakora</option><option value="51" title="Nama Lokal: ALU-ALU
 Nama Lokal Daerah: ALU-ALU
 Nama Indonesia: Alu-alu/ Manggilala/Pucul
 Nama Internasional: Great barracuda
 Nama Latin: Sphyraena barracuda (Walbaum, 1792)" '="">Alu-alu/ Manggilala/Pucul</option><option value="46" title="Nama Lokal: CINCARO
 Nama Lokal Daerah: CINCARO
 Nama Indonesia: Banyar/Kembung Lelaki
 Nama Internasional: Indian mackerel
 Nama Latin: Rastrelliger kanagurta" '="">Banyar/Kembung Lelaki</option><option value="57" title="Nama Lokal: BAWAL HITAM
 Nama Lokal Daerah: BAWAL HITAM
 Nama Indonesia: Bawal hitam
 Nama Internasional: Black pomfret
 Nama Latin: Parastromateus niger (Bloch, 1795)" '="">Bawal hitam</option><option value="58" title="Nama Lokal: BAWAL PUTIH
 Nama Lokal Daerah: BAWAL PUTIH
 Nama Indonesia: Bawal putih
 Nama Internasional: Silver pomfret
 Nama Latin: Pampus argenteus (Euphrasen, 1788)" '="">Bawal putih</option><option value="73" title="Nama Lokal: BELANAK
 Nama Lokal Daerah: BELANAK
 Nama Indonesia: Belanak
 Nama Internasional: Blue-spot mullet/ Blue-tail mullet
 Nama Latin: Valamugil seheli (Forssk�l, 1775)" '="">Belanak</option><option value="299" title="Nama Lokal: belanak
 Nama Lokal Daerah: belanak
 Nama Indonesia: Belanak
 Nama Internasional: Mangrove mullets
 Nama Latin: Mugil cephalus (Linnaeus, 1758)" '="">Belanak</option><option value="67" title="Nama Lokal: beloso
 Nama Lokal Daerah: beloso
 Nama Indonesia: Beloso/Buntut kerbo
 Nama Internasional: Greater lizardfish
 Nama Latin: Saurida tumbil (Bloch, 1795)" '="">Beloso/Buntut kerbo</option><option value="264" title="Nama Lokal: MATA BESAR
 Nama Lokal Daerah: MATA BESAR
 Nama Indonesia: Bentong
 Nama Internasional: Oxeye scad
 Nama Latin: Selar boops (Valenciennes, 1833)" '="">Bentong</option><option value="279" title="Nama Lokal: BIJI NANGKA
 Nama Lokal Daerah: BIJI NANGKA
 Nama Indonesia: Biji nangka
 Nama Internasional: Yellow-stripe goatfish
 Nama Latin: Upeneus vittatus (Forssk�l, 1775)" '="">Biji nangka</option><option value="179" title="Nama Lokal: Bijinangka
 Nama Lokal Daerah: Bijinangka
 Nama Indonesia: Bijinangka/Kuniran
 Nama Internasional: Goatfish
 Nama Latin: Upeneus moluccensis" '="">Bijinangka/Kuniran</option><option value="7" title="Nama Lokal: Tongkol
 Nama Lokal Daerah: Tongkol
 Nama Indonesia: Cakalang
 Nama Internasional: Skipjack tuna
 Nama Latin: Katsuwonus pelamis (Linnaeus, 1758)" '="">Cakalang</option><option value="18" title="Nama Lokal: CUCUT
 Nama Lokal Daerah: CUCUT
 Nama Indonesia: Cucut botol
 Nama Internasional: Longnose velvet dogfish
 Nama Latin: Centrocymnus crepidater" '="">Cucut botol</option><option value="225" title="Nama Lokal: CUMI-CUMI
 Nama Lokal Daerah: CUMI-CUMI
 Nama Indonesia: Cumi-cumi
 Nama Internasional: Common squids
 Nama Latin: Loligo spp" '="">Cumi-cumi</option><option value="96" title="Nama Lokal: EKOR KUNING
 Nama Lokal Daerah: EKOR KUNING
 Nama Indonesia: Ekor kuning
 Nama Internasional: Redbelly yellowtail fusilier
 Nama Latin: Caesio cuning (Bloch, 1791)" '="">Ekor kuning</option><option value="69" title="Nama Lokal: Gerot
 Nama Lokal Daerah: Gerot
 Nama Indonesia: Gerot-gerot
 Nama Internasional: Saddle grunt/ Spotted javelinfish
 Nama Latin: Pomadasys maculatum(Bloch, 1797)" '="">Gerot-gerot</option><option value="48" title="Nama Lokal: Golok-golok
 Nama Lokal Daerah: Golok-golok
 Nama Indonesia: Golok-golok
 Nama Internasional: Dorab wolf-herring
 Nama Latin: Chirocentrus dorab (Forssk�l, 1775)" '="">Golok-golok</option><option value="61" title="Nama Lokal: GULAMAH
 Nama Lokal Daerah: GULAMAH
 Nama Indonesia: Gulamah/Tigawaja
 Nama Internasional: Croackers
 Nama Latin: Nibea albiflora (Ricahardson, 1846)" '="">Gulamah/Tigawaja</option><option value="94" title="Nama Lokal: Baronang
 Nama Lokal Daerah: Baronang
 Nama Indonesia: Ikan beronang
 Nama Internasional: Orange-spotted spinefoot
 Nama Latin: Siganus guttatus (Bloch, 1787)" '="">Ikan beronang</option><option value="89" title="Nama Lokal: KAKAP MERAH
 Nama Lokal Daerah: KAKAP MERAH
 Nama Indonesia: Ikan kakap merah/Bambangan
 Nama Internasional: Red snappers
 Nama Latin: Lutjanus spp" '="">Ikan kakap merah/Bambangan</option><option value="72" title="Nama Lokal: LIDAH
 Nama Lokal Daerah: LIDAH
 Nama Indonesia: Ikan lidah
 Nama Internasional: Tongue soles
 Nama Latin: Cynoglossus spp, Pleuronectus spp." '="">Ikan lidah</option><option value="64" title="Nama Lokal: IKAN SEBELAH
 Nama Lokal Daerah: IKAN SEBELAH
 Nama Indonesia: Ikan sebelah (Terompa)
 Nama Internasional: Indian halibut/ Queensland halibut
 Nama Latin: Psettodes erumei (Schneider, 1801)" '="">Ikan sebelah (Terompa)</option><option value="90" title="Nama Lokal: KAKAP
 Nama Lokal Daerah: kakap
 Nama Indonesia: Kakap putih
 Nama Internasional: Barramundi, Giant sea perch
 Nama Latin: Lates calcarifer (Bloch)" '="">Kakap putih</option><option value="294" title="Nama Lokal: KAPAS-KAPAS
 Nama Lokal Daerah: KAPAS-KAPAS
 Nama Indonesia: Kapas-kapas
 Nama Internasional: Fals trevally
 Nama Latin: Lactarius lactarius (Bloch &amp; Schneider, 1801)" '="">Kapas-kapas</option><option value="276" title="Nama Lokal: KEMBUNG
 Nama Lokal Daerah: KEMBUNG
 Nama Indonesia: Kembung Perempuan
 Nama Internasional: Short-bodied mackerel
 Nama Latin: Rastrelliger brachysoma" '="">Kembung Perempuan</option><option value="296" title="Nama Lokal: pasir-pasir
 Nama Lokal Daerah: pasir-pasir
 Nama Indonesia: Kempar Pati
 Nama Internasional: Sharptooth jobfish
 Nama Latin: Pristipomoides typus (Bleeker, 1852)" '="">Kempar Pati</option><option value="87" title="Nama Lokal: Kerapu
 Nama Lokal Daerah: kerapu
 Nama Indonesia: Kerapu balong
 Nama Internasional: Honeycomb grouper
 Nama Latin: Epinephelus merra (Bloch, 1793)" '="">Kerapu balong</option><option value="286" title="Nama Lokal: KERAPU
 Nama Lokal Daerah: KERAPU
 Nama Indonesia: Kerapu karang
 Nama Internasional: Blue-lined seabass
 Nama Latin: Cephalophodis boenack (Bloch, 1790)" '="">Kerapu karang</option><option value="74" title="Nama Lokal: Kerong-kerong
 Nama Lokal Daerah: Kerong-kerong
 Nama Indonesia: Kerong-kerong
 Nama Internasional: Largescale terapon
 Nama Latin: Terapon theraps (Cuvier, 1829)" '="">Kerong-kerong</option><option value="70" title="Nama Lokal: KURISI
 Nama Lokal Daerah: KURISI
 Nama Indonesia: Kurisi
 Nama Internasional: Threadfin bream
 Nama Latin: Nemimterus spp" '="">Kurisi</option><option value="278" title="Nama Lokal: SENANGIN
 Nama Lokal Daerah: SENANGIN
 Nama Indonesia: Kuro/senangin
 Nama Internasional: Threadfins
 Nama Latin: Polynemus spp" '="">Kuro/senangin</option><option value="34" title="Nama Lokal: KUWE
 Nama Lokal Daerah: KUWE
 Nama Indonesia: Kwee
 Nama Internasional: Bigeye trevally
 Nama Latin: Caranx sexfaciatus (Quoy &amp; Gaimard,1825)" '="">Kwee</option><option value="269" title="Nama Lokal: kuwe
 Nama Lokal Daerah: kuwe
 Nama Indonesia: Kwee
 Nama Internasional: Tille trevally
 Nama Latin: Caranx tile (Cuvier, 1833)" '="">Kwee</option><option value="271" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang biru
 Nama Internasional: Mackerel scad
 Nama Latin: Decapterus macarellus (Cuvier, 1833)" '="">Layang biru</option><option value="38" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang deles
 Nama Internasional: Shortfin scad
 Nama Latin: Decapterus macrosoma (Bleeker, 1855)" '="">Layang deles</option><option value="31" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang/Benggol
 Nama Internasional: Indian scad
 Nama Latin: Decapterus. Ruselli (R�ppell, 1830)" '="">Layang/Benggol</option><option value="62" title="Nama Lokal: LAYUR
 Nama Lokal Daerah: CUALAI
 Nama Indonesia: Layur
 Nama Internasional: Hairtails
 Nama Latin: Trichiurus spp" '="">Layur</option><option value="42" title="Nama Lokal: LEMURU
 Nama Lokal Daerah: LEMURU
 Nama Indonesia: Lemuru
 Nama Internasional: Bali sardinella
 Nama Latin: Sardinella lemuru (Bleeker, 1853)" '="">Lemuru</option><option value="54" title="Nama Lokal: MANYUNG
 Nama Lokal Daerah: MANYUNG
 Nama Indonesia: Manyung
 Nama Internasional: Giant catfish
 Nama Latin: Arius thalassinus (R�ppell, 1837)" '="">Manyung</option><option value="133" title="Nama Lokal: LAYARAN
 Nama Lokal Daerah: LAYARAN
 Nama Indonesia: Moris layaran
 Nama Internasional: Horned coralfish
 Nama Latin: Heniochus permutatus" '="">Moris layaran</option><option value="80" title="Nama Lokal: Pari
 Nama Lokal Daerah: Pari
 Nama Indonesia: Pari burung
 Nama Internasional: Eaglerays
 Nama Latin: Myliobatus spp, Aetobatus spp. (Euphrasen, 1790) Aetomylaeus spp" '="">Pari burung</option><option value="285" title="Nama Lokal: pari
 Nama Lokal Daerah: pari
 Nama Indonesia: Pari kelelawar
 Nama Internasional: Devilrays, Mantarays
 Nama Latin: Mobula spp" '="">Pari kelelawar</option><option value="78" title="Nama Lokal: PARI
 Nama Lokal Daerah: PARI
 Nama Indonesia: Pari kembang/Pari macan
 Nama Internasional: Stingrays
 Nama Latin: Dasyatis spp" '="">Pari kembang/Pari macan</option><option value="66" title="Nama Lokal: PISANG-PISANG
 Nama Lokal Daerah: PISANG-PISANG
 Nama Indonesia: Peperek
 Nama Internasional: Slipmouths or Pony fishes
 Nama Latin: Leiognathus spp" '="">Peperek</option><option value="204" title="Nama Lokal: RAJUNGAN
 Nama Lokal Daerah: RAJUNGAN
 Nama Indonesia: Rajungan
 Nama Internasional: Swimming Crabs
 Nama Latin: Portunus pelagicus (Linnaeus, 1766)" '="">Rajungan</option><option value="262" title="Nama Lokal: SELAR
 Nama Lokal Daerah: SELAR
 Nama Indonesia: Selar kuning
 Nama Internasional: Black banded trevally
 Nama Latin: Seriolina nigrofasciata (R�ppell, 1829)" '="">Selar kuning</option><option value="263" title="Nama Lokal: selar
 Nama Lokal Daerah: selar
 Nama Indonesia: Selar kuning
 Nama Internasional: White mouth jack
 Nama Latin: Uraspis uraspis" '="">Selar kuning</option><option value="174" title="Nama Lokal: SEMBILANG
 Nama Lokal Daerah: SEMBILANG
 Nama Indonesia: Sembilang karang
 Nama Internasional: Striped catfish-eel
 Nama Latin: Plotosus lineatus" '="">Sembilang karang</option><option value="226" title="Nama Lokal: SOTONG
 Nama Lokal Daerah: SOTONG
 Nama Indonesia: Sotong
 Nama Internasional: Cuttle fish
 Nama Latin: Sepia Spp" '="">Sotong</option><option value="60" title="Nama Lokal: SWANGGI
 Nama Lokal Daerah: SWANGGI
 Nama Indonesia: Swanggi
 Nama Internasional: Purple-spotted bigeye
 Nama Latin: Priacanthus tayenus (Richardson, 1846)" '="">Swanggi</option><option value="37" title="Nama Lokal: TALANG-TALANG
 Nama Lokal Daerah: TALANG-TALANG
 Nama Indonesia: Talang-talang
 Nama Internasional: Talang queenfish
 Nama Latin: Scomberoides commersonnianus (Lacep�de, 1802)" '="">Talang-talang</option><option value="270" title="Nama Lokal: Talang-talang
 Nama Lokal Daerah: Talang-talang
 Nama Indonesia: Talang-talang
 Nama Internasional: Needlescale queenfish
 Nama Latin: Scomberoides tol (Cuvier, 1832)" '="">Talang-talang</option><option value="41" title="Nama Lokal: TAMBAN
 Nama Lokal Daerah: TAMBAN
 Nama Indonesia: Tembang
 Nama Internasional: Goldstripe sardinella
 Nama Latin: Sardinella gibbosa (Bleeker, 1849)" '="">Tembang</option><option value="273" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Deepbody sardinella
 Nama Latin: Sardinella brachysoma (Bleeker, 1852)" '="">Tembang</option><option value="274" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Fringescale sardinella
 Nama Latin: Sardinella fimbriata (Valenciennes, 1847)" '="">Tembang</option><option value="16" title="Nama Lokal: TENGGIRI
 Nama Lokal Daerah: TENGGIRI
 Nama Indonesia: Tenggiri
 Nama Internasional: Narrow-barred Spanish mackerel
 Nama Latin: Scomberomorus commerson (Lacep�de, 1800)" '="">Tenggiri</option><option value="17" title="Nama Lokal: tenggiri
 Nama Lokal Daerah: tenggiri
 Nama Indonesia: Tenggiri papan
 Nama Internasional: Indo-Pacific king mackerel
 Nama Latin: Scomberomorus guttatus (Bloch &amp; Schneider, 1801)" '="">Tenggiri papan</option><option value="39" title="Nama Lokal: TERI
 Nama Lokal Daerah: TERI
 Nama Indonesia: Teri
 Nama Internasional: Anchovies
 Nama Latin: Stolephorus spp." '="">Teri</option><option value="44" title="Nama Lokal: terubuk
 Nama Lokal Daerah: terubuk
 Nama Indonesia: Terubuk
 Nama Internasional: Hilsha shad
 Nama Latin: Tenualosa ilisha (Hamilton-Buchanan,1822)" '="">Terubuk</option><option value="35" title="Nama Lokal: TETENGKEK
 Nama Lokal Daerah: TETENGKEK
 Nama Indonesia: Tetengkek
 Nama Internasional: Torpedo scad
 Nama Latin: Megalaspis cordyla (Linnaeus, 1758)" '="">Tetengkek</option><option value="260" title="Nama Lokal: TONGKOL
 Nama Lokal Daerah: TONGKOL
 Nama Indonesia: Tongkol abu-abu
 Nama Internasional: Longtail tuna
 Nama Latin: Thunnus tonggol (Bleeker, 1851)" '="">Tongkol abu-abu</option><option value="302" title="Nama Lokal: UDANG DOGOL
 Nama Lokal Daerah: UDANG DOGOL
 Nama Indonesia: Udang  Dogol
 Nama Internasional: Endeavour prawn/ Shrimp red greasiback
 Nama Latin: Metapenaeus ensis, (de Haan, 1850), Metapenaeus monoceros, (Fabricius, 1798)" '="">Udang  Dogol</option><option value="301" title="Nama Lokal: udang windu
 Nama Lokal Daerah: udang windu
 Nama Indonesia: Udang  Windu
 Nama Internasional: Tiger prawn/ Brown tiger prawn
 Nama Latin: Penaeus esculentus (Haswell, 1879)" '="">Udang  Windu</option><option value="184" title="Nama Lokal: udang putih
 Nama Lokal Daerah: udang putih
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn/ White shrimp
 Nama Latin: Penaeus merguiensis (de Man, 1888 )" '="">Udang Jerbung/Udang putih</option><option value="187" title="Nama Lokal: udang jerubung
 Nama Lokal Daerah: udang jerubung
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn, Indian banana
 Nama Latin: Penaeus indicus (Milne Edwards, 1837)" '="">Udang Jerbung/Udang putih</option><option value="290" title="Nama Lokal: UDANG LAINNYA
 Nama Lokal Daerah: UDANG LAINNYA
 Nama Indonesia: Udang Lainnya
 Nama Internasional: Other shrimps
 Nama Latin: " '="">Udang Lainnya</option><option value="183" title="Nama Lokal: UDANG WINDU
 Nama Lokal Daerah: UDANG WINDU
 Nama Indonesia: Udang windu
 Nama Internasional: Jumbo tiger prawn/ shrimp, Giant tiger prawn/shrimp, Blue tiger prawn
 Nama Latin: Penaeus monodon (Fabricius, 1798)" '="">Udang windu</option></select></td><td><select name="id_alat_tangkap_4"><option value="1" title="Nama Internasional: Double rigs shrimp trawl">Pukat tarik udang ganda</option><option value="2" title="Nama Internasional: Otter Trawl">Otter Trawl</option><option value="3" title="Nama Internasional: Fish net">Pukat tarik ikan</option><option value="4" title="Nama Internasional: Pelagic danish seine">Payang (tmsk. Lampara)</option><option value="5" title="Nama Internasional: Demersal danish seine">Dogol (tmsk. Lampara dasar, cantrang)</option><option value="6" title="Nama Internasional: Beach seine">Pukat Pantai (Jaring arad)</option><option value="26" title="Nama Internasional: Other traps">Perangkap lainnya</option><option value="7" title="Nama Internasional: Purse seine">Pukat Cincin</option><option value="8" title="Nama Internasional: Drift gill nets">Jaring insang hanyut</option><option value="9" title="Nama Internasional: Encircling gill nets">Jaring insang lingkar</option><option value="10" title="Nama Internasional: Shrimp entangling gill net">Jaring klitik</option><option value="11" title="Nama Internasional: Set gill nets">Jaring insang tetap</option><option value="12" title="Nama Internasional: Trammel net">Jaring tiga lapis</option><option value="13" title="Nama Internasional: Boat/raft lift net">Bagan perahu/rakit</option><option value="14" title="Nama Internasional: Stationary lift net">Bagan tancap</option><option value="15" title="Nama Internasional: Scoop nets">Serok dan songko</option><option value="16" title="Nama Internasional: Other lift nets">Jaring Angkat Lainnya</option><option value="17" title="Nama Internasional: Tuna long line">Rawai tuna</option><option value="18" title="Nama Internasional: Other drift long line">Rawai hanyut lainnya selain rawai tuna</option><option value="19" title="Nama Internasional: Set long line">Rawai tetap</option><option value="20" title="Nama Internasional: Skipjack pole and line">Huhate</option><option value="21" title="Nama Internasional: Trowl lines">Pancing tonda</option><option value="22" title="Nama Internasional: Hand lines">Pancing ulur</option><option value="23" title="Nama Internasional: Guiding barrier">Sero (tmsk. Kelong)</option><option value="24" title="Nama Internasional: Stow net">Jermal</option><option value="25" title="Nama Internasional: Portable trap">Bubu (tmsk. Bubu ambai)</option><option value="27" title="Nama Internasional: Shell fish gears">Alat penangkap kerang</option><option value="28" title="Nama Internasional: Sea weed collectors">Alat pengumpul rumput laut</option><option value="29" title="Nama Internasional: Muroami">Muroami</option><option value="30" title="Nama Internasional: Cast net">Jala tebar</option><option value="31" title="Nama Internasional: ">Tidak Ada Alat Tangkap</option><option value="32" title="Nama Internasional: Beam trawl">Pukat tarik berbingkai</option><option value="33" title="Nama Internasional: Shore lift net">Anco</option><option value="34" title="Nama Internasional: Set bottom long line">Rawai dasar tetap</option><option value="35" title="Nama Internasional: Vertical line (incl. Vertical long line)">Pancing tegak</option><option value="36" title="Nama Internasional: Squid Jigger">Pancing cumi</option><option value="37" title="Nama Internasional: Other lines">Pancing lainnya</option><option value="38" title="Nama Internasional: Sea cucumber gears">Alat penangkap teripang (ladung)</option><option value="39" title="Nama Internasional: Crab gears">Alat penangkap kepiting</option><option value="40" title="Nama Internasional: Harpoon">Garpu dan Tombak</option><option value="41" title="Nama Internasional: Stern shrimp trawl">Pukat tarik udang tunggal</option><option value="42" title="Nama Internasional: ">Pukat cincin satu kapal</option><option value="43" title="Nama Internasional: ">Pukat Cincin Pelagis Besar dengan satu kapal</option><option value="44" title="Nama Internasional: ">Jaring lingkar tanpa tali kerut/Lampara</option><option value="45" title="Nama Internasional: ">Pukat hela dasar berpalang</option><option value="46" title="Nama Internasional: ">Pukat hela dasar berpapan</option><option value="47" title="Nama Internasional: ">Pukat hela dasar dua kapal</option><option value="48" title="Nama Internasional: ">Pukat hela dasar lainnya</option><option value="49" title="Nama Internasional: ">Pukat hela dasar udang</option><option value="50" title="Nama Internasional: ">Pukat hela pertengahan berpapan</option><option value="51" title="Nama Internasional: ">Puka hela pertengahan dua kapal</option><option value="52" title="Nama Internasional: ">Pukat hela pertengahan udang</option><option value="53" title="Nama Internasional: ">Pukat hela kembar berpapan</option><option value="54" title="Nama Internasional: ">Pukat hela lainnya</option><option value="55" title="Nama Internasional: ">Pukat dorong</option><option value="56" title="Nama Internasional: ">Boke Ami</option><option value="57" title="Nama Internasional: ">Jala jatuh berkapal</option><option value="58" title="Nama Internasional: ">Jala tebar lainnya</option><option value="59" title="Nama Internasional: ">Jaring liong bun</option><option value="60" title="Nama Internasional: ">Jaring insang oseanik</option><option value="61" title="Nama Internasional: tramel net">Jaring insang berlapis</option><option value="62" title="Nama Internasional: ">Jaring insang kombinasi dengan tramel net</option><option value="63" title="Nama Internasional: ">Jaring insang lainnya</option><option value="64" title="Nama Internasional: ">Set Net</option><option value="65" title="Nama Internasional: ">Bubu bersayap</option><option value="66" title="Nama Internasional: Long bag set net">Pukat labuh</option><option value="67" title="Nama Internasional: ">Togo</option><option value="68" title="Nama Internasional: ">Ambai</option><option value="69" title="Nama Internasional: ">Pengerih</option><option value="70" title="Nama Internasional: ">Perangkap ikan peloncat</option><option value="71" title="Nama Internasional: ">Seser</option><option value="72" title="Nama Internasional: ">Pancing berjoran</option><option value="73" title="Nama Internasional: ">Squid angling</option><option value="74" title="Nama Internasional: ">Huhate Mekanis</option><option value="75" title="Nama Internasional: ">Pancing layang-layang</option><option value="76" title="Nama Internasional: ">Ladung</option><option value="77" title="Nama Internasional: ">Panah</option><option value="78" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="79" title="Nama Internasional: ">Ladung</option><option value="80" title="Nama Internasional: ">Panah</option><option value="81" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="82" title="Nama Internasional: tes">Alat tangkap tes</option><option value="83" title="Nama Internasional: ">Pengangkut Ikan</option></select></td><td><input type="text" name="koef_koreksi_4" value="0" style="width: 180px;" title="Data yang di masukan Harus Angka"></td></tr>
<tr>
<td>5. </td><td><select name="id_species_5"><option value="0">Kosong</option><option value="3" title="Nama Lokal: AJI-AJI
 Nama Lokal Daerah: AJI-AJI
 Nama Indonesia: Albakora
 Nama Internasional: Albacore
 Nama Latin: Thunnus alalunga (Bonnaterre, 1788)" '="">Albakora</option><option value="51" title="Nama Lokal: ALU-ALU
 Nama Lokal Daerah: ALU-ALU
 Nama Indonesia: Alu-alu/ Manggilala/Pucul
 Nama Internasional: Great barracuda
 Nama Latin: Sphyraena barracuda (Walbaum, 1792)" '="">Alu-alu/ Manggilala/Pucul</option><option value="46" title="Nama Lokal: CINCARO
 Nama Lokal Daerah: CINCARO
 Nama Indonesia: Banyar/Kembung Lelaki
 Nama Internasional: Indian mackerel
 Nama Latin: Rastrelliger kanagurta" '="">Banyar/Kembung Lelaki</option><option value="57" title="Nama Lokal: BAWAL HITAM
 Nama Lokal Daerah: BAWAL HITAM
 Nama Indonesia: Bawal hitam
 Nama Internasional: Black pomfret
 Nama Latin: Parastromateus niger (Bloch, 1795)" '="">Bawal hitam</option><option value="58" title="Nama Lokal: BAWAL PUTIH
 Nama Lokal Daerah: BAWAL PUTIH
 Nama Indonesia: Bawal putih
 Nama Internasional: Silver pomfret
 Nama Latin: Pampus argenteus (Euphrasen, 1788)" '="">Bawal putih</option><option value="73" title="Nama Lokal: BELANAK
 Nama Lokal Daerah: BELANAK
 Nama Indonesia: Belanak
 Nama Internasional: Blue-spot mullet/ Blue-tail mullet
 Nama Latin: Valamugil seheli (Forssk�l, 1775)" '="">Belanak</option><option value="299" title="Nama Lokal: belanak
 Nama Lokal Daerah: belanak
 Nama Indonesia: Belanak
 Nama Internasional: Mangrove mullets
 Nama Latin: Mugil cephalus (Linnaeus, 1758)" '="">Belanak</option><option value="67" title="Nama Lokal: beloso
 Nama Lokal Daerah: beloso
 Nama Indonesia: Beloso/Buntut kerbo
 Nama Internasional: Greater lizardfish
 Nama Latin: Saurida tumbil (Bloch, 1795)" '="">Beloso/Buntut kerbo</option><option value="264" title="Nama Lokal: MATA BESAR
 Nama Lokal Daerah: MATA BESAR
 Nama Indonesia: Bentong
 Nama Internasional: Oxeye scad
 Nama Latin: Selar boops (Valenciennes, 1833)" '="">Bentong</option><option value="279" title="Nama Lokal: BIJI NANGKA
 Nama Lokal Daerah: BIJI NANGKA
 Nama Indonesia: Biji nangka
 Nama Internasional: Yellow-stripe goatfish
 Nama Latin: Upeneus vittatus (Forssk�l, 1775)" '="">Biji nangka</option><option value="179" title="Nama Lokal: Bijinangka
 Nama Lokal Daerah: Bijinangka
 Nama Indonesia: Bijinangka/Kuniran
 Nama Internasional: Goatfish
 Nama Latin: Upeneus moluccensis" '="">Bijinangka/Kuniran</option><option value="7" title="Nama Lokal: Tongkol
 Nama Lokal Daerah: Tongkol
 Nama Indonesia: Cakalang
 Nama Internasional: Skipjack tuna
 Nama Latin: Katsuwonus pelamis (Linnaeus, 1758)" '="">Cakalang</option><option value="18" title="Nama Lokal: CUCUT
 Nama Lokal Daerah: CUCUT
 Nama Indonesia: Cucut botol
 Nama Internasional: Longnose velvet dogfish
 Nama Latin: Centrocymnus crepidater" '="">Cucut botol</option><option value="225" title="Nama Lokal: CUMI-CUMI
 Nama Lokal Daerah: CUMI-CUMI
 Nama Indonesia: Cumi-cumi
 Nama Internasional: Common squids
 Nama Latin: Loligo spp" '="">Cumi-cumi</option><option value="96" title="Nama Lokal: EKOR KUNING
 Nama Lokal Daerah: EKOR KUNING
 Nama Indonesia: Ekor kuning
 Nama Internasional: Redbelly yellowtail fusilier
 Nama Latin: Caesio cuning (Bloch, 1791)" '="">Ekor kuning</option><option value="69" title="Nama Lokal: Gerot
 Nama Lokal Daerah: Gerot
 Nama Indonesia: Gerot-gerot
 Nama Internasional: Saddle grunt/ Spotted javelinfish
 Nama Latin: Pomadasys maculatum(Bloch, 1797)" '="">Gerot-gerot</option><option value="48" title="Nama Lokal: Golok-golok
 Nama Lokal Daerah: Golok-golok
 Nama Indonesia: Golok-golok
 Nama Internasional: Dorab wolf-herring
 Nama Latin: Chirocentrus dorab (Forssk�l, 1775)" '="">Golok-golok</option><option value="61" title="Nama Lokal: GULAMAH
 Nama Lokal Daerah: GULAMAH
 Nama Indonesia: Gulamah/Tigawaja
 Nama Internasional: Croackers
 Nama Latin: Nibea albiflora (Ricahardson, 1846)" '="">Gulamah/Tigawaja</option><option value="94" title="Nama Lokal: Baronang
 Nama Lokal Daerah: Baronang
 Nama Indonesia: Ikan beronang
 Nama Internasional: Orange-spotted spinefoot
 Nama Latin: Siganus guttatus (Bloch, 1787)" '="">Ikan beronang</option><option value="89" title="Nama Lokal: KAKAP MERAH
 Nama Lokal Daerah: KAKAP MERAH
 Nama Indonesia: Ikan kakap merah/Bambangan
 Nama Internasional: Red snappers
 Nama Latin: Lutjanus spp" '="">Ikan kakap merah/Bambangan</option><option value="72" title="Nama Lokal: LIDAH
 Nama Lokal Daerah: LIDAH
 Nama Indonesia: Ikan lidah
 Nama Internasional: Tongue soles
 Nama Latin: Cynoglossus spp, Pleuronectus spp." '="">Ikan lidah</option><option value="64" title="Nama Lokal: IKAN SEBELAH
 Nama Lokal Daerah: IKAN SEBELAH
 Nama Indonesia: Ikan sebelah (Terompa)
 Nama Internasional: Indian halibut/ Queensland halibut
 Nama Latin: Psettodes erumei (Schneider, 1801)" '="">Ikan sebelah (Terompa)</option><option value="90" title="Nama Lokal: KAKAP
 Nama Lokal Daerah: kakap
 Nama Indonesia: Kakap putih
 Nama Internasional: Barramundi, Giant sea perch
 Nama Latin: Lates calcarifer (Bloch)" '="">Kakap putih</option><option value="294" title="Nama Lokal: KAPAS-KAPAS
 Nama Lokal Daerah: KAPAS-KAPAS
 Nama Indonesia: Kapas-kapas
 Nama Internasional: Fals trevally
 Nama Latin: Lactarius lactarius (Bloch &amp; Schneider, 1801)" '="">Kapas-kapas</option><option value="276" title="Nama Lokal: KEMBUNG
 Nama Lokal Daerah: KEMBUNG
 Nama Indonesia: Kembung Perempuan
 Nama Internasional: Short-bodied mackerel
 Nama Latin: Rastrelliger brachysoma" '="">Kembung Perempuan</option><option value="296" title="Nama Lokal: pasir-pasir
 Nama Lokal Daerah: pasir-pasir
 Nama Indonesia: Kempar Pati
 Nama Internasional: Sharptooth jobfish
 Nama Latin: Pristipomoides typus (Bleeker, 1852)" '="">Kempar Pati</option><option value="87" title="Nama Lokal: Kerapu
 Nama Lokal Daerah: kerapu
 Nama Indonesia: Kerapu balong
 Nama Internasional: Honeycomb grouper
 Nama Latin: Epinephelus merra (Bloch, 1793)" '="">Kerapu balong</option><option value="286" title="Nama Lokal: KERAPU
 Nama Lokal Daerah: KERAPU
 Nama Indonesia: Kerapu karang
 Nama Internasional: Blue-lined seabass
 Nama Latin: Cephalophodis boenack (Bloch, 1790)" '="">Kerapu karang</option><option value="74" title="Nama Lokal: Kerong-kerong
 Nama Lokal Daerah: Kerong-kerong
 Nama Indonesia: Kerong-kerong
 Nama Internasional: Largescale terapon
 Nama Latin: Terapon theraps (Cuvier, 1829)" '="">Kerong-kerong</option><option value="70" title="Nama Lokal: KURISI
 Nama Lokal Daerah: KURISI
 Nama Indonesia: Kurisi
 Nama Internasional: Threadfin bream
 Nama Latin: Nemimterus spp" '="">Kurisi</option><option value="278" title="Nama Lokal: SENANGIN
 Nama Lokal Daerah: SENANGIN
 Nama Indonesia: Kuro/senangin
 Nama Internasional: Threadfins
 Nama Latin: Polynemus spp" '="">Kuro/senangin</option><option value="34" title="Nama Lokal: KUWE
 Nama Lokal Daerah: KUWE
 Nama Indonesia: Kwee
 Nama Internasional: Bigeye trevally
 Nama Latin: Caranx sexfaciatus (Quoy &amp; Gaimard,1825)" '="">Kwee</option><option value="269" title="Nama Lokal: kuwe
 Nama Lokal Daerah: kuwe
 Nama Indonesia: Kwee
 Nama Internasional: Tille trevally
 Nama Latin: Caranx tile (Cuvier, 1833)" '="">Kwee</option><option value="271" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang biru
 Nama Internasional: Mackerel scad
 Nama Latin: Decapterus macarellus (Cuvier, 1833)" '="">Layang biru</option><option value="38" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang deles
 Nama Internasional: Shortfin scad
 Nama Latin: Decapterus macrosoma (Bleeker, 1855)" '="">Layang deles</option><option value="31" title="Nama Lokal: SELAYANG
 Nama Lokal Daerah: SELAYANG
 Nama Indonesia: Layang/Benggol
 Nama Internasional: Indian scad
 Nama Latin: Decapterus. Ruselli (R�ppell, 1830)" '="">Layang/Benggol</option><option value="62" title="Nama Lokal: LAYUR
 Nama Lokal Daerah: CUALAI
 Nama Indonesia: Layur
 Nama Internasional: Hairtails
 Nama Latin: Trichiurus spp" '="">Layur</option><option value="42" title="Nama Lokal: LEMURU
 Nama Lokal Daerah: LEMURU
 Nama Indonesia: Lemuru
 Nama Internasional: Bali sardinella
 Nama Latin: Sardinella lemuru (Bleeker, 1853)" '="">Lemuru</option><option value="54" title="Nama Lokal: MANYUNG
 Nama Lokal Daerah: MANYUNG
 Nama Indonesia: Manyung
 Nama Internasional: Giant catfish
 Nama Latin: Arius thalassinus (R�ppell, 1837)" '="">Manyung</option><option value="133" title="Nama Lokal: LAYARAN
 Nama Lokal Daerah: LAYARAN
 Nama Indonesia: Moris layaran
 Nama Internasional: Horned coralfish
 Nama Latin: Heniochus permutatus" '="">Moris layaran</option><option value="80" title="Nama Lokal: Pari
 Nama Lokal Daerah: Pari
 Nama Indonesia: Pari burung
 Nama Internasional: Eaglerays
 Nama Latin: Myliobatus spp, Aetobatus spp. (Euphrasen, 1790) Aetomylaeus spp" '="">Pari burung</option><option value="285" title="Nama Lokal: pari
 Nama Lokal Daerah: pari
 Nama Indonesia: Pari kelelawar
 Nama Internasional: Devilrays, Mantarays
 Nama Latin: Mobula spp" '="">Pari kelelawar</option><option value="78" title="Nama Lokal: PARI
 Nama Lokal Daerah: PARI
 Nama Indonesia: Pari kembang/Pari macan
 Nama Internasional: Stingrays
 Nama Latin: Dasyatis spp" '="">Pari kembang/Pari macan</option><option value="66" title="Nama Lokal: PISANG-PISANG
 Nama Lokal Daerah: PISANG-PISANG
 Nama Indonesia: Peperek
 Nama Internasional: Slipmouths or Pony fishes
 Nama Latin: Leiognathus spp" '="">Peperek</option><option value="204" title="Nama Lokal: RAJUNGAN
 Nama Lokal Daerah: RAJUNGAN
 Nama Indonesia: Rajungan
 Nama Internasional: Swimming Crabs
 Nama Latin: Portunus pelagicus (Linnaeus, 1766)" '="">Rajungan</option><option value="262" title="Nama Lokal: SELAR
 Nama Lokal Daerah: SELAR
 Nama Indonesia: Selar kuning
 Nama Internasional: Black banded trevally
 Nama Latin: Seriolina nigrofasciata (R�ppell, 1829)" '="">Selar kuning</option><option value="263" title="Nama Lokal: selar
 Nama Lokal Daerah: selar
 Nama Indonesia: Selar kuning
 Nama Internasional: White mouth jack
 Nama Latin: Uraspis uraspis" '="">Selar kuning</option><option value="174" title="Nama Lokal: SEMBILANG
 Nama Lokal Daerah: SEMBILANG
 Nama Indonesia: Sembilang karang
 Nama Internasional: Striped catfish-eel
 Nama Latin: Plotosus lineatus" '="">Sembilang karang</option><option value="226" title="Nama Lokal: SOTONG
 Nama Lokal Daerah: SOTONG
 Nama Indonesia: Sotong
 Nama Internasional: Cuttle fish
 Nama Latin: Sepia Spp" '="">Sotong</option><option value="60" title="Nama Lokal: SWANGGI
 Nama Lokal Daerah: SWANGGI
 Nama Indonesia: Swanggi
 Nama Internasional: Purple-spotted bigeye
 Nama Latin: Priacanthus tayenus (Richardson, 1846)" '="">Swanggi</option><option value="37" title="Nama Lokal: TALANG-TALANG
 Nama Lokal Daerah: TALANG-TALANG
 Nama Indonesia: Talang-talang
 Nama Internasional: Talang queenfish
 Nama Latin: Scomberoides commersonnianus (Lacep�de, 1802)" '="">Talang-talang</option><option value="270" title="Nama Lokal: Talang-talang
 Nama Lokal Daerah: Talang-talang
 Nama Indonesia: Talang-talang
 Nama Internasional: Needlescale queenfish
 Nama Latin: Scomberoides tol (Cuvier, 1832)" '="">Talang-talang</option><option value="41" title="Nama Lokal: TAMBAN
 Nama Lokal Daerah: TAMBAN
 Nama Indonesia: Tembang
 Nama Internasional: Goldstripe sardinella
 Nama Latin: Sardinella gibbosa (Bleeker, 1849)" '="">Tembang</option><option value="273" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Deepbody sardinella
 Nama Latin: Sardinella brachysoma (Bleeker, 1852)" '="">Tembang</option><option value="274" title="Nama Lokal: TEMBANG
 Nama Lokal Daerah: TEMBANG
 Nama Indonesia: Tembang
 Nama Internasional: Fringescale sardinella
 Nama Latin: Sardinella fimbriata (Valenciennes, 1847)" '="">Tembang</option><option value="16" title="Nama Lokal: TENGGIRI
 Nama Lokal Daerah: TENGGIRI
 Nama Indonesia: Tenggiri
 Nama Internasional: Narrow-barred Spanish mackerel
 Nama Latin: Scomberomorus commerson (Lacep�de, 1800)" '="">Tenggiri</option><option value="17" title="Nama Lokal: tenggiri
 Nama Lokal Daerah: tenggiri
 Nama Indonesia: Tenggiri papan
 Nama Internasional: Indo-Pacific king mackerel
 Nama Latin: Scomberomorus guttatus (Bloch &amp; Schneider, 1801)" '="">Tenggiri papan</option><option value="39" title="Nama Lokal: TERI
 Nama Lokal Daerah: TERI
 Nama Indonesia: Teri
 Nama Internasional: Anchovies
 Nama Latin: Stolephorus spp." '="">Teri</option><option value="44" title="Nama Lokal: terubuk
 Nama Lokal Daerah: terubuk
 Nama Indonesia: Terubuk
 Nama Internasional: Hilsha shad
 Nama Latin: Tenualosa ilisha (Hamilton-Buchanan,1822)" '="">Terubuk</option><option value="35" title="Nama Lokal: TETENGKEK
 Nama Lokal Daerah: TETENGKEK
 Nama Indonesia: Tetengkek
 Nama Internasional: Torpedo scad
 Nama Latin: Megalaspis cordyla (Linnaeus, 1758)" '="">Tetengkek</option><option value="260" title="Nama Lokal: TONGKOL
 Nama Lokal Daerah: TONGKOL
 Nama Indonesia: Tongkol abu-abu
 Nama Internasional: Longtail tuna
 Nama Latin: Thunnus tonggol (Bleeker, 1851)" '="">Tongkol abu-abu</option><option value="302" title="Nama Lokal: UDANG DOGOL
 Nama Lokal Daerah: UDANG DOGOL
 Nama Indonesia: Udang  Dogol
 Nama Internasional: Endeavour prawn/ Shrimp red greasiback
 Nama Latin: Metapenaeus ensis, (de Haan, 1850), Metapenaeus monoceros, (Fabricius, 1798)" '="">Udang  Dogol</option><option value="301" title="Nama Lokal: udang windu
 Nama Lokal Daerah: udang windu
 Nama Indonesia: Udang  Windu
 Nama Internasional: Tiger prawn/ Brown tiger prawn
 Nama Latin: Penaeus esculentus (Haswell, 1879)" '="">Udang  Windu</option><option value="184" title="Nama Lokal: udang putih
 Nama Lokal Daerah: udang putih
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn/ White shrimp
 Nama Latin: Penaeus merguiensis (de Man, 1888 )" '="">Udang Jerbung/Udang putih</option><option value="187" title="Nama Lokal: udang jerubung
 Nama Lokal Daerah: udang jerubung
 Nama Indonesia: Udang Jerbung/Udang putih
 Nama Internasional: Banana prawn, Indian banana
 Nama Latin: Penaeus indicus (Milne Edwards, 1837)" '="">Udang Jerbung/Udang putih</option><option value="290" title="Nama Lokal: UDANG LAINNYA
 Nama Lokal Daerah: UDANG LAINNYA
 Nama Indonesia: Udang Lainnya
 Nama Internasional: Other shrimps
 Nama Latin: " '="">Udang Lainnya</option><option value="183" title="Nama Lokal: UDANG WINDU
 Nama Lokal Daerah: UDANG WINDU
 Nama Indonesia: Udang windu
 Nama Internasional: Jumbo tiger prawn/ shrimp, Giant tiger prawn/shrimp, Blue tiger prawn
 Nama Latin: Penaeus monodon (Fabricius, 1798)" '="">Udang windu</option></select></td><td><select name="id_alat_tangkap_5"><option value="1" title="Nama Internasional: Double rigs shrimp trawl">Pukat tarik udang ganda</option><option value="2" title="Nama Internasional: Otter Trawl">Otter Trawl</option><option value="3" title="Nama Internasional: Fish net">Pukat tarik ikan</option><option value="4" title="Nama Internasional: Pelagic danish seine">Payang (tmsk. Lampara)</option><option value="5" title="Nama Internasional: Demersal danish seine">Dogol (tmsk. Lampara dasar, cantrang)</option><option value="6" title="Nama Internasional: Beach seine">Pukat Pantai (Jaring arad)</option><option value="26" title="Nama Internasional: Other traps">Perangkap lainnya</option><option value="7" title="Nama Internasional: Purse seine">Pukat Cincin</option><option value="8" title="Nama Internasional: Drift gill nets">Jaring insang hanyut</option><option value="9" title="Nama Internasional: Encircling gill nets">Jaring insang lingkar</option><option value="10" title="Nama Internasional: Shrimp entangling gill net">Jaring klitik</option><option value="11" title="Nama Internasional: Set gill nets">Jaring insang tetap</option><option value="12" title="Nama Internasional: Trammel net">Jaring tiga lapis</option><option value="13" title="Nama Internasional: Boat/raft lift net">Bagan perahu/rakit</option><option value="14" title="Nama Internasional: Stationary lift net">Bagan tancap</option><option value="15" title="Nama Internasional: Scoop nets">Serok dan songko</option><option value="16" title="Nama Internasional: Other lift nets">Jaring Angkat Lainnya</option><option value="17" title="Nama Internasional: Tuna long line">Rawai tuna</option><option value="18" title="Nama Internasional: Other drift long line">Rawai hanyut lainnya selain rawai tuna</option><option value="19" title="Nama Internasional: Set long line">Rawai tetap</option><option value="20" title="Nama Internasional: Skipjack pole and line">Huhate</option><option value="21" title="Nama Internasional: Trowl lines">Pancing tonda</option><option value="22" title="Nama Internasional: Hand lines">Pancing ulur</option><option value="23" title="Nama Internasional: Guiding barrier">Sero (tmsk. Kelong)</option><option value="24" title="Nama Internasional: Stow net">Jermal</option><option value="25" title="Nama Internasional: Portable trap">Bubu (tmsk. Bubu ambai)</option><option value="27" title="Nama Internasional: Shell fish gears">Alat penangkap kerang</option><option value="28" title="Nama Internasional: Sea weed collectors">Alat pengumpul rumput laut</option><option value="29" title="Nama Internasional: Muroami">Muroami</option><option value="30" title="Nama Internasional: Cast net">Jala tebar</option><option value="31" title="Nama Internasional: ">Tidak Ada Alat Tangkap</option><option value="32" title="Nama Internasional: Beam trawl">Pukat tarik berbingkai</option><option value="33" title="Nama Internasional: Shore lift net">Anco</option><option value="34" title="Nama Internasional: Set bottom long line">Rawai dasar tetap</option><option value="35" title="Nama Internasional: Vertical line (incl. Vertical long line)">Pancing tegak</option><option value="36" title="Nama Internasional: Squid Jigger">Pancing cumi</option><option value="37" title="Nama Internasional: Other lines">Pancing lainnya</option><option value="38" title="Nama Internasional: Sea cucumber gears">Alat penangkap teripang (ladung)</option><option value="39" title="Nama Internasional: Crab gears">Alat penangkap kepiting</option><option value="40" title="Nama Internasional: Harpoon">Garpu dan Tombak</option><option value="41" title="Nama Internasional: Stern shrimp trawl">Pukat tarik udang tunggal</option><option value="42" title="Nama Internasional: ">Pukat cincin satu kapal</option><option value="43" title="Nama Internasional: ">Pukat Cincin Pelagis Besar dengan satu kapal</option><option value="44" title="Nama Internasional: ">Jaring lingkar tanpa tali kerut/Lampara</option><option value="45" title="Nama Internasional: ">Pukat hela dasar berpalang</option><option value="46" title="Nama Internasional: ">Pukat hela dasar berpapan</option><option value="47" title="Nama Internasional: ">Pukat hela dasar dua kapal</option><option value="48" title="Nama Internasional: ">Pukat hela dasar lainnya</option><option value="49" title="Nama Internasional: ">Pukat hela dasar udang</option><option value="50" title="Nama Internasional: ">Pukat hela pertengahan berpapan</option><option value="51" title="Nama Internasional: ">Puka hela pertengahan dua kapal</option><option value="52" title="Nama Internasional: ">Pukat hela pertengahan udang</option><option value="53" title="Nama Internasional: ">Pukat hela kembar berpapan</option><option value="54" title="Nama Internasional: ">Pukat hela lainnya</option><option value="55" title="Nama Internasional: ">Pukat dorong</option><option value="56" title="Nama Internasional: ">Boke Ami</option><option value="57" title="Nama Internasional: ">Jala jatuh berkapal</option><option value="58" title="Nama Internasional: ">Jala tebar lainnya</option><option value="59" title="Nama Internasional: ">Jaring liong bun</option><option value="60" title="Nama Internasional: ">Jaring insang oseanik</option><option value="61" title="Nama Internasional: tramel net">Jaring insang berlapis</option><option value="62" title="Nama Internasional: ">Jaring insang kombinasi dengan tramel net</option><option value="63" title="Nama Internasional: ">Jaring insang lainnya</option><option value="64" title="Nama Internasional: ">Set Net</option><option value="65" title="Nama Internasional: ">Bubu bersayap</option><option value="66" title="Nama Internasional: Long bag set net">Pukat labuh</option><option value="67" title="Nama Internasional: ">Togo</option><option value="68" title="Nama Internasional: ">Ambai</option><option value="69" title="Nama Internasional: ">Pengerih</option><option value="70" title="Nama Internasional: ">Perangkap ikan peloncat</option><option value="71" title="Nama Internasional: ">Seser</option><option value="72" title="Nama Internasional: ">Pancing berjoran</option><option value="73" title="Nama Internasional: ">Squid angling</option><option value="74" title="Nama Internasional: ">Huhate Mekanis</option><option value="75" title="Nama Internasional: ">Pancing layang-layang</option><option value="76" title="Nama Internasional: ">Ladung</option><option value="77" title="Nama Internasional: ">Panah</option><option value="78" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="79" title="Nama Internasional: ">Ladung</option><option value="80" title="Nama Internasional: ">Panah</option><option value="81" title="Nama Internasional: ">Alat penjepit dan melukai lainnya</option><option value="82" title="Nama Internasional: tes">Alat tangkap tes</option><option value="83" title="Nama Internasional: ">Pengangkut Ikan</option></select></td><td><input type="text" name="koef_koreksi_5" value="0" style="width: 180px;" title="Data yang di masukan Harus Angka"></td></tr>
</tbody>
</table><button class="buttonKirim" type="submit" name="submit"> KIRIM </button></form>