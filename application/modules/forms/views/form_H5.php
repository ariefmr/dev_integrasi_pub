<table id="entry_h1" border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td>Domisili Kapal</td><td><div class="btn-group" data-toggle="buttons-radio"><button type="button" class="btn domisili_kapal lokal active" value="lokal">Lokal</button><button type="button" class="btn domisili_kapal luar" value="luar">Luar Pelabuhan</button></div></td></tr>
<tr>
<td>Nama Kapal</td><td><p id="nama_kapal">AC MILAN V</p></td></tr>
</tbody>
</table>

<br>

<ul class="nav nav-tabs" id="nav_tab">
        <li class="active"><a href="#tab_kapal">Detail Kapal</a></li>
        <li><a href="#tab_muat">Muatan</a></li>
        <li><a href="#tab_angkut">Angkut Ikan</a></li>
    </ul>

<br>
<div class="tab-content">  
        <div class="tab-pane active" id="tab_kapal">
          <div id="detail_kapal">
<input type="hidden" name="nama_kapal" value="AC MILAN V">

<input type="hidden" name="asal_kapal" value="020301">

<input type="hidden" name="pemilik_kapal" value="DTM EDWARD">

<input type="hidden" name="dimensi_p" value="75.4">

<input type="hidden" name="dimensi_l" value="7">

<input type="hidden" name="dimensi_d" value="2.18">

<input type="hidden" name="dimensi_dr" value="0">

<input type="hidden" name="tanda_selar" value="1388/PPB">

<input type="hidden" name="tonase" value="98">

<input type="hidden" name="merk_mesin_utama" value="">

<input type="hidden" name="tenaga_mesin_total" value="0">
<table id="detail_kapal_h1" class="table table-hover" style="width:60%;" border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td>Pemilik Kapal</td><td><input type="text" name="" value="" ></td></tr>
<tr>
<td>Asal Kapal</td><td><input type="text" name="" value="" ></td></tr>
<tr>
<td>Nama Nahkoda</td><td><input type="text" name="nama_nahkoda" value=""></td></tr>
<tr>
<td>Jumlah ABK</td><td>WNI <input type="text" name="jml_abk_wni" value="0" style="width: 50px;"> <i>Orang</i> &nbsp; &nbsp;WNA <input type="text" name="jml_abk_wna" value="0" style="width: 50px;"> <i>Orang</i></td></tr>
<tr>
<td>Ukuran Kapal</td><td><i class="divider-vertical">P</i> <input type="text" name="" value="" style="width: 50px;" > m &nbsp;<i class="divider-vertical">L</i> <input type="text" name="" value="" style="width: 50px;" > m &nbsp;<i class="divider-vertical">D </i><input type="text" name="" value="" style="width: 50px;" > m &nbsp;<i class="divider-vertical">DR </i><input type="text" name="" value="" style="width: 50px;" > m &nbsp;</td></tr>
<tr>
<td>Tonase / Nomor Selar</td><td><input type="text" name="" value="" ></td></tr>
<tr>
<td>Merk Mesin / Besar PK</td><td><input type="text" name="" value=" " ></td></tr>
<tr>
<td>Alat Tangkap Utama</td><td><select name="id_alat_tangkap">
<option value="0">Kosong</option>
<option value="1">Pukat tarik udang ganda</option>
<option value="2">Otter Trawl</option>
<option value="3">Pukat tarik ikan</option>
<option value="4">Payang (tmsk. Lampara)</option>
<option value="5">Dogol (tmsk. Lampara dasar, cantrang)</option>
<option value="6">Pukat Pantai (Jaring arad)</option>
<option value="26">Perangkap lainnya</option>
<option value="7" selected="selected">Pukat Cincin</option>
<option value="8">Jaring insang hanyut</option>
<option value="9">Jaring insang lingkar</option>
<option value="10">Jaring klitik</option>
<option value="11">Jaring insang tetap</option>
<option value="12">Jaring tiga lapis</option>
<option value="13">Bagan perahu/rakit</option>
<option value="14">Bagan tancap</option>
<option value="15">Serok dan songko</option>
<option value="16">Jaring Angkat Lainnya</option>
<option value="17">Rawai tuna</option>
<option value="18">Rawai hanyut lainnya selain rawai tuna</option>
<option value="19">Rawai tetap</option>
<option value="20">Huhate</option>
<option value="21">Pancing tonda</option>
<option value="22">Pancing ulur</option>
<option value="23">Sero (tmsk. Kelong)</option>
<option value="24">Jermal</option>
<option value="25">Bubu (tmsk. Bubu ambai)</option>
<option value="27">Alat penangkap kerang</option>
<option value="28">Alat pengumpul rumput laut</option>
<option value="29">Muroami</option>
<option value="30">Jala tebar</option>
<option value="31">Tidak Ada Alat Tangkap</option>
<option value="32">Pukat tarik berbingkai</option>
<option value="33">Anco</option>
<option value="34">Rawai dasar tetap</option>
<option value="35">Pancing tegak</option>
<option value="36">Pancing cumi</option>
<option value="37">Pancing lainnya</option>
<option value="38">Alat penangkap teripang (ladung)</option>
<option value="39">Alat penangkap kepiting</option>
<option value="40">Garpu dan Tombak</option>
<option value="41">Pukat tarik udang tunggal</option>
<option value="42">Pukat cincin satu kapal</option>
<option value="43">Pukat Cincin Pelagis Besar dengan satu kapal</option>
<option value="44">Jaring lingkar tanpa tali kerut/Lampara</option>
<option value="45">Pukat hela dasar berpalang</option>
<option value="46">Pukat hela dasar berpapan</option>
<option value="47">Pukat hela dasar dua kapal</option>
<option value="48">Pukat hela dasar lainnya</option>
<option value="49">Pukat hela dasar udang</option>
<option value="50">Pukat hela pertengahan berpapan</option>
<option value="51">Puka hela pertengahan dua kapal</option>
<option value="52">Pukat hela pertengahan udang</option>
<option value="53">Pukat hela kembar berpapan</option>
<option value="54">Pukat hela lainnya</option>
<option value="55">Pukat dorong</option>
<option value="56">Boke Ami</option>
<option value="57">Jala jatuh berkapal</option>
<option value="58">Jala tebar lainnya</option>
<option value="59">Jaring liong bun</option>
<option value="60">Jaring insang oseanik</option>
<option value="61">Jaring insang berlapis</option>
<option value="62">Jaring insang kombinasi dengan tramel net</option>
<option value="63">Jaring insang lainnya</option>
<option value="64">Set Net</option>
<option value="65">Bubu bersayap</option>
<option value="66">Pukat labuh</option>
<option value="67">Togo</option>
<option value="68">Ambai</option>
<option value="69">Pengerih</option>
<option value="70">Perangkap ikan peloncat</option>
<option value="71">Seser</option>
<option value="72">Pancing berjoran</option>
<option value="73">Squid angling</option>
<option value="74">Huhate Mekanis</option>
<option value="75">Pancing layang-layang</option>
<option value="76">Ladung</option>
<option value="77">Panah</option>
<option value="78">Alat penjepit dan melukai lainnya</option>
<option value="79">Ladung</option>
<option value="80">Panah</option>
<option value="81">Alat penjepit dan melukai lainnya</option>
<option value="82">Alat tangkap tes</option>
<option value="83">Pengangkut Ikan</option>
</select></td></tr>
<tr>
<td>Alat Bantu Penangkapan</td><td>1. <select name="id_alat_bantu_1">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select><br>2. <select name="id_alat_bantu_2">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select><br>3. <select name="id_alat_bantu_3">
<option value="0" selected="selected">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select></td></tr>
<tr>
<td>Tujuan Keberangkatan</td><td><input type="text" name="tujuan" value="0"></td></tr>
</tbody>
</table></div>
        </div>
        <div class="tab-pane" id="tab_muat">
          <table id="entry_h1" border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>Nama Perbekalan</th><th>Jumlah</th><th>Satuan</th></tr>
</thead>
<tbody>
<tr>
<td>Solar</td><td><input type="text" name="bekal_Solar" value="0"></td><td>Lt</td></tr>
<tr>
<td>Bensin</td><td><input type="text" name="bekal_Bensin" value="0"></td><td>Lt</td></tr>
<tr>
<td>Minyak tanah</td><td><input type="text" name="bekal_Minyak tanah" value="0"></td><td>Lt</td></tr>
<tr>
<td>Oli</td><td><input type="text" name="bekal_Oli" value="0"></td><td>Lt</td></tr>
<tr>
<td>Es</td><td><input type="text" name="bekal_Es" value="0"></td><td>Kg</td></tr>
<tr>
<td>Garam</td><td><input type="text" name="bekal_Garam" value="0"></td><td>Kg</td></tr>
<tr>
<td>Air</td><td><input type="text" name="bekal_Air" value="0"></td><td>Lt</td></tr>
<tr>
<td>Umpan</td><td><input type="text" name="bekal_Umpan" value="0"></td><td>Kg</td></tr>
<tr>
<td>Ransum</td><td><input type="text" name="bekal_Ransum" value="0"></td><td>Rp</td></tr>
</tbody>
</table>        </div>
        <div class="tab-pane" id="tab_angkut">
         <table id="entry_h1" border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td><label class="checkbox"><input type="checkbox" name="is_angkut" value="true">Angkut Ikan?</label></td><td><p id="jml_baris"></p></td><td><p id="is_tranship"></p></td><td><p id="nama_kapal_asal"></p></td></tr>
</tbody>
</table>            <div class="hide" id="container_angkut_ikan">
                <table id="angkut_ikan"></table>
            </div>
        </div>
    </div>

  <br>

  <div class="tab-pane active" id="tab_muat">
          <table id="entry_h1" border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>Nama Perbekalan</th><th>Jumlah</th><th>Satuan</th></tr>
</thead>
<tbody>
<tr>
<td>Solar</td><td><input type="text" name="bekal_Solar" value="0"></td><td>Lt</td></tr>
<tr>
<td>Bensin</td><td><input type="text" name="bekal_Bensin" value="0"></td><td>Lt</td></tr>
<tr>
<td>Minyak tanah</td><td><input type="text" name="bekal_Minyak tanah" value="0"></td><td>Lt</td></tr>
<tr>
<td>Oli</td><td><input type="text" name="bekal_Oli" value="0"></td><td>Lt</td></tr>
<tr>
<td>Es</td><td><input type="text" name="bekal_Es" value="0"></td><td>Kg</td></tr>
<tr>
<td>Garam</td><td><input type="text" name="bekal_Garam" value="0"></td><td>Kg</td></tr>
<tr>
<td>Air</td><td><input type="text" name="bekal_Air" value="0"></td><td>Lt</td></tr>
<tr>
<td>Umpan</td><td><input type="text" name="bekal_Umpan" value="0"></td><td>Kg</td></tr>
<tr>
<td>Ransum</td><td><input type="text" name="bekal_Ransum" value="0"></td><td>Rp</td></tr>
</tbody>
</table>        </div>

<br>

<table id="angkut_ikan"><thead><tr><th>Jenis Ikan</th><th>Kondisi <br>Ikan</th><th>Volume <br>Ikan</th><th>Asal</th><th>Harga</th><th>Jenis Mata Uang Asing</th><th>Kurs (RP/Jenis Mata Uang)</th><th>Nilai Transhipment</th></tr></thead><tbody><tr><td><select name="id_species_1"><option value="0">kosong</option><option value="3">Albakora</option><option value="51">Alu-alu/ Manggilala/Pucul</option><option value="46">Banyar/Kembung Lelaki</option><option value="57">Bawal hitam</option><option value="58">Bawal putih</option><option value="73">Belanak</option><option value="299">Belanak</option><option value="67">Beloso/Buntut kerbo</option><option value="264">Bentong</option><option value="279">Biji nangka</option><option value="179">Bijinangka/Kuniran</option><option value="7">Cakalang</option><option value="18">Cucut botol</option><option value="225">Cumi-cumi</option><option value="96">Ekor kuning</option><option value="69">Gerot-gerot</option><option value="48">Golok-golok</option><option value="61">Gulamah/Tigawaja</option><option value="94">Ikan beronang</option><option value="89">Ikan kakap merah/Bambangan</option><option value="72">Ikan lidah</option><option value="64">Ikan sebelah (Terompa)</option><option value="90">Kakap putih</option><option value="294">Kapas-kapas</option><option value="276">Kembung Perempuan</option><option value="296">Kempar Pati</option><option value="87">Kerapu balong</option><option value="286">Kerapu karang</option><option value="74">Kerong-kerong</option><option value="70">Kurisi</option><option value="278">Kuro/senangin</option><option value="34">Kwee</option><option value="269">Kwee</option><option value="271">Layang biru</option><option value="38">Layang deles</option><option value="31">Layang/Benggol</option><option value="62">Layur</option><option value="42">Lemuru</option><option value="54">Manyung</option><option value="133">Moris layaran</option><option value="80">Pari burung</option><option value="285">Pari kelelawar</option><option value="78">Pari kembang/Pari macan</option><option value="66">Peperek</option><option value="204">Rajungan</option><option value="262">Selar kuning</option><option value="263">Selar kuning</option><option value="174">Sembilang karang</option><option value="226">Sotong</option><option value="60">Swanggi</option><option value="37">Talang-talang</option><option value="270">Talang-talang</option><option value="41">Tembang</option><option value="273">Tembang</option><option value="274">Tembang</option><option value="16">Tenggiri</option><option value="17">Tenggiri papan</option><option value="39">Teri</option><option value="44">Terubuk</option><option value="35">Tetengkek</option><option value="260">Tongkol abu-abu</option><option value="302">Udang  Dogol</option><option value="301">Udang  Windu</option><option value="184">Udang Jerbung/Udang putih</option><option value="187">Udang Jerbung/Udang putih</option><option value="290">Udang Lainnya</option><option value="183">Udang windu</option></select></td><td><select name="id_jns_kondisi_ikan_1"><option value="segar">Segar</option><option value="beku">Beku</option><option value="hidup">Hidup</option><option value="asin">Asin</option></select></td><td><input type="text" name="jml_ikan_1" class="input_vol_ikan" value="" style="width: 70px;"></td><td><select name="asal_1"><option value="dalam">Dalam</option><option value="luar">Luar</option></select></td><td><input type="text" name="harga_ikan_1" class="input_harga_ikan" value="" style="width: 100px;"></td><td><input type="text" name="mata_uang_1" class="input_mata_uang" value="" style="width: 100px;"></td><td><input type="text" name="kurs_1" class="input_kurs" value="0" style="width: 100px;"></td><td><input type="text" name="nilai_transhipment_1" class="input_nilai" value="0" style="width: 100px;"></td></tr><tr><td><select name="id_species_2"><option value="0">kosong</option><option value="3">Albakora</option><option value="51">Alu-alu/ Manggilala/Pucul</option><option value="46">Banyar/Kembung Lelaki</option><option value="57">Bawal hitam</option><option value="58">Bawal putih</option><option value="73">Belanak</option><option value="299">Belanak</option><option value="67">Beloso/Buntut kerbo</option><option value="264">Bentong</option><option value="279">Biji nangka</option><option value="179">Bijinangka/Kuniran</option><option value="7">Cakalang</option><option value="18">Cucut botol</option><option value="225">Cumi-cumi</option><option value="96">Ekor kuning</option><option value="69">Gerot-gerot</option><option value="48">Golok-golok</option><option value="61">Gulamah/Tigawaja</option><option value="94">Ikan beronang</option><option value="89">Ikan kakap merah/Bambangan</option><option value="72">Ikan lidah</option><option value="64">Ikan sebelah (Terompa)</option><option value="90">Kakap putih</option><option value="294">Kapas-kapas</option><option value="276">Kembung Perempuan</option><option value="296">Kempar Pati</option><option value="87">Kerapu balong</option><option value="286">Kerapu karang</option><option value="74">Kerong-kerong</option><option value="70">Kurisi</option><option value="278">Kuro/senangin</option><option value="34">Kwee</option><option value="269">Kwee</option><option value="271">Layang biru</option><option value="38">Layang deles</option><option value="31">Layang/Benggol</option><option value="62">Layur</option><option value="42">Lemuru</option><option value="54">Manyung</option><option value="133">Moris layaran</option><option value="80">Pari burung</option><option value="285">Pari kelelawar</option><option value="78">Pari kembang/Pari macan</option><option value="66">Peperek</option><option value="204">Rajungan</option><option value="262">Selar kuning</option><option value="263">Selar kuning</option><option value="174">Sembilang karang</option><option value="226">Sotong</option><option value="60">Swanggi</option><option value="37">Talang-talang</option><option value="270">Talang-talang</option><option value="41">Tembang</option><option value="273">Tembang</option><option value="274">Tembang</option><option value="16">Tenggiri</option><option value="17">Tenggiri papan</option><option value="39">Teri</option><option value="44">Terubuk</option><option value="35">Tetengkek</option><option value="260">Tongkol abu-abu</option><option value="302">Udang  Dogol</option><option value="301">Udang  Windu</option><option value="184">Udang Jerbung/Udang putih</option><option value="187">Udang Jerbung/Udang putih</option><option value="290">Udang Lainnya</option><option value="183">Udang windu</option></select></td><td><select name="id_jns_kondisi_ikan_2"><option value="segar">Segar</option><option value="beku">Beku</option><option value="hidup">Hidup</option><option value="asin">Asin</option></select></td><td><input type="text" name="jml_ikan_2" class="input_vol_ikan" value="" style="width: 70px;"></td><td><select name="asal_2"><option value="dalam">Dalam</option><option value="luar">Luar</option></select></td><td><input type="text" name="harga_ikan_2" class="input_harga_ikan" value="" style="width: 100px;"></td><td><input type="text" name="mata_uang_2" class="input_mata_uang" value="" style="width: 100px;"></td><td><input type="text" name="kurs_2" class="input_kurs" value="0" style="width: 100px;"></td><td><input type="text" name="nilai_transhipment_2" class="input_nilai" value="0" style="width: 100px;"></td></tr><tr><td><select name="id_species_3"><option value="0">kosong</option><option value="3">Albakora</option><option value="51">Alu-alu/ Manggilala/Pucul</option><option value="46">Banyar/Kembung Lelaki</option><option value="57">Bawal hitam</option><option value="58">Bawal putih</option><option value="73">Belanak</option><option value="299">Belanak</option><option value="67">Beloso/Buntut kerbo</option><option value="264">Bentong</option><option value="279">Biji nangka</option><option value="179">Bijinangka/Kuniran</option><option value="7">Cakalang</option><option value="18">Cucut botol</option><option value="225">Cumi-cumi</option><option value="96">Ekor kuning</option><option value="69">Gerot-gerot</option><option value="48">Golok-golok</option><option value="61">Gulamah/Tigawaja</option><option value="94">Ikan beronang</option><option value="89">Ikan kakap merah/Bambangan</option><option value="72">Ikan lidah</option><option value="64">Ikan sebelah (Terompa)</option><option value="90">Kakap putih</option><option value="294">Kapas-kapas</option><option value="276">Kembung Perempuan</option><option value="296">Kempar Pati</option><option value="87">Kerapu balong</option><option value="286">Kerapu karang</option><option value="74">Kerong-kerong</option><option value="70">Kurisi</option><option value="278">Kuro/senangin</option><option value="34">Kwee</option><option value="269">Kwee</option><option value="271">Layang biru</option><option value="38">Layang deles</option><option value="31">Layang/Benggol</option><option value="62">Layur</option><option value="42">Lemuru</option><option value="54">Manyung</option><option value="133">Moris layaran</option><option value="80">Pari burung</option><option value="285">Pari kelelawar</option><option value="78">Pari kembang/Pari macan</option><option value="66">Peperek</option><option value="204">Rajungan</option><option value="262">Selar kuning</option><option value="263">Selar kuning</option><option value="174">Sembilang karang</option><option value="226">Sotong</option><option value="60">Swanggi</option><option value="37">Talang-talang</option><option value="270">Talang-talang</option><option value="41">Tembang</option><option value="273">Tembang</option><option value="274">Tembang</option><option value="16">Tenggiri</option><option value="17">Tenggiri papan</option><option value="39">Teri</option><option value="44">Terubuk</option><option value="35">Tetengkek</option><option value="260">Tongkol abu-abu</option><option value="302">Udang  Dogol</option><option value="301">Udang  Windu</option><option value="184">Udang Jerbung/Udang putih</option><option value="187">Udang Jerbung/Udang putih</option><option value="290">Udang Lainnya</option><option value="183">Udang windu</option></select></td><td><select name="id_jns_kondisi_ikan_3"><option value="segar">Segar</option><option value="beku">Beku</option><option value="hidup">Hidup</option><option value="asin">Asin</option></select></td><td><input type="text" name="jml_ikan_3" class="input_vol_ikan" value="" style="width: 70px;"></td><td><select name="asal_3"><option value="dalam">Dalam</option><option value="luar">Luar</option></select></td><td><input type="text" name="harga_ikan_3" class="input_harga_ikan" value="" style="width: 100px;"></td><td><input type="text" name="mata_uang_3" class="input_mata_uang" value="" style="width: 100px;"></td><td><input type="text" name="kurs_3" class="input_kurs" value="0" style="width: 100px;"></td><td><input type="text" name="nilai_transhipment_3" class="input_nilai" value="0" style="width: 100px;"></td></tr><tr><td><select name="id_species_4"><option value="0">kosong</option><option value="3">Albakora</option><option value="51">Alu-alu/ Manggilala/Pucul</option><option value="46">Banyar/Kembung Lelaki</option><option value="57">Bawal hitam</option><option value="58">Bawal putih</option><option value="73">Belanak</option><option value="299">Belanak</option><option value="67">Beloso/Buntut kerbo</option><option value="264">Bentong</option><option value="279">Biji nangka</option><option value="179">Bijinangka/Kuniran</option><option value="7">Cakalang</option><option value="18">Cucut botol</option><option value="225">Cumi-cumi</option><option value="96">Ekor kuning</option><option value="69">Gerot-gerot</option><option value="48">Golok-golok</option><option value="61">Gulamah/Tigawaja</option><option value="94">Ikan beronang</option><option value="89">Ikan kakap merah/Bambangan</option><option value="72">Ikan lidah</option><option value="64">Ikan sebelah (Terompa)</option><option value="90">Kakap putih</option><option value="294">Kapas-kapas</option><option value="276">Kembung Perempuan</option><option value="296">Kempar Pati</option><option value="87">Kerapu balong</option><option value="286">Kerapu karang</option><option value="74">Kerong-kerong</option><option value="70">Kurisi</option><option value="278">Kuro/senangin</option><option value="34">Kwee</option><option value="269">Kwee</option><option value="271">Layang biru</option><option value="38">Layang deles</option><option value="31">Layang/Benggol</option><option value="62">Layur</option><option value="42">Lemuru</option><option value="54">Manyung</option><option value="133">Moris layaran</option><option value="80">Pari burung</option><option value="285">Pari kelelawar</option><option value="78">Pari kembang/Pari macan</option><option value="66">Peperek</option><option value="204">Rajungan</option><option value="262">Selar kuning</option><option value="263">Selar kuning</option><option value="174">Sembilang karang</option><option value="226">Sotong</option><option value="60">Swanggi</option><option value="37">Talang-talang</option><option value="270">Talang-talang</option><option value="41">Tembang</option><option value="273">Tembang</option><option value="274">Tembang</option><option value="16">Tenggiri</option><option value="17">Tenggiri papan</option><option value="39">Teri</option><option value="44">Terubuk</option><option value="35">Tetengkek</option><option value="260">Tongkol abu-abu</option><option value="302">Udang  Dogol</option><option value="301">Udang  Windu</option><option value="184">Udang Jerbung/Udang putih</option><option value="187">Udang Jerbung/Udang putih</option><option value="290">Udang Lainnya</option><option value="183">Udang windu</option></select></td><td><select name="id_jns_kondisi_ikan_4"><option value="segar">Segar</option><option value="beku">Beku</option><option value="hidup">Hidup</option><option value="asin">Asin</option></select></td><td><input type="text" name="jml_ikan_4" class="input_vol_ikan" value="" style="width: 70px;"></td><td><select name="asal_4"><option value="dalam">Dalam</option><option value="luar">Luar</option></select></td><td><input type="text" name="harga_ikan_4" class="input_harga_ikan" value="" style="width: 100px;"></td><td><input type="text" name="mata_uang_4" class="input_mata_uang" value="" style="width: 100px;"></td><td><input type="text" name="kurs_4" class="input_kurs" value="0" style="width: 100px;"></td><td><input type="text" name="nilai_transhipment_4" class="input_nilai" value="0" style="width: 100px;"></td></tr><tr><td><select name="id_species_5"><option value="0">kosong</option><option value="3">Albakora</option><option value="51">Alu-alu/ Manggilala/Pucul</option><option value="46">Banyar/Kembung Lelaki</option><option value="57">Bawal hitam</option><option value="58">Bawal putih</option><option value="73">Belanak</option><option value="299">Belanak</option><option value="67">Beloso/Buntut kerbo</option><option value="264">Bentong</option><option value="279">Biji nangka</option><option value="179">Bijinangka/Kuniran</option><option value="7">Cakalang</option><option value="18">Cucut botol</option><option value="225">Cumi-cumi</option><option value="96">Ekor kuning</option><option value="69">Gerot-gerot</option><option value="48">Golok-golok</option><option value="61">Gulamah/Tigawaja</option><option value="94">Ikan beronang</option><option value="89">Ikan kakap merah/Bambangan</option><option value="72">Ikan lidah</option><option value="64">Ikan sebelah (Terompa)</option><option value="90">Kakap putih</option><option value="294">Kapas-kapas</option><option value="276">Kembung Perempuan</option><option value="296">Kempar Pati</option><option value="87">Kerapu balong</option><option value="286">Kerapu karang</option><option value="74">Kerong-kerong</option><option value="70">Kurisi</option><option value="278">Kuro/senangin</option><option value="34">Kwee</option><option value="269">Kwee</option><option value="271">Layang biru</option><option value="38">Layang deles</option><option value="31">Layang/Benggol</option><option value="62">Layur</option><option value="42">Lemuru</option><option value="54">Manyung</option><option value="133">Moris layaran</option><option value="80">Pari burung</option><option value="285">Pari kelelawar</option><option value="78">Pari kembang/Pari macan</option><option value="66">Peperek</option><option value="204">Rajungan</option><option value="262">Selar kuning</option><option value="263">Selar kuning</option><option value="174">Sembilang karang</option><option value="226">Sotong</option><option value="60">Swanggi</option><option value="37">Talang-talang</option><option value="270">Talang-talang</option><option value="41">Tembang</option><option value="273">Tembang</option><option value="274">Tembang</option><option value="16">Tenggiri</option><option value="17">Tenggiri papan</option><option value="39">Teri</option><option value="44">Terubuk</option><option value="35">Tetengkek</option><option value="260">Tongkol abu-abu</option><option value="302">Udang  Dogol</option><option value="301">Udang  Windu</option><option value="184">Udang Jerbung/Udang putih</option><option value="187">Udang Jerbung/Udang putih</option><option value="290">Udang Lainnya</option><option value="183">Udang windu</option></select></td><td><select name="id_jns_kondisi_ikan_5"><option value="segar">Segar</option><option value="beku">Beku</option><option value="hidup">Hidup</option><option value="asin">Asin</option></select></td><td><input type="text" name="jml_ikan_5" class="input_vol_ikan" value="" style="width: 70px;"></td><td><select name="asal_5"><option value="dalam">Dalam</option><option value="luar">Luar</option></select></td><td><input type="text" name="harga_ikan_5" class="input_harga_ikan" value="" style="width: 100px;"></td><td><input type="text" name="mata_uang_5" class="input_mata_uang" value="" style="width: 100px;"></td><td><input type="text" name="kurs_5" class="input_kurs" value="0" style="width: 100px;"></td><td><input type="text" name="nilai_transhipment_5" class="input_nilai" value="0" style="width: 100px;"></td></tr></tbody></table>