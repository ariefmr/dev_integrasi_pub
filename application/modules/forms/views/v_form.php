<!-- TAMPIL DATA -->
<p class="lead"><?php echo $content_title;?></p>
<hr>
<div class="row">
        <div class="col-lg-3">
        	<ul class="list-group">
        		<li class="list-group-item">
          			User: <?php echo $username_pengguna; ?>
        		</li>
        		<li class="list-group-item">
          			Pelabuhan: <?php echo $nama_pelabuhan; ?>
        		</li>
      		</ul>
		</div>
        <div class="col-lg-3">
        	<div class="panel panel-primary">
			  <!-- Default panel contents -->
				<div class="panel-heading">Deskripsi Form</div>
					  <div class="panel-body">
					    <p>
					    <?php
					    	// Text deskripsi File diambil dari dropbox, hardlink.
					    	echo file_get_contents($link_dropbox_1);
					    ?></p>
				</div>
      		</div>
      		<div class="panel panel-primary">
			  <!-- Default panel contents -->
				<div class="panel-heading">Database info</div>
					  <div class="panel-body">
					    <p>
					    <?php
					    	// Text deskripsi File diambil dari dropbox, hardlink.
					    	echo file_get_contents($link_dropbox_2);
					    ?></p>
				</div>
      		</div>
		</div>
		
		<div id="kolom_utama" class="col-lg-9"> <!-- START kolom_utama -->
			<div class="bs-example bs-example-tabs">
		      <ul id="panel_tabs" class="nav nav-tabs">
		        <li><a href="#form_lama" data-toggle="tab">Existing</a></li>
		        <li class="active"><a href="#form_desain" data-toggle="tab">Desain</a></li>
		        <li><a href="#form_entry" data-toggle="tab">Final</a></li>
		      </ul>
		    </div>
			<div id="myTabContent" class="tab-content"><!--START tab_content -->
			        <div class="tab-pane" id="form_lama"><!--START tab -->
			        	<div id="panel_form_lama" class="panel panel-default"> <!--START panel_form -->
						  	<div class="panel-heading">
						    	<h3 class="panel-title"><?php echo $form_title_1;?></h3>
						  	</div>
							<div class="panel-body">
							    <?php
							    	$this->load->view($module.'/'.$form_file);
							    ?>
							</div>
						</div><!--panel_form END-->
					</div><!--tab END-->
			        <div class="tab-pane fade in active" id="form_desain"><!--START tab -->
				          	<div id="panel_form_desain" class="panel panel-default"> <!--START panel_form -->
								<div class="panel-heading">
								    <h3 class="panel-title"><?php echo $form_title_2;?></h3>
								</div>
								<div class="panel-body">
								    <?php
								    	if( empty($mockup_url) && empty($module_1) )
								    	{
								    		echo "<div class='alert alert-info'>In progress..</div>";
								    	}else{
								    		if( empty($module_1) )
								    		{
								    			echo "<img src='".$mockup_url."' class='img-thumbnail' />";
								    		}else
								    		{
								    			echo Modules::run($module_1);
								    		}
								    		
								    	}
								    ?>
								</div>
							</div><!--panel_form END-->
				    </div><!--tab END-->
			        <div class="tab-pane fade" id="form_entry"><!--START tab -->
				          	<div id="panel_form_final" class="panel panel-default"> <!--START panel_form -->
								<div class="panel-heading">
								    <h3 class="panel-title"><?php echo $form_title_3;?></h3>
								</div>
								<div class="panel-body">
								    <?php
								    	if(empty($module_2))
								    	{
								    		echo "<div class='alert alert-info'>In progress..</div>";
								    	}else{
								    		$this->load->view($module_2);
								    	}
								    ?>
								</div>
							</div><!--panel_form END-->
				    </div><!--tab END-->
			</div><!--tab_content END -->
		</div><!-- kolom_utama END -->
</div>

<script>
	$('#panel_tabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})
</script>