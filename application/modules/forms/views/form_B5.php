<form action="http://pippnew.djpt.kkp.go.id/dataentry/index.php/jurnal_b5/input" id="form_entry" method="post" accept-charset="utf-8"><div style="display:none">
<input type="hidden" name="id_pelabuhan" value="020301">
</div>
<div id="pilih_tanggal">
    <table class="tanggal">
        <tbody><tr>
            <td class="tengah"><label for="tahun">Tahun</label>
            <select size="1" name="tahun" id="opsi_tahun">
        	<option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option>            </select></td>
            
           <td class="tengah"> <label for="bulan">Bulan</label>
            <select size="1" name="bulan" id="opsi_bulan">
        	<option value="1">Januari</option><option value="2">Pebruari</option><option value="3">Maret</option><option value="4">April</option><option value="5">Mei</option><option value="6">Juni</option><option value="7">Juli</option><option value="8">Agustus</option><option value="9">September</option><option value="10">Oktober</option><option value="11">November</option><option value="12">Desember</option>            </select>
            
        </td></tr>    
    </tbody></table> 
    <br>
    <p id="tanggal_selected"></p>   
</div>
<table id="tabel_b5" border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>Nama Fasilitas</th><th>Kondisi</th><th>Keterangan</th></tr>
</thead>
<tbody>
<tr>
<td><strong>Fasilitas Pokok</strong></td><td></td><td></td></tr>
<tr>
<td><strong>Fasilitas Pelindung</strong></td><td></td><td></td></tr>
<tr>
<td>Pemecah Gelombang (Breakwater)</td><td><select name="3">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_3" value="" style="width: 180px;"></td></tr>
<tr>
<td>Dinding Penahan Tanah (Revetment)</td><td><select name="4">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_4" value="" style="width: 180px;"></td></tr>
<tr>
<td>Groin</td><td><select name="5">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_5" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Tambat</strong></td><td></td><td></td></tr>
<tr>
<td>Dermaga</td><td><select name="7">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_7" value="" style="width: 180px;"></td></tr>
<tr>
<td>Jetty</td><td><select name="8">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_8" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Perairan</strong></td><td></td><td></td></tr>
<tr>
<td>Kolam Pelabuhan</td><td><select name="10">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_10" value="" style="width: 180px;"></td></tr>
<tr>
<td>Alur Pelayaran</td><td><select name="11">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_11" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Penghubung</strong></td><td></td><td></td></tr>
<tr>
<td>Jalan</td><td><select name="13">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_13" value="" style="width: 180px;"></td></tr>
<tr>
<td>Drainase Terbuka</td><td><select name="14">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_14" value="" style="width: 180px;"></td></tr>
<tr>
<td>Drainase Tertutup</td><td><select name="15">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_15" value="" style="width: 180px;"></td></tr>
<tr>
<td>Jembatan</td><td><select name="16">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_16" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Pembatas Lahan</strong></td><td></td><td></td></tr>
<tr>
<td>Pagar Keliling</td><td><select name="18">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_18" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Fungsional</strong></td><td></td><td></td></tr>
<tr>
<td><strong>Fasilitas Pemasaran Hasil Perikanan</strong></td><td></td><td></td></tr>
<tr>
<td>Tempat Pelelangan Ikan</td><td><select name="21">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_21" value="" style="width: 180px;"></td></tr>
<tr>
<td>Pasar Ikan</td><td><select name="22">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_22" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Navigasi Pelayaran dan Komunikasi</strong></td><td></td><td></td></tr>
<tr>
<td>Telepon</td><td><select name="24">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_24" value="" style="width: 180px;"></td></tr>
<tr>
<td>Radio SSB </td><td><select name="25">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_25" value="" style="width: 180px;"></td></tr>
<tr>
<td>Internet</td><td><select name="26">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_26" value="" style="width: 180px;"></td></tr>
<tr>
<td>Rambu-rambu</td><td><select name="27">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_27" value="" style="width: 180px;"></td></tr>
<tr>
<td>Lampu Suar</td><td><select name="28">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_28" value="" style="width: 180px;"></td></tr>
<tr>
<td>Menara Pengawas</td><td><select name="29">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_29" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Suplai Air Bersih</strong></td><td></td><td></td></tr>
<tr>
<td>Sumber Air Bersih</td><td><select name="31">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_31" value="" style="width: 180px;"></td></tr>
<tr>
<td>Water Treatment</td><td><select name="32">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_32" value="" style="width: 180px;"></td></tr>
<tr>
<td>Penampung Air</td><td><select name="33">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_33" value="" style="width: 180px;"></td></tr>
<tr>
<td>Fire Safety/Hydrant</td><td><select name="34">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_34" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Es</strong></td><td></td><td></td></tr>
<tr>
<td>Pabrik Es</td><td><select name="36">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_36" value="" style="width: 180px;"></td></tr>
<tr>
<td>Gudang Es</td><td><select name="37">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_37" value="" style="width: 180px;"></td></tr>
<tr>
<td>Mesin Penghancur Es</td><td><select name="38">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_38" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Listrik</strong></td><td></td><td></td></tr>
<tr>
<td>Genset</td><td><select name="40">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_40" value="" style="width: 180px;"></td></tr>
<tr>
<td>Rumah Genset</td><td><select name="41">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_41" value="" style="width: 180px;"></td></tr>
<tr>
<td>Instalasi Listrik</td><td><select name="42">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_42" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Bahan Bakar</strong></td><td></td><td></td></tr>
<tr>
<td>SPBN</td><td><select name="44">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_44" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tangki BBM</td><td><select name="45">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_45" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Pemeliharaan Kapal dan Alat Penangkap Ikan</strong></td><td></td><td></td></tr>
<tr>
<td>Docking</td><td><select name="47">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_47" value="" style="width: 180px;"></td></tr>
<tr>
<td>Bengkel</td><td><select name="48">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_48" value="" style="width: 180px;"></td></tr>
<tr>
<td>Penjemuran Jaring</td><td><select name="49">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_49" value="" style="width: 180px;"></td></tr>
<tr>
<td>Perbaikan Jaring</td><td><select name="50">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_50" value="" style="width: 180px;"></td></tr>
<tr>
<td>Gudang Peralatan</td><td><select name="51">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_51" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Penanganan dan Pengolahan Hasil Perikanan</strong></td><td></td><td></td></tr>
<tr>
<td>Lab. Pembinaan dan Pengujian Mutu Hasil Perikanan</td><td><select name="53">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_53" value="" style="width: 180px;"></td></tr>
<tr>
<td>Gedung Packing</td><td><select name="54">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_54" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Pengolahan Ikan</td><td><select name="55">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_55" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Penyimpanan Ikan Segar</td><td><select name="56">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_56" value="" style="width: 180px;"></td></tr>
<tr>
<td>Cold Storage</td><td><select name="57">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_57" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Pendaratan Tuna</td><td><select name="58">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_58" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Penanganan Tuna</td><td><select name="59">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_59" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Perkantoran</strong></td><td></td><td></td></tr>
<tr>
<td>Kantor Administrasi Pelabuhan</td><td><select name="61">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_61" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kantor Syahbandar</td><td><select name="62">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_62" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kantor Pengawas Perikanan</td><td><select name="63">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_63" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Transportasi</strong></td><td></td><td></td></tr>
<tr>
<td>Kendaraan Dinas Roda 4</td><td><select name="65">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_65" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kendaraan Dinas Roda 2</td><td><select name="66">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_66" value="" style="width: 180px;"></td></tr>
<tr>
<td>Garasi Alat Berat</td><td><select name="67">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_67" value="" style="width: 180px;"></td></tr>
<tr>
<td>Garasi Mobil</td><td><select name="68">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_68" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Parkir</td><td><select name="69">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_69" value="" style="width: 180px;"></td></tr>
<tr>
<td>Alat Berat</td><td><select name="70">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_70" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kapal Pengawas Perikanan</td><td><select name="71">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_71" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kapal Keruk</td><td><select name="72">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_72" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kapal Tunda</td><td><select name="73">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_73" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Pengolahan Limbah</strong></td><td></td><td></td></tr>
<tr>
<td>Instalasi Pengolah Limbah (IPAL)</td><td><select name="75">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_75" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Pembuangan Sampah</td><td><select name="76">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_76" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Penunjang</strong></td><td></td><td></td></tr>
<tr>
<td><strong>Fasilitas Pembinaan Nelayan</strong></td><td></td><td></td></tr>
<tr>
<td>Balai Pertemuan Nelayan</td><td><select name="79">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_79" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Pengelola Pelabuhan</strong></td><td></td><td></td></tr>
<tr>
<td>Rumah Karyawan</td><td><select name="81">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_81" value="" style="width: 180px;"></td></tr>
<tr>
<td>Mess Karyawan</td><td><select name="82">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_82" value="" style="width: 180px;"></td></tr>
<tr>
<td>Pos Jaga</td><td><select name="83">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_83" value="" style="width: 180px;"></td></tr>
<tr>
<td>Pos Pelayanan Terpadu</td><td><select name="84">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_84" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Sosial dan Umum</strong></td><td></td><td></td></tr>
<tr>
<td>Tempat Penginapan Nelayan</td><td><select name="86">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_86" value="" style="width: 180px;"></td></tr>
<tr>
<td>Guest House</td><td><select name="87">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_87" value="" style="width: 180px;"></td></tr>
<tr>
<td>Tempat Peribadatan</td><td><select name="88">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_88" value="" style="width: 180px;"></td></tr>
<tr>
<td>Klinik Kesehatan</td><td><select name="89">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_89" value="" style="width: 180px;"></td></tr>
<tr>
<td>MCK</td><td><select name="90">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_90" value="" style="width: 180px;"></td></tr>
<tr>
<td>Kios Nelayan/Toko/Waserda</td><td><select name="91">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_91" value="" style="width: 180px;"></td></tr>
<tr>
<td>Warnet</td><td><select name="92">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_92" value="" style="width: 180px;"></td></tr>
<tr>
<td>Wartel</td><td><select name="93">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_93" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Penunjang Ekonomi</strong></td><td></td><td></td></tr>
<tr>
<td>Bank Pemerintah</td><td><select name="95">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_95" value="" style="width: 180px;"></td></tr>
<tr>
<td>Bank Swasta</td><td><select name="96">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_96" value="" style="width: 180px;"></td></tr>
<tr>
<td>Koperasi</td><td><select name="97">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_97" value="" style="width: 180px;"></td></tr>
<tr>
<td>Lembaga Ekonomi Masyarakat Pesisir</td><td><select name="98">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_98" value="" style="width: 180px;"></td></tr>
<tr>
<td><strong>Fasilitas Kios IPTEK</strong></td><td></td><td></td></tr>
<tr>
<td>Kios IPTEK</td><td><select name="100">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_100" value="" style="width: 180px;"></td></tr>
<tr>
<td>Showroom Hasil Produk Perikanan</td><td><select name="101">
<option value="0">Belum dimiliki.</option>
<option value="1">Sudah berfungsi dalam keadaan baik.</option>
<option value="2">Sudah berfungsi dalam keadaan rusak.</option>
<option value="3">Dalam keadaan baik tapi belum berfungsi.</option>
<option value="4">Dalam keadaan rusak dan belum berfungsi.</option>
</select></td><td><input type="text" name="ket_101" value="" style="width: 180px;"></td></tr>
</tbody>
</table><button class="buttonKirim" type="submit" name="KIRIM"> KIRIM </button></form>