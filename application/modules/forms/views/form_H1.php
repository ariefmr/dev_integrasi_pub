<table id="detail_kapal_h1" class="table table-hover" style="width:60%;" border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td>Domisili Kapal</td><td><div class="btn-group" data-toggle="buttons-radio"><button type="button" class="btn domisili_kapal lokal active" value="lokal">Lokal</button><button type="button" class="btn domisili_kapal luar" value="luar">Luar Pelabuhan</button></div></td></tr>
<tr>
<tr>
<td>Nama Kapal</td><td><p id="nama_kapal">ADHI MINA PERKASA</p></td></tr>
<tr>
<td>Pemilik Kapal</td><td><input type="text" name="" value="SUMINTO AD TP" disabled=""></td></tr>
<tr>
<td>Tonase / Nomor Selar / Domisili</td><td><input type="text" name="" value="92 GT / 283/Fp / PPN Pekalongan" disabled=""></td></tr>
<tr>
<td>Nama Nahkoda</td><td><input type="text" name="nama_nahkoda" value=""></td></tr>
<tr>
<td>Jumlah ABK</td><td>WNI <input type="text" name="jml_abk_wni" value="0" style="width: 50px;"> <i>Orang</i> &nbsp; &nbsp;WNA <input type="text" name="jml_abk_wna" value="0" style="width: 50px;"> <i>Orang</i></td></tr>
<tr>
<td>Ukuran Kapal</td><td><i class="divider-vertical">P</i> <input type="text" name="" value="19.45" style="width: 50px;" disabled=""> m &nbsp;<i class="divider-vertical">L</i> <input type="text" name="" value="7.1" style="width: 50px;" disabled=""> m &nbsp;<i class="divider-vertical">D </i><input type="text" name="" value="2.1" style="width: 50px;" disabled=""> m &nbsp;<i class="divider-vertical">DR </i><input type="text" name="" value="0" style="width: 50px;" disabled=""> m &nbsp;</td></tr>
<tr>
<td>Merk Mesin / Besar PK</td><td><input type="text" name="" value="NISSAN / 280" disabled=""></td></tr>
<tr>
<td>Alat Tangkap Utama</td><td><select name="id_alat_tangkap">
<option value="0">Kosong</option>
<option value="1">Pukat tarik udang ganda</option>
<option value="2">Otter Trawl</option>
<option value="3">Pukat tarik ikan</option>
<option value="4">Payang (tmsk. Lampara)</option>
<option value="5">Dogol (tmsk. Lampara dasar, cantrang)</option>
<option value="6">Pukat Pantai (Jaring arad)</option>
<option value="26">Perangkap lainnya</option>
<option value="7" selected="selected">Pukat Cincin</option>
<option value="8">Jaring insang hanyut</option>
<option value="9">Jaring insang lingkar</option>
<option value="10">Jaring klitik</option>
<option value="11">Jaring insang tetap</option>
<option value="12">Jaring tiga lapis</option>
<option value="13">Bagan perahu/rakit</option>
<option value="14">Bagan tancap</option>
<option value="15">Serok dan songko</option>
<option value="16">Jaring Angkat Lainnya</option>
<option value="17">Rawai tuna</option>
<option value="18">Rawai hanyut lainnya selain rawai tuna</option>
<option value="19">Rawai tetap</option>
<option value="20">Huhate</option>
<option value="21">Pancing tonda</option>
<option value="22">Pancing ulur</option>
<option value="23">Sero (tmsk. Kelong)</option>
<option value="24">Jermal</option>
<option value="25">Bubu (tmsk. Bubu ambai)</option>
<option value="27">Alat penangkap kerang</option>
<option value="28">Alat pengumpul rumput laut</option>
<option value="29">Muroami</option>
<option value="30">Jala tebar</option>
<option value="31">Tidak Ada Alat Tangkap</option>
<option value="32">Pukat tarik berbingkai</option>
<option value="33">Anco</option>
<option value="34">Rawai dasar tetap</option>
<option value="35">Pancing tegak</option>
<option value="36">Pancing cumi</option>
<option value="37">Pancing lainnya</option>
<option value="38">Alat penangkap teripang (ladung)</option>
<option value="39">Alat penangkap kepiting</option>
<option value="40">Garpu dan Tombak</option>
<option value="41">Pukat tarik udang tunggal</option>
<option value="42">Pukat cincin satu kapal</option>
<option value="43">Pukat Cincin Pelagis Besar dengan satu kapal</option>
<option value="44">Jaring lingkar tanpa tali kerut/Lampara</option>
<option value="45">Pukat hela dasar berpalang</option>
<option value="46">Pukat hela dasar berpapan</option>
<option value="47">Pukat hela dasar dua kapal</option>
<option value="48">Pukat hela dasar lainnya</option>
<option value="49">Pukat hela dasar udang</option>
<option value="50">Pukat hela pertengahan berpapan</option>
<option value="51">Puka hela pertengahan dua kapal</option>
<option value="52">Pukat hela pertengahan udang</option>
<option value="53">Pukat hela kembar berpapan</option>
<option value="54">Pukat hela lainnya</option>
<option value="55">Pukat dorong</option>
<option value="56">Boke Ami</option>
<option value="57">Jala jatuh berkapal</option>
<option value="58">Jala tebar lainnya</option>
<option value="59">Jaring liong bun</option>
<option value="60">Jaring insang oseanik</option>
<option value="61">Jaring insang berlapis</option>
<option value="62">Jaring insang kombinasi dengan tramel net</option>
<option value="63">Jaring insang lainnya</option>
<option value="64">Set Net</option>
<option value="65">Bubu bersayap</option>
<option value="66">Pukat labuh</option>
<option value="67">Togo</option>
<option value="68">Ambai</option>
<option value="69">Pengerih</option>
<option value="70">Perangkap ikan peloncat</option>
<option value="71">Seser</option>
<option value="72">Pancing berjoran</option>
<option value="73">Squid angling</option>
<option value="74">Huhate Mekanis</option>
<option value="75">Pancing layang-layang</option>
<option value="76">Ladung</option>
<option value="77">Panah</option>
<option value="78">Alat penjepit dan melukai lainnya</option>
<option value="79">Ladung</option>
<option value="80">Panah</option>
<option value="81">Alat penjepit dan melukai lainnya</option>
<option value="82">Alat tangkap tes</option>
<option value="83">catchoz</option>
</select></td></tr>
<tr>
<td>Alat Bantu Penangkapan</td><td>1. <select name="id_alat_bantu_1">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20" selected="selected">Lampu</option>
</select><br>2. <select name="id_alat_bantu_2">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19" selected="selected">Rumpon</option>
<option value="20">Lampu</option>
</select><br>3. <select name="id_alat_bantu_3">
<option value="0" selected="selected">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select></td></tr>
<tr>
<td>WPP</td><td><select name="id_wpp">
<option value="0">Kosong</option>
<option value="01">Selat Malaka</option>
<option value="02">Laut Cina Selatan</option>
<option value="03">Laut Jawa</option>
<option value="04" selected="selected">Laut Flores dan Selat Makassar</option>
<option value="05">Laut Banda</option>
<option value="06">Laut Arafura</option>
<option value="07">Laut Maluku</option>
<option value="08">Laut Sulawesi dan Samudera Pasifik</option>
<option value="09">Samudera Hindia</option>
<option value="10">WPP-RI 571</option>
<option value="11">WPP-RI 572</option>
<option value="12">WPP-RI 573</option>
<option value="13">WPP-RI 711</option>
<option value="14">WPP-RI 712</option>
<option value="15">WPP-RI 713</option>
<option value="16">WPP-RI 714</option>
<option value="17">WPP-RI 715</option>
<option value="18">WPP-RI 716</option>
<option value="19">WPP-RI 717</option>
<option value="20">WPP-RI 718</option>
</select></td></tr>
<tr>
<td>DPI</td><td><select name="id_dpi">
<option value="0">Kosong</option>
<option value="0401" selected="selected">WPP Laut Flores dan dan Selat Makasar 1</option>
<option value="0402">WPP Laut Flores dan dan Selat Makasar 2</option>
<option value="0403">WPP Laut Flores dan dan Selat Makasar 3</option>
<option value="0404">WPP Laut Flores dan Selat Makassar 4</option>
</select></td></tr>
<tr>
<td>Jumlah Hari Trip</td><td><input type="text" name="jml_hari_trip" value="0" style="width: 50px;"></td></tr>
<tr>
<td>Jumlah Tebaran Per Trip</td><td><input type="text" name="jml_tebaran_trip" value="0" style="width: 50px;"></td></tr>
<tr>
<td>Pelabuhan Asal Keberangkatan</td><td><select id="id_pelabuhan_asal" name="id_pelabuhan_asal"><option value="">Pilih pelabuhan</option><option value="150604">PPI Sape</option><option value="141901">PPP Asem Doyong</option><option value="190104">PPI Batulicin</option><option value="080901">PPP Lempasing</option><option value="110312">PPP Tegalsari</option><option value="120801">PPP Eretan</option><option value="121301">PPP Blanakan</option><option value="130209">PPN Brondong</option><option value="140801">PPP Morodemak</option><option value="141701">PPP Bajomulyo</option><option value="160201">PPP Sadeng</option><option value="210102">PPP Dagho</option><option value="210701">PPS Bitung</option><option value="111775">PPS Cilacap</option><option value="240101">PPS Kendari</option><option value="100402">PP BARELANG</option><option value="020301">PPS Belawan</option><option value="250601">PPN Tual</option><option value="101401">PPN Kejawanan</option><option value="140303">PPN Pengambengan</option><option value="150205">PPP MUNCAR</option><option value="141301">PPP Tawang</option><option value="151405">PPP Pondokdadap</option><option value="111145">PPP Karimunjawa</option><option value="140304">PPP Klidang Lor</option><option value="141801">PPP Wonokerto</option><option value="240303">Pelabuhan Perikanan Pantai Kwandang</option><option value="132101">PPN Prigi</option><option value="290301">PPN Karangantu</option><option value="030301">PPS Bungus</option><option value="170901">PPN Pemangkat</option><option value="170207">PPP Teluk Batang</option><option value="090101">PPS Jakarta</option><option value="110623" selected="">PPN Pekalongan</option><option value="250501">PPN Ambon</option><option value="270102">PPN Sungai Liat</option><option value="270201">PPN Tanjung Pandan</option><option value="152202">UPPPP Mayangan</option><option value="160901">PPP Kupang</option><option value="010401">PPP Lampulo</option><option value="190301">PPP Banjarmasin</option><option value="020802">PPN Sibolga</option><option value="150101">PPP Labuhan Lombok</option><option value="132102">PPP Tamperan</option><option value="170223">PPP Sungai Rengas</option><option value="280101">PPN Ternate</option><option value="030205">PPP Sikakap</option><option value="180501">PPI Kumai</option><option value="121302">PPP Muara Ciasem</option><option value="200501">PPI Manggar</option><option value="102101">PPN Palabuhan Ratu</option><option value="190201">PPI Muara Kintap</option><option value="142201">PPP Tasikagung Rembang</option><option value="090102">PPI Muara Angke</option><option value="110203">PPP LABUAN BANTEN</option><option value="231001">UPTD.PPI Paotere</option><option value="270401">PPI Donggala</option></select></td></tr>
<tr>
<td>Maksud Kunjungan</td><td><input type="checkbox" name="is_bongkar" value="t" checked="checked"> <i>Bongkar</i> <input type="checkbox" name="is_tambat" value="t"> <i>Tambat</i> <input type="checkbox" name="is_labuh" value="t"> <i>Labuh</i> <input type="checkbox" name="is_dock" value="t"> <i>Docking</i> <input type="checkbox" name="is_perbaikan" value="t"> <i>Floating Repair</i> <input type="checkbox" name="is_perbekalan" value="t"> <i>Perbekalan</i> <input type="checkbox" name="is_muat_ikan" value="t"> <i>Muat Ikan</i> </td></tr>
<tr>
<td>Posisi Tambat Labuh</td><td><input type="text" name="posisi_tambat_labuh" value=""></td></tr>
</tbody>
</table>