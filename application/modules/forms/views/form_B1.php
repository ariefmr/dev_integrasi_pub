<form action="http://localhost/data-entry/index.php/jurnal_b1/input" id="form_entry" method="post" accept-charset="utf-8">
<div id="pilih_tanggal">
    <table class="tanggal">
        <tbody><tr>
            <td class="tengah"><label for="tahun">Tahun</label>
            <select size="1" name="tahun" id="opsi_tahun">
        	<option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option>            </select></td>
            
           <td class="tengah"> <label for="bulan">Bulan</label>
            <select size="1" name="bulan" id="opsi_bulan">
        	<option value="1">Januari</option><option value="2">Pebruari</option><option value="3">Maret</option><option value="4">April</option><option value="5">Mei</option><option value="6">Juni</option><option value="7">Juli</option><option value="8">Agustus</option><option value="9">September</option><option value="10">Oktober</option><option value="11">November</option><option value="12">Desember</option>            </select>
            
        </td></tr>    
    </tbody></table> 
    <p id="tanggal_selected"></p>   
</div>

<div id="display_table">
    <table id="tabel_b1" border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>No.</th><th>Item</th><th>Satuan</th><th>Jumlah</th></tr>
</thead>
<tbody>
<tr>
<td>1. </td><td>Pungutan Pengusahaan Perikanan</td><td>Rp</td><td><input type="text" name="1" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Pungutan Perikanan Asing</td><td>Rp</td><td><input type="text" name="2" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Jasa Tambat Labuh</td><td>Rp</td><td><input type="text" name="3" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Jasa Pengadaan Es</td><td>Rp</td><td><input type="text" name="4" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>5. </td><td>Jasa Pengadaan Air</td><td>Rp</td><td><input type="text" name="5" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>6. </td><td>Jasa Sewa Cool Room</td><td>Rp</td><td><input type="text" name="6" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Jasa Alat-alat, Slipway/Dock</td></tr>
<tr>
<td colspan="4" class="highlight">Alat-alat</td></tr>
<tr>
<td>1. </td><td>Kapal Inspeksi</td><td>Rp</td><td><input type="text" name="9" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Kapal Keruk</td><td>Rp</td><td><input type="text" name="10" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Kapal Tunda</td><td>Rp</td><td><input type="text" name="11" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Forklift</td><td>Rp</td><td><input type="text" name="12" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>5. </td><td>Crane Truck</td><td>Rp</td><td><input type="text" name="13" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>6. </td><td>Dump Truck</td><td>Rp</td><td><input type="text" name="14" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>7. </td><td>Tangki BBM &amp; Instalasi</td><td>Rp</td><td><input type="text" name="15" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>8. </td><td>Gerobak/Keranjang/Tray</td><td>Rp</td><td><input type="text" name="16" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>9. </td><td>Alat Komunikasi</td><td>Rp</td><td><input type="text" name="17" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>10. </td><td>Slipway/Dock</td><td>Rp</td><td><input type="text" name="18" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>11. </td><td>Pelayanan Bengkel</td><td>Rp</td><td><input type="text" name="19" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>12. </td><td>Pemasangan Papan Reklame</td><td>Rp</td><td><input type="text" name="20" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Tanah Pelabuhan</td></tr>
<tr>
<td>1. </td><td>Biaya Pengembangan</td><td>Rp</td><td><input type="text" name="23" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Sumbangan Pemeliharaan</td><td>Rp</td><td><input type="text" name="24" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Bangunan Pelabuhan</td></tr>
<tr>
<td>1. </td><td>Bangunan Sementara</td><td>Rp</td><td><input type="text" name="26" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Bangunan Semi Permanen</td><td>Rp</td><td><input type="text" name="27" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Bangunan Permanen</td><td>Rp</td><td><input type="text" name="28" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Penggunaan Tanah</td></tr>
<tr>
<td>1. </td><td>Lapangan Penjemuran</td><td>Rp</td><td><input type="text" name="30" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Penumpukan Barang</td><td>Rp</td><td><input type="text" name="31" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Kebersihan</td></tr>
<tr>
<td>1. </td><td>Bangunan Permanen Tertutup</td><td>Rp</td><td><input type="text" name="34" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Perkantoran/Pertokoan</td><td>Rp</td><td><input type="text" name="35" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Gudang Ikan/TPI</td><td>Rp</td><td><input type="text" name="36" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Tempat Pemasaran Ikan</td><td>Rp</td><td><input type="text" name="37" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>5. </td><td>Tempat Pengepakan Ikan</td><td>Rp</td><td><input type="text" name="38" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>6. </td><td>Warung Makan/Kios</td><td>Rp</td><td><input type="text" name="39" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>7. </td><td>Pengepakan Ikan di luar</td><td>Rp</td><td><input type="text" name="40" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>8. </td><td>Kendaraan Bongkar Muat</td><td>Rp</td><td><input type="text" name="41" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>9. </td><td>Truck/Truck Tangki</td><td>Rp</td><td><input type="text" name="42" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>10. </td><td>Pick Up</td><td>Rp</td><td><input type="text" name="43" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>11. </td><td>Gerobak/Roda Tiga</td><td>Rp</td><td><input type="text" name="44" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>12. </td><td>Kebersihan Kolam Pelabuhan</td><td>Rp</td><td><input type="text" name="45" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>13. </td><td>Jasa Instalasi Pengolahan Air Limbah</td><td>Rp</td><td><input type="text" name="46" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>14. </td><td>Jasa Pengambilan Air Laut Bersih</td><td>Rp</td><td><input type="text" name="47" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Jasa Penggunaan Fasilitas</td></tr>
<tr>
<td>1. </td><td>Guest House/Mess/Wisma</td><td>Rp</td><td><input type="text" name="49" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Asrama</td><td>Rp</td><td><input type="text" name="50" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Aula/Ruang Rapat</td><td>Rp</td><td><input type="text" name="51" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Pas Masuk</td></tr>
<tr>
<td colspan="4" class="highlight">Pas Harian</td></tr>
<tr>
<td>1. </td><td>Orang </td><td>Rp</td><td><input type="text" name="54" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Sepeda/Becak/Gerobak</td><td>Rp</td><td><input type="text" name="55" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Sepeda Motor</td><td>Rp</td><td><input type="text" name="56" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Roda Tiga Bermotor</td><td>Rp</td><td><input type="text" name="57" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>5. </td><td>Mobil Penumpang/Umum</td><td>Rp</td><td><input type="text" name="58" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>6. </td><td>Truk/Bus</td><td>Rp</td><td><input type="text" name="59" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>7. </td><td>Trailer/Gandeng/Container</td><td>Rp</td><td><input type="text" name="60" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>8. </td><td>Stiker</td><td>Rp</td><td><input type="text" name="61" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Parkir</td></tr>
<tr>
<td>1. </td><td>Sepeda Motor</td><td>Rp</td><td><input type="text" name="63" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Mobil Penumpang</td><td>Rp</td><td><input type="text" name="64" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Bus/Truk</td><td>Rp</td><td><input type="text" name="65" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Trailer/Gandeng/Container</td><td>Rp</td><td><input type="text" name="66" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td colspan="4" class="highlight">Jasa Kebersihan Pelabuhan</td></tr>
<tr>
<td colspan="4" class="highlight">Jasa Tanah dan Bangunan</td></tr>
</tbody>
</table><br><table id="tabel_b1_non" border="0" cellpadding="4" cellspacing="0">
<thead>
<tr>
<th>No.</th><th>Item</th><th>Satuan</th><th>Jumlah</th></tr>
</thead>
<tbody>
<tr>
<td>1. </td><td>Retribusi Lelang</td><td>Rp</td><td><input type="text" name="1_non" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>2. </td><td>Persentase Retribusi Lelang</td><td>%</td><td><input type="text" name="2_non" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>3. </td><td>Persentase Biaya Pemeliharaan TPI dr Retribusi Lelang</td><td>%</td><td><input type="text" name="3_non" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"></td></tr>
<tr>
<td>4. </td><td>Pendapatan jasa lainnya</td><td>Rp</td><td><input type="text" name="4_non" value="0" style="width: 80px;" title="Data yang dimasukan Harus Angka"><textarea name="desk_lain" cols="40" rows="10" placeholder="Deskripsi pendapatan jasa lain."></textarea></td></tr>
</tbody>
</table><button class="buttonKirim" type="submit" name="KIRIM">KIRIM</button></div></form>