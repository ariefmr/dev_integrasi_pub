<!-- TAMPIL DATA -->
<div class="row">
        <div class="col-lg-3">
        	<div class="panel panel-primary">
			  <!-- Default panel contents -->
				<div class="panel-heading">Daftar Form Entry</div>
					  <div class="panel-body">
					    <p>Penjelasan Form Entry Aktifitas Kapal di Pelabuhan</p>
				</div>
	        	<ul class="list-group">
				 	<li class="list-group-item">
	          			H1 - Kapal Masuk
	        		</li>
	        		<li class="list-group-item">
	          			S2 - Kapal
	        		</li>
	        		<li class="list-group-item">
	          			S4 - Alat Penangkap Ikan
	        		</li>
	        		<li class="list-group-item">
	          			B2 - Operasional Perbekalan Dari Dalam Dan Luar Pelabuhan
	        		</li>
	        		<li class="list-group-item">
	          			H5 - Kapal Keluar
	        		</li>
	      		</ul>
      		</div>
		</div>
		<div class="col-lg-9">
        	<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Form H1</h3>
			  </div>
			  <div class="panel-body">
			    Deskripsi Form H1.
			  </div>
			</div>
		</div>
</div>