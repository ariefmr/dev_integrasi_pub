
<table border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td>Jenis / Sifat </td><td>: Kapal di Pelabuhan / Situasional</td></tr>
<tr>
<td>Sumber Data </td><td>: Perusahaan / Perorangan / Pemilik Kapal</td></tr>
</tbody>
</table>
<br>

<table id="tabel_entry_kapal" border="0" cellpadding="4" cellspacing="0">
<tbody>
<tr>
<td>Nama Kapal</td><td><input type="text" name="nama_kapal" value="" title="" data-original-title="Diisi dengan nama kapal sesuai surat-surat kelengkapan kapal (SPI atau surat lainnya)|"> <sup> * wajib isi.</sup></td></tr>
<tr>
<td>Bendera</td><td><input type="text" name="bendera" value="" placeholder="INDONESIA" data-original-title="" title=""></td></tr>
<tr>
<td>Fungsi Kapal</td><td><select name="fungsi_kapal" title="Diisi dengan fungsi utama kapal sesuai SPI yaitu penangkap ikan atau pengangkut">
<option value="Penangkap Ikan">Penangkap Ikan</option>
<option value="Pengangkut">Pengangkut Ikan</option>
</select></td></tr>
<tr>
<td>Jenis Kapal</td><td><select name="id_kateg_kapal" title="Diisi dengan jenis kapal motor atau motor tempel atau kapal tanpa motor|">
<option value="1">Motor tempel</option>
<option value="3">Perahu tanpa motor</option>
<option value="2">Kapal motor</option>
</select></td></tr>
<tr>
<td>Tonase Kapal (GT)</td><td><input type="text" name="tonase" value="" title="" data-original-title="Diisi dengan ukuran tonase kapal (GT)|"> <sup> * wajib isi.</sup></td></tr>
<tr>
<td>Tanda Selar</td><td><input type="text" name="tanda_selar" value="" title="" data-original-title="Diisi dengan tanda selar yang terdapat pada kapal"> <sup> * wajib isi.</sup></td></tr>
<tr>
<td>Nomor SPI</td><td><input type="text" name="no_spi" value="" title="" data-original-title="Diisi dengan nomor SPI"> <sup class="wajib30gt"> * &gt;30GT wajib isi.</sup></td></tr>
<tr>
<td>Tanggal SPI</td><td><input type="text" name="tgl_spi" value="" title="" data-format="yyyy-MM-dd" id="dp1377674438358" class="hasDatepicker" data-original-title="Diisi dengan tanggal dikeluarkannya SPI"> <sup class="wajib30gt"> * &gt;30GT wajib isi.</sup></td></tr>
<tr>
<td>Pemilik Kapal </td><td><input type="text" name="pemilik_kapal" value="" title="" data-original-title="Diisi dengan nama pemilik kapal (sesuai SPI)|"> <sup> * wajib isi.</sup></td></tr>
<tr>
<td>Domisili Kapal</td><td><select name="domisili_kapal">
<option value="150604">PPI Sape</option>
<option value="141901">PPP Asem Doyong</option>
<option value="190104">PPI Batulicin</option>
<option value="080901">PPP Lempasing</option>
<option value="110312">PPP Tegalsari</option>
<option value="120801">PPP Eretan</option>
<option value="121301">PPP Blanakan</option>
<option value="130209">PPN Brondong</option>
<option value="140801">PPP Morodemak</option>
<option value="160201">PPP Sadeng</option>
<option value="210102">PPP Dagho</option>
<option value="111775">PPS Cilacap</option>
<option value="240101">PPS Kendari</option>
<option value="100402">PP BARELANG</option>
<option value="020301">PPS Belawan</option>
<option value="101401">PPN Kejawanan</option>
<option value="140303">PPN Pengambengan</option>
<option value="150205">PPP MUNCAR</option>
<option value="141301">PPP Tawang</option>
<option value="250601">PPN Tual</option>
<option value="111145">PPP Karimunjawa</option>
<option value="140304">PPP Klidang Lor</option>
<option value="141801">PPP Wonokerto</option>
<option value="240303">PPP Kwandang</option>
<option value="210701">PPS Bitung</option>
<option value="141701">PPP Bajomulyo</option>
<option value="151405">PPP Pondokdadap</option>
<option value="132101">PPN Prigi</option>
<option value="290301">PPN Karangantu</option>
<option value="030301">PPS Bungus</option>
<option value="170901">PPN Pemangkat</option>
<option value="170207">PPP Teluk Batang</option>
<option value="090101">PPS Jakarta</option>
<option value="270102">PPN Sungai Liat</option>
<option value="270201">PPN Tanjung Pandan</option>
<option value="152202">UPPPP Mayangan</option>
<option value="160901">PPP Kupang</option>
<option value="020802">PPN Sibolga</option>
<option value="150101">PPP Labuhan Lombok</option>
<option value="190301">PPP Banjarmasin</option>
<option value="010401">PPP Lampulo</option>
<option value="250501">PPN Ambon</option>
<option value="110623">PPN Pekalongan</option>
<option value="132102">PPP Tamperan</option>
<option value="170223">PPP Sungai Rengas</option>
<option value="280101">PPN Ternate</option>
<option value="030205">PPP Sikakap</option>
<option value="180501">PPI Kumai</option>
<option value="121302">PPP Muara Ciasem</option>
<option value="200501">PPI Manggar</option>
<option value="102101">PPN Palabuhan Ratu</option>
<option value="190201">PPI Muara Kintap</option>
<option value="142201">PPP Tasikagung Rembang</option>
<option value="090102">PPI Muara Angke</option>
<option value="231001">UPTD.PPI Paotere</option>
<option value="110203">PPP LABUAN BANTEN</option>
<option value="270401">PPI Donggala</option>
</select></td></tr>
<tr>
<td>Domisili Daerah</td><td><input type="text" name="domisili_daerah" value="" title="" data-original-title="Diisi dengan nama daerah pelabuhan asal kapal"></td></tr>
<tr>
<td>Pelabuhan / Pangkalan Singgah</td><td><pre> 1.<select name="domisili_kapal">
<option value="0">Kosong</option>
<optgroup label="1">
<option value="150604">PPI Sape</option>
<option value="141901">PPP Asem Doyong</option>
<option value="190104">PPI Batulicin</option>
<option value="080901">PPP Lempasing</option>
<option value="110312">PPP Tegalsari</option>
<option value="120801">PPP Eretan</option>
<option value="121301">PPP Blanakan</option>
<option value="130209">PPN Brondong</option>
<option value="140801">PPP Morodemak</option>
<option value="160201">PPP Sadeng</option>
<option value="210102">PPP Dagho</option>
<option value="111775">PPS Cilacap</option>
<option value="240101">PPS Kendari</option>
<option value="100402">PP BARELANG</option>
<option value="020301">PPS Belawan</option>
<option value="101401">PPN Kejawanan</option>
<option value="140303">PPN Pengambengan</option>
<option value="150205">PPP MUNCAR</option>
<option value="141301">PPP Tawang</option>
<option value="250601">PPN Tual</option>
<option value="111145">PPP Karimunjawa</option>
<option value="140304">PPP Klidang Lor</option>
<option value="141801">PPP Wonokerto</option>
<option value="240303">PPP Kwandang</option>
<option value="210701">PPS Bitung</option>
<option value="141701">PPP Bajomulyo</option>
<option value="151405">PPP Pondokdadap</option>
<option value="132101">PPN Prigi</option>
<option value="290301">PPN Karangantu</option>
<option value="030301">PPS Bungus</option>
<option value="170901">PPN Pemangkat</option>
<option value="170207">PPP Teluk Batang</option>
<option value="090101">PPS Jakarta</option>
<option value="270102">PPN Sungai Liat</option>
<option value="270201">PPN Tanjung Pandan</option>
<option value="152202">UPPPP Mayangan</option>
<option value="160901">PPP Kupang</option>
<option value="020802">PPN Sibolga</option>
<option value="150101">PPP Labuhan Lombok</option>
<option value="190301">PPP Banjarmasin</option>
<option value="010401">PPP Lampulo</option>
<option value="250501">PPN Ambon</option>
<option value="110623">PPN Pekalongan</option>
<option value="132102">PPP Tamperan</option>
<option value="170223">PPP Sungai Rengas</option>
<option value="280101">PPN Ternate</option>
<option value="030205">PPP Sikakap</option>
<option value="180501">PPI Kumai</option>
<option value="121302">PPP Muara Ciasem</option>
<option value="200501">PPI Manggar</option>
<option value="102101">PPN Palabuhan Ratu</option>
<option value="190201">PPI Muara Kintap</option>
<option value="142201">PPP Tasikagung Rembang</option>
<option value="090102">PPI Muara Angke</option>
<option value="231001">UPTD.PPI Paotere</option>
<option value="110203">PPP LABUAN BANTEN</option>
<option value="270401">PPI Donggala</option>
</optgroup>
</select><br> 2.<select name="domisili_kapal2">
<option value="0">Kosong</option>
<optgroup label="1">
<option value="150604">PPI Sape</option>
<option value="141901">PPP Asem Doyong</option>
<option value="190104">PPI Batulicin</option>
<option value="080901">PPP Lempasing</option>
<option value="110312">PPP Tegalsari</option>
<option value="120801">PPP Eretan</option>
<option value="121301">PPP Blanakan</option>
<option value="130209">PPN Brondong</option>
<option value="140801">PPP Morodemak</option>
<option value="160201">PPP Sadeng</option>
<option value="210102">PPP Dagho</option>
<option value="111775">PPS Cilacap</option>
<option value="240101">PPS Kendari</option>
<option value="100402">PP BARELANG</option>
<option value="020301">PPS Belawan</option>
<option value="101401">PPN Kejawanan</option>
<option value="140303">PPN Pengambengan</option>
<option value="150205">PPP MUNCAR</option>
<option value="141301">PPP Tawang</option>
<option value="250601">PPN Tual</option>
<option value="111145">PPP Karimunjawa</option>
<option value="140304">PPP Klidang Lor</option>
<option value="141801">PPP Wonokerto</option>
<option value="240303">PPP Kwandang</option>
<option value="210701">PPS Bitung</option>
<option value="141701">PPP Bajomulyo</option>
<option value="151405">PPP Pondokdadap</option>
<option value="132101">PPN Prigi</option>
<option value="290301">PPN Karangantu</option>
<option value="030301">PPS Bungus</option>
<option value="170901">PPN Pemangkat</option>
<option value="170207">PPP Teluk Batang</option>
<option value="090101">PPS Jakarta</option>
<option value="270102">PPN Sungai Liat</option>
<option value="270201">PPN Tanjung Pandan</option>
<option value="152202">UPPPP Mayangan</option>
<option value="160901">PPP Kupang</option>
<option value="020802">PPN Sibolga</option>
<option value="150101">PPP Labuhan Lombok</option>
<option value="190301">PPP Banjarmasin</option>
<option value="010401">PPP Lampulo</option>
<option value="250501">PPN Ambon</option>
<option value="110623">PPN Pekalongan</option>
<option value="132102">PPP Tamperan</option>
<option value="170223">PPP Sungai Rengas</option>
<option value="280101">PPN Ternate</option>
<option value="030205">PPP Sikakap</option>
<option value="180501">PPI Kumai</option>
<option value="121302">PPP Muara Ciasem</option>
<option value="200501">PPI Manggar</option>
<option value="102101">PPN Palabuhan Ratu</option>
<option value="190201">PPI Muara Kintap</option>
<option value="142201">PPP Tasikagung Rembang</option>
<option value="090102">PPI Muara Angke</option>
<option value="231001">UPTD.PPI Paotere</option>
<option value="110203">PPP LABUAN BANTEN</option>
<option value="270401">PPI Donggala</option>
</optgroup>
</select><br> 3.<select name="domisili_kapal3">
<option value="0">Kosong</option>
<optgroup label="1">
<option value="150604">PPI Sape</option>
<option value="141901">PPP Asem Doyong</option>
<option value="190104">PPI Batulicin</option>
<option value="080901">PPP Lempasing</option>
<option value="110312">PPP Tegalsari</option>
<option value="120801">PPP Eretan</option>
<option value="121301">PPP Blanakan</option>
<option value="130209">PPN Brondong</option>
<option value="140801">PPP Morodemak</option>
<option value="160201">PPP Sadeng</option>
<option value="210102">PPP Dagho</option>
<option value="111775">PPS Cilacap</option>
<option value="240101">PPS Kendari</option>
<option value="100402">PP BARELANG</option>
<option value="020301">PPS Belawan</option>
<option value="101401">PPN Kejawanan</option>
<option value="140303">PPN Pengambengan</option>
<option value="150205">PPP MUNCAR</option>
<option value="141301">PPP Tawang</option>
<option value="250601">PPN Tual</option>
<option value="111145">PPP Karimunjawa</option>
<option value="140304">PPP Klidang Lor</option>
<option value="141801">PPP Wonokerto</option>
<option value="240303">PPP Kwandang</option>
<option value="210701">PPS Bitung</option>
<option value="141701">PPP Bajomulyo</option>
<option value="151405">PPP Pondokdadap</option>
<option value="132101">PPN Prigi</option>
<option value="290301">PPN Karangantu</option>
<option value="030301">PPS Bungus</option>
<option value="170901">PPN Pemangkat</option>
<option value="170207">PPP Teluk Batang</option>
<option value="090101">PPS Jakarta</option>
<option value="270102">PPN Sungai Liat</option>
<option value="270201">PPN Tanjung Pandan</option>
<option value="152202">UPPPP Mayangan</option>
<option value="160901">PPP Kupang</option>
<option value="020802">PPN Sibolga</option>
<option value="150101">PPP Labuhan Lombok</option>
<option value="190301">PPP Banjarmasin</option>
<option value="010401">PPP Lampulo</option>
<option value="250501">PPN Ambon</option>
<option value="110623">PPN Pekalongan</option>
<option value="132102">PPP Tamperan</option>
<option value="170223">PPP Sungai Rengas</option>
<option value="280101">PPN Ternate</option>
<option value="030205">PPP Sikakap</option>
<option value="180501">PPI Kumai</option>
<option value="121302">PPP Muara Ciasem</option>
<option value="200501">PPI Manggar</option>
<option value="102101">PPN Palabuhan Ratu</option>
<option value="190201">PPI Muara Kintap</option>
<option value="142201">PPP Tasikagung Rembang</option>
<option value="090102">PPI Muara Angke</option>
<option value="231001">UPTD.PPI Paotere</option>
<option value="110203">PPP LABUAN BANTEN</option>
<option value="270401">PPI Donggala</option>
</optgroup>
</select></pre></td></tr>
<tr>
<td>WPP</td><td><select name="id_wpp" title="Diisi dengan nama wilayah penangkapan sesuai dengan SPI|" class="kpl_penangkap">
<option value="0">Kosong</option>
<option value="01">Selat Malaka</option>
<option value="02">Laut Cina Selatan</option>
<option value="03">Laut Jawa</option>
<option value="04">Laut Flores dan Selat Makassar</option>
<option value="05">Laut Banda</option>
<option value="06">Laut Arafura</option>
<option value="07">Laut Maluku</option>
<option value="08">Laut Sulawesi dan Samudera Pasifik</option>
<option value="09">Samudera Hindia</option>
</select></td></tr>
<tr>
<td>DPI</td><td><select name="id_dpi" title="Diisi dengan nama daerah penangkapan sesuai dengan SPI|" class="kpl_penangkap">
<option value="0">Kosong</option>
<option value="0101">WPP Selat Malaka 1</option>
<option value="0102">WPP Selat Malaka 2</option>
</select></td></tr>
<tr>
<td>Alat Tangkap</td><td>1. <select name="alat_tangkap" title="Diisi dengan nama alat tangkap sesuai SPI|" class="kpl_penangkap">

<option value="1">Pukat tarik udang ganda</option>
<option value="2">Otter Trawl</option>
<option value="3">Pukat tarik ikan</option>
<option value="4">Payang (tmsk. Lampara)</option>
<option value="5">Dogol (tmsk. Lampara dasar, cantrang)</option>
<option value="6">Pukat Pantai (Jaring arad)</option>
<option value="26">Perangkap lainnya</option>
<option value="7">Pukat Cincin</option>
<option value="8">Jaring insang hanyut</option>
<option value="9">Jaring insang lingkar</option>
<option value="10">Jaring klitik</option>
<option value="11">Jaring insang tetap</option>
<option value="12">Jaring tiga lapis</option>
<option value="13">Bagan perahu/rakit</option>
<option value="14">Bagan tancap</option>
<option value="15">Serok dan songko</option>
<option value="16">Jaring Angkat Lainnya</option>
<option value="17">Rawai tuna</option>
<option value="18">Rawai hanyut lainnya selain rawai tuna</option>
<option value="19">Rawai tetap</option>
<option value="20">Huhate</option>
<option value="21">Pancing tonda</option>
<option value="22">Pancing ulur</option>
<option value="23">Sero (tmsk. Kelong)</option>
<option value="24">Jermal</option>
<option value="25">Bubu (tmsk. Bubu ambai)</option>
<option value="27">Alat penangkap kerang</option>
<option value="28">Alat pengumpul rumput laut</option>
<option value="29">Muroami</option>
<option value="30">Jala tebar</option>
<option value="31">Tidak Ada Alat Tangkap</option>
<option value="32">Pukat tarik berbingkai</option>
<option value="33">Anco</option>
<option value="34">Rawai dasar tetap</option>
<option value="35">Pancing tegak</option>
<option value="36">Pancing cumi</option>
<option value="37">Pancing lainnya</option>
<option value="38">Alat penangkap teripang (ladung)</option>
<option value="39">Alat penangkap kepiting</option>
<option value="40">Garpu dan Tombak</option>
<option value="41">Pukat tarik udang tunggal</option>
<option value="42">Pukat cincin satu kapal</option>
<option value="43">Pukat Cincin Pelagis Besar dengan satu kapal</option>
<option value="44">Jaring lingkar tanpa tali kerut/Lampara</option>
<option value="45">Pukat hela dasar berpalang</option>
<option value="46">Pukat hela dasar berpapan</option>
<option value="47">Pukat hela dasar dua kapal</option>
<option value="48">Pukat hela dasar lainnya</option>
<option value="49">Pukat hela dasar udang</option>
<option value="50">Pukat hela pertengahan berpapan</option>
<option value="51">Puka hela pertengahan dua kapal</option>
<option value="52">Pukat hela pertengahan udang</option>
<option value="53">Pukat hela kembar berpapan</option>
<option value="54">Pukat hela lainnya</option>
<option value="55">Pukat dorong</option>
<option value="56">Boke Ami</option>
<option value="57">Jala jatuh berkapal</option>
<option value="58">Jala tebar lainnya</option>
<option value="59">Jaring liong bun</option>
<option value="60">Jaring insang oseanik</option>
<option value="61">Jaring insang berlapis</option>
<option value="62">Jaring insang kombinasi dengan tramel net</option>
<option value="63">Jaring insang lainnya</option>
<option value="64">Set Net</option>
<option value="65">Bubu bersayap</option>
<option value="66">Pukat labuh</option>
<option value="67">Togo</option>
<option value="68">Ambai</option>
<option value="69">Pengerih</option>
<option value="70">Perangkap ikan peloncat</option>
<option value="71">Seser</option>
<option value="72">Pancing berjoran</option>
<option value="73">Squid angling</option>
<option value="74">Huhate Mekanis</option>
<option value="75">Pancing layang-layang</option>
<option value="76">Ladung</option>
<option value="77">Panah</option>
<option value="78">Alat penjepit dan melukai lainnya</option>
<option value="79">Ladung</option>
<option value="80">Panah</option>
<option value="81">Alat penjepit dan melukai lainnya</option>
<option value="82">Alat tangkap tes</option>
<option value="83">Pengangkut Ikan</option>
</select> <sup class="wajibPenangkap"> * kapal penangkap ikan wajib isi.</sup><br>2. <select name="alat_tangkap2" title="Diisi dengan nama alat tangkap lainnya yang digunakan dalan operasi penangkapan ikan" class="kpl_penangkap">
<option value="0">Kosong</option>
<option value="1">Pukat tarik udang ganda</option>
<option value="2">Otter Trawl</option>
<option value="3">Pukat tarik ikan</option>
<option value="4">Payang (tmsk. Lampara)</option>
<option value="5">Dogol (tmsk. Lampara dasar, cantrang)</option>
<option value="6">Pukat Pantai (Jaring arad)</option>
<option value="26">Perangkap lainnya</option>
<option value="7">Pukat Cincin</option>
<option value="8">Jaring insang hanyut</option>
<option value="9">Jaring insang lingkar</option>
<option value="10">Jaring klitik</option>
<option value="11">Jaring insang tetap</option>
<option value="12">Jaring tiga lapis</option>
<option value="13">Bagan perahu/rakit</option>
<option value="14">Bagan tancap</option>
<option value="15">Serok dan songko</option>
<option value="16">Jaring Angkat Lainnya</option>
<option value="17">Rawai tuna</option>
<option value="18">Rawai hanyut lainnya selain rawai tuna</option>
<option value="19">Rawai tetap</option>
<option value="20">Huhate</option>
<option value="21">Pancing tonda</option>
<option value="22">Pancing ulur</option>
<option value="23">Sero (tmsk. Kelong)</option>
<option value="24">Jermal</option>
<option value="25">Bubu (tmsk. Bubu ambai)</option>
<option value="27">Alat penangkap kerang</option>
<option value="28">Alat pengumpul rumput laut</option>
<option value="29">Muroami</option>
<option value="30">Jala tebar</option>
<option value="31">Tidak Ada Alat Tangkap</option>
<option value="32">Pukat tarik berbingkai</option>
<option value="33">Anco</option>
<option value="34">Rawai dasar tetap</option>
<option value="35">Pancing tegak</option>
<option value="36">Pancing cumi</option>
<option value="37">Pancing lainnya</option>
<option value="38">Alat penangkap teripang (ladung)</option>
<option value="39">Alat penangkap kepiting</option>
<option value="40">Garpu dan Tombak</option>
<option value="41">Pukat tarik udang tunggal</option>
<option value="42">Pukat cincin satu kapal</option>
<option value="43">Pukat Cincin Pelagis Besar dengan satu kapal</option>
<option value="44">Jaring lingkar tanpa tali kerut/Lampara</option>
<option value="45">Pukat hela dasar berpalang</option>
<option value="46">Pukat hela dasar berpapan</option>
<option value="47">Pukat hela dasar dua kapal</option>
<option value="48">Pukat hela dasar lainnya</option>
<option value="49">Pukat hela dasar udang</option>
<option value="50">Pukat hela pertengahan berpapan</option>
<option value="51">Puka hela pertengahan dua kapal</option>
<option value="52">Pukat hela pertengahan udang</option>
<option value="53">Pukat hela kembar berpapan</option>
<option value="54">Pukat hela lainnya</option>
<option value="55">Pukat dorong</option>
<option value="56">Boke Ami</option>
<option value="57">Jala jatuh berkapal</option>
<option value="58">Jala tebar lainnya</option>
<option value="59">Jaring liong bun</option>
<option value="60">Jaring insang oseanik</option>
<option value="61">Jaring insang berlapis</option>
<option value="62">Jaring insang kombinasi dengan tramel net</option>
<option value="63">Jaring insang lainnya</option>
<option value="64">Set Net</option>
<option value="65">Bubu bersayap</option>
<option value="66">Pukat labuh</option>
<option value="67">Togo</option>
<option value="68">Ambai</option>
<option value="69">Pengerih</option>
<option value="70">Perangkap ikan peloncat</option>
<option value="71">Seser</option>
<option value="72">Pancing berjoran</option>
<option value="73">Squid angling</option>
<option value="74">Huhate Mekanis</option>
<option value="75">Pancing layang-layang</option>
<option value="76">Ladung</option>
<option value="77">Panah</option>
<option value="78">Alat penjepit dan melukai lainnya</option>
<option value="79">Ladung</option>
<option value="80">Panah</option>
<option value="81">Alat penjepit dan melukai lainnya</option>
<option value="82">Alat tangkap tes</option>
<option value="83">Pengangkut Ikan</option>
</select></td></tr>
<tr>
<td>Alat Bantu Penangkapan</td><td>1. <select name="alat_bantu" title="Diisi dengan nama alat bantu yang digunakan dalam operasi penangkapan ikan" class="kpl_penangkap">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select><br>2: <select name="alat_bantu2" title="Diisi dengan nama alat bantu yang digunakan dalam operasi penangkapan ikan" class="kpl_penangkap">
<option value="0">Kosong</option>
<option value="1">Deck Machinery</option>
<option value="2">Winch</option>
<option value="3">Line Hauler</option>
<option value="4">Net Hauler</option>
<option value="5">Power Block</option>
<option value="6">Net Roller</option>
<option value="7">Net Arranger</option>
<option value="8">Warping End</option>
<option value="9">Ball Roller</option>
<option value="10">Line Arranger</option>
<option value="11">Branch Reel</option>
<option value="12">Instrumentasi</option>
<option value="13">Sonar</option>
<option value="14">Fish Finder</option>
<option value="15">Net Zonde</option>
<option value="16">Net Depth Meter</option>
<option value="17">Radio Buoy</option>
<option value="18">Fish Agregating Device</option>
<option value="19">Rumpon</option>
<option value="20">Lampu</option>
</select></td></tr>
<tr>
<td>Ukuran Kapal (Meter)</td><td><pre>Panjang :<input type="text" name="dimensi_p" value="" size="8" style="width:10%" data-original-title="" title=""> <sup> * wajib isi.</sup><br>Lebar   :<input type="text" name="dimensi_l" value="" size="8" style="width:10%" data-original-title="" title=""><br>Dalam   :<input type="text" name="dimensi_d" value="" size="8" style="width:10%" data-original-title="" title=""><br>Draft   :<input type="text" name="dimensi_dr" value="" size="8" style="width:10%" data-original-title="" title=""></pre></td></tr>
<tr>
<td>Bahan Kapal</td><td><select name="bahan_kapal" title="Bahan utama penyusun kapal, seperti : kayu, fibreglass, atau besi">
<option value="Kayu">Kayu</option>
<option value="Fiberglass">Fiberglass</option>
<option value="Besi">Besi</option>
<option value="lainnya">lainnya</option>
</select></td></tr>
<tr>
<td>Tahun Kapal Dibangun</td><td><input type="text" name="tahun_dibangun" value="" title="" class="must_number" data-original-title="Diisi dengan tahun pembuatan kapal"></td></tr>
<tr>
<td>Merk Mesin Utama</td><td><input type="text" name="merk_mesin_utama" value="" title="" data-original-title="Diisi dengan merk mesin utama"></td></tr>
<tr>
<td>Merk Mesin Bantu</td><td><input type="text" name="merk_mesin_bantu" value="" title="" data-original-title="Diisi dengan merk mesin bantu"></td></tr>
<tr>
<td>Tenaga Mesin Total</td><td><input type="text" name="tenaga_mesin_total" value="" title="" class="must_number" data-original-title="Diisi dengan ukuran tenaga mesin dalam satuan HP"></td></tr>
<tr>
<td>Jenis BBM</td><td><select name="jenis_bbm" title="Diisi dengan jenis bahan bakar utama">
<option value="Solar">Solar</option>
<option value="Minyak Tanah">Minyak Tanah</option>
<option value="Bensin">Bensin</option>
<option value="Gas">Gas</option>
<option value="lainnya">lainnya</option>
</select></td></tr>
<tr>
<td>Jumlah Total Palkah (Unit)</td><td><input type="text" name="jml_palka" value="" title="" class="must_number" data-original-title="Jumlah ruangan penyimpanan kapal yang umumnya dipergunakan dalam menyimpan ikan dan perbekalan"></td></tr>
<tr>
<td>Volumen Total Palkah (Meter Kubik)</td><td><input type="text" name="vol_palka" value="" title="" class="must_number" data-original-title="Volume total palkah yang terdapat di kapal"></td></tr>
<tr>
<td>Kapasitas Muat Optimum BBM (Liter)</td><td><input type="text" name="kapasitas_bbm" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah kapasitas maksimal bahan bakar setiap trip"></td></tr>
<tr>
<td>Kapasitas Muat Optimum Es (Kg)</td><td><input type="text" name="kapasitas_es" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah kapasitas maksimal trip es yang digunakan setiap trip"></td></tr>
<tr>
<td>Kapasitas Muat Optimum Garam (Kg)</td><td><input type="text" name="kapasitas_garam" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah kapasitas maksimal garam yang digunakan setiap trip"></td></tr>
<tr>
<td>Kapasitas Muat Optimum Air (Liter)</td><td><input type="text" name="kapasitas_air" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah kapasitas maksimal air tawar yang digunakan setiap trip"></td></tr>
<tr>
<td>Kapasitas Muat Optimum Umpan (Kg)</td><td><input type="text" name="kapasitas_umpan" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah umpan yang dibawa setiap trip"></td></tr>
<tr>
<td>Biaya Ransum Maksimal (Rp)</td><td><input type="text" name="biaya_ransum" value="" title="" class="must_number" data-original-title="Diisi dengan biaya ransum setiap trip"></td></tr>
<tr>
<td>Jumlah ABK WNI (Orang)</td><td><input type="text" name="abk_wni_per_trip" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah ABK berkebangsaan Indonesia setiap trip"></td></tr>
<tr>
<td>Jumlah ABK WNA (Orang)</td><td><input type="text" name="abk_wna_per_trip" value="" title="" class="must_number" data-original-title="Diisi dengan jumlah ABK berkebangsaan Indonesia setiap trip"></td></tr>
</tbody>
</table>
