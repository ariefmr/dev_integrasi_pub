<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forms extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */

	//private $global_config = array(); // Dari database
	private $dev_mode = FALSE;
	private $assets_paths = '';

		function __construct()
		{
			parent::__construct();

			//$this->load->config('custom_constants');
			$this->assets_paths = $this->config->item('assets_paths');

			$this->load->library('entry/mkform', 'table');

			$this->load->model('admin/mdl_konfigurasi');
			$this->dev_mode = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

			//TODO: LOGIN CHECK HERE
		}

	public function index()
	{
		$data['page_title'] = 'Aktifitas Kapal di Pelabuhan';
		$data['module'] = 'forms';
		$data['view_file'] = 'v_aktifitas';

		echo Modules::run('templates/type/default_template', $data);
	}

	public function view($id_form)
	{


		$data['module'] = 'forms';
		$data['view_file'] = 'v_form';
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');


		switch ($id_form) {
		    case 'H1':
		        $data['form_file'] = 'form_H1';
		        $mockup_file = 'form_H1.jpg';

		        $data['module_1'] = '';// module_1 untuk tab design, module_2 untuk tab final
		        $data['module_2'] = 'entry/H1';// module_folder/view_file

				$data['page_title'] = 'Form H1';
				$data['content_title'] = 'H1 - Kapal Masuk Pelabuhan';

				$data['form_title_1'] = 'Form H1';
				$data['form_title_2'] = 'Desain (Mockup) H1';
				$data['form_title_3'] = 'Form Final H1';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/4k6l0m0r82jg96p/desc_h1.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/xgpdpamm20v2uu6/db_info_h1.txt?dl=1';
		        break;
		    case 'H2':
		        $data['form_file'] = 'form_H2';
		        $mockup_file = 'form_H2.jpg';

		        $data['module_1'] = '';
		        $data['module_2'] = 'entry/H2'; // Panggil view file, bukan module/controller

				$data['page_title'] = 'Form H2';
				$data['content_title'] = 'H2 - Produksi Ikan dan Kapal';

				$data['form_title_1'] = 'Form H2';
				$data['form_title_2'] = 'Desain (Mockup) H2';
				$data['form_title_3'] = 'Form Final H2';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/5ml017ossfy76ou/desc_h2.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/sh20b3gm9g1anws/db_info_h2.txt?dl=1';
		        break;
		    case 'H3':
		        $data['form_file'] = 'form_H3';
		        $mockup_file = 'form_H3.jpg';

		        $data['module_1'] = '';
		        $data['module_2'] = 'entry/H3';

				$data['page_title'] = 'Form H3';
				$data['content_title'] = 'H3 - Distribusi Ikan dari Pelabuhan Luar';

				$data['form_title_1'] = 'Form H3';
				$data['form_title_2'] = 'Desain (Mockup) H3';
				$data['form_title_3'] = 'Form Final H3';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		     case 'H4':
		        $data['form_file'] = 'form_H4';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form H4';
				$data['content_title'] = 'H4 - Harga Ikan ditempat Pedagang';

				$data['form_title_1'] = 'Form H4';
				$data['form_title_2'] = 'Desain (Mockup) H4';
				$data['form_title_3'] = 'Form Final H4';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'H5':
		        $data['form_file'] = 'form_H5';
		        $mockup_file = 'form_H5.jpg';

		        $data['module_1'] = '';
		        $data['module_2'] = 'entry/H5';

				$data['page_title'] = 'Form H5';
				$data['content_title'] = 'H5 - Kapal Keluar Pelabuhan';

				$data['form_title_1'] = 'Form H5';
				$data['form_title_2'] = 'Desain (Mockup) H5';
				$data['form_title_3'] = 'Form Final H5';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/8giyyhqht59qk9w/desc_h5.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/m80mlwoa656l4a2/db_info_h5.txt?dl=1';
		        break;
		    case 'B1':
		        $data['form_file'] = 'form_B1';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form B1';
				$data['content_title'] = 'B1 - Jasa dan Pendapatan Pelabuhan';

				$data['form_title_1'] = 'Form B1';
				$data['form_title_2'] = 'Desain (Mockup) B1';
				$data['form_title_3'] = 'Form Final B1';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'B2':
		        $data['form_file'] = 'form_B2';
		        $mockup_file = 'form_B2.jpg';

		        $data['module_1'] = '';
		        $data['module_2'] = 'entry/B2';

				$data['page_title'] = 'Form B2';
				$data['content_title'] = 'B2 - Operasional Perbekalan Dari Dalam Dan Luar Pelabuhan';

				$data['form_title_1'] = 'Form B2';
				$data['form_title_2'] = 'Desain (Mockup) B2';
				$data['form_title_3'] = 'Form Final B2';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'B3':
		        $data['form_file'] = 'form_B3';
		        $mockup_file = 'form_B3.jpg';

		        $data['module_1'] = '';
		        $data['module_2'] = 'entry/B3';

				$data['page_title'] = 'Form B3';
				$data['content_title'] = 'B3 - Distribusi Ikan Yang didaratkan di Pelabuhan';

				$data['form_title_1'] = 'Form B3';
				$data['form_title_2'] = 'Desain (Mockup) B3';
				$data['form_title_3'] = 'Form Final B3';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/cysbe7lulqr9u59/desc_B3.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/emyjioba49om0xd/db_info_B3.txt?dl=1';
		        break;
		    case 'B4':
		        $data['form_file'] = 'form_B4';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form B4';
				$data['content_title'] = 'B4 - Usaha Pengolahan Ikan di Pelabuhan';

				$data['form_title_1'] = 'Form B4';
				$data['form_title_2'] = 'Desain (Mockup) B4';
				$data['form_title_3'] = 'Form Final B4';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'B5':
		        $data['form_file'] = 'form_B5';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form B5';
				$data['content_title'] = 'B5 - Kondisi Fasilitas di Pelabuhan';

				$data['form_title_1'] = 'Form B5';
				$data['form_title_2'] = 'Desain (Mockup) B5';
				$data['form_title_3'] = 'Form Final B5';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'B6':
		        $data['form_file'] = 'form_B6';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form B6';
				$data['content_title'] = 'B6 - Koefisien Koreksi Prduksi Ikan Di Pelabuhan';

				$data['form_title_1'] = 'Form B6';
				$data['form_title_2'] = 'Desain (Mockup) B6';
				$data['form_title_3'] = 'Form Final B6';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'B7':
		        $data['form_file'] = 'form_B7';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form B7';
				$data['content_title'] = 'B7 - Masalah Dan Upaya di Pelabuhan';

				$data['form_title_1'] = 'Form B7';
				$data['form_title_2'] = 'Desain (Mockup) B7';
				$data['form_title_3'] = 'Form Final B7';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S1':
		        $data['form_file'] = 'form_S1';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S1';
				$data['content_title'] = 'S1 - Industri di Pelabuhan';

				$data['form_title_1'] = 'Form S1';
				$data['form_title_2'] = 'Desain (Mockup) S1';
				$data['form_title_3'] = 'Form Final S1';
				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S2':
		        $data['form_file'] = 'form_S2';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S2';
				$data['content_title'] = 'S2 - Registrasi Kapal Penangkap Ikan';

				$data['form_title_1'] = 'Form S2';
				$data['form_title_2'] = 'View S2';
				$data['form_title_3'] = 'View Final S2';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/bqpza39fl26vle6/desc_S2.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/d6b85lztqy13935/db_info_S2.txt?dl=1';
		        break;
		    case 'S3':
		        $data['form_file'] = 'form_S3';

		        $mockup_file = '';

		        $data['module_1'] = 'mst_jenis_ikan/table_ikan';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S3';
				$data['content_title'] = 'S3 - Registrasi Sumber Daya Ikan';

				$data['form_title_1'] = 'Form S3';
				$data['form_title_2'] = 'View S3';
				$data['form_title_3'] = 'View Final S3';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/n1mf15sv2hqdhni/desc_S3.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/ddrpqny05ocadei/db_info_S3.txt?dl=1';
		        break;
		    case 'S4':
		        $data['form_file'] = 'form_S4';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S4';
				$data['content_title'] = 'S4 - Registrasi Alat Tangkap';

				$data['form_title_1'] = 'Form S4';
				$data['form_title_2'] = 'View S4';
				$data['form_title_3'] = 'View Final S4';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/6kj3c03qkrxfzac/desc_S4.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/if3ag8int9kc0lg/db_info_S4.txt?dl=1';
		        break;
		    case 'S6':
		        $data['form_file'] = 'form_S6';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S6';
				$data['content_title'] = 'S6 - Data Umum Pelabuhan Ikan';

				$data['form_title_1'] = 'Form S6';
				$data['form_title_2'] = 'Desain (Mockup) S6';
				$data['form_title_3'] = 'Form Final S6';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S7':
		        $data['form_file'] = 'form_S7';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S7';
				$data['content_title'] = 'S7 - Fasilitas Pokok';

				$data['form_title_1'] = 'Form S7';
				$data['form_title_2'] = 'Desain (Mockup) S7';
				$data['form_title_3'] = 'Form Final S7';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S8':
		        $data['form_file'] = 'form_S8';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S8';
				$data['content_title'] = 'S8 - Fasilitas Fungsional';

				$data['form_title_1'] = 'Form S8';
				$data['form_title_2'] = 'Desain (Mockup) S8';
				$data['form_title_3'] = 'Form Final S8';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S9':
		        $data['form_file'] = 'form_S9';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S9';
				$data['content_title'] = 'S9 - Fasilitas Penunjang';

				$data['form_title_1'] = 'Form S9';
				$data['form_title_2'] = 'Desain (Mockup) S9';
				$data['form_title_3'] = 'Form Final S9';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S10':
		        $data['form_file'] = 'form_S10';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S10';
				$data['content_title'] = 'S10 - data Lingkungan Fisik Pelabuhan';

				$data['form_title_1'] = 'Form S10';
				$data['form_title_2'] = 'Desain (Mockup) S10';
				$data['form_title_3'] = 'Form Final S10';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S11':
		        $data['form_file'] = 'form_S11';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S11';
				$data['content_title'] = 'S11 - Kelembagaan UPT Pelabuhan';

				$data['form_title_1'] = 'Form S11';
				$data['form_title_2'] = 'Desain (Mockup) S11';
				$data['form_title_3'] = 'Form Final S11';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S12':
		        $data['form_file'] = 'form_S12';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S12';
				$data['content_title'] = 'S12 - Kelembagaan di Dalam Pelabuhan';

				$data['form_title_1'] = 'Form S12';
				$data['form_title_2'] = 'Desain (Mockup) S12';
				$data['form_title_3'] = 'Form Final S12';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		    case 'S13':
		        $data['form_file'] = 'form_S13';
		        $mockup_file = '';

		        $data['module_1'] = '';
		        $data['module_2'] = '';

				$data['page_title'] = 'Form S13';
				$data['content_title'] = 'S13 - Masyarakat Perikanan';

				$data['form_title_1'] = 'Form S13';
				$data['form_title_2'] = 'Desain (Mockup) S13';
				$data['form_title_3'] = 'Form Final S13';

				$data['link_dropbox_1'] = 'https://www.dropbox.com/s/xmvkesrxld8cqld/desc_.txt?dl=1';
				$data['link_dropbox_2'] = 'https://www.dropbox.com/s/zcqko7vhjppz2bp/db_info_.txt?dl=1';
		        break;
		}

		$data['mockup_url'] = empty($mockup_file) ? '' : $this->assets_paths['mockup_images']."/".$mockup_file;

		// Untuk development, dropbox files tidak diunduh langsung dari dropbox
		if($this->dev_mode->nilai === 'true')
			{
				$data['link_dropbox_1'] = site_url('misc/desc_.txt');
				$data['link_dropbox_2'] = site_url('misc/db_info_.txt');
			}
		
		echo Modules::run('templates/type/default_template', $data);
	}
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */