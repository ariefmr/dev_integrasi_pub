<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_alat_tangkap extends MX_Controller {

	/**
	 * Controller Mst Alat Tangkap
	 * created by ariefmr
 	 * at wadadah
 	 * 24-09-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_alat_tangkap');
		}

	public function index()
	{

		$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap();
		$this->load->view('daftar_alat_tangkap', $data);
	}

	public function daftar_alat_tangkap($aktif = '') 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis alat_tangkap';
		$data['content_title'] = 'Daftar Jenis alat_tangkap';
		$data['module'] = 'mst_alat_tangkap';
		$data['view_file'] = 'daftar_alat_tangkap';
		if($aktif === "aktif"){
			$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap(TRUE);
		}else{
			$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap();

		}
		echo Modules::run('templates/type/default_template', $data);	
	}

	// Wigdet Pencarian Alat Tangkap untuk keperluan entry form 
	public function wgt_pilih_alat_tangkap()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_alat_tangkap'] = FALSE;
		$this->load->view('pilih_alat_tangkap', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar alat_tangkap
	public function select_alat_tangkap()
	{
		$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap();
		$this->load->view('select_alat_tangkap', $data);
	}

	public function json_alat_tangkap()
	{
		$array_of_alat_tangkap = (Array) $this->mdl_alat_tangkap->list_opsi_alat_tangkap();

		echo json_encode($array_of_alat_tangkap);
	}

	public function table_alat_tangkap()
	{
		$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap();	
		$this->load->view('daftar_alat_tangkap', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}

	public function search_alat_tangkap()
	{
		$get_nama_alat_tangkap = $this->input->get('q', FALSE);
		$get_limit_result = $this->input->get('limit', FALSE);
		$search_result = $this->mdl_alat_tangkap->search_alat_tangkap($get_nama_alat_tangkap, $get_limit_result);
		$array_result = Array( 'total' => count($search_result),'list_alat_tangkap' => $search_result );
		echo json_encode($array_result);
	}

}

/* End of file mst_alat_tangkap.php */
/* Location: ./application/modules/mst_alat_tangkap/controllers*/