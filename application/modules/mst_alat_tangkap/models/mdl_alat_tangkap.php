<?php
/*
 * class Mdl_alat_tangkap
 * created by ariefmr
 * at wadadah
 * 24-09-2013
 */

class Mdl_alat_tangkap extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_master = NULL;

    function __construct()
    {
        $this->db_master = $this->load->database('db_dss', TRUE);
    }
    
    public function list_alat_tangkap($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_master->like('aktif', 'ya');
        }

        $query = $this->db_master->get('mst_alat_tangkap');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_alat_tangkap()
    {
        $query = 'SELECT id_alat_tangkap AS id, nama_alat_tangkap AS text FROM mst_alat_tangkap';
        
        $run_query = $this->db_master->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>