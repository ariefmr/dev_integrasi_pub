<?php
	/*
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_pilih_alat_tangkap' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('Nama alat_tangkap','SIPI','Perusahaan','Alat Tangkap','Tonase');

	
	$counter = 1;
	foreach ($list_alat_tangkap as $item) {
		$this->table->add_row($counter.'.', $item->nama_alat_tangkap, $item->no_sipi, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_alat_tangkap, $item->gt_alat_tangkap);
		$counter++;
	}
	
	$table_list_alat_tangkap = $this->table->generate();
	*/
?>

<!-- TAMPIL DATA -->
	<div class="form-group">
					<label for="id_alat_tangkap" class="col-lg-4 control-label">Pilih Alat Tangkap :</label>
					<div class="col-lg-8">
                         <input id="start_search" name="id_alat_tangkap" type="hidden" class="bigdrop">
                    </div>
    </div>
	


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null;

	function formatListalat_tangkapResult(alat_tangkap)
	{
		//var markup = "<table class='alat_tangkap-result'><tr>";
        //markup += "<td class='alat_tangkap-info'><div class='alat_tangkap-nama'>" + alat_tangkap.nama_alat_tangkap + "</div>";
        //markup += "</td></tr></table>";
        //return markup;
		//return "<div class='result_alat_tangkap' id='"+alat_tangkap.id_alat_tangkap+"'>"+alat_tangkap.nama_alat_tangkap+" / "+alat_tangkap.no_sipi+"</div>";
		return "<div class='result_alat_tangkap' id='"+alat_tangkap.id_alat_tangkap+"'><strong>"+alat_tangkap.nama_alat_tangkap+"</strong> / <small>"+alat_tangkap.tanda_selar+"</small></div>";

	}

	function formatListalat_tangkapSelection(alat_tangkap)
	{
		return alat_tangkap.nama_alat_tangkap;
	}

	$(document).ready( function () {
		


		$("#start_search").select2({
									id: function(e) { return e.id_alat_tangkap },  	
									placeholder: "Mulai ketik nama alat tangkap..",
									width: "100%",
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: "<?php echo base_url('mst_alat_tangkap/search_alat_tangkap'); ?>",
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_alat_tangkap};
									        }
									},
                                    formatResult: formatListAlatTangkapResult,
                                    formatSelection: formatListAlatTangkapSelection
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_alat_tangkap(e.val);
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_alat_tangkap/json_alat_tangkap'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_alat_tangkap(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>