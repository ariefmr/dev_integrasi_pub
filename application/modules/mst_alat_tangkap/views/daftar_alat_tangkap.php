<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_alat_tangkap' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	//$this->table->set_heading('No', 'ID alat_tangkap', 'Nama Alat Tangkap', 'Nama Alat Tangkap Inggris','Jenis Kapal', 'ID Jenis Logbook', 'Jenis Alat Tangkap', 'ID Pengguna Buat', 'Tanggal Buat', 'ID Pengguna Ubah', 'Tanggal Ubah', 'Aktif');
	$this->table->set_heading('No', 'Nama Alat Tangkap', 'Nama Alat Tangkap Inggris','Jenis Kapal', 'Jenis Alat Tangkap','Aktif');
	
	$counter = 1;

	if($list_alat_tangkap !== FALSE){
		foreach ($list_alat_tangkap as $item) {
			$this->table->add_row($counter.'.', $item->nama_alat_tangkap, $item->nama_alat_tangkap_inggris, $item->jenis_kapal, $item->jenis_alat_tangkap,$item->aktif);
			$counter++;
		}
	}
	

	$table_list_alat_tangkap = $this->table->generate();
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading">Daftar Alat Tangkap Master (DSS)</div>
			<div class="panel-body overflowed">
<!-- TAMPIL DATA -->
		<?php
			echo $table_list_alat_tangkap;

		?>
			</div>
		</div>
	</div>
</div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_alat_tangkap').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>