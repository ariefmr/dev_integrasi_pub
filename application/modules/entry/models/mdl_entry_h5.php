<?php
/*
 * class Mdl_H5
 */

class Mdl_entry_H5 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input_aktivitas($aktivitas)
    {
    	//$this->db->insert_batch('trs_pemasaran_masuk', $data);
    	//echo $this->db->last_query();
        //var_dump($aktivitas);
        //echo "<br><br>";
        //var_dump($perbekalan);

        $this->db->insert('trs_aktivitas_kapal', $aktivitas);
        ///echo $this->db->last_query();
        $id_aktivitas_kapal_current = $this->db->insert_id();
        return $id_aktivitas_kapal_current;
    }

    public function input_perbekalan($perbekalan)
    {
        $this->db->insert_batch('trs_perbekalan_kapal_keluar', $perbekalan);
        //return $this->db->last_query();
    }


    public function input_alat_bantu_keluar($alat_bantu_keluar)
    {
        $this->db->insert_batch('trs_alat_bantu_keluar', $alat_bantu_keluar);
    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_aktivitas_kapal SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_aktivitas_kapal = $id" ;
        
        $query = $this->db->query($sql);

        $sql_check_perbekalan = $this->db->get_where('trs_perbekalan_kapal_keluar', array('id_aktivitas_kapal' => $id))->num_rows();

        if($sql_check_perbekalan > 0)
        {
            $sql_delete_perbekalan = "UPDATE db_pipp.trs_perbekalan_kapal_keluar SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_aktivitas_kapal = $id ";
            $query_delete_perbekalan = $this->db->query($sql_delete_perbekalan);
        }

    }

    public function update_chain($id_aktivitas_kapal_keluar, $id_aktivitas_kapal_masuk)
    {
        $data = array(
               'id_aktivitas_referensi' => $id_aktivitas_kapal_masuk,
            );
        $this->db->where('id_aktivitas_kapal', $id_aktivitas_kapal_keluar);
        $this->db->update('trs_aktivitas_kapal', $data); 

    }
    // TODO data produksi  (View H5)
    // TODO delete produksi (Delete H5)
    // TODO edit produksi (Edit H5)
}