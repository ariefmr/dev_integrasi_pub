<?php
/*
 * class Mdl_S6B
 */

class Mdl_entry_k5 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('trs_k5', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_k5 SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now() WHERE id_k5=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
        $this->db->where('id_k5', $array_to_edit['id_k5']);
        $query = $this->db->update('trs_k5',$array_to_edit);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

}