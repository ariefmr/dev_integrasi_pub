<?php
/*
 * class Mdl_H2
 */

class Mdl_entry_H2 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input_batch($data)
    {
    	$this->db->insert_batch('trs_produksi', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_produksi SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_produksi=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
            foreach ($array_to_edit as $id_produksi => $data) {
                $this->db->where('id_produksi', $id_produksi);
                $this->db->update('trs_produksi',$data);
            }
    }

    // TODO data produksi  (View H2)
    // TODO delete produksi (Delete H2)
    // TODO edit produksi (Edit H2)
}