<?php
/*
 * class Mdl_S6B
 */

class Mdl_entry_s7g extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('mst_fas_hubung', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.mst_fas_hubung SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_fas_hubung=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
        $this->db->where('id_fas_hubung', $array_to_edit['id_fas_hubung']);
        $query = $this->db->update('mst_fas_hubung',$array_to_edit);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

    // TODO data produksi  (View S6B)
    // TODO delete produksi (Delete S6B)
    // TODO edit produksi (Edit S6B)
}