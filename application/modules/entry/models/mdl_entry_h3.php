<?php
/*
 * class Mdl_H3
 */

class Mdl_entry_H3 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input_batch($data)
    {
    	$this->db->insert_batch('trs_pemasaran_masuk', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_pemasaran_masuk SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_pemasaran_masuk ='$id'" ;
        
        $query = $this->db->query($sql);

    }

    public function edit($id_pemasaran_masuk, $id_jenis_kondisi_ikan, $jml_ikan, $harga_ikan)
    {

        $sql = " UPDATE db_pipp.trs_pemasaran_masuk SET id_jenis_kondisi_ikan='$id_jenis_kondisi_ikan', jml_ikan='$jml_ikan', harga_ikan='$harga_ikan' WHERE id_pemasaran_masuk='$id_pemasaran_masuk' " ;
        
        $query = $this->db->query($sql);

    }


    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}