<?php
/*
 * class Mdl_H3
 */

class Mdl_entry_H1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

      public function input_aktivitas($aktivitas)
    {
        //$this->db->insert_batch('trs_pemasaran_masuk', $data);
        //echo $this->db->last_query();
        //var_dump($aktivitas);
        //echo "<br><br>";
        //var_dump($perbekalan);

        $this->db->insert('trs_aktivitas_kapal', $aktivitas);
        //echo $this->db->last_query();
        $id_aktivitas_kapal_current = $this->db->insert_id();
        return $id_aktivitas_kapal_current;
    }

    public function input_kunjungan($kunjungan)
    {
        $this->db->insert('trs_maksud_kunjungan', $kunjungan);
        //echo $this->db->last_query();
    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_aktivitas_kapal SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_aktivitas_kapal = $id" ;
        
        $query = $this->db->query($sql);

        $sql_check_maksud_kunjungan = $this->db->get_where('trs_maksud_kunjungan', array('id_aktivitas_kapal' => $id))->num_rows();

        if($sql_check_maksud_kunjungan > 0)
        {
            $sql_delete_maksud_kunjungan = "UPDATE db_pipp.trs_maksud_kunjungan SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_aktivitas_kapal = $id ";
            $query_delete_maksud_kunjungan = $this->db->query($sql_delete_maksud_kunjungan);
        }

        $sql_check_chain_h5 = $this->db->get_where('trs_aktivitas_kapal', array('id_aktivitas_referensi' => $id, 'aktivitas' => 'keluar'))->num_rows();

        if($sql_check_chain_h5 > 0)
        {
            $sql_delete_chain_h5 = "UPDATE db_pipp.trs_aktivitas_kapal SET id_aktivitas_referensi = NULL WHERE id_aktivitas_referensi = $id ";
            $query_delete_chain_h5 = $this->db->query($sql_delete_chain_h5);
        }

        $sql_check_chain_h2 = $this->db->get_where('trs_produksi', array('id_aktivitas_referensi' => $id))->num_rows();

        if($sql_check_chain_h2 > 0)
        {
            $sql_delete_chain_h2 = "UPDATE db_pipp.trs_produksi SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_aktivitas_referensi = $id ";
            $query_delete_chain_h2 = $this->db->query($sql_delete_chain_h2);
            
            $sql = " UPDATE db_pipp.trs_aktivitas_kapal SET id_aktivitas_referensi = NULL WHERE id_aktivitas_kapal = $id" ;
            $query = $this->db->query($sql);
        }

    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}