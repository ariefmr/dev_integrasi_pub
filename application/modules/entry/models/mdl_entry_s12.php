<?php
/*
 * class Mdl_S6B
 */

class Mdl_entry_s12 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('mst_lembaga', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.mst_lembaga SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_lembaga=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
        $this->db->where('id_lembaga', $array_to_edit['id_lembaga']);
        $query = $this->db->update('mst_lembaga',$array_to_edit);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

    // TODO data produksi  (View S6B)
    // TODO delete produksi (Delete S6B)
    // TODO edit produksi (Edit S6B)
}