<?php
/*
 * class Mdl_S6A
 */

class Mdl_entry_s6a extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('mst_pelabuhan_baru', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.mst_pelabuhan_baru SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_pelabuhan=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
        $this->db->where('id_pelabuhan', $array_to_edit['id_pelabuhan']);
        $query = $this->db->update('mst_pelabuhan_baru',$array_to_edit);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

    public function list_status()
    {
        $query = $this->db->get('mst_status_pelabuhan');

        return $query;
    }

    // TODO data produksi  (View S6A)
    // TODO delete produksi (Delete S6A)
    // TODO edit produksi (Edit S6A)
}