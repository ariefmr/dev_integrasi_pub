<?php
/*
 * class Mdl_S6B
 */

class Mdl_entry_s1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('mst_industri', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.mst_industri SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_industri=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    public function edit($array_to_edit)
    {
            foreach ($array_to_edit as $id_industri => $data) {
                $this->db->where('id_industri', $id_industri);
                $this->db->update('mst_industri',$data);
            }
    }

    // TODO data produksi  (View S6B)
    // TODO delete produksi (Delete S6B)
    // TODO edit produksi (Edit S6B)
}