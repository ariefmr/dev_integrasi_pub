<?php
/*
 * class Mdl_B2
 */

class Mdl_entry_b1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input_batch($data)
    {
    	$this->db->insert_batch('trs_pendapatan_jasa', $data);
    	//var_dump($this->db->last_query()) ;
        //var_dump(this->db->last_query() );

    }

    public function input_batch2($data)
    {
        $this->db->insert_batch('trs_pendapatan_non_jasa', $data);
        //echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_pendapatan_jasa SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_pendapatan_jasa ='$id'" ;
        
        $query = $this->db->query($sql);

    }

    /*public function edit($id_pendapatan_jasa, $jml, $harga)
    {

        $sql = "UPDATE db_pipp.trs_pendapatan_jasa SET jml='$jml', harga='$harga' WHERE id_pendapatan_jasa='$id_pendapatan_jasa'" ;

        $query = $this->db->query($sql);

    }*/



    public function edit($array_to_edit)
    {

        $temp_id = '';
        $temp_jml = '';
        $temp_in = array();

        $sql_batch = "UPDATE trs_pendapatan_jasa 
                      SET jml = CASE ";

                      foreach ($array_to_edit as $item) {
                        $temp_id = $item['id_pendapatan_jasa'];
                        $temp_jml = $item['jml'];

                        $sql_batch .= " WHEN trs_pendapatan_jasa.id_pendapatan_jasa = ".$temp_id." 
                                        THEN ".$temp_jml." ";
                        array_push($temp_in, $temp_id);
                      }
        $sql_batch .= "END 
                       WHERE trs_pendapatan_jasa.id_pendapatan_jasa IN (".implode(',', $temp_in).")
                        ";

        $query = $this->db->query($sql_batch);
        // var_dump($sql_batch);


        //$this->db->update_batch('trs_pendapatan_jasa', $array_to_edit, 'id_pendapatan_jasa');
        //echo "<br>";
        //echo $this->db->last_query();
        // $sql_batch = '';
        // foreach($array_to_edit as $index)
        // {
        //     $sql_batch .= "UPDATE db_pipp.trs_pendapatan_jasa SET db_pipp.trs_pendapatan_jasa.jml = ".$index['jml']."
        //                     WHERE db_pipp.trs_pendapatan_jasa.id_pendapatan_jasa =".$index['id_pendapatan_jasa']." ;. ";
        //                     var_dump($index);
        // }

        // var_dump($sql_batch);
        // $this->db->where('id_pendapatan_jasa', $array_to_edit['id_pendapatan_jasa']);
        // $this->db->where('id_jns_jasa', $array_to_edit['id_jns_jasa']);
        // $query = $this->db->update('trs_pendapatan_jasa',$array_to_edit);

        // if($this->db->affected_rows() > 0){
        //     $result = true;
        // }else{
        //     $result = false;
        // }
        // return $result;

    }

    public function edit2($array_to_edit)
    {

        $temp_id = '';
        $temp_jml = '';
        $temp_in = array();

        $sql_batch = "UPDATE trs_pendapatan_non_jasa 
                      SET jml = CASE ";

                      foreach ($array_to_edit as $item) {
                        $temp_id = $item['id_pendapatan_non_jasa'];
                        $temp_jml = $item['jml'];

                        $sql_batch .= " WHEN trs_pendapatan_non_jasa.id_pendapatan_non_jasa = ".$temp_id." 
                                        THEN ".$temp_jml." ";
                        array_push($temp_in, $temp_id);
                      }
        $sql_batch .= "END 
                       WHERE trs_pendapatan_non_jasa.id_pendapatan_non_jasa IN (".implode(',', $temp_in).")
                        ";

        $desk_lain = $array_to_edit[4]['desk_lain'];
        $id_desk_lain = $array_to_edit[4]['id_pendapatan_non_jasa'];
        $sql_desk_lain = "UPDATE trs_pendapatan_non_jasa SET desk_lain = '".$desk_lain."' WHERE id_pendapatan_non_jasa = ".$id_desk_lain." ";


                        
        $query = $this->db->query($sql_batch);
        $query = $this->db->query($sql_desk_lain);

    }

    // public function edit2($array_to_edit)
    // {

    //     // $where = array('id_jns_non_jasa',
    //     //                 YEAR (tgl_catat),
    //     //                 MONTH (tgl_catat)
    //     //                 );
    //     $this->db->update_batch('trs_pendapatan_non_jasa', $array_to_edit, 'id_pendapatan_non_jasa');
    //     echo "<br>";
    //     echo $this->db->last_query();
    //     // $this->db->where('id_pendapatan_non_jasa', $array_to_edit['id_pendapatan_non_jasa']);
    //     // $this->db->where('id_jns_non_jasa', $array_to_edit['id_jns_non_jasa']);
    //     // $query = $this->db->update('trs_pendapatan_non_jasa',$array_to_edit);

    //     // if($this->db->affected_rows() > 0){
    //     //     $result = true;
    //     // }else{
    //     //     $result = false;
    //     // }
    //     // return $result;

    // }

    // TODO data produksi  (View H2)
    // TODO delete produksi (Delete H2)
    // TODO edit produksi (Edit H2)
}