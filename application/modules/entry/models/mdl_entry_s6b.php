<?php
/*
 * class Mdl_S6B
 */

class Mdl_entry_s6b extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input($data)
    {
    	$this->db->insert('mst_luas_pelabuhan', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.mst_luas_pelabuhan SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_luas_pelabuhan=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }

    /*public function edit($array_to_edit)
    {
            foreach ($array_to_edit as $id_produksi => $data) {
                $this->db->where('id_produksi', $id_produksi);
                $this->db->update('mst_luas_pelabuhan',$data);
            }
    }*/

    public function edit($array_to_edit)
    {
        $this->db->where('id_luas_pelabuhan', $array_to_edit['id_luas_pelabuhan']);
        $query = $this->db->update('mst_luas_pelabuhan',$array_to_edit);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

    // TODO data produksi  (View S6B)
    // TODO delete produksi (Delete S6B)
    // TODO edit produksi (Edit S6B)
}