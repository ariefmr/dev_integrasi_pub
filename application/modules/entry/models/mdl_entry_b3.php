<?php
/*
 * class Mdl_B3
 */

class Mdl_entry_B3 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function input_batch($data)
    {
    	$this->db->insert_batch('trs_pemasaran_keluar', $data);
    	//echo $this->db->last_query();

    }

    public function delete($id)
    {
        $id_pengguna_ubah = $this->session->userdata('id_pengguna');

        $sql = " UPDATE db_pipp.trs_pemasaran_keluar SET aktif='Tidak', id_pengguna_ubah=$id_pengguna_ubah, tanggal_ubah = now()  WHERE id_pemasaran_keluar ='$id'" ;
        
        $query = $this->db->query($sql);

    }

    public function edit($array_edit)
    {

        //$sql = " UPDATE db_pipp.trs_pemasaran_keluar SET id_jenis_kondisi_ikan='$id_jenis_kondisi_ikan', jml_ikan='$jml_ikan', harga_ikan='$harga_ikan' WHERE id_pemasaran_keluar='$id_pemasaran_keluar' " ;
        $this->db->where('id_pemasaran_keluar', $array_edit['id_pemasaran_keluar']);
        $this->db->update('trs_pemasaran_keluar', $array_edit); 

    }

    // TODO data produksi  (View B3)
    // TODO delete produksi (Delete B3)
    // TODO edit produksi (Edit B3)
}