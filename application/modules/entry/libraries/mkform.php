<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Mkform
{
      
      
      function input($attr)
      { 
        $is_hidden = preg_match('/hide/',$attr['input_class']) ? 'hide' : '';
         $html = '<div class="form-group '.$is_hidden.' ">';
         $html .= $attr['label_class'] !== "hide" ? '<label for="'.$attr['input_id'].'" class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>' : '';
         $html .=       '<div class="'.$attr['input_width'].'">
                         <input id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" type="'.$attr['input_type'].'" class="'.$attr['input_class'].'" value="'.$attr['input_value'].'" placeholder="'.$attr['input_placeholder'].'">
                         </div>
                       </div>';                       
         return $html;
      }

      function textarea($attr)
      { 
        $is_hidden = preg_match('/hide/',$attr['input_class']) ? 'hide' : '';
         $html = '<div class="form-group '.$is_hidden.' ">';
         $html .= $attr['label_class'] !== "hide" ? '<label for="'.$attr['input_id'].'" class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>' : '';
         $html .=       '<div class="'.$attr['input_width'].'">
                         <textarea cols="'.$attr['col'].'" rows="'.$attr['row'].'" id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'" placeholder="'.$attr['input_placeholder'].'">'.$attr['input_value'].'</textarea>
                         </div>
                       </div>';                       
         return $html;
      }


      function inputPerbekalan()
      {
        $CI =& get_instance();
        $CI->load->model('tables/mdl_tables');

        $list_perbekalan = $CI->mdl_tables->pipp_get_all('mst_jenis_perbekalan');

        $html = ' 
                    <div class="row" id="kolom-perbekalan"> <!-- main-row -->
                      <div class="col-lg-12"> <!-- col-lg-8 -->

                        <div class="form-group">
                          <label class="col-lg-12">Perbekalan</label>
                        </div>

                        <div class="form-group"> <!-- input form-group -->
                        ';
          $divider = 0;
         foreach ($list_perbekalan as $item) {
            
              if($divider === 0){
                $html .= ' <div class="row" style="margin-bottom: 0.5em ;"> <!-- row input-->
                ';
              }
              if($divider <= 3){
                $html .= '
                <label for="perbekalan_'.$item->id_jns_perbekalan.'" class="control-label col-lg-2">'.$item->nama_jns_perbekalan.' :</label>';
                $html .= '<input id="perbekalan_'.$item->id_jns_perbekalan.'" name="perbekalan-'.$item->id_jns_perbekalan.'-'.$item->nama_jns_perbekalan.'" 
                          type="text" class="input-perbekalan form-control col-lg-2" placeholder="" style="width: 5em !important;"> ';
                $html .= '<label class="col-lg-1">'.$item->satuan.'</label>';
                $divider++;
              }   
              if($divider === 3){
                $html .= '</div> <!-- END row input-->

                ';
                $divider = 0;
              }
            }
        $html .='
                
                </div>  <!-- END input form-group -->
                </div> <!-- END col-lg-8 -->
                </div> <!-- END main-row -->';
        return $html;
      }

      function pilih_pelabuhan($attr)
      {    
          $array_pelabuhan = Modules::run('mst_pelabuhan/array_pelabuhan'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select multiple id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pelabuhan as $pelabuhan) {
                        $selected = $pelabuhan->id === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$pelabuhan->id.'" '.$selected.'>'.$pelabuhan->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              placeholder: ""
                                            });
                </script>'; 

          return $html.$js;
      }

      function pilih_pelabuhan_dropdown($attr)
      {
        $array_pelabuhan = Modules::run('mst_pelabuhan/array_pelabuhan_lama'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pelabuhan as $pelabuhan) {
                        $selected = $pelabuhan->id === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$pelabuhan->id.'" '.$selected.'>'.$pelabuhan->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              placeholder: "'.$attr['placeholder'].'"
                                            });
                </script>'; 

          return $html.$js;
      }

      function pilih_pelabuhan_lama($attr)
      {    
            $array_pelabuhan = Modules::run('mst_pelabuhan/array_pelabuhan_lama'); 
            if(isset($attr['input_placeholder'])){
              $input_placeholder = $attr['input_placeholder'];
            }else{
              $input_placeholder = '';
            }
           $max_select = !isset($attr['max_select']) ? "3" : $attr['max_select']; 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select multiple id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pelabuhan as $pelabuhan) {
                        $selected = $pelabuhan->id === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$pelabuhan->id.'" '.$selected.'>'.$pelabuhan->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              placeholder: "'.$input_placeholder.'",
                                              maximumSelectionSize: '.$max_select.'
                                            });
                </script>'; 

          return $html.$js;
      }

      function pilih_pelabuhan_multiple($attr)
      {    
            $array_pelabuhan = Modules::run('mst_pelabuhan/array_pelabuhan_lama'); 
            if(isset($attr['input_placeholder'])){
              $input_placeholder = $attr['input_placeholder'];
            }else{
              $input_placeholder = '';
            }
       
           $max_select = !isset($attr['max_select']) ? "3" : $attr['max_select']; 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select multiple id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pelabuhan as $pelabuhan) {
                        $html .= '<option value="'.$pelabuhan->id.'" >'.$pelabuhan->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              placeholder: "'.$input_placeholder.'",
                                              maximumSelectionSize: '.$max_select.'
                                            });';
          if(!empty($attr['opsi_selected'])){
              $selected = implode(",",$attr['opsi_selected']);
              $js .= '$("#'.$attr['input_id'].'").select2("val", ['.$selected.']);';
          }
          $js .='</script>'; 

          return $html.$js;
      }

      //bikin pilih industri
      function pilih_industri($attr)
      {    
          $array_industri = Modules::run('mst_industri/array_industri', $attr['id_pelabuhan_user']); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_industri as $industri) {
                        $selected = $industri->id === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$industri->id.'" '.$selected.'>'.$industri->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          return $html;
      }

      //pilih jenis perbekalan
      function pilih_perbekalan($attr)
      {    
          $array_perbekalan = Modules::run('tables/pipp_list_all', 'mst_jenis_perbekalan'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_perbekalan as $perbekalan) {
                        $selected = $perbekalan->id_jns_perbekalan === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$perbekalan->id_jns_perbekalan.'" data-tipe-satuan="'.$perbekalan->satuan.'" '.$selected.'>'.$perbekalan->nama_jns_perbekalan.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          return $html;
      }

      //pilih jenis pilih_status_pelabuhan
      function pilih_status_pelabuhan($attr)
      {    
          $array_pilih = Modules::run('mst_pelabuhan/array_status_pelabuhan'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pilih as $item) {
                        $selected = $item->id_status_pelabuhan === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$item->id_status_pelabuhan.'" '.$selected.'>'.$item->status_pelabuhan.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          return $html;
      }

      //pilih jenis pilih wpp
      function pilih_wpp($attr)
      {    
          $array_pilih = Modules::run('mst_wpp/array_wpp'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pilih as $item) {
                        $selected = $item->id_wpp === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$item->id_wpp.'" '.$selected.'>'.$item->nama_wpp.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          return $html;
      }

      //pilih jenis pilih wpp
      function pilih_provinsi($attr)
      {    
          $array_pilih = Modules::run('mst_pelabuhan/array_status_pelabuhan'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pilih as $item) {
                        $selected = $item->id_status_pelabuhan === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$item->id_status_pelabuhan.'" '.$selected.'>'.$item->status_pelabuhan.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          return $html;
      }

      function pilih_ikan_multiple($attr)
      {    
          $array_pilih = Modules::run('mst_jenis_ikan/array_ikan'); 
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select multiple id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_pilih as $item) {
                        $html .= '<option value="'.$item->id.'">'.$item->text.'</option>';
                  }
          $html .= '</select>
                    </div>
                    </div>';
          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              maximumSelectionSize: '.$attr['max_select'].'
                                            });';
          if(!empty($attr['opsi_selected'])){
              $selected = implode(",",$attr['opsi_selected']);
              $js .= '$("#'.$attr['input_id'].'").select2("val", ['.$selected.']);';
          }
          $js .= '</script>'; 

          return $html.$js;
      }

      function pilih_dpi($attr)
      {
        $list_dpi_wpp = Modules::run('mst_dpi/list_dpi_wpp');
           $html = '<div class="form-group">';
           $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
           $html .= '<div class="'.$attr['input_width'].'">
                    <select multiple id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($list_dpi_wpp as $no_wpp => $array_dpi) {
                    $html .= '<optgroup label="'.$no_wpp.'">';
                      foreach ($array_dpi as $item) {
                        $html .= '<option value="'.$item['id_dpi'].'">'.$item['nama_dpi'].'</option>';
                      }
                    $html .= '</optgroup>';
                  }
          $html .= '</select>
                    </div>
                    </div>';

          $js = '<script>
                $("#'.$attr['input_id'].'").select2({  width: "100%",
                                              placeholder: "Mulai ketik nama DPI"
                                            });
                </script>'; 

          return $html.$js;
      }

      function ref_text($array_info)
      {
          $html = '<dl class="dl-horizontal text-ref">';

                  foreach ($array_info as $label => $text) {
                    $html .= '<dt>'.$label.'</dt>
                              <dd id="'.$text['id'].'">'.$text['value'].'</dd>';
                  }
                      
          $html .='</dl>';
         return $html;
      }

      function tbl_ref_text($array_info)
      {
        $hide = array('id_pengguna_buat','tanggal_buat','id_pengguna_ubah', 'tanggal_ubah', 'gambar');

          $html = '<dl class="dl-horizontal text-ref">';
                  foreach ($array_info as $label => $text) {                    
                    if($text === FALSE)
                    {
                    $html .= '<dt>Tidak ada data</dt>
                              <dd> - </dd>';
                    }elseif( strpos($label, 'id_') === FALSE  && strpos($label, 'is_') === FALSE && !in_array($label, $hide) ){
                    $text = ($label === 'tgl_catat') ? fmt_tgl($text) : $text;
                    $html .= '<dt>'.fmt_field_name($label).'</dt>
                              <dd id="id_'.$label.'">'.kos($text,'Tidak ada data.').'</dd>';
                    }
                  }
                      
          $html .='</dl>';
         return $html;
      }
      function dropdown($attr)
      {
         $array_opsi = array();
         if( $attr['array_opsi'] === '' )
         {
            $get_opsi = Modules::run('tables/pipp_list_all', $attr['from_table'], $attr['field_text']);
            //var_dump('holaho',$get_opsi);
            foreach ($get_opsi as $index => $record) {
                $temp_array = array();
                $temp_value = '';
                $temp_text = '';
                  foreach ($record as $key => $value) {
                      if($key === $attr['field_value'] )
                      { 
                        $temp_value = $value;
                      }

                      if($key === $attr['field_text'] )
                      {
                        $temp_text = $value;
                      }
                  }
                $temp_array = array( $temp_value => $temp_text );
                $array_opsi = $temp_array + $array_opsi;
            }

         } else {
            $array_opsi = $attr['array_opsi'];
         }
         $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
         $html .= '<div class="'.$attr['input_width'].'">
                         <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
          foreach ($array_opsi as $value => $text) {
                if(is_object($text)){
                  $array_values = (array) $text;
                  $selected = (String) $array_values[ $attr['field_id'] ] === $attr['opsi_selected'] ? 'selected' : '';
                  $html .= '<option value="'.$array_values[ $attr['field_id'] ].'" '.$selected.'>'.$array_values[ $attr['field_text'] ].'</option>';
                }else{
                  $selected = (String)$value === $attr['opsi_selected'] ? 'selected' : '';
                  $html .= '<option value="'.$value.'" '.$selected.'>'.$text.'</option>';
                }                
          }
         $html .= '</select>
                       </div>
                  </div>';

         return $html;
      }

      function dropdown_dss($attr)
      {
         $array_opsi = array();
         if( $attr['array_opsi'] === '' )
         {
            $get_opsi = Modules::run('tables/dss_list_all', $attr['from_table'], $attr['field_text']);
            //var_dump('holaho',$get_opsi);
            foreach ($get_opsi as $index => $record) {
                $temp_array = array();
                $temp_value = '';
                $temp_text = '';
                  foreach ($record as $key => $value) {
                      if($key === $attr['field_value'] )
                      { 
                        $temp_value = $value;
                      }

                      if($key === $attr['field_text'] )
                      {
                        $temp_text = $value;
                      }
                  }
                $temp_array = array( $temp_value => $temp_text );
                $array_opsi = $temp_array + $array_opsi;
          }

         } else {
            $array_opsi = $attr['array_opsi'];
         }
         $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
         $html .= '<div class="'.$attr['input_width'].'">
                         <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_opsi as $value => $text) {
                        $selected = (String)$value === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$value.'" '.$selected.'>'.$text.'</option>';
                  }
         $html .= '</select>
                       </div>
                  </div>';

         return $html;
      }

      function checkbox($attr)
      {
        $html = '<div class="form-group">';
        $html .= '<label class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>';
          foreach ($attr['array_checkbox'] as $name => $label) {
            $html .= '<label class="checkbox-inline"><input type="checkbox" id="'.$name.'" value="1">'.$label.'</label>';
          }
        $html .= '</div>';
        return $html;
      }

      function datepick_range($attr)
      {
        $set_mindate = !isset($attr['free_date']) ? 'minDate: new Date("2013-01-01"),' : 'changeMonth: true, changeYear: true,';
        $set_date_awal = empty($attr['input_value_1']) ? "new Date('".date('Y-m-01')."')" : "new Date('".$attr['input_value_1']."')";
        $set_date_akhir = empty($attr['input_value_2']) ? "new Date('".date('Y-m-t')."')" : "new Date('".$attr['input_value_2']."')";

         $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "hide" ? '<label for="'.$attr['input_id_1'].'" class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>' : '';
         $html .=       '<div class="'.$attr['input_width'].'">
                         <input id="'.$attr['input_id_1'].'" name="'.$attr['input_name_1'].'" type="hidden">
                         <input id="'.$attr['input_id_1'].'_display" 
                                readonly="true"
                                name="'.$attr['input_name_1'].'_display"
                                type="'.$attr['input_type'].'"
                                class="'.$attr['input_class'].'"
                                placeholder="'.$attr['placeholder_1'].'" >
                         </div>
                         <div class="'.$attr['input_width'].'">
                         <input id="'.$attr['input_id_2'].'" name="'.$attr['input_name_2'].'" type="hidden">
                         <input id="'.$attr['input_id_2'].'_display"
                                readonly="true"
                                name="'.$attr['input_name_2'].'_display" 
                                type="'.$attr['input_type'].'"
                                class="'.$attr['input_class'].'" 
                                placeholder="'.$attr['placeholder_2'].'" >
                         </div>
                       </div>';
         //TODO : set mindate dari konfigurasi
        
         $js =  "<script>
                $(document).ready(function() {
                  $('#".$attr['input_id_1']."_display').datepicker({  
                                                            altField: '#".$attr['input_id_1']."',
                                                            altFormat: 'yy-mm-dd',
                                                            dateFormat: 'dd/mm/yy',
                                                            defaultDate: '+1w',
                                                            changeMonth: true,
                                                            numberOfMonths: 2,
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                                                            onClose: function ( selectedDate ){
                                                              $('#".$attr['input_id_2']."_display').datepicker( 'option', 'minDate', selectedDate );
                                                            }
                                                         });
                  $('#".$attr['input_id_1']."_display').datepicker('setDate', $set_date_awal);

                  $('#".$attr['input_id_2']."_display').datepicker({  
                                                            altField: '#".$attr['input_id_2']."',
                                                            altFormat: 'yy-mm-dd',
                                                            dateFormat: 'dd/mm/yy',
                                                            defaultDate: '+1w',
                                                            changeMonth: true,
                                                            numberOfMonths: 2,
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                                                            onClose: function ( selectedDate ){
                                                              $('#".$attr['input_id_1']."_display').datepicker( 'option', 'maxDate', selectedDate );
                                                            }
                                                         });
                  $('#".$attr['input_id_2']."_display').datepicker('setDate', $set_date_akhir);
                  
                  ";

          $js .= "});
                </script>";                      
         return $html.$js;
      }

      function datepick($attr)
      {
        $set_mindate = !isset($attr['free_date']) ? 'minDate: new Date("2013-01-01"),' : 'changeMonth: true, changeYear: true,';
        $set_value_date = !isset($attr['input_value']) ? "new Date()" : "new Date('".$attr['input_value']."')";
         $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "hide" ? '<label for="'.$attr['input_id'].'" class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>' : '';
         $html .=       '<div class="'.$attr['input_width'].'">
                         <input id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" type="hidden">
                         <input id="'.$attr['input_id'].'_display" readonly="true" name="'.$attr['input_name'].'_display" type="'.$attr['input_type'].'" class="'.$attr['input_class'].'" placeholder="'.$attr['input_placeholder'].'">
                         </div>
                       </div>';
         //TODO : set mindate dari konfigurasi
         $js =  "<script>
                $(document).ready(function() {
                  $('#".$attr['input_id']."_display').datepicker({  
                                                            altField: '#".$attr['input_id']."',
                                                            altFormat: 'yy-mm-dd',
                                                            ".$set_mindate."
                                                            maxDate: new Date(),
                                                            defaultDate: new Date(),
                                                            dateFormat: 'dd/mm/yy',
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab']
                                                         });
                  $('#".$attr['input_id']."_display').datepicker( 'setDate', ".$set_value_date." );
                  $('#".$attr['input_id']."_display').focus(function(){ 
                          $('#ui-datepicker-div').css('z-index', '9999');
                  });";

                if($attr['input_id'] === "tgl_catat"){
                  $js .= "$('#".$attr['input_id']."_display').prop('disabled', true);
                          if( $('#info_tgl_catat_global').length > 0 ){
                            $('#info_tgl_catat_global').text( $('#".$attr['input_id']."_display').val() );
                          }";
                } 
          $js .= "});
                </script>";                      
         return $html.$js;
      }

      function datepick_bulan_tahun($attr)
      {
        // $set_value_date = !isset($attr['input_value']) ? "new Date()" : "new Date('".$attr['input_value']."')";
        if(isset($attr['input_name'])){
          $input_name = 'name="'.$attr['input_name'].'"';
        }else{
          $input_name = '';
        }
        $html = '<button type="button" title="Klik tombol untuk ganti bulan tahun." data-bulan="'.date('n').'" data-tahun="'.date('Y').'"
                              id="'.$attr['button_id'].'" 
                              class="btn btn-default">
                '.$attr['default_text'].'              
                </button>
                <input type="hidden" '.$input_name.' id="'.$attr['datepick_id'].'" value="'.$attr['input_value'].'">';
         //TODO : set mindate dari konfigurasi
         $js =  "<script>
                $(document).ready(function() {
                          $('#".$attr['datepick_id']."').datepicker({                
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                maxDate: new Date(),
                                defaultDate: new Date('".$attr['input_value']."'),
                                dateFormat: 'yy-mm-dd',
                                monthNamesShort: nama_bulan_short,
                                onClose: function(dateText, inst) { 
                                    var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
                                    var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
                                    var newDate = new Date(year, month, 1);
                                    var valDate = $(this).val();
                                    var fmtDate = format_thnbln_to_str(newDate);
                                    $(this).datepicker('setDate', newDate);  
                                    $('#".$attr['button_id']."').text(fmtDate);
                                    $('#".$attr['button_id']."').data('bulan',(+month)+1);
                                    $('#".$attr['button_id']."').data('tahun',year);  
                                },
                                beforeShow: function (input, inst) {
                                    $('#ui-datepicker-div').addClass('datepick-bulan-tahun');
                                }
                            });
    
                    $('#".$attr['button_id']."').click(function() {
                        $('#".$attr['datepick_id']."').datepicker('show').datepicker('widget').show().position({
                                                                    my: 'right bottom',
                                                                    at: 'right bottom',
                                                                    of: this 
                                                                });
                    });
                  ";
          $js .= "});
                </script>";                      
         return $html.$js;
      }

      function pilih_jenis($attr, $opsi)
      {
        $array_opsi = $opsi;
        $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
         $html .= '<div class="'.$attr['input_width'].'">
                         <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_opsi as $item) {
                       $selected = $value === $attr['opsi_selected'] ? 'selected' : '';
                       $html .= '<option value="'.$item->id_status_pelabuhan.'" '.$item->status_pelabuhan.'>'.$text.'</option>';
                  }
         $html .= '</select>
                       </div>
                  </div>';

         return var_dump($array_opsi);

      }

      function modal_edit($attr)
      {
          $html = '<div id="'.$attr["modal_id"].'" class="modal fade" tabindex="-1" data-width="'.$attr["modal_width"].'" style="display: none;">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">'.$attr["modal_title"].'</h4>
                    </div>
                    <div class="modal-body" id="form_edit_container"></div>
                  </div>';
          $js = '<script>
                $(document).ready(function(){
                   $(".edit_this").on("click", function(){
                    var thisIdRecord = $(this).data("idRecord");
                    var link_view_form = "'.$attr["link_view_form"].'"+"/"+thisIdRecord;
                    //$("#form_edit_container").load(link_view_form, function() { console.log("yes"); set_validation(); } );
                    $("#form_edit_container").load(link_view_form);
                    $("#'.$attr["modal_id"].'").modal("show");
                  });
                });
                </script>';
          return $html.$js;
      }

      function modal_detail($attr)
      {
          $html = '<div id="'.$attr["modal_id"].'" class="modal fade" tabindex="-1" data-width="'.$attr["modal_width"].'" style="display: none;">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">'.$attr["modal_title"].'</h4>
                    </div>
                    <div class="modal-body" id="form_edit_container"></div>
                  </div>';
          $js = '<script>
                $(document).ready(function(){
                   $(".detail_this").on("click", function(e){
                    e.stopPropagation();
                    e.preventDefault();
                    var thisIdRecord = $(this).data("idRecord");
                    var link_view_form = "'.$attr["link_view_form"].'"+"/"+thisIdRecord;
                    $("#form_edit_container").html("<h3>Harap tunggu</h3>");
                    $("#form_edit_container").load(link_view_form);
                    $("#'.$attr["modal_id"].'").modal("show");
                  });
                });
                </script>';
          return $html.$js;
      }

      function generate_link_edit($attr)
      {
          $tes = 'nama_jenis_pelabuhan';
          $pos = strripos($attr, '_')+1;
          $num = substr($attr, 0,4);
          $num2 = substr($attr, 5,5);
          $num3 = substr($attr, 11);

          echo $attr;
          echo '<br>';
          echo $pos;
          echo '<br>';
          echo $num;
          echo '<br>';
          echo $num2;
          echo '<br>';
          echo $num3;
          echo '<br>';
        

        
      }
      
}
 
/* End of file Template.php */
/* Location: ./application/libraries/Template.php */