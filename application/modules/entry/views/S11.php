<?php
if( !$detail_pegawai ){
    $detail_pegawai = array(
                   'id_pegawai' => '',
                   'nip' => '',
                   'status' => '',
                   'nama_pegawai' => '',
                   'tempat_lahir' => '',
                   'tgl_lahir' => '',
                   'gol' => '',
                   'gol_tmt' => '',
                   'pangkat' => '',
                   'jabatan' => '',
                   'jabatan_tmt' => '',
                   'seksi_bidang' => '',
                   'mulai_kerja' => '',
                   'penempatan' => ''
                   );
    $submit_form = 'entry/S11/input';
}else{
    $submit_form = 'entry/S11/update';
}

//dulunya hidden input
echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

$hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
$hide = array(
                            'input_id' => 'id_pegawai', 
                            'input_name' => 'id_pegawai' , 
                            'label_text' => '',
                            'input_value' => $detail_pegawai["id_pegawai"], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php

    $nama_pegawai = array(
                                'input_id' => 'nama_pegawai', 
                                'input_name' => 'nama_pegawai' , 
                                'label_text' => 'Nama Pegawai <em>*</em> :',
                                'input_value' => $detail_pegawai["nama_pegawai"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_pegawai);

    $nip = array(
                                'input_id' => 'nip', 
                                'input_name' => 'nip' , 
                                'label_text' => 'NIP <em>*</em> :',
                                'input_value' => $detail_pegawai["nip"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control validate[required]' 
                                );
    echo $this->mkform->input($nip);

    /*$status = array(
                                'input_id' => 'status', 
                                'input_name' => 'status' , 
                                'label_text' => 'Status :',
                                'input_value' => $detail_pegawai["status"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($status);*/

    $opsi = array(
                   'CPNS' => 'CPNS',
                   'PNS' => 'PNS',
                   'Honorer' => 'Honorer',
                   'Tenaga Lepas' => 'Tenaga Lepas',
                  );
    $status = array(
                                'input_id' => 'status', 
                                'input_name' => 'status',
                                'label_text' => 'Status Pegawai :', 
                                'array_opsi' => $opsi, 
                                'opsi_selected' => 'PNS', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($status);

    $tempat_lahir = array(
                                'input_id' => 'tempat_lahir', 
                                'input_name' => 'tempat_lahir' , 
                                'label_text' => 'Tempat Lahir <em>*</em> :',
                                'input_value' => $detail_pegawai["tempat_lahir"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tempat_lahir);

    $tgl_lahir = array(
                                'input_id' => 'tgl_lahir', 
                                'input_name' => 'tgl_lahir' , 
                                'label_text' => 'Tanggal Lahir <em>*</em> :', 
                                'input_value' => $detail_pegawai["tgl_lahir"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control',
                                'free_date' => 'yes'
                                // 'min_date' => ''
                                );
    echo $this->mkform->datepick($tgl_lahir);

    $opsi = array(
                   '-' => '-',
                   'I' => 'I',
                   'II' => 'II',
                   'III' => 'III',
                   'IV' => 'IV'
                  );
    $gol = array(
                                'input_id' => 'gol', 
                                'input_name' => 'gol',
                                'label_text' => 'Golongan <em>*</em> :',
                                'array_opsi' => $opsi, 
                                'opsi_selected' => $detail_pegawai["gol"],  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($gol);

    $gol_tmt = array(
                                'input_id' => 'gol_tmt', 
                                'input_name' => 'gol_tmt' , 
                                'label_text' => 'Golongan TMT <em>*</em> :', 
                                'input_value' => $detail_pegawai["gol_tmt"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control',
                                'free_date' => 'yes'
                                // 'min_date' => ''
                                );
    echo $this->mkform->datepick($gol_tmt);

    $pangkat = array(
                                'input_id' => 'pangkat', 
                                'input_name' => 'pangkat' , 
                                'label_text' => 'Pangkat <em>*</em> :',
                                'input_value' => $detail_pegawai["pangkat"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($pangkat);

    $jabatan = array(
                                'input_id' => 'jabatan', 
                                'input_name' => 'jabatan' , 
                                'label_text' => 'Jabatan <em>*</em> :',
                                'input_value' => $detail_pegawai["jabatan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jabatan);

    $jabatan_tmt = array(
                                'input_id' => 'jabatan_tmt', 
                                'input_name' => 'jabatan_tmt' , 
                                'label_text' => 'Jabatan TMT <em>*</em> :', 
                                'input_value' => $detail_pegawai["jabatan_tmt"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control',
                                'free_date' => 'yes'
                                );
    echo $this->mkform->datepick($jabatan_tmt);

    $opsi_seksi = array(
                   'Kepala Pelabuhan' => 'Kepala Pelabuhan',
                   'Bagian Tata Usaha' => 'Bagian Tata Usaha',
                   'Bidang Pengembangan' => 'Bidang Pengembangan',
                   'Bidang Tata Operasional' => 'Bidang Tata Operasional',
                   'Fungsional' => 'Fungsional',
                   'Lainnya' => 'Lainnya'
                  );

    $seksi_bidang = array(
                                'input_id' => 'seksi_bidang', 
                                'input_name' => 'seksi_bidang',
                                'label_text' => 'Seksi Bidang <em>*</em> :',
                                'array_opsi' => $opsi_seksi, 
                                'opsi_selected' => $detail_pegawai["seksi_bidang"],  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($seksi_bidang);
    // echo $this->mkform->input($seksi_bidang);

    $mulai_kerja = array(
                                'input_id' => 'mulai_kerja', 
                                'input_name' => 'mulai_kerja' , 
                                'label_text' => 'Mulai Kerja :', 
                                'input_value' => $detail_pegawai["mulai_kerja"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control',
                                'free_date' => 'yes'
                                );
    echo $this->mkform->datepick($mulai_kerja);

    $penempatan = array(
                                'input_id' => 'penempatan', 
                                'input_name' => 'penempatan' , 
                                'label_text' => 'Penempatan :',
                                'input_value' => $detail_pegawai["penempatan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($penempatan);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_pegawai").addClass("validate[required]]");
          $("#nip").addClass("validate[custom[onlyNumberSp]]");
          $("#status").addClass("validate[custom[onlyLetterSp]");
          $("#tempat_lahir").addClass("validate[required,custom[onlyLetterSp]]");
          $("#tgl_lahir").addClass("validate[required]");
          $("#gol").addClass("validate[required]");
          $("#gol_tmt").addClass("validate[required]");
          $("#pangkat").addClass("validate[required]");
          $("#jabatan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jabatan_tmt").addClass("validate[required]");
          $("#seksi_bidang").addClass("validate[custom[onlyLetterSp]");
          $("#mulai_kerja").addClass("validate[required]");
          $("#penempatan").addClass("validate[custom[onlyLetterSp]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

