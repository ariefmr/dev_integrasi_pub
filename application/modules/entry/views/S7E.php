<?php
if( !$list_S7E ){
    $list_S7E = array(
                   'id_kolam_pelabuhan' => '',
                   'dalam_rencana' => '',
                   'kondisi_perairan' => '',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'lebar_mulut_kolam' => '',
                   'luas_kolam' => '',
                   'dalam_sekarang' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'gambar' => '',
                   'ket_sumber_dana' => ''
                   );
    $submit_form = 'entry/S7E/input';
}else{
    $submit_form = 'entry/S7E/update';
}

echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_kolam_pelabuhan', 
                            'input_name' => 'id_kolam_pelabuhan' , 
                            'label_text' => '',
                            'input_value' => $list_S7E['id_kolam_pelabuhan'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input

    $dalam_rencana = array(
                                'input_id' => 'dalam_rencana', 
                                'input_name' => 'dalam_rencana' , 
                                'label_text' => 'Kedalaman Rencana (m) <m>*<m> :',
                                'input_value' => $list_S7E['dalam_rencana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($dalam_rencana);  

    $kondisi_perairan = array(
                                'input_id' => 'kondisi_perairan', 
                                'input_name' => 'kondisi_perairan' , 
                                'label_text' => 'Kondisi Perairan <m>*<m> :',
                                'input_value' => $list_S7E['kondisi_perairan'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($kondisi_perairan);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan / Rehabilitasi <m>*<m> :',
                                'input_value' => $list_S7E['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehabilitasi :',
                                'input_value' => $list_S7E['ket_rehab'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);

    $luas_kolam = array(
                                'input_id' => 'luas_kolam', 
                                'input_name' => 'luas_kolam' , 
                                'label_text' => 'Luas Kolam <m>*<m> :',
                                'input_value' => $list_S7E['luas_kolam'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($luas_kolam);

    $lebar_mulut_kolam = array(
                                'input_id' => 'lebar_mulut_kolam', 
                                'input_name' => 'lebar_mulut_kolam' , 
                                'label_text' => 'Lebar Mulut Kolam <m>*<m> :',
                                'input_value' => $list_S7E['lebar_mulut_kolam'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lebar_mulut_kolam);

    $dalam_sekarang = array(
                                'input_id' => 'dalam_sekarang', 
                                'input_name' => 'dalam_sekarang' , 
                                'label_text' => 'Kedalaman Sekarang (m) <m>*<m> :',
                                'input_value' => $list_S7E['dalam_sekarang'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($dalam_sekarang);

    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S7E['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);

    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <m>*<m> :',
                                'input_value' => $list_S7E['sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);

    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai :',
                                'input_value' => $list_S7E['nilai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);

    $ket_sumber_dana = array(
                                'input_id' => 'ket_sumber_dana', 
                                'input_name' => 'ket_sumber_dana' , 
                                'label_text' => 'Keterangan Sumber Dana :',
                                'input_value' => $list_S7E['ket_sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_sumber_dana);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S7E['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#dalam_rencana").addClass("validate[required,custom[onlyNumberSp]]");
          $("#kondisi_perairan").addClass("validate[custom[onlyLetterSp]]");
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");  
          $("#luas_kolam").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lebar_mulut_kolam").addClass("validate[required,custom[onlyNumberSp]]");
          $("#dalam_sekarang").addClass("validate[required,custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");
          

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
