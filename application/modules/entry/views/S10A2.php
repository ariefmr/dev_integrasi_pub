<?php
//dulunya hidden input
echo form_open('entry/S10A2/input', 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $tinggi_gel_max = array(
                                'input_id' => 'tinggi_gel_max', 
                                'input_name' => 'tinggi_gel_max' , 
                                'label_text' => 'Tinggi Gelombang Max (m) <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tinggi_gel_max);

    $bulan_gel_max = array(
                                'input_id' => 'bulan_gel_max', 
                                'input_name' => 'bulan_gel_max' , 
                                'label_text' => 'Bulan Gelombang Max <em>*</em>:',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($bulan_gel_max);

    $desk_gel = array(
                                'input_id' => 'desk_gel', 
                                'input_name' => 'desk_gel' , 
                                'label_text' => 'Deskripsi Gelombang :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($desk_gel);

    $kec_arus_max = array(
                                'input_id' => 'kec_arus_max', 
                                'input_name' => 'kec_arus_max' , 
                                'label_text' => 'Kecepatan Arus Max (m/detik):',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($kec_arus_max);

    $sumber_data = array(
                                'input_id' => 'sumber_data', 
                                'input_name' => 'sumber_data' , 
                                'label_text' => 'Sumber Data :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_data);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tinggi_gel_max").addClass("validate[required,custom[number]]");
          $("#bulan_gel_max").addClass("validate[required]");
          $("#kec_arus_max").addClass("validate[custom[number]]");
        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>