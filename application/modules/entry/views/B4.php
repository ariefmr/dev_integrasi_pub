<?php
echo form_open('entry/B4/input', 'id="form_entry" class="form-horizontal" role="form"');
?>
   <div class="panel">
    <div class="panel-body">
  <?php
  $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
    $tmpl = array ( 'table_open'  => '<table id="table_b4" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Industri','Hasil Olahan', 'Produksi', 'Harga Jual', 'Kategori','Tujuan','Keterangan');



    $table_b4 = $this->table->generate();

       $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);

    ?>
    <div class="form-group">
      <div class="col-lg-4">
        <button id="add_row" type="button" class="btn btn-default">Tambah Baris</button>
        <button id="del_row" type="button" class="btn btn-default">Kurangi Baris</button>
      </div>
    </div> 
    
    <?php

    echo $table_b4;
  ?>
          </div>
    <div class="panel-footer">
              <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>



<div id="template_inputs" class="hide">
<?php
    //dropdown untuk jenis Supplier
    //$attr_opsi_supplier = array('input_id' => 'jenisolahikan_1', 'input_name' => 'jenisolahikan_1', 'id_pelabuhan_user' => $id_pelabuhan, 'label_text' => '', 'array_opsi' => '', 'opsi_selected' => '', 
    //                        'input_width' => 'col-lg-9', 'input_class' => '', 'label_class' => 'none');
    //echo $this->mkform->pilih_industri($attr_opsi_supplier);

    $opsi = Modules::run('table/pipp_tbl_opsi', 'mst_jenis_hasil_olah');
    $jenisolahikan_1 = array('input_id' => 'jenisolahikan_1', 'input_name' => 'jenisolahikan_1', 'label_text' => '', 
        'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($jenisolahikan_1);

    //content input untuk volume
    $produksi_1 = array('input_id' => 'produksi_1', 'input_name' => 'produksi_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($produksi_1);

    $harga_1 = array('input_id' => 'harga_1', 'input_name' => 'harga_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($harga_1);
    
    $opsi2 = array(
                   '1' => 'Lokal',
                   '2' => 'Regional',
                   '3' => 'Ekspor'
                  );

    $kategori_1 = array('input_id' => 'kategori_1', 'input_name' => 'kategori_1', 'label_text' => '', 
        'array_opsi' => $opsi2, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($kategori_1);

    //content input untuk satuan hidden
    $tujuan_1 = array('input_id' => 'tujuan_1', 'input_name' => 'tujuan_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($tujuan_1);

    $ket_1 = array('input_id' => 'ket_1', 'input_name' => 'ket_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($ket_1);

 ?>
 <input id="pilih_supplier" name="pilih_supplier" type="text" class=" " placeholder="">
  <!--<input id="pil_perbekalan" name="pil_perbekalan" type="text" class="select_perbekalan" placeholder=""> -->
</div>

<script>
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k'; // TODO : Calon jadi library javascript
        list_opsi_supplier = <?php echo Modules::run('mst_industri/json_industri', $id_pelabuhan); ?>;

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
        $('#table_b4').dataTable().fnAddData( [
                tempRowNum+".",
                cloner("#template_inputs #pilih_supplier", "supplier_"+tempRowNum),
                cloner("#template_inputs #jenisolahikan_1", "jenisolahikan_"+tempRowNum),
                cloner("#template_inputs #produksi_1", "produksi_"+tempRowNum),
                cloner("#template_inputs #harga_1", "harga_"+tempRowNum),
                cloner("#template_inputs #kategori_1", "kategori_"+tempRowNum),
                cloner("#template_inputs #tujuan_1", "tujuan_"+tempRowNum),
                cloner("#template_inputs #ket_1", "ket_"+tempRowNum) 
                ] );
        $('#supplier_'+tempRowNum).select2({width: "100%",
                                              placeholder: "Mulai ketik nama supplier..",
                                              data: list_opsi_supplier //list data di javascript harus json!
                                            });
          
            rowCount++;
          /*$('#produksi_'+tempRowNum).select2({width: "100%",
                                              placeholder: "Mulai ketik nama perbekalan..",
                                              data: list_opsi_perbekalan //list data di javascript harus json!
                                            });*/
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_b4').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
      }


    $(document).ready( function () {
      

      /* Desc : Inisialisasi plugin dataTable ke element #table_b4
       * konfigurasinya untuk matiin fungsi search, info table, pagination, dan sorting
       */
      $('#table_b4').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"}
                        
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      } );

      
      // Panggil fungsi initTableRows;
      initTableRows();

      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $("#add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $("#del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });

    } );  
</script>