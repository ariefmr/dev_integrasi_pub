<?php
if( !$list_S9A ){
    $list_S9A = array(
                   'id_balai' => '',
                   'nama_balai' => '',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'jns_konstruksi' => '',
                   'luas' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'ket' => '',
                   'definisi' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S9A/input';
}else{
    $submit_form = 'entry/S9A/update';
}

//echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

$hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
$hide = array(
                            'input_id' => 'id_balai', 
                            'input_name' => 'id_balai' , 
                            'label_text' => '',
                            'input_value' => $list_S9A['id_balai'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">
  <?php

    $nama_balai = array(
                                'input_id' => 'nama_balai', 
                                'input_name' => 'nama_balai' , 
                                'label_text' => 'Nama Fasilitas <em>*</em> :',
                                'input_value' => $list_S9A['nama_balai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_balai);
    
    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun <em>*</em> :',
                                'input_value' => $list_S9A['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S9A['ket_rehab'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);
    
    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <em>*</em> :',
                                'input_value' => $list_S9A['jns_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);
    
    $luas = array(
                                'input_id' => 'luas', 
                                'input_name' => 'luas' , 
                                'label_text' => 'Luas (m2) <em>*</em> :',
                                'input_value' => $list_S9A['luas'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($luas);
    
    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S9A['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);
    
    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <em>*</em> :',
                                'input_value' => $list_S9A['sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);
    
    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai :',
                                'input_value' => $list_S9A['nilai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);
    
    $ket = array(
                                'input_id' => 'ket', 
                                'input_name' => 'ket' , 
                                'label_text' => 'Keterangan Sumber Dana:',
                                'input_value' => $list_S9A['ket'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket);
    
    $definisi = array(
                                'input_id' => 'definisi', 
                                'input_name' => 'definisi' , 
                                'label_text' => 'Definisi:',
                                'input_value' => $list_S9A['definisi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($definisi);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S9A['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
   
  ?>
  <hr>
  
    </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_balai").addClass("validate[required]");
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#luas").addClass("validate[required,custom[onlyNumberSp]]");
          $("#jns_konstruksi").addClass("validate[required]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>


