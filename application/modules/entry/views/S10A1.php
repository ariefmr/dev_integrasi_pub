<?php
//dulunya hidden input
echo form_open('entry/S10A1/input', 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $jns_tanah_dasar = array(
                                'input_id' => 'jns_tanah_dasar', 
                                'input_name' => 'jns_tanah_dasar' , 
                                'label_text' => 'Jenis Tanah Dasar <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_tanah_dasar);

    $dalam_tanah_dasar = array(
                                'input_id' => 'dalam_tanah_dasar', 
                                'input_name' => 'dalam_tanah_dasar' , 
                                'label_text' => 'Dalam Tanah Dasar :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($dalam_tanah_dasar);

    $jns_tanah_darat = array(
                                'input_id' => 'jns_tanah_darat', 
                                'input_name' => 'jns_tanah_darat' , 
                                'label_text' => 'Jenis Tanah Darat <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_tanah_darat);

    $dalam_tanah_darat = array(
                                'input_id' => 'dalam_tanah_darat', 
                                'input_name' => 'dalam_tanah_darat' , 
                                'label_text' => 'Dalam Tanah Darat :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($dalam_tanah_darat);

    $zona_gempa = array(
                                'input_id' => 'zona_gempa', 
                                'input_name' => 'zona_gempa' , 
                                'label_text' => 'Zona Gempa :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($zona_gempa);

    $sumber_data = array(
                                'input_id' => 'sumber_data', 
                                'input_name' => 'sumber_data' , 
                                'label_text' => 'Sumber Data :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_data);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#jns_tanah_dasar").addClass("validate[required,custom[onlyLetterSp]]");
          $("#dalam_tanah_dasar").addClass("validate[custom[onlyNumberSp]]");
          $("#jns_tanah_darat").addClass("validate[required,custom[onlyLetterSp]]");
          $("#dalam_tanah_darat").addClass("validate[custom[onlyNumberSp]]");
          $("#zona_gempa").addClass("validate[custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
