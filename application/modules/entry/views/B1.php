<?php

        $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
        echo $this->mkform->input($hide);
        ?>
<?php

  if(!$jasa_isi){
 		$submit_form = 'entry/B1/input';
  }else{
  	$submit_form = 'entry/B1/update';
  }

	echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

    //tabel jasa pelabuhan
    $tmpl = array ( 'table_open'  => '<table id="table_b1" class="table table-bordered valid-rp">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading(
                              'No.', 
                              'Nama Jasa', 
                              'Satuan', 
                              'Jumlah'
                              );


    $counter = 1;
    if ($jasa_isi){
      foreach ($list_B1 as $item) {

        $input_jml = array(
                      'input_id' => $item->id_pendapatan_jasa, 
                      'input_name' => $item->id_pendapatan_jasa , 
                      'label_text' => '', 
                      'input_value' => $item->jml, 
                      'input_placeholder' => '',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-9', 
                      'label_class' => '', 
                      'input_class' => 'form-control' 
                      );

        $this->table->add_row(
                              $counter, 
                              $item->nama_jns_jasa, 
                              $item->satuan, 
                              $this->mkform->input($input_jml)

                              );
        $counter++;
      }
    }else{
      foreach ($list_B1 as $item) {
        $input_jml = array(
                      'input_id' => $item->id_jns_jasa, 
                      'input_name' => $item->id_jns_jasa , 
                      'label_text' => '', 
                      'input_value' => '0', 
                      'input_placeholder' => '',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-12', 
                      'label_class' => '', 
                      'input_class' => 'form-control' 
                      );

        $this->table->add_row(
                              $counter, 
                              $item->nama_jns_jasa, 
                              $item->satuan, 
                              $this->mkform->input($input_jml)
                              );
        $counter++;
      }
    }
      
    $table_b1 = $this->table->generate();
    $this->table->clear();

?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Jasa Di Pelabuhan</text></h3>
      </div>
      <!-- <div class="row"> -->
      Ganti tanggal :
        <?php 
        $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                             'datepick_id' => 'datepicker_jurnal',
                                             'default_text' => fmt_bulan_tahun($tmp_tgl_catat),
                                             'input_name' => 'tgl_catat',
                                             'input_value' => $tmp_tgl_catat.'-01'
                                            );
        echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
        ?>

        <button type="button" id="start_filter" class="btn btn-success">Ganti</button> 

      <!-- </div> -->
      <div class="panel-body overflowed">


        <?php 
        echo "<br>";
        echo $table_b1; 
        ?>
      </div>
    </div>
  </div>
</div>

<!-- tutup tabel jasa entry -->

<?php
    //tabel non jasa pelabuhan
    $tmpl = array ( 'table_open'  => '<table id="table_b1_non" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading(
                              'No.', 
                              'Nama Non Jasa', 
                              'Satuan', 
                              'Jumlah'
                              );

    $counter = 1;
    if ($jasa_isi){
      foreach ($list_B1_non as $item) {
        $input_jml = array(
                      'input_id' => $item->id_pendapatan_non_jasa."_non", 
                      'input_name' => $item->id_pendapatan_non_jasa."_non", 
                      'label_text' => '', 
                      'input_value' => $item->jml, 
                      'input_placeholder' => '',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-12', 
                      'label_class' => '', 
                      'input_class' => 'form-control jns-'.$item->id_jns_non_jasa.''
                      );

        if($item->id_jns_non_jasa == 4){
        	$input_desk = array(
                      'input_id' => 'desk_lain', 
                      'input_name' => 'desk_lain', 
                      'label_text' => '', 
                      'input_value' => $item->desk_lain, 
                      'input_placeholder' => 'Masukan deskripsi lain',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-12', 
                      'label_class' => '', 
                      'input_class' => 'form-control' 
                      );
          $this->table->add_row(
                                $counter, 
                                $item->nama_jns_non_jasa, 
                                $item->satuan, 
                                $this->mkform->input($input_jml),
                                $this->mkform->input($input_desk)
                                );
        }else{
          $this->table->add_row(
                                $counter, 
                                $item->nama_jns_non_jasa, 
                                $item->satuan, 
                                $this->mkform->input($input_jml)
                                );
        }
        $counter++;
      }
    }else{
      foreach ($list_B1_non as $item) {
        $input_jml = array(
                      'input_id' => $item->id_jns_non_jasa."_non", 
                      'input_name' => $item->id_jns_non_jasa."_non", 
                      'label_text' => '', 
                      'input_value' => '0',
                      'input_placeholder' => '',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-12', 
                      'label_class' => '', 
                      'input_class' => 'form-control' 
                      );

        $input_desk = array(
                      'input_id' => 'desk_lain', 
                      'input_name' => 'desk_lain', 
                      'label_text' => '', 
                      'input_value' => '', 
                      'input_placeholder' => 'Masukan deskripsi lain',
                      'input_type' => 'text', 
                      'input_width' => 'col-lg-12', 
                      'label_class' => '', 
                      'input_class' => 'form-control' 
                      );

        if($item->id_jns_non_jasa == 4){
          $this->table->add_row(
                                $counter, 
                                $item->nama_jns_non_jasa, 
                                $item->satuan, 
                                $this->mkform->input($input_jml),
                                $this->mkform->input($input_desk)
                                );
        }else{
          $this->table->add_row(
                                $counter, 
                                $item->nama_jns_non_jasa, 
                                $item->satuan, 
                                $this->mkform->input($input_jml)
                                );
        }
        $counter++;
      }
    }
    
    $table_b1_non = $this->table->generate();
    $this->table->clear();

?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Non Jasa Di Pelabuhan</text></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $table_b1_non; ?>
      </div>
      <div class="panel-footer">
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
    </div>
  </div>
</div>
  
  

  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

  </form>

<script type="text/javascript">
        
        var array_uri = ['entry','B1','index'];

        function start_filter()
        {
          var new_segments = array_uri.join("/"),
              new_url = site_url+new_segments+"/",
              get_tanggal = $('#datepicker_jurnal').val(),
              arr_tanggal = get_tanggal.split('-'),
              new_tanggal = arr_tanggal[0]+"/"+arr_tanggal[1],
              url_redirect = new_url+new_tanggal; 
        
          // console.log(url_redirect);
          window.open(url_redirect,'_self');
        }
    
        function set_validation()
        {
          $(".valid-rp input, .jns-1, .jns-4").addClass("validate[custom[onlyNumberSp]]");
          $(".jns-2, .jns-3").addClass("validate[custom[onlyPercentage]]");


        }

        $(document).ready(function(){

            $("#start_filter").click(function(){
                    start_filter();
                });

            set_validation();
            $("#form_entry").validationEngine();
        });
</script>