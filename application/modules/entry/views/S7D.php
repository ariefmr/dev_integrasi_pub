<?php
if( !$list_S7D ){
    $list_S7D = array(
                   'id_dermaga' => '',
                   'nama_dermaga' => '',
                   'nama_fas' => '',
                   'jns_dermaga' => '',
                   'tipe_dermaga' => '',
                   'is_lampu' => '',
                   'posisi_tambat' => '',
                   'jns_fender' => '',
                   'jns_bolard' => '',
                   'desk_dermaga' => '',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'jns_konstruksi' => '',
                   'panjang' => '',
                   'lebar' => '',
                   'elevasi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'gambar' => '',
                   'ket_sumber_dana' => ''
                   );

    $submit_form = 'entry/S7D/input';
}else{
    $submit_form = 'entry/S7D/update';
}

echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_dermaga', 
                            'input_name' => 'id_dermaga' , 
                            'label_text' => '',
                            'input_value' => $list_S7D["id_dermaga"], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S7D["tahun"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);
    
    $nama_fas = array(
                                'input_id' => 'nama_fas', 
                                'input_name' => 'nama_fas' , 
                                'label_text' => 'Nama Fasilitas :',
                                'input_value' => $list_S7D["nama_fas"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_fas);

    $nama_dermaga = array(
                                'input_id' => 'nama_dermaga', 
                                'input_name' => 'nama_dermaga' , 
                                'label_text' => 'Nama Dermaga <m>*<m> :',
                                'input_value' => $list_S7D["nama_dermaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_dermaga);

    $opsi_jenis_dermaga = array (
         'Beton' => 'Beton',
         'Kayu' => 'Kayu',
        'Baja' => 'Baja',
        'Batu' => 'Batu'
    );

    $opsi_tipe_dermaga = array (
         'Dermaga' => 'Warf/Dermaga',
         'Jetty' => 'Jetty',
        'Causeway' => 'Causeway'
    );

    $jns_dermaga = array(
                                'input_id' => 'jns_dermaga', 
                                'input_name' => 'jns_dermaga',
                                'label_text' => 'Jenis Dermaga <m>*<m> :', 
                                'array_opsi' => $opsi_jenis_dermaga, 
                                'opsi_selected' => $list_S7D["jns_dermaga"],
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($jns_dermaga);/*

    $jns_dermaga = array(
                                'input_id' => 'jns_dermaga', 
                                'input_name' => 'jns_dermaga' , 
                                'label_text' => 'Jenis Dermaga <m>*<m> :',
                                'input_value' => $list_S7D["jns_dermaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_dermaga);*/

    $tipe_dermaga = array(
                                'input_id' => 'tipe_dermaga', 
                                'input_name' => 'tipe_dermaga',
                                'label_text' => 'Tipe Dermaga <m>*<m> :', 
                                'array_opsi' => $opsi_tipe_dermaga, 
                                'opsi_selected' => $list_S7D["tipe_dermaga"],
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($tipe_dermaga);/*

    $tipe_dermaga = array(
                                'input_id' => 'tipe_dermaga', 
                                'input_name' => 'tipe_dermaga' , 
                                'label_text' => 'Tipe Dermaga <m>*<m> :',
                                'input_value' => $list_S7D["tipe_dermaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tipe_dermaga);*/

    $is_lampu = array(
                                'input_id' => 'is_lampu', 
                                'input_name' => 'is_lampu' , 
                                'label_text' => 'Lampu <m>*<m> :',
                                'input_value' => $list_S7D["is_lampu"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($is_lampu);

    $posisi_tambat = array(
                                'input_id' => 'posisi_tambat', 
                                'input_name' => 'posisi_tambat' , 
                                'label_text' => 'Posisi Tambat <m>*<m> :',
                                'input_value' => $list_S7D["posisi_tambat"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($posisi_tambat);

    $jns_fender = array(
                                'input_id' => 'jns_fender', 
                                'input_name' => 'jns_fender' , 
                                'label_text' => 'Jenis Fender <m>*<m> :',
                                'input_value' => $list_S7D["jns_fender"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_fender);

    $jns_bolard = array(
                                'input_id' => 'jns_bolard', 
                                'input_name' => 'jns_bolard' , 
                                'label_text' => 'Jenis Bolard <m>*<m> :',
                                'input_value' => $list_S7D["jns_bolard"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_bolard);

    $desk_dermaga = array(
                                'input_id' => 'desk_dermaga', 
                                'input_name' => 'desk_dermaga' , 
                                'label_text' => 'Deskripsi Dermaga :',
                                'input_value' => $list_S7D["desk_dermaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($desk_dermaga);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S7D["ket_rehab"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);

    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <m>*<m> :',
                                'input_value' => $list_S7D["jns_konstruksi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);

    $panjang = array(
                                'input_id' => 'panjang', 
                                'input_name' => 'panjang' , 
                                'label_text' => 'Panjang <m>*<m> :',
                                'input_value' => $list_S7D["panjang"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($panjang);

    $lebar = array(
                                'input_id' => 'lebar', 
                                'input_name' => 'lebar' , 
                                'label_text' => 'Lebar <m>*<m> :',
                                'input_value' => $list_S7D["lebar"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lebar);

    $elevasi = array(
                                'input_id' => 'elevasi', 
                                'input_name' => 'elevasi' , 
                                'label_text' => 'Elevasi <m>*<m> :',
                                'input_value' => $list_S7D["elevasi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($elevasi);

    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <m>*<m> :',
                                'input_value' => $list_S7D["sumber_dana"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);

    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai <m>*<m> :',
                                'input_value' => $list_S7D["nilai"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);

    $ket_sumber_dana = array(
                                'input_id' => 'ket_sumber_dana', 
                                'input_name' => 'ket_sumber_dana' , 
                                'label_text' => 'Keterangan Sumber Dana :',
                                'input_value' => $list_S7D["ket_sumber_dana"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_sumber_dana);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S7D['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");  
          $("#nama_fasilitas").addClass("validate[required,custom[onlyLetterSp]]");
          $("#nama_dermaga").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jns_dermaga").addClass("validate[required,custom[onlyLetterSp]]");
          $("#tipe_dermaga").addClass("validate[required,custom[onlyLetterSp]]");
          $("#is_lampu").addClass("validate[required]");
          $("#posisi_tambat").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jns_fender").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jns_bolard").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jns_konstruksi").addClass("validate[required]");
          $("#panjang").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lebar").addClass("validate[required,custom[onlyNumberSp]]");
          $("#elevasi").addClass("validate[required,custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
