<?php
if( !$list_S6D ){
    $list_S6D = array(
                   'id_fas_transportasi' => '',
                   'id_jns_transportasi' => '',
                   'nama' => '',
                   'jenis' => '',
                   'jarak' => '',
                   'kota_tujuan' => '',
                   'frekuensi' => '',
                   'tgl_catat' => ''
                   );
    $submit_form = 'entry/S6D/input';
}else{
    $submit_form = 'entry/S6D/update';
}

echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_fas_transportasi', 
                            'input_name' => 'id_fas_transportasi' , 
                            'label_text' => '',
                            'input_value' => $list_S6D['id_fas_transportasi'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    /*$opsi = array(
                   '1' => 'Kereta Api',
                   '2' => 'Bus Antar Kota',
                   '3' => 'Pesawat Terbang'
                  );
    $attr_nama_jns_transportasi = array('input_id' => 'id_jns_transportasi', 'input_name' => 'id_jns_transportasi', 'label_text' => 'Jenis Fasilitas Transportasi', 'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_transportasi);*/

    //dropdown 
    $attr_nama_jns_transportasi = array(
                                'input_id' => 'id_jns_transportasi', 
                                'input_name' => 'id_jns_transportasi',
                                'label_text' => 'Jenis Fasilitas Transportasi :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6D['id_jns_transportasi'], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_transportasi', 
                                'field_value' => 'id_jns_transportasi',
                                'field_text' => 'nama_jns_transportasi'
                            );
    echo $this->mkform->dropdown($attr_nama_jns_transportasi);

    $attr_nama_fasilitas = array(
                                'input_id' => 'nama', 
                                'input_name' => 'nama' , 
                                'label_text' => 'Nama Fasilitas <m>*<m> :', 
                                'input_value' => $list_S6D['nama'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_fasilitas);

    $attr_jenis = array(
                                'input_id' => 'jenis', 
                                'input_name' => 'jenis' , 
                                'label_text' => 'Jenis :', 
                                'input_value' => $list_S6D['jenis'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jenis);
    
    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak <m>*<m> :', 
                                'input_value' => $list_S6D['jarak'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_kota_tujuan = array(
                                'input_id' => 'kota_tujuan', 
                                'input_name' => 'kota_tujuan' , 
                                'label_text' => 'Kota Tujuan :', 
                                'input_value' => $list_S6D['kota_tujuan'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kota_tujuan);

    $attr_frekuensi = array(
                                'input_id' => 'frekuensi', 
                                'input_name' => 'frekuensi' , 
                                'label_text' => 'Frekuensi (Hari) :', 
                                'input_value' => $list_S6D['frekuensi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_frekuensi);

    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => $list_S6D['tgl_catat'], 'input_placeholder' => '',
                            'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_fasilitas").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jarak").addClass("validate[required,custom[onlyNumberSp]]");
          $("#frekuensi").addClass("validate[required,custom[onlyNumberSp]]");
          


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

