<?php
if( !$list_S12 ){
    $list_S12 = array(
                'id_lembaga' => '',
                'nama_lembaga' => '',
                'alamat' => '',
                'nama_pimpinan' => '',
                'jml_tenaga' => ''
                   );
    $submit_form = 'entry/S12/input';
}else{
    $submit_form = 'entry/S12/update';
}

//dulunya hidden input
echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    $hide = array(
                                'input_id' => 'id_lembaga', 
                                'input_name' => 'id_lembaga' , 
                                'label_text' => '',
                                'input_value' => $list_S12["id_lembaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

    $nama_lembaga = array(
                                'input_id' => 'nama_lembaga', 
                                'input_name' => 'nama_lembaga' , 
                                'label_text' => 'Nama Lembaga <em>*</em>:',
                                'input_value' => $list_S12["nama_lembaga"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_lembaga);

    $alamat = array(
                                'input_id' => 'alamat', 
                                'input_name' => 'alamat' , 
                                'label_text' => 'Alamat <em>*</em>:',
                                'input_value' => $list_S12["alamat"],
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($alamat);

    $nama_pimpinan = array(
                                'input_id' => 'nama_pimpinan', 
                                'input_name' => 'nama_pimpinan' , 
                                'label_text' => 'Nama Pimpinan <em>*</em>:',
                                'input_value' => $list_S12["nama_pimpinan"],
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_pimpinan);

    $jml_tenaga = array(
                                'input_id' => 'jml_tenaga', 
                                'input_name' => 'jml_tenaga' , 
                                'label_text' => 'Jumlah Tenaga <em>*</em> :',
                                'input_value' => $list_S12["jml_tenaga"],
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml_tenaga);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_lembaga").addClass("validate[required,custom[onlyLetterSp]]");
          $("#alamat").addClass("validate[required,custom[onlyLetterSp]]");
          $("#nama_pimpinan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jml_tenaga").addClass("validate[custom[required,onlyNumberSp]]");
        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
