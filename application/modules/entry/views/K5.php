<?php
if( !$list_K5 ){
    $list_K5 = array(
                   'id_k5' => '',
                   'tipe' => '',
                   'kategori' => '',
                   'hasil_pengamatan' => '',
                   'tahun' => '',
                   'uraian' => '',
                   'jns_konstruksi' => '',
                   'panjang' => '',
                   'lebar' => '',
                   'elevasi' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'permasalahan' => '',
                   'tindak_lanjut' => '',
                   'gambar' => '',
                   'tgl_catat' => ''
                   );
    $submit_form = 'entry/K5/input';
}else{
    $submit_form = 'entry/K5/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>

<div class="panel">
    <div class="panel-body">
        
  <?php

    //hidden id untuk edit
    $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
    // FORM Input
    $hide = array(
                                'input_id' => 'id_k5', 
                                'input_name' => 'id_k5' , 
                                'label_text' => '',
                                'input_value' => $list_K5["id_k5"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

    $opsi = array(
                   'Kebersihan' => 'Kebersihan',
                   'Keamanan' => 'Keamanan',
                   'Ketertiban' => 'Ketertiban',
                   'Keselamatan' => 'Keselamatan',
                   'Kebakaran' => 'Kebakaran'
                  );
    $tipe = array(
                                'input_id' => 'tipe', 
                                'input_name' => 'tipe',
                                'label_text' => 'Tipe :', 
                                'array_opsi' => $opsi, 
                                'opsi_selected' => $list_K5['tipe'], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($tipe);

    $opsi_kategori = array(
                   'Lingkungan Pelabuhan Perikanan' => 'Lingkungan Pelabuhan Perikanan',
                   'Dermaga Bongkar' => 'Dermaga Bongkar',
                   'Tempat Pelelangan Ikan' => 'Tempat Pelelangan Ikan',
                   'Sarana Pengujian Mutu' => 'Sarana Pengujian Mutu'
                  );
    $kategori = array(
                                'input_id' => 'kategori', 
                                'input_name' => 'kategori',
                                'label_text' => 'Kategori :', 
                                'array_opsi' => $opsi_kategori, 
                                'opsi_selected' => $list_K5['kategori'], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($kategori);

    $uraian = array(
                                'input_id' => 'uraian', 
                                'input_name' => 'uraian' , 
                                'label_text' => 'Uraian :',
                                'input_value' => $list_K5["uraian"],
                                'input_placeholder' => '',
                                'row' => '3',
                                'col' => '50',
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->textarea($uraian);

    $opsi_hasil_pengamatan = array(
                   'Baik' => 'Baik',
                   'Sedang' => 'Sedang',
                   'Kurang' => 'Kurang'
                  );
    $hasil_pengamatan = array(
                                'input_id' => 'hasil_pengamatan', 
                                'input_name' => 'hasil_pengamatan',
                                'label_text' => 'Hasil Pengamatan :', 
                                'array_opsi' => $opsi_hasil_pengamatan, 
                                'opsi_selected' => $list_K5['hasil_pengamatan'], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($hasil_pengamatan);

    $permasalahan = array(
                                'input_id' => 'permasalahan', 
                                'input_name' => 'permasalahan' , 
                                'label_text' => 'Permasalahan :',
                                'input_value' => $list_K5["permasalahan"],
                                'input_placeholder' => '',
                                'row' => '5',
                                'col' => '50',
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->textarea($permasalahan);

    $tindak_lanjut = array(
                                'input_id' => 'tindak_lanjut', 
                                'input_name' => 'tindak_lanjut' , 
                                'label_text' => 'Tindak Lanjut :',
                                'input_value' => $list_K5["tindak_lanjut"],
                                'input_placeholder' => '',
                                'row' => '5',
                                'col' => '50',
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->textarea($tindak_lanjut);
    
  ?>
  <hr>
  
    </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
</div>
 
<div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
</div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");  
          $("#jns_konstruksi").addClass("validate[required]");
          $("#panjang").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lebar").addClass("validate[required,custom[onlyNumberSp]]");
          $("#elevasi").addClass("validate[required,custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>