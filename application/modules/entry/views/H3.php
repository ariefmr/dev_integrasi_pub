<?php
echo form_open('entry/H3/input', 'id="form_entry" class="form-horizontal" role="form"');
?>
                                
  <div class="panel">
    <div class="panel-body">
  <?php
   $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
    $tmpl = array ( 'table_open'  => '<table id="table_h3" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (Kg)','Harga','Asal Ikan','Transportasi');

    $table_h3 = $this->table->generate();

    $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal :', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
    echo $this->mkform->datepick($attr_datepick);
  ?>

  <div class="form-group">
    <?php  $attr_datepick = array('input_id' => 'tgl_aktivitas',
                               'input_name' => 'tgl_aktivitas' , 
                               'label_text' => 'Tanggal Catat <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text',
                                 'input_width' => 'col-lg-6', 
                                 'label_class' => 'col-lg-3 control-label',
                                  'input_class' => 'form-control validate[required] datepicker' );
        echo $this->mkform->datepick($attr_datepick); ?>
    <div class="col-lg-4">
      <button id="add_row" type="button" class="btn btn-default">Tambah Baris</button>
      <button id="del_row" type="button" class="btn btn-default">Kurangi Baris</button>
    </div>

  </div> 
  <?php

    echo $table_h3;
  ?>                     
          </div>
    <div class="panel-footer">
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>


<!-- Bagian untuk Content pada table, tambah dan kurang bari -->
<div id="template_inputs" class="hide">
<?php
    /* Elemen ini dipakai javascript untuk di clone atau diduplikasi
     */

    //nama heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (Kg)','Harga','Asal Ikan','Transportasi');

    //dropdown untuk kondisi ikan
    //array opsi di dapat dari config->custom_constants
    $attr_opsi_kondisi_ikan = array('input_id' => 'kondisi_1', 'input_name' => 'kondisi_1', 'label_text' => '', 'array_opsi' => $opsi_kondisi_ikan, 'opsi_selected' => 'segar', 
                            'input_width' => 'col-lg-9', 'input_class' => 'form-control', 'label_class' => 'none');
    echo $this->mkform->dropdown($attr_opsi_kondisi_ikan);
    
    //content Input Berat    
    $attr_berat = array('input_id' => 'jml_1', 'input_name' => 'jml_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_berat);
    
    //content input untuk Harga
    $attr_harga = array('input_id' => 'harga_1', 'input_name' => 'harga_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_harga);

    //content Input Asal Ikan
    $attr_asal_ikan = array('input_id' => 'asal_1', 'input_name' => 'asal_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_asal_ikan);

    //dropdown untuk Transportasi
    $opsi_transportasi = array('Kapal Angkut' => 'Kapal Angkut',
                               'Mobil' => 'Mobil'
                               );
    $attr_opsi_transportasi = array('input_id' => 'transportasi_1', 'input_name' => 'transportasi_1', 'label_text' => '', 'array_opsi' => $opsi_jenis_transportasi, 'opsi_selected' => 'mobil', 
                            'input_width' => 'col-lg-9', 'input_class' => 'form-control', 'label_class' => 'none');
    echo $this->mkform->dropdown($attr_opsi_transportasi);
 ?>
 <input id="pilih_ikan" name="pilih_ikan" type="text" class=" " placeholder="">
</div>

<script>
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k', // TODO : Calon jadi library javascript
        list_opsi_ikan = <?php echo Modules::run('mst_jenis_ikan/json_ikan'); ?>;

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
        $('#table_h3').dataTable().fnAddData( [
                tempRowNum+".",
                cloner("#template_inputs #pilih_ikan", "ikan_"+tempRowNum),
                cloner("#template_inputs #kondisi_1", "kondisi_"+tempRowNum),
                cloner("#template_inputs #jml_1", "jml_"+tempRowNum),
                cloner("#template_inputs #harga_1", "harga_"+tempRowNum),
                cloner("#template_inputs #asal_1", "asal_"+tempRowNum), 
                cloner("#template_inputs #transportasi_1", "transportasi_"+tempRowNum)
                ] );
            $("#ikan_"+tempRowNum).select2({  width: "100%",
                                              placeholder: "Mulai ketik nama ikan..",
                                              data: list_opsi_ikan
                                            });
            rowCount++;
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_h3').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
      }

    $(document).ready( function () {
      

      /* Desc : Inisialisasi plugin dataTable ke element #table_h3
       * konfigurasinya untuk matiin fungsi search, info table, pagination, dan sorting
       */
      $('#table_h3').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "25%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      } );

      
      // Panggil fungsi initTableRows;
      initTableRows();

      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $("#add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $("#del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });

    } );  
</script>