<?php
if( !$list_S6C ){
    $list_S6C = array(
                   'id_akses_pelabuhan' => '',
                   'id_jns_tujuan' => '',
                   'nama_tujuan' => '',
                   'jarak' => '',
                   'rute' => '',
                   'kond_rute' => '',
                   'tgl_catat' => ''
                   );
    $submit_form = 'entry/S6C/input';
}else{
    $submit_form = 'entry/S6C/update';
}

echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_akses_pelabuhan', 
                            'input_name' => 'id_akses_pelabuhan' , 
                            'label_text' => '',
                            'input_value' => $list_S6C['id_akses_pelabuhan'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    /*$opsi = array(
                   '1' => 'Ibu Kota Provinsi',
                   '2' => 'Ibu Kota Kecamatan',
                   '3' => 'Ibu Kota Kabupaten'
                  );
    $attr_nama_jns_tujuan = array('input_id' => 'id_jns_tujuan', 'input_name' => 'id_jns_tujuan', 'label_text' => 'Jenis Tujuan', 'array_opsi' => $opsi, 'opsi_selected' => 'segar', 
                            'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_tujuan);*/

    //dropdown 
    $attr_nama_jns_tujuan = array(
                                'input_id' => 'id_jns_tujuan', 
                                'input_name' => 'id_jns_tujuan',
                                'label_text' => 'Jenis Tujuan :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6C['id_jns_tujuan'], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_tujuan', 
                                'field_value' => 'id_jns_tujuan',
                                'field_text' => 'nama_jns_tujuan'
                            );
    echo $this->mkform->dropdown($attr_nama_jns_tujuan);

    $attr_nama_tujuan = array(
                                'input_id' => 'nama_tujuan', 
                                'input_name' => 'nama_tujuan' , 
                                'label_text' => 'Nama Tujuan <m>*<m> :', 
                                'input_value' => $list_S6C['nama_tujuan'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_tujuan);

    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak <m>*<m> :', 
                                'input_value' => $list_S6C['jarak'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_rute = array(
                                'input_id' => 'rute', 
                                'input_name' => 'rute' , 
                                'label_text' => 'Rute :', 
                                'input_value' => $list_S6C['rute'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_rute);
    
    $attr_kond_rute = array(
                                'input_id' => 'kond_rute', 
                                'input_name' => 'kond_rute' , 
                                'label_text' => 'Kondisi Rute :', 
                                'input_value' => $list_S6C['kond_rute'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kond_rute);

    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_tujuan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jarak").addClass("validate[required,custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
