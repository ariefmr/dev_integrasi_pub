<?php
if( !$list_S8C ){
    $list_S8C = array(
                   'id_sumber_air' => '',
                   'id_jns_sumber_air' => '',
                   'tgl_catat' => '',
                   'nama_fas_pemasaran' => '',
                   'tahun' => '',
                   'debit' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8C/input';
}else{
    $submit_form = 'entry/S8C/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_sumber_air', 
                            'input_name' => 'id_sumber_air' , 
                            'label_text' => '',
                            'input_value' => $list_S8C['id_sumber_air'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    /*$opsi = array(
                   '1' => 'Sumur Dangkal',
                   '2' => 'Sumur Bor'
                  );
    $id_jns_sumber_air = array('input_id' => 'id_jns_sumber_air', 'input_name' => 'id_jns_sumber_air', 'label_text' => 'Jenis Sumber Air', 
        'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($id_jns_sumber_air);*/
    $opsi = array(
                   '1' => 'Sumur Dangkal',
                   '2' => 'Sumur Bor'
                  );
    $id_jns_sumber_air = array(
                                'input_id' => 'id_jns_sumber_air', 
                                'input_name' => 'id_jns_sumber_air',
                                'label_text' => 'Jenis Sumber Air :', 
                                'array_opsi' => $opsi, 
                                'opsi_selected' => $list_S8C["id_jns_sumber_air"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($id_jns_sumber_air);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S8C['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $debit = array(
                                'input_id' => 'debit', 
                                'input_name' => 'debit' , 
                                'label_text' => 'Debit <m>*<m> :',
                                'input_value' => $list_S8C['debit'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($debit);

    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S8C['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8C['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#debit").addClass("validate[required,custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>


