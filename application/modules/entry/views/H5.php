<?php
$hidden_input = array('nama_kapal_info' => '');
echo form_open('entry/H5/input', 'id="form_entry" class="form-horizontal" role="form"',$hidden_input);
?>

            
            <?php echo Modules::run('mst_kapal/wgt_pilih_kapal'); ?>
           

   <div class="well well-sm panel-text-referensi">
   
            <?php                        // Label , Isi 
                  $array_info_kapal = array( 
                                              'Nama Kapal / SIPI : ' => array( 'id' => 'nama_kapal',
                                                                      'value' =>'...'),
                                              'Tanda Selar : ' => array( 'id' => 'tanda_selar',
                                                                      'value' => '...'),
                                              'Pemilik :' => array( 'id' => 'nama_pemilik',
                                                                  'value' => '...'),  
                                              'Izin WPP :' => array( 'id' => 'wpp_info',
                                                                  'value' => '...'),
                                              'Izin Pelabuhan Pangkalan :' => array( 'id' => 'pelabuhan_pangkalan_info',
                                                                  'value' => '...'),
                                              'Izin Alat Tangkap :' => array( 'id' => 'alat_tangkap_info',
                                                                  'value' => '...')
                                           ); 
                  echo $this->mkform->ref_text($array_info_kapal);
             ?>  
  </div>

  <div class="panel form-input">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-8">

  <?php
        
        $attr_datepick = array('input_id' => 'tgl_catat',
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide',
                                'label_class' => 'hide',
                                'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);
        


        $opsi_sumber_data = array( 'Tempat Pelelangan Ikan' => 'Tempat Pelelangan Ikan',
                            'Nahkoda' => 'Nahkoda',
                            'Lainnya' => 'Lainnya'
                           );
        
        // $attr_opsi_sumber = array('input_id' => 'sumber_data', 'input_name' => 'sumber_data', 'label_text' => 'Sumber Data :', 'array_opsi' => $opsi_sumber_data, 'opsi_selected' => 'Tempat Pelelangan Ikan', 
        //                     'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
        //echo $this->mkform->dropdown($attr_opsi_sumber);

        //conten Tanggal Keberangkatan
        $attr_datepick = array('input_id' => 'tgl_aktivitas',
                               'input_name' => 'tgl_aktivitas' , 
                               'label_text' => 'Tanggal Berangkat <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text',
                                 'input_width' => 'col-lg-6', 
                                 'label_class' => 'col-lg-4 control-label',
                                  'input_class' => 'form-control validate[required] datepicker' );
        echo $this->mkform->datepick($attr_datepick);

        //content tujuan keberangkatan
        $opsi_tujuan = array( 'tangkap' => 'Menangkap Ikan',
                            'angkut' => 'Mengangkut Ikan',
                            'docking' => 'Docking'
                           );
        
        $attr_opsi_tujuan = array('input_id' => 'tujuan_berangkat',
                                  'input_name' => 'tujuan_berangkat',
                                  'label_text' => 'Tujuan Berangkat <em>*</em> :',
                                  'array_opsi' => $opsi_tujuan,
                                  'opsi_selected' => 'Menangkap Ikan', 
                                  'input_width' => 'col-lg-6',
                                  'input_class' => 'form-control',
                                  'label_class' => 'col-lg-4 control-label');
        echo $this->mkform->dropdown($attr_opsi_tujuan);
        
        ?>
        <div id="pilih_wpp_dpi">
        <?php
        $attr_opsi_dpi = array('input_id' => 'id_dpi',
                               'input_name' => 'id_dpi' ,
                               'label_text' => 'DPI <em>*</em> :',
                               'input_value' => '',
                               'input_placeholder' => 'mulai ketik DPI',
                               'input_type' => 'text',
                               'input_width' => 'col-lg-8',
                               'label_class' => 'col-lg-4 control-label',
                               'input_class' => '' );
              echo $this->mkform->pilih_dpi($attr_opsi_dpi);
        ?>
        </div>
        <?php 
              $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_tujuan',
                                           'input_name' => 'id_pelabuhan_tujuan[]',
                                           'label_text' => 'Pelabuhan Tujuan <em>*</em> :',
                                           'array_opsi' => $opsi_tujuan,
                                           'opsi_selected' => '', 
                                           'input_width' => 'col-lg-6',
                                           'input_class' => '',
                                           'label_class' => 'col-lg-4 control-label');
              echo $this->mkform->pilih_pelabuhan_lama($attr_opsi_pelabuhan);
   
        //content alat tangkap
        $attr_alat_tangkap = array('input_id' => 'id_alat_tangkap',
                                   'input_name' => 'id_alat_tangkap',
                                   'label_text' => 'Alat Tangkap <em>*</em> :',
                                   'input_value' => '',
                                   'input_placeholder' => 'mulai ketik alat tangkap',
                                   'input_type' => 'text',
                                   'input_width' => '',
                                   'label_class' => 'col-lg-4',
                                   'input_class' => '' );
        echo $this->mkform->input($attr_alat_tangkap);
        //echo '<input id="pilih_alat_tangkap" name="pilih_alat_tangkap" type="text" class="col-lg-12" placeholder="">';

        //content Alat Bantu Penangkapan
        $attr_alat_bantu = array('input_id' => 'id_alat_bantu',
                                 'input_name' => 'id_alat_bantu',
                                 'label_text' => 'Alat Bantu Tangkap :',
                                 'input_value' => '',
                                 'input_placeholder' => '',
                                 'input_type' => 'text',
                                 'input_width' => '',
                                 'label_class' => 'col-lg-4',
                                 'input_class' => '' );
        echo $this->mkform->input($attr_alat_bantu);

        //conten Nama Nahkoda
        $attr_nahkoda = array('input_id' => 'nama_nahkoda',
                              'input_name' => 'nama_nahkoda',
                              'label_text' => 'Nama Nahkoda <em>*</em> :',
                              'input_value' => '',
                              'input_placeholder' => 'masukan nama nahkoda',
                              'input_type' => 'text',
                              'input_width' => 'col-lg-6',
                              'label_class' => 'col-lg-4',
                              'input_class' => 'form-control' );
        echo $this->mkform->input($attr_nahkoda);

        //conten Jumlah ABK
        $attr_abk = array('input_id' => 'jumlah_abk',
                          'input_name' => 'jumlah_abk',
                          'label_text' => 'Jumlah ABK <em>*</em> :',
                          'input_value' => '',
                          'input_placeholder' => 'masukan jumlah abk',
                          'input_type' => 'text',
                          'input_width' => 'col-lg-6',
                          'label_class' => 'col-lg-4',
                          'input_class' => 'form-control' );
        echo $this->mkform->input($attr_abk);
  ?>
  <input type="hidden" id="input_filter_gt" name="input_filter_gt">
        </div>
      </div>
    </div>
  </div>
              <div class="panel form-input"> <!-- main panel -->
                  <div class="panel-body"> <!-- panel-body -->
                 
                   <?php echo $this->mkform->inputPerbekalan(); ?>
                  </div>
                    <div class="panel-footer">
                        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
                    </div>
              </div>
          </div>
<div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button id="btn-simpan" type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>
</form>




<script>
    var id_pelabuhan = is_admin_mode ? id_pelabuhan_admin : id_pelabuhan_pengguna_session,
        entry_status = "<?php echo $entry_status; ?>",
        nama_kapal_entry = "<?php echo $nama_kapal; ?>",
        list_opsi_alat_tangkap = <?php echo Modules::run('mst_alat_tangkap/json_alat_tangkap'); ?>,
        //list_opsi_dpi = [{"id":"0","text":"Kosong"}],
        list_opsi_wpp = <?php echo Modules::run('mst_wpp/json_wpp'); ?>,
        list_alat_bantu = <?php echo Modules::run('tables/pipp_json_all', 'mst_alat_bantu'); ?>,
        //list_perbekalan = <?php //echo Modules::run('tables/pipp_json_all', 'mst_jenis_perbekalan'); ?>,
        url_search_dpi = "<?php echo base_url('mst_dpi/search_dpi'); ?>",
        url_detail_kapal = "<?php echo base_url('mst_kapal/detail_kapal'); ?>",
        url_check_berangkat = "<?php echo base_url('jurnal/H5/json_check_berangkat'); ?>",
        val_tujuan_berangkat = function() { return $("#tujuan_berangkat").val(); },
        rowCount = 0, // rowCount untuk counter jumlah baris table
        error_count = 0,
        is_perbekalan_kosong = true,
        notifharaptunggu,
        is_dpi_ok = function() { return $("#id_dpi").select2("val").length > 0 ? true : false; },
        is_pelabuhan_tujuan_ok = function() { return $("#id_pelabuhan_tujuan").select2("val").length > 0 ? true : false; },
        is_alat_tangkap_ok = function() { return $("#id_alat_tangkap").select2("val") !== '' ? true : false; },
        is_alat_bantu_ok = function() { return $("#id_alat_bantu").select2("val").length > 0 ? true : false;};
        //defaultRowCount = list_perbekalan.length, // TODO : valuenya nanti dari konfigurasi global
        //shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        //shortcut_kurang_baris = 'alt+k'; // TODO : Calon jadi library javascript
        //list_opsi_ikan = <?php //echo Modules::run('mst_jenis_ikan/json_ikan'); ?>;

        function notif_entry_success()
        {
          if(entry_status === 'success')
          {
            var pesan = ' Entry Form Keberangkatan Kapal <br>'+nama_kapal_entry+' Berhasil';
            $.pnotify({
                title: 'Entry Berhasil',
                text: pesan,
                type: 'success'
            });
          }
        }

        function update_opsi_dpi(izin_dpi)
        {
          $("#id_dpi option").each(function(){ 
              $(this).prop("disabled", false);
          });
          $("#id_dpi option").each(function(){ 
            var this_val = $(this).val();
            if ($.inArray(this_val, izin_dpi) < 0)
            {
              $(this).prop("disabled", true);
            }
          });
          $("#id_dpi").select2("val","");
          $("#id_dpi").select2("val", izin_dpi);
        }

        function update_opsi_pelabuhan(izin_pelabuhan_pangkalan)
        {
          // var id_pelabuhan_current = '' + id_pelabuhan;
          //     izin_pelabuhan_pangkalan.push(id_pelabuhan_current);

          // $("#id_pelabuhan_tujuan option").each(function(){ 
          //     $(this).prop("disabled", false);
          // });    
          // $("#id_pelabuhan_tujuan option").each(function(){ 
          //   var this_val = $(this).val();
          //   if ($.inArray(this_val, izin_pelabuhan_pangkalan) < 0)
          //   {
          //     $(this).prop("disabled", true);
          //   }
          // });
          $("#id_pelabuhan_tujuan").select2("val","");
          $("#id_pelabuhan_tujuan").select2("val", izin_pelabuhan_pangkalan);
        }
      

        function get_detail_kapal(a_id_kapal, a_filter_gt)
        {
          notifharaptunggu = haraptunggu();
            $.ajax({
              dataType: "json",
              url: url_detail_kapal,
              data: { id_kapal: a_id_kapal, filter_gt: a_filter_gt},
              success: function(data){
                check_berangkat(data, a_filter_gt)
              }
            });
        }
        
        function check_berangkat (data_kapal,a_filter_gt) {
            $.ajax({
              dataType: "json",
              url: url_check_berangkat,
              data: { id_kapal: data_kapal.id_kapal},
              success: function(data) {
                // Kalau data[0] === kosong, berarti tidak ada entry aktivitas kapal keluar sebelumnya.
                if(data[0] !== "kosong")
                {
                    var dt = new Date(data[0].tgl_aktivitas),
                        tgl_keluar = dt.getDate()+' '+nama_bulan[dt.getMonth()]+' '+dt.getFullYear(), 
                        pesan = ' Kapal <br>'+data_kapal.nama_kapal+' Telah Entry Kapal Keluar.'+
                                ' <br> Dari Pelabuhan '+ data[0].nama_pelabuhan_berangkat +
                                ' Pada Tanggal: '+tgl_keluar;
                        console.log(data[0]);        
                    $.pnotify({
                        title: 'Peringatan',
                        text: pesan,
                        type: 'warning',
                        delay: 10000
                    });
                    notifharaptunggu.pnotify_remove();

                }else{
                  set_kapal(data_kapal,a_filter_gt);
                }
              }
            });
        }

        function set_kapal(data, a_filter_gt)
        {
          var array_dpi = [],
              array_pelabuhan_pangkalan = [],
              item_kosong = [],
              pesan_warning = '',
              required_item = ['id_dpi','id_wpp','nama_wpp','id_alat_tangkap','nama_penanggung_jawab', 'id_pelabuhan_pangkalan'];
          $(".form-input input,select").prop("disabled", false);
          error_count = 0;

          if(a_filter_gt === "below")
          {
            $("#input_filter_gt").val("below");            
          }else
          {
            $("#input_filter_gt").val("above");       
          }

          //Cek value data kapal 
          for( item in data )
          {
            if(required_item.indexOf(item) > -1 )
            {
              if(data[item] === null || data[item] === "" || data[item] === '0' || data[item] === 'kosong' || data[item] === undefined)
              {
                console.log(data[item]);
                data[item] = 'Tidak ada data.';
                item_kosong.push(item);
              }
            }

          }
          
          // for ( item in data )
          // {
          //   console.log(item+": "+data[item]+" ("+(typeof data[item])+") isnull( "+(data[item] === null)+" )");
          // }
          //console.log(data);
          if(item_kosong.length > 0)
          {
            data_kosong = item_kosong.join(',');
            pesan_warning= 'Data berikut tidak tersedia untuk kapal '+data.nama_kapal+' : <br>'+ data_kosong;
            $.pnotify({
              title: 'Data Kapal Tidak Lengkap',
              text: pesan_warning,
              type: 'warning'
            });
          }
     

          if(data.id_dpi !== 'Tidak ada data.')
          {
              if(data.id_dpi.indexOf(",") > 0 )
              {
                array_dpi = data.id_dpi.split(", ");
              }else{
                array_dpi.push(data.id_dpi);
              }
              update_opsi_dpi(array_dpi);      
          }

          if(data.id_pelabuhan_pangkalan !== 'Tidak ada data.')
          {
              if(data.id_pelabuhan_pangkalan.indexOf(",") > 0 )
              {
              array_pelabuhan_pangkalan = data.id_pelabuhan_pangkalan.split(", ");
              }else{
                array_pelabuhan_pangkalan.push(data.id_pelabuhan_pangkalan);
              }
            //console.log(array_pelabuhan_pangkalan);

              update_opsi_pelabuhan(array_pelabuhan_pangkalan);  
          }

          if(data.id_alat_tangkap !== 'Tidak ada data.')
          {
            $("#id_alat_tangkap").select2("val",data.id_alat_tangkap);
            if(current_gt() === "above") { 
              $("#id_alat_tangkap").select2("readonly",true); 
            }
            else { 
              $("#id_alat_tangkap").select2("readonly",false);
            }
            if(data.nama_alat_tangkap === 'Pengangkut')
            {
              $("#tujuan_berangkat").val('angkut');
              listener_tujuan_berangkat('angkut');
            }
          }

          
          $("#nama_kapal").text(data.nama_kapal+' / '+data.no_sipi);
          $("#tanda_selar").text(data.tanda_selar);
          $("#nama_pemilik").text(data.nama_penanggung_jawab);
          $("#wpp_info").text(data.nama_wpp);
          $("#alat_tangkap_info").text(data.nama_alat_tangkap);
          $("#pelabuhan_pangkalan_info").text(data.nama_pelabuhan_pangkalan);

          $("input[name=nama_kapal_info]").val(data.nama_kapal);

          notifharaptunggu.pnotify_remove();
        }

        /*
        function set_dpi(data)
        {
          //update list_opsi_dpi sama data hasil ajax
          list_opsi_dpi = data.list_dpi;
          //reinisiasi select2 untuk id_dpi
          $("#id_dpi").select2({  width: "60%",
                                              placeholder: "Mulai ketik nama DPI..",
                                              data: list_opsi_dpi
                                            });
          // Enable id_dpi setelah pilih wpp
          $("#id_dpi").select2("enable", true);
          // Buka otomatis
          $("#id_dpi").select2("open");

        }
        */

        function extended_validation()
        {
              error_count = 0;
                //console.log(val_tujuan_berangkat());
                if(!is_dpi_ok() && val_tujuan_berangkat() === 'tangkap')
                {
                  $('#s2id_id_dpi').validationEngine('showPrompt', 'Pilih 1 DPI','','',true);
                  error_count++;
                }else{
                  $('#s2id_id_dpi').validationEngine('hide');
                }

                if(!is_pelabuhan_tujuan_ok())
                {
                  $("#s2id_id_pelabuhan_tujuan").validationEngine('showPrompt', 'Minimal Pilih 1 Pelabuhan Tujuan','','',true);
                  error_count++;
                }else{
                  $("#s2id_id_pelabuhan_tujuan").validationEngine('hide');
                }

                if(!is_alat_tangkap_ok())
                {
                  $("#s2id_id_alat_tangkap").validationEngine('showPrompt', 'Tidak Boleh Kosong','','',true);
                  error_count++;
                }else{
                  $("#s2id_id_alat_tangkap").validationEngine('hide');
                }

                if(!is_alat_bantu_ok())
                {
                  $("#s2id_id_alat_bantu").validationEngine('showPrompt', 'Minimal pilih 1 Alat Bantu','','',true);
                  error_count++;
                }else{
                  $("#s2id_id_alat_bantu").validationEngine('hide');
                }

                //$.pnotify_remove_all();
                $(".input-perbekalan").each(function(d) {
                  if ( $(this).val() !== '' ){
                      is_perbekalan_kosong = false;
                      return false;
                  }
                });
                
                if(is_perbekalan_kosong)
                {
                    $("#kolom-perbekalan").validationEngine('showPrompt', 'Perbekalan Tidak Boleh Kosong','','`',true);
                    error_count++;
                }else{
                    $("#kolom-perbekalan").validationEngine('hide');
                }


                //console.log(is_perbekalan_kosong,is_alat_bantu_ok,is_alat_tangkap_ok,is_pelabuhan_tujuan_ok, is_dpi_ok, error_count);
                return (error_count === 0);
        }

        function set_validation()
        {
          $("#tgl_aktivitas").addClass("validate[required]");
          $("#nama_nahkoda").addClass("validate[required]");
          $("#tgl_aktivitas").addClass("validate[required]");
          $("#jumlah_abk").addClass("validate[required,custom[onlyNumberSp]]");
          $(".input-perbekalan").addClass("validate[custom[onlyNumberSp]]");

        }

        function listener_tujuan_berangkat(opsi)
        {
                  if(opsi === 'angkut' || opsi === 'docking')
                  {
                    $("#pilih_wpp_dpi").hide();
                  }else{
                    $("#pilih_wpp_dpi").show();
                  }
        }

        $(document).ready(function(){
            notif_entry_success();

            //initialisasi json alat_bantu supaya bisa dipake select2
            list_alat_bantu.forEach(function(d) { d.id = d.id_alat_bantu; d.text = d.nama_alat_bantu; });
            $("#id_alat_bantu").select2({  width: "60%",
                                              placeholder: "Mulai ketik nama alat bantu..",
                                              multiple: true,
                                              maximumSelectionSize: 3,
                                              formatSelectionTooBig : function() { return "Maksimal 3 alat bantu keluar.";},
                                              data: list_alat_bantu
                                            });



            $("#id_alat_tangkap").select2({  width: "60%",
                                              placeholder: "Mulai ketik nama alat tangkap..",
                                              data: list_opsi_alat_tangkap
                                            });
            

            $("#pilih_pelabuhan").hide();
            $("#tujuan_berangkat").on("change", function(){
                var opsi = $(this).val();
                listener_tujuan_berangkat(opsi);
            });

            $(".form-input input,select").not("#id_pelabuhan_current").prop("disabled", true);

            //$("#btn-simpan").on("mouseover", extended_validation);

            set_validation();
            $("#form_entry").validationEngine();

            // $("#form_entry").bind("jqv.form.validating", function(event, errorFound) {
            //         $("#form_entry").validationEngine('hideAll');
            // });

            $("#form_entry").validationEngine('attach',{
              onValidationComplete: function(form, status) { 

                  if( extended_validation() === true){
                    if(status === true)
                    {
                      sound_fx.woosh.play();
                      return status;                         
                    }else{
                      sound_fx.blop.play();
                      return status;
                    }
                  }else{
                    sound_fx.blop.play();
                    return false;
                  }
              }
            });

        });

</script>