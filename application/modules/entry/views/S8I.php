<?php
if( !$list_S8I ){
    $list_S8I = array(
                   'id_layanan_es' => '',
                   'id_jns_layanan_es' => '3',
                   'tgl_catat' => '',
                   'jml' => '',
                   'tahun' => '',
                   'kapasitas' => '',
                   'ket_teknis' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8I/input';
}else{
    $submit_form = 'entry/S8I/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_layanan_es', 
                            'input_name' => 'id_layanan_es' , 
                            'label_text' => '',
                            'input_value' => $list_S8I['id_layanan_es'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    $opsi = array(
                   '3' => 'Mesin Penghancur Es'
                  );
    $id_jns_layanan_es = array(
        'input_id' => 'id_jns_layanan_es', 
        'input_name' => 'id_jns_layanan_es', 
        'label_text' => 'Jenis Layanan Air', 
        'array_opsi' => $opsi, 
        'opsi_selected' => $list_S8I['id_jns_layanan_es'], 
        'input_width' => 'col-lg-6', 
        'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($id_jns_layanan_es);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S8I['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $jml = array(
                                'input_id' => 'jml', 
                                'input_name' => 'jml' , 
                                'label_text' => 'Jumlah <m>*<m> :',
                                'input_value' => $list_S8I['jml'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml);

    $kapasitas = array(
                                'input_id' => 'kapasitas', 
                                'input_name' => 'kapasitas' , 
                                'label_text' => 'Kapasitas <m>*<m> :',
                                'input_value' => $list_S8I['kapasitas'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($kapasitas);

    $ket_teknis = array(
                                'input_id' => 'ket_teknis', 
                                'input_name' => 'ket_teknis' , 
                                'label_text' => 'Keterangan Teknis :',
                                'input_value' => $list_S8I['ket_teknis'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_teknis);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8I['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#jml").addClass("validate[required,custom[onlyNumberSp]]");
          $("#kapasitas").addClass("validate[required,custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

