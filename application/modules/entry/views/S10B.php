<?php
//dulunya hidden input
echo form_open('entry/S10B/input', 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $nama_sungai = array(
                                'input_id' => 'nama_sungai', 
                                'input_name' => 'nama_sungai' , 
                                'label_text' => 'Nama Sungai <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_sungai);

    $nama_lokal = array(
                                'input_id' => 'nama_lokal', 
                                'input_name' => 'nama_lokal' , 
                                'label_text' => 'Nama Lokal :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_lokal);

    $lebar_sungai = array(
                                'input_id' => 'lebar_sungai', 
                                'input_name' => 'lebar_sungai' , 
                                'label_text' => 'Lebar Sungai :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lebar_sungai);

    $dalam_sungai = array(
                                'input_id' => 'dalam_sungai', 
                                'input_name' => 'dalam_sungai' , 
                                'label_text' => 'Dalam Sungai :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($dalam_sungai);

    $desk_sungai = array(
                                'input_id' => 'desk_sungai', 
                                'input_name' => 'desk_sungai' , 
                                'label_text' => 'Deskripsi Sungai :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($desk_sungai);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_sungai").addClass("validate[required,custom[onlyLetterSp]]");
          $("#nama_lokal").addClass("validate[custom[onlyLetterSp]]");
          $("#lebar_sungai").addClass("validate[custom[onlyNumberSp]]");
          $("#dalam_sungai").addClass("validate[custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
