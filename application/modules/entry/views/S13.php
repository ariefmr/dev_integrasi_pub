<?php
if( !$list_S13 ){
    $list_S13 = array(
                'id_masy_perikanan' => '',
                'nama' => '',
                'alamat' => '',
                'profesi' => ''
               );
    $submit_form = 'entry/S13/input';
}else{
    $submit_form = 'entry/S13/update';
}

//dulunya hidden input
echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php
  
    $hide = array(
                                'input_id' => 'id_masy_perikanan', 
                                'input_name' => 'id_masy_perikanan' , 
                                'label_text' => '',
                                'input_value' => $list_S13["id_masy_perikanan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

    $nama = array(
                                'input_id' => 'nama', 
                                'input_name' => 'nama' , 
                                'label_text' => 'Nama <em>*</em> :',
                                'input_value' => $list_S13["nama"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama);

    $alamat = array(
                                'input_id' => 'alamat', 
                                'input_name' => 'alamat' , 
                                'label_text' => 'Alamat <em>*</em> :',
                                'input_value' => $list_S13["alamat"],
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($alamat);

    $profesi = array(
                                'input_id' => 'profesi', 
                                'input_name' => 'profesi' , 
                                'label_text' => 'Profesi <em>*</em> :',
                                'input_value' => $list_S13["profesi"],
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($profesi);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama").addClass("validate[required,custom[onlyLetterSp]]");
          $("#alamat").addClass("validate[required,custom[onlyLetterSp]]");
          $("#profesi").addClass("validate[required]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
