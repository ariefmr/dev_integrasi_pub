<?php
if( !$list_S7C ){
    $list_S7C = array(
                   'id_groin' => '',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'jns_konstruksi' => '',
                   'panjang' => '',
                   'lebar' => '',
                   'elevasi' => '',
                   'jml_groin' => '',
                   'jarak_groin' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'gambar' => '',
                   'ket_sumber_dana' => ''
                   );
    $submit_form = 'entry/S7C/input';
}else{
    $submit_form = 'entry/S7C/update';
}

//dulunya hidden input
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_groin', 
                            'input_name' => 'id_groin' , 
                            'label_text' => '',
                            'input_value' => $list_S7C["id_groin"], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S7C["tahun"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S7C["ket_rehab"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);

    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <m>*<m> :',
                                'input_value' => $list_S7C["jns_konstruksi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);

    $panjang = array(
                                'input_id' => 'panjang', 
                                'input_name' => 'panjang' , 
                                'label_text' => 'Panjang <m>*<m> :',
                                'input_value' => $list_S7C["panjang"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($panjang);

    $lebar = array(
                                'input_id' => 'lebar', 
                                'input_name' => 'lebar' , 
                                'label_text' => 'Lebar <m>*<m> :',
                                'input_value' => $list_S7C["lebar"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lebar);

    $elevasi = array(
                                'input_id' => 'elevasi', 
                                'input_name' => 'elevasi' , 
                                'label_text' => 'Elevasi <m>*<m> :',
                                'input_value' => $list_S7C["elevasi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($elevasi);

    $jml_groin = array(
                                'input_id' => 'jml_groin', 
                                'input_name' => 'jml_groin' , 
                                'label_text' => 'Jumlah Groin <m>*<m> :',
                                'input_value' => $list_S7C["jml_groin"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml_groin);

    $jarak_groin = array(
                                'input_id' => 'jarak_groin', 
                                'input_name' => 'jarak_groin' , 
                                'label_text' => 'Jarak Groin <m>*<m> :',
                                'input_value' => $list_S7C["jarak_groin"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jarak_groin);

    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S7C["ket_konstruksi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);

    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <m>*<m> :',
                                'input_value' => $list_S7C["sumber_dana"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);

    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai <m>*<m> :',
                                'input_value' => $list_S7C["nilai"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);

    $ket_sumber_dana = array(
                                'input_id' => 'ket_sumber_dana', 
                                'input_name' => 'ket_sumber_dana' , 
                                'label_text' => 'Keterangan Sumber Dana :',
                                'input_value' => $list_S7C["ket_sumber_dana"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_sumber_dana);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S7C['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");  
          $("#jns_konstruksi").addClass("validate[required]");
          $("#panjang").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lebar").addClass("validate[required,custom[onlyNumberSp]]");
          $("#elevasi").addClass("validate[required,custom[onlyNumberSp]]");
          $("#jml_groin").addClass("validate[required,custom[onlyNumberSp]]");
          $("#jarak_groin").addClass("validate[required,custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

