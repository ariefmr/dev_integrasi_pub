<?php
if( !$list_S8J ){
    $list_S8J = array(
                   'id_layanan_listrik' => '',
                   'nama_layanan_listrik' => '',
                   'tgl_catat' => '',
                   'tahun' => '',
                   'daya_total' => '',
                   'ket_teknis' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8J/input';
}else{
    $submit_form = 'entry/S8J/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_layanan_listrik', 
                            'input_name' => 'id_layanan_listrik' , 
                            'label_text' => '',
                            'input_value' => $list_S8J['id_layanan_listrik'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>
  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    $nama_layanan_listrik = array(
                                'input_id' => 'nama_layanan_listrik', 
                                'input_name' => 'nama_layanan_listrik' , 
                                'label_text' => 'Nama Layanan Listrik :',
                                'input_value' => $list_S8J['nama_layanan_listrik'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_layanan_listrik);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S8J['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $daya_total = array(
                                'input_id' => 'daya_total', 
                                'input_name' => 'daya_total' , 
                                'label_text' => 'Daya Total <m>*<m> :',
                                'input_value' => $list_S8J['daya_total'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($daya_total);

    $ket_teknis = array(
                                'input_id' => 'ket_teknis', 
                                'input_name' => 'ket_teknis' , 
                                'label_text' => 'Keterangan Teknis :',
                                'input_value' => $list_S8J['ket_teknis'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_teknis);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8J['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#daya_total").addClass("validate[required,custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

