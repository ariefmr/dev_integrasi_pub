<?php
if( !$list_S9D ){
    $list_S9D = array(
                   'id_fas_umum' => '',
                   'id_jns_fas_umum' => '7',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'jns_konstruksi' => '',
                   'luas' => '',
                   'jml' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'ket' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S9D/input';
}else{
    $submit_form = 'entry/S9D/update';
}

//echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

$hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
$hide = array(
                            'input_id' => 'id_fas_umum', 
                            'input_name' => 'id_fas_umum' , 
                            'label_text' => '',
                            'input_value' => $list_S9D['id_fas_umum'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">
  <?php

    $opsi = array(
                   '7' => 'Kios Iptek'
                  );
    $id_jns_fas_umum = array(
                                'input_id' => 'id_jns_fas_umum', 
                                'input_name' => 'id_jns_fas_umum',
                                'label_text' => 'Jenis Fasilitas Umum <em>*</em> :', 
                                'array_opsi' => $opsi, 
                                'opsi_selected' => $list_S9D["id_jns_fas_umum"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_fas_umum',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($id_jns_fas_umum);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun <em>*</em> :',
                                'input_value' => $list_S9D['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);
    
    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S9D['ket_rehab'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);
    
    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <em>*</em> :',
                                'input_value' => $list_S9D['jns_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);
    
    $luas = array(
                                'input_id' => 'luas', 
                                'input_name' => 'luas' , 
                                'label_text' => 'Luas <em>*</em> :',
                                'input_value' => $list_S9D['luas'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($luas);
    
    $jml = array(
                                'input_id' => 'jml', 
                                'input_name' => 'jml' , 
                                'label_text' => 'Jumlah :',
                                'input_value' => $list_S9D['jml'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml);
    
    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S9D['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);
    
    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <em>*</em> :',
                                'input_value' => $list_S9D['sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);
    
    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai :',
                                'input_value' => $list_S9D['nilai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);
    
    $ket = array(
                                'input_id' => 'ket', 
                                'input_name' => 'ket' , 
                                'label_text' => 'Keterangan :',
                                'input_value' => $list_S9D['ket'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S9D['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
   
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#id_jns_fas_umum").addClass("validate[required]");
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#jns_konstruksi").addClass("validate[required]");
          $("#luas").addClass("validate[required,custom[onlyNumberSp]]");
          $("#jml").addClass("validate[custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
