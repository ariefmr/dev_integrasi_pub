<?php
if( !$list_S8B ){
    $list_S8B = array(
                   'id_fas_nav_kom' => '',
                   'id_jns_fas_nav_kom' => '',
                   'tgl_catat' => '',
                   'nama_fas_pemasaran' => '',
                   'tahun' => '',
                   'jml' => '',
                   'ket_teknis' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8B/input';
}else{
    $submit_form = 'entry/S8B/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

$hide = array(
                            'input_id' => 'id_fas_nav_kom', 
                            'input_name' => 'id_fas_nav_kom' , 
                            'label_text' => '',
                            'input_value' => $list_S8B['id_fas_nav_kom'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>
  <div class="panel">
        <div class="panel-body">
  <?php

    /*$opsi = array(
                   '1' => 'A',
                   '2' => 'B',
                   '3' => 'C',
                   '4' => 'D',
                   '5' => 'E',
                   '6' => 'F',
                  );
    $id_jns_fas_nav_kom = array('input_id' => 'id_jns_fas_nav_kom', 'input_name' => 'id_jns_fas_nav_kom', 'label_text' => 'Jenis Fas. Navigasi Dan Komunikasi', 
        'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($id_jns_fas_nav_kom);*/
    //dropdown 
    $id_jns_fas_nav_kom = array(
                                'input_id' => 'id_jns_fas_nav_kom', 
                                'input_name' => 'id_jns_fas_nav_kom',
                                'label_text' => 'Jenis Fasilitas <em>*</em> :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S8B["id_jns_fas_nav_kom"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_fas_nav_kom', 
                                'field_value' => 'id_jns_fas_nav_kom',
                                'field_text' => 'nama_jns_fas_nav_kom'
                            );
    echo $this->mkform->dropdown($id_jns_fas_nav_kom);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S8B['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $jml = array(
                                'input_id' => 'jml', 
                                'input_name' => 'jml' , 
                                'label_text' => 'Jumlah <m>*<m> :',
                                'input_value' => $list_S8B['jml'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml);

    $ket_teknis = array(
                                'input_id' => 'ket_teknis', 
                                'input_name' => 'ket_teknis' , 
                                'label_text' => 'Keterangan Teknis :',
                                'input_value' => $list_S8B['ket_teknis'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_teknis);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8B['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#jml").addClass("validate[required,custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
