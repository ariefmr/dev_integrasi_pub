<?php
if( !$list_S8N ){
    $list_S8N = array(
                   'id_fas_pelihara' => '',
                   'id_jns_fas_pelihara' => '',
                   'tgl_catat' => '',
                   'tahun' => '',
                   'jns_alat_bantu' => '',
                   'jns_konstruksi' => '',
                   'luas_total' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8N/input';
}else{
    $submit_form = 'entry/S8N/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_fas_pelihara', 
                            'input_name' => 'id_fas_pelihara' , 
                            'label_text' => '',
                            'input_value' => $list_S8N['id_fas_pelihara'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    /*$opsi = array(
                   '1' => 'A',
                   '2' => 'B',
                   '3' => 'C',
                   '4' => 'C',
                   '5' => 'C',
                   '6' => 'C',
                  );
    $id_jns_fas_pelihara = array('input_id' => 'id_jns_fas_pelihara', 'input_name' => 'id_jns_fas_pelihara', 'label_text' => 'Jenis Fasilitas', 
        'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($id_jns_fas_pelihara);*/

    $id_jns_fas_pelihara = array(
                                'input_id' => 'id_jns_fas_pelihara', 
                                'input_name' => 'id_jns_fas_pelihara',
                                'label_text' => 'Jenis Fasilitas <em>*</em> :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S8N["id_jns_fas_pelihara"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_fas_pelihara', 
                                'field_value' => 'id_jns_fas_pelihara',
                                'field_text' => 'nama_jns_fas_pelihara'
                            );
    echo $this->mkform->dropdown($id_jns_fas_pelihara);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <em>*</em> :',
                                'input_value' => $list_S8N['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <em>*</em> :',
                                'input_value' => $list_S8N['jns_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);

    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S8N['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8N['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#jns_konstruksi").addClass("validate[required]");
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#jns_konstruksi").addClass("validate[required]");
      
        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
