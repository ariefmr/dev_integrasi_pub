<?php
if( !$data_terakhir ){
    $data_terakhir = array(
                   'id_luas_pelabuhan' => '',
                   'luas_perairan' => '',
                   'luas_daratan' => '',
                   'luas_industri_siap' => '',
                   'luas_industri_terpakai' => '',
                   'luas_industri_belum' => '',
                   'luas_fas_siap' => '',
                   'luas_fas_terpakai' => '',
                   'luas_fas_belum' => ''
                   );
    $submit_form = 'entry/S6B/input';
}else{
    $submit_form = 'entry/S6B/input';
}

//dulunya hidden input
echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
/*$hide = array(
                            'input_id' => 'id_luas_pelabuhan', 
                            'input_name' => 'id_luas_pelabuhan' , 
                            'label_text' => '',
                            'input_value' => $data_terakhir['id_luas_pelabuhan'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);*/
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $attr_luas_perairan = array(
                                'input_id' => 'luas_perairan', 
                                'input_name' => 'luas_perairan' , 
                                'label_text' => 'Luas Daerah Perairan :', 
                                'input_value' => $data_terakhir['luas_perairan'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_perairan);

    $attr_luas_daratan = array(
                                'input_id' => 'luas_daratan', 
                                'input_name' => 'luas_daratan' , 
                                'label_text' => 'Luas Lahan Daratan (ha):', 
                                'input_value' => $data_terakhir['luas_daratan'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_daratan);

    echo '<hr> <div class="form-group">
                          <label class="col-lg-12">Luas Lahan Untuk Industri</label>
                        </div>' ;

    $attr_luas_industri_siap = array(
                                'input_id' => 'luas_industri_siap', 
                                'input_name' => 'luas_industri_siap' , 
                                'label_text' => 'Kondisi Siap Bangun :', 
                                'input_value' => $data_terakhir['luas_industri_siap'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_industri_siap);
    
    $attr_luas_industri_terpakai = array(
                                'input_id' => 'luas_industri_terpakai', 
                                'input_name' => 'luas_industri_terpakai' , 
                                'label_text' => 'Sudah Terpakai :', 
                                'input_value' => $data_terakhir['luas_industri_terpakai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_industri_terpakai);
    
    $attr_luas_industri_belum = array(
                                'input_id' => 'luas_industri_belum', 
                                'input_name' => 'luas_industri_belum' , 
                                'label_text' => 'Belum Siap Bangun :', 
                                'input_value' => $data_terakhir['luas_industri_belum'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_industri_belum);

    echo '<hr> <div class="form-group">
                          <label class="col-lg-12">Luas Lahan Untuk Fasilitas</label>
                        </div>' ;
    
    $attr_luas_fas_siap = array(
                                'input_id' => 'luas_fas_siap', 
                                'input_name' => 'luas_fas_siap' , 
                                'label_text' => 'Kondisi Siap Bangun :', 
                                'input_value' => $data_terakhir['luas_fas_siap'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_fas_siap);
    
    $attr_luas_fas_terpakai = array(
                                'input_id' => 'luas_fas_terpakai', 
                                'input_name' => 'luas_fas_terpakai' , 
                                'label_text' => 'Sudah Terpakai :', 
                                'input_value' => $data_terakhir['luas_fas_terpakai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_fas_terpakai);
    
    $attr_luas_fas_belum = array(
                                'input_id' => 'luas_fas_belum', 
                                'input_name' => 'luas_fas_belum' , 
                                'label_text' => 'Belum Siap Bangun :', 
                                'input_value' => $data_terakhir['luas_fas_belum'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_luas_fas_belum);
    



  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#luas_perairan").addClass("validate[required,custom[number]]");
          $("#luas_daratan").addClass("validate[required,custom[number]]");
          $("#luas_industri_siap").addClass("validate[required,custom[number]]");
          $("#luas_industri_terpakai").addClass("validate[required,custom[number]]");
          $("#luas_fas_siap").addClass("validate[required,custom[number]]");
          $("#luas_fas_terpakai").addClass("validate[required,custom[number]]");
          $("#luas_fas_belum").addClass("validate[required,custom[number]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
