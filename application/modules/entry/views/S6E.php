<?php
if( !$list_S6E ){
    $list_S6E = array(
                   'id_amenities' => '',
                   'id_jns_amenities' => '',
                   'nama_jns_amenities' => '',
                   'nama_amenities' => '',
                   'jarak' => '',
                   'ket' => '',
                   'tgl_catat' => ''
                   );
    $submit_form = 'entry/S6E/input';
}else{
    $submit_form = 'entry/S6E/update';
}

echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_amenities', 
                            'input_name' => 'id_amenities' , 
                            'label_text' => '',
                            'input_value' => $list_S6E['id_amenities'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    /*$opsi = array(
                   '1' => 'Hotel',
                   '2' => 'Rumah Makan',
                   '3' => 'Tempat Wisata'
                  );
    $attr_nama_jns_amenities = array('input_id' => 'id_jns_amenities', 'input_name' => 'id_jns_amenities', 'label_text' => 'Jenis Amenities', 'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_amenities);*/

    
    $attr_nama_jns_amenities = array(
                                'input_id' => 'id_jns_amenities', 
                                'input_name' => 'id_jns_amenities',
                                'label_text' => 'Propinsi :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6E["id_jns_amenities"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_amenities', 
                                'field_value' => 'id_jns_amenities',
                                'field_text' => 'nama_jns_amenities'
                            );
    echo $this->mkform->dropdown($attr_nama_jns_amenities);


    $attr_nama_amenities = array(
                                'input_id' => 'nama_amenities', 
                                'input_name' => 'nama_amenities' , 
                                'label_text' => 'Nama Amenities <m>*<m> :', 
                                'input_value' => $list_S6E['nama_amenities'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_amenities);

    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak dari Pelabuhan (Km) <m>*<m> :', 
                                'input_value' => $list_S6E['jarak'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_ket = array(
                                'input_id' => 'ket', 
                                'input_name' => 'ket' , 
                                'label_text' => 'Keterangan Amenities :', 
                                'input_value' => $list_S6E['ket'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_ket);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => $list_S6E['tgl_catat'], 'input_placeholder' => '',
                            'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_amenities").addClass("validate[required,custom[onlyLetterSp]]");
          $("#jarak").addClass("validate[required,custom[onlyNumberSp]]");


        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
