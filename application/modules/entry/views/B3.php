<?php
echo form_open('entry/B3/input', 'id="form_entry" class="form-horizontal" role="form"');
?>
                                
  <div class="panel">
    <div class="panel-body">
  <?php
   $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
    $tmpl = array ( 'table_open'  => '<table id="table_b3" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (Kg)','Harga','Kategori','Tujuan','Transportasi');

    $table_b3 = $this->table->generate();

        $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);

    ?>
    
    <div class="form-group">
        <?php  $attr_datepick = array('input_id' => 'tgl_aktivitas',
                               'input_name' => 'tgl_aktivitas' , 
                               'label_text' => 'Tanggal Catat <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text',
                                 'input_width' => 'col-lg-6', 
                                 'label_class' => 'col-lg-3 control-label',
                                  'input_class' => 'form-control validate[required] datepicker' );
        echo $this->mkform->datepick($attr_datepick); ?>
      <div class="col-lg-4">
        <button id="add_row" type="button" class="btn btn-default">Tambah Baris</button>
        <button id="del_row" type="button" class="btn btn-default">Kurangi Baris</button>
      </div>
    </div> 
    
    <?php

    echo $table_b3;
  ?>
          </div>
    <div class="panel-footer">
              <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>


<!-- Bagian untuk Content pada table, tambah dan kurang bari -->
<div id="template_inputs" class="hide">
<?php
    /* Elemen ini dipakai javascript untuk di clone atau diduplikasi
     */

    //nama heading('No.', 'Jenis Ikan', 'Kondisi','Berat','Harga','Tujuan','Transportasi');

    //dropdown untuk kondisi ikan
    //array opsi di dapat dari config->custom_constants
    $opsi_jenis_kondisi_ikan = Modules::run('tables/pipp_tbl_opsi', 
                                  'mst_jenis_kondisi_ikan',
                                  'id_jenis_kondisi_ikan',
                                  'nama_jenis_kondisi_ikan');

    $attr_opsi_kondisi_ikan = array('input_id' => 'kondisi_1', 'input_name' => 'kondisi_1', 'label_text' => '',
                            'array_opsi' => $opsi_jenis_kondisi_ikan,'field_id'=> 'id_jenis_kondisi_ikan', 'field_text' => 'nama_jenis_kondisi_ikan',
                             'opsi_selected' => 'segar', 'input_width' => 'col-lg-9', 'input_class' => 'form-control', 'label_class' => 'none');
    echo $this->mkform->dropdown($attr_opsi_kondisi_ikan);
    
    //content Input Berat    
    $attr_berat = array('input_id' => 'jml_1', 'input_name' => 'jml_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_berat);
    
    //content input untuk Harga
    $attr_harga = array('input_id' => 'harga_1', 'input_name' => 'harga_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_harga);

    $opsi_kategori = Modules::run('tables/pipp_tbl_opsi', 
                                  'mst_kateg_tujuan',
                                  'id_kateg_tujuan',
                                  'nama_kateg_tujuan');

    $kategori_1 = array('input_id' => 'kategori_1', 'input_name' => 'kategori_1', 'label_text' => '', 
        'array_opsi' => $opsi_kategori , 'field_id' => 'id_kateg_tujuan', 'field_text' => 'nama_kateg_tujuan',
        'opsi_selected' => '', 'input_width' => 'col-lg-6',
        'input_class' => 'form-control select-kategori', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($kategori_1);

    //content Input Tujuan
    $attr_asal_ikan = array('input_id' => 'tujuan_1', 'input_name' => 'tujuan_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => '' );
    echo $this->mkform->input($attr_asal_ikan);

    //dropdown untuk Transportasi
    $attr_opsi_transportasi = array('input_id' => 'transportasi_1', 'input_name' => 'transportasi_1', 'label_text' => '', 'array_opsi' => $opsi_jenis_transportasi, 'opsi_selected' => 'mobil', 
                            'input_width' => 'col-lg-9', 'input_class' => 'form-control', 'label_class' => 'none');
    echo $this->mkform->dropdown($attr_opsi_transportasi);
 ?>
 <input id="pilih_ikan" name="pilih_ikan" type="text" class=" " placeholder="">
</div>

<script>
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k', // TODO : Calon jadi library javascript
        list_opsi_ikan = <?php echo Modules::run('mst_jenis_ikan/json_ikan'); ?>,
        list_opsi_tujuan = [],
        list_opsi_export = <?php echo Modules::run('tables/dss_tbl_json',
                                                    'mst_negara',
                                                    'nama_negara',
                                                    'nama_negara');?>,
        list_opsi_lokal = <?php echo Modules::run('tables/dss_tbl_json',
                                                  'mst_kabupaten_kota',
                                                  'nama_kabupaten_kota',
                                                  'nama_kabupaten_kota',
                                                  'json');?>,
        list_opsi_regional = <?php echo Modules::run('tables/dss_tbl_json',
                                                     'mst_propinsi',
                                                     'nama_propinsi',
                                                     'nama_propinsi',
                                                     'json');?>,
        list_opsi_tujuan = ['',list_opsi_lokal, list_opsi_regional, list_opsi_export];

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
        $('#table_b3').dataTable().fnAddData( [
                tempRowNum+".",
                cloner("#template_inputs #pilih_ikan", "ikan_"+tempRowNum),
                cloner("#template_inputs #kondisi_1", "kondisi_"+tempRowNum),
                cloner("#template_inputs #jml_1", "jml_"+tempRowNum),
                cloner("#template_inputs #harga_1", "harga_"+tempRowNum),
                cloner("#template_inputs #kategori_1", "kategori_"+tempRowNum),
                cloner("#template_inputs #tujuan_1", "tujuan_"+tempRowNum), 
                cloner("#template_inputs #transportasi_1", "transportasi_"+tempRowNum)
                ] );
            $("#ikan_"+tempRowNum).select2({  width: "100%",
                                              placeholder: "Mulai ketik nama ikan..",
                                              data: list_opsi_ikan
                                            });
            $("#tujuan_"+tempRowNum).select2({  width: "100%",
                                              data: list_opsi_lokal
                                            });

            rowCount++;
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_b3').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
      }

    $(document).ready( function () {
      

      /* Desc : Inisialisasi plugin dataTable ke element #table_b3
       * konfigurasinya untuk matiin fungsi search, info table, pagination, dan sorting
       */
      $('#table_b3').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "25%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},                        
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "5%", "sClass": "text-center"}
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      } );

      $('#table_b3').on('change','.select-kategori', function(){
        var selected_kategori = $(this).val(),
            attr_id = $(this).attr('id'),
            elm_id = attr_id.split('_')[1],
            target_id = $('#tujuan_'+elm_id);
            
        target_id.select2({ width: '100%',data: list_opsi_tujuan[selected_kategori] });
      });
      
      // Panggil fungsi initTableRows;
      initTableRows();

      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $("#add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $("#del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });

    } );  
</script>