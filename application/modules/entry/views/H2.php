<?php
$hidden_input = array('id_kapal' => 0,'nama_kapal_info' => '');
echo form_open('entry/H2/input', 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
?>

            <?php echo Modules::run('mst_kapal/wgt_pilih_kapal'); ?>


    <div class="panel panel-text-referensi">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-8">
            <?php  
                   $array_info_kapal = array( 
                                              'Nama Kapal / SIPI : ' => array( 'id' => 'nama_kapal',
                                                                      'value' =>'...'),
                                              'Tanda Selar : ' => array( 'id' => 'tanda_selar',
                                                                      'value' => '...'),
                                              'Pemilik :' => array( 'id' => 'nama_pemilik',
                                                                  'value' => '...'),
                                              'Nahkoda :' => array( 'id' => 'nahkoda',
                                                                  'value' => '...'),
                                              'Tanggal Kedatangan :' => array( 'id' => 'tgl_masuk_info',
                                                                  'value' => '...'),
                                              'Alat Tangkap : ' => array( 'id' => 'alat_tangkap',
                                                                      'value' => '...'),
                                              'DPI : ' => array( 'id' => 'dpi',
                                                                      'value' => '...'),
                                           ); 
                  echo $this->mkform->ref_text($array_info_kapal);

             ?> 

            </div>
          </div>
        </div>
  </div>

  <div class="panel">
        <div class="panel-body">
        <input type="hidden" id="ref_aktivitas_kapal" name="ref_aktivitas_kapal" value="00">
  <?php
    
    $tmpl = array ( 'table_open'  => '<table id="table_h2" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (Kg)','Harga Produsen (Rp/Kg)','Harga Pedagang (Rp/Kg)','Koefisien Koreksi (%)');

    $table_h2 = $this->table->generate();

    $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal :', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
    echo $this->mkform->datepick($attr_datepick);

    $attr_datepick_tgl_keberangkatan = array('input_id' => 'tgl_aktivitas',
                                         'input_name' => 'tgl_aktivitas' ,
                                         'label_text' => 'Tanggal Bongkar Ikan <em>*</em> :',
                                         'input_value' => '', 'input_placeholder' => '',
                                         'input_type' => 'text',
                                         'input_width' => 'col-lg-6',
                                         'label_class' => 'col-lg-4',
                                         'input_class' => 'form-control validate[required] datepicker' );
    echo $this->mkform->datepick($attr_datepick_tgl_keberangkatan);

    $opsi_sumber_data = array( 
                        'STBLKK' => 'STBLKK',
                        'Tempat Pelelangan Ikan' => 'Tempat Pelelangan Ikan',
                        'Nahkoda' => 'Nahkoda',
                        'Lainnya' => 'Lainnya'
                       );
    $attr_opsi_sumber = array('input_id' => 'sumber_data', 'input_name' => 'sumber_data', 'label_text' => 'Sumber Data :', 'array_opsi' => $opsi_sumber_data, 'opsi_selected' => 'Tempat Pelelangan Ikan', 
                            'input_width' => 'col-lg-4', 'input_class' => 'form-control', 'label_class' => 'col-lg-2 control-label');
    //echo $this->mkform->dropdown($attr_opsi_sumber);
    ?>

  <!-- Hanya tampil jika tidak chain dari H1-->
  <?php
    $attr_datepick_tgl_masuk = array('input_id' => 'tgl_masuk', 'input_name' => 'tgl_masuk' , 'label_text' => 'Tanggal Kedatangan :', 'input_value' => '', 'input_placeholder' => '',
                 'input_type' => 'text', 'input_width' => 'col-lg-6 manual_input', 'label_class' => 'col-lg-4 manual_input', 'input_class' => 'form-control' );
    echo $this->mkform->datepick($attr_datepick_tgl_masuk);

    //conten Nama Nahkoda
    $attr_nahkoda = array('input_id' => 'nama_nahkoda', 'input_name' => 'nama_nahkoda' , 'label_text' => 'Nama Nahkoda :', 'input_value' => '', 'input_placeholder' => 'masukan nama nahkoda',
                'input_type' => 'text', 'input_width' => 'col-lg-6 manual_input', 'label_class' => 'col-lg-4 manual_input', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_nahkoda);

    $attr_opsi_dpi = array('input_id' => 'id_dpi', 'input_name' => 'id_dpi' , 'label_text' => 'DPI :', 'input_value' => '', 'input_placeholder' => 'mulai ketik DPI',
                    'input_type' => 'text', 'input_width' => 'col-lg-8 manual_input', 'label_class' => 'col-lg-4 manual_input', 'input_class' => '' );
    echo $this->mkform->pilih_dpi($attr_opsi_dpi);

    $attr_alat_tangkap = array('input_id' => 'id_alat_tangkap', 'input_name' => 'id_alat_tangkap' , 'label_text' => 'Alat Tangkap :', 'input_value' => '', 'input_placeholder' => 'mulai ketik alat tangkap',
                    'input_type' => 'text', 'input_width' => 'manual_input', 'label_class' => 'col-lg-4 manual_input', 'input_class' => '' );
    echo $this->mkform->input($attr_alat_tangkap);
  ?>
  <hr class="manual_input">
  <input type="hidden" id="input_filter_gt" name="input_filter_gt">
  <div class="form-group">
    <div class="col-lg-4">
      <button id="add_row" type="button" class="btn btn-default">Tambah Baris</button>
      <button id="del_row" type="button" class="btn btn-default">Kurangi Baris</button>
    </div>
  
  </div> 
  
  
  <?php    
    echo $table_h2;
  ?>
  
          </div>
    <div class="panel-footer">
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>


<!-- Bagian untuk Content pada table, tambah dan kurang bari -->
<div id="template_inputs" class="hide">
<?php
    /* Elemen ini dipakai javascript untuk di clone atau diduplikasi
     */
    //heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (KG)','Harga Produsen','Harga Pedagang');

    //dropdown untuk kondisi ikan
    $attr_opsi_kondisi_ikan = array('input_id' => 'kondisi_1', 'input_name' => 'kondisi_1',
                                   'label_text' => '', 'array_opsi' => '', 'opsi_selected' => 'segar', 
                                  'input_width' => '', 'input_class' => 'form-control test', 'label_class' => 'none',
                                  'from_table' => 'mst_jenis_kondisi_ikan', 'field_value' => 'id_jenis_kondisi_ikan',
                                  'field_text' => 'nama_jenis_kondisi_ikan');
    echo $this->mkform->dropdown($attr_opsi_kondisi_ikan);
    
    //content input Jumlah(Kg)
    $attr_jml = array('input_id' => 'jml_1', 'input_name' => 'jml_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_jml);

    //content input Harga Produsen
    $attr_hrg_produsen = array('input_id' => 'hrg_produsen_1', 'input_name' => 'hrg_produsen_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_hrg_produsen);

    //content input Harga Pedagang
    $attr_hrg_pedagang = array('input_id' => 'hrg_pedagang_1', 'input_name' => 'hrg_pedagang_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_hrg_pedagang);

        //content input Harga Pedagang
    $attr_koef_koreksi = array('input_id' => 'koef_koreksi_1', 'input_name' => 'koef_koreksi_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_koef_koreksi);

 ?>
 <input id="pilih_ikan" name="pilih_ikan" type="text" class="col-lg-12" placeholder="">
</div>

<script>
    var id_aktivitas_masuk = <?php echo $id_aktivitas_masuk; ?>,
        list_opsi_alat_tangkap = <?php echo Modules::run('mst_alat_tangkap/json_alat_tangkap'); ?>;  
    // Mekanisme set detail kapal dari hasil pencarian kapal.
    // get_detail_kapal ini dipanggil sama script yang ada di file mst_kapal/views/pilih_kapal.php
    function get_detail_kapal(a_id_kapal, a_filter_gt)
        {
            // Ambil data kapal via ajax berdasarkan id_kapal dari kapal yang dipilih
            $.ajax({
              dataType: "json",
              url: "<?php echo base_url('mst_kapal/detail_kapal'); ?>", // URL detail kapal
              data: { id_kapal: a_id_kapal, filter_gt: a_filter_gt}, // Parameter get, jadinya mst_kapal/detail_kapal?id_kapal=a_id_kapal
              success: function(data){
                set_kapal(data, a_filter_gt);
              } // Kalau ajax berhasil, jalanin function set_kapal
            });
        }

    function get_detail_aktivitas(id_aktivitas)
        {
          notifharaptunggu = haraptunggu();
            $.ajax({
              dataType: "json",
              url: "<?php echo base_url('jurnal/H1/json_detail_H1'); ?>", //
              data: { id_aktivitas: id_aktivitas}, // 
              success: set_detail_aktivitas //       
            });
        }
    function set_detail_aktivitas(data)
        { 
          var d_nama_alat_tangkap  = '',
              d_nama_dpi = '';
          $("#nahkoda").text(data.nama_nahkoda);
          $("#nama_nahkoda").val(data.nama_nahkoda);

          $("#tgl_masuk_info").text(data.tgl_aktivitas);
          $("#tgl_masuk").val(data.tgl_aktivitas);

          list_opsi_alat_tangkap.forEach(function(d){
              if(d.id === data.id_alat_tangkap)
              {
                d_nama_alat_tangkap = d.text;
              }
          });
            if (data.id_dpi !== null) {
              $("#id_dpi").select2("val",data.id_dpi);
              d_nama_dpi = $("#id_dpi").select2("data")[0].text;
              $("#dpi").text(d_nama_dpi);
            };
           
            $("#alat_tangkap").text(d_nama_alat_tangkap);
            $("input[name=id_kapal]").val(data.id_kapal);
            $("#id_alat_tangkap").select2("val",data.id_alat_tangkap);

          get_detail_kapal(data.id_kapal, data.filter_gt);
          notifharaptunggu.pnotify_remove();

        }    
    function set_kapal(data, a_filter_gt)
        {
          // data disini hasil dari ajax di get_detail_kapal. 
          // Kalau mau liat isi dari data tinggal jalanin kode ini:
          //console.log(data);
          // semua text yang ada di ref_text diupdate disini berdasarkan data kapal yang didapat
          $("#nama_kapal").text(data.nama_kapal+' / '+data.no_sipi);
          $("input[name=nama_kapal_info]").val(data.nama_kapal);
          $("#tanda_selar").text(data.tanda_selar);
          $("#nama_pemilik").text(data.nama_penanggung_jawab);
          if(id_aktivitas_masuk === 'false'){
            $("#dpi").text(data.nama_dpi);
            $("#id_dpi").select2("val",data.id_dpi);
            $("#alat_tangkap").text(data.nama_alat_tangkap);
            $("input[name=id_kapal]").val(data.id_kapal);
            $("input[name=id_dpi]").val(data.id_dpi);
            $("#id_alat_tangkap").select2("val",data.id_alat_tangkap_perizinan);
          }

          if(a_filter_gt === "below")
          {
            $("#input_filter_gt").val("below");            
          }else
          {
            $("#input_filter_gt").val("above");       
          }
          // Untuk hidden input

        }

    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k', // TODO : Calon jadi library javascript
        list_opsi_ikan = <?php echo Modules::run('mst_jenis_ikan/json_ikan'); ?>;

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
        $('#table_h2').dataTable().fnAddData( [
                tempRowNum+".",
                cloner("#template_inputs #pilih_ikan", "ikan_"+tempRowNum),
                cloner("#template_inputs #kondisi_1", "kondisi_"+tempRowNum),
                cloner("#template_inputs #jml_1", "jml_"+tempRowNum),
                cloner("#template_inputs #hrg_produsen_1", "hrg_produsen_"+tempRowNum),
                cloner("#template_inputs #hrg_pedagang_1", "hrg_pedagang_"+tempRowNum),
                cloner("#template_inputs #koef_koreksi_1", "koef_koreksi_"+tempRowNum)
                ] );
            $("#ikan_"+tempRowNum).select2({  width: "100%",
                                              placeholder: "Mulai ketik nama ikan..",
                                              data: list_opsi_ikan
            
                                            });
            $("#jml_"+tempRowNum+", #hrg_pedagang_"+tempRowNum+", #hrg_produsen_"+tempRowNum).addClass("validate[custom[onlyNumberSp]]");
            rowCount++;
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_h2').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
      }

      function check_ikan_double()
      {
        var list_ikan_selected= [],
            found_double = false,
            found_at = '',
            ikan_double = [],
            row_counter = 0;

          $("[id^=ikan]").each(function(d,i) {
            var this_val = +$(this).select2("val");
            if( list_ikan_selected.indexOf(this_val) < 0 )
            {
              if( this_val !== 0)
              {
                list_ikan_selected.push(this_val);
              }
            }else{
              console.log("found!",row_counter);
              found_double = true;
              //found_at = $(this).parent().parent().children().first().text();
              list_for_pesan = "- "+$(this).select2('data').text;
              ikan_double.push(list_for_pesan);
            }
            row_counter++;
          });

          if(found_double)
          {
            var pesan = ' Ditemukan entry duplikat untuk jenis ikan berikut : <br>'+ikan_double.join('<br>')+'.';
            $.pnotify({
                text: pesan,
                type: 'notice',
                hide: true,
                closer: false,
                sticker: false
            });
          }
    
      }

    $(document).ready( function () {
      
             $("#id_alat_tangkap").select2({  width: "60%",
                                              placeholder: "Mulai ketik nama alat tangkap..",
                                              data: list_opsi_alat_tangkap
                                            });
      /* Desc : Inisialisasi plugin dataTable ke element #table_h2
       * konfigurasinya untuk matiin fungsi search, info table, pagination, dan sorting
       */
      $('#table_h2').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "25%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": false,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      } );

      
      // Panggil fungsi initTableRows;
      initTableRows();

      if(id_aktivitas_masuk !== false){
        get_detail_aktivitas(id_aktivitas_masuk);
        $("#panel-pilih-kapal").hide();
        $(".manual_input").hide();
        $("#ref_aktivitas_kapal").val(id_aktivitas_masuk);
      }else{
        $(".manual_input").show();
        $("#id_alat_tangkap").select2({  width: "60%",
                                              placeholder: "Mulai ketik nama alat tangkap..",
                                              data: list_opsi_alat_tangkap
                                            });
      }

      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $("#add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $("#del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });


      
      $("#table_h2").on("click","[id^=ikan]", check_ikan_double);
      $("#form_entry").validationEngine();
    } );  
</script>