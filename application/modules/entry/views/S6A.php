<?php
if( !$list_S6A ){
    $list_S6A = array(
                   'id_pelabuhan' => '',
                   'nama_pelabuhan' => '',
                   'nama_pelabuhan_inggris' => '',
                   'id_kelas_pelabuhan' => '',
                   'kelas_pelabuhan' => '',
                   'kode_pelabuhan' => '',
                   'id_wpp' => '',
                   'nama_kepala_pelabuhan' => '',
                   'alamat_pelabuhan' => '',
                   'desa' => '',
                   'kecamatan' => '',
                   'kabupaten' => '',
                   'provinsi' => '',
                   'id_status_pelabuhan' => '',
                   'lintang' => '',
                   'bujur' => '',
                   'no_telpon' => '',
                   'no_telpon2' => '',
                   'id_pengelola_pelabuhan' => '',
                   'id_propinsi' => '',
                   'pengelolah_pelabuhan' => '',
                   'email' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S6A/input';
}else{
    $submit_form = 'entry/S6A/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

$hide = array(
                            'input_id' => 'id_pelabuhan', 
                            'input_name' => 'id_pelabuhan' , 
                            'label_text' => '',
                            'input_value' => $list_S6A["id_pelabuhan"], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);
?>

  <div class="panel">
    <div class="panel-body">

  <?php
    $attr_nama_pelabuhan = array(
                                'input_id' => 'nama_pelabuhan', 
                                'input_name' => 'nama_pelabuhan' , 
                                'label_text' => 'Nama Pelabuhan <em>*</em> :', 
                                'input_value' => $list_S6A["nama_pelabuhan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_pelabuhan);

    $attr_nama_pelabuhan_inggris = array(
                                'input_id' => 'nama_pelabuhan_inggris', 
                                'input_name' => 'nama_pelabuhan_inggris' , 
                                'label_text' => 'Nama Pelabuhan Inggris :', 
                                'input_value' => $list_S6A["nama_pelabuhan_inggris"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_pelabuhan_inggris);

    $nama_kepala_pelabuhan = array(
                                'input_id' => 'nama_kepala_pelabuhan', 
                                'input_name' => 'nama_kepala_pelabuhan' , 
                                'label_text' => 'Nama Kepala Pelabuhan <em>*</em> :', 
                                'input_value' => $list_S6A["nama_kepala_pelabuhan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_kepala_pelabuhan);

    /*$attr_kelas_pelabuhan = array(
                                'input_id' => 'kelas_pelabuhan', 
                                'input_name' => 'kelas_pelabuhan' , 
                                'label_text' => 'Kelas Pelabuhan :', 
                                'input_value' => $list_S6A["kelas_pelabuhan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kelas_pelabuhan);*/

    //dropdown 
    $attr_kelas_pelabuhan = array(
                                'input_id' => 'id_kelas_pelabuhan', 
                                'input_name' => 'id_kelas_pelabuhan',
                                'label_text' => 'Kelas Pelabuhan :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6A["id_kelas_pelabuhan"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_kelas_pelabuhan', 
                                'field_value' => 'id_kelas_pelabuhan',
                                'field_text' => 'nama_kelas_pelabuhan'
                            );
    echo $this->mkform->dropdown($attr_kelas_pelabuhan);

    /*$attr_kode_pelabuhan = array(
                                'input_id' => 'kode_pelabuhan', 
                                'input_name' => 'kode_pelabuhan' , 
                                'label_text' => 'Kode Pelabuhan :', 
                                'input_value' => $list_S6A[""], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kode_pelabuhan);*/
    

    //pilih_status_pelabuhan
    $wpp = array(
        'input_id' => 'id_wpp', 
        'input_name' => 'id_wpp', 
        'label_text' => 'Nama WPP :', 
        'opsi_selected' => $list_S6A["id_wpp"], 
        'input_width' => 'col-lg-6 manual_input', 
        'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 manual_input control-label');
    echo $this->mkform->pilih_wpp($wpp);

    /*$wpp = array(
                                'input_id' => 'id_wpp', 
                                'input_name' => 'id_wpp',
                                'label_text' => 'Status Pelabuhan :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6A["id_wpp"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_wpp', 
                                'field_value' => 'id_wpp',
                                'field_text' => 'nama_wpp'
                            );
    echo $this->mkform->dropdown($wpp);*/

    /*$attr_wpp = array(
                                'input_id' => 'id_wpp', 
                                'input_name' => 'id_wpp' , 
                                'label_text' => 'WPP :', 
                                'input_value' => $list_S6A["id_wpp"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_wpp);*/
    
    $attr_alamat_pelabuhan = array(
                                'input_id' => 'alamat_pelabuhan', 
                                'input_name' => 'alamat_pelabuhan' , 
                                'label_text' => 'Alamat Pelabuhan <em>*</em> :', 
                                'input_value' => $list_S6A["alamat_pelabuhan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_alamat_pelabuhan);
    
    $attr_desa = array(
                                'input_id' => 'desa', 
                                'input_name' => 'desa' , 
                                'label_text' => 'Desa :', 
                                'input_value' => $list_S6A["desa"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_desa);
    
    $attr_kecamatan = array(
                                'input_id' => 'kecamatan', 
                                'input_name' => 'kecamatan' , 
                                'label_text' => 'Kecamatan :', 
                                'input_value' => $list_S6A["kecamatan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kecamatan);
    
    $attr_kabupaten = array(
                                'input_id' => 'kabupaten', 
                                'input_name' => 'kabupaten' , 
                                'label_text' => 'Kabupaten :', 
                                'input_value' => $list_S6A["kabupaten"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kabupaten);

    $attr_provinsi = array(
                                'input_id' => 'provinsi', 
                                'input_name' => 'provinsi' , 
                                'label_text' => 'Provinsi :', 
                                'input_value' => $list_S6A["provinsi"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    // echo $this->mkform->input($attr_provinsi);

    //dropdown 
    $propinsi = array(
                                'input_id' => 'id_propinsi', 
                                'input_name' => 'id_propinsi',
                                'label_text' => 'Propinsi :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6A["id_propinsi"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_propinsi', 
                                'field_value' => 'id_propinsi',
                                'field_text' => 'nama_propinsi'
                            );
    echo $this->mkform->dropdown_dss($propinsi);

    if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') )
    {
        //dropdown 
        $attr_status_pelabuhan = array(
                                    'input_id' => 'id_status_pelabuhan', 
                                    'input_name' => 'id_status_pelabuhan',
                                    'label_text' => 'Status Pelabuhan :', 
                                    'array_opsi' => '', 
                                    'opsi_selected' => $list_S6A["id_status_pelabuhan"], 
                                    'input_width' => 'col-lg-6 manual_input', 
                                    'input_class' => 'form-control test', 
                                    'label_class' => 'col-lg-4 manual_input control-label',
                                    'from_table' => 'mst_status_pelabuhan', 
                                    'field_value' => 'id_status_pelabuhan',
                                    'field_text' => 'status_pelabuhan'
                                );
        echo $this->mkform->dropdown($attr_status_pelabuhan);
        // var_dump($item->id_status_pelabuhan);
    } 

    /*$status_pelabuhan = array(
                                'input_id' => 'status_pelabuhan', 
                                'input_name' => 'status_pelabuhan' , 
                                'label_text' => 'Status Pelabuhan :', 
                                'input_value' => $list_S6A[""], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );*/
    //echo $this->mkform->input($attr_status_pelabuhan);

    $attr_lintang = array(
                                'input_id' => 'lintang', 
                                'input_name' => 'lintang' , 
                                'label_text' => 'Lintang :', 
                                'input_value' => $list_S6A["lintang"], 
                                'input_placeholder' => '000.0000000',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_lintang);

    $attr_bujur = array(
                                'input_id' => 'bujur', 
                                'input_name' => 'bujur' , 
                                'label_text' => 'Bujur :', 
                                'input_value' => $list_S6A["bujur"], 
                                'input_placeholder' => '000.0000000',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_bujur);

    $attr_no_telpon = array(
                                'input_id' => 'no_telpon', 
                                'input_name' => 'no_telpon' , 
                                'label_text' => 'No Telpon <em>*</em> :', 
                                'input_value' => $list_S6A["no_telpon"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_no_telpon);

    $attr_no_telpon2 = array(
                                'input_id' => 'no_telpon2', 
                                'input_name' => 'no_telpon2' , 
                                'label_text' => 'No Fax :', 
                                'input_value' => $list_S6A["no_telpon2"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_no_telpon2);

    /*$attr_pengelolah_pelabuhan = array(
                                'input_id' => 'pengelolah_pelabuhan', 
                                'input_name' => 'pengelolah_pelabuhan' , 
                                'label_text' => 'Pengelolah Pelabuhan :', 
                                'input_value' => $list_S6A["pengelolah_pelabuhan"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_pengelolah_pelabuhan);*/

    //dropdown 
    $attr_pengelolah_pelabuhan = array(
                                'input_id' => 'id_pengelola_pelabuhan', 
                                'input_name' => 'id_pengelola_pelabuhan',
                                'label_text' => 'Pengelola Pelabuhan :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S6A["id_pengelola_pelabuhan"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_pengelola_pelabuhan', 
                                'field_value' => 'id_pengelola_pelabuhan',
                                'field_text' => 'nama_pengelola_pelabuhan'
                            );
    echo $this->mkform->dropdown($attr_pengelolah_pelabuhan);

    $attr_email = array(
                                'input_id' => 'email', 
                                'input_name' => 'email' , 
                                'label_text' => 'Email :', 
                                'input_value' => $list_S6A["email"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_email);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S6A['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
  ?>
  <hr>
  
    </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#nama_pelabuhan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#nama_kepala_pelabuhan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#alamat_pelabuhan").addClass("validate[required,custom[onlyLetterSp]]");
          $("#no_telpon").addClass("validate[required,custom[onlyNumberSp]]");
        

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
