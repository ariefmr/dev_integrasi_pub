<?php
//dulunya hidden input
echo form_open('entry/S10A3/input', 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $curah_hujan_max = array(
                                'input_id' => 'curah_hujan_max', 
                                'input_name' => 'curah_hujan_max' , 
                                'label_text' => 'Curah Hujan Maximum <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($curah_hujan_max);

    $hujan_bulanan_avg = array(
                                'input_id' => 'hujan_bulanan_avg', 
                                'input_name' => 'hujan_bulanan_avg' , 
                                'label_text' => 'Hujan Bulanan AVG <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($hujan_bulanan_avg);

    $lembab_udara_avg = array(
                                'input_id' => 'lembab_udara_avg', 
                                'input_name' => 'lembab_udara_avg' , 
                                'label_text' => 'Lembab Udara AVG <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lembab_udara_avg);

    $suhu_udara_avg = array(
                                'input_id' => 'suhu_udara_avg', 
                                'input_name' => 'suhu_udara_avg' , 
                                'label_text' => 'Suhu Udara AVG <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($suhu_udara_avg);

    $bulan_angin_besar = array(
                                'input_id' => 'bulan_angin_besar', 
                                'input_name' => 'bulan_angin_besar' , 
                                'label_text' => 'Bulan Angin Besar :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($bulan_angin_besar);

    $kec_angin_max = array(
                                'input_id' => 'kec_angin_max', 
                                'input_name' => 'kec_angin_max' , 
                                'label_text' => 'Kecepatan Angin Maximum <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($kec_angin_max);

    $arah_angin = array(
                                'input_id' => 'arah_angin', 
                                'input_name' => 'arah_angin' , 
                                'label_text' => 'Arah Angin <em>*</em> :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($arah_angin);

    $sumber_data = array(
                                'input_id' => 'sumber_data', 
                                'input_name' => 'sumber_data' , 
                                'label_text' => 'Sumber Data :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_data);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#curah_hujan_max").addClass("validate[required,custom[onlyNumberSp]]");
          $("#hujan_bulanan_avg").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lembab_udara_avg").addClass("validate[custom[onlyNumberSp]]");
          $("#suhu_udara_avg").addClass("validate[required,custom[onlyNumberSp]]");
          $("#kec_angin_max").addClass("validate[required,custom[onlyNumberSp]]");
          $("#arah_angin").addClass("validate[required]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
