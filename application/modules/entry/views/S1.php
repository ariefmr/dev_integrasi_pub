<?php
//dulunya hidden input
echo form_open('entry/S1/input', 'id="form_entry" class="form-horizontal" role="form"');
?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    $opsi = array(
                   '1' => 'Perorangan',
                   '2' => 'PT',
                   '3' => 'UD',
                   '4' => 'KUD'
                  );
    $attr_nama_jns_badan_usaha = array('input_id' => 'id_jns_badan_usaha', 'input_name' => 'id_jns_badan_usaha', 'label_text' => 'Jenis Badan Usaha', 'array_opsi' => $opsi, 'opsi_selected' => 'segar', 
                            'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_badan_usaha);

    $attr_nama_industri = array(
                                'input_id' => 'nama_industri', 
                                'input_name' => 'nama_industri' , 
                                'label_text' => 'Nama Industri :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_industri);

    $attr_nama_pemilik = array(
                                'input_id' => 'nama_pemilik', 
                                'input_name' => 'nama_pemilik' , 
                                'label_text' => 'Nama Pemilik :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_pemilik);
    
    $attr_alamat = array(
                                'input_id' => 'alamat', 
                                'input_name' => 'alamat' , 
                                'label_text' => 'Alamat :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_alamat);
    
    $attr_nilai_investasi_produsen = array(
                                'input_id' => 'nilai_investasi_produsen', 
                                'input_name' => 'nilai_investasi_produsen' , 
                                'label_text' => 'Nilai Investasi Produsen :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nilai_investasi_produsen);

    $attr_jml_tenaga_kerja = array(
                                'input_id' => 'jml_tenaga_kerja', 
                                'input_name' => 'jml_tenaga_kerja' , 
                                'label_text' => 'Jumlah Tenaga Kerja :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jml_tenaga_kerja);

    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>


