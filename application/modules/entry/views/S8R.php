<?php
if( !$list_S8R ){
    $list_S8R = array(
                   'id_kendaraan' => '',
                   'id_jns_kendaraan' => '',
                   'nama_kendaraan' => '',
                   'tgl_catat' => '',
                   'tahun' => '',
                   'nama_merek' => '',
                   'ket_rehab' => '',
                   'jns_konstruksi' => '',
                   'ket_teknis' => '',
                   'jml' => '',
                   'jml' => '',
                   'luas' => '',
                   'nilai' => '',
                   'ket' => '',
                   'sumber_dana' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S8R/input';
}else{
    $submit_form = 'entry/S8R/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

//hidden id untuk edit
$hide = array(
                            'input_id' => 'id_kendaraan', 
                            'input_name' => 'id_kendaraan' , 
                            'label_text' => '',
                            'input_value' => $list_S8R['id_kendaraan'], 
                            'input_placeholder' => '',
                            'input_type' => 'text', 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_class' => 'col-lg-4 manual_input', 
                            'input_class' => 'hide' 
                            );
echo $this->mkform->input($hide);

?>

  <div class="panel">
        <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    /*$opsi = array(
                   '1' => 'A',
                   '2' => 'B',
                   '3' => 'C',
                   '4' => 'C',
                   '5' => 'C',
                   '6' => 'C',
                  );
    $id_jns_kendaraan = array('input_id' => 'id_jns_kendaraan', 'input_name' => 'id_jns_kendaraan', 'label_text' => 'Jenis Kendaraan', 
        'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 
        'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($id_jns_kendaraan);*/
/*
    $id_jns_kendaraan = array(
                                'input_id' => 'id_jns_kendaraan', 
                                'input_name' => 'id_jns_kendaraan',
                                'label_text' => 'Jenis Fasilitas Kendaraan <em>*</em> :', 
                                'array_opsi' => '', 
                                'opsi_selected' => $list_S8R["id_jns_kendaraan"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_kendaraan', 
                                'field_value' => 'id_jns_kendaraan',
                                'field_text' => 'nama_jns_kendaraan'
                            );
    echo $this->mkform->dropdown($id_jns_kendaraan);*/

    $opsi_jenis_kendaraan = array (
                                 'Forklip' => 'Forklip',
                                 'Alat Berat' => 'Alat Berat',
                                'Kapal Pengawas' => 'Kapal Pengawas',
                                'Kapal Tunda' => 'Kapal Tunda',
                                'Kapal Keruk' => 'Kapal Keruk',
                                'Kapal Syahbandar' => 'Kapal Syahbandar',
                                'Lainnya' => 'Lainnya',

                                );

    $nama_kendaraan = array(
                                'input_id' => 'nama_kendaraan', 
                                'input_name' => 'nama_kendaraan',
                                'label_text' => 'Tipe Dermaga <m>*<m> :', 
                                'array_opsi' => $opsi_jenis_kendaraan, 
                                'opsi_selected' => $list_S8R["nama_kendaraan"],
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label',
                                /*'from_table' => 'mst_jenis_sumber_air', 
                                'field_value' => 'id_jns_sumber_air',
                                'field_text' => 'nama_jns_sumber_air'*/
                            );
    echo $this->mkform->dropdown($nama_kendaraan);


    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <em>*</em> :',
                                'input_value' => $list_S8R['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S8R['ket_rehab'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);

    $nama_merek = array(
                                'input_id' => 'nama_merek', 
                                'input_name' => 'nama_merek' , 
                                'label_text' => 'Nama Merek :',
                                'input_value' => $list_S8R['nama_merek'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nama_merek);

    $jml = array(
                                'input_id' => 'jml', 
                                'input_name' => 'jml' , 
                                'label_text' => 'Jumlah :',
                                'input_value' => $list_S8R['jml'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jml);

    $ket_teknis = array(
                                'input_id' => 'ket_teknis', 
                                'input_name' => 'ket_teknis' , 
                                'label_text' => 'Keterangan Teknis :',
                                'input_value' => $list_S8R['ket_teknis'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_teknis);

    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <em>*</em> :',
                                'input_value' => $list_S8R['sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);

    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai :',
                                'input_value' => $list_S8R['nilai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);

    $ket = array(
                                'input_id' => 'ket', 
                                'input_name' => 'ket' , 
                                'label_text' => 'Keterangan :',
                                'input_value' => $list_S8R['ket'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S8R['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);

  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#id_jns_kendaraan").addClass("validate[required]");
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");
          $("#jml").addClass("validate[required,custom[onlyNumberSp]]");
          $("#luas").addClass("validate[required,custom[onlyNumberSp]]");
          $("#nama_merek").addClass("validate[custom[onlyLetterSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");

        }

        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>
