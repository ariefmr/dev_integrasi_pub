<?php
echo form_open('entry/B2/input', 'id="form_entry" class="form-horizontal" role="form"');
?>
   <div class="panel">
    <div class="panel-body">
  <?php
  $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);
    $tmpl = array ( 'table_open'  => '<table id="table_b2" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Supplier', 'Jenis Perbekalan', 'Volume', 'Satuan','Harga Satuan','Luar Pelabuhan');


    $table_b2 = $this->table->generate();

       $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);

    ?>
    <div class="form-group">
      <div class="col-lg-4">
        <button id="add_row" type="button" class="btn btn-default">Tambah Baris</button>
        <button id="del_row" type="button" class="btn btn-default">Kurangi Baris</button>
      </div>
    </div> 
    
    <?php

    echo $table_b2;
  ?>
          </div>
    <div class="panel-footer">
              <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>



<div id="template_inputs" class="hide">
<?php
    //dropdown untuk jenis Supplier
    //$attr_opsi_supplier = array('input_id' => 'supplier_1', 'input_name' => 'supplier_1', 'id_pelabuhan_user' => $id_pelabuhan, 'label_text' => '', 'array_opsi' => '', 'opsi_selected' => '', 
    //                        'input_width' => 'col-lg-9', 'input_class' => '', 'label_class' => 'none');
    //echo $this->mkform->pilih_industri($attr_opsi_supplier);

    //dropdown untuk jenis perbekalan
    $attr_opsi_jenis_perbekalan = array('input_id' => 'perbekalan_1', 'input_name' => 'perbekalan_1', 'label_text' => '', 'array_opsi' => '', 'opsi_selected' => '0', 'input_placeholder' => 'test',
                            'input_width' => 'col-lg-9', 'input_class' => 'form-control select_perbekalan', 'label_class' => 'none');
    echo $this->mkform->pilih_perbekalan($attr_opsi_jenis_perbekalan);
    
    /*
    $attr_satuan = array('input_id' => 'satuan_1', 'input_name' => 'satuan_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_satuan);
    */
    //content input untuk volume
    $attr_volume = array('input_id' => 'volume_1', 'input_name' => 'volume_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_volume);

    //content Label untuk satuan
    echo '<input type="text" id="satuandisplay_1" name="satuandisplay_1" class="form-control text-center" value="Lt" disabled>';

    //content input untuk satuan hidden
    $attr_satuan = array('input_id' => 'satuan_1', 'input_name' => 'satuan_1' , 'label_text' => '', 'input_value' => 'Lt', 'input_placeholder' => '',
                     'input_type' => 'hidden', 'input_width' => 'hide', 'label_class' => '', 'input_class' => 'hide' );
    echo $this->mkform->input($attr_satuan);

    $attr_harga = array('input_id' => 'harga_1', 'input_name' => 'harga_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => 'col-lg-4', 'label_class' => 'col-lg-2', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_harga);
 ?>

    <input id="cek_1" name="cek_1" type="checkbox" value="1">

 <input id="pilih_supplier" name="pilih_supplier" type="text" class=" " placeholder="">
 <!-- <input id="pil_perbekalan" name="pil_perbekalan" type="text" class="select_perbekalan" placeholder=""> -->
</div>

<script>
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k'; // TODO : Calon jadi library javascript
        list_opsi_supplier = <?php echo Modules::run('mst_industri/json_industri', $id_pelabuhan); ?>;

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
        $('#table_b2').dataTable().fnAddData( [
                tempRowNum+".",
                cloner("#template_inputs #pilih_supplier", "supplier_"+tempRowNum),
                cloner("#template_inputs #perbekalan_1", "perbekalan_"+tempRowNum),
                cloner("#template_inputs #volume_1", "volume_"+tempRowNum),
                cloner("#template_inputs #satuandisplay_1", "satuandisplay_"+tempRowNum) + cloner("#template_inputs #satuan_1", "satuan_"+tempRowNum),
                cloner("#template_inputs #harga_1", "harga_"+tempRowNum), 
                cloner("#template_inputs #cek_1", "cek_"+tempRowNum)
                ] );
          $('#supplier_'+tempRowNum).select2({width: "100%",
                                              placeholder: "Mulai ketik nama supplier..",
                                              data: list_opsi_supplier //list data di javascript harus json!
                                            });
          $('#satuan_'+tempRowNum).val('Lt');
            rowCount++;
          /*$('#perbekalan_'+tempRowNum).select2({width: "100%",
                                              placeholder: "Mulai ketik nama perbekalan..",
                                              data: list_opsi_perbekalan //list data di javascript harus json!
                                            });*/
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_b2').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
      }

      //set satuan
      function listenerSatuan() {
        var jenisPerbekalan = $(this).val(), 
            indexNumber = $(this).prop('id').split("_")[1],
            tipeSatuan = $('option:selected', this).data('tipeSatuan');
            //tipeSatuan = $('select2:selected', this).data('satuan');
            //tipeSatuan = $("#perbekalan_"+indexNumber).select2("val",data.satuan);
        console.log(jenisPerbekalan);
        console.log(indexNumber);
        console.log(tipeSatuan);
        $('#satuandisplay_'+indexNumber).val(tipeSatuan);
        $('#satuan_'+indexNumber).val(tipeSatuan);

      }


    $(document).ready( function () {
      

      /* Desc : Inisialisasi plugin dataTable ke element #table_b2
       * konfigurasinya untuk matiin fungsi search, info table, pagination, dan sorting
       */
      $('#table_b2').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "30%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "5%", "sClass": "text-center"},
                        { "sWidth": "5%", "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"}
                        
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      } );

      
      // Panggil fungsi initTableRows;
      initTableRows();

      //
      $('.select_perbekalan').on("change", listenerSatuan);

      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $("#add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $("#del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });

    } );  
</script>