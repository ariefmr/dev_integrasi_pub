<?php
if( !$list_S7H ){
    $list_S7H = array(
                   'tgl_catat' => '',
                   'id_fas_hubung' => '',
                   'id_jns_fas_hubung' => '',
                   'tahun' => '',
                   'ket_rehab' => '',
                   'beban_gandar' => '',
                   'jns_konstruksi' => '',
                   'panjang' => '',
                   'lebar' => '',
                   'ket_konstruksi' => '',
                   'id_jns_ket_konstruksi' => '',
                   'sumber_dana' => '',
                   'nilai' => '',
                   'ket_sumber_dana' => '',
                   'gambar' => ''
                   );
    $submit_form = 'entry/S7H/input';
}else{
    $submit_form = 'entry/S7H/update';
}

// echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>

<div class="panel">
    <div class="panel-body">

  <?php $hide = array(
                                'input_id' => 'id_pelabuhan', 
                                'input_name' => 'id_pelabuhan' , 
                                'label_text' => '',
                                'input_value' => $id_pelabuhan_selected, 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide); ?>
  <?php

    // FORM Input
    $hide = array(
                                'input_id' => 'id_fas_hubung', 
                                'input_name' => 'id_fas_hubung' , 
                                'label_text' => '',
                                'input_value' => $list_S7H["id_fas_hubung"], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($hide);

    //dropdown 
    $opsi_fas_hubung =  array(
                                '2' => 'Jembatan',
                                '3' => 'Drainase Terbuka',
                                '4' => 'Drainase Tertutup'
                              );
    $id_jns_fas_hubung = array(
                                'input_id' => 'id_jns_fas_hubung', 
                                'input_name' => 'id_jns_fas_hubung',
                                'label_text' => 'Jenis Fasilitas Penghubung :', 
                                'array_opsi' => $opsi_fas_hubung, 
                                'opsi_selected' => $list_S7H["id_jns_fas_hubung"], 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-4 manual_input control-label'
                                //'from_table' => 'mst_jenis_fas_hubung', 
                                //'field_value' => 'id_jns_fas_hubung',
                                //'field_text' => 'nama_jns_fas_hubung'
                            );
    echo $this->mkform->dropdown($id_jns_fas_hubung);

    $tahun = array(
                                'input_id' => 'tahun', 
                                'input_name' => 'tahun' , 
                                'label_text' => 'Tahun Pembuatan <m>*<m> :',
                                'input_value' => $list_S7H['tahun'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($tahun);

    $ket_rehab = array(
                                'input_id' => 'ket_rehab', 
                                'input_name' => 'ket_rehab' , 
                                'label_text' => 'Keterangan Rehab :',
                                'input_value' => $list_S7H['ket_rehab'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_rehab);

    $beban_gandar = array(
                                'input_id' => 'beban_gandar', 
                                'input_name' => 'beban_gandar' , 
                                'label_text' => 'Beban Gandar <m>*<m> :',
                                'input_value' => $list_S7H['beban_gandar'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($beban_gandar);

    $jns_konstruksi = array(
                                'input_id' => 'jns_konstruksi', 
                                'input_name' => 'jns_konstruksi' , 
                                'label_text' => 'Jenis Konstruksi <m>*<m> :',
                                'input_value' => $list_S7H['jns_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($jns_konstruksi);

    $panjang = array(
                                'input_id' => 'panjang', 
                                'input_name' => 'panjang' , 
                                'label_text' => 'Panjang <m>*<m> :',
                                'input_value' => $list_S7H['panjang'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($panjang);

    $lebar = array(
                                'input_id' => 'lebar', 
                                'input_name' => 'lebar' , 
                                'label_text' => 'Lebar <m>*<m> :',
                                'input_value' => $list_S7H['lebar'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($lebar);

    $ket_konstruksi = array(
                                'input_id' => 'ket_konstruksi', 
                                'input_name' => 'ket_konstruksi' , 
                                'label_text' => 'Keterangan Konstruksi :',
                                'opsi_selected' => $list_S7H['ket_konstruksi'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test',
								'label_class' => 'col-lg-4 manual_input control-label',
                                'from_table' => 'mst_jenis_ket_konstruksi', 
                                'field_value' => 'id_jns_ket_konstruksi',
                                'field_text' => 'nama_jns_ket_konstruksi',
                                'array_opsi' => ''
                                );
    echo $this->mkform->dropdown($ket_konstruksi);

    $sumber_dana = array(
                                'input_id' => 'sumber_dana', 
                                'input_name' => 'sumber_dana' , 
                                'label_text' => 'Sumber Dana <m>*<m> :',
                                'input_value' => $list_S7H['sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($sumber_dana);

    $nilai = array(
                                'input_id' => 'nilai', 
                                'input_name' => 'nilai' , 
                                'label_text' => 'Nilai :',
                                'input_value' => $list_S7H['nilai'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($nilai);

    $ket_sumber_dana = array(
                                'input_id' => 'ket_sumber_dana', 
                                'input_name' => 'ket_sumber_dana' , 
                                'label_text' => 'Keterangan Sumber Dana :',
                                'input_value' => $list_S7H['ket_sumber_dana'], 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($ket_sumber_dana);

    $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => $list_S7H['gambar'], 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($gambar);
    
  ?>
  <hr>
  
          </div>
    <div class="panel-footer">
        <?php
        $attr_datepick = array(
                                'input_id' => 'tgl_catat', 
                                'input_name' => 'tgl_catat' , 
                                'label_text' => 'Tanggal Catat :',
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'hide', 
                                'label_class' => 'hide', 
                                'input_class' => 'form-control' 
                              );
        echo $this->mkform->datepick($attr_datepick);
        ?>
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global"></text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>

<script type="text/javascript">
    
        function set_validation()
        {
          $("#tahun").addClass("validate[required,custom[isCorrectYear]]");  
          $("#beban_gandar").addClass("validate[required,custom[onlyNumberSp]]");
          $("#jns_konstruksi").addClass("validate[required]");
          $("#panjang").addClass("validate[required,custom[onlyNumberSp]]");
          $("#lebar").addClass("validate[required,custom[onlyNumberSp]]");
          $("#sumber_dana").addClass("validate[required]");
          $("#nilai").addClass("validate[custom[onlyNumberSp]]");


        $(document).ready(function(){
            set_validation();
            $("#form_entry").validationEngine();
        });
</script>

