<?php
$hidden_input = array('nama_kapal_info' => '');
    echo form_open('entry/H1/input','id="form_entry" class="form-horizontal" role="form" ', $hidden_input);
?>

            <?php echo Modules::run('mst_kapal/wgt_pilih_kapal'); ?>

    <div class="well well-sm panel-text-referensi">
             <?php                        // Label , Isi 
                  $array_info_kapal = array( 
                                              'Nama Kapal / SIPI : ' => array( 'id' => 'nama_kapal',
                                                                      'value' => '...'),
                                              'Tanda Selar : ' => array( 'id' => 'tanda_selar',
                                                                      'value' => '...'),
                                              'Pemilik :' => array( 'id' => 'nama_pemilik',
                                                                  'value' => '...'),
                                              'Tanggal Keberangkatan : ' => array( 'id' => 'tgl_keberangkatan_info',
                                                                      'value' => '...'),
                                              'Tujuan Keberangkatan : ' => array( 'id' => 'tujuan_berangkat_info',
                                                                      'value' => '...'),
                                              'Pelabuhan Keberangkatan : ' => array( 'id' => 'pelabuhan_berangkat_info',
                                                                      'value' => '...'),
                                              'Alat Tangkap : ' => array( 'id' => 'alat_tangkap',
                                                                      'value' => '...'),
                                              'DPI : ' => array( 'id' => 'dpi',
                                                                      'value' => '...'),
                                              'Nahkoda : ' => array( 'id' => 'nahkoda',
                                                                      'value' => '...'),
                                              'Jumlah ABK : ' => array( 'id' => 'jml_abk',
                                                                      'value' => '...')
                                           ); 
                  echo $this->mkform->ref_text($array_info_kapal);
             ?>  
  </div>


  <div class="panel form-input">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-9">
        <input type="hidden" id="ref_aktivitas_kapal" name="ref_aktivitas_kapal" value="00">
        <input type="hidden" name="nama_kapal_info" id="nama_kapal_info">
  <?php
        $attr_datepick = array('input_id' => 'tgl_catat', 
                               'input_name' => 'tgl_catat' , 
                               'label_text' => 'Tanggal Catat :', 
                               'input_value' => '', 
                               'input_placeholder' => '',
                               'input_type' => 'text', 
                               'input_width' => 'hide', 
                               'label_class' => 'hide', 
                               'input_class' => 'form-control' );
        echo $this->mkform->datepick($attr_datepick);
        //conten Jumlah ABK
        // $attr_id_pelabuhan_berangkat = array('input_id' => 'id_pelabuhan_berangkat', 
        //                                      'input_name' => 'id_pelabuhan_berangkat' , 
        //                                      'label_text' => '', 
        //                                      'input_value' => '', 
        //                                      'input_placeholder' => 'masukan jumlah abk',
        //                                      'input_type' => 'hidden', 
        //                                      'input_width' => 'hide', 
        //                                      'label_class' => 'hide', 
        //                                      'input_class' => 'form-control' );
        // echo $this->mkform->input($attr_id_pelabuhan_berangkat);

        $attr_id_alat_tangkap = array('input_id' => 'id_alat_tangkap', 
                                      'input_name' => 'id_alat_tangkap' , 
                                      'label_text' => '', 
                                      'input_value' => '', 
                                      'input_placeholder' => 'masukan jumlah abk',
                                      'input_type' => 'hidden', 
                                      'input_width' => 'hide', 
                                      'label_class' => 'hide', 
                                      'input_class' => 'form-control' );
        echo $this->mkform->input($attr_id_alat_tangkap);

        $attr_datepick_tgl_keberangkatan = array('input_id' => 'tgl_keberangkatan',
                                                 'input_name' => 'tgl_keberangkatan' ,
                                                 'label_text' => 'Tanggal Keberangkatan <em>*</em> :',
                                                 'input_value' => '', 'input_placeholder' => '',
                                                 'input_type' => 'text',
                                                 'input_width' => 'col-lg-6 manual_input',
                                                 'label_class' => 'col-lg-4 manual_input',
                                                 'input_class' => 'form-control validate[required] datepicker' );
        echo $this->mkform->datepick($attr_datepick_tgl_keberangkatan);

        $attr_datepick_tgl_kedatangan = array('input_id' => 'tgl_kedatangan',
                                              'input_name' => 'tgl_kedatangan' ,
                                              'label_text' => 'Tanggal Kedatangan <em>*</em> :',
                                              'input_value' => '',
                                              'input_placeholder' => '',
                                              'input_type' => 'text',
                                              'input_width' => 'col-lg-6',
                                              'label_class' => 'col-lg-4',
                                              'input_class' => 'form-control validate[required] datepicker' );
        echo $this->mkform->datepick($attr_datepick_tgl_kedatangan);
        
        $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_berangkat',
                                                 'input_name' => 'id_pelabuhan_berangkat',
                                                 'label_text' => 'Pelabuhan Keberangkatan <em>*</em> :',
                                                 'array_opsi' => '', 
                                                 'opsi_selected' => '', 
                                                 'input_width' => 'col-lg-6 manual_input',
                                                 'input_class' => '',
                                                 'label_class' => 'col-lg-4 manual_input control-label',
                                                 'placeholder' => 'Pilih Pelabuhan Tujuan');
        echo $this->mkform->pilih_pelabuhan_dropdown($attr_opsi_pelabuhan);

        // $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_berangkat',
        //                              'input_name' => 'id_pelabuhan_berangkat',
        //                              'label_text' => 'Pelabuhan Keberangkatan <em>*</em> :',
        //                              'array_opsi' => '', 'opsi_selected' => '', 
        //                              'input_width' => 'col-lg-6 manual_input',
        //                              'input_class' => '',
        //                              'label_class' => 'col-lg-4 manual_input control-label');
        // echo $this->mkform->pilih_pelabuhan_lama($attr_opsi_pelabuhan);

        $attr_opsi_dpi = array('input_id' => 'id_dpi',
                               'input_name' => 'id_dpi',
                               'label_text' => 'DPI :',
                               'input_value' => '',
                               'input_placeholder' => 'mulai ketik DPI',
                               'input_type' => 'text',
                               'input_width' => 'col-lg-6 manual_input',
                               'label_class' => 'col-lg-4 text-right manual_input',
                               'input_class' => '' );
              echo $this->mkform->pilih_dpi($attr_opsi_dpi);

        //conten Nama Nahkoda
        $attr_nahkoda = array('input_id' => 'nama_nahkoda',
                              'input_name' => 'nama_nahkoda',
                              'label_text' => 'Nama Nahkoda <em>*</em> :',
                              'input_value' => '',
                              'input_placeholder' => 'masukan nama nahkoda',
                              'input_type' => 'text',
                              'input_width' => 'col-lg-6 manual_input',
                              'label_class' => 'col-lg-4 manual_input',
                              'input_class' => 'form-control' );
        echo $this->mkform->input($attr_nahkoda);

        //conten Jumlah ABK
        $attr_abk = array('input_id' => 'jumlah_abk',
                          'input_name' => 'jumlah_abk',
                          'label_text' => 'Jumlah ABK <em>*</em> :',
                          'input_value' => '',
                          'input_placeholder' => 'masukan jumlah abk',
                          'input_type' => 'text',
                          'input_width' => 'col-lg-6 manual_input',
                          'label_class' => 'col-lg-4 manual_input',
                          'input_class' => 'form-control' );
        echo $this->mkform->input($attr_abk);

        /*content Sumber Data
        $opsi_sumber_data = array( 'tempat_pelelangan_ikan' => 'Tempat Pelelangan Ikan',
                            'nahkoda' => 'Nahkoda',
                            'lainnya' => 'Lainnya'
                           );
        
        $attr_opsi_sumber = array('input_id' => 'sumber_data', 'input_name' => 'sumber_data', 'label_text' => 'Sumber Data :', 'array_opsi' => $opsi_sumber_data, 'opsi_selected' => 'Tempat Pelelangan Ikan', 
                            'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
        echo $this->mkform->dropdown($attr_opsi_sumber);
        */

        //conten Jumlah Hari Trip
        $attr_jml = array('input_id' => 'jml_hari_trip',
                          'input_name' => 'jml_hari_trip',
                          'label_text' => 'Jumlah Hari Trip <em>*</em> :',
                          'input_value' => '',
                          'input_placeholder' => 'masukan jml hari trip',
                          'input_type' => 'text',
                          'input_width' => 'col-lg-6',
                          'label_class' => 'col-lg-4',
                          'input_class' => 'form-control' );
        echo $this->mkform->input($attr_jml);

        //content Maksud Kunjungan Checkbox
        /*$opsi_checkbox_maksudkunjungan = array( 'is_bongkar' => 'Bongkar',
                                                'is_tambat' => 'Tambat',
                                                'is_labuh' => 'Labuh',
                                                'is_docking' => 'Docking',
                                                'is_floating' => 'Floating',
                                                'is_repair' => 'Repair',
                                                'is_perbekalan' => 'Perbekalan',
                                                'is_muat' => 'Muat Ikan'
                                                );
        $attr_maksud_kunjungan = array('label_text' => 'Maksud Kunjungan :', 'array_checkbox' => $opsi_checkbox_maksudkunjungan, 'label_class' => 'col-lg-4');
        echo $this->mkform->checkbox2($attr_maksud_kunjungan);
        */
  ?>
      <input type="hidden" id="tgl_keberangkatan" name="tgl_keberangkatan">
      <input type="hidden" id="tujuan_berangkat" name="tujuan_berangkat">     
  <input type="hidden" id="input_filter_gt" name="input_filter_gt">

  <div class="form-group">
    
    <label class="col-lg-4 control-label">Maksud Kunjungan :</label>
    
    <div class="col-lg-6">
      <div class="row">
        <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_bongkar" class="check-maksud-kunjungan" name="is_bongkar" value="1">Bongkar Ikan</label>
        <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_tambat" class="check-maksud-kunjungan" name="is_tambat" value="1">Tambat</label>
      </div>
      <div class="row">
        <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_labuh" class="check-maksud-kunjungan" name="is_labuh" value="1">Labuh</label>
        <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_docking" class="check-maksud-kunjungan" name="is_docking" value="1">Docking</label>
      </div>
    <div class="row">
      <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_perbaikan" class="check-maksud-kunjungan" name="is_perbaikan" value="1">Repair</label>
      <label class="checkbox-inline col-lg-5"><input type="checkbox" id="is_perbekalan" class="check-maksud-kunjungan" name="is_perbekalan" value="1">Perbekalan</label>
    </div>
    </div>
  </div>
  

        </div>
      </div>
    </div>
    <div class="panel-footer">
        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
    </div>
  </div>
 
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <button id="btn-simpan" type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
  </div>

</form>



<script>
      var entry_status = "<?php echo $entry_status; ?>",
          nama_kapal_entry = "<?php echo $nama_kapal; ?>",
          id_aktivitas_keluar = <?php echo $id_aktivitas_keluar ?>,
          list_opsi_alat_tangkap = <?php echo Modules::run('mst_alat_tangkap/json_alat_tangkap'); ?>,
          notifharaptunggu,
          one_day=1000*60*60*24;

        function notif_entry_success()
        {
          if(entry_status === 'success')
          {
            var pesan = ' Entry Form Keberangkatan Kapal <br>'+nama_kapal_entry+' Berhasil';
            $.pnotify({
                title: 'Entry Berhasil',
                text: pesan,
                type: 'success'
            });
          }
        }

        function update_opsi_dpi(izin_dpi)
        {
          $("#id_dpi option").each(function(){ 
            var this_val = $(this).val();
            if ($.inArray(this_val, izin_dpi) < 0)
            {
              $(this).prop("disabled", true);
            }
          });
          $("#id_dpi").select2("val", izin_dpi);
        }

        function update_opsi_pelabuhan(id_pelabuhan_berangkat)
        {
              

          // $("#id_pelabuhan_berangkat option").each(function(){ 
          //   var this_val = $(this).val();
          //   if ($.inArray(this_val, izin_pelabuhan_pangkalan) < 0)
          //   {
          //     $(this).prop("disabled", true);
          //   }
          // });
          // $("#id_pelabuhan_berangkat").select2("val", izin_pelabuhan_pangkalan);

          $("#id_pelabuhan_tujuan").select2("val","");
          $("#id_pelabuhan_tujuan").select2("val", id_pelabuhan_berangkat);
        } 

      // Mekanisme set detail kapal dari hasil pencarian kapal.
      // get_detail_kapal ini dipanggil sama script yang ada di file mst_kapal/views/pilih_kapal.php
        function get_detail_kapal(a_id_kapal, a_filter_gt)
        {
          notifharaptunggu = haraptunggu();
            // Ambil data kapal via ajax berdasarkan id_kapal dari kapal yang dipilih
            $.ajax({
              dataType: "json",
              url: "<?php echo base_url('mst_kapal/detail_kapal'); ?>", // URL detail kapal
              data: { id_kapal: a_id_kapal, filter_gt: a_filter_gt}, // Parameter get, jadinya mst_kapal/detail_kapal?id_kapal=a_id_kapal
              success: function(data){
                set_kapal(data, a_filter_gt);
              } // Kalau ajax berhasil, jalanin function set_kapal
            });
        }

        function get_detail_aktivitas(id_aktivitas)
        {
            $.ajax({
              dataType: "json",
              url: "<?php echo base_url('jurnal/H5/json_detail_H5'); ?>", //
              data: { id_aktivitas: id_aktivitas}, // 
              success: set_detail_aktivitas
            });
        }

        function set_kapal(data, a_filter_gt)
        {
          // data disini hasil dari ajax di get_detail_kapal. 
          // Kalau mau liat isi dari data tinggal jalanin kode ini:
          // console.log(data);
          // semua text yang ada di ref_text diupdate disini berdasarkan data kapal yang didapat
          var array_dpi = [],
              array_pelabuhan_pangkalan = [],
              item_kosong = [],
              pesan_warning = '',
              required_item = ['id_dpi','id_wpp','nama_wpp','id_alat_tangkap','nama_penanggung_jawab', 'id_pelabuhan_pangkalan'];
          $(".form-input input,select").prop("disabled", false);

          //Cek value data kapal 
          for( item in data )
          {
            if(required_item.indexOf(item) > -1 )
            {
              if(data[item] === null || data[item] === "" || data[item] === '0' || data[item] === 'kosong' || data[item] === undefined)
              {
                console.log(data[item]);
                data[item] = 'Tidak ada data.';
                item_kosong.push(item);
              }
            }

          }
          
          // for ( item in data )
          // {
          //   console.log(item+": "+data[item]+" ("+(typeof data[item])+") isnull( "+(data[item] === null)+" )");
          // }
          //console.log(data);
          if(item_kosong.length > 0)
          {
            data_kosong = item_kosong.join(',');
            pesan_warning= 'Data berikut tidak tersedia untuk kapal '+data.nama_kapal+' : <br>'+ data_kosong;
            $.pnotify({
              title: 'Data Kapal Tidak Lengkap',
              text: pesan_warning,
              type: 'warning'
            });
          }
     

          if(data.id_dpi !== 'Tidak ada data.')
          {
              if(data.id_dpi.indexOf(",") > 0 )
              {
                array_dpi = data.id_dpi.split(", ");
              }else{
                array_dpi.push(data.id_dpi);
              }
              update_opsi_dpi(array_dpi);      
          }

          if(data.id_pelabuhan_pangkalan !== 'Tidak ada data.' && id_aktivitas_keluar === '')
          {
              if(data.id_pelabuhan_pangkalan.indexOf(",") > 0 )
              {
              array_pelabuhan_pangkalan = data.id_pelabuhan_pangkalan.split(", ");
              }else{
                array_pelabuhan_pangkalan.push(data.id_pelabuhan_pangkalan);
              }
            console.log(array_pelabuhan_pangkalan);

              update_opsi_pelabuhan(array_pelabuhan_pangkalan);  
          }

          if(data.id_alat_tangkap !== 'Tidak ada data.')
          {
            $("#id_alat_tangkap").select2("val",data.id_alat_tangkap);
            if(current_gt() === "above") { 
              $("#id_alat_tangkap").select2("readonly",true); 
            }
            else { 
              $("#id_alat_tangkap").select2("readonly",false);
            }
            if(data.nama_alat_tangkap === 'Pengangkut')
            {
              $("#tujuan_berangkat").val('angkut');
              //listener_tujuan_berangkat('angkut');
            }
          }

          if(a_filter_gt === "below")
          {
            $("#input_filter_gt").val("below");            
          }else
          {
            $("#input_filter_gt").val("above");       
          }
          
          $("#nama_kapal").text(data.nama_kapal+' / '+data.no_sipi);
          $("#tanda_selar").text(data.tanda_selar);
          $("#nama_pemilik").text(data.nama_penanggung_jawab);
          $("#wpp_info").text(data.nama_wpp);
          $("#alat_tangkap_info").text(data.nama_alat_tangkap);
          $("#pelabuhan_pangkalan_info").text(data.nama_pelabuhan_pangkalan);

          $("input[name=nama_kapal_info]").val(data.nama_kapal);
                      notifharaptunggu.pnotify_remove();


        }

        function listener_update_jumlah_hari_trip()
        {
                $("#tgl_keberangkatan_display,#tgl_kedatangan_display").on("change",function(e){
                      var new_tgl_keberangkatan = new Date( $("#tgl_keberangkatan").val() ),
                          new_tgl_kedatangan = new Date( $("#tgl_kedatangan").val() );
                      
                      $("#jml_hari_trip").val( jumlah_hari_trip(new_tgl_kedatangan,new_tgl_keberangkatan) );
                });
        }

        function jumlah_hari_trip(date_tiba, date_berangkat)
        {
            console.log(date_tiba);
            _Diff=Math.ceil((date_tiba.getTime()-date_berangkat.getTime())/(one_day));
            return _Diff; 
        }

        var testDate;
        function set_detail_aktivitas(data)
        {
          var d_tgl_aktivitas = new Date(data.tgl_aktivitas),
              d_tgl_tiba = new Date(),
              tgl_keberangkatan_info = format_date_to_str(d_tgl_aktivitas),
              d_tujuan_berangkat = istilah_tujuan(data.tujuan_berangkat),
              d_nama_alat_tangkap  = '';
          
          testDate = d_tgl_aktivitas;

          list_opsi_alat_tangkap.forEach(function(d){
              if(d.id === data.id_alat_tangkap)
              {
                d_nama_alat_tangkap = d.text;
              }
          });
          
          console.log(data);
          $("#jml_hari_trip").val( jumlah_hari_trip(d_tgl_tiba,d_tgl_aktivitas) );
          $("#id_alat_tangkap").val(data.id_alat_tangkap);
          $("#alat_tangkap").text(d_nama_alat_tangkap);

          $("#tgl_keberangkatan_info").text(tgl_keberangkatan_info);
          $("#tgl_keberangkatan").val(data.tgl_aktivitas);

          $("#tujuan_berangkat_info").text(d_tujuan_berangkat);
          $("#tujuan_berangkat").val(data.tujuan_berangkat);

          $("#pelabuhan_berangkat_info").text(data.nama_pelabuhan_berangkat);
          $("#id_pelabuhan_berangkat").val(data.id_pelabuhan);

          $("#nahkoda").text(data.nama_nahkoda);
          $("#nama_nahkoda").val(data.nama_nahkoda);

          $("#jml_abk").text(data.jumlah_abk);
          $("#jumlah_abk").val(data.jumlah_abk);


          if(data.tujuan_berangkat === 'tangkap')
          {
            $("#id_dpi").select2("val",data.id_dpi);
            var nama_dpi_from_h5 = $("#id_dpi").select2("data")[0].text;
            console.log("ini",nama_dpi_from_h5);
            $("#dpi").text(nama_dpi_from_h5);
          }
         

          $("input[name=id_kapal]").val(data.id_kapal); // ASLINYA PUNYA SELECT2 dari WGT_PILIH_KAPAL


            get_detail_kapal(data.id_kapal, data.filter_gt);

            var split_tgl_dtg =  $("#tgl_keberangkatan").val().split('-'),
                limit_tgl_dtg = new Date( +split_tgl_dtg[0], (+split_tgl_dtg[1] - 1), split_tgl_dtg[2]  );
            //console.log(limit_tgl_dtg);

            $("#tgl_kedatangan_display").datepicker('option', 'minDate', limit_tgl_dtg);
        }

        function notice_maksud_kosong()
        {
          var count_checked = 0;
          $.pnotify_remove_all();
          $(".check-maksud-kunjungan").each(function(i,d){ 
            if( $(this).is(":checked") )
            {
              count_checked++;
            }
          });
              if(count_checked === 0)
              {
                  $.pnotify({
                      title: 'Tidak ada maksud kunjungan.',
                      text: 'Lanjutkan entry?',
                      type: 'notice'
                    });
              }          
        }

        $(document).ready(function(){
            notif_entry_success();
            listener_update_jumlah_hari_trip();
            if(id_aktivitas_keluar !== false){
              get_detail_aktivitas(id_aktivitas_keluar);
              $("#panel-pilih-kapal").hide();
              $(".manual_input").hide();
              $("#ref_aktivitas_kapal").val(id_aktivitas_keluar);


            }else{
              $(".manual_input").show();
                // Hitung jumlah hari trip
                manual_jumlah_hari_trip();
                // Hide ref text tujuan keberangkatan
                $("#tujuan_berangkat_info").hide();
                $(".form-input input,select").prop("disabled", true);
            }

            $("#btn-simpan").on("mouseover", notice_maksud_kosong);

        });


</script>