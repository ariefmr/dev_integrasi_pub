<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class B4 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_b4');
			//TODO: LOGIN CHECK HERE
		}

		public function index($id_pelabuhan='')
		{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/B4'),
									'Entry_active' => base_url('entry/B4'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s1'];


		$data['breadcrumbs'] = 'Industri dan Jasa > Usaha Pengolahan Ikan > Entry';
		
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['b4'];

		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'B4';
		// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();

		$data['id_pelabuhan'] = $id_pelabuhan;

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_gabung = array();
		$index = 1;

				foreach ($array_input as $post => $value) {
					if(preg_match('/supplier/', $post))
            		{
            			$pos = strripos($post, '_')+1;
                		$num = substr($post, $pos);
                		if($array_input[$post] !== ''){
                			$array_gabung[$index] = array(
                											'id_pelabuhan' => $id_pelabuhan,
                											'id_industri' => $array_input['supplier_'.$num],
                											'tgl_catat' => $array_input['tgl_catat'],
                										  	'id_jns_hasil_olah' => $array_input['jenisolahikan_'.$num],
                										  	'jml' => $array_input['produksi_'.$num],
                										  	'harga' => $array_input['harga_'.$num],
                										  	'id_kateg_tujuan' => $array_input['kategori_'.$num],
                										  	'tujuan' => $array_input['tujuan_'.$num],
                										  	'ket' => $array_input['ket_'.$num],
                											'tanggal_buat' => date('Y-m-d H:i:s'),
                											'id_pengguna_buat' => $id_pengguna
                										 );
                			$index++;
                		}
            		}
				}

		$this->mdl_entry_b4->input_batch($array_gabung);

		$url = base_url('jurnal/B4');
		redirect($url);
		echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br><br> isi array_gabung ---><br>";
		//var_dump($array_gabung);
	}

	public function hapus($id)
	{
		$this->mdl_entry_b4->delete($id);

		$url = base_url('jurnal/B4');
		redirect($url);
	}

	public function ubah()
	{
		$array_edit = $this->input->post(NULL, TRUE);
		
		$id_perbekalan = $array_edit['id_perbekalan'];
		$jml = $array_edit['jml'];
		$harga = $array_edit['harga'];
		
		$this->mdl_entry_b4->edit($id_perbekalan, $jml, $harga);

		//var_dump($array_edit);
		$url = base_url('jurnal/B4');
		redirect($url);
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */