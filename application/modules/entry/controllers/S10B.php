<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S10B extends MX_Controller {

	/**
	 * Nama File: S10B.php
	 * Author: ariefmr
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_s10b');

			//$this->load->config('custom_constants');

		}

		public function index($id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/S10B'),
									'Entry_active' => base_url('entry/S10B'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s10'];


		$data['breadcrumbs'] = 'Data Lingkungan Fisik Pelabuhan > Sungai > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s10']['b'];
		
		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'S10B';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		//inputan dari form di masukan ke array edit
		$array_input = $this->input->post(NULL, TRUE);
		//id dari session untuk pelabuhan dan pengguna
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');

		$array_input['id_pelabuhan'] = $id_pelabuhan;
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		$this->mdl_entry_s10b->input($array_input);

		$url = base_url('jurnal/S10B');
		redirect($url);

		//DEBUGGING
		//echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br> isi array_input JSON---><br>";				
		//echo json_encode($array_input);
		

	}

	public function hapus($id)
	{
		$this->mdl_entry_s10b->delete($id);

		$url = base_url('jurnal/S10B');
		redirect($url);
	}

}
/* End of file S10B.php */
/* Location: ./application/modules/home/controllers/home.php */