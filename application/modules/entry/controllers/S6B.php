<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S6B extends MX_Controller {

	/**
	 * Nama File: S6B.php
	 * Author: ariefmr
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_s6b');

			//$this->load->config('custom_constants');

		}

		public function index($id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		// var_dump($id_pelabuhan);
		$this->load->model('jurnal/mdl_view_s6b');
		// var_dump($data_terakhir);
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/S6B'),
									'Entry_active' => base_url('entry/S6B'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s6'];

		$data['breadcrumbs'] = 'Data Umum > Luas Daerah Kerja Perairan > Entry';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s6']['b'];
		
		$data['module'] = 'entry';
		$data['view_file'] = 'S6B';

		$data['list_S6B'] = FALSE;

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		/*$data_terakhir = $this->mdl_view_s6b->get_data_terakhir(7);
		if($data_terakhir){
			$data['data_terakhir'] = (array) $data_terakhir;

		}else{
			$data['data_terakhir'] = FALSE;
			// var_dump($data_terakhir);
		}*/

		if (!$this->mdl_view_s6b->get_data_terakhir(7)){
			$data['data_terakhir'] = FALSE;
		}else{
			$data['data_terakhir'] = (array) $this->mdl_view_s6b->get_data_terakhir(7);
		}

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		//inputan dari form di masukan ke array edit
		$array_input = $this->input->post(NULL, TRUE);
		//id dari session untuk pelabuhan dan pengguna
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');

		$array_input['id_pelabuhan'] = $id_pelabuhan;
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		/*$luas_industri_siap = $array_input['luas_industri_siap'];
		$luas_industri_terpakai = $array_input['luas_industri_terpakai'];
		$luas_industri_belum = $luas_industri_siap - $luas_industri_terpakai;
		
		$array_input['luas_industri_belum'] = $luas_industri_belum;

		$luas_fas_siap = $array_input['luas_fas_siap'];
		$luas_fas_terpakai = $array_input['luas_fas_terpakai'];
		$luas_fas_belum = $luas_fas_siap - $luas_fas_terpakai;
		
		$array_input['luas_fas_belum'] = $luas_fas_belum;*/

		$this->mdl_entry_s6b->input($array_input);

		$url = base_url('jurnal/S6B');
		redirect($url);

		//DEBUGGING
		//echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br> isi array_input JSON---><br>";				
		//echo json_encode($array_input);
		

	}

	public function edit($id_record, $id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
        $this->load->model('jurnal/mdl_view_s6b');
        
        $get_detail = $this->mdl_view_s6b->detail_record($id_record);
        if( !$get_detail )
        {
            $data['list_S6B'] = 'Data tidak ditemukan';
        }else{
            $data['list_S6B'] = (array)$get_detail;
        }
         
        $this->load->view('entry/S6B', $data);
        
    }

	public function update()
    {
        //id dari session untuk pelabuhan dan pengguna
        $array_input = $this->input->post(NULL, TRUE);
        if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');
        
		//inputan dari form di masukan ke array edit
        $array_to_edit = $array_input;
        $array_to_edit['id_pelabuhan'] = $id_pelabuhan;
        $array_to_edit['id_pengguna_ubah'] = $id_pengguna;
        $array_to_edit['tanggal_ubah'] = date('Y-m-d H:i:s');
        
        if( $this->mdl_entry_s6b->edit($array_to_edit) ){
            $url = base_url('jurnal/S6B');
            redirect($url);
        }else{
            $url = base_url('entry/S6B');
            redirect($url);
            // echo $this->db->last_query();
        }
        // echo $this->db->last_query();
        // var_dump($array_to_edit);
    }

    public function hapus($id)
	{
		$this->mdl_entry_s6b->delete($id);

		$url = base_url('jurnal/S6B');
		redirect($url);
	}

}
// TODO BETULIN KETERANGAN
/* End of file S6B.php */
/* Location: ./application/modules/home/controllers/home.php */