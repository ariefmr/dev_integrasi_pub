<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S11 extends MX_Controller {

	/**
	 * Nama File: S11.php
	 * Author: ariefmr
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_s11');

			//$this->load->config('custom_constants');

		}

		public function index($id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/S11'),
									'Entry_active' => base_url('entry/S11'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s11'];

		$data['breadcrumbs'] = 'Kelembagaan UPT Pelabuhan > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s11'];

		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'S11';

		$data['detail_pegawai'] = FALSE;

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		//inputan dari form di masukan ke array edit
		$array_input = $this->input->post(NULL, TRUE);
		//id dari session untuk pelabuhan dan pengguna
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');

		$array_input['id_pelabuhan'] = $id_pelabuhan;
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		//unset inputan dari _display bawaan dari datepicker agar bisa masuk database
		unset(
	        	$array_input['tgl_lahir_display'], 
	        	$array_input['gol_tmt_display'],
	        	$array_input['jabatan_tmt_display'],
	        	$array_input['mulai_kerja_display']
        );

		$this->mdl_entry_s11->input($array_input);

		$url = base_url('jurnal/S11');
		redirect($url);

		//DEBUGGING
		//echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br> isi array_input JSON---><br>";				
		//echo json_encode($array_input);
		

	}

	public function hapus($id)
	{
		$this->mdl_entry_s11->delete($id);

		$url = base_url('jurnal/S11');
		redirect($url);
	}

	public function edit($id_record, $id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
        $this->load->model('jurnal/mdl_view_s11');
        
        $get_detail = $this->mdl_view_s11->detail_record($id_record);
        if( !$get_detail )
        {
            $data['detail_pegawai'] = 'Data tidak ditemukan';
        }else{
            $data['detail_pegawai'] = (array)$get_detail;
        }
         
        $this->load->view('entry/S11', $data);
        
    }

    public function update()
    {
        $array_input = $this->input->post(NULL, TRUE);
        $array_input['tanggal_ubah'] = date('Y-m-d H:i:s');

        //unset inputan dari _display bawaan dari datepicker agar bisa masuk database
        unset(
	        	$array_input['tgl_lahir_display'], 
	        	$array_input['gol_tmt_display'],
	        	$array_input['jabatan_tmt_display'],
	        	$array_input['mulai_kerja_display']
        );
        
        if( $this->mdl_entry_s11->edit($array_input) ){
            $url = base_url('jurnal/S11');
            redirect($url);
            // echo $this->db->last_query();
        }else{
            $url = base_url('entry/S11');
            redirect($url);
            // echo $this->db->last_query();
        }
        // echo $this->db->last_query();
        // var_dump($array_input);
    }

}
/* End of file S11.php */
/* Location: ./application/modules/home/controllers/home.php */