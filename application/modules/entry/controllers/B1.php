<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class B1 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_b1');
			//$this->load->model('mdl_view_b1');
			//TODO: LOGIN CHECK HERE
		}

		public function index($tahun = '', $bulan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		
		$tgl_catat = array( 'bulan' => $bulan ,
                              'tahun' => $tahun
                          );
		if(empty($bulan) && empty($tahun)){
				$tgl_catat = array( 'bulan' => date("m", time() ), 
                              'tahun' => date("Y", time() )
                          );
				$bulan_ini = date('m');
				$tahun_ini = date('Y');
		}else{
			$bulan_ini = $bulan;
			$tahun_ini = $tahun;
			// vdump($tahun_ini, true);
		}

		$data['tmp_tgl_catat'] = $tgl_catat['tahun'].'-'.$tgl_catat['bulan'];

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/B1'),
									'Entry_active' => base_url('entry/B1'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['b1'];

		$data['breadcrumbs'] = 'Industri dan Jasa > Jasa Pelabuhan > Entry';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['b1'];

		$data['module'] = 'entry';
		$data['view_file'] = 'B1';

		$this->load->model('admin/mdl_konfigurasi');

		// $bulan_ini = date('m');
		// $tahun_ini = date('Y');

		$this->load->model('jurnal/mdl_view_b1');
		
		$data_bulan_ini = $this->mdl_view_b1->pendapatan_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini);
		$data_bulan_ini_non = $this->mdl_view_b1->pendapatan_non_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini);

		if($data_bulan_ini){
			$data['jasa_isi'] = TRUE;
			$data['list_B1'] = $data_bulan_ini;
			$data['list_B1_non'] = $data_bulan_ini_non;
		}else{
			$data['jasa_isi'] = FALSE;
			$data['list_B1'] = $this->mdl_view_b1->pendapatan_jasa_field($id_pelabuhan, $bulan_ini, $tahun_ini);
			$data['list_B1_non'] = $this->mdl_view_b1->pendapatan_non_jasa_field($id_pelabuhan, $bulan_ini, $tahun_ini);
		}

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();

   		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_gabung = array();
		$array_gabung2 = array();
		$index = 1;
		$index2 = 1;

				foreach ($array_input as $post => $value) {					
					if(preg_match('/non/', $post)){
						$num = strstr($post, "_non", true);
						if($array_input[$post] !== ''){
							$array_gabung2[$index2] = array(
							'id_pelabuhan' => $id_pelabuhan,
						  	'id_jns_non_jasa' => $num,
						  	'jml' => $value,
							'tanggal_buat' => date('Y-m-d H:i:s'),
							// 'bulan' => date("m", strtotime($array_input['tgl_catat']) ),
							// 'tahun' => date("Y", strtotime($array_input['tgl_catat']) ),
							'tgl_catat' => $array_input['tgl_catat'],
							'desk_lain' => '',
							'id_pengguna_buat' => $id_pengguna
							);
						}
						$index2++;
					}else if(preg_match('/desk_lain/', $post)){
						$coba = array('desk_lain' => $value, );
						//array_push($array_gabung2[4], $coba);
						$array_gabung2[4] = $coba + $array_gabung2[4];
					}else{
						if($array_input[$post] !== ''){
							$array_gabung[$index] = array(
								'id_pelabuhan' => $id_pelabuhan,
							  	'id_jns_jasa' => $post,
							  	'jml' => $value,
								'tanggal_buat' => date('Y-m-d H:i:s'),
								'tgl_catat' => $array_input['tgl_catat'],
								// 'bulan' => date("m", strtotime($array_input['tgl_catat']) ),
								// 'tahun' => date("Y", strtotime($array_input['tgl_catat']) ),
								'id_pengguna_buat' => $id_pengguna
							);
					}
					$index++;
					}
				}

		$this->mdl_entry_b1->input_batch($array_gabung);
		$this->mdl_entry_b1->input_batch2($array_gabung2);

		$url = base_url('jurnal/B1');
		redirect($url);

		// echo "<br> isi array_input ---><br>";				
		// var_dump($array_input);
		// echo "<br><br> isi array_gabung ---><br>";
		// var_dump($array_gabung);
		// echo "<br><br> isi array_gabung2 ---><br>";
		// var_dump($array_gabung2);
		// echo "<br><br> isi json ---><br>";
		// echo json_encode($array_gabung);
		// echo json_encode($array_gabung2);
	}

	public function hapus($id)
	{
		$this->mdl_entry_b1->delete($id);

		$url = base_url('jurnal/B1');
		redirect($url);
	}

	public function update()
	{	
		//id dari session untuk pelabuhan dan pengguna
        $array_input = $this->input->post(NULL, TRUE);
        if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');
        
		//inputan dari form di masukan ke array edit
        $array_to_edit = $array_input;
        $array_to_edit['id_pelabuhan'] = $id_pelabuhan;
        // $array_to_edit['id_pelabuhan'] = $id_pelabuhan;
        // $array_to_edit['id_pengguna_ubah'] = $id_pengguna;
        // $array_to_edit['tanggal_ubah'] = date('Y-m-d H:i:s');

		$array_gabung = array();
		$array_gabung2 = array();
		$index = 1;
		$index2 = 1;

				/*foreach ($array_to_edit as $post => $value) {					
					if(preg_match('/non/', $post)){
						$num = strstr($post, "_non", true);
						if($array_to_edit[$post] !== ''){
							$array_gabung2[$index2] = array(
							// 'id_pelabuhan' => $id_pelabuhan,
							// 'tgl_catat' => date('Y-m-d'),
						  	'id_jns_non_jasa' => $num,
						  	'jml' => $value,
							// 'tanggal_buat' => date('Y-m-d H:i:s'),
							'desk_lain' => '',
						  	'id_pengguna_ubah' => $id_pengguna,
							'tanggal_ubah' => date('Y-m-d H:i:s')
							// 'id_pengguna_buat' => $id_pengguna
							);
						}
						$index2++;
					}else if(preg_match('/desk_lain/', $post)){
						$coba = array('desk_lain' => $value, );
						//array_push($array_gabung2[4], $coba);
						$array_gabung2[4] = $coba + $array_gabung2[4];
					}else{
						if($array_to_edit[$post] !== ''){
							$array_gabung[$index] = array(
								// 'id_pelabuhan' => $id_pelabuhan,
								// 'tgl_catat' => date('Y-m-d'),
							  	//'id_pendapatan_jasa' => $id,
							  	'id_jns_jasa' => $post,
							  	'jml' => $value,
							  	'id_pengguna_ubah' => $id_pengguna,
								'tanggal_ubah' => date('Y-m-d H:i:s')
								// 'id_pengguna_buat' => $id_pengguna
							);
						}
					$index++;
					}
				}*/

				foreach ($array_to_edit as $post => $value) {					
					if(preg_match('/non/', $post)){
						$num = strstr($post, "_non", true);
						if($array_to_edit[$post] !== ''){
							$array_gabung2[$index2] = array(
						  	'id_pendapatan_non_jasa' => $num,
						  	'jml' => $value,
							'desk_lain' => '',
						  	'id_pengguna_ubah' => $id_pengguna,
							'tanggal_ubah' => date('Y-m-d H:i:s')
							);
						}
						$index2++;
					}else if(preg_match('/desk_lain/', $post)){
						$coba = array('desk_lain' => $value, );
						//array_push($array_gabung2[4], $coba);
						$array_gabung2[4] = $coba + $array_gabung2[4];
					}else{
						if($array_to_edit[$post] !== ''){
							$array_gabung[$index] = array(
							  	'id_pendapatan_jasa' => $post,
							  	'jml' => $value,
							  	'id_pengguna_ubah' => $id_pengguna,
								'tanggal_ubah' => date('Y-m-d H:i:s')
							);
						}
					$index++;
					}
				}

		$this->mdl_entry_b1->edit($array_gabung);
		$this->mdl_entry_b1->edit2($array_gabung2);

        if( $this->mdl_entry_b1->edit($array_gabung) ){
			$url = base_url('jurnal/B1');
			redirect($url);
		}else{
			$url = base_url('entry/B1');
			redirect($url);
		}
		
		// echo $this->db->last_query();
		// var_dump($array_to_edit);

		// echo "<br> isi array_to_edit ---><br>";				
		// var_dump($array_to_edit);
		// echo "<br><br> isi array_gabung ---><br>";
		// var_dump($array_gabung);
		// echo "<br><br> isi array_gabung2 ---><br>";
		// var_dump($array_gabung2);
		// echo "<br><br> isi json ---><br>";
		// echo json_encode($array_gabung);
		// echo json_encode($array_gabung2);

	}

	/*public function ubah()
	{
		$array_edit = $this->input->post(NULL, TRUE);
		
		$id_perbekalan = $array_edit['id_perbekalan'];
		$jml = $array_edit['jml'];
		$harga = $array_edit['harga'];
		
		$this->mdl_entry_b1->edit($id_perbekalan, $jml, $harga);

		//var_dump($array_edit);
		$url = base_url('jurnal/B1');
		redirect($url);
	}*/

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */