<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H2 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_h2');
			//TODO: LOGIN CHECK HERE
		}

	public function index($id_aktivitas_masuk = '', $id_pelabuhan='')
	{
		$this->ref($id_aktivitas_masuk, $id_pelabuhan);
	}

	public function ref($id_aktivitas_masuk = '', $id_pelabuhan='')
	{
		// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		
		$data['id_aktivitas_masuk'] = empty($id_aktivitas_masuk) ? 'false' : $id_aktivitas_masuk;;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/H2'),
									'Entry_active' => base_url('entry/H2'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h2'];


		$data['breadcrumbs'] = 'Produksi > Produksi Dan Harga > Entry';
		
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h2'];

		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'H2';

		$data['entry_status'] = 'NULL';
		$data['nama_kapal'] = 'NULL';
		if(isset($_GET['entry_status']) && isset($_GET['nama_kapal'])) {
		    $data['entry_status'] = $_GET['entry_status'];
		    $data['nama_kapal'] = $_GET['nama_kapal'];
		}


		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);

	}

	public function input()
	{
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_gabung = array();
		$index = 1;
		$id_aktivitas_masuk = $array_input['ref_aktivitas_kapal'] === '00' ? '' : $array_input['ref_aktivitas_kapal'];
    $id_dpi = isset($array_input['id_dpi']) ?  $array_input['id_dpi'] : '';

				foreach ($array_input as $name => $value) {
					if(preg_match('/ikan/', $name))
            		{
            			$pos = strripos($name, '_')+1;
                		$num = substr($name, $pos);
                		if($array_input[$name] !== ''){
                			$array_gabung[$index] = array(
                										  'id_pelabuhan' => $id_pelabuhan,
                										  'id_kapal' => $array_input['id_kapal'],
                                      'nama_kapal' =>  $array_input['nama_kapal_info'],
                										  'tgl_catat' => $array_input['tgl_catat'],
                                      'tgl_aktivitas' => $array_input['tgl_aktivitas'],                                      
                										  'id_jenis_ikan' => $array_input[$name],
                										  'id_jenis_kondisi_ikan' => $array_input['kondisi_'.$num],
                										  'jml_ikan' => $array_input['jml_'.$num],
                										  'harga_pedagang' => $array_input['hrg_pedagang_'.$num],
                										  'harga_produsen' => $array_input['hrg_produsen_'.$num],
                                      'koefisien_koreksi' => $array_input['koef_koreksi_'.$num],
                										  'id_alat_tangkap' => $array_input['id_alat_tangkap'],
                										  'id_dpi' => $id_dpi,
                										  'tanggal_buat' => date('Y-m-d H:i:s'),
                										  'id_pengguna_buat' => $id_pengguna,
                										  'id_aktivitas_referensi' => $id_aktivitas_masuk,
                										  'filter_gt' => $array_input['input_filter_gt'],
                										  'aktif' => 'Ya'
                										);
                			$index++;
                		}
            		}
				}

		$this->mdl_entry_h2->input_batch($array_gabung);

		$module_redirect = 'entry/H2';
		if(!empty($id_aktivitas_masuk))
		{
			$this->load->model('mdl_entry_h5');
			$this->mdl_entry_h5->update_chain($id_aktivitas_masuk, 'H2');
			$module_redirect = 'jurnal/H1';
		}
		$entry_status = '?entry_status=success&nama_kapal='.$array_input['nama_kapal_info'];

		$url = base_url($module_redirect.'/'.$entry_status);
		redirect($url);
		/*	DEBUGGING		
		echo "<br> isi array_input ---><br>";				
		var_dump($array_input);
		echo "<br><br> isi array_gabung ---><br>";
		foreach ($array_gabung as $key => $value) {
			echo "<br><br><br>";
			echo $key;
			echo "<br><br>";
			foreach ($value as $field => $value) {
				echo 'Field: '.$field.'<br>';
				echo 'Value: '.$value.'<br><br>';
			}
		}
		*/
	}

	public function hapus()
	{
		$list_id_to_delete = $this->input->get(NULL, TRUE);
		foreach ($list_id_to_delete as $key => $id_produksi) {
			$this->mdl_entry_h2->delete($id_produksi);
		}

		//$url = base_url('jurnal/H2');
		//redirect($url);
	}

	public function ubah()
	{
				$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_to_edit = array();
		
		foreach ($array_input as $name => $value) {
					if(preg_match('/idProduksi/', $name))
            		{
            			$pos = strripos($name,'_')+1;
                		$num = substr($name, $pos);
                		$array_to_edit[''.$num] = array(
                									'id_jenis_kondisi_ikan' => $array_input['kondisi_'.$num],
                									'jml_ikan' => $array_input['jml_'.$num],
                									'harga_produsen' => $array_input['hrgProdusen_'.$num],
                									'harga_pedagang' => $array_input['hrgPedagang_'.$num],
                									'tanggal_buat' => date('Y-m-d H:i:s'),
                									'id_pengguna_buat' => $id_pengguna
                								);
                	}
		}
		
		$this->mdl_entry_h2->edit($array_to_edit);

		//var_dump($array_edit);
		$url = base_url('jurnal/H2');
		redirect($url);
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */