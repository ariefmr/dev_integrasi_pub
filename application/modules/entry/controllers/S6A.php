<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S6A extends MX_Controller {

	/**
	 * Nama File: S6A.php
	 * Author: ariefmr
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_s6a');
			$this->load->model('mst_pelabuhan/mdl_pelabuhan');

			//$this->load->config('custom_constants');

		}

		public function index($id_pelabuhan='')
		{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		
		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/S6A'),
									'Entry_active' => base_url('entry/S6A'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s6'];


		$data['breadcrumbs'] = 'Pemasaran > Data Umum Pelabuhan > Entry';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s6']['a'];
		
		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'S6A';

    	$data['list_S6A'] = FALSE;

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		//inputan dari form di masukan ke array edit
		$array_input = $this->input->post(NULL, TRUE);
		//id dari session untuk pelabuhan dan pengguna
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');

		$array_input['id_pelabuhan'] = $id_pelabuhan;
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		$this->load->model('mst_pelabuhan/mdl_pelabuhan');
		if( $_FILES['gambar']['error'] !== UPLOAD_ERR_NO_FILE)
		{
				/* Awal Program Upload */

				// Ambil nama pelabuhan untuk bikin nama file gambar yang diupload
				$nama_pelabuhan = $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);
				$nama_pelabuhan = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_pelabuhan);
				$nama_pelabuhan = str_replace(" ", "_", $nama_pelabuhan);

				

				// Timestamp supaya nama file unique
				$getdate = new DateTime();
				$time_upload = $getdate->getTimestamp();

				// Persiapkan nama File, ini ga ada hubungan sama nama file asli
				// Sengaja hardcode 'balai_' karena memang gambar yang diupload tentang balai
				$nama_file = 'profil_'.$nama_pelabuhan.'_'.$time_upload.'.jpg';
				//var_dump($nama_file);

				// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				

				// Ambil path ke folder uploads yang diset di custom constants
				$assets_paths = $this->config->item('assets_paths');

				// Rules upload nya di set di custom constants
				$rules = $this->config->item('upload_rules');


				// Persiapkan config untuk upload
				$config['upload_path'] = $rules['upload_path'];  // Set path tempat file disimpan di server
				$config['file_name'] = $nama_file; //  Set nama file yang bakal disimpan

				// Set rules ini dari custom constants
				$config['allowed_types'] = $rules['allowed_types']; 
				$config['max_size']	= $rules['max_size']; 
				$config['max_width']  = $rules['max_width'];
				$config['max_height']  = $rules['max_height'];

				// Set config 
				$this->load->library('upload', $config);

				/* PROSES UPLOAD SEBENERNYA DIJALANIN DI FUNGSI $this->upload->do_upload() :
				*  parameter nya di isi name dari input file di form entry sebelumnya
				*  Misal di form entry nya <input type=file name=gambar...
				*  Di sini masukin parameter nya: 'gambar'
				*/
				if ( ! $this->upload->do_upload('gambar') ) // Kondisi kalau upload gagal;
				{
					$info_error_upload = array('error' => $this->upload->display_errors());
					var_dump($info_error_upload);
					$url = base_url('jurnal/S6A');
					redirect($url);
					exit; // Gagal Upload Langsung Exit, record ga masuk 
					// PR: Bikin notifikasi gagal upload
				} 
				else // Kondisi upload berhasil
				{
					//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

					$array_input['gambar'] = $nama_file;
				}
		}else{ // TANPA UPLOAD KOSONGKAN FIELD GAMBAR
			$array_input['gambar'] = '';
		}

		$this->mdl_entry_s6a->input($array_input);

		$url = base_url('jurnal/S6A');
		redirect($url);

		//DEBUGGING
		//echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br> isi array_input JSON---><br>";				
		//echo json_encode($array_input);
		

	}

	public function hapus($id)
	{
		$this->mdl_entry_S6A->delete($id);

		$url = base_url('jurnal/S6A');
		redirect($url);
	}

	public function edit($id_record, $id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
        $this->load->model('jurnal/mdl_view_s6a');
        
        $get_detail = $this->mdl_view_s6a->detail_record($id_record);
        if( !$get_detail )
        {
            $data['list_S6A'] = 'Data tidak ditemukan';
        }else{
            $data['list_S6A'] = (array)$get_detail;
        }
         
        $this->load->view('entry/S6A', $data);
        
    }

    public function update()
    {
    	$array_input = $this->input->post(NULL, TRUE);
    	// $id_pelabuhan = $this->input->post('id_pelabuhan', TRUE);
    	if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
				$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
			}else{
				$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
			}
			
		$id_pengguna = $this->session->userdata('id_pengguna');
		

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
        
		//inputan dari form di masukan ke array edit
       
        $array_to_edit = $array_input;
        $array_to_edit['id_pelabuhan'] = $id_pelabuhan;
        $array_to_edit['id_pengguna_ubah'] = $id_pengguna;
        $array_to_edit['tanggal_ubah'] = date('Y-m-d H:i:s');
        
        $this->load->model('mst_pelabuhan/mdl_pelabuhan');
		if( $_FILES['gambar']['error'] !== UPLOAD_ERR_NO_FILE)
		{
				/* Awal Program Upload */

				// Ambil nama pelabuhan untuk bikin nama file gambar yang diupload
				$nama_pelabuhan = $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);
				$nama_pelabuhan = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_pelabuhan);
				$nama_pelabuhan = str_replace(" ", "_", $nama_pelabuhan);

				

				// Timestamp supaya nama file unique
				$getdate = new DateTime();
				$time_upload = $getdate->getTimestamp();

				// Persiapkan nama File, ini ga ada hubungan sama nama file asli
				// Sengaja hardcode 'balai_' karena memang gambar yang diupload tentang balai
				$nama_file = 'profil_'.$nama_pelabuhan.'_'.$time_upload.'.jpg';
				//var_dump($nama_file);

				// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				

				// Ambil path ke folder uploads yang diset di custom constants
				$assets_paths = $this->config->item('assets_paths');

				// Rules upload nya di set di custom constants
				$rules = $this->config->item('upload_rules');


				// Persiapkan config untuk upload
				$config['upload_path'] = $rules['upload_path'];  // Set path tempat file disimpan di server
				$config['file_name'] = $nama_file; //  Set nama file yang bakal disimpan

				// Set rules ini dari custom constants
				$config['allowed_types'] = $rules['allowed_types']; 
				$config['max_size']	= $rules['max_size']; 
				$config['max_width']  = $rules['max_width'];
				$config['max_height']  = $rules['max_height'];

				// Set config 
				$this->load->library('upload', $config);

				/* PROSES UPLOAD SEBENERNYA DIJALANIN DI FUNGSI $this->upload->do_upload() :
				*  parameter nya di isi name dari input file di form entry sebelumnya
				*  Misal di form entry nya <input type=file name=gambar...
				*  Di sini masukin parameter nya: 'gambar'
				*/
				if ( ! $this->upload->do_upload('gambar') ) // Kondisi kalau upload gagal;
				{
					$info_error_upload = array('error' => $this->upload->display_errors());
					var_dump($info_error_upload);
					//$this->index(); 
					//exit; // Gagal Upload Langsung Exit, record ga masuk 
					// PR: Bikin notifikasi gagal upload
				} 
				else // Kondisi upload berhasil
				{
					//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

					$array_to_edit['gambar'] = $nama_file;
				}
		}
        
        if( $this->mdl_entry_s6a->edit($array_to_edit) ){
            $url = base_url('jurnal/S6A/views/'.$id_pelabuhan);
            redirect($url);
        }else{
            $url = base_url('entry/S6A');
            redirect($url);
            // echo $this->db->last_query();
        }
        // echo $this->db->last_query();
        // var_dump($array_to_edit);
    }

}
// TODO BETULIN KETERANGAN
/* End of file S6A.php */
/* Location: ./application/modules/home/controllers/home.php */