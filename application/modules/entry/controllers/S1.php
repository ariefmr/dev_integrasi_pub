<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S1 extends MX_Controller {

	/**
	 * Nama File: S1.php
	 * Author: ariefmr
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_s1');

			//$this->load->config('custom_constants');

		}

		public function index($id_pelabuhan='')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$data['id_pelabuhan_selected'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/S1'),
									'Entry_active' => base_url('entry/S1'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s1'];

		$data['breadcrumbs'] = 'Industri dan Jasa > Industri di Pelabuhan > Entry';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s1'];
		
		$data['module'] = 'entry';
		$data['view_file'] = 'S1';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		//inputan dari form di masukan ke array edit
		$array_input = $this->input->post(NULL, TRUE);
		//id dari session untuk pelabuhan dan pengguna
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');

		$array_input['id_pelabuhan'] = $id_pelabuhan;
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		$this->mdl_entry_s1->input($array_input);

		$url = base_url('jurnal/S1');
		redirect($url);

		//DEBUGGING
		//echo "<br> isi array_input ---><br>";				
		//var_dump($array_input);
		//echo "<br> isi array_input JSON---><br>";				
		//echo json_encode($array_input);
		

	}

	public function hapus($id)
	{
		$this->mdl_entry_s1->delete($id);

		$url = base_url('jurnal/S1');
		redirect($url);
	}

	public function ubah()
	{
		$array_edit = $this->input->post(NULL, TRUE);
		
		$id_pemasaran_masuk = $array_edit['id_pemasaran_masuk'];
		$id_jenis_kondisi_ikan = $array_edit['id_jenis_kondisi_ikan'];
		$jml_ikan = $array_edit['jml_ikan'];
		$harga_ikan = $array_edit['harga_ikan'];
		
		$this->mdl_entry_s1->edit($id_pemasaran_masuk, $id_jenis_kondisi_ikan, $jml_ikan, $harga_ikan);

		//var_dump($array_edit);
		$url = base_url('jurnal/S1');
		redirect($url);
	}

}
/* End of file S1.php */
/* Location: ./application/modules/home/controllers/home.php */