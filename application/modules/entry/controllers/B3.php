<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class B3 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_b3');

			//$this->load->config('custom_constants');
			$this->opsi_kondisi_ikan = $this->config->item('kondisi_ikan');
			$this->opsi_jenis_transportasi = $this->config->item('transportasi');
		}

		public function index($id_pelabuhan='')
		{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}


		$data['id_pelabuhan_selected'] = $id_pelabuhan;

		$data['id_pelabuhan'] = $id_pelabuhan;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/B3'),
									'Entry_active' => base_url('entry/B3'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['b3'];

		$data['breadcrumbs'] = 'Pemasaran > Keluar Pelabuhan > Entry';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['b3'];
		
		$data['module'] = 'entry';
		$data['view_file'] = 'B3';

		$data['opsi_kondisi_ikan'] = $this->opsi_kondisi_ikan;
		$data['opsi_jenis_transportasi'] = $this->opsi_jenis_transportasi;

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_gabung = array();
		$index = 1;

				foreach ($array_input as $post => $value) {
					if(preg_match('/ikan/', $post))
            		{
            			$pos = strripos($post, '_')+1;
                		$num = substr($post, $pos);
                		if($array_input[$post] !== ''){
                			$array_gabung[$index] = array(
                										  'id_pelabuhan' => $id_pelabuhan,
	            										  'tgl_catat' => $array_input['tgl_aktivitas'],
	            										  'id_jenis_ikan' => $array_input[$post],
	            										  'id_jenis_kondisi_ikan' => $array_input['kondisi_'.$num],
	            										  'jml_ikan' => $array_input['jml_'.$num],
	            										  'harga_ikan' => $array_input['harga_'.$num],
	            										  'id_kateg_tujuan' => $array_input['kategori_'.$num],
	            										  'kota_tujuan' => $array_input['tujuan_'.$num],
	            										  'jenis_transportasi' => $array_input['transportasi_'.$num],
	            										  'tanggal_buat' => date('Y-m-d H:i:s'),
                										  'id_pengguna_buat' => $id_pengguna
                										 );
                			$index++;
                		}
            		}
				}

		$this->mdl_entry_b3->input_batch($array_gabung);

		$url = base_url('jurnal/B3');
		redirect($url);

		/* DEBUGGING
		echo "<br> isi array_input ---><br>";				
		var_dump($array_input);
		echo "<br><br> isi array_gabung ---><br>";
		var_dump($array_gabung);
		*/
	}

	public function hapus($id)
	{
		$this->mdl_entry_b3->delete($id);

		$url = base_url('jurnal/B3');
		redirect($url);
	}

	public function ubah()
	{
		$array_edit = $this->input->post(NULL, TRUE);
		$tgl_catat = $array_edit['tgl_current'];
		unset($array_edit['tgl_current']);

		
		$this->mdl_entry_b3->edit($array_edit);

		$split_tgl = explode('-', $tgl_catat);
		$tahun = $split_tgl[0];
		$bulan = $split_tgl[1];
		//var_dump($array_edit);
		$url = base_url('jurnal/B3/index/'.$tahun.'/'.$bulan);
		redirect($url);
	}
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */