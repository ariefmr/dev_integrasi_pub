<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H1 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_h1');
			//TODO: LOGIN CHECK HERE
		}

	public function index($id_aktivitas_keluar = '', $id_pelabuhan='')
	{
			$this->ref($id_aktivitas_keluar, $id_pelabuhan);
	}

	public function ref($id_aktivitas_keluar = '', $id_pelabuhan='')
	{
		$id_aktivitas_keluar = empty($id_aktivitas_keluar) ? 'false' : $id_aktivitas_keluar;
		// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		
		$data['id_aktivitas_keluar'] = $id_aktivitas_keluar;
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/H1'),
									'Entry_active' => base_url('entry/H1'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h1'];

		$data['breadcrumbs'] = 'Produksi > Kapal Masuk > Entry';
		
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h1'];
		
		//dulunya data['id_pelabuhan']
		$data['module'] = 'entry';
		$data['view_file'] = 'H1';
		
		$data['entry_status'] = 'NULL';
		$data['nama_kapal'] = 'NULL';
		if(isset($_GET['entry_status']) && isset($_GET['nama_kapal'])) {
		    $data['entry_status'] = $_GET['entry_status'];
		    $data['nama_kapal'] = $_GET['nama_kapal'];
		}


		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_aktivitas = array();
		$array_maksud_kunjungan = array();
		$tanggal_buat = date('Y-m-d H:i:s');
		
		$id_aktivitas_keluar = $array_input['ref_aktivitas_kapal'] === '00' ? '' : $array_input['ref_aktivitas_kapal'];


		$array_aktivitas['id_pelabuhan'] = $id_pelabuhan;
		$array_aktivitas['id_pelabuhan_masuk'] = $id_pelabuhan;
		$array_aktivitas['aktivitas'] = 'masuk';
		$array_aktivitas['id_pengguna_buat'] = $id_pengguna;
		$array_aktivitas['id_kapal'] = $array_input['id_kapal'];
		$array_aktivitas['nama_kapal'] = $array_input['nama_kapal_info'];
		$array_aktivitas['tanggal_buat'] = $tanggal_buat;		
		$array_aktivitas['tgl_catat'] = $array_input['tgl_catat'];
		if($array_input['tujuan_berangkat'] === 'tangkap'){ $array_aktivitas['id_dpi'] = $array_input['id_dpi']; }
		$array_aktivitas['tgl_aktivitas'] = $array_input['tgl_kedatangan']; 
		$array_aktivitas['id_alat_tangkap'] = $array_input['id_alat_tangkap'];
		$array_aktivitas['nama_nahkoda'] = $array_input['nama_nahkoda'];
		$array_aktivitas['jumlah_abk'] = $array_input['jumlah_abk'];
		$array_aktivitas['id_pelabuhan_asal'] = $array_input['id_pelabuhan_berangkat'];
		$array_aktivitas['jml_hari_trip'] = $array_input['jml_hari_trip'];	
		$array_aktivitas['filter_gt'] = $array_input['input_filter_gt'];

		$id_aktivitas_current = $this->mdl_entry_h1->input_aktivitas($array_aktivitas);
		
		$module_redirect = 'entry/H1';
		if(!empty($id_aktivitas_keluar))
		{
			$this->load->model('mdl_entry_h5');
			$this->mdl_entry_h5->update_chain($id_aktivitas_keluar, $id_aktivitas_current);
			$module_redirect = 'jurnal/H5';
		}

		$array_maksud_kunjungan['id_aktivitas_kapal'] = $id_aktivitas_current;
		$array_maksud_kunjungan['id_pelabuhan'] = $id_pelabuhan;
		$array_maksud_kunjungan['id_kapal'] = $array_input['id_kapal'];
		$array_maksud_kunjungan['tgl_aktivitas'] = $array_input['tgl_kedatangan'];
		$array_maksud_kunjungan['tanggal_buat'] = $tanggal_buat;
		$array_maksud_kunjungan['id_pengguna_buat'] = $id_pengguna;
			foreach ($array_input as $post => $value) {
				if(preg_match('/is_/', $post))
            		{
            			$array_maksud_kunjungan[$post] = $value;
            		}
			}

		 $this->mdl_entry_h1->input_kunjungan($array_maksud_kunjungan);
		
		$entry_status = '?entry_status=success&nama_kapal='.$array_input['nama_kapal_info'];
		$url = base_url($module_redirect.'/'.$entry_status);
		redirect($url);
	}

	public function hapus($id)
	{
		$this->mdl_entry_h1->delete($id);

		$url = base_url('jurnal/H1');
		redirect($url);
	}
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */