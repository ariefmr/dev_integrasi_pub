<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H5 extends MX_Controller {

	/**
	 * Nama File: H5.php
	 * Author: ariefmr
	 * Deskripsi: Controller H5
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_entry_h5');
			//TODO: LOGIN CHECK HERE
		}

		public function index($id_pelabuhan='')
		{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
				// $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		$data['id_pelabuhan'] = $id_pelabuhan;
		
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');
		
		$data['link_daftar'] = Array(
									'View' => base_url('jurnal/H5'),
									'Entry_active' => base_url('entry/H5'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h5'];

		
		$data['breadcrumbs'] = 'Produksi > Kapal Keluar > Entry';
		
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h5'];

		$data['module'] = 'entry';
		$data['view_file'] = 'H5';
		
		$data['entry_status'] = 'NULL';
		$data['nama_kapal'] = 'NULL';
		if(isset($_GET['entry_status']) && isset($_GET['nama_kapal'])) {
		    $data['entry_status'] = $_GET['entry_status'];
		    $data['nama_kapal'] = $_GET['nama_kapal'];
		}

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function input()
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		

		
		$array_aktivitas = array();
		$array_perbekalan = array();
		$array_alat_bantu_keluar = array();
		$index_perbekalan = 1;
		$index_aktivitas = 1;
		$index_alat_bantu = 1;
		$tanggal_buat = date('Y-m-d H:i:s');

		$array_id_alat_bantu = !empty($array_input["id_alat_bantu"]) ? explode(",", $array_input["id_alat_bantu"]) : 0; 
		
		$array_aktivitas['id_pelabuhan'] = $id_pelabuhan;
		$array_aktivitas['id_pelabuhan_asal'] = $id_pelabuhan;

		$array_aktivitas['aktivitas'] = 'keluar';
		$array_aktivitas['id_pengguna_buat'] = $id_pengguna;
		$array_aktivitas['id_kapal'] = $array_input['id_kapal'];
		$array_aktivitas['nama_kapal'] = $array_input['nama_kapal_info'];
		$array_aktivitas['tanggal_buat'] = $tanggal_buat;		
		$array_aktivitas['tgl_catat'] = $array_input['tgl_catat'];
		$array_aktivitas['tgl_aktivitas'] = $array_input['tgl_aktivitas'];
		$array_aktivitas['id_alat_tangkap'] = $array_input['id_alat_tangkap'];
		$array_aktivitas['nama_nahkoda'] = $array_input['nama_nahkoda'];
		$array_aktivitas['jumlah_abk'] = $array_input['jumlah_abk'];
		$array_aktivitas['filter_gt'] = $array_input['input_filter_gt'];

		if( !in_array($id_pelabuhan, $array_input['id_pelabuhan_tujuan']) )
		{
			array_push($array_input['id_pelabuhan_tujuan'], $id_pelabuhan);			
		}
		$str_pelabuhan_tujuan = implode(",",$array_input['id_pelabuhan_tujuan']);

		$array_aktivitas['tujuan_berangkat'] = $array_input['tujuan_berangkat'];
			if($array_aktivitas['tujuan_berangkat'] === 'tangkap'){
				//$array_aktivitas['id_wpp'] = $array_input['id_wpp'];
				$array_aktivitas['id_dpi'] = $array_input['id_dpi'];
				$array_aktivitas['id_pelabuhan_tujuan'] = $str_pelabuhan_tujuan;
			}elseif($array_aktivitas['tujuan_berangkat'] === 'angkut'){
				$array_aktivitas['id_pelabuhan_tujuan'] = $str_pelabuhan_tujuan;
			}elseif ($array_aktivitas['tujuan_berangkat'] === 'docking') {
				$array_aktivitas['id_pelabuhan_docking'] = $str_pelabuhan_tujuan;
			}
	
		$id_aktivitas_current = $this->mdl_entry_h5->input_aktivitas($array_aktivitas);
		
		foreach ($array_input as $name => $value) {
					if(preg_match('/perbekalan/', $name))
		            {
		            	if($value !== '' && $value !== '0'){
		            		$array_name = explode("-",$name);
		            		$id_jenis_perbekalan = $array_name[1];
		            		$nama_jenis_perbekalan = str_replace("_", " ", $array_name[2]);
							$array_perbekalan[$index_perbekalan] = array(
																'id_pelabuhan' => $id_pelabuhan,
																'id_aktivitas_kapal' => $id_aktivitas_current,
																'tgl_aktivitas' => $array_aktivitas['tgl_aktivitas'],
																'id_kapal' => $array_aktivitas['id_kapal'],
																'id_jns_perbekalan' => $id_jenis_perbekalan,
																'nama_jns_perbekalan' => $nama_jenis_perbekalan,
																'jml_perbekalan' => $value,
																'tanggal_buat' => $tanggal_buat,
                										  		'id_pengguna_buat' => $id_pengguna
															  );
							$index_perbekalan++;
		            	}
					}
		}

		
		if( count($array_id_alat_bantu) > 0)
		{
			foreach ($array_id_alat_bantu as $index => $id_alat_bantu) {
				$array_alat_bantu_keluar[$index_alat_bantu] = array(
																	'id_aktivitas_kapal' => $id_aktivitas_current,
																	'id_kapal' => $array_aktivitas['id_kapal'],
																	'id_pelabuhan' => $id_pelabuhan,
																	'id_alat_bantu' => $id_alat_bantu,
																	'tanggal_buat' => $tanggal_buat,
	                										  		'id_pengguna_buat' => $id_pengguna
																  );
				$index_alat_bantu++;
			}	
		}

		if( count($array_perbekalan) > 0 )
		{
			$this->mdl_entry_h5->input_perbekalan($array_perbekalan);	
		}
		if( count($array_alat_bantu_keluar > 0) )
		{
			$this->mdl_entry_h5->input_alat_bantu_keluar($array_alat_bantu_keluar);			
		}


		$entry_status = '?entry_status=success&nama_kapal='.$array_input['nama_kapal_info'];
		$url = base_url('entry/H5'.$entry_status);
		redirect($url);
		//var_dump($array_perbekalan);
		//echo "<br><br>";
		//var_dump($array_aktivitas);
		
	}

	public function hapus($id)
	{
		$this->mdl_entry_h5->delete($id);

		$url = base_url('jurnal/H5');
		redirect($url);
	}

}
// TODO BETULIN KETERANGAN
/* End of file H5.php */
/* Location: ./application/modules/entry/controllers/ */