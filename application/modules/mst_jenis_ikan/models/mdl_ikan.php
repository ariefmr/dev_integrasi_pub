<?php
/*
 * class Mdl_ikan
 */

class Mdl_ikan extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss = NULL;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }
    
    public function list_ikan()
    {
        $query = $this->db_dss->get('mst_jenis_ikan');
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_ikan()
    {
        $query = 'SELECT id_jenis_ikan AS id, CONCAT(nama_jenis_ikan," (",nama_latin,")") AS text FROM mst_jenis_ikan';
        
        $run_query = $this->db_dss->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>