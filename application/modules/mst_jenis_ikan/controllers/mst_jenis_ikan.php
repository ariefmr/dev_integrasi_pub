<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_jenis_ikan extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: 
	 * Deskripsi: 
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_ikan');
		}

	public function index()
	{

		$data['list_ikan'] = $this->mdl_ikan->list_ikan();
		$this->load->view('daftar_ikan', $data);
	}

	public function daftar_ikan() 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis Ikan';
		$data['content_title'] = 'Daftar Jenis Ikan';
		$data['module'] = 'mst_jenis_ikan';
		$data['view_file'] = 'daftar_ikan';
		$data['list_ikan'] = $this->mdl_ikan->list_ikan();

		$data['path_gambar_ikan'] = $this->assets_paths['folder_ikan_dss'];

		echo Modules::run('templates/type/default_template', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar ikan
	public function select_ikan()
	{
		$data['list_ikan'] = $this->mdl_ikan->list_ikan();
		$this->load->view('select_ikan', $data);
	}

	public function array_ikan()
	{
		$array_ikan = $this->mdl_ikan->list_opsi_ikan();
		return $array_ikan;
	}

	public function json_ikan()
	{
		$array_of_ikan = (Array) $this->mdl_ikan->list_opsi_ikan();

		echo json_encode($array_of_ikan);
	}

	public function table_ikan()
	{
		$data['path_gambar_ikan'] = $this->assets_paths['pipp_images'];
		$data['list_ikan'] = $this->mdl_ikan->list_ikan();	
		$this->load->view('daftar_ikan', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}
}

/* End of file jenis_ikan.php */
/* Location: ./application/modules/jenis_ikan/controllers/welcome.php */