<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_ikan' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'Nama Ikan','Gambar', 'Nama Internasional', 'Nama Latin', 'Nama Daerah', 'Aktif');
	$counter = 1;
	$default_gambar_ikan = '<img src="'.$path_gambar_ikan.'/" class="img-thumbnail" alt="140x140" style="width: 140px; height: 140px;">';

	if($list_ikan !== FALSE){
		foreach ($list_ikan as $item) {
			$this->table->add_row($counter.'.', $item->nama_jenis_ikan, '<img src="'.$path_gambar_ikan.'/'.$item->foto_ikan.'" class="img-thumbnail">', 
									$item->nama_jenis_ikan_inggris, $item->nama_latin, $item->nama_daerah, $item->aktif);
			$counter++;
		}
	}
	

	$table_list_ikan = $this->table->generate();
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading">Daftar Jenis Ikan Master (DSS)</div>
			<div class="panel-body overflowed">
			<!-- TAMPIL DATA -->
		<?php
			echo $table_list_ikan;
		?>
			</div>
		</div>
	</div>
</div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_ikan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>