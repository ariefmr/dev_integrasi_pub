<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Type extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: 
	 * Deskripsi: 
	 * 
	 */
	private $dev_mode = FALSE;

	
	function __construct()
	{
		parent::__construct();

		//$this->load->config('custom_constants');
		$this->load->model('admin/mdl_konfigurasi');
		$this->load->model('mst_pelabuhan/mdl_pelabuhan');

		$this->dev_mode = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
	}
	

	public function default_template($data)
	{
		$id_pengguna = $this->session->userdata('id_pengguna');
		$nama_pengguna = $this->session->userdata('fullname');
		$username_pengguna = $this->session->userdata('username');
		$id_pelabuhan_pengguna = $this->pipp_lib->id_pelabuhan_pengguna();

		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
        $id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }
    
		if($this->pipp_lib->id_grup_pengguna() === "6" || $this->pipp_lib->id_grup_pengguna() === "9")
		{
			$nama_pelabuhan = $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_pengguna, array('nama_pelabuhan'))->nama_pelabuhan;			
		}else{
			$nama_pelabuhan = 'Pusat';
		}

		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		// var_dump($id_pelabuhan_pengguna);

		$data['nama_pengguna'] = $nama_pengguna;
		$data['username_pengguna'] = $username_pengguna;
		$data['nama_pelabuhan'] = $nama_pelabuhan;
		$data['id_pelabuhan_pengguna'] = $id_pelabuhan_pengguna;
		$data['id_pengguna'] = $id_pengguna;
		$data['paths'] = $this->config->item('assets_paths');
		$data['basic'] = $this->config->item('basic');
		$data['menus'] = $this->config->item('menus');
		$data['dev_mode'] = $this->dev_mode;

		$this->load->view('default', $data);
	}

	public function forms($data)
	{
		$id_pengguna = $this->session->userdata('id_pengguna');
		$nama_pengguna = $this->session->userdata('fullname');
		$username_pengguna = $this->session->userdata('username');
		$id_pelabuhan_pengguna = $this->pipp_lib->id_pelabuhan_pengguna();
		
		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
        $id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }		
		if($this->pipp_lib->id_grup_pengguna() === "6" || $this->pipp_lib->id_grup_pengguna() === "9")
		{
			$nama_pelabuhan = $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_pengguna, array('nama_pelabuhan'))->nama_pelabuhan;			
		}else{
			$nama_pelabuhan = 'Pusat';
		}

		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		// var_dump($id_pelabuhan_pengguna);

		$data['nama_pengguna'] = $nama_pengguna;
		$data['username_pengguna'] = $username_pengguna;
		$data['nama_pelabuhan'] = $nama_pelabuhan;
		$data['id_pelabuhan_pengguna'] = $id_pelabuhan_pengguna;
		$data['id_pengguna'] = $id_pengguna;
		$data['paths'] = $this->config->item('assets_paths');
		$data['basic'] = $this->config->item('basic');
		$data['menus'] = $this->config->item('menus');
		$data['dev_mode'] = $this->dev_mode;
		
		$this->load->view('forms', $data);
	}

	public function reporting($data)
	{
		$id_pengguna = $this->session->userdata('id_pengguna');
		$nama_pengguna = $this->session->userdata('fullname');
		$username_pengguna = $this->session->userdata('username');
		$id_pelabuhan_pengguna = $this->pipp_lib->id_pelabuhan_pengguna();

		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
        $id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }
    
		if($this->pipp_lib->id_grup_pengguna() === "6" || $this->pipp_lib->id_grup_pengguna() === "9")
		{
			$nama_pelabuhan = $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_pengguna, array('nama_pelabuhan'))->nama_pelabuhan;			
		}else{
			$nama_pelabuhan = 'Pusat';
		}

		if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan_pengguna = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		// var_dump($id_pelabuhan_pengguna);

		$data['nama_pengguna'] = $nama_pengguna;
		$data['username_pengguna'] = $username_pengguna;
		$data['nama_pelabuhan'] = $nama_pelabuhan;
		$data['id_pelabuhan_pengguna'] = $id_pelabuhan_pengguna;
		$data['id_pengguna'] = $id_pengguna;
		$data['paths'] = $this->config->item('assets_paths');
		$data['basic'] = $this->config->item('basic');
		$data['menus'] = $this->config->item('menus');
		$data['dev_mode'] = $this->dev_mode;

		$this->load->view('reporting', $data);
	}

	public function global_js()
	{
		$id_pengguna = $this->session->userdata('id_pengguna');
		$nama_pengguna = $this->session->userdata('fullname');
		$username_pengguna = $this->session->userdata('username');
		$id_pelabuhan_pengguna = $this->pipp_lib->id_pelabuhan_pengguna();
		
		if($this->pipp_lib->id_grup_pengguna() === "6" || $this->pipp_lib->id_grup_pengguna() === "9")
		{
			$nama_pelabuhan = $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_pengguna, array('nama_pelabuhan'))->nama_pelabuhan;			
		}else{
			$nama_pelabuhan = 'Pusat';
		}

		$data['nama_pengguna'] = $nama_pengguna;
		$data['username_pengguna'] = $username_pengguna;
		$data['nama_pelabuhan'] = $nama_pelabuhan;
		$data['id_pelabuhan_pengguna'] = $id_pelabuhan_pengguna;
		$data['id_pengguna'] = $id_pengguna;
		$data['paths'] = $this->config->item('assets_paths');
		$data['basic'] = $this->config->item('basic');
		$data['menus'] = $this->config->item('menus');
		$data['dev_mode'] = $this->dev_mode;

		$this->output->set_content_type('application/javascript');
		$this->load->view('global_js', $data);
	}
}

/* End of file jenis_ikan.php */
/* Location: ./application/modules/jenis_ikan/controllers/welcome.php */