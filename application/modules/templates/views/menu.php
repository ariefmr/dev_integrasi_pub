          <div class="nav-collapse collapse">
            <ul class="nav navbar-nav"> 
            <?php
            $admin_menu = $menus['admin_menu'];

            if( $this->pipp_lib->id_grup_pengguna() === "8" )
			{
				$menus += $admin_menu;
				unset($menus['admin_menu']);
			}else{
				unset($menus['admin_menu']);
			}
			            // Daftar menu digenerasi berdasarkan array menu di custom_constants.php
            while($item = current($menus)){
		            	if(is_array($item)){
		            		?>
		            		<li class="dropdown">
						        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo key($menus);?><b class="caret"></b></a>
						        <ul class="dropdown-menu">
			            		<?php
				            		foreach ($item as $text => $url) {
				            			if(is_array($url)){
				            			?>
				            				<li class="dropdown-submenu"><a href="#"><?php echo $text;?></a>
					                            <ul class="dropdown-menu">
					                            <?php foreach ($url as $key => $value) {
					                           	?>
					                              <li><a href="<?php echo $value;?>"><?php echo $key;?></a></li>
					                             <?php
					                         		}
					                             ?>
					                             </ul>
					                        </li>
				            			<?php
				            			}else{
				            			?>
					            			<li><a href="<?php echo $url;?>"> <?php echo $text;?> </a></li>
					            		<?php
				            			}
				            		}
			            		?>
			            		</ul>
			            	</li>
			            <?php
		            	}else{
		            		?>
		            		<li><a href="<?php echo $item;?>"> <?php echo key($menus);?> </a></li>
		            		<?php
		            	}
		    			next($menus);
		    }?>
		    <!--
		    <li id="fat-menu" class="dropdown">
              <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nama_pengguna; ?><b class="caret"></b></a>
              <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="http://integrasi.djpt.kkp.go.id">Menu Utama Integrasi</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="http://http://integrasi.djpt.kkp.go.id/login_baru/portal/logout">Sign Out</a></li>
              </ul>
            </li>
            -->
            </ul>
          </div><!--/.nav-collapse -->