<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title><?php echo $page_title;?></title>

    <!-- Third party core CSS -->
    <link href="<?php echo $paths['misc_css'];?>/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/flick/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/jquery.pnotify.default.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo $paths['main_css'];?>/style.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/sticky-footer-navbar.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal.css" rel="stylesheet">
    
    
    <!-- Loading additional css -->
    <?php if( !empty($additional_css) )
            {
                foreach ($additional_css as $css_file) {
    ?>
            <link href="<?php echo $paths['misc_css'].'/'.$css_file ;?>" rel="stylesheet">
    <?php 
                }//closing foreach  
            }//closing if
    ?>
    

    <!-- Third Party js for this template-->
    <script src="<?php echo $paths['misc_js'];?>/jquery-1.10.2.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery.pnotify.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modal.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modalmanager.js"></script>

    <!-- Main custom js -->
    <script src="<?php echo site_url('templates/type/global_js'); ?>"></script>
    
    <!-- Loading additional js -->
    <?php if( !empty($additional_js) )
            {
                foreach ($additional_js as $js_file) {
    ?>
            <script src="<?php echo $paths['misc_js'].'/'.$js_file;?>"></script>
    <?php 
                }//closing foreach  
            }//closing if
    ?>

    <?php
      if($dev_mode->nilai !== 'true')
      { 
    ?>
          <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <?php
      }
    ?>

  
  </head>

  <body>
    <!-- Wrap all page content here -->
    <div id="wrap"> <!-- Begin main wrap -->
      <div id="banner">
       <div class="container">
       	<div class="row">
         		<div class="col-md-6">
         		<a href="<?php echo base_url(); ?>"><img src="<?php echo $paths['pipp_images'];?>/logo.png" class="img-responsive" alt=""></a>
            </div>
        	</div>          
       </div>
    </div>
      <!-- Fixed navbar -->
      <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>"><?php echo $basic['app_name'];?></a>
        </div>
          <?php
          $this->load->view('templates/menu');
          ?>
      </div>
              </div>


      <!-- Begin page content -->
        <div id="content"> <!-- Begin main content -->
          <div class="container"> <!-- Begin main container -->
            <?php //echo set_breadcrumb(); ?>
              <?php
                $this->load->view($module.'/'.$view_file);
              ?>
          </div>  <!-- END main container -->
        </div> <!-- END main content -->
    </div> <!-- END main wrap -->

    <div id="footer">
      <div class="container">
        <p class="text-muted credit"><?php echo $basic['copyright'].' . '.$basic['owner'];?></p>
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->

  </body>
</html>