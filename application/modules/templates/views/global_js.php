/*
 * Global JS.
 * Kumpulan fungsi yang bisa dipake di semua halaman.
 * Fungsi Shortcut dari JKey
 * Fungsi Notifikasi dari pnotify
 */


$.pnotify.defaults.delay = 4000;

var nama_bulan = new Array("Januari", "Pebruari", "Maret", 
"April", "Mei", "Juni", "Juli", "Agustus", "September", 
"Oktober", "November", "Desember");

var nama_bulan_short = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'];
var sound_fx = { 
                woosh: new Audio('<?php echo $paths["misc_sounds"];?>/Woosh-Mark_DiAngelo-4778593.mp3'),
                blop: new Audio('<?php echo $paths["misc_sounds"];?>/Blop-Mark_DiAngelo-79054334.mp3')
                };

var site_url = '<?php echo site_url(); ?>'; 

var id_pelabuhan_pengguna_session =  <?php echo $this->pipp_lib->id_pelabuhan_pengguna(); ?>;
var id_pelabuhan_admin = <?php echo $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp'); ?>;
var is_admin_mode = false;
<?php if ($this->pipp_lib->info_is_admin('is_admin_otoritas')): ?>
     is_admin_mode = true;
<?php endif ?>

function loading_kirim(pesan) {
    var percent = 0;
    var notice = $.pnotify({
        title: pesan,
        text: 'Test',
        type: 'info',
        icon: 'picon picon-throbber',
        hide: true,
        closer: false,
        sticker: false,
        opacity: 1,
        shadow: false
    });

    return notice;
}

function haraptunggu() {
    var notice = $.pnotify({
        text: 'Harap Tunggu..',
        type: 'info',
        icon: false,
        hide: false,
        shadow: true
    });

    return notice;
}

function loading_kirim(pesan) {
    var percent = 0;
    var notice = $.pnotify({
        title: pesan,
        text: 'Test',
        type: 'info',
        icon: 'picon picon-throbber',
        hide: true,
        closer: false,
        sticker: false,
        opacity: 1,
        shadow: false
    });

    return notice;
}

function istilah_tujuan(istilah)
{
    var new_istilah = '';
    switch(istilah)
    {
        case 'tangkap' : new_istilah = 'Menangkap Ikan';
            break;
        case 'angkut'  : new_istilah = 'Mengangkut Ikan';
            break;
        case 'docking' : new_istilah = 'Docking';
    }

    return new_istilah;
}

function format_date_to_str(objectDate)
{
  // Javasript date use month number start from 0. So January is 0, December is 11;
  return objectDate.getDate()+"/"+ (objectDate.getMonth() +1 )+"/"+objectDate.getFullYear();
}

function format_thnbln_to_str(objectDate)
{
  var bulan = nama_bulan[objectDate.getMonth()];
  return  bulan+", "+objectDate.getFullYear();
}



    $(document).on("mouseover","input[type='file']",function() {
        $.pnotify_remove_all();
        $.pnotify({
            title: "Upload Gambar",
            text: 'Max File Size: 2Mb. <br> Resolution: 2048x1536.<br> File Type: gif, png, jpg,jpeg. ',
            type: 'info',
            closer: true
        });
    });

$(document).ready(function(){
  $("#div_ganti_pelabuhan_admin").hide();
  $("#ganti_pelabuhan").on("click", function(e){
    e.preventDefault();
    $("#div_ganti_pelabuhan_admin").slideToggle();
  })
  $("#id_pelabuhan_current").on("change", function(e) {
    var redir_url = btoa(document.URL),
        open_url = site_url+"admin/ganti_pelabuhan?id_pelabuhan="+e.val+"&url="+redir_url;

    window.open(open_url, '_self'); 
  });
});
