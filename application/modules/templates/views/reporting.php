<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title><?php echo $page_title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $paths['misc_css'];?>/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/flick/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/jquery.pnotify.default.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/sticky-footer-navbar.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/validationEngine.jquery.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="<?php echo $paths['main_css'];?>/style.css" rel="stylesheet">
    
    
    
    <!-- Loading additional css -->
    <?php if( !empty($additional_css) )
            {
                foreach ($additional_css as $css_file) {
    ?>
            <link href="<?php echo $paths['misc_css'].'/'.$css_file ;?>" rel="stylesheet">
    <?php 
                }//closing foreach  
            }//closing if
    ?>
    <?php if (!in_array('select2.css', $additional_css)): ?>
      <link href="<?php echo $paths['misc_css'];?>/select2.css" rel="stylesheet">  
    <?php endif ?>

    <!-- Custom jss for this template TODO : Not always include datatables in everypage-->
    <script src="<?php echo $paths['misc_js'];?>/jquery-1.10.2.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery-ui-1.10.3.custom.min.js"></script> 
    <script src="<?php echo $paths['main_js'];?>/jquery.validationEngine-en.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery.validationEngine.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery.pnotify.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modal.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modalmanager.js"></script>
    

    <!-- Main custom js -->
    <script src="<?php echo site_url('templates/type/global_js'); ?>"></script>
    <!-- Loading additional js -->
    <?php if( !empty($additional_js) )
            {
                foreach ($additional_js as $js_file) {
    ?>
            <script src="<?php echo $paths['misc_js'].'/'.$js_file;?>"></script>
    <?php 
                }//closing foreach  
            }//closing if
    ?>

    <?php if (!in_array('select2.min.js', $additional_js)): ?>
      <script src="<?php echo $paths['misc_js'];?>/select2.min.js"></script>
    <?php endif ?>
    <?php if (in_array('jquery.dataTables.min.js', $additional_js)): ?>
      <script src="<?php echo $paths['main_js'];?>/dataTables.plugins.js"></script>
    <?php endif ?>

    <script type="text/javascript">
       jQuery.fn.dataTableExt.oSort['numeric-comma-asc']  = function(a,b) {
        var x = (a == "-") ? 0 : a.replace( /,/, "." );
        var y = (b == "-") ? 0 : b.replace( /,/, "." );
        x = parseFloat( x );
        y = parseFloat( y );
        return ((x < y) ? -1 : ((x > y) ?  1 : 0));
      };

      jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function(a,b) {
        var x = (a == "-") ? 0 : a.replace( /,/, "." );
        var y = (b == "-") ? 0 : b.replace( /,/, "." );
        x = parseFloat( x );
        y = parseFloat( y );
        return ((x < y) ?  1 : ((x > y) ? -1 : 0));
      };
    </script>
  <?php
      if($dev_mode->nilai !== 'true')
      { 
    ?>
          <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <?php
      }
    ?>
  </head>

  <body>
    <!-- Wrap all page content here -->
    <div id="wrap"> <!-- Begin main wrap -->
      <div id="banner">
       <div class="container">
        <div class="row">
          <div class="col-md-6">
          <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/pipp/images/logo.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-6">
          </div>
          </div>          
       </div>
      </div>
      <!-- Fixed navbar -->
      <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>"><?php echo $basic['app_name'];?></a>
        </div>
          <?php
          $this->load->view('templates/menu');
          ?>
      </div>
        </div>

      <!-- Begin page content -->
        <div id="content"> <!-- Begin main content -->
          <div class="container"> <!-- Begin main container -->
           <!-- TAMPIL DATA -->
           <div class="row">
              <div class="col-lg-8">
                <p><?php echo $breadcrumbs;?></p>
              </div>
              <div class="col-lg-4 text-right">
                 <?php echo $nama_pengguna.", ".$nama_pelabuhan.". ";?>
              <?php if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ): ?>
              <a id="ganti_pelabuhan">Pilih Pelabuhan.</a>
              <?php endif ?>
              </div>
           </div>
           <hr class="hr-for-form">
      <?php if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ): ?>
      <div id="div_ganti_pelabuhan_admin">
         <div class="row">
            <div class="col-lg-8 col-lg-offset-6">
              <?php $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_current',
                                                 'input_name' => 'id_pelabuhan_current',
                                                 'label_text' => 'Pelabuhan: ',
                                                 'array_opsi' => '', 
                                                 'opsi_selected' => $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp'), 
                                                 'input_width' => 'col-lg-6',
                                                 'input_class' => '',
                                                 'label_class' => 'col-lg-2 control-label',
                                                 'placeholder' => 'Ganti Pelabuhan');
                    echo $this->mkform->pilih_pelabuhan_dropdown($attr_opsi_pelabuhan);
             ?>
             </div>
        </div>
      <hr>
      </div>
     <?php endif ?>
          <div class="row">
            <div class="col-lg-12">
              <div class="panel panel-default">
                <div class="panel-body">
                <strong>
                  <?php echo $content_title; ?>
                </strong>
                Pelabuhan <?php echo $nama_pelabuhan_info; ?>
                </div>
              </div>
            </div>
          </div>

<!--           <div class="row">
            <div class="col-lg-2">
            <div class="panel panel-default">
                 <div class="panel-body">
                  <p>Daftar Laporan :</p>
                </div>
              <ul class="list-group">
                <?php foreach ($list_report as $text => $uri): ?>
                  <li class="list-group-item">
                    <?php echo anchor($uri, $text); ?>
                  </li>
                <?php endforeach ?>
              </ul>
            </div>
            </div> -->
          <div class="row">  
            <div class="col-lg-12">
                   <?php
                      // if(!isset($module_run)){
                        $this->load->view($module.'/'.$view_file);
                      // }
                    ?>
            </div>
          </div>


          </div>  <!-- END main container -->
        </div> <!-- END main content -->
    </div> <!-- END main wrap -->

    <div id="footer">
      <div class="container">
        <p class="text-muted credit"><?php echo $basic['copyright'].' . '.$basic['owner'];?></p>
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
  </body>
</html>