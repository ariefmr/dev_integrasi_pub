<?php
/*
 * class Mdl_pelabuhan
 * created by ariefmr
 * at papyrus hotel
 * 12-09-2013
 */

class Mdl_pelabuhan extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss = NULL;
    private $db_pipp = NULL;
    
    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_pipp = $this->load->database('default', TRUE);
    }
    
    /*public function list_pelabuhan($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_dss->like('aktif', 'ya');
        }

        $query = $this->db_dss->get('mst_pelabuhan_pipp');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }*/

    /*public function list_pelabuhan_baru($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_dss->like('aktif', 'ya');
        }

        $query = $this->db_dss->get('mst_pelabuhan_baru');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }*/

    public function list_pelabuhan_lama($is_aktif = TRUE)
    {
        $aktif = $is_aktif ? 'Ya' : 'Tidak';
        $sql = "SELECT
                db_master.mst_pelabuhan.*,
                db_master.mst_propinsi.nama_propinsi,
                db_master.mst_kabupaten_kota.nama_kabupaten_kota,
                db_master.mst_deskripsi_status.nama_deskripsi_status,
                db_master.mst_kelas_pelabuhan.nama_kelas_pelabuhan,
                db_pipp.mst_pengelola_pelabuhan.nama_pengelola_pelabuhan
                FROM db_master.mst_pelabuhan
                LEFT JOIN db_master.mst_propinsi ON db_master.mst_propinsi.id_propinsi = db_master.mst_pelabuhan.id_propinsi
                LEFT JOIN db_master.mst_kabupaten_kota ON db_master.mst_kabupaten_kota.id_kabupaten_kota = db_master.mst_pelabuhan.id_kabupaten_kota
                LEFT JOIN db_master.mst_deskripsi_status ON db_master.mst_deskripsi_status.id_deskripsi_status = db_master.mst_pelabuhan.id_deskripsi_status
                LEFT JOIN db_master.mst_kelas_pelabuhan ON db_master.mst_kelas_pelabuhan.id_kelas_pelabuhan = db_master.mst_pelabuhan.id_kelas_pelabuhan
                LEFT JOIN db_pipp.mst_pengelola_pelabuhan ON db_pipp.mst_pengelola_pelabuhan.id_pengelola_pelabuhan = db_master.mst_pelabuhan.pengelola_pelabuhan 
                WHERE db_master.mst_pelabuhan.aktif = '".$aktif."'";

        $query = $this->db_dss->query($sql);
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_pelabuhan_lama()
    {
        $query = 'SELECT id_pelabuhan AS id, nama_pelabuhan AS text FROM mst_pelabuhan WHERE aktif = "Ya"
                ORDER BY nama_pelabuhan';
        
        $run_query = $this->db_dss->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_pelabuhan_baru($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_pipp->like('aktif', 'ya');
        }

        /*$sql = "SELECT
                db_pipp.mst_pelabuhan_baru.id_pelabuhan,
                db_pipp.mst_pelabuhan_baru.kode_pelabuhan,
                db_master.mst_wpp.nama_wpp,
                db_pipp.mst_pelabuhan_baru.kelas_pelabuhan,
                db_pipp.mst_pelabuhan_baru.nama_pelabuhan,
                db_pipp.mst_pelabuhan_baru.nama_pelabuhan_inggris,
                db_pipp.mst_pelabuhan_baru.alamat_pelabuhan,
                db_pipp.mst_pelabuhan_baru.desa,
                db_pipp.mst_pelabuhan_baru.kecamatan,
                db_pipp.mst_pelabuhan_baru.kabupaten,
                db_pipp.mst_pelabuhan_baru.provinsi,
                db_pipp.mst_pelabuhan_baru.id_deskripsi_status,
                db_pipp.mst_pelabuhan_baru.lintang,
                db_pipp.mst_pelabuhan_baru.bujur,
                db_pipp.mst_pelabuhan_baru.no_telpon,
                db_pipp.mst_pelabuhan_baru.no_telpon2,
                db_pipp.mst_pelabuhan_baru.id_pengguna_buat,
                db_pipp.mst_pelabuhan_baru.tanggal_buat,
                db_pipp.mst_pelabuhan_baru.id_pengguna_ubah,
                db_pipp.mst_pelabuhan_baru.tanggal_ubah,
                db_pipp.mst_pelabuhan_baru.pengelolah_pelabuhan,
                db_pipp.mst_pelabuhan_baru.email,
                db_pipp.mst_pelabuhan_baru.gambar,
                db_pipp.mst_pelabuhan_baru.id_sync,
                db_pipp.mst_pelabuhan_baru.aktif
                FROM db_pipp.mst_pelabuhan_baru
                LEFT JOIN db_master.mst_wpp ON db_master.mst_wpp.id_wpp = db_pipp.mst_pelabuhan_baru.id_wpp
                WHERE db_pipp.mst_pelabuhan_baru.aktif = 'Ya'";*/
        $sql = "SELECT
                db_master.mst_pelabuhan.*,
                db_master.mst_wpp.nama_wpp,
                db_master.mst_propinsi.nama_propinsi,
                db_master.mst_status_pelabuhan.status_pelabuhan,
                db_master.mst_kelas_pelabuhan.nama_kelas_pelabuhan,
                db_master.mst_pengelola_pelabuhan.nama_pengelola_pelabuhan,
                db_master.mst_deskripsi_status.nama_deskripsi_status,
                FROM db_master.mst_pelabuhan
                LEFT JOIN db_master.mst_wpp ON db_master.mst_wpp.id_wpp = db_master.mst_pelabuhan.id_wpp
                LEFT JOIN db_master.mst_propinsi ON db_master.mst_propinsi.id_propinsi = db_master.mst_pelabuhan.id_propinsi
                LEFT JOIN db_master.mst_deskripsi_status ON db_master.mst_deskripsi_status.id_deskripsi_status = db_master.mst_pelabuhan.id_deskripsi_status
                LEFT JOIN db_master.mst_kelas_pelabuhan ON db_master.mst_kelas_pelabuhan.id_kelas_pelabuhan = db_master.mst_pelabuhan.id_kelas_pelabuhan
                LEFT JOIN db_master.mst_pengelola_pelabuhan ON db_master.mst_pengelola_pelabuhan.id_pengelola_pelabuhan = db_master.mst_pelabuhan.id_pengelola_pelabuhan ";

        $query = $this->db_dss->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_pelabuhan()
    {
        $query = 'SELECT id_pelabuhan AS id, nama_pelabuhan AS text FROM mst_pelabuhan_baru 
                    WHERE aktif = "Ya" ORDER BY nama_pelabuhan';
        
        $run_query = $this->db_pipp->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_per_pelabuhan($id_pelabuhan)
    {
        
        $sql = "SELECT
                db_master.mst_pelabuhan.id_pelabuhan,
                db_master.mst_pelabuhan.nama_pelabuhan,
                db_master.mst_pelabuhan.nama_pelabuhan_inggris,
                db_master.mst_propinsi.nama_propinsi,
                db_master.mst_kabupaten_kota.nama_kabupaten_kota,
                db_master.mst_pelabuhan.alamat_pelabuhan,
                db_master.mst_kelas_pelabuhan.nama_kelas_pelabuhan,
                db_master.mst_pengelola_pelabuhan.nama_pengelola_pelabuhan,
                db_master.mst_deskripsi_status.nama_deskripsi_status,
                db_master.mst_pelabuhan.id_kabupaten_kota,
                db_master.mst_pelabuhan.id_propinsi,
                db_master.mst_pelabuhan.lintang,
                db_master.mst_pelabuhan.bujur,
                db_master.mst_pelabuhan.no_telp1,
                db_master.mst_pelabuhan.no_telp2,
                db_master.mst_pelabuhan.no_faksimile,
                db_master.mst_pelabuhan.email,
                db_master.mst_pelabuhan.aktif,
                db_master.mst_pelabuhan.tanggal_ubah
                FROM db_master.mst_pelabuhan
                LEFT JOIN db_master.mst_propinsi ON db_master.mst_propinsi.id_propinsi = db_master.mst_pelabuhan.id_propinsi
                LEFT JOIN db_master.mst_kabupaten_kota ON db_master.mst_kabupaten_kota.id_kabupaten_kota = db_master.mst_pelabuhan.id_kabupaten_kota
                LEFT JOIN db_master.mst_deskripsi_status ON db_master.mst_deskripsi_status.id_deskripsi_status = db_master.mst_pelabuhan.id_deskripsi_status
                LEFT JOIN db_master.mst_kelas_pelabuhan ON db_master.mst_kelas_pelabuhan.id_kelas_pelabuhan = db_master.mst_pelabuhan.id_kelas_pelabuhan
                LEFT JOIN db_master.mst_pengelola_pelabuhan ON db_master.mst_pengelola_pelabuhan.id_pengelola_pelabuhan = db_master.mst_pelabuhan.pengelola_pelabuhan
                WHERE db_master.mst_pelabuhan.id_pelabuhan = '$id_pelabuhan'";

        $query = $this->db_dss->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_pelabuhan($id_pelabuhan, $fields_detail ='')
    {   
        if(count($fields_detail) > 1)
        {
            $this->db->select( implode(",",$fields_detail) );
        }elseif ( is_array($fields_detail) && count($fields_detail) === 1 ) {
            $this->db->select( $fields_detail[0] );
        }

        $this->db_dss->where('id_pelabuhan', $id_pelabuhan);
        $query = $this->db_dss->get('mst_pelabuhan');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_pegawai($id_pelabuhan)
    {   
        // $sql = "SELECT 
        //         distinct(seksi_bidang) as bidang, count(seksi_bidang) as jumlah 
        //         FROM db_pipp.mst_pegawai 
        //         WHERE id_pelabuhan='$id_pelabuhan' 
        //         group by seksi_bidang " ;

        $sql = " 
                SELECT 
                    seksi_bidang as bidang,
                    count(Case when GOL = 'I' and status <> 'Honorer' THEN GOL END) as 'I',
                    count(Case when GOL = 'II' and status <> 'Honorer' THEN GOL END) as 'II',
                    count(Case when GOL = 'III' and status <> 'Honorer' THEN GOL END) as 'III',
                    count(Case when GOL = 'IV' and status <> 'Honorer' THEN GOL END) as 'IV',
                    count(Case when `status` = 'Honorer' Then `status` END) as 'Honorer',
                    count(Case when GOL = 'I' or GOL = 'II' or GOL = 'III' or GOL = 'IV' or GOL ='-' Then id_pegawai END ) as Jumlah
                FROM
                    db_pipp.mst_pegawai
                WHERE
                    id_pelabuhan = '$id_pelabuhan'
                group by seksi_bidang
                ";

        $query = $this->db_pipp->query($sql);


        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_status_pelabuhan()
    {
        $this->db->select('id_deskripsi_status');
        $this->db->select('mst_deskripsi_status');
        $query = $this->db_dss->get('mst_deskripsi_status');
        
        return $query->result();
    }

}
?>