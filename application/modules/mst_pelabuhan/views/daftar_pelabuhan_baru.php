<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pelabuhan' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	if($this->pipp_lib->info_is_admin('is_admin_otoritas')){
				$this->table->set_heading(	
								'No',
								'Aksi',
								'Situasional',
								'Nama Pelabuhan', 
								'Nama Pelabuhan Inggris',
								'Kelas Pelabuhan',
								'Kepala Pelabuhan',
								'Status Pelabuhan', 
								'Pengelola Pelabuhan', 
								'WPP', 
								'Alamat Pelabuhan', 
								'Provinsi', 
								'Kabupaten', 
								'Kecamatan', 
								'Desa', 
								'Lintang', 
								'Bujur', 
								'No Telp 1', 
								'No Fax', 
								'Email'
								);

	}else{
		$this->table->set_heading(	
								'No',
								'Nama Pelabuhan', 
								'Nama Pelabuhan Inggris',
								'Kelas Pelabuhan',
								'Kepala Pelabuhan',
								'Status Pelabuhan', 
								'Pengelola Pelabuhan', 
								'WPP', 
								'Alamat Pelabuhan', 
								'Provinsi', 
								'Kabupaten', 
								'Kecamatan', 
								'Desa', 
								'Lintang', 
								'Bujur', 
								'No Telp 1', 
								'No Fax', 
								'Email'
								);
	}
	
	$counter = 1;
	//$default_gambar_pelabuhan = '<img src="'.$path_gambar_pelabuhan.'/blank.png" class="img-thumbnail" >';
		$selects = "<select class='list_forms_select'>";
		foreach ($list_forms as $index => $item) {
			$selects .= "<option value='".$item['link']."'>".$item['judul']."</option>";
		}
		$selects .= "</select>";

	if($list_pelabuhan !== FALSE){
		foreach ($list_pelabuhan as $item) {
			if($this->pipp_lib->info_is_admin('is_admin_otoritas')){
					$this->table->add_row(
										$counter.'.', 
										'<button class="btn btn-info edit_this" data-id-record="'.$item->id_pelabuhan.'">EDIT</button>',
										'<div class="select-forms" data-id-pelabuhan="'.$item->id_pelabuhan.'">'
										.$selects.
										'</div>',
										$item->nama_pelabuhan, 
										$item->nama_pelabuhan_inggris, 
										$item->nama_kelas_pelabuhan, 
										$item->nama_kepala_pelabuhan, 
										$item->status_pelabuhan, 
										$item->nama_pengelola_pelabuhan, 
										$item->nama_wpp, 
										$item->alamat_pelabuhan, 
										$item->provinsi, 
										$item->kabupaten, 
										$item->kecamatan, 
										$item->desa, 
										$item->lintang, 
										$item->bujur, 
										$item->no_telpon, 
										$item->no_telpon2,
										$item->email
										);
								$counter++;
			}else{
			$this->table->add_row(
								$counter.'.', 
								$item->nama_pelabuhan, 
								$item->nama_pelabuhan_inggris, 
								$item->nama_kelas_pelabuhan, 
								$item->nama_kepala_pelabuhan, 
								$item->status_pelabuhan, 
								$item->nama_pengelola_pelabuhan, 
								$item->nama_wpp, 
								$item->alamat_pelabuhan, 
								$item->provinsi, 
								$item->kabupaten, 
								$item->kecamatan, 
								$item->desa, 
								$item->lintang, 
								$item->bujur, 
								$item->no_telpon, 
								$item->no_telpon2,
								$item->email
								);
				$counter++;
			}
		}
	}
	

	$table_list_pelabuhan = $this->table->generate();
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading"><?php echo $content_title; ?></div>
			<div class="panel-body overflowed">
<!-- TAMPIL DATA -->
<a href="<?php echo base_url('mst_pelabuhan/daftar_pelabuhan_baru/aktif'); ?>">Pelabuhan Aktif</a>

		<?php
			echo $table_list_pelabuhan;

		?>>
			</div>
		</div>
	</div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s6a',
                             'modal_title' => 'Edit S6A',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S6A/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>

	$(document).ready( function () {
		$('#table_daftar_pelabuhan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );

		$(".list_forms_select").on("change", function(){
			var id_pelabuhan = $(this).parent().data('idPelabuhan'),
				link_jurnal = $(this).val(),
				link_entry = link_jurnal.replace('jurnal','entry')+"/index/";

				window.open(link_entry+id_pelabuhan, '_self');
		});

	} );
</script>