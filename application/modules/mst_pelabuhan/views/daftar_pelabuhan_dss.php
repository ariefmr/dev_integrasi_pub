<?php
  //OLAH DATA TAMPIL
  $template = array( "table_open" => "<table id='table_daftar_pelabuhan' class='table table-hover table-condensed'>");
  $this->table->set_template($template);
  if($this->pipp_lib->info_is_admin('is_admin_otoritas')){
        $this->table->set_heading(  
                'No',
                'Aksi',
                'Situasional',
                'Nama Pelabuhan', 
                'Nama Pelabuhan Inggris',
                'Kelas Pelabuhan',
                'Status Pelabuhan', 
                'Pengelola Pelabuhan', 
                'Alamat Pelabuhan', 
                'Propinsi',
                'Kabupaten', 
                // 'Kecamatan', 
                // 'Desa', 
                'Lintang', 
                'Bujur', 
                'No Telp 1', 
                'No Fax', 
                'Email'
                );

  }else{
    $this->table->set_heading(  
                'No',
                'Nama Pelabuhan', 
                'Nama Pelabuhan Inggris',
                'Kelas Pelabuhan',
                'Status Pelabuhan', 
                'Pengelola Pelabuhan', 
                'Alamat Pelabuhan', 
                'Propinsi',
                'Kabupaten', 
                // 'Kecamatan', 
                // 'Desa', 
                'Lintang', 
                'Bujur', 
                'No Telp 1', 
                'No Fax', 
                'Email'
                );
  }
  
  // //$default_gambar_pelabuhan = '<img src="'.$path_gambar_pelabuhan.'/blank.png" class="img-thumbnail" >';
    $selects = "<select class='list_forms_select'>";
    foreach ($list_forms as $index => $item) {
      if($item['judul'] !== 'Data Umum Pelabuhan'){
        $selects .= "<option value='".$item['link']."'>".$item['judul']."</option>";
      }
    }
    $selects .= "</select>";


  if($list_pelabuhan !== FALSE){
    $counter = 1;

    foreach ($list_pelabuhan as $item) {
      if($this->pipp_lib->info_is_admin('is_admin_otoritas')){
          $this->table->add_row(
                    $counter.'.', 
                    '<button class="btn btn-info edit_this" data-id-record="'.$item->id_pelabuhan.'">EDIT</button>',
                    '<div class="select-forms" data-id-pelabuhan="'.$item->id_pelabuhan.'" data-nama-pelabuhan="'.$item->nama_pelabuhan.'">'
                    .$selects.
                    '</div>',
                    $item->nama_pelabuhan.'<small title="id_pelabuhan di database"> (id: '.$item->id_pelabuhan.')</small>', 
                    $item->nama_pelabuhan_inggris, 
                    $item->nama_kelas_pelabuhan, 
                    // $item->nama_status_pelabuhan,  //Table referensi status pelabuhan di DSS belum ada datanya
                    $item->nama_deskripsi_status,
                    $item->nama_pengelola_pelabuhan, 
                    $item->alamat_pelabuhan, 
                    $item->nama_propinsi, 
                    $item->nama_kabupaten_kota, 
                    // $item->kecamatan, 
                    // $item->desa, 
                    $item->lintang, 
                    $item->bujur, 
                    $item->no_telp1, 
                    $item->no_telp2,
                    $item->email
                    );
                $counter++;
      }else{
      $this->table->add_row(
                    $counter.'.', 
                    $item->nama_pelabuhan.'<small title="id_pelabuhan di database"> (id: '.$item->id_pelabuhan.')</small>', 
                    $item->nama_pelabuhan_inggris, 
                    $item->nama_kelas_pelabuhan, 
                    // $item->nama_status_pelabuhan,  //Table referensi status pelabuhan di DSS belum ada datanya
                    $item->deskripsi_status,
                    $item->nama_pengelola_pelabuhan, 
                    $item->alamat_pelabuhan, 
                    $item->nama_propinsi, 
                    $item->nama_kabupaten_kota, 
                    // $item->kecamatan, 
                    // $item->desa, 
                    $item->lintang, 
                    $item->bujur, 
                    $item->no_telp1, 
                    $item->no_telp2,
                    $item->email
                );
        $counter++;
      }
    }
  }
  

  $table_list_pelabuhan = $this->table->generate();
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-heading"><?php echo $content_title; ?></div>
      <div class="panel-body overflowed">
      <p class="text-right">
      <?php if ($is_aktif === 'aktif'): ?>
          <a href="<?php echo base_url('mst_pelabuhan/daftar_pelabuhan_dss/nonaktif'); ?>">Tampilkan Pelabuhan Non-Aktif</a>     
      <?php else: ?>
          <a href="<?php echo base_url('mst_pelabuhan/daftar_pelabuhan_dss/aktif'); ?>">Tampilkan Pelabuhan Aktif</a>     
      <?php endif ?>
      </p>
    <span class="badge">Jumlah Pelabuhan  <?php echo $info_aktif." : ".$jumlah_pelabuhan; ?></span>
    <?php
      echo $table_list_pelabuhan;

    ?>>
      </div>
    </div>
  </div>
</div>

<?php
    // $attr_modal_edit = array('modal_id' => 'modal_edit_s6a',
    //                          'modal_title' => 'Edit S6A',
    //                          'modal_width' => '760',
    //                          'link_view_form' => base_url('entry/S6A/edit')
    //                         ); 
    // echo $this->mkform->modal_edit($attr_modal_edit);
?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var link_edit_pelabuhan_dss = "http://integrasi.djpt.kkp.go.id/datamaster_baru/admin/ubah/mst_pelabuhan/";

  $(document).ready( function () {
    $('#table_daftar_pelabuhan').dataTable( {
      "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
    } );

    $(".list_forms_select").on("change", function(e){
      var id_pelabuhan = $(this).parent().data('idPelabuhan'),
        nama_pelabuhan = $(this).parent().data('namaPelabuhan'),
        link_jurnal = $(this).val(),
        judul_form = $(this).find("option:selected").text(),
        link_entry = btoa(link_jurnal.replace('jurnal','entry')+"/index/"),
        open_url = site_url+"admin/ganti_pelabuhan?id_pelabuhan="+id_pelabuhan+"&url="+link_entry,
        pesan = "Membuka form '"+judul_form+"' di pelabuhan "+nama_pelabuhan+". Lanjutkan?";

        if(confirm(pesan))
        {
          window.open(open_url, '_blank');
        }
    });

    $(".edit_this").on("click", function(){
        var id_pelabuhan = $(this).data('idRecord'),
              link_edit = link_edit_pelabuhan_dss+id_pelabuhan,
              pesan = "Membuka aplikasi datamaster pelabuhan di integrasi. Lanjutkan? ";

        if(confirm(pesan))
          {
            window.open(link_edit, '_blank');
          }else{
            return false;
          }   

    });

  } );
</script>