<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pelabuhan' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'ID Pelabuhan', 'ID Group Pelabuhan', 'Nama Pelabuhan', 'Nama Pelabuhan Inggris','Alamat Pelabuhan', 'No Telp 1', 'No Telp 2', 'No Faksimile', 'Kode Pos', 'Foto Pelabuhan', 'Lintang', 'Bujur', 'Status', 'Deskripsi Status', 'Tanggal Ubah', 'Aktif');
	$counter = 1;
	$default_gambar_pelabuhan = '<img src="'.$path_gambar_pelabuhan.'/blank.png" class="img-thumbnail" >';

	if($list_pelabuhan !== FALSE){
		foreach ($list_pelabuhan as $item) {
			$this->table->add_row($counter.'.', $item->id_pelabuhan, $item->id_group_pelabuhan, $item->nama_pelabuhan, $item->nama_pelabuhan_inggris, $item->alamat_pelabuhan, $item->no_telp1, $item->no_telp2, $item->no_faksimile, $item->kode_pos, $default_gambar_pelabuhan, $item->lintang, $item->bujur, $item->status, $item->deskripsi_status, $item->tanggal_buat, $item->aktif);
			$counter++;
		}
	}
	

	$table_list_pelabuhan = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<a href="<?php echo base_url('mst_pelabuhan/daftar_pelabuhan/aktif'); ?>">Pelabuhan Aktif</a>
		<?php
			echo $table_list_pelabuhan;

		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_pelabuhan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>