<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_pelabuhan extends MX_Controller {

	/**
	 * Controller Mst Pelabuhan
	 * created by ariefmr
 	 * at papyrus hotel
 	 * 12-09-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_pelabuhan');
		}

	public function index()
	{

		$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_baru();
		$this->load->view('daftar_pelabuhan_baru', $data);
	}

	public function daftar_pelabuhan_baru($aktif = '') 
	{
		$this->load->library('entry/mkform');
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis pelabuhan';
		$data['content_title'] = 'Daftar Jenis pelabuhan';
		$data['module'] = 'mst_pelabuhan';
		$data['view_file'] = 'daftar_pelabuhan_baru';
		if($aktif === "aktif"){
			$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_baru(TRUE);

			$data['path_gambar_pelabuhan'] = $this->assets_paths['pipp_images'];
		}else{
			$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_baru();

			$data['path_gambar_pelabuhan'] = $this->assets_paths['pipp_images'];
		}

		$get_list_forms = $this->config->item('link_terkait');
		$list_forms = array();

		foreach ($get_list_forms as $index => $list_link) {
			if( strpos($index, 's') !== false) {
				foreach ($list_link as $judul => $link) {
					array_push($list_forms, array('judul'=> $judul, 'link'=> $link));
				}
			}
		}

		$data['list_forms'] = $list_forms;
		echo Modules::run('templates/type/default_template', $data);	
	}

	public function daftar_pelabuhan_dss($aktif = '') 
	{
		$this->load->library('entry/mkform');
		$data['is_aktif'] = $aktif === '' ? 'aktif' : $aktif ; // aktif/nonaktif
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$is_aktif = $data['is_aktif'] === 'aktif' ? TRUE : FALSE;
		$info_aktif = $data['is_aktif'] === 'aktif'? 'Aktif' : 'Non-Aktif';
		$data['info_aktif'] = $info_aktif;
		$data['page_title'] = 'Daftar Pelabuhan '.$info_aktif.' (DSS)';
		$data['content_title'] = 'Daftar Pelabuhan '.$info_aktif.' (DSS)';
		$data['module'] = 'mst_pelabuhan';
		$data['view_file'] = 'daftar_pelabuhan_dss';

			$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_lama($is_aktif);
			$data['jumlah_pelabuhan'] = count($data['list_pelabuhan']);
			$data['path_gambar_pelabuhan'] = $this->assets_paths['pipp_images'];


		$get_list_forms = $this->config->item('link_terkait');
		$list_forms = array();

		foreach ($get_list_forms as $index => $list_link) {
			if( strpos($index, 's') !== false) {
				foreach ($list_link as $judul => $link) {
					array_push($list_forms, array('judul'=> $judul, 'link'=> $link));
				}
			}
		}

		$data['list_forms'] = $list_forms;
		echo Modules::run('templates/type/default_template', $data);	
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar pelabuhan
	public function select_pelabuhan()
	{
		$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_baru();
		$this->load->view('select_pelabuhan', $data);
	}

	public function array_pelabuhan()
	{
		$array_pelabuhan = $this->mdl_pelabuhan->list_opsi_pelabuhan();
		return $array_pelabuhan;
	}

	public function json_pelabuhan()
	{
		$array_of_pelabuhan = (Array) $this->mdl_pelabuhan->list_opsi_pelabuhan();

		echo json_encode($array_of_pelabuhan);
	}

	public function table_pelabuhan()
	{
		$data['path_gambar_pelabuhan'] = $this->assets_paths['pipp_images'];
		$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_baru();	
		$this->load->view('daftar_pelabuhan_baru', $data);
	}


	// UNTUK PELABUHAN LAMA
	public function select_pelabuhan_lama()
	{
		$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_lama();
		$this->load->view('select_pelabuhan', $data);
	}

	public function array_pelabuhan_lama()
	{
		$array_pelabuhan = $this->mdl_pelabuhan->list_opsi_pelabuhan_lama();
		return $array_pelabuhan;
	}

	public function json_pelabuhan_lama()
	{
		$array_of_pelabuhan = (Array) $this->mdl_pelabuhan->list_opsi_pelabuhan_lama();

		echo json_encode($array_of_pelabuhan);
	}

	public function table_pelabuhan_lama()
	{
		$data['path_gambar_pelabuhan'] = $this->assets_paths['pipp_images'];
		$data['list_pelabuhan'] = $this->mdl_pelabuhan->list_pelabuhan_lama();	
		$this->load->view('daftar_pelabuhan_baru', $data);
	}

	public function array_status_pelabuhan()
	{
		$array_pelabuhan = $this->mdl_pelabuhan->list_status_pelabuhan();
		return $array_pelabuhan;
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}
}

/* End of file mst_pelabuhan.php */
/* Location: ./application/modules/mst_pelabuhan/controllers*/