<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_kapal extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: 
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_kapal');
		}

	public function index()
	{
		$data['list_kapal'] = $this->mdl_kapal->list_kapal();
		$this->load->view('daftar_kapal', $data);
	}

	//
	public function daftar_kapal() 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');
		$data['page_title'] = 'Daftar Jenis Ikan';
		$data['content_title'] = 'Daftar Jenis Ikan';
		$data['module'] = 'mst_kapal';
		$data['view_file'] = 'daftar_kapal';
		$data['list_kapal'] = $this->mdl_kapal->list_kapal();

		echo Modules::run('templates/type/default_template', $data);
	}

	// Wigdet Pencarian kapal untuk keperluan entry form 
	public function wgt_pilih_kapal()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_kapal'] = FALSE;
		$this->load->view('pilih_kapal', $data);
	}

	public function search_kapal()
	{
		$get_search_query = $this->input->get('q', FALSE);
		$get_filter_gt = $this->input->get('gt', FALSE);
		$get_filter_query = $this->input->get('filter', FALSE);
		$get_limit_result = $this->input->get('limit', FALSE);
		$search_result = $this->mdl_kapal->search_kapal($get_search_query, $get_filter_gt, $get_filter_query, $get_limit_result);
		if($search_result !== FALSE){
			$array_result = Array( 'total' => count($search_result),'list_kapal' => $search_result,
									'search' => Array('search_q' => $get_search_query,
														'gt' => $get_filter_gt,  'filter' => $get_filter_query)
								);
		}else{
			$array_result = Array( 'total' => count($search_result),'list_kapal' => Array(),
									'search' => Array('search_q' => $get_search_query,
														'gt' => $get_filter_gt,  'filter' => $get_filter_query)
								 );
		}
		
		echo json_encode($array_result);
	}

	public function detail_kapal()
	{
		$id_kapal = $this->input->get('id_kapal', FALSE);
		$filter_gt = $this->input->get('filter_gt', FALSE);

		$search_result = $this->mdl_kapal->detail_kapal($id_kapal, $filter_gt);
		$array_result = $search_result !== FALSE ? $search_result[0] : Array('kosong');
		echo json_encode($array_result);

	}

	// Menghasilkan elemen dropdown select dengan opsi daftar kapal
	public function select_kapal()
	{
		$data['list_kapal'] = $this->mdl_kapal->list_kapal();
		$this->load->view('select_kapal', $data);
	}

	public function table_kapal()
	{
		$data['list_kapal'] = $this->mdl_kapal->list_kapal();	
		$this->load->view('daftar_kapal', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}
}

/* End of file jenis_kapal.php */
/* Location: ./application/modules/jenis_kapal/controllers/welcome.php */