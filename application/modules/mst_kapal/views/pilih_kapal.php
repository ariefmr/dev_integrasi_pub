<!-- TAMPIL DATA -->
  	<div id="panel-pilih-kapal" class="panel">
        <div class="panel-body">
			<div class="row">
					<div class="col-lg-4">
						<div class="btn-group pull-right">
						  <button id="toggle_gt" type="button" class="btn btn-primary" data-current-gt="below" data-toggle="button">&#8804 30 GT</button>

						  <div class="btn-group">
						    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						      Cari <text id="info_filter">Nama Kapal  </text>
						      <span class="caret"></span>
						    </button>
						    <ul class="dropdown-menu">
						      <li><a href="#" class="ubah_filter" data-this-filter="namakapal">Nama Kapal</a></li>
						      <li><a href="#" class="ubah_filter" data-this-filter="tandaselar">Tanda Selar</a></li>
						      <li><a href="#" class="ubah_filter" data-this-filter="nomorsipi">Nomor SIPI</a></li>
						    </ul>
						  </div>
						</div>
					</div>
					<div class="col-lg-8">
						 <input id="start_search" name="id_kapal" type="hidden" class="bigdrop">
					</div>

					
			</div>
		</div>
	</div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null,
		url_search_kapal = "<?php echo base_url('mst_kapal/search_kapal'); ?>",
		current_gt = function(){ return $("#toggle_gt").data("currentGt") },
		current_filter = 'namakapal';

	function formatListKapalResult(kapal)
	{
		html = "<table id='"+kapal.id_kapal+"' class='table table-condensed table-border'><tr>"
			 + "<td>"+kapal.nama_kapal+"</td>"
			 + "<td>"+kapal.tanda_selar+"</td>"
			 + "<td>GT: "+kapal.gt_kapal+"</td>"
			 + "<td>"+kapal.no_sipi+"</td>"
			 + "<td>"+kapal.tanggal_sipi+"</td>"
			 + "</tr></table>"
		return  html;

	}

	function formatListKapalSelection(kapal)
	{
		return kapal.nama_kapal;
	}

	function formatSearchingText(term)
	{
		return "Sedang mencari..";
	}

	function formatKurangText(term, minLength)
	{
		var char_yang_kurang = minLength - term.length,
			info_filter = $("#info_filter").text();
			info_gt = $("#toggle_gt").text();
			text_info = "Pencarian kapal "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

			text_info += "Input minimal "+char_yang_kurang+" karakter";  
		return text_info;
	}

	function formatNotFound(term)
	{
		var link_daftar = "";
			if(current_gt()	 === "below")
				{
					link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
				}else
				{
					link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
				}
		 
		return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. <a href='"+link_daftar+"'>Kapal Belum Terdaftar?</a>";
	}


	function searchURLGenerator()
	{

	}
	$(document).ready( function () {
		
		$("#toggle_gt").click(function(){
			var currentGt = $(this).data("currentGt");
				if(currentGt === "below")
				{
					$(this).text("> 30 GT");
					$(this).data("currentGt","above");
					
				}else
				{
					$(this).html("&#8804 30 GT");
					$(this).data("currentGt","below");			
				}
				$("#start_search").select2("open");
		});

		$(".ubah_filter").click(function(){
			var thisFilter = $(this).data("thisFilter"),
				thisText = $(this).text();

				$("#info_filter").text(thisText);
				current_filter = thisFilter;
				$("#start_search").select2("open");
		});

		$("#start_search").select2({
									id: function(e) { return e.id_kapal },
									allowClear: false,  	
									placeholder: "Pilih Kapal..",
									width: "100%",
									cache: true,
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: url_search_kapal,
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	gt: current_gt,
																	filter: current_filter,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_kapal};
									        }
									},
                                    formatResult: formatListKapalResult,
                                    formatSelection: formatListKapalSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_kapal(e.val, current_gt());
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_kapal/json_kapal'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_kapal(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>