<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'Nama Kapal','SIPI','Tonase','Perusahaan/Pemilik','Alat Tangkap','WPP', 'DPI');
	$counter = 1;
	foreach ($list_kapal as $item) {
		$this->table->add_row($counter.'.', $item->nama_kapal, $item->no_sipi, $item->gt_kapal, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_alat_tangkap, $item->nama_wpp, $item->nama_dpi);
		$counter++;
	}

	$table_list_kapal = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"bInfo": false
		} );
	} );
</script>