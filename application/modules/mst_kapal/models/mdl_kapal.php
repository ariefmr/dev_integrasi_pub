<?php
/*
 * class Mdl_kapal
 */

class Mdl_kapal extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }
    
    public function list_kapal($limit = '100')
    {
        $query = 'SELECT mst_kapal.id_kapal, mst_kapal.nama_kapal, mst_kapal.gt_kapal,
                    mst_perusahaan.nama_perusahaan, mst_perusahaan.nama_penanggung_jawab, 
                    mst_alat_tangkap.nama_alat_tangkap,mst_kapal.id_alat_tangkap,
                    mst_izin.no_sipi, mst_izin.id_dpi, mst_izin.nama_dpi, mst_izin.nama_wpp
                    FROM mst_kapal
                    LEFT JOIN (mst_perusahaan, mst_alat_tangkap, mst_izin)
                    ON (    mst_kapal.id_alat_tangkap_perizinan = mst_alat_tangkap.id_alat_tangkap
                            AND mst_kapal.id_perusahaan = mst_perusahaan.id_perusahaan 
                            AND mst_izin.id_kapal = mst_kapal.id_kapal
                        )  
                    LIMIT 0, '.$limit.'';
        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_kapal($id_kapal, $filter_gt)
    {   
        switch ($filter_gt) {
            case 'above':
               $query = "Select * from mst_kapal
                     left outer join mst_alat_tangkap on mst_kapal.id_alat_tangkap = mst_alat_tangkap.id_alat_tangkap
                     left outer join mst_izin on mst_izin.id_kapal = mst_kapal.id_kapal 
                     left outer join mst_perusahaan on mst_perusahaan.id_perusahaan = mst_kapal.id_perusahaan 
                     where mst_kapal.id_kapal = $id_kapal
                        and mst_izin.tanggal_akhir_sipi >= now()
                        order by mst_izin.tanggal_akhir_sipi desc limit 1";
                break;
            case 'below':
                $query = "Select mkd.id_kapal_daerah as id_kapal, 
                                 mkd.id_perusahaan_daerah as id_perusahaan,
                                mkd.*, mat.*, mpd.*,mid.*
                             from mst_kapal_daerah mkd
                     left outer join mst_alat_tangkap mat on mkd.id_alat_tangkap = mat.id_alat_tangkap
                     left outer join mst_izin_daerah mid on mid.id_kapal_daerah = mkd.id_kapal_daerah 
                     left outer join mst_perusahaan_daerah mpd on mpd.id_perusahaan_daerah = mkd.id_perusahaan_daerah 
                     where mkd.id_kapal_daerah = $id_kapal
                        and mid.tanggal_akhir_sipi >= now()
                        order by mid.tanggal_akhir_sipi desc limit 1";
                break;
        }
        

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function search_kapal($search_query, $filter_gt, $filter_query, $limit_result)
    {
        // $query = "SELECT mst_kapal.id_kapal, mst_kapal.nama_kapal,
        //             mst_kapal.tanda_selar, CEIL(mst_kapal.gt_kapal) AS gt_kapal,
        //             mst_izin.no_sipi, mst_izin.tanggal_sipi, mst_izin.tanggal_akhir_sipi
        //               FROM mst_kapal
        //                 LEFT JOIN mst_izin ON mst_kapal.id_kapal = mst_izin.id_kapal";

        switch ($filter_gt) {
            case 'above':
               $query =" Select * from mst_izin , mst_kapal , mst_perusahaan ";
                break;
            case 'below':
                $query =" Select mst_kapal_daerah.id_kapal_daerah as id_kapal, 
                            mst_kapal_daerah.*, mst_izin_daerah.*, mst_perusahaan_daerah.* 
                            from mst_izin_daerah , mst_kapal_daerah , mst_perusahaan_daerah ";
                break;
        }

            

        switch ($filter_query) {
            case 'namakapal':
                    if($filter_gt === 'above')
                        {
                        $query .= " WHERE mst_kapal.nama_kapal LIKE '%".$search_query."%' ";
                    }elseif($filter_gt === 'below')
                    {
                         $query .= " WHERE mst_kapal_daerah.nama_kapal LIKE '%".$search_query."%' ";
                    }   
                break;
            case 'tandaselar':
                    if($filter_gt === 'above')
                    {
                        $query .= " WHERE mst_kapal.tanda_selar LIKE '%".$search_query."%' ";
                    }elseif($filter_gt === 'below')
                    {
                        $query .= " WHERE mst_kapal_daerah.tanda_selar LIKE '%".$search_query."%' ";
                    }
                break;
            case 'nomorsipi':
                    if($filter_gt === 'above')
                    {
                        $query .= " WHERE mst_izin.no_sipi LIKE '%".$search_query."%' ";  
                    }elseif($filter_gt === 'below')
                    {
                        $query .= " WHERE mst_izin_daerah.no_sipi LIKE '%".$search_query."%' ";
                    }
                break;
        }

        switch ($filter_gt) {
            case 'above':
                $query .= " AND mst_kapal.gt_kapal > 29 ";
                $query  .= "  AND mst_izin.id_kapal = mst_kapal.id_kapal
                      AND mst_izin.tanggal_akhir_sipi >= now()                         
                      AND mst_perusahaan.id_perusahaan = mst_kapal.id_perusahaan
                      ORDER BY mst_kapal.nama_kapal ASC
                      LIMIT 0,100 ";
                break;
            case 'below':
                $query .= " AND mst_kapal_daerah.gt_kapal <= 30 ";
                $query  .= "  AND mst_izin_daerah.id_kapal_daerah = mst_kapal_daerah.id_kapal_daerah
                      AND mst_izin_daerah.tanggal_akhir_sipi >= now()                         
                      AND mst_perusahaan_daerah.id_perusahaan_daerah = mst_kapal_daerah.id_perusahaan_daerah
                      AND mst_kapal_daerah.aktif = 'Ya'
                      ORDER BY mst_kapal_daerah.nama_kapal ASC
                      LIMIT 0,100 ";
                break;
        }
        
        
        // var_dump($query);              
        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>