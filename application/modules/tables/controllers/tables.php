<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tables extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();

			$this->load->library('entry/mkform');
			$this->load->model('mdl_tables');
			$this->load->model('admin/mdl_konfigurasi');
			//TODO: LOGIN CHECK HERE
		}

	public function pipp_get_all($nama_tabel)
	{
		$data['page_title'] = 'Tables';
		$data['module'] = 'tables';
		$data['view_file'] = 'v_dump';

		$data['result'] = $this->mdl_tables->pipp_get_all($nama_tabel);
		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		echo Modules::run('templates/type/default_template', $data);
	}

	public function pipp_tbl_opsi($nama_tabel, $field_id, $field_text)
	{
		$array_result = $this->mdl_tables->pipp_tbl_opsi($nama_tabel,
																														 $field_id,
																														 $field_text,
																														 FALSE);
		return $array_result;
	}

	public function pipp_tbl_json($nama_tabel, $field_id, $field_text)
	{
		$array_result = $this->mdl_tables->pipp_tbl_opsi($nama_tabel,
																														 $field_id,
																														 $field_text,
																														 TRUE);

		return json_encode($array_result);
	}

	public function dss_tbl_opsi($nama_tabel, $field_id, $field_text)
	{
		$array_result = $this->mdl_tables->dss_tbl_opsi($nama_tabel, 
																										$field_id,
																										$field_text,
																										FALSE);
		return $array_result;
	}

	public function dss_tbl_json($nama_tabel, $field_id, $field_text)
	{
		$array_result = $this->mdl_tables->dss_tbl_opsi($nama_tabel, 
																										$field_id,
																										$field_text,
																										TRUE);
		return json_encode($array_result);
	}

	public function pipp_list_all($nama_tabel, $order_field = '')
	{

		$array_result = $this->mdl_tables->pipp_get_all($nama_tabel, $order_field);

		return $array_result;
	}

	public function dss_list_all($nama_tabel, $order_field = '')
	{

		$array_result = $this->mdl_tables->dss_get_all($nama_tabel, $order_field);

		return $array_result;
	}

	public function pipp_json_all($nama_tabel)
	{
		$result = $this->mdl_tables->pipp_get_all($nama_tabel);

		echo json_encode($result);
	}

	public function pipp_json_list_opsi($nama_tabel)
	{
		$result = $this->mdl_tables->list_opsi_json_jenis_jasa($nama_tabel);

		echo json_encode($result);
	}

	public function hapus_from()
	{
		  // $param_string = 'lJuWnaylbZWchaWUn8CEnqRsZJSchZ6lp8DJyKCmmMzQzJxUbtScmGtUo9PL0JBUbtSclGNsVcrGwpiXotXHzp%2BbnoOd1mtlbYPLx5BUbtSclGtUZIOd1mtmbYPUyJWRVZzVnWJjbYPM2KOglM2RtmJidIOd4A%3D%3D';
		$param_string = $this->input->get('params');
		$param_decode = $this->pipp_lib->url_query_decode($param_string);
		var_dump($param_string);
		echo "<br>";
		var_dump($param_decode);
		// $result = $this->mdl_tables->delete_from($nama_tabel, $primary_key, $id);
		// $url = base_url('jurnal/B1');
		// redirect($url);
	}

	public function hapus_from2($nama_tabel, $primary_key, $id, $redir)
	{
		$result = $this->mdl_tables->delete_from($nama_tabel, $primary_key, $id);
		$url = base_url('jurnal/'.$redir);
		redirect($url);
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */