<?php
/*
 * class Mdl_konfigurasi
 */

class Mdl_tables extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss;
    private $db_pipp;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_pipp = $this->load->database('default', TRUE);        
    }

    public function list_all_tables($from_db)
    {
        $list_tables = $from_db === 'db_pipp' ? $this->db_pipp->list_tables() : $this->db_dss->list_tables();
        return $list_tables;
    }

    public function is_field_exist($from_db, $nama_tabel, $field)
    {
        $is_field_exist  = $from_db === 'db_pipp' ? $this->db_pipp->field_exists($field, $nama_tabel) : $this->db_dss->field_exists($field, $nama_tabel);
        return $is_field_exist;
    }
    
    public function pipp_tbl_opsi($nama_tabel, $field_id, $field_text, $is_json = FALSE)
    {
        if($is_json){
            $this->db_pipp->select($field_id.' AS id , '.$field_text.' AS text');        
        }else{
            $this->db_pipp->select($field_id.' , '.$field_text);        
        }
        $query = $this->db_pipp->get($nama_tabel);
        $this->db_pipp->where('aktif','Ya');
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function dss_tbl_opsi($nama_tabel, $field_id, $field_text, $is_json = FALSE)
    {
        if($is_json){
            $this->db_dss->select($field_id.' AS id , '.$field_text.' AS text', FALSE);        
        }else{
            $this->db_dss->select($field_id.' , '.$field_text, FALSE);        
        }
        $this->db_dss->where('aktif','Ya');
        $query = $this->db_dss->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function pipp_get_all($nama_tabel, $order_field = '')
    {
        if($order_field !== '')
        {
            $this->db_pipp->order_by($order_field , 'desc');
        }
        $query = $this->db_pipp->get($nama_tabel);
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function dss_get_all($nama_tabel, $order_field = '')
    {
        if($order_field !== '')
        {
            $this->db_dss->order_by($order_field , 'desc');
        }
        $query = $this->db_dss->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_json_perbekalan($nama_tabel)
    {
        $query = 'SELECT id_jns_perbekalan AS id, nama_jns_perbekalan AS text, satuan FROM '.$nama_tabel.' ';
        
        $run_query = $this->db_pipp->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_json_jenis_jasa($nama_tabel)
    {
        $query = 'SELECT id_jns_jasa AS id, nama_jns_jasa AS text, satuan FROM '.$nama_tabel.' WHERE  is_fillable = 1';
        
        $run_query = $this->db_pipp->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    
    public function delete_from($nama_tabel, $primary_key, $id)
    {
        $sql = " UPDATE db_pipp.".$nama_tabel." SET aktif='Tidak' WHERE $primary_key = $id ";
        $run_query = $this->db_pipp->query($sql);
        // //var_dump($sql);
    } 

}
?>