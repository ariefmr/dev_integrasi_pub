          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title"> Berita PIPP Terkini </h3>
            </div>
            <div class="panel-body panel-body-berita">
                  <?php
                  if($list_berita !== FALSE) {
                    $counter_berita = 0;
                    foreach ($list_berita as $item) {
                      if($counter_berita < $limit_berita_terkini){
                  ?> 
                  <div class="media">
                      <div class="media-body">
                        <div class="media-heading" title="<?php echo $item->judul ?>">
                          <h3><?php echo kos(character_limiter($item->judul, 50), '...'); ?></h3>
                           <small>
                          <a href="<?php echo base_url('berita/view/'.$item->id_berita); ?>">
                          <?php echo ' '.kos($item->nama_pengguna,'---').' | '.$item->nama_pelabuhan.' | '.fmt_tgl($item->tanggal_buat).''; ?>
                          </a>
                         </small>
                        </div>                    
                        <?php echo character_limiter($item->body, 250); ?>
                      </div>
                  </div>
                  <?php
                      }
                    $counter_berita++;
                  }
                } 
                  ?>
            </div>
          </div>