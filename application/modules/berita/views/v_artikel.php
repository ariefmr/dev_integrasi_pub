<div class="row">
        <div class="col-lg-3">
        	<ul class="list-group">
        		<li class="list-group-item">
          			User: <?php echo $item_berita['full_name']; ?>
        		</li>
        		<li class="list-group-item">
          			Pelabuhan: <?php echo $nama_pelabuhan; ?>
        		</li>
      		</ul>
      		<ul class="list-group text-center">
      			<li class="list-group-item">
          			<a href="<?php echo base_url('berita/daftar'); ?>" class="btn btn-info"> Daftar Berita PIPP </a>
        		</li>
        		<li class="list-group-item">
          			<a href="<?php echo base_url('berita/entry'); ?>" class="btn btn-info"> Entry Berita PIPP </a>
        		</li>
      		</ul>
		</div>
		<div class="col-lg-9">
        	<div class="panel">
			  <div class="panel-heading">
			    <h3 class="panel-title">Berita PIPP</h3>
			  </div>
			  <div class="panel-body">
	
						  	<article id="post-<?php echo $item_berita['id_berita']; ?>" class="post-<?php echo $item_berita['id_berita']; ?> post type-post status-publish format-standard hentry category-releases tag-bootswatch tag-theme-options clearfix" role="article">
									
									<header>
									
										
										<div class="page-header"><h1 class="h2"><?php echo $item_berita['judul']; ?></h1>
<p class="meta">Dikirim pada  <time datetime="<?php echo $item_berita['tanggal_buat'] ?>" pubdate=""><?php echo $this->pipp_lib->fmt_tgl( $item_berita['tanggal_buat'] ); ?></time></p>
                                    
                                        </div>
										
										</header> <!-- end article header -->
								
									<section class="post_content clearfix ">
												
												<?php if( !empty($item_berita['gambar'] ) )
												{
												?>
												<p class="text-center">
												<img class="img-thumbnail" alt="140x140" style="width: auto; height: 20em;" src="<?php echo $paths['pipp_uploads'].'/'.$item_berita['gambar'];?>">
												</p>

												<?php
												}	
												?>
												<p>
												<?php
										                
										                    echo $item_berita['body'];
										        ?>
										        </p>

									</section> <!-- end article section -->
									
									<footer>
						              <p class="meta"> oleh <?php echo $item_berita['full_name']; ?>.</p>
                                    
									</footer> <!-- end article footer -->
								
								</article>
			  </div>
			</div>
		</div>
</div>