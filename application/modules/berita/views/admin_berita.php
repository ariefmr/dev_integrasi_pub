<div class="row">
        <div class="col-lg-3">
        	<ul class="list-group">
          		<li class="list-group-item">
          			User: <?php echo $username_pengguna; ?>
        		</li>
        		<li class="list-group-item">
          			Pelabuhan: <?php echo $nama_pelabuhan; ?>
        		</li>
      		</ul>
      		<ul class="list-group text-center">
        		<li class="list-group-item">
          			<a href="<?php echo base_url('berita/daftar'); ?>" class="btn btn-info"> Daftar Berita PIPP </a>
        		</li>
        		<li class="list-group-item">
          			<a href="<?php echo base_url('berita/entry'); ?>" class="btn btn-info"> Entry Berita PIPP </a>
        		</li>
      		</ul>
		</div>
		<div class="col-lg-9">
        	<div class="panel">
			  <div class="panel-heading">
			    <h3 class="panel-title">Berita PIPP</h3>
			  </div>
			  <div class="panel-body">
			  		<div class="row">
			  			<div class="col-lg-12">
				            <div class="btn-group">
				                <button type="button" class="btn btn-default" id="filter-info-selected" data-filter-selected="<?php echo $filter; ?>">
				                																<?php 		if($filter === 'new'){ echo $filter_info." ".$nama_pelabuhan;}else{echo $filter_info;} ?></button>
				                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				                  <span class="caret"></span>
				                </button>
				                <ul class="dropdown-menu" role="menu">
				                 <!--  <li><a href="<?php echo site_url(array('jurnal','h5','views','all',$tmp_tgl_catat)); ?>" title="">Entry Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
				                  <li><a href="<?php echo site_url(array('jurnal','h5','views','kembali',$tmp_tgl_catat)); ?>" title="">Kembali Ke Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
				                  <li><a href="<?php echo site_url(array('jurnal','h5','views','ke',$tmp_tgl_catat)); ?>">Ke Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
				               -->
				               	  <li><a href="#" class="filter-entry" data-set-filter="new">Berita Dari Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
				                  <li><a href="#" class="filter-entry" data-set-filter="all">Berita Dari Seluruh Pelabuhan PIPP</a></li>
				                  <li><a href="#" class="filter-entry" data-set-filter="lain">Berita Dari Pelabuhan Lain</a></li>
				               </ul>
				            </div>
			   							<button type="button" title="Klik Tombol Ini Untuk Merubah Filter Tanggal." id="ganti_tanggal" class="btn btn-default" data-filter-tanggal="<?php echo $tmp_tgl_catat; ?>">Sejak : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></button>
                                        <button type="button" id="start_filter" class="btn btn-success">Filter</button> 
                            			<input type="text" style="height: 0px; width:0px; border: 0px;" id="datepicker_jurnal">
                        </div>
                	</div>
                	<div class="row" id="pilih-pelabuhan">
                	  <div class="col-lg-6">
			            <?php
			                          $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_filter',
                                           'input_name' => 'id_pelabuhan_filter',
                                           'label_text' => '',
                                           'array_opsi' => '',
                                           'opsi_selected' => '0', 
                                           'input_width' => '',
                                           'input_class' => '',
                                           'label_class' => '',
                                           'input_placeholder' => 'Pilih Pelabuhan');
              						echo $this->mkform->pilih_pelabuhan_lama($attr_opsi_pelabuhan); 
			            ?>
			            </div>
                	</div>
                	
			  	<?php
			  		if($list_berita !== FALSE)
			  		{
			  	?>
			  	<div class="row">
			  		<div class="col-lg-12">
			  	<?php
			  		foreach ($list_berita as $item) {
			  	?>		
						  	<article id="post-<?php echo $item->id_berita ?>" class="post-<?php echo $item->id_berita ?> post type-post status-publish format-standard hentry category-releases tag-bootswatch tag-theme-options clearfix" role="article">
									
									<header>
									
										<a href="http://320press.com/wpbs/v2-1-released/" title="v2.1 Released"></a>
										
										<div class="page-header"><h1 class="h2"><?php echo $item->judul; ?></h1></div>
										
										<p class="meta">Dikirim pada  <time datetime="<?php echo $item->tanggal_buat ?>" pubdate=""><?php echo $this->pipp_lib->fmt_tgl( $item->tanggal_buat ); ?></time> oleh <?php echo $item->full_name; ?> (<?php echo $item->nama_pelabuhan; ?>) .</p>
									
									</header> <!-- end article header -->
								
									<section class="post_content clearfix">
												<?php
										                if (strlen($item->body) > 250)
										                {
										                    echo substr($item->body, 0, 250)."&#133;";
										                }
										                else
										                {
										                    echo $item->body;
										                }
										        ?>
									</section> <!-- end article section -->
									
									<footer>
						
                <p><a href="<?php echo base_url('berita/view/'.$item->id_berita); ?>">Lihat artikel »</a></p>
										
										
									</footer> <!-- end article footer -->
								
								</article>
				<?php				
			  		}
			  	?>
			  	</div>
			  	</div>
			  	<?php
			  	}else{			  	
			  	?>
			  	<div class="row">
			  	<hr>
			  		<div class="col-lg-12">
					  	<div class="well">
					  		<p> Tidak ada berita. </p>
					  	</div>
				  	</div>
			  	</div>
			  	<?php
			  	} 
			  	?>
			  </div>
			</div>
		</div>
</div>

<script>
var current_filter = function() { return $("#filter-info-selected").data('filterSelected');},
    array_uri = ['berita','daftar'],
    //array_pelabuhan_ori = <?php echo Modules::run('mst_pelabuhan/json_pelabuhan'); ?>,
    array_pelabuhan_new,
    id_pelabuhan_pilihan = <?php if( is_array($id_pelabuhan_banyak) ){ echo json_encode($id_pelabuhan_banyak); }else{ echo $id_pelabuhan_banyak; } ?>,
    get_id_pelabuhan_selected = function(){ return $("#id_pelabuhan_filter").select2("val"); };

    // function set_id_kosong_pelabuhan() 
    // {
    // 	var tmp_array = array_pelabuhan_ori;
    // 	tmp_array.reverse();
    // 	tmp_array.push({id: 0, text: 'Semua Pelabuhan'});
    // 	tmp_array.reverse();
    // 	return tmp_array;
    // }
	function start_filter()
	{
	  var new_segments = array_uri.join("/"),
	      new_url = site_url+new_segments+"/"+current_filter()+"/",
	      new_tanggal = $("#ganti_tanggal").data('filterTanggal');
	      url_redirect = new_url+new_tanggal,
	      pelabuhan_selected = get_id_pelabuhan_selected(),
	      boleh_open = true;
	      //console.log(pelabuhan_selected);
	      if(current_filter() === 'lain')
	      {
	      	if( pelabuhan_selected.length > 0)
	      	{
	      		url_redirect = url_redirect+"?id_pelabuhan_selected="+pelabuhan_selected.join("_");	
	      	}else{
	      		boleh_open = false;
	      	}
	      }
	      
	  if(boleh_open){
	  	window.open(url_redirect,'_self');
	  }
	}

	function listener_ganti_tanggal(tgl_selected)
	{   
	  var set_date = new Date(tgl_selected),
	      new_date = format_date_to_str(set_date);
	  $("#ganti_tanggal").data('filterTanggal', tgl_selected);    
	  $("#ganti_tanggal").text("Sejak : "+new_date);    
	}
  	$(document).ready(function() {

  		//array_pelabuhan_new = set_id_kosong_pelabuhan();
  		if(current_filter() === 'lain' && id_pelabuhan_pilihan.length > 0)
  		{	
  			$("#id_pelabuhan_filter").select2("val", id_pelabuhan_pilihan);
  		}else{
  			$("#pilih-pelabuhan").hide();
  		}
  		
        $("#datepicker_jurnal").datepicker({ 
                                                            maxDate: new Date(),
                                                            defaultDate: new Date(),
                                                            dateFormat: 'yy-mm-dd',
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                                                            onSelect: listener_ganti_tanggal
                                            });
    
        $("#ganti_tanggal").click(function() {
            $("#datepicker_jurnal").datepicker("show").datepicker("widget").show().position({
                                                        my: "right bottom",
                                                        at: "center bottom",
                                                        of: this 
                                                    });
        });

        $(".filter-entry").click(function(e){
            $("#filter-info-selected").data('filterSelected', $(this).data('setFilter'));
            $("#filter-info-selected").text( $(this).text() );
            if($(this).data('setFilter') === 'lain')
            {
            	$("#pilih-pelabuhan").show();
            }else{
            	$("#pilih-pelabuhan").hide();
            }
            e.stopPropagation();
            e.preventDefault();
            return false;
        });

        $("#start_filter").click(function(){
            start_filter();
        });
    });
</script>