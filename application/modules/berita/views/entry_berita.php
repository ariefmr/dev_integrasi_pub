<div class="row">
        <div class="col-lg-3">
        	<ul class="list-group">
          		<li class="list-group-item">
          			User: <?php echo $username_pengguna; ?>
        		</li>
        		<li class="list-group-item">
          			Pelabuhan: <?php echo $nama_pelabuhan; ?>
        		</li>
	      	</ul>
			<ul class="list-group text-center">
        		<li class="list-group-item">
          			<a href="<?php echo base_url('berita/daftar'); ?>" class="btn btn-info"> Daftar Berita PIPP </a>
        		</li>
        		<li class="list-group-item">
          			<a href="<?php echo base_url('berita/entry'); ?>" class="btn btn-info"> Entry Berita PIPP </a>
        		</li>
      		</ul>
		</div>
		<?php echo form_open_multipart('berita/input', 'id="form_entry" class="form-horizontal" role="form"'); ?>
		<div class="col-lg-9">
        	<div class="panel">
			  <div class="panel-heading">
			    <h3 class="panel-title">Berita PIPP</h3>
			  </div>
			  <div class="panel-body">
    			<script src="<?php echo $paths['misc_js'];?>/jquery.markitup.js"></script>
    			<link href="<?php echo $paths['misc_css'];?>/simple/style.css" rel="stylesheet">
			  	<div class="row">
				  	<?php  
				  		    $attr_datepick = array('input_id' => 'tgl_catat', 'input_name' => 'tgl_catat' , 'label_text' => 'Tanggal Catat :', 'input_value' => '', 'input_placeholder' => '',
						                     'input_type' => 'text', 'input_width' => 'hide', 'label_class' => 'hide', 'input_class' => 'form-control' );
						    echo $this->mkform->datepick($attr_datepick);  

				  			$attr_judul_berita = array('input_id' => 'judul',
	                              'input_name' => 'judul',
	                              'label_text' => 'Judul Berita <em>*</em> :',
	                              'input_value' => '',
	                              'input_placeholder' => 'Judul Berita',
	                              'input_type' => 'text',
	                              'input_width' => 'col-lg-10',
	                              'label_class' => 'col-lg-2',
	                              'input_class' => 'form-control' );
	        			echo $this->mkform->input($attr_judul_berita);
	         		?>
         		</div>
         		<div class="row">
	         		<div class="col-lg-2">
	         			<br>
	         			<br>
	         			<label for="body">Isi Berita <em>*</em> :</label>
         			</div>
	         		<div class="col-lg-8">
					  	<textarea id="editor_berita" name="body">
					  		
					  	</textarea>
				  	</div>
			  	</div>
			  	<div class="row">
			  	<?php     $gambar = array(
                                'input_id' => 'gambar', 
                                'input_name' => 'gambar' , 
                                'label_text' => 'Gambar :',
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'file', 
                                'input_width' => 'col-lg-4', 
                                'label_class' => 'col-lg-2', 
                                'input_class' => 'form-control' 
                                );
    						echo $this->mkform->input($gambar);
     			?>		
			  	</div>
			  </div>
			    <div class="panel-footer">
			        <h3 class="panel-title">Tanggal : <text id="info_tgl_catat_global">26-06-2013</text></h3>
			    </div>
			</div>
			  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
			    <button type="submit" class="btn btn-primary btn-lg btn-block">SIMPAN</button>
			  </div>
			  <?php echo form_close(); ?>
		</div>
</div>
<style>
.markItUp .markItUpButton1 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/bold.png);
}
.markItUp .markItUpButton2 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/italic.png);
}
.markItUp .markItUpButton3 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/stroke.png);
}

.markItUp .markItUpButton4 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/list-bullet.png); 
}
.markItUp .markItUpButton5 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/list-numeric.png);
}

.markItUp .markItUpButton6 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/link.png);
}

.markItUp .markItUpButton7 a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/clean.png);
}
.markItUp .preview a {
	background-image:url(<?php echo $paths['misc_css'];?>/sets/default/images/preview.png);
}
</style>
<script>
	
	var settings_editor_berita = {
			onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
			onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
			onTab:    		{keepDefault:false, replaceWith:'    '},
			markupSet:  [ 	
				{name:'Bold', key:'B', openWith:'(!(<strong>|!|<b>)!)', closeWith:'(!(</strong>|!|</b>)!)' },
				{name:'Italic', key:'I', openWith:'(!(<em>|!|<i>)!)', closeWith:'(!(</em>|!|</i>)!)'  },
				{name:'Stroke through', key:'S', openWith:'<del>', closeWith:'</del>' },
				{separator:'---------------' },
				{name:'Bulleted List', openWith:'    <li>', closeWith:'</li>', multiline:true, openBlockWith:'<ul>\n', closeBlockWith:'\n</ul>'},
				{name:'Numeric List', openWith:'    <li>', closeWith:'</li>', multiline:true, openBlockWith:'<ol>\n', closeBlockWith:'\n</ol>'},
				{separator:'---------------' },
				{name:'Link', key:'L', openWith:'<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', closeWith:'</a>', placeHolder:'Your text to link...' },
				{separator:'---------------' },
				{name:'Clean', className:'clean', replaceWith:function(markitup) { return markitup.selection.replace(/<(.*?)>/g, "") } },		
				{name:'Preview', className:'preview',  call:'preview'}
			]
	}

	$(document).ready(function() {
      $("#editor_berita").markItUp(settings_editor_berita);
   	});
</script>