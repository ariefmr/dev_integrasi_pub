<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();

			$this->load->library('entry/mkform');
			$this->load->model('mdl_berita');
			$this->load->model('admin/mdl_konfigurasi');
			//TODO: LOGIN CHECK HERE
			$data['paths'] = $this->config->item('assets_paths');
		}

	public function index()
	{
		$this->daftar();
	}

	public function view($id_berita)
	{
		$this->load->model('mst_pelabuhan/mdl_pelabuhan');

		$identifier = array('where' => 'id_berita', 'id' => $id_berita );
		$data['item_berita'] = (array) $this->mdl_berita->get_berita($identifier);
		
		$data['nama_pelabuhan'] = $this->mdl_pelabuhan->detail_pelabuhan($data['item_berita']['id_pelabuhan'], array('nama_pelabuhan'))->nama_pelabuhan;

		$data['page_title'] = $data['item_berita']['judul']."| PIPP | KKP";	
		$data['content_title'] = $data['item_berita']['judul'];
		$data['module'] = 'berita';
		$data['view_file'] = 'v_artikel';

		$data['list_konfigurasi'] = $this->mdl_konfigurasi->list_konfigurasi();

		
		echo Modules::run('templates/type/default_template', $data);	
	}

	public function wgt_berita_terkini($limit_berita = '3')
	{
		$this->load->helper('text');
		$tanggal_buat = date('Y-m-d H:i:s');
		$data['limit_berita_terkini']  = $limit_berita;

		$data['list_berita'] = $this->mdl_berita->list_berita('db_pipp','all', $tanggal_buat);

		$this->load->view('v_wgt_berita', $data);
	}

	public function daftar($filter= 'new',$tgl_catat = '')
	{	
		

		$data['additional_js'] = Array('select2.min.js');
		$data['additional_css'] = Array('select2.css');


		if($tgl_catat === ''){
				$tgl_catat = date('Y-m-d');
		}
		$data['id_pelabuhan_banyak'] = 0;
		$data['filter'] = $filter;
		switch ($filter) {
			case 'new' :
				$filter_info = 'Berita Dari Pelabuhan';
				$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
				break;
			case 'all' :
				$filter_info = 'Berita Dari Seluruh Pelabuhan PIPP';
				$id_pelabuhan = NULL;
				break;
			case 'lain' :
				$filter_info = 'Berita Dari Pelabuhan Lain';
				$id_pelabuhan = $this->input->get('id_pelabuhan_selected');
				$data['id_pelabuhan_banyak'] = explode("_",$id_pelabuhan);
				break;
		}

		$data['filter_info'] = $filter_info;

		$data['tmp_tgl_catat'] = $tgl_catat;
		$data['page_title'] = 'Index Berita PIPP';	
		$data['content_title'] = 'Index Berita PIPP';
		$data['module'] = 'berita';
		$data['view_file'] = 'admin_berita';

		$data['list_berita'] = $this->mdl_berita->list_berita('db_pipp',$filter, $tgl_catat, $id_pelabuhan);
		

		$data['list_konfigurasi'] = $this->mdl_konfigurasi->list_konfigurasi();

		
		echo Modules::run('templates/type/default_template', $data);
	}
	public function entry()
	{
		$data['page_title'] = 'Entry Berita PIPP';	
		$data['content_title'] = 'Entry Berita PIPP';
		$data['module'] = 'berita';
		$data['view_file'] = 'entry_berita';

		$data['list_konfigurasi'] = $this->mdl_konfigurasi->list_konfigurasi();

		
		echo Modules::run('templates/type/default_template', $data);
	} 

	public function input()
	{

		$this->load->model('mst_pelabuhan/mdl_pelabuhan');
		

		$id_pelabuhan_pengguna = $this->pipp_lib->id_pelabuhan_pengguna();
		$id_pengguna = $this->session->userdata('id_pengguna');
		$array_input = $this->input->post(NULL, TRUE);
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');
		$array_input['id_pengguna_buat'] = $id_pengguna;
		$array_input['id_pelabuhan'] = $id_pelabuhan_pengguna;
		unset( $array_input['tgl_catat'] );

		if( $_FILES['gambar']['error'] !== UPLOAD_ERR_NO_FILE)
		{
				/* Awal Program Upload */

				// Ambil nama pelabuhan untuk bikin nama file gambar yang diupload
				$nama_pelabuhan = $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_pengguna, array('nama_pelabuhan'))->nama_pelabuhan;
				$nama_pelabuhan = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_pelabuhan);
				$nama_pelabuhan = str_replace(" ", "_", $nama_pelabuhan);
				

				// Timestamp supaya nama file unique
				$getdate = new DateTime();
				$time_upload = $getdate->getTimestamp();

				// Persiapkan nama File, ini ga ada hubungan sama nama file asli
				// Sengaja hardcode 'balai_' karena memang gambar yang diupload tentang balai
				$nama_file_balai = 'berita_'.$nama_pelabuhan.'_'.$time_upload.'.jpg';
				//var_dump($nama_file_balai);

				// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				

				// Ambil path ke folder uploads yang diset di custom constants
				$assets_paths = $this->config->item('assets_paths');

				// Rules upload nya di set di custom constants
				$rules = $this->config->item('upload_rules');


				// Persiapkan config untuk upload
				$config['upload_path'] = $rules['upload_path'];  // Set path tempat file disimpan di server
				$config['file_name'] = $nama_file_balai; //  Set nama file yang bakal disimpan

				// Set rules ini dari custom constants
				$config['allowed_types'] = $rules['allowed_types']; 
				$config['max_size']	= $rules['max_size']; 
				$config['max_width']  = $rules['max_width'];
				$config['max_height']  = $rules['max_height'];

				// Set config 
				$this->load->library('upload', $config);

				/* PROSES UPLOAD SEBENERNYA DIJALANIN DI FUNGSI $this->upload->do_upload() :
				*  parameter nya di isi name dari input file di form entry sebelumnya
				*  Misal di form entry nya <input type=file name=gambar...
				*  Di sini masukin parameter nya: 'gambar'
				*/
				if ( ! $this->upload->do_upload('gambar') ) // Kondisi kalau upload gagal;
				{
					$info_error_upload = array('error' => $this->upload->display_errors());

					var_dump($info_error_upload);
					//$this->index(); 
					exit; // Gagal Upload Langsung Exit, record ga masuk 
					// PR: Bikin notifikasi gagal upload
				} 
				else // Kondisi upload berhasil
				{
					//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

					$array_input['gambar'] = $nama_file_balai;
				}
		}else{ // TANPA UPLOAD KOSONGKAN FIELD GAMBAR
			$array_input['gambar'] = '';
			


		}

		$this->mdl_berita->input($array_input);			
		$url = base_url('berita');
		redirect($url);	
	}
}
// TODO BETULIN KETERANGAN
// syncftp
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */