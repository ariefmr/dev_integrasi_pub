<?php
/*
 * class Mdl_konfigurasi
 */

class Mdl_Berita extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss;
    private $db_pipp;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_pipp = $this->load->database('default', TRUE);        
    }

    public function list_berita($from_db, $filter= 'new',$tgl_catat = '',$id_pelabuhan = '')
    {
        if($from_db === 'db_dss')
        {
            $query = $this->db_dss->get('mst_news');
        }elseif($from_db === 'db_pipp')
        {   
                $tanggal_buat = date("Y-m-d H:i:s", strtotime($tgl_catat));

               $sql = "SELECT db_pipp.mst_berita.*, db_master.mst_pengguna.full_name ,
                            db_master.mst_pengguna.nama_pengguna, 
                             db_master.mst_pelabuhan.nama_pelabuhan
                                FROM db_pipp.mst_berita
                                JOIN db_master.mst_pengguna ON db_pipp.mst_berita.id_pengguna_buat = db_master.mst_pengguna.id_pengguna
                                JOIN db_master.mst_pelabuhan ON db_pipp.mst_berita.id_pelabuhan = db_master.mst_pelabuhan.id_pelabuhan 
                                WHERE db_pipp.mst_berita.aktif = 'Ya' ";
            switch ($filter) {
                case 'new' :
                    $sql .= "AND db_pipp.mst_berita.id_pelabuhan = $id_pelabuhan " ;
                    $sql .= "AND db_pipp.mst_berita.tanggal_buat >= '$tanggal_buat' ";
                    break;
                case 'all' :
                    $sql .= "AND db_pipp.mst_berita.tanggal_buat >= ('$tanggal_buat' - INTERVAL 7 DAY) ";
                    break;
                case 'lain' :
                    $id_pelabuhan = str_replace("_", ",", $id_pelabuhan);
                    $sql .= "AND db_pipp.mst_berita.id_pelabuhan IN ($id_pelabuhan)  ";
                    $sql .= "AND db_pipp.mst_berita.tanggal_buat >= '$tanggal_buat' ";
                    break;
            }

             
            $sql .= "ORDER BY db_pipp.mst_berita.tanggal_buat DESC ";

            
            $query = $this->db_pipp->query($sql);

        }

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_berita($identifier)
    {

            $sql = "SELECT db_pipp.mst_berita.*, db_master.mst_pengguna.full_name , db_master.mst_pelabuhan.nama_pelabuhan
                    FROM db_pipp.mst_berita
                    JOIN db_master.mst_pengguna ON db_pipp.mst_berita.id_pengguna_buat = db_master.mst_pengguna.id_pengguna
                    JOIN db_master.mst_pelabuhan ON db_pipp.mst_berita.id_pelabuhan = db_master.mst_pelabuhan.id_pelabuhan
                    WHERE db_pipp.mst_berita.aktif = 'Ya'";
            if($identifier['where'] === 'id_pelabuhan')
            {
                $sql .= "AND db_pipp.mst_berita.id_pelabuhan = ".$identifier['id']." ";
                $sql .= "ORDER BY db_pipp.mst_berita.tanggal_buat DESC";
            }elseif($identifier['where'] === 'id_berita')
            {
                $sql .= "AND db_pipp.mst_berita.id_berita = ".$identifier['id']." ";
            }

            $query = $this->db_pipp->query($sql);

        if($query->num_rows() > 0){
            if($identifier['where'] === 'id_pelabuhan')
            {
                $result = $query->result();                
            }elseif($identifier['where'] === 'id_berita')
            {
                $result = $query->row();                
            }    
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_pipp->insert('mst_berita', $data);
    }

}
?>