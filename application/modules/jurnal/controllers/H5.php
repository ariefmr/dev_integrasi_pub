<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H5 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model(array('mdl_view_h5','mst_pelabuhan/mdl_pelabuhan'));
			//TODO: LOGIN CHECK HERE
		}

	public function index($filter = '',$tgl_aktivitas = '', $id_pelabuhan = '')
	{
		$this->views($filter,$tgl_aktivitas, $id_pelabuhan);
	}

	public function views($filter = '',$tgl_aktivitas = '', $id_pelabuhan = '' )
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		if(empty($tgl_aktivitas)){
				$tgl_aktivitas = empty($filter) ? date('Y-m-d', mktime(0,0,0, 1,1,2013)) : $tgl_aktivitas;
		}

		$filter = empty($filter) ? 'all' : $filter;
    $data['filter'] = $filter;

		switch ($filter) {
      case 'all' :
        $filter_info = 'Semua entry oleh / dari / ke Pelabuhan';
        break;
			case 'entry' :
				$filter_info = 'Entry 0leh Pelabuhan';
				break;
			case 'kembali' :
				$filter_info = ' Kembali ke Pelabuhan ';
				break;
			case 'ke' :
				$filter_info = ' Dari Pelabuhan lain ke Pelabuhan ';
				break;
		}
		$data['filter_info'] = $filter_info;

		$data['tmp_tgl_aktivitas'] = $tgl_aktivitas;
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/H5'),
									'Entry' => base_url('entry/H5'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h5'];
		

		$data['breadcrumbs'] = 'Produksi > Kapal Keluar > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h5'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'H5';

		$data['entry_status'] = 'NULL';
		$data['nama_kapal'] = 'NULL';
		if(isset($_GET['entry_status']) && isset($_GET['nama_kapal'])) {
		    $data['entry_status'] = $_GET['entry_status'];
		    $data['nama_kapal'] = $_GET['nama_kapal'];
		}

		
		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$list_H5 = $this->mdl_view_h5->list_per_tanggal($filter,$id_pelabuhan, $tgl_aktivitas);

    if($list_H5 !== FALSE)
    {
        foreach ($list_H5 as &$item) {
            $list_pelabuhan_tujuan = explode(",",$item->id_pelabuhan_tujuan);
            $arr_nama_pelabuhan_tujuan = array();
            foreach ($list_pelabuhan_tujuan as $idplb) {
              $nama_pelabuhan_tjn = kos($this->mdl_pelabuhan->detail_pelabuhan($idplb, array('nama_pelabuhan'))->nama_pelabuhan);
              array_push($arr_nama_pelabuhan_tujuan, $nama_pelabuhan_tjn);
            }
            $list_nama_pelabuhan_tujuan = implode(", ", $arr_nama_pelabuhan_tujuan);
            $item->list_nama_pelabuhan_tujuan = $list_nama_pelabuhan_tujuan;
        }
    }
    // var_dump($list_H5);
    
    $data['list_H5'] = $list_H5;

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

		echo Modules::run('templates/type/forms', $data);
	}

	public function json_detail_H5()
	{
		$id_aktivitas = $this->input->get('id_aktivitas', FALSE);
		$search_result = $this->mdl_view_h5->detail_aktivitas($id_aktivitas);
		$array_result = $search_result !== FALSE ? $search_result['aktivitas'] : Array('kosong');
		echo json_encode($array_result);
	}

	public function detail_H5($id_aktivitas)
	{
		//$id_aktivitas = $this->input->get('id_aktivitas', FALSE);
		$detail = $this->mdl_view_h5->detail_aktivitas($id_aktivitas);
		//var_dump($detail);
		if($detail !== FALSE )
		{
		
                  $nama_kapal = empty( $detail['aktivitas']->nama_kapal ) ? $detail['aktivitas']->nama_kapal_ref : $detail['aktivitas']->nama_kapal; 
		              $array_info_detail = array( 
                                              'Tanggal Catat : ' => array( 
                                                                      'id' => 'tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($detail['aktivitas']->tgl_catat) ),
                                              'Tanggal Keberangkatan :' => array( 
                                                                      'id' => 'tgl_aktivitas',
                                                                      'value' => $this->pipp_lib->fmt_tgl($detail['aktivitas']->tgl_aktivitas) ),
                                              'Pelabuhan Keberangkatan :' => array( 'id' => 'pelabuhan_keberangkatan',
                                                                  'value' => $detail['aktivitas']->nama_pelabuhan_berangkat),
                                              'Pelabuhan Tujuan :' => array( 'id' => 'pelabuhan_tujuan',
                                                                  'value' => $detail['aktivitas']->nama_pelabuhan_tujuan),
                                              'Nama Nahkoda :' => array( 'id' => 'nama_nahkoda_info',
                                                                  'value' => $detail['aktivitas']->nama_nahkoda),
                                          	  'Nama Kapal / SIPI : ' => array( 'id' => 'nama_kapal',
                                                                      'value' => $nama_kapal." / ".$detail['aktivitas']->no_sipi),
                                              'Tanda Selar : ' => array( 'id' => 'tanda_selar',
                                                                      'value' => $detail['aktivitas']->tanda_selar),
                                              'Pemilik :' => array( 'id' => 'nama_pemilik',
                                                                  'value' => $detail['aktivitas']->nama_penanggung_jawab),  
                                              'DPI :' => array( 'id' => 'dpi_info',
                                                                  'value' => $detail['aktivitas']->nama_dpi),                                              
                                              'Alat Tangkap :' => array( 'id' => 'alat_tangkap_info',
                                                                  'value' => $detail['aktivitas']->nama_alat_tangkap),                                      
                                              'Jumlah ABK :' => array( 'id' => 'jumlah_abk_info',
                                                                  'value' => $detail['aktivitas']->jumlah_abk),                                      
                                              'Tujuan Berangkat :' => array( 'id' => 'tujuan_berangkat_info',
                                                                  'value' => $this->pipp_lib->istilah_tujuan($detail['aktivitas']->tujuan_berangkat)),
                                             
                                           );

			echo '<div class="well well-sm panel-text-referensi">';
            echo $this->mkform->ref_text($array_info_detail);
      echo '</div>';

              	$tmpl = array ( 'table_open'  => '<table id="table_detail_H5" class="table table-bordered">' );
			    $this->table->set_template($tmpl);
			    $this->table->set_heading('No.', 'Jenis Perbekalan','Jumlah','Satuan');
              	$number = 1;
              if($detail['perbekalan'] !== FALSE)
              {
              	foreach ($detail['perbekalan'] as $item_perbekalan) {
              		$this->table->add_row($number.'. ', $item_perbekalan->nama_jns_perbekalan, $item_perbekalan->jml_perbekalan,$item_perbekalan->satuan);
              		$number++;
              	}
              	echo "<h3 class='text-center'>Data Perbekalan</h3>";
              	echo $this->table->generate();
              }else{
              	echo "<p class='heading text-center'>Tidak ada data perbekalan.</p>";
              }
        echo '<hr>';
        echo '<p class="text-center">
              <span class="badge" title="'.$detail['aktivitas']->id_pengguna.'">
              Operator : '.$detail['aktivitas']->nama_pengguna.' ( '.$detail['aktivitas']->nama_pelabuhan_entry.' )
              </span>
              </p>';
              
        }else{
        	echo "Data tidak ditemukan.";
        }
	}

	public function json_check_berangkat()
	{
		$id_kapal = $this->input->get('id_kapal', FALSE);
		$search_result = $this->mdl_view_h5->check_berangkat($id_kapal);
		$array_result = $search_result === FALSE ? Array('kosong') : $search_result;
		echo json_encode($array_result);
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */