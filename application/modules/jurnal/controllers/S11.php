<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S11 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_S11');
			//TODO: LOGIN CHECK HERE
		}

	public function index($tgl_catat = '0', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		if(empty($tgl_catat)){
				$tgl_catat = date('Y-m-d');
		}
		$data['tmp_tgl_catat'] = $tgl_catat;
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/S11'),
									'Entry' => base_url('entry/S11'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s11'];

		$data['breadcrumbs'] = 'Kelembagaan UPT Pelabuhan > View';
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s11'];
		$data['module'] = 'jurnal';
		$data['view_file'] = 'S11';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_S11'] = $this->mdl_view_S11->list_pegawai($id_pelabuhan);

		$data['detail_pegawai'] = FALSE;


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}

	public function detail($id_pegawai = '')
    {
    	if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		$id = $id_pegawai;

		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/S11'),
									'Entry' => base_url('entry/S11'),
									'Pencarian' => '#'
									);

		$data['link_terkait'] = Array(	
										'S11 - Kelembagaan UPT Pelabuhan' => base_url('jurnal/S11')
										);

		$data['breadcrumbs'] = 'Kelembagaan UPT Pelabuhan > View > Detail';
		
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s11_detail'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'S11';
        
        $this->load->model('admin/mdl_konfigurasi');
        $data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
        
        // $data['data_pegawai'] = $this->mdl_view_S11->data_per_pegawai($id_pegawai);
        // $data['id_pelabuhan'] = $id_pelabuhan;
        $data['detail_pegawai'] = $this->mdl_view_S11->detail_pegawai($id);
        // $data['detail_pendidikan'] = $this->mdl_view_S11->detail_pendidikan(1/*$id_pegawai*/);
        // $data['detail_kursus'] = $this->mdl_view_S11->detail_kursus(1/*$id_pegawai*/);
        
        $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
        
    }



}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */