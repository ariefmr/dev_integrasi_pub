<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S10A extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_s10a');
			//TODO: LOGIN CHECK HERE
		}

	public function index($tgl_catat = '0', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		if(empty($tgl_catat)){
				$tgl_catat = date('Y-m-d');
		}
		$data['tmp_tgl_catat'] = $tgl_catat;
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/S10A'),
									//'Entry' => base_url('entry/S10A'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s10'];

		$data['breadcrumbs'] = 'Data Lingkungan Fisik Pelabuhan > Geoteknik > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s10']['a'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'S10A';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_geoteknik'] = $this->mdl_view_s10a->list_geoteknik($id_pelabuhan);
		$data['list_hidro'] = $this->mdl_view_s10a->list_hidro($id_pelabuhan);
		$data['list_klimatologi'] = $this->mdl_view_s10a->list_klimatologi($id_pelabuhan);
		$data['list_sedimentasi'] = $this->mdl_view_s10a->list_sedimentasi($id_pelabuhan);
		$data['list_banjir'] = $this->mdl_view_s10a->list_banjir($id_pelabuhan);
		$data['list_tsunami'] = $this->mdl_view_s10a->list_tsunami($id_pelabuhan);


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}



}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */