<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H2 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_h2');
			//TODO: LOGIN CHECK HERE
		}

	public function index($filter = 'all',$tgl_aktivitas = '', $id_pelabuhan = '')
	{
			$this->views($filter,$tgl_aktivitas, $id_pelabuhan);
	}

	public function views($filter = 'all',$tahun = '', $bulan = '', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		if(empty($tahun) || empty($bulan)){
				$tahun = date('Y');
				$bulan = date('n');
		}

		$data['tmp_tgl_aktivitas'] = date($tahun.'-'.$bulan);
		$data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css','select2.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/H2'),
									'Entry' => base_url('entry/H2'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h2'];

		$data['breadcrumbs'] = 'Produksi > Produksi Dan Harga > View';
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h2'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'H2';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_h2'] = $this->mdl_view_h2->list_per_pelabuhan($id_pelabuhan, $tahun, $bulan);


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */