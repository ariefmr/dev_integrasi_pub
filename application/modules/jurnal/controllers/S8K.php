<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S8K extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_s8k');
			//TODO: LOGIN CHECK HERE
		}

	public function index($tgl_catat = '0', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		if(empty($tgl_catat)){
				$tgl_catat = date('Y-m-d');
		}
		$data['tmp_tgl_catat'] = $tgl_catat;
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/S8K'),
									'Entry' => base_url('entry/S8K'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s8'];

		$data['breadcrumbs'] = 'Fasilitas Fungsional > Rumah Genset > View';
		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['s8']['k'];
		$data['module'] = 'jurnal';
		$data['view_file'] = 'S8K';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_S8K'] = $this->mdl_view_s8k->list_per_tanggal($id_pelabuhan);

        $data_terbaru = $this->mdl_view_s8k->data_terbaru($id_pelabuhan);
        if (!$data_terbaru){
          $data['data_terbaru'] = FALSE;
        }else{
          $data['data_terbaru'] = (array) $data_terbaru;
        }

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}



}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */