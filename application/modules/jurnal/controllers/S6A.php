<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S6A extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mst_pelabuhan/mdl_pelabuhan');
			//TODO: LOGIN CHECK HERE
		}

	public function index( $id_pelabuhan = '')
	{
		$this->views($id_pelabuhan);
	}
	public function views($id_pelabuhan = '')
	{
		$tgl_catat = '';
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
			$data['is_admin'] = FALSE;
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
			$data['is_admin'] = TRUE;
		}
		// if ( empty($id_pelabuhan) )
		// {
		// 	$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		// }
		if(empty($tgl_catat)){
				$tgl_catat = date('Y-m-d');
		}
		$data['id_pelabuhan'] = $id_pelabuhan;
		$data['tmp_tgl_catat'] = $tgl_catat;
		$data['additional_js'] = Array('jquery.dataTables.min.js','select2.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css', 'select2.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/S6A'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['s6'];

		$data['breadcrumbs'] = 'Data Umum > Profil Pelabuhan > View';
		$titles = $this->config->item('form_titles');
		
		$data['page_title'] = $titles['s6']['a'];
		$data['module'] = 'jurnal';
		$data['view_file'] = 'S6A';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_S6A'] = $this->mdl_pelabuhan->list_per_pelabuhan($id_pelabuhan);

		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}



}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */