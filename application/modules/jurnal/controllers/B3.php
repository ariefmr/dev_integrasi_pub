<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class B3 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_b3');
			//TODO: LOGIN CHECK HERE
		}

	public function index($tahun = '', $bulan = '', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		if(empty($tahun) || empty($bulan)){
				$tahun = date('Y');
				$bulan = date('n');
		}

		$data['tmp_tgl_catat'] = date($tahun.'-'.$bulan);

		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/B3'),
									'Entry' => base_url('entry/B3'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['b3'];
										

		$data['breadcrumbs'] = 'Pemasaran > Keluar Pelabuhan > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['b3'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'B3';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');

		$data['list_B3'] = $this->mdl_view_b3->list_per_tanggal($id_pelabuhan, $tahun, $bulan);


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}
	
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */