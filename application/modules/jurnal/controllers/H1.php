<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class H1 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_h1');
			//TODO: LOGIN CHECK HERE
		}

	public function index($filter = 'all',$tgl_aktivitas = '', $id_pelabuhan = '')
	{
		$this->views($filter,$tgl_aktivitas, $id_pelabuhan);
	}

	public function views($filter = 'all',$tgl_aktivitas = '', $id_pelabuhan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}

		if(empty($tgl_aktivitas)){
				$tgl_aktivitas = date('Y-m-d');
		}

		$data['filter'] = $filter;
		switch ($filter) {
			case 'all' :
				$filter_info = 'Dari Pelabuhan Lain / Kembali Ke Pelabuhan';
				break;
			case 'kembali' :
				$filter_info = ' Kembali Ke Pelabuhan ';
				break;
			case 'lain' :
				$filter_info = ' Dari Pelabuhan Lain Ke ';
				break;
		}
		$data['filter_info'] = $filter_info;

		$data['tmp_tgl_aktivitas'] = $tgl_aktivitas;
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/H1'),
									'Entry' => base_url('entry/H1'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['h1'];

		

		$data['breadcrumbs'] = 'Produksi > Kapal Masuk > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['h1'];
		
		$data['module'] = 'jurnal';
		$data['view_file'] = 'H1';

		$data['entry_status'] = 'NULL';
		$data['nama_kapal'] = 'NULL';
		if(isset($_GET['entry_status']) && isset($_GET['nama_kapal'])) {
		    $data['entry_status'] = $_GET['entry_status'];
		    $data['nama_kapal'] = $_GET['nama_kapal'];
		}
		
		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_H1'] = $this->mdl_view_h1->list_per_tanggal($filter, $tgl_aktivitas, $id_pelabuhan);


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}

	public function detail_H1($id_aktivitas)
	{
		//$id_aktivitas = $this->input->get('id_aktivitas', FALSE);
		$detail = $this->mdl_view_h1->detail_aktivitas($id_aktivitas);
		if($detail !== FALSE )
		{
			if($detail['kunjungan'] !== FALSE)
			{
				    $list_kunjungan = array('is_tambat' => 'Tambat', 'is_labuh' => 'Labuh',
            						 'is_docking' => 'Docking', 'is_bongkar' => 'Bongkar Ikan',
            						 'is_perbaikan' => 'Repair', 'is_perbekalan' => 'Perbekalan');
		            $arr_kunjungan = array();
		            //var_dump($detail['kunjungan']);
		            foreach ( $list_kunjungan as $key => $istilah_tujuan) {
		            	//var_dump($istilah_tujuan, $detail['kunjungan'][$key]);
		            		if($detail['kunjungan'][$key] === "1")
		            		{
		            			array_push($arr_kunjungan, $istilah_tujuan);
		            		}
		            }
		            $text_kunjungan = implode(", ",$arr_kunjungan);
			}else{
				$text_kunjungan = 'Tidak ada data.';
			}
      $tgl_keberangkatan = empty($detail['aktivitas']->tgl_keberangkatan_ori) ? subdays($detail['aktivitas']->tgl_aktivitas, $detail['aktivitas']->jml_hari_trip ) : $detail['aktivitas']->tgl_keberangkatan_ori;
      $tujuan_berangkat_ori = empty($detail['aktivitas']->tujuan_keberangkatan_ori) ? '...' : $this->pipp_lib->istilah_tujuan($detail['aktivitas']->tujuan_berangkat_ori);


		              $array_info_detail = array( 
		              						  'Nama Kapal / SIPI : ' => array( 'id' => 'nama_kapal',
                                                                      'value' => $detail['aktivitas']->nama_kapal." / ".$detail['aktivitas']->no_sipi),
                                              'Tanda Selar : ' => array( 'id' => 'tanda_selar',
                                                                      'value' => $detail['aktivitas']->tanda_selar),
                                              'Pemilik :' => array( 'id' => 'nama_pemilik',
                                                                  'value' => $detail['aktivitas']->nama_penanggung_jawab),
                                              'Tanggal Keberangkatan :' => array( 
                                                                      'id' => 'tgl_aktivitas',
                                                                      'value' => fmt_tgl($tgl_keberangkatan) ),
                                              'Pelabuhan Keberangkatan :' => array( 'id' => 'pelabuhan_keberangkatan',
                                                                  'value' => $detail['aktivitas']->nama_pelabuhan_berangkat_ori),  
                                              'Tanggal Kedatangan : ' => array( 
                                                                      'id' => 'tgl_catat',
                                                                      'value' => fmt_tgl($detail['aktivitas']->tgl_aktivitas) ),
                                              'Pelabuhan Kedatangan :' => array( 'id' => 'pelabuhan_tujuan',
                                                                  'value' => $detail['aktivitas']->nama_pelabuhan_tujuan),
                                              'Jumlah Hari Trip :' => array( 'id' => 'jml_hari_trip',
                                                                  'value' => $detail['aktivitas']->jml_hari_trip." hari"),
                                              'Tujuan Berangkat :' => array( 'id' => 'tujuan_berangkat_info',
                                                                  'value' => $tujuan_berangkat_ori),
                                              'Nama Nahkoda :' => array( 'id' => 'nama_nahkoda_info',
                                                                  'value' => $detail['aktivitas']->nama_nahkoda),
                                              'DPI :' => array( 'id' => 'dpi_info',
                                                                  'value' => $detail['aktivitas']->nama_dpi),                                              
                                              'Alat Tangkap :' => array( 'id' => 'alat_tangkap_info',
                                                                  'value' => $detail['aktivitas']->nama_alat_tangkap),                                      
                                              'Jumlah ABK :' => array( 'id' => 'jumlah_abk_info',
                                                                  'value' => $detail['aktivitas']->jumlah_abk),                                      
                                              
                                              'Maksud kunjungan :' => array( 'id' => 'maksud_kunjungan_info',
                                                                  'value' => $text_kunjungan),

                                             
                                           );


			echo '<div class="well well-sm panel-text-referensi">';
            echo $this->mkform->ref_text($array_info_detail);
      echo '</div>';
            echo '<hr>';
        echo '<p class="text-center">
              <span class="badge" title="'.$detail['aktivitas']->id_pengguna.'"> Operator : '.$detail['aktivitas']->nama_pengguna.' ( '.$detail['aktivitas']->nama_pelabuhan_entry.' )</span>
              </p>';
            echo '</div>';



        }else{
        	var_dump($detail);
        }
	}

	public function json_detail_H1()
	{
		$id_aktivitas = $this->input->get('id_aktivitas', FALSE);
		$search_result = $this->mdl_view_h1->detail_aktivitas($id_aktivitas);
		$array_result = $search_result !== FALSE ? $search_result['aktivitas'] : Array('kosong');
		echo json_encode($array_result);
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */