<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class B1 extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: AriefMR
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_view_b1');
			//TODO: LOGIN CHECK HERE
		}

	public function index($tahun = '', $bulan = '')
	{
		if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
			$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
		}else{
			$id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
		}
		// if(empty($tgl_catat)){
		// 		$tgl_catat = date('Y-m-d');
		// }
		// $bulan_ini = date('m');
		// $tahun_ini = date('Y');
		// $data['tmp_tgl_catat'] = $tgl_catat;

		$tgl_catat = array( 'bulan' => $bulan ,
                              'tahun' => $tahun
                          );
		if(empty($bulan) && empty($tahun)){
				$tgl_catat = array( 'bulan' => date("m", time() ), 
                              'tahun' => date("Y", time() )
                          );
				$bulan_ini = date('m');
				$tahun_ini = date('Y');
		}else{
			$bulan_ini = $bulan;
			$tahun_ini = $tahun;
			// vdump($tahun_ini, true);
		}

		$data['tmp_tgl_catat'] = $tgl_catat['tahun'].'-'.$tgl_catat['bulan'];
		
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['link_daftar'] = Array(
									'View_active' => base_url('jurnal/B1'),
									'Entry' => base_url('entry/B1'),
									'Pencarian' => '#'
									);

		$terkait = $this->config->item('link_terkait');
		$data['link_terkait'] = $terkait['b1'];


		$data['breadcrumbs'] = 'Industri dan Jasa > Jasa Pelabuhan > View';

		$titles = $this->config->item('form_titles');
		$data['page_title'] = $titles['b1'];

		$data['module'] = 'jurnal';
		$data['view_file'] = 'B1';

		$this->load->model('admin/mdl_konfigurasi');

		$data['dev_mode'] = $this->mdl_konfigurasi->get_konfigurasi('dev_mode');
 	
		$data['list_B1'] = $this->mdl_view_b1->pendapatan_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini);
		
		// $data['list_B1'] = $this->mdl_view_b1->pendapatan_jasa_per_tahun($id_pelabuhan);

		$data['list_B1_non'] = $this->mdl_view_b1->pendapatan_non_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini);
		
		// $data['list_B1_non'] = $this->mdl_view_b1->pendapatan_non_jasa_per_tahun($id_pelabuhan);
		// $data['bulan_ini'] = $bulan_ini;


		$data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan);

echo Modules::run('templates/type/forms', $data);
	}
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */