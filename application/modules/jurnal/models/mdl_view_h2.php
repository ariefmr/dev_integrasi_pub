<?php
/*
 * class Mdl_H2
 */

class Mdl_view_h2 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_pelabuhan($id_pelabuhan, $tahun, $bulan)
    {

        $sql = "SELECT 
                db_master.mst_kapal.nama_kapal as nama_kapal_ref, 
                db_master.mst_alat_tangkap.nama_alat_tangkap,
                db_master.mst_pelabuhan.nama_pelabuhan,
                db_master.mst_dpi.nama_dpi,
                db_master.mst_jenis_ikan.nama_jenis_ikan,
                db_master.mst_jenis_ikan.nama_jenis_ikan_inggris,                
                db_master.mst_pengguna.nama_pengguna,
                db_pipp.mst_jenis_kondisi_ikan.nama_jenis_kondisi_ikan,
                db_pipp.trs_produksi.nama_kapal as nama_kapal_entry,                
                db_pipp.trs_produksi.*
                FROM db_pipp.trs_produksi 
                LEFT JOIN db_master.mst_kapal ON db_master.mst_kapal.id_kapal = db_pipp.trs_produksi.id_kapal
                LEFT JOIN db_master.mst_jenis_ikan ON db_master.mst_jenis_ikan.id_jenis_ikan = db_pipp.trs_produksi.id_jenis_ikan
                LEFT JOIN db_master.mst_alat_tangkap ON db_master.mst_alat_tangkap.id_alat_tangkap = db_pipp.trs_produksi.id_alat_tangkap
                LEFT JOIN db_master.mst_pelabuhan ON db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_produksi.id_pelabuhan
                LEFT JOIN db_master.mst_dpi ON db_master.mst_dpi.id_dpi = db_pipp.trs_produksi.id_dpi
                LEFT JOIN db_master.mst_pengguna ON db_master.mst_pengguna.id_pengguna = db_pipp.trs_produksi.id_pengguna_buat
                LEFT JOIN db_pipp.mst_jenis_kondisi_ikan ON db_pipp.mst_jenis_kondisi_ikan.id_jenis_kondisi_ikan = db_pipp.trs_produksi.id_jenis_kondisi_ikan
                WHERE db_pipp.trs_produksi.id_pelabuhan = $id_pelabuhan
                AND EXTRACT(MONTH FROM db_pipp.trs_produksi.tgl_aktivitas) = '$bulan'
                AND EXTRACT(YEAR FROM db_pipp.trs_produksi.tgl_aktivitas) = '$tahun'
                AND db_pipp.trs_produksi.filter_gt = 'above'
                AND db_pipp.trs_produksi.aktif = 'Ya'
            UNION
                SELECT 
                db_master.mst_kapal_daerah.nama_kapal as nama_kapal_ref, 
                db_master.mst_alat_tangkap.nama_alat_tangkap,
                db_master.mst_pelabuhan.nama_pelabuhan,
                db_master.mst_dpi.nama_dpi,
                db_master.mst_jenis_ikan.nama_jenis_ikan,
                db_master.mst_jenis_ikan.nama_jenis_ikan_inggris,                
                db_master.mst_pengguna.nama_pengguna,
                db_pipp.mst_jenis_kondisi_ikan.nama_jenis_kondisi_ikan, 
                db_pipp.trs_produksi.nama_kapal as nama_kapal_entry,
                db_pipp.trs_produksi.*
                FROM db_pipp.trs_produksi 
                LEFT JOIN db_master.mst_kapal_daerah ON db_master.mst_kapal_daerah.id_kapal_daerah = db_pipp.trs_produksi.id_kapal
                LEFT JOIN db_master.mst_jenis_ikan ON db_master.mst_jenis_ikan.id_jenis_ikan = db_pipp.trs_produksi.id_jenis_ikan
                LEFT JOIN db_master.mst_alat_tangkap ON db_master.mst_alat_tangkap.id_alat_tangkap = db_pipp.trs_produksi.id_alat_tangkap
                LEFT JOIN db_master.mst_pelabuhan ON db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_produksi.id_pelabuhan
                LEFT JOIN db_master.mst_dpi ON db_master.mst_dpi.id_dpi = db_pipp.trs_produksi.id_dpi
                LEFT JOIN db_master.mst_pengguna ON db_master.mst_pengguna.id_pengguna = db_pipp.trs_produksi.id_pengguna_buat
                LEFT JOIN db_pipp.mst_jenis_kondisi_ikan ON db_pipp.mst_jenis_kondisi_ikan.id_jenis_kondisi_ikan = db_pipp.trs_produksi.id_jenis_kondisi_ikan
                WHERE db_pipp.trs_produksi.id_pelabuhan = $id_pelabuhan
                AND EXTRACT(MONTH FROM db_pipp.trs_produksi.tgl_aktivitas) = '$bulan'
                AND EXTRACT(YEAR FROM db_pipp.trs_produksi.tgl_aktivitas) = '$tahun'
                AND db_pipp.trs_produksi.filter_gt = 'below'
                AND db_pipp.trs_produksi.aktif = 'Ya'
                ";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    // TODO data produksi  (View H2)
    // TODO delete produksi (Delete H2)
    // TODO edit produksi (Edit H2)
}