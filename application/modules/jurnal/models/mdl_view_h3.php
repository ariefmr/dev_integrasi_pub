<?php
/*
 * class Mdl_H3
 */

class Mdl_view_h3 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan, $tgl_catat)
    {

        $sql = "SELECT 
                db_master.mst_jenis_ikan.nama_jenis_ikan, 
                db_pipp.mst_jenis_kondisi_ikan.nama_jenis_kondisi_ikan,
                db_pipp.trs_pemasaran_masuk.id_pemasaran_masuk,
                db_pipp.trs_pemasaran_masuk.id_jenis_kondisi_ikan,
                db_pipp.trs_pemasaran_masuk.jml_ikan,
                db_pipp.trs_pemasaran_masuk.harga_ikan,
                db_pipp.trs_pemasaran_masuk.kota_asal,
                db_pipp.trs_pemasaran_masuk.jenis_transportasi
                FROM db_pipp.trs_pemasaran_masuk
                LEFT JOIN db_master.mst_jenis_ikan ON db_master.mst_jenis_ikan.id_jenis_ikan = db_pipp.trs_pemasaran_masuk.id_jenis_ikan
                LEFT JOIN db_pipp.mst_jenis_kondisi_ikan ON db_pipp.mst_jenis_kondisi_ikan.id_jenis_kondisi_ikan = db_pipp.trs_pemasaran_masuk.id_jenis_kondisi_ikan
                WHERE db_pipp.trs_pemasaran_masuk.tgl_catat = '$tgl_catat'
                AND db_pipp.trs_pemasaran_masuk.id_pelabuhan = $id_pelabuhan
                AND db_pipp.trs_pemasaran_masuk.aktif = 'Ya'";
    	
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}