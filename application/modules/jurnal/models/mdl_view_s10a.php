<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s10a extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_geoteknik($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT mst_geoteknik.id_geoteknik, mst_geoteknik.dalam_tanah_darat, mst_geoteknik.dalam_tanah_dasar, mst_geoteknik.jns_tanah_darat,
                            mst_geoteknik.jns_tanah_dasar, mst_geoteknik.zona_gempa, mst_gempa.tahun_gempa, mst_gempa.desk_gempa,
                            mst_gempa.sumber_data as sd_gempa, mst_geoteknik.sumber_data, mst_geoteknik.tgl_catat
                            from mst_geoteknik left join mst_gempa on mst_geoteknik.id_pelabuhan=mst_gempa.id_pelabuhan
                            where mst_geoteknik.id_pelabuhan=$id_pelabuhan 
                                AND mst_geoteknik.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_hidro($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_hidrooseanografi.id_hidrooseanografi, mst_hidrooseanografi.tgl_catat, mst_hidrooseanografi.tinggi_gel_max, mst_hidrooseanografi.bulan_gel_max,
                            mst_hidrooseanografi.desk_gel, mst_hidrooseanografi.kec_arus_max, mst_hidrooseanografi.sumber_data
                            from mst_hidrooseanografi
                            where mst_hidrooseanografi.id_pelabuhan=$id_pelabuhan
                            AND mst_hidrooseanografi.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_klimatologi($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_klimatologi.id_klimatologi, mst_klimatologi.tgl_catat, mst_klimatologi.curah_hujan_max, mst_klimatologi.hujan_bulanan_avg, mst_klimatologi.lembab_udara_avg,
                            mst_klimatologi.suhu_udara_avg, mst_klimatologi.bulan_angin_besar, mst_klimatologi.kec_angin_max,
                            mst_klimatologi.arah_angin, mst_klimatologi.sumber_data
                            from mst_klimatologi
                            where mst_klimatologi.id_pelabuhan=$id_pelabuhan
                            AND mst_klimatologi.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_sedimentasi($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_sedimentasi.id_sedimentasi, mst_sedimentasi.tgl_catat, mst_sedimentasi.tk_sed_alur, mst_sedimentasi.sumber_sed_alur, mst_sedimentasi.tk_sed_kolam,
                            mst_sedimentasi.sumber_sed_kolam, mst_sedimentasi.sumber_data
                            from mst_sedimentasi
                            where mst_sedimentasi.id_pelabuhan=$id_pelabuhan
                            AND mst_sedimentasi.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_banjir($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_banjir.id_banjir, mst_banjir.tgl_catat, mst_banjir.tinggi_banjir, mst_banjir.tahun_banjir, mst_banjir.sumber_data
                            from mst_banjir
                            where mst_banjir.id_pelabuhan=$id_pelabuhan
                            AND mst_banjir.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_tsunami($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_tsunami.id_tsunami, mst_tsunami.tgl_catat, mst_tsunami.tinggi_air, mst_tsunami.tahun_tsunami, mst_tsunami.sumber_data
                            from mst_tsunami
                            where mst_tsunami.id_pelabuhan=$id_pelabuhan
                            AND mst_tsunami.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}