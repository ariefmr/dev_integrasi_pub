<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s6a extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT * , db_master.mst_wpp.nama_wpp
                FROM db_pipp.mst_pelabuhan_baru
                LEFT JOIN db_master.mst_wpp ON db_master.mst_wpp.id_wpp = db_pipp.mst_pelabuhan_baru.id_wpp
                WHERE db_pipp.mst_pelabuhan_baru.id_pelabuhan = $id_pelabuhan ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_pelabuhan', $id_record);
        $query = $this->db->get('mst_pelabuhan_baru');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}