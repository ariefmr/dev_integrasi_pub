<?php
/*
 * class Mdl_B3
 */

class Mdl_view_b3 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan, $tahun, $bulan)
    {

        $sql = "SELECT 
                    db_master.mst_jenis_ikan.nama_jenis_ikan,
                    db_master.mst_pengguna.nama_pengguna,
                    db_pipp.mst_jenis_kondisi_ikan.nama_jenis_kondisi_ikan,
                    db_pipp.mst_kateg_tujuan.nama_kateg_tujuan,
                    db_pipp.trs_pemasaran_keluar.id_pemasaran_keluar,
                    db_pipp.trs_pemasaran_keluar.id_jenis_kondisi_ikan,
                    db_pipp.trs_pemasaran_keluar.id_kateg_tujuan,                    
                    db_pipp.trs_pemasaran_keluar.jml_ikan,
                    db_pipp.trs_pemasaran_keluar.harga_ikan,
                    db_pipp.trs_pemasaran_keluar.kota_tujuan,
                    db_pipp.trs_pemasaran_keluar.jenis_transportasi,
                    db_pipp.trs_pemasaran_keluar.tgl_catat                    
                FROM
                    db_pipp.trs_pemasaran_keluar
                        LEFT JOIN
                    db_master.mst_jenis_ikan ON db_master.mst_jenis_ikan.id_jenis_ikan = db_pipp.trs_pemasaran_keluar.id_jenis_ikan
                        LEFT JOIN
                    db_pipp.mst_jenis_kondisi_ikan ON db_pipp.mst_jenis_kondisi_ikan.id_jenis_kondisi_ikan = db_pipp.trs_pemasaran_keluar.id_jenis_kondisi_ikan
                        LEFT JOIN
                    db_pipp.mst_kateg_tujuan ON db_pipp.mst_kateg_tujuan.id_kateg_tujuan = db_pipp.trs_pemasaran_keluar.id_kateg_tujuan
                        LEFT JOIN
                    db_master.mst_pengguna ON db_master.mst_pengguna.id_pengguna = db_pipp.trs_pemasaran_keluar.id_pengguna_buat
                WHERE db_pipp.trs_pemasaran_keluar.id_pelabuhan = $id_pelabuhan
                    AND EXTRACT(MONTH FROM db_pipp.trs_pemasaran_keluar.tgl_catat) = '$bulan'
                    AND EXTRACT(YEAR FROM db_pipp.trs_pemasaran_keluar.tgl_catat) = '$tahun'
                    AND db_pipp.trs_pemasaran_keluar.aktif = 'Ya'
                ORDER BY db_pipp.trs_pemasaran_keluar.tgl_catat ASC";
    	
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }


    // TODO data produksi  (View B3)
    // TODO delete produksi (Delete B3)
    // TODO edit produksi (Edit B3)
}