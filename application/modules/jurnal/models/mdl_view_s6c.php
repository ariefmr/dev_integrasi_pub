<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s6c extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT db_pipp.mst_akses_pelabuhan.id_akses_pelabuhan, db_pipp.mst_akses_pelabuhan.tgl_catat, db_pipp.mst_jenis_tujuan.nama_jns_tujuan, db_pipp.mst_akses_pelabuhan.nama_tujuan,
                            db_pipp.mst_akses_pelabuhan.jarak, db_pipp.mst_akses_pelabuhan.rute, db_pipp.mst_akses_pelabuhan.kond_rute, db_pipp.mst_akses_pelabuhan.id_jns_tujuan
                            FROM db_pipp.mst_akses_pelabuhan, db_pipp.mst_jenis_tujuan
                            WHERE db_pipp.mst_akses_pelabuhan.id_pelabuhan=$id_pelabuhan AND
                            db_pipp.mst_akses_pelabuhan.id_jns_tujuan = db_pipp.mst_jenis_tujuan.id_jns_tujuan AND
                            db_pipp.mst_akses_pelabuhan.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_akses_pelabuhan', $id_record);
        $query = $this->db->get('mst_akses_pelabuhan');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}