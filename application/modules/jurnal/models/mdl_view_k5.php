<?php
/*
 * class Mdl_H3
 */

class Mdl_view_k5 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT * 
                            FROM db_pipp.trs_k5 
                            WHERE id_pelabuhan = $id_pelabuhan
                            and trs_k5.aktif = 'Ya' 
                                order by tipe ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_k5', $id_record);
        $query = $this->db->get('trs_k5');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }



    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT trs_k5.tgl_catat, trs_k5.id_k5, trs_k5.tahun, trs_k5.ket_rehab,
                                trs_k5.jns_konstruksi, trs_k5.panjang, trs_k5.lebar,
                                trs_k5.elevasi, trs_k5.ket_konstruksi, trs_k5.sumber_dana,
                                trs_k5.nilai, trs_k5.ket_sumber_dana, trs_k5.id_row, trs_k5.gambar,
                                mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                from trs_k5, mst_jenis_ket_konstruksi
                                where trs_k5.id_pelabuhan=$id_pelabuhan
                                and mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=trs_k5.ket_konstruksi
                                and trs_k5.aktif = 'Ya' 
                                order by db_pipp.trs_k5.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}