<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s8e extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_sumber_air.*, db_pipp.mst_jenis_sumber_air.nama_jns_sumber_air,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_sumber_air, db_pipp.mst_jenis_sumber_air, db_pipp.mst_jenis_ket_konstruksi
                                WHERE   db_pipp.mst_sumber_air.id_pelabuhan=$id_pelabuhan and
                                        db_pipp.mst_sumber_air.id_jns_sumber_air = db_pipp.mst_jenis_sumber_air.id_jns_sumber_air and
                                        db_pipp.mst_sumber_air.id_jns_sumber_air = '4' 
                                and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_sumber_air.ket_konstruksi
                                        and db_pipp.mst_sumber_air.aktif = 'Ya' ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_sumber_air', $id_record);
        $query = $this->db->get('mst_sumber_air');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_sumber_air.*, db_pipp.mst_jenis_sumber_air.nama_jns_sumber_air,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_sumber_air, db_pipp.mst_jenis_sumber_air, db_pipp.mst_jenis_ket_konstruksi
                                WHERE   db_pipp.mst_sumber_air.id_pelabuhan=$id_pelabuhan and
                                        db_pipp.mst_sumber_air.id_jns_sumber_air = db_pipp.mst_jenis_sumber_air.id_jns_sumber_air and
                                        db_pipp.mst_sumber_air.id_jns_sumber_air = '4' 
                                and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_sumber_air.ket_konstruksi
                                        and db_pipp.mst_sumber_air.aktif = 'Ya'
                                order by db_pipp.mst_sumber_air.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}