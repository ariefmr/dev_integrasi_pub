<?php

class Mdl_view_s6b extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT db_pipp.mst_luas_pelabuhan.*
                            FROM db_pipp.mst_luas_pelabuhan
                            LEFT JOIN db_pipp.mst_pelabuhan_baru ON db_pipp.mst_pelabuhan_baru.id_pelabuhan = db_pipp.mst_luas_pelabuhan.id_pelabuhan
                            WHERE db_pipp.mst_luas_pelabuhan.id_pelabuhan = $id_pelabuhan
                            AND db_pipp.mst_luas_pelabuhan.aktif = 'Ya'
                            order by  db_pipp.mst_luas_pelabuhan.id_luas_pelabuhan desc";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_luas_pelabuhan', $id_record);
        $query = $this->db->get('mst_luas_pelabuhan');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_data_terakhir($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT db_pipp.mst_luas_pelabuhan.*
                            FROM db_pipp.mst_luas_pelabuhan
                            LEFT JOIN db_pipp.mst_pelabuhan_baru ON db_pipp.mst_pelabuhan_baru.id_pelabuhan = db_pipp.mst_luas_pelabuhan.id_pelabuhan
                            WHERE db_pipp.mst_luas_pelabuhan.id_pelabuhan = $id_pelabuhan 
                            AND db_pipp.mst_luas_pelabuhan.aktif = 'Ya'
                            order by mst_luas_pelabuhan.id_luas_pelabuhan desc
                            limit 1";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}