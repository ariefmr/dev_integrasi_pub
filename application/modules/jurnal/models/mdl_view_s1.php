<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT db_pipp.mst_industri.*, db_pipp.mst_jenis_badan_usaha.nama_jns_badan_usaha
                                from db_pipp.mst_industri, db_pipp.mst_jenis_badan_usaha
                                where db_pipp.mst_industri.id_pelabuhan=$id_pelabuhan and
                                db_pipp.mst_industri.id_jns_badan_usaha=db_pipp.mst_jenis_badan_usaha.id_jns_badan_usaha and
                                db_pipp.mst_industri.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}