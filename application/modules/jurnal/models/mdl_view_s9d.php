<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s9d extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_fas_umum.* , db_pipp.mst_jenis_fas_umum.nama_jns_fas_umum,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_fas_umum, db_pipp.mst_jenis_fas_umum, db_pipp.mst_jenis_ket_konstruksi
                                WHERE db_pipp.mst_fas_umum.id_pelabuhan=$id_pelabuhan 
                                and db_pipp.mst_fas_umum.id_jns_fas_umum = db_pipp.mst_jenis_fas_umum.id_jns_fas_umum 
                                and db_pipp.mst_fas_umum.id_jns_fas_umum = 7
                                and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_fas_umum.ket_konstruksi
                                and db_pipp.mst_fas_umum.aktif = 'Ya'";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_fas_umum.* , db_pipp.mst_jenis_fas_umum.nama_jns_fas_umum,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_fas_umum, db_pipp.mst_jenis_fas_umum, db_pipp.mst_jenis_ket_konstruksi
                                WHERE db_pipp.mst_fas_umum.id_pelabuhan=$id_pelabuhan 
                                and db_pipp.mst_fas_umum.id_jns_fas_umum = db_pipp.mst_jenis_fas_umum.id_jns_fas_umum 
                                and db_pipp.mst_fas_umum.id_jns_fas_umum = 7
                                and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_fas_umum.ket_konstruksi
                                and db_pipp.mst_fas_umum.aktif = 'Ya'
                                order by db_pipp.mst_fas_umum.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_fas_umum', $id_record);
        $query = $this->db->get('mst_fas_umum');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}