<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s9b extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);

    }

    public function list_per_tanggal($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_fas_pengelola.* , db_pipp.mst_jenis_fas_pengelola.nama_jns_fas_pengelola,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_fas_pengelola, db_pipp.mst_jenis_fas_pengelola, db_pipp.mst_jenis_ket_konstruksi
                                WHERE db_pipp.mst_fas_pengelola.id_pelabuhan=$id_pelabuhan and 
                                db_pipp.mst_fas_pengelola.id_jns_fas_pengelola = db_pipp.mst_jenis_fas_pengelola.id_jns_fas_pengelola
                                and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_fas_pengelola.ket_konstruksi
                                and db_pipp.mst_fas_pengelola.aktif = 'Ya' ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_fas_pengelola.* , db_pipp.mst_jenis_fas_pengelola.nama_jns_fas_pengelola,
                                db_pipp.mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                FROM db_pipp.mst_fas_pengelola, 
                                    db_pipp.mst_jenis_fas_pengelola, 
                                    db_pipp.mst_jenis_ket_konstruksi
                                WHERE db_pipp.mst_fas_pengelola.id_pelabuhan=$id_pelabuhan 
                                    and db_pipp.mst_fas_pengelola.id_jns_fas_pengelola = db_pipp.mst_jenis_fas_pengelola.id_jns_fas_pengelola
                                    and db_pipp.mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=db_pipp.mst_fas_pengelola.ket_konstruksi
                                    and db_pipp.mst_fas_pengelola.aktif = 'Ya'
                                order by db_pipp.mst_fas_pengelola.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru_spesifik($id_pelabuhan, $id_jns_fas_pengelola)
    {
        // die;
        $this->db_pipp->select(' mst_fas_pengelola.* , 
                            mst_jenis_fas_pengelola.nama_jns_fas_pengelola,
                            mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi');
        $this->db_pipp->from('mst_fas_pengelola');
        $this->db_pipp->join('mst_jenis_fas_pengelola',
                        'mst_fas_pengelola.id_jns_fas_pengelola = mst_jenis_fas_pengelola.id_jns_fas_pengelola',
                        'inner');
        $this->db_pipp->join('mst_jenis_ket_konstruksi',
                        'mst_fas_pengelola.ket_konstruksi = mst_jenis_ket_konstruksi.id_jns_ket_konstruksi',
                        'left');
        $this->db_pipp->where('mst_fas_pengelola.id_pelabuhan', $id_pelabuhan);
        $this->db_pipp->where('mst_fas_pengelola.aktif', 'Ya');
        $this->db_pipp->where('mst_fas_pengelola.id_jns_fas_pengelola', $id_jns_fas_pengelola);
        $this->db_pipp->order_by('mst_fas_pengelola.tahun', 'desc');
        $this->db_pipp->limit(1);

        $query_aktivitas = $this->db_pipp->get();                                
        // echo $this->db_pipp->last_query();
        // die;

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_fas_pengelola', $id_record);
        $query = $this->db->get('mst_fas_pengelola');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}