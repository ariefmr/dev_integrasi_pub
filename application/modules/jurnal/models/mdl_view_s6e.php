<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s6e extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT db_pipp.mst_amenities.id_amenities, db_pipp.mst_amenities.tgl_catat, db_pipp.mst_jenis_amenities.nama_jns_amenities, db_pipp.mst_amenities.nama_amenities,
                            db_pipp.mst_amenities.jarak, db_pipp.mst_amenities.ket
                            from db_pipp.mst_amenities, db_pipp.mst_jenis_amenities
                            where db_pipp.mst_amenities.id_pelabuhan='$id_pelabuhan' and
                            db_pipp.mst_amenities.id_jns_amenities=db_pipp.mst_jenis_amenities.id_jns_amenities and
                            db_pipp.mst_amenities.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_amenities', $id_record);
        $query = $this->db->get('mst_amenities');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}