<?php
/*
 * class Mdl_H1
 */

class Mdl_view_h1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($filter, $tgl_aktivitas, $id_pelabuhan)
    {
        switch ($filter) {
            case 'all':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,

                                db_pipp.trs_aktivitas_kapal.*,
                                db_pipp.trs_maksud_kunjungan.*

                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas >= '$tgl_aktivitas'      
                        UNION
                            SELECT  db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_entry,
                            db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,

                                db_pipp.trs_aktivitas_kapal.*,
                                    db_pipp.trs_maksud_kunjungan.*

                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal_daerah, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas >= '$tgl_aktivitas' ";
                break;
            // case 'entry':
            //     $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal,
            //                         db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
            //                         db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
            //                         db_pipp.trs_aktivitas_kapal.*,
            //                         db_pipp.trs_maksud_kunjungan.*
            //                 FROM db_pipp.trs_aktivitas_kapal
            //                 LEFT JOIN (db_master.mst_kapal, db_master.mst_pelabuhan)
            //                 ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
            //                     AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
            //                 LEFT JOIN db_pipp.trs_maksud_kunjungan
            //                 ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
            //                 WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
            //                     AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
            //                     AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
            //                     AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
            //                     AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
            //                 UNION
            //                 SELECT  db_master.mst_kapal_daerah.nama_kapal,
            //                         db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
            //                         db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
            //                         db_pipp.trs_aktivitas_kapal.*,
            //                         db_pipp.trs_maksud_kunjungan.*
            //                 FROM db_pipp.trs_aktivitas_kapal
            //                 LEFT JOIN (db_master.mst_kapal_daerah, db_master.mst_pelabuhan)
            //                 ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
            //                     AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
            //                 LEFT JOIN db_pipp.trs_maksud_kunjungan
            //                 ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
            //                 WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
            //                     AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
            //                     AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
            //                     AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
            //                     AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'   ";
            //     break;
            case 'kembali':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                    db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
                                    db_pipp.trs_aktivitas_kapal.*,
                                    db_pipp.trs_maksud_kunjungan.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                            UNION
                            SELECT  db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_entry,
                                    db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
                                    db_pipp.trs_aktivitas_kapal.*,
                                    db_pipp.trs_maksud_kunjungan.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal_daerah, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya' ";
                break;
            case 'lain':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                    db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
                                    db_pipp.trs_aktivitas_kapal.*,
                                    db_pipp.trs_maksud_kunjungan.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal != $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya' 
                            UNION
                            SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                    db_master.mst_pelabuhan.nama_pelabuhan as nama_pelabuhan_berangkat,
                                    db_pipp.trs_aktivitas_kapal.tgl_aktivitas as tgl_kedatangan,
                                    db_pipp.trs_aktivitas_kapal.*,
                                    db_pipp.trs_maksud_kunjungan.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN (db_master.mst_kapal_daerah, db_master.mst_pelabuhan)
                            ON (db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = db_master.mst_pelabuhan.id_pelabuhan)
                            LEFT JOIN db_pipp.trs_maksud_kunjungan
                            ON db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = db_pipp.trs_maksud_kunjungan.id_aktivitas_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal != $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.aktivitas = 'masuk'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'";
                break;
        }

        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_aktivitas($id_aktivitas)
    {
        $this->db->select('filter_gt');
        $this->db->select('id_aktivitas_referensi');
        $info_aktivitas = $this->db->get_where('trs_aktivitas_kapal', array('id_aktivitas_kapal' => $id_aktivitas));
        $filter_gt = $info_aktivitas->row()->filter_gt;
        $id_aktivitas_referensi = $info_aktivitas->row()->id_aktivitas_referensi;

        if($filter_gt === 'above')
        {
            $sql_aktivitas = "SELECT plb_tujuan.nama_pelabuhan_tujuan, 
                            plb.nama_pelabuhan as nama_pelabuhan_entry,  ";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .= "
                            trs_ori.tgl_keberangkatan_ori,
                            trs_ori.tujuan_berangkat_ori,
                            trs_ori.id_pelabuhan_berangkat_ori,
                            trs_ori.nama_pelabuhan_berangkat_ori,
                             ";
            }else{
            $sql_aktivitas .= "
                            plb_berangkat.id_pelabuhan_berangkat_ori,
                            plb_berangkat.nama_pelabuhan_berangkat_ori,
                             ";   
            }
            $sql_aktivitas .= "
                            kpl.nama_kapal,
                            kpl.tanda_selar, 
                            att.nama_alat_tangkap,
                            izn.no_sipi,
                            prs.nama_penanggung_jawab,
                            mpg.nama_pengguna,
                            mpg.id_pengguna, 
                            dpi.nama_dpi,
                            av.* 
                            FROM db_pipp.trs_aktivitas_kapal av";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .= "                           
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan,db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_tujuan 
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan) plb_tujuan 
                            ON plb_tujuan.id_pelabuhan = av.id_pelabuhan";
            }else{
            $sql_aktivitas .= "                           
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan,db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_tujuan 
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_masuk) plb_tujuan 
                            ON plb_tujuan.id_pelabuhan = av.id_pelabuhan
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan AS id_pelabuhan_berangkat_ori, db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_berangkat_ori
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal) plb_berangkat
                            ON plb_berangkat.id_pelabuhan_berangkat_ori = av.id_pelabuhan";
            }
            $sql_aktivitas .= "                
                            LEFT OUTER JOIN db_master.mst_alat_tangkap att
                                ON att.id_alat_tangkap = av.id_alat_tangkap 
                            LEFT OUTER JOIN db_master.mst_kapal kpl
                                ON kpl.id_kapal = av.id_kapal
                            LEFT OUTER JOIN db_master.mst_dpi dpi
                                ON dpi.id_dpi = av.id_dpi
                            JOIN db_master.mst_izin izn
                                ON kpl.id_kapal = izn.id_kapal
                            JOIN db_master.mst_pengguna mpg
                                ON mpg.id_pengguna = av.id_pengguna_buat 
                            JOIN db_master.mst_pelabuhan plb
                                ON plb.id_pelabuhan = av.id_pelabuhan
                            JOIN db_master.mst_perusahaan prs
                                ON kpl.id_perusahaan = prs.id_perusahaan";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .=    
                            "JOIN (SELECT    db_pipp.trs_aktivitas_kapal.tgl_aktivitas AS tgl_keberangkatan_ori,
                                            db_pipp.trs_aktivitas_kapal.tujuan_berangkat AS tujuan_berangkat_ori, 
                                            db_pipp.trs_aktivitas_kapal.id_pelabuhan AS id_pelabuhan_berangkat_ori,
                                            db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_berangkat_ori
                                FROM db_pipp.trs_aktivitas_kapal JOIN db_master.mst_pelabuhan ON db_pipp.trs_aktivitas_kapal.id_pelabuhan = db_master.mst_pelabuhan.id_pelabuhan
                                WHERE db_pipp.trs_aktivitas_kapal.id_aktivitas_referensi = $id_aktivitas) trs_ori ";
            }
            $sql_aktivitas .= "
                            WHERE av.id_aktivitas_kapal = $id_aktivitas
                            AND av.aktivitas = 'masuk'
                            GROUP BY av.id_aktivitas_kapal ";
        }else{
            
            $sql_aktivitas = "SELECT plb_tujuan.nama_pelabuhan_tujuan, 
                            plb.nama_pelabuhan as nama_pelabuhan_entry,  ";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .= "
                            trs_ori.tgl_keberangkatan_ori,
                            trs_ori.tujuan_berangkat_ori,
                            trs_ori.id_pelabuhan_berangkat_ori,
                            trs_ori.nama_pelabuhan_berangkat_ori,
                             ";
            }else{
            $sql_aktivitas .= "
                            plb_berangkat.id_pelabuhan_berangkat_ori,
                            plb_berangkat.nama_pelabuhan_berangkat_ori,
                             ";   
            }
            $sql_aktivitas .= "
                            kpl.nama_kapal,
                            kpl.tanda_selar, 
                            att.nama_alat_tangkap,
                            izn.no_sipi,
                            prs.nama_penanggung_jawab,
                            mpg.nama_pengguna,
                            mpg.id_pengguna, 
                            dpi.nama_dpi,
                            av.* 
                            FROM db_pipp.trs_aktivitas_kapal av";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .= "                           
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan,db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_tujuan 
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan) plb_tujuan 
                            ON plb_tujuan.id_pelabuhan = av.id_pelabuhan";
            }else{
            $sql_aktivitas .= "                           
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan,db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_tujuan 
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_masuk) plb_tujuan 
                            ON plb_tujuan.id_pelabuhan = av.id_pelabuhan
                            LEFT OUTER JOIN (SELECT db_master.mst_pelabuhan.id_pelabuhan AS id_pelabuhan_berangkat_ori, db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_berangkat_ori
                                FROM db_master.mst_pelabuhan, db_pipp.trs_aktivitas_kapal 
                                WHERE db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal) plb_berangkat
                            ON plb_berangkat.id_pelabuhan_berangkat_ori = av.id_pelabuhan";
            }
            $sql_aktivitas .= "                
                            LEFT OUTER JOIN db_master.mst_alat_tangkap att
                                ON att.id_alat_tangkap = av.id_alat_tangkap 
                            LEFT OUTER JOIN db_master.mst_kapal_daerah kpl
                                ON kpl.id_kapal_daerah = av.id_kapal
                            LEFT OUTER JOIN db_master.mst_dpi dpi
                                ON dpi.id_dpi = av.id_dpi
                            JOIN db_master.mst_izin_daerah izn
                                ON kpl.id_kapal_daerah = izn.id_kapal_daerah
                            JOIN db_master.mst_pengguna mpg
                                ON mpg.id_pengguna = av.id_pengguna_buat 
                            JOIN db_master.mst_pelabuhan plb
                                ON plb.id_pelabuhan = av.id_pelabuhan
                            JOIN db_master.mst_perusahaan_daerah prs
                                ON kpl.id_perusahaan_daerah = prs.id_perusahaan_daerah  ";
            if(!empty($id_aktivitas_referensi)){
            $sql_aktivitas .=    
                            "JOIN (SELECT    db_pipp.trs_aktivitas_kapal.tgl_aktivitas AS tgl_keberangkatan_ori,
                                            db_pipp.trs_aktivitas_kapal.tujuan_berangkat AS tujuan_berangkat_ori, 
                                            db_pipp.trs_aktivitas_kapal.id_pelabuhan AS id_pelabuhan_berangkat_ori,
                                            db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_berangkat_ori
                                FROM db_pipp.trs_aktivitas_kapal JOIN db_master.mst_pelabuhan ON db_pipp.trs_aktivitas_kapal.id_pelabuhan = db_master.mst_pelabuhan.id_pelabuhan
                                WHERE db_pipp.trs_aktivitas_kapal.id_aktivitas_referensi = $id_aktivitas) trs_ori ";
            }
            $sql_aktivitas .= "
                            WHERE av.id_aktivitas_kapal = $id_aktivitas
                            AND av.aktivitas = 'masuk'
                            GROUP BY av.id_aktivitas_kapal ";
        }
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        
        if($query_aktivitas->num_rows() > 0){
            $query_kunjungan = $this->db->get_where('trs_maksud_kunjungan', array('id_aktivitas_kapal' => $id_aktivitas));

                if($query_kunjungan->num_rows() > 0)
                {
                    $result = array('aktivitas' => $query_aktivitas->row(), 'kunjungan' => (array) $query_kunjungan->row() );
                }else{
                    $result = array('aktivitas' => $query_aktivitas->row(), 'kunjungan' => false );                
                }
        }else{
            $result = false;
        }
        return $result;                    
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}