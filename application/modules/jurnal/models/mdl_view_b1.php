<?php
/*
 * class Mdl_B1
 */

class Mdl_view_b1 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    }

    public function pendapatan_jasa_per_tahun($id_pelabuhan)
    {

        //$sql = "SELECT * from trs_pendapatan_jasa where trs_pendapatan_jasa.tgl_catat between '2013-11-01' and '2013-11-30';";
        $sql = "SELECT db_pipp.mst_jenis_jasa.nama_jns_jasa, db_pipp.mst_jenis_jasa.satuan, db_pipp.trs_pendapatan_jasa.jml,
                        db_pipp.trs_pendapatan_jasa.id_jns_jasa,db_pipp.trs_pendapatan_jasa.id_pendapatan_jasa, 
                        month(db_pipp.trs_pendapatan_jasa.tgl_catat) as bulan, year(db_pipp.trs_pendapatan_jasa.tgl_catat) as tahun
                    from db_pipp.trs_pendapatan_jasa, db_pipp.mst_jenis_jasa 
                    where db_pipp.mst_jenis_jasa.id_jns_jasa = db_pipp.trs_pendapatan_jasa.id_jns_jasa 
                        and db_pipp.trs_pendapatan_jasa.id_pelabuhan = $id_pelabuhan
                        and year(db_pipp.trs_pendapatan_jasa.tgl_catat) = YEAR(CURRENT_TIMESTAMP)
                        and db_pipp.mst_jenis_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_jasa.aktif ='Ya' ";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function pendapatan_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini)
    {

        //$sql = "SELECT * from trs_pendapatan_jasa where trs_pendapatan_jasa.tgl_catat between '2013-11-01' and '2013-11-30';";
        $sql = "SELECT db_pipp.mst_jenis_jasa.nama_jns_jasa, db_pipp.mst_jenis_jasa.satuan, db_pipp.trs_pendapatan_jasa.jml,
                        db_pipp.trs_pendapatan_jasa.id_jns_jasa,db_pipp.trs_pendapatan_jasa.id_pendapatan_jasa
                    from db_pipp.trs_pendapatan_jasa, db_pipp.mst_jenis_jasa 
                    where db_pipp.mst_jenis_jasa.id_jns_jasa = db_pipp.trs_pendapatan_jasa.id_jns_jasa 
                        and MONTH (db_pipp.trs_pendapatan_jasa.tgl_catat) = $bulan_ini
                        and YEAR (db_pipp.trs_pendapatan_jasa.tgl_catat) = $tahun_ini
                        and db_pipp.trs_pendapatan_jasa.id_pelabuhan = $id_pelabuhan
                        and db_pipp.mst_jenis_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_jasa.aktif ='Ya' ";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    /*public function pendapatan_jasa_now($id_pelabuhan, $bulan_ini, $tahun_ini)
    {

        $sql = "SELECT db_pipp.mst_jenis_jasa.nama_jns_jasa, db_pipp.mst_jenis_jasa.satuan, db_pipp.trs_pendapatan_jasa.jml,
                        db_pipp.trs_pendapatan_jasa.id_jns_jasa 
                    from db_pipp.trs_pendapatan_jasa, db_pipp.mst_jenis_jasa 
                    where db_pipp.mst_jenis_jasa.id_jns_jasa = db_pipp.trs_pendapatan_jasa.id_jns_jasa 
                        and MONTH (db_pipp.trs_pendapatan_jasa.tgl_catat) = $bulan_ini
                        and YEAR (db_pipp.trs_pendapatan_jasa.tgl_catat) = $tahun_ini
                        and db_pipp.trs_pendapatan_jasa.id_pelabuhan = $id_pelabuhan
                        and db_pipp.mst_jenis_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_jasa.aktif ='Ya' ";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }*/

    public function pendapatan_jasa_field($id_pelabuhan, $bulan_ini, $tahun_ini)
    {

        $sql = "SELECT db_pipp.mst_jenis_jasa.nama_jns_jasa, db_pipp.mst_jenis_jasa.satuan, db_pipp.mst_jenis_jasa.id_jns_jasa
                    from db_pipp.mst_jenis_jasa
                    where  db_pipp.mst_jenis_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_jasa.aktif ='Ya' ";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function pendapatan_non_jasa_per_tahun($id_pelabuhan)
    {
        $sql = "SELECT db_pipp.mst_jenis_non_jasa.nama_jns_non_jasa, db_pipp.mst_jenis_non_jasa.satuan, 
                        db_pipp.trs_pendapatan_non_jasa.jml, db_pipp.trs_pendapatan_non_jasa.desk_lain,
                        db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa, db_pipp.trs_pendapatan_non_jasa.id_pendapatan_non_jasa, 
                        month(db_pipp.trs_pendapatan_non_jasa.tgl_catat) as bulan, year(db_pipp.trs_pendapatan_non_jasa.tgl_catat) as tahun
                    from db_pipp.trs_pendapatan_non_jasa, db_pipp.mst_jenis_non_jasa 
                    where db_pipp.mst_jenis_non_jasa.id_jns_non_jasa = db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa 
                        and db_pipp.trs_pendapatan_non_jasa.id_pelabuhan = $id_pelabuhan
                        and year(db_pipp.trs_pendapatan_non_jasa.tgl_catat) = YEAR(CURRENT_TIMESTAMP)
                        and db_pipp.mst_jenis_non_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_non_jasa.aktif ='Ya'";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function pendapatan_non_jasa_per_bulan($id_pelabuhan, $bulan_ini, $tahun_ini)
    {
        $sql = "SELECT db_pipp.mst_jenis_non_jasa.nama_jns_non_jasa, db_pipp.mst_jenis_non_jasa.satuan, 
                        db_pipp.trs_pendapatan_non_jasa.jml, db_pipp.trs_pendapatan_non_jasa.desk_lain,
                        db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa, db_pipp.trs_pendapatan_non_jasa.id_pendapatan_non_jasa
                    from db_pipp.trs_pendapatan_non_jasa, db_pipp.mst_jenis_non_jasa 
                    where db_pipp.mst_jenis_non_jasa.id_jns_non_jasa = db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa 
                        and MONTH (db_pipp.trs_pendapatan_non_jasa.tgl_catat) = $bulan_ini
                        and YEAR (db_pipp.trs_pendapatan_non_jasa.tgl_catat) = $tahun_ini
                        and db_pipp.trs_pendapatan_non_jasa.id_pelabuhan = $id_pelabuhan
                        and db_pipp.mst_jenis_non_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_non_jasa.aktif ='Ya'";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    /*public function pendapatan_non_jasa_now($id_pelabuhan, $bulan_ini, $tahun_ini)
    {
        $sql = "SELECT db_pipp.mst_jenis_non_jasa.nama_jns_non_jasa, db_pipp.mst_jenis_non_jasa.satuan, 
                        db_pipp.trs_pendapatan_non_jasa.jml, db_pipp.trs_pendapatan_non_jasa.desk_lain,
                        db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa 
                    from db_pipp.trs_pendapatan_non_jasa, db_pipp.mst_jenis_non_jasa 
                    where db_pipp.mst_jenis_non_jasa.id_jns_non_jasa = db_pipp.trs_pendapatan_non_jasa.id_jns_non_jasa 
                        and MONTH (db_pipp.trs_pendapatan_non_jasa.tgl_catat) = $bulan_ini
                        and YEAR (db_pipp.trs_pendapatan_non_jasa.tgl_catat) = $tahun_ini
                        and db_pipp.trs_pendapatan_non_jasa.id_pelabuhan = $id_pelabuhan
                        and db_pipp.mst_jenis_non_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_non_jasa.aktif ='Ya'";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }*/

    public function pendapatan_non_jasa_field($id_pelabuhan, $bulan_ini, $tahun_ini)
    {
        $sql = "SELECT db_pipp.mst_jenis_non_jasa.nama_jns_non_jasa, db_pipp.mst_jenis_non_jasa.satuan, 
                        db_pipp.mst_jenis_non_jasa.id_jns_non_jasa 
                    from  db_pipp.mst_jenis_non_jasa 
                    where  db_pipp.mst_jenis_non_jasa.is_fillable = 1
                        and db_pipp.mst_jenis_non_jasa.aktif ='Ya'";
        
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    /*
        $id_pelabuhan(int)
        $array_jns_jasa(array(int))
        example: 
            $id_pelabuhan = 10;
            $arrah_jns_jasa = array(1,2,3,4)

    */

    public function nama_pelabuhan($id_pelabuhan)
    {
        $this->db->select('mbp.nama_pelabuhan');
        $this->db->from('db_master.mst_pelabuhan mbp');
        $this->db->where('mbp.id_pelabuhan', $id_pelabuhan);

        $query = $this->db->get();
        // echo $this->db_pipp->last_query(); die;
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_pendapatan_jasa($id_pelabuhan, $tahun, $bulan)
    {
        $this->db_pipp->select('*');
        $this->db_pipp->from('trs_pendapatan_jasa');
        $this->db_pipp->where('id_pelabuhan', $id_pelabuhan);
        $this->db_pipp->where('YEAR(trs_pendapatan_jasa.tgl_catat) ', $tahun);
        $this->db_pipp->where('MONth(trs_pendapatan_jasa.tgl_catat) ', $bulan);

        $query = $this->db_pipp->get();
        // echo $this->db_pipp->last_query(); die;
        if($query->num_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function pendapatan_jasa_pelayanan($id_pelabuhan, $tahun, $bulan)
    {
        $hasil = $this->cek_pendapatan_jasa($id_pelabuhan, $tahun, $bulan);
        if($hasil == true){

            $this->db_pipp->select('mst_jenis_jasa.nama_jns_jasa, trs_pendapatan_jasa.jml');
            $this->db_pipp->join('trs_pendapatan_jasa', 
                                'trs_pendapatan_jasa.id_jns_jasa = mst_jenis_jasa.id_jns_jasa',
                                'left');
            $this->db_pipp->where('trs_pendapatan_jasa.id_pelabuhan', $id_pelabuhan);
             $this->db_pipp->where('YEAR(trs_pendapatan_jasa.tgl_catat) ', $tahun);
            $this->db_pipp->where('MONth(trs_pendapatan_jasa.tgl_catat) ', $bulan);
        }   
        else {
            $this->db_pipp->select('mst_jenis_jasa.nama_jns_jasa');
        }
        
        $this->db_pipp->from('mst_jenis_jasa');
        $this->db_pipp->where('mst_jenis_jasa.is_fillable', '1');
        // $between = ' BETWEEN "2013-01-12" AND "2013-17-12"';
       

        $query = $this->db_pipp->get();
        // echo $this->db_pipp->last_query();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        // vdump($result);
        return $result;
    }

    public function pendapatan_non_jasa_pelayanan($id_pelabuhan, $tahun, $bulan)
    {
        $hasil = $this->cek_pendapatan_jasa($id_pelabuhan, $tahun, $bulan);

        if($hasil == true){
            $this->db_pipp->select('mst_jenis_non_jasa.nama_jns_non_jasa, 
                                    trs_pendapatan_non_jasa.jml, 
                                    mst_jenis_non_jasa.satuan');
            $this->db_pipp->where('trs_pendapatan_non_jasa.id_pelabuhan', $id_pelabuhan);   
            $this->db_pipp->join('trs_pendapatan_non_jasa', 
                                    'trs_pendapatan_non_jasa.id_jns_non_jasa = mst_jenis_non_jasa.id_jns_non_jasa',
                                    'left');
             $this->db_pipp->where('YEAR(trs_pendapatan_non_jasa.tgl_catat) ', $tahun);
            $this->db_pipp->where('MONth(trs_pendapatan_non_jasa.tgl_catat) ', $bulan);

            
        }
        else {
            $this->db_pipp->select('mst_jenis_non_jasa.nama_jns_non_jasa, 
                                    mst_jenis_non_jasa.satuan');
        }
        $this->db_pipp->from('mst_jenis_non_jasa');
        $this->db_pipp->where('mst_jenis_non_jasa.is_fillable', '1');
        // $this->db_pipp->where_in('trs_pendapatan_non_jasa.id_jns_non_jasa', $array_jns_non_jasa);
       

        $query = $this->db_pipp->get();
        // echo $this->db_pipp->last_query(); die;
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    // TODO data produksi  (View B3)
    // TODO delete produksi (Delete B3)
    // TODO edit produksi (Edit B3)
    //and trs_pendapatan_jasa.tgl_catat between '2013-11-01' and '2013-11-30'
}