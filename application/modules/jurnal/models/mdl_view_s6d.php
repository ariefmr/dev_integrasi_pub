<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s6d extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT  db_pipp.mst_fas_transportasi.id_fas_transportasi, db_pipp.mst_fas_transportasi.tgl_catat, db_pipp.mst_jenis_transportasi.nama_jns_transportasi,
                            db_pipp.mst_fas_transportasi.nama, db_pipp.mst_fas_transportasi.jenis, db_pipp.mst_fas_transportasi.jarak, db_pipp.mst_fas_transportasi.kota_tujuan,
                            db_pipp.mst_fas_transportasi.frekuensi, db_pipp.mst_fas_transportasi.id_jns_transportasi
                            from db_pipp.mst_fas_transportasi, db_pipp.mst_jenis_transportasi
                            where db_pipp.mst_fas_transportasi.id_pelabuhan=$id_pelabuhan and
                            db_pipp.mst_fas_transportasi.id_jns_transportasi=db_pipp.mst_jenis_transportasi.id_jns_transportasi and
                            db_pipp.mst_fas_transportasi.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_fas_transportasi', $id_record);
        $query = $this->db->get('mst_fas_transportasi');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}