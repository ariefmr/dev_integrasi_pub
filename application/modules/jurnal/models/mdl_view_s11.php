<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s11 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_pegawai($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_pegawai.* 
                                FROM db_pipp.mst_pegawai
                                WHERE db_pipp.mst_pegawai.id_pelabuhan=$id_pelabuhan";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_pegawai($id_pegawai)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_pegawai.* 
                                FROM db_pipp.mst_pegawai
                                WHERE db_pipp.mst_pegawai.id_pegawai=$id_pegawai";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_pegawai', $id_record);
        $query = $this->db->get('mst_pegawai');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}