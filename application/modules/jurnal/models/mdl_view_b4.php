<?php
/*
 * class Mdl_B2
 */

class Mdl_view_b4 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan, $tgl_catat)
    {

        $sql = "SELECT mst_olah_ikan.id_olah_ikan, mst_industri.nama_industri, mst_jenis_hasil_olah.nama_jns_hasil_olah,
                mst_olah_ikan.jml, mst_olah_ikan.harga, mst_kateg_tujuan.nama_kateg_tujuan, mst_olah_ikan.tujuan,
                mst_olah_ikan.ket
                from mst_industri, mst_jenis_hasil_olah, mst_olah_ikan left join mst_kateg_tujuan using(id_kateg_tujuan)
                where mst_olah_ikan.id_pelabuhan=$id_pelabuhan 
                and mst_olah_ikan.id_jns_hasil_olah=mst_jenis_hasil_olah.id_jns_hasil_olah 
                and mst_olah_ikan.id_industri=mst_industri.id_industri 
                and mst_olah_ikan.id_pelabuhan=mst_industri.id_pelabuhan
                and mst_olah_ikan.tgl_catat='$tgl_catat' 
                and mst_olah_ikan.aktif = 'Ya'
                order by mst_industri.nama_industri asc
                ";
    	
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }


    // TODO data produksi  (View B3)
    // TODO delete produksi (Delete B3)
    // TODO edit produksi (Edit B3)
}