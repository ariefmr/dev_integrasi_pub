<?php
/*
 * class Mdl_H3
 */

class Mdl_view_h5 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($filter,$id_pelabuhan, $tgl_aktivitas)
    {
        // Filter all semua entry oleh / dari / ke pelabuhan
        switch ($filter) {
            case 'all':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas >= '$tgl_aktivitas'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan)        
                        UNION
                            SELECT  db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal_daerah
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas >= '$tgl_aktivitas'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan)         ";
                break;
            case 'entry':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                        UNION
                            SELECT  db_master.mst_kapal_daerah.nama_kapal,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal_daerah
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan = $id_pelabuhan
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'";
                break;
            case 'kembali':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan 
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan')                              
                            UNION
                                SELECT  db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal_daerah
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal = $id_pelabuhan 
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan') ";
                break;
            case 'ke':
                $sql_aktivitas = "SELECT  db_master.mst_kapal.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal.id_kapal
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal != $id_pelabuhan 
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'above'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan') 
                            UNION
                            SELECT  db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_entry,
                                db_pipp.trs_aktivitas_kapal.*
                            FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_kapal_daerah
                                ON db_pipp.trs_aktivitas_kapal.id_kapal = db_master.mst_kapal_daerah.id_kapal_daerah
                            WHERE db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                AND db_pipp.trs_aktivitas_kapal.id_pelabuhan_asal != $id_pelabuhan 
                                AND db_pipp.trs_aktivitas_kapal.tgl_aktivitas = '$tgl_aktivitas'
                                AND db_pipp.trs_aktivitas_kapal.filter_gt = 'below'
                                AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                AND (db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '$id_pelabuhan%'
                                OR db_pipp.trs_aktivitas_kapal.id_pelabuhan_tujuan LIKE '%$id_pelabuhan') ";
                break;
        }
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_aktivitas($id_aktivitas)
    {   
        $this->db->select('filter_gt');
        $filter_gt = $this->db->get_where('trs_aktivitas_kapal', array('id_aktivitas_kapal' => $id_aktivitas))->row()->filter_gt;

        if($filter_gt === 'above')
        {
                 $sql_aktivitas = "SELECT 
                                        db_master.mst_kapal.nama_kapal AS nama_kapal_ref,
                                        db_master.mst_kapal.tanda_selar,
                                        db_master.mst_perusahaan.nama_penanggung_jawab,
                                        db_master.mst_izin.no_sipi,
                                        db_master.mst_alat_tangkap.nama_alat_tangkap,
                                        IFNULL(db_master.mst_dpi.nama_dpi, '') AS nama_dpi,
                                        db_master.mst_pengguna.nama_pengguna,
                                        db_master.mst_pengguna.id_pengguna,
                                        db_pipp.trs_aktivitas_kapal.*
                                    FROM
                                        db_pipp.trs_aktivitas_kapal
                                    LEFT JOIN db_master.mst_kapal
                                        ON db_master.mst_kapal.id_kapal = db_pipp.trs_aktivitas_kapal.id_kapal
                                    LEFT JOIN db_master.mst_izin
                                        ON db_master.mst_izin.id_kapal = db_pipp.trs_aktivitas_kapal.id_kapal
                                    LEFT JOIN db_master.mst_alat_tangkap
                                        ON db_master.mst_alat_tangkap.id_alat_tangkap = db_pipp.trs_aktivitas_kapal.id_alat_tangkap
                                    LEFT JOIN    db_master.mst_perusahaan
                                        ON db_master.mst_perusahaan.id_perusahaan = db_master.mst_kapal.id_perusahaan
                                    LEFT JOIN db_master.mst_pengguna
                                    ON db_master.mst_pengguna.id_pengguna = db_pipp.trs_aktivitas_kapal.id_pengguna_buat
                                    LEFT JOIN db_master.mst_dpi
                                        ON db_pipp.trs_aktivitas_kapal.id_dpi = db_master.mst_dpi.id_dpi
                                    WHERE
                                        db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = $id_aktivitas
                                            AND db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                    GROUP BY db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal";
        }else{
            $sql_aktivitas = "SELECT 
                            db_master.mst_kapal_daerah.nama_kapal AS nama_kapal_ref,
                            db_master.mst_kapal_daerah.tanda_selar, 
                            db_master.mst_perusahaan_daerah.nama_penanggung_jawab,
                            db_master.mst_izin_daerah.no_sipi, 
                            db_master.mst_alat_tangkap.nama_alat_tangkap,
                            db_master.mst_dpi.nama_dpi,
                            db_master.mst_pengguna.nama_pengguna,
                            db_master.mst_pengguna.id_pengguna,                                                        
                            db_pipp.trs_aktivitas_kapal.* 
                            FROM db_pipp.trs_aktivitas_kapal
                                    LEFT JOIN db_master.mst_kapal_daerah
                                        ON db_master.mst_kapal_daerah.id_kapal_daerah = db_pipp.trs_aktivitas_kapal.id_kapal
                                    LEFT JOIN db_master.mst_izin_daerah
                                        ON db_master.mst_izin_daerah.id_kapal_daerah = db_pipp.trs_aktivitas_kapal.id_kapal
                                    LEFT JOIN db_master.mst_alat_tangkap
                                        ON db_master.mst_alat_tangkap.id_alat_tangkap = db_pipp.trs_aktivitas_kapal.id_alat_tangkap
                                    LEFT JOIN    db_master.mst_perusahaan_daerah
                                        ON db_master.mst_perusahaan_daerah.id_perusahaan_daerah = db_master.mst_kapal_daerah.id_perusahaan_daerah
                                    LEFT JOIN db_master.mst_pengguna
                                    ON db_master.mst_pengguna.id_pengguna = db_pipp.trs_aktivitas_kapal.id_pengguna_buat
                                    LEFT JOIN db_master.mst_dpi
                                        ON db_pipp.trs_aktivitas_kapal.id_dpi = db_master.mst_dpi.id_dpi
                                    WHERE
                                        db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal = $id_aktivitas
                                            AND db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                            GROUP BY db_pipp.trs_aktivitas_kapal.id_aktivitas_kapal ";
        }
       

        $query_aktivitas = $this->db->query($sql_aktivitas);                                


        if($query_aktivitas->num_rows() > 0){
            $row_result = $query_aktivitas->row();
            $row_result->nama_pelabuhan_entry = $this->pipp_lib->str_nama_pelabuhan($row_result->id_pelabuhan);
            $row_result->nama_pelabuhan_berangkat = $this->pipp_lib->str_nama_pelabuhan($row_result->id_pelabuhan_asal);
            $row_result->nama_pelabuhan_tujuan = $this->pipp_lib->str_nama_pelabuhan($row_result->id_pelabuhan_tujuan);
    
            //var_dump($row_result);
            $sql_perbekalan = "SELECT db_pipp.mst_jenis_perbekalan.satuan, db_pipp.trs_perbekalan_kapal_keluar.* 
                                FROM db_pipp.trs_perbekalan_kapal_keluar
                                LEFT JOIN db_pipp.mst_jenis_perbekalan
                                    ON db_pipp.mst_jenis_perbekalan.id_jns_perbekalan = db_pipp.trs_perbekalan_kapal_keluar.id_jns_perbekalan
                                WHERE id_aktivitas_kapal = ".$row_result->id_aktivitas_kapal;            
            $query_perbekalan = $this->db->query($sql_perbekalan);
                if($query_perbekalan->num_rows() > 0)
                {
                    $result = array('aktivitas' => $row_result, 'perbekalan' => $query_perbekalan->result() );
                }else{
                    $result = array('aktivitas' => $row_result, 'perbekalan' => false );                
                }
        
        }else{
            $result = false;
        }

        return $result;                    
    }

    public function check_berangkat($id_kapal)
    {
        $sql = "SELECT db_master.mst_pelabuhan.nama_pelabuhan AS nama_pelabuhan_berangkat, db_pipp.trs_aktivitas_kapal.* 
                        FROM db_pipp.trs_aktivitas_kapal
                            LEFT JOIN db_master.mst_pelabuhan 
                            ON db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_aktivitas_kapal.id_pelabuhan 
                                    WHERE db_pipp.trs_aktivitas_kapal.id_kapal = $id_kapal
                                    AND db_pipp.trs_aktivitas_kapal.aktivitas = 'keluar'
                                    AND db_pipp.trs_aktivitas_kapal.aktif = 'Ya'
                                    AND db_pipp.trs_aktivitas_kapal.id_aktivitas_referensi IS NULL ";

        $query = $this->db->query($sql);                                
        

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result; 
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}