<?php
/*
 * class Mdl_B2
 */

class Mdl_view_b2 extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan, $tgl_catat)
    {

        $sql = "SELECT
                db_pipp.trs_perbekalan.id_perbekalan, 
                db_pipp.trs_perbekalan.id_pelabuhan, 
                db_pipp.trs_perbekalan.id_industri, 
                db_pipp.trs_perbekalan.id_jns_perbekalan, 
                db_master.mst_pelabuhan.nama_pelabuhan, 
                db_pipp.mst_industri.nama_industri, 
                db_pipp.mst_jenis_perbekalan.nama_jns_perbekalan,
                db_pipp.trs_perbekalan.tgl_catat,
                db_pipp.trs_perbekalan.jml,
                db_pipp.trs_perbekalan.satuan,
                db_pipp.trs_perbekalan.harga,
                db_pipp.trs_perbekalan.is_luar_pelabuhan
                FROM db_pipp.trs_perbekalan
                LEFT JOIN db_master.mst_pelabuhan ON db_master.mst_pelabuhan.id_pelabuhan = db_pipp.trs_perbekalan.id_pelabuhan
                LEFT JOIN db_pipp.mst_industri ON db_pipp.mst_industri.id_industri = db_pipp.trs_perbekalan.id_industri
                LEFT JOIN db_pipp.mst_jenis_perbekalan ON db_pipp.mst_jenis_perbekalan.id_jns_perbekalan= db_pipp.trs_perbekalan.id_jns_perbekalan
                WHERE  MONTH(db_pipp.trs_perbekalan.tgl_catat) = ".$tgl_catat['bulan']."
                                    AND YEAR(db_pipp.trs_perbekalan.tgl_catat) = ".$tgl_catat['tahun']."
                AND db_pipp.trs_perbekalan.id_pelabuhan = $id_pelabuhan
                AND db_pipp.trs_perbekalan.aktif = 'Ya'";
    	
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }


    // TODO data produksi  (View B3)
    // TODO delete produksi (Delete B3)
    // TODO edit produksi (Edit B3)
}