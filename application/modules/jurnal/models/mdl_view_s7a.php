<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s7a extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT mst_breakwater.tgl_catat, mst_breakwater.id_breakwater, mst_breakwater.tahun, mst_breakwater.ket_rehab,
                                mst_breakwater.jns_konstruksi, mst_breakwater.panjang, mst_breakwater.lebar,
                                mst_breakwater.elevasi, mst_breakwater.sumber_dana,
                                mst_breakwater.nilai, mst_breakwater.ket_sumber_dana, mst_breakwater.id_row, mst_breakwater.gambar, 
                                mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                from mst_breakwater, mst_jenis_ket_konstruksi
                                where mst_breakwater.id_pelabuhan=$id_pelabuhan
                                and mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=mst_breakwater.ket_konstruksi
                                and mst_breakwater.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_breakwater', $id_record);
        $query = $this->db->get('mst_breakwater');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }



    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT mst_breakwater.tgl_catat, mst_breakwater.id_breakwater, mst_breakwater.tahun, mst_breakwater.ket_rehab,
                                mst_breakwater.jns_konstruksi, mst_breakwater.panjang, mst_breakwater.lebar,
                                mst_breakwater.elevasi, mst_breakwater.ket_konstruksi, mst_breakwater.sumber_dana,
                                mst_breakwater.nilai, mst_breakwater.ket_sumber_dana, mst_breakwater.id_row, mst_breakwater.gambar,
                                mst_jenis_ket_konstruksi.nama_jns_ket_konstruksi
                                from mst_breakwater, mst_jenis_ket_konstruksi
                                where mst_breakwater.id_pelabuhan=$id_pelabuhan
                                and mst_jenis_ket_konstruksi.id_jns_ket_konstruksi=mst_breakwater.ket_konstruksi
                                and mst_breakwater.aktif = 'Ya' 
                                order by db_pipp.mst_breakwater.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}