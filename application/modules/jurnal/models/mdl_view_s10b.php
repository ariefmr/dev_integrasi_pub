<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s10b extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_sungai($id_pelabuhan)
    {

        $sql_aktivitas = "  SELECT mst_sungai.id_sungai, mst_sungai.nama_sungai, mst_sungai.nama_lokal, mst_sungai.lebar_sungai, mst_sungai.dalam_sungai,
                            mst_sungai.desk_sungai, mst_sungai.tgl_catat
                            from mst_sungai
                            where mst_sungai.id_pelabuhan=$id_pelabuhan
                            AND mst_sungai.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}