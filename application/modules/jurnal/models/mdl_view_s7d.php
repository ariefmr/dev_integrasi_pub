<?php
/*
 * class Mdl_H3
 */

class Mdl_view_s7d extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function list_per_tanggal($id_pelabuhan)
    {

        $sql_aktivitas = "SELECT db_pipp.mst_dermaga.*
                                from db_pipp.mst_dermaga
                                where db_pipp.mst_dermaga.id_pelabuhan = $id_pelabuhan
                                and db_pipp.mst_dermaga.aktif = 'Ya' ";
        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_record($id_record)
    {
        $this->db->where('id_dermaga', $id_record);
        $query = $this->db->get('mst_dermaga');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru($id_pelabuhan)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_dermaga.*
                                from db_pipp.mst_dermaga
                                where db_pipp.mst_dermaga.id_pelabuhan = $id_pelabuhan
                                and db_pipp.mst_dermaga.aktif = 'Ya' 
                                order by db_pipp.mst_dermaga.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_terbaru_spesifik($id_pelabuhan, $tipe_dermaga)
    {
        $sql_aktivitas = "SELECT db_pipp.mst_dermaga.*
                                from db_pipp.mst_dermaga
                                where db_pipp.mst_dermaga.id_pelabuhan = $id_pelabuhan
                                and db_pipp.mst_dermaga.tipe_dermaga = '".$tipe_dermaga."'
                                and db_pipp.mst_dermaga.aktif = 'Ya' 
                                order by db_pipp.mst_dermaga.tahun desc
                                limit 1 ";

        
        $query_aktivitas = $this->db->query($sql_aktivitas);                                
        

        if($query_aktivitas->num_rows() > 0){
            $result = $query_aktivitas->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}