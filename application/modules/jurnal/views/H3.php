<?php 
	$tmpl = array ( 'table_open'  => '<table id="table_H3" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Jenis Ikan', 'Kondisi', 'Jumlah Ikan', 'Harga Ikan', 'Kota Asal', 'Jenis Transportasi', 'Aksi');
    
    //Debugging
    //echo json_encode($list_H3);
    
    $number = 1;
    if ($list_H3){
	    foreach ($list_H3 as $item) {
            $link_edit = '<a class="edit_button btn btn-warning" data-id-pemasaran-masuk="'.$item->id_pemasaran_masuk.'" 
                                                 data-nama-jenis-ikan="'.$item->nama_jenis_ikan.'"
                                                 data-id-jenis-kondisi-ikan="'.$item->id_jenis_kondisi_ikan.'"
                                                 data-jml-ikan="'.$item->jml_ikan.'"
                                                 data-harga-ikan="'.$item->harga_ikan.'"
                                                 data-kota-tujuan="'.$item->kota_asal.'"
                                                 data-jenis-transportasi="'.$item->jenis_transportasi.'"
                            href="#">Edit</a>';
            $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/H3/hapus/'.$item->id_pemasaran_masuk).'">Hapus</a>';
			$this->table->add_row($number.'. ', $item->nama_jenis_ikan,
                                        $item->nama_jenis_kondisi_ikan,
                                        $item->jml_ikan, 
                                        $item->harga_ikan,
                                        $item->kota_asal, 
                                        $item->jenis_transportasi,
                                        $link_edit.' '.$link_delete );
			$number++;
		}
    }

    $table_H3 = $this->table->generate();
	$this->table->clear();
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
            </div>
		  <div class="panel-body overflowed">
         Ganti Tanggal: 
                            <button type="button" title="Klik Tombol Ini Untuk Merubah Filter Tanggal Entry." id="ganti_tanggal" class="btn btn-default" 
                                data-filter-tanggal="<?php echo $tmp_tgl_catat; ?>">
                              <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?>
                            </button>
                                        <button type="button" id="start_filter" class="btn btn-success">Filter</button> 
                            <input type="hidden" id="datepicker_jurnal">
		    <?php echo $table_H3; ?>
		  </div>
		</div>
	</div>
</div>

  <!-- Modal -->
  <div id="modal-edit-h3" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Pelabuhan : <?php //echo $text_nama_industri?></h4>
    </div>
        <div class="modal-body">
     <!-- FORM OPEN -->
    <?php
    echo form_open('entry/H3/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $tmpl = array ( 'table_open'  => '<table id="table_H3_edit" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Nama Jenis Ikan', 'Kondisi', 'Jumlah Ikan', 'Harga Ikan');

    $attr_id_pemasaran_masuk = array('input_id' => 'id_pemasaran_masuk', 
                                             'input_name' => 'id_pemasaran_masuk' , 
                                             'label_text' => '', 
                                             'input_value' => '', 
                                             'input_placeholder' => '',
                                             'input_type' => 'hidden', 
                                             'input_width' => 'hide', 
                                             'label_class' => 'hide', 
                                             'input_class' => 'form-control' );
    echo $this->mkform->input($attr_id_pemasaran_masuk);

    $text_nama_jenis_ikan = '<text id="nama_jenis_ikan" ></text>';

    $input_kondisi_ikan = '<select id="id_jenis_kondisi_ikan" name="id_jenis_kondisi_ikan" class="form-control">
                            <option value="1">Segar</option>
                            <option value="2">Beku</option>
                            <option value="3">Hidup</option>
                            <option value="4">Asin</option>
                            </select>';
    
    //content Input Berat    
    $input_jml_ikan = '<input id="jml_ikan" name="jml_ikan" type="text" class="form-control" placeholder="">';
    
    //content input untuk Harga
    $input_harga_ikan = '<input id="harga_ikan" name="harga_ikan" type="text" class="form-control" placeholder="">';

    $this->table->add_row($text_nama_jenis_ikan, $input_kondisi_ikan, $input_jml_ikan, $input_harga_ikan);
    
    $table_H3_edit = $this->table->generate();
    echo $table_H3_edit;
 ?>
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
      <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </div>
    </form>
  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->


 <script>
    var array_uri = ['jurnal','H3','index'];

    function listener_ganti_tanggal(tgl_selected)
    {   
        var set_date = new Date(tgl_selected),
            new_date = format_date_to_str(set_date);
        $("#ganti_tanggal").data('filterTanggal', tgl_selected);    
        $("#ganti_tanggal").text(new_date);    
    }
    function start_filter()
    {
        var new_segments = array_uri.join("/"),
            new_url = site_url+new_segments+"/",
            new_tanggal = $("#ganti_tanggal").data('filterTanggal'),
            url_redirect = new_url+new_tanggal; 
            
        // console.log(url_redirect);
        window.open(url_redirect,'_self');
    }
    $('document').ready(function(){

        $("#datepicker_jurnal").datepicker({ 
                                                            maxDate: new Date(),
                                                            defaultDate: new Date(),
                                                            dateFormat: 'yy-mm-dd',
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                                                            onSelect: listener_ganti_tanggal
                                            });
    
        $("#ganti_tanggal").click(function() {
            $("#datepicker_jurnal").datepicker("show").datepicker("widget").show().position({
                                                        my: "right bottom",
                                                        at: "right bottom",
                                                        of: this 
                                                    });
        });


        $("#start_filter").click(function(){
            start_filter();
        });

        $(".edit_button").on("click", function() {
            
            var idPemasaranMasuk = $(this).data('idPemasaranMasuk'),
                namaJenisIkan = $(this).data('namaJenisIkan'),
                jmlIkan = $(this).data('jmlIkan');
                idJenisKondisiIkan = $(this).data('idJenisKondisiIkan'),
                hargaIkan = $(this).data('hargaIkan');

                $("#id_pemasaran_masuk").val(idPemasaranMasuk);
                $("#nama_jenis_ikan").text(namaJenisIkan);
                $("#id_jenis_kondisi_ikan").val(idJenisKondisiIkan);
                $("#jml_ikan").val(jmlIkan);
                $("#harga_ikan").val(hargaIkan);


            $("#modal-edit-h3").modal("show");
              
        });

        $('#table_H3').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "25%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );

      $('#table_H3_edit').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "40%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false, 
            "bSort": true
          } );

    });
 </script>