<?php 

    //-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_sungai" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
      'No.',
      'Nama Sungai di Peta', 
      'Nama Lokal',
      'Lebar Sungai (m)',
      'Dalam Sungai (m)',
      'Deskripsi',
      'Tgl Catat',
      'Aksi'
      ));
    if ($list_sungai){
      $counter = 1;
      foreach ($list_sungai as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S10B/hapus/'.$item->id_sungai).'">Hapus</a>';
        $this->table->add_row($counter.'. ', $item->nama_sungai,$item->nama_lokal, $item->lebar_sungai,$item->dalam_sungai,
          $item->desk_sungai,$this->pipp_lib->fmt_tgl($item->tgl_catat),$link_delete);

        $counter++;  
      }
    }
    $tabel_list_sungai = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Sungai</h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_sungai; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s8a',
                             'modal_title' => 'Edit S8A',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S8A/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });    
    $('#tabel_list_sungai').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );

  });
</script>