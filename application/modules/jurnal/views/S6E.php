<?php 

    //$template = array ( "table_open" => "<table id='tabel_S6E' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S6E" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
                                      'No.',
                                      'Tgl Catat',
                                      'Jenis Amenities',
                                      'Nama Amenities',
                                      'Jarak (km)',
                                      'Keterangan',
                                      'Aksi'
                                    )
                              );

    $counter = 1;
    if ($list_S6E){
      foreach ($list_S6E as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S6E/hapus/'.$item->id_amenities).'">Hapus</a>';
        /*$link_edit = '<a class="edit_button btn btn-warning" 
                                                data-id="'.$item->id_amenities.'"
                                                data-nama-amenities="'.$item->nama_amenities.'"
                                                data-jarak="'.$item->jarak.'"
                                                data-ket="'.$item->ket.'"
                            href="#">Edit</a>';*/
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_amenities.'">Edit</button>';
        $this->table->add_row(
                                $counter,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $item->nama_jns_amenities,
                                $item->nama_amenities, 
                                $item->jarak,
                                $item->ket,
                                $btn_edit.' '.$link_delete
                              );
        $counter++;  
      }
    }
    $tabel_S6E = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S6E; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s6e',
                             'modal_title' => 'Edit S6E',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S6E/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<!-- Modal -->
  <div id="modal-edit-s6e" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Pelabuhan : <?php //echo $text_nama_industri?></h4>
    </div>
        <div class="modal-body">
     <!-- FORM OPEN -->
    <?php
    echo form_open('entry/S6E/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $id_amenities = array(
                                'input_id' => 'id_amenities', 
                                'input_name' => 'id_amenities' , 
                                'label_text' => '', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($id_amenities);

    $opsi = array(
                   '1' => 'Hotel',
                   '2' => 'Rumah Makan',
                   '3' => 'Tempat Wisata'
                  );
    $attr_nama_jns_amenities = array('input_id' => 'id_jns_amenities', 'input_name' => 'id_jns_amenities', 'label_text' => 'Jenis Amenities', 'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_amenities);

    $attr_nama_amenities = array(
                                'input_id' => 'nama_amenities', 
                                'input_name' => 'nama_amenities' , 
                                'label_text' => 'Nama Amenities :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_amenities);

    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak dari Pelabuhan (Km):', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_ket = array(
                                'input_id' => 'ket', 
                                'input_name' => 'ket' , 
                                'label_text' => 'Keterangan Amenities :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_ket);

 ?>
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
      <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </div>
    </form>
  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });

      $(".edit_button").on("click", function() {
            
            var namaAmenities = $(this).data('namaAmenities');
                idAmenities = $(this).data('id');
                jarak = $(this).data('jarak');
                ket = $(this).data('ket');

                $("#id_amenities").val(idAmenities);
                $("#nama_amenities").val(namaAmenities);
                $("#jarak").val(jarak);
                $("#ket").val(ket);


            $("#modal-edit-s6e").modal("show");
              
        });

      $('#tabel_S6E').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "25%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>