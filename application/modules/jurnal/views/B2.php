<?php 
	$tmpl = array ( 'table_open'  => '<table id="table_B2" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Industri', 'Perbekalan', 'Jumlah', 'Harga Satuan','Total','Asal','Aksi');

    //Debugging
    //echo json_encode($list_B2);
    
    $number = 1;
    if ($list_B2){
        foreach ($list_B2 as $item) {
            $link_edit = '<a class="edit_button btn btn-warning" data-id-perbekalan="'.$item->id_perbekalan.'" 
                                                 data-id-pelabuhan="'.$item->id_pelabuhan.'"
                                                 data-id-industri="'.$item->id_industri.'"
                                                 data-nama-jns-perbekalan="'.$item->nama_jns_perbekalan.'"
                                                 data-nama-industri="'.$item->nama_industri.'"
                                                 data-jml="'.$item->jml.'"
                                                 data-satuan="'.$item->satuan.'"
                                                 data-harga="'.$item->harga.'"
                            href="#">Edit</a>';
            $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/B2/hapus/'.$item->id_perbekalan).'">Hapus</a>';
            if ($item->satuan == 'Rp'){
                $jml_satuan = $item->satuan.' '. number_format($item->jml,2, ',','.');
                $hrg_per_satuan = 'Rp '.number_format($item->harga,2, ',','.').'/unit';
                $total = $jml_satuan;
            }else{
                $jml_satuan = number_format($item->jml,0, ',','.') .' '. $item->satuan;
                $hrg_per_satuan = 'Rp '.number_format($item->harga,2, ',','.').'/'.$item->satuan;
                $total = 'Rp '.number_format($item->jml * $item->harga,2, ',','.');
            }

            if($item->is_luar_pelabuhan === '1'){
                $asal = "Luar Pelabuhan";
            }else{
                $asal = "Dalam Pelabuhan";
            }

            
            
            $this->table->add_row($number.'. ',  
                                        $item->nama_industri, 
                                        $item->nama_jns_perbekalan, 
                                        $jml_satuan, 
                                        $hrg_per_satuan,
                                        $total,
                                        $asal,
                                        $link_edit.' '.$link_delete );
            $number++;
        }
    }

    $table_B2 = $this->table->generate();
	$this->table->clear();
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal : <?php echo fmt_bulan_tahun($tmp_tgl_catat); ?></h3>
            </div>
		  <div class="panel-body overflowed">
            Ganti tanggal :
		    <?php 
            $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                                 'datepick_id' => 'datepicker_jurnal',
                                                 'default_text' => fmt_bulan_tahun($tmp_tgl_catat),
                                                 'input_value' => $tmp_tgl_catat.'-01'
                                                );
            echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
            ?>
            <button type="button" id="start_filter" class="btn btn-success">Filter</button> 
            <?php
            echo $table_B2; 
            $text_nama_industri = '<text id="nama_industri" ></text>';
            ?>
		  </div>
		</div>
	</div>
</div>
    <!-- FORM OPEN -->
    
  <!-- Modal -->
              <div id="modal-edit-b2" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Perusahaan : <?php echo $text_nama_industri?></h4>
                </div>
                    <div class="modal-body">
<?php
    echo form_open('entry/B2/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $tmpl = array ( 'table_open'  => '<table id="table_B2_edit" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Perbekalan', 'Satuan', 'Jumlah', 'Harga');

    $attr_id_perbekalan = array('input_id' => 'id_perbekalan', 
                                             'input_name' => 'id_perbekalan' , 
                                             'label_text' => '', 
                                             'input_value' => '', 
                                             'input_placeholder' => '',
                                             'input_type' => 'hidden', 
                                             'input_width' => 'hide', 
                                             'label_class' => 'hide', 
                                             'input_class' => 'form-control' );
    echo $this->mkform->input($attr_id_perbekalan);

    $text_perbekalan = '<text id="nama_jns_perbekalan" ></text>';
    $text_satuan = '<text id="satuan" ></text>';

    //$opsi_perbekalan = echo Modules::run('tables/pipp_list_all', 'mst_jenis_perbekalan');
    //dropdown untuk kondisi ikan
    $attr_opsi_jenis_perbekalan = array('input_id' => 'id_jns_perbekalan', 
                                    'input_name' => 'id_jns_perbekalan', 
                                    'label_text' => '', 
                                    'array_opsi' => '', 
                                    'opsi_selected' => '', 
                                    'input_width' => '', 
                                    'input_class' => 'form-control select_perbekalan', 
                                    'label_class' => 'none');
    $input_jns_perbekalan = $this->mkform->pilih_perbekalan($attr_opsi_jenis_perbekalan);
    
    //content Input Berat    
    $input_jml = '<input id="jml" name="jml" type="text" class="form-control" placeholder="">';
    
    //content input untuk Harga
    $input_harga = '<input id="harga" name="harga" type="text" class="form-control" placeholder="">';

    $this->table->add_row($text_perbekalan, $text_satuan, $input_jml, $input_harga);
    
    $table_B2_edit = $this->table->generate();
    echo $table_B2_edit;
 ?>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
    </form>
                  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

 <script>
    var array_uri = ['jurnal','B2','index'];
    //set satuan
    function listenerSatuan() {
        var jenisPerbekalan = $(this).val(), 
            tipeSatuan = $('option:selected', this).data('tipeSatuan');

        console.log(tipeSatuan);
        $('#satuan').text(tipeSatuan);
    }

    function start_filter()
    {
      var new_segments = array_uri.join("/"),
          new_url = site_url+new_segments+"/",
          get_tanggal = $('#datepicker_jurnal').val(),
          arr_tanggal = get_tanggal.split('-'),
          new_tanggal = arr_tanggal[0]+"/"+arr_tanggal[1],
          url_redirect = new_url+new_tanggal; 
    
      // console.log(url_redirect);
      window.open(url_redirect,'_self');
    }

    $(document).ready(function(){

        $('.select_perbekalan').on("change", listenerSatuan);

        $(".edit_button").on("click", function() {
            
            var idPerbekalan = $(this).data('idPerbekalan'),
                idPelabuhan = $(this).data('idPelabuhan'),
                idIndustri = $(this).data('idIndustri');
                idJnsPerbekalan = $(this).data('idJnsPerbekalan'),
                namaJnsPerbekalan = $(this).data('namaJnsPerbekalan'),
                namaIndustri = $(this).data('namaIndustri'),
                jml = $(this).data('jml'),
                satuan = $(this).data('satuan'),
                harga = $(this).data('harga')

                $("#id_perbekalan").val(idPerbekalan);
                $("#nama_jns_perbekalan").text(namaJnsPerbekalan);
                $("#nama_industri").text(namaIndustri);
                $("#satuan").text(satuan);
                $("#jml").val(jml);
                $("#satuan").val(satuan)
                $("#harga").val(harga);


            $("#modal-edit-b2").modal();
              
        });

        $("#start_filter").click(function(){
            start_filter();
        });

     	$('#table_B2').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );

      $('#table_B2_edit').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "40%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false, 
            "bSort": true
          } );

    });
 </script>