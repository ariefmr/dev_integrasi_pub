<?php 

    //$template = array ( "table_open" => "<table id='tabel_S10A' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_list_geoteknik" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading('No.', 'Tgl Catat', 'Jenis Tanah Dasar', 'Dalam Tanah Dasar', 'Jenis Tanah Darat','Dalam Tanah Darat','Zona Gempa',
        'Sumber Data', 'Aksi');

    if ($list_geoteknik){
      $counter = 1;
      // $nama_table = 'mst_geoteknik';
      // $primary_key = 'id_geoteknik';
      // $redir = 'jurnal/S10A';
      // foreach ($list_geoteknik as $item) {
      //   $array_uri = array( 'tbl_' => $nama_table,
      //                       'prim_' => $primary_key,
      //                       'id_' => $item->id_geoteknik,
      //                       'red_' =>$redir
      //                      );
      //   $param_string =  $this->pipp_lib->url_query_encode(serialize($array_uri));
      //   $param_decode = $this->pipp_lib->url_query_decode($param_string);
      //   var_dump($param_decode);
      //   $link_delete = '<a class="btn btn-danger" href="'.base_url('tables/hapus_from?params='.$param_string).'" >Hapus</a>';
      $nama_table = 'mst_geoteknik';
        $primary_key = 'id_geoteknik';
        $redir = 'S10A';
        foreach ($list_geoteknik as $item) {
          $array_uri = array(
                            'tables',
                            'hapus_from2',
                            $nama_table,
                            $primary_key,
                            $item->id_geoteknik,
                            $redir
                           );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
          $counter, 
          $this->pipp_lib->fmt_tgl($item->tgl_catat), 
          $item->jns_tanah_dasar, 
          $item->dalam_tanah_dasar, 
          $item->jns_tanah_darat, 
          $item->dalam_tanah_darat, 
          $item->zona_gempa, 
          $item->sumber_data,
          $link_delete
          );

        $counter++;  
      }
    }
    $tabel_list_geoteknik = $this->table->generate();
    $this->table->clear();

//-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_hidro" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading('No.', 'Tgl Catat', 'Tinggi Gelombang Max', 'Bulan Terjadinya Gelombang Tertinggi', 'Deskripsi Gelombang',
        'Kecepatan Arus Max rata-rata','Sumber Data', 'Aksi');

    if ($list_hidro){
      $counter = 1;
      $nama_table = 'mst_hidrooseanografi';
      $primary_key = 'id_hidrooseanografi';
      $redir = 'S10A';
      foreach ($list_hidro as $item) {
        $array_uri = array(
                          'tables',
                          'hapus_from2',
                          $nama_table,
                          $primary_key,
                          $item->id_hidrooseanografi,
                          $redir
                         );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
              $counter, 
              $this->pipp_lib->fmt_tgl($item->tgl_catat), 
              $item->tinggi_gel_max, 
              $item->bulan_gel_max, 
              $item->desk_gel, 
              $item->kec_arus_max, 
              $item->sumber_data,
              $link_delete
              );
        $counter++;  
      }
    }
    $tabel_list_hidro = $this->table->generate();
    $this->table->clear();

    //-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_klimatologi" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(
      'No.', 
      'Tgl Catat', 
      'Curah Hujan Max ', 
      'Hujan Bulanan Rata-rata', 
      'Kelembaban Udara Rata-rata',
      'Suhu Udara Rata-rata',
      'Bulan Kejadian Angin Besar',
      'Kecepatan Angin Maksimum Rata-rata',
      'Arah Angin Dominian Menuju',
      'Sumber Data', 
      'Aksi'
      );
    
    if ($list_klimatologi){
      $counter = 1;
      foreach ($list_klimatologi as $item) {
        $nama_table = 'mst_klimatologi';
        $primary_key = 'id_klimatologi';
        $redir = 'S10A';
          $array_uri = array(
                            'tables',
                            'hapus_from2',
                            $nama_table,
                            $primary_key,
                            $item->id_klimatologi,
                            $redir
                           );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
          $counter, 
          $this->pipp_lib->fmt_tgl($item->tgl_catat), 
          $item->curah_hujan_max, 
          $item->hujan_bulanan_avg, 
          $item->lembab_udara_avg, 
          $item->suhu_udara_avg, 
          $item->bulan_angin_besar, 
          $item->kec_angin_max,
          $item->arah_angin,
          $item->sumber_data,
          $link_delete
          );

        $counter++;  
      }
    }
    $tabel_list_klimatologi = $this->table->generate();
    $this->table->clear();

    //-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_sedimentasi" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(
                              'No.', 
                              'Tgl Catat', 
                              'Tingkat Sedimentasi Alur Pelayaran', 
                              'Sumber Sedimentasi Alur Pelayaran', 
                              'Tingkat Sedimentasi Kolam Pelabuhan',
                              'Sumber Sedimentasi Kolam Pelabuhan',
                              'Sumber Data', 
                              'Aksi'
                            );
    if ($list_sedimentasi){
      $counter = 1;
      foreach ($list_sedimentasi as $sed) {
        $nama_table = 'mst_sedimentasi';
        $primary_key = 'id_sedimentasi';
        $redir = 'S10A';
          $array_uri = array(
                            'tables',
                            'hapus_from2',
                            $nama_table,
                            $primary_key,
                            $sed->id_sedimentasi,
                            $redir
                           );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
          $counter, 
          $this->pipp_lib->fmt_tgl($sed->tgl_catat), 
          $sed->tk_sed_alur, 
          $sed->sumber_sed_alur, 
          $sed->tk_sed_kolam, 
          $sed->sumber_sed_kolam, 
          $sed->sumber_data,
          $link_delete
          );

        $counter++;  
      }
    }
    $tabel_list_sedimentasi = $this->table->generate();
    $this->table->clear();

    //-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_banjir" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(
                              'No.', 
                              'Tgl Catat', 'Tinggi Banjir ', 
                              'Tahun Terakhir Terjadi Banjir',
                              'Sumber Data', 
                              'Aksi'
                            );
    if ($list_banjir){
      $counter = 1;
      foreach ($list_banjir as $ban) {
        $nama_table = 'mst_banjir';
        $primary_key = 'id_banjir';
        $redir = 'S10A';
          $array_uri = array(
                            'tables',
                            'hapus_from2',
                            $nama_table,
                            $primary_key,
                            $ban->id_banjir,
                            $redir
                           );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
          $counter, 
          $this->pipp_lib->fmt_tgl($ban->tgl_catat), 
          $ban->tinggi_banjir,
          $ban->tahun_banjir, 
          $ban->sumber_data,
          $link_delete
          );

        $counter++;  
      }
    }
    $tabel_list_banjir = $this->table->generate();
    $this->table->clear();

    //-------------------------------------------------------------------------------------
    $template = array ( 'table_open' => '<table id="tabel_list_tsunami" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(
                              'No.', 
                              'Tgl Catat', 
                              'Tinggi Air di Pelabuhan Saat Tsunami ', 
                              'Tahun Terakhir Terjadi Tsunami',
                              'Sumber Data', 
                              'Aksi'
                            );
    if ($list_tsunami){
      $counter = 1;
      foreach ($list_tsunami as $tsu) {
        $nama_table = 'mst_tsunami';
        $primary_key = 'id_tsunami';
        $redir = 'S10A';
          $array_uri = array(
                            'tables',
                            'hapus_from2',
                            $nama_table,
                            $primary_key,
                            $tsu->id_tsunami,
                            $redir
                           );
        $link_delete = '<a class="btn btn-danger" href="'.base_url(implode('/', $array_uri)).'">Hapus</a>';
        $this->table->add_row(
          $counter, 
          $this->pipp_lib->fmt_tgl($tsu->tgl_catat), 
          $tsu->tinggi_air,
          $tsu->tahun_tsunami, 
          $tsu->sumber_data,
          $link_delete
          );

        $counter++;  
      }
    }
    $tabel_list_tsunami = $this->table->generate();
    $this->table->clear();

    $link_entry_geo = '<a class="btn btn-warning" href="'.base_url('entry/S10A1/').'">Entry</a>';
    $link_entry_hid = '<a class="btn btn-warning" href="'.base_url('entry/S10A2/').'">Entry</a>';
    $link_entry_kli = '<a class="btn btn-warning" href="'.base_url('entry/S10A3/').'">Entry</a>';
    $link_entry_sed = '<a class="btn btn-warning" href="'.base_url('entry/S10A4/').'">Entry</a>';
    $link_entry_ban = '<a class="btn btn-warning" href="'.base_url('entry/S10A5/').'">Entry</a>';
    $link_entry_tsu = '<a class="btn btn-warning" href="'.base_url('entry/S10A6/').'">Entry</a>';


 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Geoteknik</h3>
        <?php echo $link_entry_geo; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_geoteknik; ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Hidrooseanografi</h3>
        <?php echo $link_entry_hid; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_hidro; ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Klimatologi</h3>
        <?php echo $link_entry_kli; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_klimatologi; ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Sedimentasi</h3>
        <?php echo $link_entry_sed; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_sedimentasi; ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Banjir</h3>
        <?php echo $link_entry_ban; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_banjir; ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title col-lg-11">Tsunami</h3>
        <?php echo $link_entry_tsu; ?>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_list_tsunami; ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });

    $('#tabel_list_geoteknik').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
    
    $('#tabel_list_hidro').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
    
    $('#tabel_list_klimatologi').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
    
    $('#tabel_list_sedimentasi').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
    
    $('#tabel_list_banjir').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
    
    $('#tabel_list_tsunami').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );

  });
</script>