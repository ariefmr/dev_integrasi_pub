<?php 
	$tmpl = array ( 'table_open'  => '<table id="table_H1" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Kapal', 'Tanggal Masuk', 'Pelabuhan Keberangkatan','Entry Produksi Kapal');
    
    //Debugging
    //echo json_encode($list_H1);
    $number = 1;
    $now = time();
    if($list_H1){
        foreach ($list_H1 as $item) {
            
            if($item->is_bongkar !== NULL ){
               if( $item->id_aktivitas_referensi === NULL )
               {
                    $url_entry_produksi = anchor('entry/H2/ref/'.$item->id_aktivitas_kapal, 'Entry');
                    $judul_button = 'Entry Produksi'; 
               }elseif($item->id_aktivitas_referensi !== NULL){
                    $url_entry_produksi = anchor(site_url(array('jurnal','H1','views',$filter,$tmp_tgl_aktivitas,'#'))
                                                , 'Sudah Entry', 'title="Sudah Entry"'); 
                    $judul_button = 'Sudah Entry';
               }
            }else{
                    $url_entry_produksi = anchor(site_url(array('jurnal','H1','views',$filter,$tmp_tgl_aktivitas,'#'))
                                                , 'Tidak Bongkar Ikan', 'title="Maksud kunjungan bukan bongkar ikan."'); 
                $judul_button = 'Tidak Bongkar Ikan'; 
            }
            $tgl_aktivitas_entry = strtotime($item->tgl_aktivitas);
            $date_diff = floor( ($now - $tgl_aktivitas_entry)/(60*60*24) );

            if( $item->id_pelabuhan === $id_pelabuhan_pengguna && $date_diff < 30 )
            {
                $url_delete_entry = anchor( site_url( array('entry','H1','hapus', $item->id_aktivitas_kapal ) ), 'Hapus' , 'class="hapus-aktivitas"');
            }else{
                $url_delete_entry = anchor(site_url(array('jurnal','H1','views',$filter,$tmp_tgl_aktivitas,'#')), 'Tidak Bisa Hapus', 'class="disabled"');
                //$url_delete_entry = '';
            }

            $url_detail_aktvitas = anchor( site_url(array('jurnal','H1','views',$filter,$tmp_tgl_aktivitas,'#')), 'Detail', 'class="detail_this" data-id-record="'.$item->id_aktivitas_kapal.'"');

            $button_aksi = '<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" id="list_aksi" data-toggle="dropdown">
                                    '.$judul_button.'
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">'
                          .'<li>'.$url_entry_produksi.'</li>'
                          .'<li>'.$url_delete_entry.'</li>'
                          .'<li>'.$url_detail_aktvitas.'</li>'
                          .'</ul></div>';
            $nama_kapal_info = empty($item->nama_kapal) ? $item->nama_kapal_entry : $item->nama_kapal;               
            $this->table->add_row($number.'. ', 
                                    $nama_kapal_info,
                                    $this->pipp_lib->fmt_tgl($item->tgl_kedatangan),
                                    $item->nama_pelabuhan_berangkat,
                                    $button_aksi);
            $number++;
        }
    }
    $table_H1 = $this->table->generate();
	
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_aktivitas);?></h3>
            </div>
		  <div class="panel-body">
            <div class="btn-group">
                <button type="button" class="btn btn-default" id="filter-info-selected" data-filter-selected="<?php echo $filter; ?>"><?php echo $filter_info." ".$nama_pelabuhan_info; ?></button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                 <!--  <li><a href="<?php echo site_url(array('jurnal','h5','views','all',$tmp_tgl_aktivitas)); ?>" title="">Entry Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
                  <li><a href="<?php echo site_url(array('jurnal','h5','views','kembali',$tmp_tgl_aktivitas)); ?>" title="">Kembali Ke Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
                  <li><a href="<?php echo site_url(array('jurnal','h5','views','ke',$tmp_tgl_aktivitas)); ?>">Ke Pelabuhan <?php echo $nama_pelabuhan; ?></a></li>
               --><li><a href="#" class="filter-entry" data-set-filter="all">Dari Pelabuhan Lain / Kembali Ke Pelabuhan <?php echo $nama_pelabuhan_info; ?></a></li>
                  <li><a href="#" class="filter-entry" data-set-filter="kembali">Kembali Ke Pelabuhan <?php echo $nama_pelabuhan_info; ?></a></li>
                  <li><a href="#" class="filter-entry" data-set-filter="lain">Dari Pelabuhan Lain Ke <?php echo $nama_pelabuhan_info; ?></a></li>
               </ul>
            </div>
                            <button type="button" title="Klik Tombol Ini Untuk Merubah Filter Tanggal Kedatangan Kapal." id="ganti_tanggal" class="btn btn-default" data-filter-tanggal="<?php echo $tmp_tgl_aktivitas; ?>">
                            <?php if($filter === 'all'){ echo "Sejak tanggal "; } ?> <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_aktivitas); ?></button>
                                        <button type="button" id="start_filter" class="btn btn-success">Filter</button> 
                            <input type="hidden" id="datepicker_jurnal">
		    <?php echo $table_H1; ?>
		  </div>
		</div>
	</div>
</div>

<?php
    $attr_modal_detail = array('modal_id' => 'modal_detail_h1',
                             'modal_title' => 'Detail H1',
                             'modal_width' => '760',
                             'link_view_form' => base_url('jurnal/H1/detail_H1')
                            ); 
    echo $this->mkform->modal_detail($attr_modal_detail);
?>


 <script>
 var entry_status = "<?php echo $entry_status; ?>",
        nama_kapal_entry = "<?php echo $nama_kapal; ?>",
        current_filter = function(){ return $("#filter-info-selected").data('filterSelected'); },
        array_uri = ['jurnal','H1','views'];
        // BIMTEK
        // BIMTEK LAGI
        // BIMTEK BIMTEEEEK
        
    function notif_entry_success()
    {
          if(entry_status === 'success')
          {
            var pesan = ' Entry Form Produksi Kapal <br>'+nama_kapal_entry+' Berhasil';
            $.pnotify({
                title: 'Entry Berhasil',
                text: pesan,
                type: 'success'
            });
          }
    }

    function start_filter()
    {
        var new_segments = array_uri.join("/"),
            new_url = site_url+new_segments+"/"+current_filter()+"/",
            new_tanggal = $("#ganti_tanggal").data('filterTanggal'),
            url_redirect = new_url+new_tanggal; 
            
        //console.log(url_redirect);
        window.open(url_redirect,'_self');
    }

    function listener_ganti_tanggal(tgl_selected)
    {   
        var set_date = new Date(tgl_selected),
            new_date = format_date_to_str(set_date);
        $("#ganti_tanggal").data('filterTanggal', tgl_selected); 
        if(current_filter() === 'all')
        {
            $("#ganti_tanggal").text('Sejak tanggal '+new_date);    
        }else{
            $("#ganti_tanggal").text(new_date);    
        }

     }
    $(document).ready(function(){
        notif_entry_success();

        $("#datepicker_jurnal").datepicker({ 
                                                            maxDate: new Date(),
                                                            defaultDate: new Date(),
                                                            dateFormat: 'yy-mm-dd',
                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                                                            onSelect: listener_ganti_tanggal
                                            });
    
        $("#ganti_tanggal").click(function() {
            $("#datepicker_jurnal").datepicker("show").datepicker("widget").show().position({
                                                        my: "right bottom",
                                                        at: "right bottom",
                                                        of: this 
                                                    });
        });


        $("#start_filter").click(function(){
            start_filter();
        });

        $(".filter-entry").click(function(e){
            var setFilter = $(this).data('setFilter');
            $("#filter-info-selected").data('filterSelected', $(this).data('setFilter'));
            $("#filter-info-selected").text( $(this).text() );
                if(setFilter === 'all')
                {
                    var tmp_tanggal = $("#ganti_tanggal").text();
                    if(tmp_tanggal.indexOf("Sejak tanggal ") < 0){
                        $("#ganti_tanggal").text("Sejak tanggal "+tmp_tanggal);    
                    }
                }else{
                    var tmp_info_tanggal = $("#ganti_tanggal").text().replace("Sejak tanggal ",""),
                        new_info_tanggal = tmp_info_tanggal.replace("Sejak tanggal ","");
                    $("#ganti_tanggal").text(new_info_tanggal);    
                }
            e.stopPropagation();
            e.preventDefault();
            return false;
        });

        $(".hapus-aktivitas").click(function(){
                return confirm("Anda yakin ingin menghapus entry kapal ini?");
        });

     	$('#table_H1').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "30%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "25%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );
    });
 </script>