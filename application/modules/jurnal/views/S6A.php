<?php 
    $hidden_input = array(
                          'id_wpp' => 571
                          );
 ?> 


<div class="row">
  <div class="panel panel-text-referensi">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-7">
        <?php 
          $number = 1;
          $temp_gambar = '';
          if ($list_S6A){
            // var_dump($list_S6A);
            foreach ($list_S6A as $item) {
              // var_dump($list_S6A);
              $array_info_kapal = array( 
                                              //'ID : ' => array( 'id' => 'id_wpp',
                                              //                        'value' => $item->id_wpp ),
                                              'Tanggal Catat : ' => array( 'id' => 'tanggal_ubah',
                                                                      'value' => $this->pipp_lib->fmt_tgl($item->tanggal_ubah) ),
                                              'Nama Pelabuhan : ' => array( 'id' => 'nama_pelabuhan',
                                                                      'value' => $item->nama_pelabuhan ),
                                              'Nama Pelabuhan Inggris: ' => array( 'id' => 'nama_pelabuhan_inggris',
                                                                      'value' => $item->nama_pelabuhan_inggris ),
                                              'Kelas Pelabuhan: ' => array( 'id' => 'nama_kelas_pelabuhan',
                                                                      'value' => $item->nama_kelas_pelabuhan ),
                                              'Alamat Pelabuhan : ' => array( 'id' => 'alamat_pelabuhan',
                                                                      'value' => $item->alamat_pelabuhan ),                                             
                                              // 'Desa : ' => array( 'id' => 'desa',
                                              //                         'value' => $item->desa ),
                                              // 'Kecamatan : ' => array( 'id' => 'kecamatan',
                                              //                         'value' => $item->kecamatan ),
                                              'Kabupaten : ' => array( 'id' => 'kabupaten',
                                                                      'value' => $item->nama_kabupaten_kota ),
                                              'Provinsi : ' => array( 'id' => 'nama_propinsi',
                                                                      'value' => $item->nama_propinsi ),
                                              'Status Pelabuhan : ' => array( 'id' => 'status_pelabuhan',
                                                                      'value' => $item->nama_deskripsi_status ),
                                              'Lintang : ' => array( 'id' => 'lintang',
                                                                      'value' => $item->lintang ),
                                              'Bujur : ' => array( 'id' => 'bujur',
                                                                      'value' => $item->bujur ),
                                              'No Telpon : ' => array( 'id' => 'no_telpon',
                                                                      'value' => $item->no_telp1 ),
                                              'Fax : ' => array( 'id' => 'no_telpon2',
                                                                      'value' => $item->no_telp2 ),
                                              'Pengelola Pelabuhan : ' => array( 'id' => 'nama_pengelola_pelabuhan',
                                                                      'value' => $item->nama_pengelola_pelabuhan ),
                                              'Email : ' => array( 'id' => 'email',
                                                                      'value' => $item->email ),
                                              'Aktif : ' => array( 'id' => 'aktif',
                                                                      'value' => $item->aktif )
                                           ); 
            }
              //$temp_gambar = $item->gambar;
              //SEMENTARA BELUM ADA GAMBAR DI DSS
              echo $this->mkform->ref_text($array_info_kapal);
              $btn_edit = '<button class="edit_this btn btn-warning btn-lg btn-block" data-id-record="'.$item->id_pelabuhan.'">Edit</button>';
          // }

         ?> 

        </div>
        <div class="col-lg-5">
          <div class="text-center">
          <?php if( !empty($temp_gambar) )
                {
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_uploads']."/".$temp_gambar ?>">
          <?php
                }else{
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_images']."/blank.png"; ?>">
          <?php
                }
          ?> 
          <br>
          <?php if (!empty($item->lintang) && !empty($item->bujur)): ?>
              <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
              <script type="text/javascript">
                var lintang = "<?php echo $item->lintang; ?>",
                    bujur = "<?php echo $item->bujur; ?>";
                function initialize() {
                  var myLatlng = new google.maps.LatLng(lintang,bujur);
                  var mapOptions = {
                    zoom: 16,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.SATELLITE
                  }
                  var map = new google.maps.Map(document.getElementById('map-pelabuhan'), mapOptions);
                      map.setTilt(45);
                  var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                      title: '<?php echo $item->nama_pelabuhan; ?>'
                  });
                }

                google.maps.event.addDomListener(window, 'load', initialize);
              </script>
              
          <?php endif ?>
             <!--  <p class="text-center">Koordinat: <?php echo $item->lintang.",".$item->bujur; ?></p> -->
              <div id="map-pelabuhan" class="image-maps">Koordinat Kosong</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if ($is_admin): ?>
  <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <?php echo $btn_edit; ?>
  </div>

<?php endif ?>

<?php
}
    // $attr_modal_edit = array('modal_id' => 'modal_edit_s6a',
    //                          'modal_title' => 'Edit S6A',
    //                          'modal_width' => '760',
    //                          'link_view_form' => base_url('entry/S6A/edit')
    //                         ); 
    // echo $this->mkform->modal_edit($attr_modal_edit);
?>

<script>
var link_edit_pelabuhan_dss = "http://integrasi.djpt.kkp.go.id/datamaster_baru/admin/ubah/mst_pelabuhan/";
  $(document).ready( function () {
    
    $(".edit_this").on("click", function(){
        var id_pelabuhan = $(this).data('idRecord'),
              link_edit = link_edit_pelabuhan_dss+id_pelabuhan,
              pesan = "Membuka aplikasi datamaster pelabuhan di integrasi. Lanjutkan? ";

        if(confirm(pesan))
          {
            window.open(link_edit, '_blank');
          }else{
            return false;
          }   

    });

  } );
</script>