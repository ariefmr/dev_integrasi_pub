<?php 

    //$template = array ( "table_open" => "<table id='tabel_S6C' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S6C" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
                                      'No.',
                                      'Tgl Catat',
                                      'Jenis Tujuan',
                                      'Nama Tujuan',
                                      'Jarak (km)',
                                      'Rute',
                                      'Kondisi Rute',
                                      'Aksi'
                                      )
                              );

    $counter = 1;
    if ($list_S6C){
      foreach ($list_S6C as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S6C/hapus/'.$item->id_akses_pelabuhan).'">Hapus</a>';
        /*$link_edit = '<a class="edit_button btn btn-warning" 
                                                data-id-akses-pelabuhan="'.$item->id_akses_pelabuhan.'"
                                                data-id-jns-tujuan="'.$item->id_jns_tujuan.'"
                                                data-nama-jns-tujuan="'.$item->nama_jns_tujuan.'"
                                                data-nama-tujuan="'.$item->nama_tujuan.'"
                                                data-jarak="'.$item->jarak.'"
                                                data-rute="'.$item->rute.'"
                                                data-kond-rute="'.$item->kond_rute.'"
                            href="#">Edit</a>';*/
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_akses_pelabuhan.'">Edit</button>';
        $this->table->add_row(
                                $counter,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $item->nama_jns_tujuan,
                                $item->nama_tujuan, 
                                $item->jarak,
                                $item->rute,
                                $item->kond_rute, 
                                $btn_edit.' '.$link_delete
                              );

        $counter++;  
      }
    }
    $tabel_S6C = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S6C; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s6c',
                             'modal_title' => 'Edit S6C',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S6C/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<!-- Modal -->
  <div id="modal-edit-s6c" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Pelabuhan : <?php //echo $text_nama_industri?></h4>
    </div>
        <div class="modal-body">
     <!-- FORM OPEN -->
    <?php
    echo form_open('entry/S6C/ubah', 'id="form_entry" class="form-horizontal" role="form"');


    // FORM Input

    $id_akses_pelabuhan = array(
                                'input_id' => 'id_akses_pelabuhan', 
                                'input_name' => 'id_akses_pelabuhan' , 
                                'label_text' => '', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($id_akses_pelabuhan);

    $opsi = array(
                   '1' => 'Ibu Kota Provinsi',
                   '2' => 'Ibu Kota Kecamatan',
                   '3' => 'Ibu Kota Kabupaten'
                  );
    $attr_nama_jns_tujuan = array('input_id' => 'id_jns_tujuan', 'input_name' => 'id_jns_tujuan', 'label_text' => 'Jenis Tujuan', 'array_opsi' => $opsi, 'opsi_selected' => '', 
                            'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_tujuan);

    $attr_nama_tujuan = array(
                                'input_id' => 'nama_tujuan', 
                                'input_name' => 'nama_tujuan' , 
                                'label_text' => 'Nama Tujuan :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_tujuan);

    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_rute = array(
                                'input_id' => 'rute', 
                                'input_name' => 'rute' , 
                                'label_text' => 'Rute :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_rute);
    
    $attr_kond_rute = array(
                                'input_id' => 'kond_rute', 
                                'input_name' => 'kond_rute' , 
                                'label_text' => 'Kondisi Rute :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kond_rute);

 ?>
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
      <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </div>
    </form>
  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
    $(".edit_button").on("click", function() {
            
            var idJnsTujuan = $(this).data('idJnsTujuan'),
                idAksesPelabuhan = $(this).data('idAksesPelabuhan');
                namaTujuan = $(this).data('namaTujuan');
                jarak = $(this).data('jarak');
                rute = $(this).data('rute'),
                kondRute = $(this).data('kondRute');

                $("#id_akses_pelabuhan").val(idAksesPelabuhan);
                $("#id_jns_tujuan").val(idJnsTujuan);
                $("#nama_tujuan").val(namaTujuan);
                $("#jarak").val(jarak);
                $("#rute").val(rute);
                $("#kond_rute").val(kondRute);


            $("#modal-edit-s6c").modal("show");
              
        });

      $('#tabel_S6C').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "25%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>