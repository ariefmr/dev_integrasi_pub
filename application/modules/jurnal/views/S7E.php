<div class="row">
  <div class="panel panel-text-referensi">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-7">
        <?php  
          $number = 1;
          $temp_gambar = '';
          if ($data_terbaru){
            // var_dump($data_terbaru);
              $array_info_kapal = array( 
                                              'Tanggal Catat : ' => array( 
                                                                      'id' => 'id_tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terbaru['tgl_catat']) ),
                                              'Tahun Pembuatan :' => array( 
                                                                      'id' => 'tahun',
                                                                      'value' => $data_terbaru['tahun'] ),
                                              'Kondisi Perairan :' => array(
                                                                      'id' => 'kondisi_perairan',
                                                                      'value' => $data_terbaru['kondisi_perairan'] ),
                                              'Kedalaman Rencana :' => array(
                                                                      'id' => 'dalam_rencana',
                                                                      'value' => $data_terbaru['dalam_rencana'] ),
                                              'Keterangan Rehabilitasi :' => array(
                                                                      'id' => 'ket_rehab',
                                                                      'value' => $data_terbaru['ket_rehab'] ),
                                              'Lebar Mulut Kolam :' => array( 
                                                                      'id' => 'lebar_mulut_kolam',
                                                                      'value' => $data_terbaru['lebar_mulut_kolam'] ),
                                              'Luas Kolam :' => array( 
                                                                      'id' => 'luas_kolam',
                                                                      'value' => $data_terbaru['luas_kolam'] ),
                                              'Kedalaman Sekarang :' => array( 
                                                                      'id' => 'dalam_sekarang',
                                                                      'value' => $data_terbaru['dalam_sekarang'] ),
                                              'Ket Konstruksi :' => array( 
                                                                      'id' => 'nama_jns_ket_konstruksi',
                                                                      'value' => $data_terbaru['nama_jns_ket_konstruksi'] ),
                                              'Sumber Dana :' => array( 
                                                                      'id' => 'sumber_dana',
                                                                      'value' => $data_terbaru['sumber_dana'] ),
                                              'Nilai :' => array( 
                                                                      'id' => 'nilai',
                                                                      'value' => $data_terbaru['nilai'] ),
                                              'Ket Sumber Dana : ' => array( 
                                                                      'id' => 'ket_sumber_dana',
                                                                      'value' => $data_terbaru['ket_sumber_dana'] )
                                           );
              $temp_gambar = $data_terbaru['gambar']; 
              echo $this->mkform->ref_text($array_info_kapal);
          }

         ?> 
        </div>
        <div class="col-lg-5">
          <div class="text-center">
          <?php if( !empty($temp_gambar) )
                {
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_uploads']."/".$temp_gambar ?>">
          <?php
                }else{
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_images']."/blank.png"; ?>">
          <?php
                }
          ?> 
            
          </div>
        </div>
      </div>
    </div>

  </div>

<?php 
    //$template = array ( "table_open" => "<table id='tabel_S7E' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S7E" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
        'No.',
        'Tgl Catat',
        'Tahun Pembuatan',
        'Kedalaman Rencana (m)',
        'Kondisi Perairan' ,
        'Ket Rehabilitasi',
        'Luas Kolam (m2)',
        'Lebar Mulut (m)',
        'Kedalaman Sekarang (m)',
        'Keterangan Konstruksi',
        'Sumber Dana',
        'Nilai (Rp)',
        'Keterangan Sumber Dana',
        'Gambar',
        'Aksi'
        )
    );

    $counter = 1;
    if ($list_S7E){
      foreach ($list_S7E as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S7E/hapus/'.$item->id_kolam_pelabuhan).'">Hapus</a>';
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_kolam_pelabuhan.'">Edit</button>';
        $item_gambar = '-';
        if( !empty($item->gambar) )
        {
          $html_img = "<img class='img-thumbnail' src='".$paths["pipp_uploads"]."/".$item->gambar."'>";
          $item_gambar = '<button type="button" class="btn btn-default gambar-pop" data-container="body" data-toggle="popover" data-placement="left" data-content="'.$html_img.'">
                            Gambar 
                          </button>';
        }
        $this->table->add_row(
            $counter,
            $this->pipp_lib->fmt_tgl($item->tgl_catat), 
            $item->tahun,
            $item->dalam_rencana,
            $item->kondisi_perairan,
            $item->ket_rehab, 
            $item->luas_kolam, 
            $item->lebar_mulut_kolam,
            $item->dalam_sekarang,
            $item->nama_jns_ket_konstruksi, 
            $item->sumber_dana,
            $item->nilai,
            $item->ket_sumber_dana,
            $item_gambar,
            $btn_edit."<br>".$link_delete
        );

        $counter++;  
      }
    }
    $tabel_S7E = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S7E; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s7e',
                             'modal_title' => 'Edit S7E',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S7E/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S7E').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>