<?php
    // var_dump($bulan_ini);

    $tmpl = array ( 'table_open'  => '<table id="table_B1" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Jasa', 'Satuan', 'Jumlah');

    $number = 1;

    if ($list_B1){
        // var_dump($list_B1);
        foreach ($list_B1 as $item) {
            
            //$link_delete = '<a class="btn btn-danger" href="'.base_url('entry/B1/hapus/'.$item->id_perbekalan).'">Hapus</a>';
                        
            $this->table->add_row($number.'. ',  
                                        $item->nama_jns_jasa, 
                                        $item->satuan, 
                                        number_format($item->jml,2, ',','.')
                                        );
            $number++;
        }
    }

    $table_B1 = $this->table->generate();
    $this->table->clear();

    //----------------------------------------------

    $tmpl = array ( 'table_open'  => '<table id="table_B1_non" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Jasa', 'Satuan', 'Jumlah', 'Deskripsi Lain');

    $number = 1;

    if ($list_B1_non){
        // var_dump($list_B1_non);
        foreach ($list_B1_non as $item) {
            
            //$link_delete = '<a class="btn btn-danger" href="'.base_url('entry/B1/hapus/'.$item->id_perbekalan).'">Hapus</a>';
                        
            $this->table->add_row($number.'. ',  
                                        $item->nama_jns_non_jasa, 
                                        $item->satuan, 
                                        number_format($item->jml,2, ',','.'),
                                        $item->desk_lain
                                        );
            $number++;
        }
    }

    $table_B1_non = $this->table->generate();
    $this->table->clear();

    $mons = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 
        6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 
        10 => "Oktober", 11 => "November", 12 => "Desember");

    $date = getdate();
    $month = $date['mon'];

    $month_name = $mons[$month];

?>

<!-- ROW table Jasa-->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Industri Jasa Pelabuhan, Bulan : <?php echo $month_name; ?></h3>
            </div>
            <?php 
            $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                                 'datepick_id' => 'datepicker_jurnal',
                                                 'default_text' => fmt_bulan_tahun($tmp_tgl_catat),
                                                 'input_name' => 'tgl_catat',
                                                 'input_value' => $tmp_tgl_catat.'-01'
                                                );
            echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);?>

            <button type="button" id="start_filter" class="btn btn-success">Filter</button> 
            
          <div class="panel-body overflowed">
            <?php

                echo $table_B1;

            ?>
          </div>
        </div>
    </div>
</div>

<!-- ROW table non Jasa-->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Industri Non Jasa Pelabuhan, Bulan : <?php echo $month_name; ?></h3>
            </div>
		  <div class="panel-body overflowed">
            <?php

                echo $table_B1_non;

            ?>
		  </div>
		</div>
	</div>
</div>

 <script>

    //set satuan
      function listenerSatuan() {

        var jenisPerbekalan = $(this).val(), 
            tipeSatuan = $('option:selected', this).data('tipeSatuan');
        
        console.log(tipeSatuan);
        $('#satuan').text(tipeSatuan);

      }

      var array_uri = ['jurnal','B1','index'];

        function start_filter()
        {
          var new_segments = array_uri.join("/"),
              new_url = site_url+new_segments+"/",
              get_tanggal = $('#datepicker_jurnal').val(),
              arr_tanggal = get_tanggal.split('-'),
              new_tanggal = arr_tanggal[0]+"/"+arr_tanggal[1],
              url_redirect = new_url+new_tanggal; 
        
          // console.log(url_redirect);
          window.open(url_redirect,'_self');
        }

    $(document).ready(function(){

        $("#start_filter").click(function(){
                    start_filter();
                });

        $('.select_perbekalan').on("change", listenerSatuan);

        $(".edit_button").on("click", function() {
            
            var idPerbekalan = $(this).data('idPerbekalan'),
                idPelabuhan = $(this).data('idPelabuhan'),
                idIndustri = $(this).data('idIndustri');
                idJnsPerbekalan = $(this).data('idJnsPerbekalan'),
                namaJnsPerbekalan = $(this).data('namaJnsPerbekalan'),
                namaIndustri = $(this).data('namaIndustri'),
                jml = $(this).data('jml'),
                satuan = $(this).data('satuan'),
                harga = $(this).data('harga')

                $("#id_perbekalan").val(idPerbekalan);
                $("#nama_jns_perbekalan").text(namaJnsPerbekalan);
                $("#nama_industri").text(namaIndustri);
                $("#satuan").text(satuan);
                $("#jml").val(jml);
                $("#satuan").val(satuan)
                $("#harga").val(harga);


            $("#modal-edit-B1").modal();
              
        });

        $('#table_B1').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "40%"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "45%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );

     	$('#table_B1_non').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "20%"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "40%", "sClass": "text-center"},
                            { "sWidth": "25%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );

    });
 </script>