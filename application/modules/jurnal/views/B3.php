<?php 
	$tmpl = array ( 'table_open'  => '<table id="table_B3" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Jenis Ikan', 'Kondisi', 'Jumlah Ikan (Kg)', 'Harga Ikan (Rp)','Tujuan / Lokasi','Tanggal Catat','Jenis Transportasi','Aksi');

    //Debugging
    //echo json_encode($list_B3);
    
    $number = 1;
    if ($list_B3){
        foreach ($list_B3 as $item) {
            $link_edit = '<a class="edit_button btn btn-warning" data-id-pemasaran-keluar="'.$item->id_pemasaran_keluar.'" 
                                                 data-nama-jenis-ikan="'.$item->nama_jenis_ikan.'"
                                                 data-id-jenis-kondisi-ikan="'.$item->id_jenis_kondisi_ikan.'"
                                                 data-jml-ikan="'.$item->jml_ikan.'"
                                                 data-harga-ikan="'.$item->harga_ikan.'"
                                                 data-kota-tujuan="'.$item->kota_tujuan.'"
                                                 data-jenis-transportasi="'.$item->jenis_transportasi.'"
                                                 data-nama-pengguna="'.$item->nama_pengguna.'"
                                                 data-id-kateg-tujuan="'.kos($item->id_kateg_tujuan,'1').'"
                                                 data-tanggal-catat="'.fmt_tgl($item->tgl_catat).'"
                            href="#">Edit</a>';
            $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/B3/hapus/'.$item->id_pemasaran_keluar).'">Hapus</a>';

                if(!empty($item->id_kateg_tujuan))
                {
                    $text_tujuan = character_limiter($item->nama_kateg_tujuan,7).' - '.$item->kota_tujuan;
                }else{
                    $text_tujuan = 'Data Perlu di Edit! (Data semula: '.$item->kota_tujuan.')';
                }

            $this->table->add_row($number.'. ', $item->nama_jenis_ikan, 
                                        $item->nama_jenis_kondisi_ikan, 
                                        fmt_angka($item->jml_ikan), 
                                        fmt_rupiah($item->harga_ikan), 
                                        $text_tujuan,
                                        fmt_tgl($item->tgl_catat), 
                                        $item->jenis_transportasi,
                                        $link_edit.' '.$link_delete );
            $number++;
        }
    }

    $table_B3 = $this->table->generate();
	$this->table->clear();
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
            </div>
		  <div class="panel-body overflowed">
            <?php 
            $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                               'datepick_id' => 'datepicker_jurnal',
                                               'default_text' => fmt_bulan_tahun($tmp_tgl_catat.'-01'),
                                               'input_name' => 'tgl_aktivitas',
                                               'input_value' => $tmp_tgl_catat.'-01'
                                              );
            echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
            ?>
            <button type="button" id="start_filter" class="btn btn-success">Ganti</button> 
		    <?php echo $table_B3; ?>
		  </div>
		</div>
	</div>
</div>
  <!-- Modal -->
  <div id="modal-edit-b3" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Pemasaran Keluar : <?php echo $nama_pelabuhan_info?> (<text id="tanggal_catat_edit"></text>)</h4>
    <small>Operator : <text id="nama_pengguna_edit"></text></small>
    </div>
        <div class="modal-body">
    <!-- FORM OPEN -->
    <?php
    echo form_open('entry/B3/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $tmpl = array ( 'table_open'  => '<table id="table_B3_edit" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Nama Jenis Ikan', 'Kondisi', 'Jumlah Ikan', 'Harga Ikan','Kategori','Lokasi Tujuan');

    $attr_id_pemasaran_keluar = array('input_id' => 'id_pemasaran_keluar', 
                                             'input_name' => 'id_pemasaran_keluar' , 
                                             'label_text' => '', 
                                             'input_value' => '', 
                                             'input_placeholder' => '',
                                             'input_type' => 'hidden', 
                                             'input_width' => 'hide', 
                                             'label_class' => 'hide', 
                                             'input_class' => 'form-control' );
    echo $this->mkform->input($attr_id_pemasaran_keluar);


    $text_nama_jenis_ikan = '<text id="nama_jenis_ikan" ></text> <input type="hidden" name="tgl_current" value="'.$tmp_tgl_catat.'">';

     $opsi_jenis_kondisi_ikan = Modules::run('tables/pipp_tbl_opsi', 
                                  'mst_jenis_kondisi_ikan',
                                  'id_jenis_kondisi_ikan',
                                  'nama_jenis_kondisi_ikan');

    $attr_opsi_kondisi_ikan = array('input_id' => 'id_jenis_kondisi_ikan', 'input_name' => 'id_jenis_kondisi_ikan', 'label_text' => '',
                            'array_opsi' => $opsi_jenis_kondisi_ikan,'field_id'=> 'id_jenis_kondisi_ikan', 'field_text' => 'nama_jenis_kondisi_ikan',
                             'opsi_selected' => 'segar', 'input_width' => 'col-lg-12', 'input_class' => 'form-control', 'label_class' => 'hide');
    $input_kondisi_ikan = $this->mkform->dropdown($attr_opsi_kondisi_ikan);
    
    $opsi_kategori = Modules::run('tables/pipp_tbl_opsi', 
                                  'mst_kateg_tujuan',
                                  'id_kateg_tujuan',
                                  'nama_kateg_tujuan');

    $kategori_1 = array('input_id' => 'id_kateg_tujuan', 'input_name' => 'id_kateg_tujuan', 'label_text' => '', 
        'array_opsi' => $opsi_kategori , 'field_id' => 'id_kateg_tujuan', 'field_text' => 'nama_kateg_tujuan',
        'opsi_selected' => '', 'input_width' => 'col-lg-12',
        'input_class' => 'form-control select-kategori', 'label_class' => 'hide');
    $input_kategori_tujuan = $this->mkform->dropdown($kategori_1);


    //content Input Berat    
    $input_tujuan = '<input id="kota_tujuan" name="kota_tujuan" type="text" class="" placeholder="">';

    $input_jml_ikan = '<input id="jml_ikan" name="jml_ikan" type="text" class="form-control" placeholder="">';
    
    //content input untuk Harga
    $input_harga_ikan = '<input id="harga_ikan" name="harga_ikan" type="text" class="form-control" placeholder="">';

    $this->table->add_row($text_nama_jenis_ikan, $input_kondisi_ikan, $input_jml_ikan, $input_harga_ikan, $input_kategori_tujuan, $input_tujuan);
    
    $table_B3_edit = $this->table->generate();
    echo $table_B3_edit;
 ?>
                            
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
      <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </div>
    </form>
  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

 <script>

    var array_uri = ['jurnal','B3','index'],
        list_opsi_ikan = <?php echo Modules::run('mst_jenis_ikan/json_ikan'); ?>,
        list_opsi_tujuan = [],
        list_opsi_export = <?php echo Modules::run('tables/dss_tbl_json',
                                                    'mst_negara',
                                                    'nama_negara',
                                                    'nama_negara');?>,
        list_opsi_lokal = <?php echo Modules::run('tables/dss_tbl_json',
                                                  'mst_kabupaten_kota',
                                                  'nama_kabupaten_kota',
                                                  'nama_kabupaten_kota',
                                                  'json');?>,
        list_opsi_regional = <?php echo Modules::run('tables/dss_tbl_json',
                                                     'mst_propinsi',
                                                     'nama_propinsi',
                                                     'nama_propinsi',
                                                     'json');?>,
        list_opsi_tujuan = ['',list_opsi_lokal, list_opsi_regional, list_opsi_export];

    
    function start_filter()
    {
        var new_segments = array_uri.join("/"),
            new_url = site_url+new_segments+"/",
            new_tanggal = $("#ganti_tanggal").data('tahun')+"/"+$("#ganti_tanggal").data('bulan'),
            url_redirect = new_url+new_tanggal; 
            
        // console.log(url_redirect);
        window.open(url_redirect,'_self');
    }

    function change_lokasi_tujuan(selected_kategori, selected_location)
    {
      $('#kota_tujuan').removeClass('form-control')
      $('#kota_tujuan').select2({ width: '100%',data: list_opsi_tujuan[selected_kategori] });
      
      if(selected_location !== '')
      {
        $('#kota_tujuan').select2('val',selected_location);
      }
    }

    $('document').ready(function(){
        $('#kota_tujuan').select2({ width: '100%',data: list_opsi_tujuan[1] });
     
        $('#table_B3_edit').on('change','.select-kategori', function(){
              var selected_kategori = $(this).val();
              change_lokasi_tujuan(selected_kategori,'');
        });

        $("#start_filter").click(function(){
            start_filter();
        });
        $(".edit_button").on("click", function() {
            
            var idPemasaranKeluar = $(this).data('idPemasaranKeluar'),
                namaJenisIkan = $(this).data('namaJenisIkan'),
                jmlIkan = $(this).data('jmlIkan'),
                idJenisKondisiIkan = $(this).data('idJenisKondisiIkan'),
                idKategTujuan = $(this).data('idKategTujuan'),
                kotaTujuan = $(this).data('kotaTujuan'),
                hargaIkan = $(this).data('hargaIkan'),
                namaPengguna = $(this).data('namaPengguna'),
                tglCatat = $(this).data('tanggalCatat');

    

                $("#nama_jenis_ikan").text(namaJenisIkan);
                $("#tanggal_catat_edit").text(tglCatat);
                $("#nama_pengguna_edit").text(namaPengguna);

                $("#id_pemasaran_keluar").val(idPemasaranKeluar);
                $("#id_jenis_kondisi_ikan").val(idJenisKondisiIkan);
                $("#id_kateg_tujuan").val(idKategTujuan);
                change_lokasi_tujuan(idKategTujuan, kotaTujuan);
                $("#jml_ikan").val(jmlIkan);
                $("#harga_ikan").val(hargaIkan);


            $("#modal-edit-b3").modal();
              
        });

     	$('#table_B3').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "25%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center","sType": "formatted-num"},
                            { "sWidth": "10%", "sClass": "text-center","sType": "formatted-currency"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );

      $('#table_B3_edit').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"}
                          ],
            "bFilter": false,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false, 
            "bSort": false
          } );

    });
 </script>