<?php 
	$tmpl = array ( 'table_open'  => '<table id="table_B4" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Industri', 'Jenis Hasil Olah', 'Jumlah', 'Harga','Nama Kategori Tujuan','Tujuan','Keterangan','Aksi');

    //Debugging
    //echo json_encode($list_B4);
    
    $number = 1;
    if ($list_B4){
        foreach ($list_B4 as $item) {
            $link_edit = '<a class="edit_button btn btn-warning" data-id-olah-ikan="'.$item->id_olah_ikan.'" 
                                                 data-nama-industri="'.$item->nama_industri.'"
                                                 data-nama-jns-hasil-olah="'.$item->nama_jns_hasil_olah.'"
                                                 data-jml="'.$item->jml.'"
                                                 data-harga="'.$item->harga.'"
                                                 data-nama-kateg-tujuan="'.$item->nama_kateg_tujuan.'"
                                                 data-tujuan="'.$item->tujuan.'"
                                                 data-ket="'.$item->ket.'"
                            href="#">Edit</a>';
            $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/B4/hapus/'.$item->id_olah_ikan).'">Hapus</a>';
            
            $this->table->add_row($number.'. ',  
                                        $item->nama_industri, 
                                        $item->nama_jns_hasil_olah, 
                                        $item->jml, 
                                        $item->harga, 
                                        $item->nama_kateg_tujuan, 
                                        $item->tujuan, 
                                        $item->ket, 
                                        /*$link_edit.' '.*/$link_delete );
            $number++;
        }
    }

    $table_B4 = $this->table->generate();
	$this->table->clear();
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
            </div>
		  <div class="panel-body overflowed">
		    <?php echo $table_B4; 
            $text_nama_industri = '<text id="nama_industri" ></text>';
            ?>
		  </div>
		</div>
	</div>
</div>
    <!-- FORM OPEN -->
    
  <!-- Modal -->
              <div id="modal-edit-B4" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Perusahaan : <?php echo $text_nama_industri?></h4>
                </div>
                    <div class="modal-body">
<?php
    echo form_open('entry/B4/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $tmpl = array ( 'table_open'  => '<table id="table_B4_edit" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Perbekalan', 'Satuan', 'Jumlah', 'Harga');

    $attr_id_olah_ikan = array('input_id' => 'id_olah_ikan', 
                                             'input_name' => 'id_olah_ikan' , 
                                             'label_text' => '', 
                                             'input_value' => '', 
                                             'input_placeholder' => '',
                                             'input_type' => 'hidden', 
                                             'input_width' => 'hide', 
                                             'label_class' => 'hide', 
                                             'input_class' => 'form-control' );
    echo $this->mkform->input($attr_id_olah_ikan);

    $text_perbekalan = '<text id="jml" ></text>';
    $text_satuan = '<text id="satuan" ></text>';

    //$opsi_perbekalan = echo Modules::run('tables/pipp_list_all', 'mst_jenis_perbekalan');
    //dropdown untuk kondisi ikan
    $attr_opsi_jenis_perbekalan = array('input_id' => 'id_jns_perbekalan', 
                                    'input_name' => 'id_jns_perbekalan', 
                                    'label_text' => '', 
                                    'array_opsi' => '', 
                                    'opsi_selected' => '', 
                                    'input_width' => '', 
                                    'input_class' => 'form-control select_perbekalan', 
                                    'label_class' => 'none');
    $input_jns_perbekalan = $this->mkform->pilih_perbekalan($attr_opsi_jenis_perbekalan);
    
    //content Input Berat    
    $input_jml = '<input id="jml" name="jml" type="text" class="form-control" placeholder="">';
    
    //content input untuk Harga
    $input_harga = '<input id="harga" name="harga" type="text" class="form-control" placeholder="">';

    $this->table->add_row($text_perbekalan, $text_satuan, $input_jml, $input_harga);
    
    $table_B4_edit = $this->table->generate();
    echo $table_B4_edit;
 ?>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
    </form>
                  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

 <script>

    //set satuan
      function listenerSatuan() {

        var jenisPerbekalan = $(this).val(), 
            tipeSatuan = $('option:selected', this).data('tipeSatuan');
        
        console.log(tipeSatuan);
        $('#satuan').text(tipeSatuan);

      }

    $(document).ready(function(){
        $('.select_perbekalan').on("change", listenerSatuan);

        $(".edit_button").on("click", function() {
            
            var idPerbekalan = $(this).data('idPerbekalan'),
                idPelabuhan = $(this).data('idPelabuhan'),
                idIndustri = $(this).data('idIndustri');
                idJnsPerbekalan = $(this).data('idJnsPerbekalan'),
                namaJnsPerbekalan = $(this).data('namaJnsPerbekalan'),
                namaIndustri = $(this).data('namaIndustri'),
                jml = $(this).data('jml'),
                satuan = $(this).data('satuan'),
                harga = $(this).data('harga')

                $("#id_olah_ikan").val(idPerbekalan);
                $("#jml").text(namaJnsPerbekalan);
                $("#nama_industri").text(namaIndustri);
                $("#satuan").text(satuan);
                $("#jml").val(jml);
                $("#satuan").val(satuan)
                $("#harga").val(harga);


            $("#modal-edit-B4").modal();
              
        });

     	$('#table_B4').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"},
                            { "sWidth": "5%", "sClass": "text-center"},
                            { "sWidth": "5%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
          } );

      $('#table_B4_edit').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "40%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false, 
            "bSort": true
          } );

    });
 </script>