<div class="row">
  <div class="panel panel-text-referensi">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-7">
        <?php  
          $number = 1;
          $temp_gambar = '';
          if ($data_terbaru){
            // var_dump($data_terbaru);
              $array_info_kapal = array( 
                                              'Tanggal Catat : ' => array( 
                                                                      'id' => 'id_tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terbaru['tgl_catat']) ),
                                              'Tahun Pembuatan :' => array( 
                                                                      'id' => 'tahun',
                                                                      'value' => $data_terbaru['tahun'] ),
                                              'Nama Dermaga :' => array( 
                                                                      'id' => 'nama_dermaga',
                                                                      'value' => $data_terbaru['nama_dermaga'] ),
                                              'Nama Fasilitas :' => array( 
                                                                      'id' => 'nama_fas',
                                                                      'value' => $data_terbaru['nama_fas'] ),
                                              'Jenis Dermaga :' => array( 
                                                                      'id' => 'jns_dermaga',
                                                                      'value' => $data_terbaru['jns_dermaga'] ),
                                              'Tipe Dermaga :' => array( 
                                                                      'id' => 'tipe_dermaga',
                                                                      'value' => $data_terbaru['tipe_dermaga'] ),
                                              'Lampu :' => array( 
                                                                      'id' => 'is_lampu',
                                                                      'value' => $data_terbaru['is_lampu'] ),
                                              'Posisi Tambat :' => array( 
                                                                      'id' => 'posisi_tambat',
                                                                      'value' => $data_terbaru['posisi_tambat'] ),
                                              'Jenis Fender :' => array( 
                                                                      'id' => 'jns_fender',
                                                                      'value' => $data_terbaru['jns_fender'] ),
                                              'Jenis Bolard :' => array( 
                                                                      'id' => 'jns_bolard',
                                                                      'value' => $data_terbaru['jns_bolard'] ),
                                              'Deskripsi Dermaga :' => array( 
                                                                      'id' => 'desk_dermaga',
                                                                      'value' => $data_terbaru['desk_dermaga'] ),
                                              'Keterangan Rehabilitasi :' => array(
                                                                      'id' => 'ket_rehab',
                                                                      'value' => $data_terbaru['ket_rehab'] ),
                                              'Jenis Konstruksi : ' => array( 
                                                                      'id' => 'jns_konstruksi',
                                                                      'value' => $data_terbaru['jns_konstruksi'] ),
                                              'Panjang :' => array(
                                                                      'id' => 'panjang',
                                                                      'value' => $data_terbaru['panjang'] ),
                                              'Lebar :' => array( 
                                                                      'id' => 'lebar',
                                                                      'value' => $data_terbaru['lebar'] ),
                                              'Elevasi :' => array( 
                                                                      'id' => 'elevasi',
                                                                      'value' => $data_terbaru['elevasi'] ),
                                              'Sumber Dana :' => array( 
                                                                      'id' => 'sumber_dana',
                                                                      'value' => $data_terbaru['sumber_dana'] ),
                                              'Nilai :' => array( 
                                                                      'id' => 'nilai',
                                                                      'value' => $data_terbaru['nilai'] ),
                                              'Ket Sumber Dana : ' => array( 
                                                                      'id' => 'ket_sumber_dana',
                                                                      'value' => $data_terbaru['ket_sumber_dana'] )
                                           );
              $temp_gambar = $data_terbaru['gambar']; 
              echo $this->mkform->ref_text($array_info_kapal);
          }

         ?> 
        </div>
        <div class="col-lg-5">
          <div class="text-center">
          <?php if( !empty($temp_gambar) )
                {
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_uploads']."/".$temp_gambar ?>">
          <?php
                }else{
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_images']."/blank.png"; ?>">
          <?php
                }
          ?> 
            
          </div>
        </div>
      </div>
    </div>

  </div>

<?php 

    //$template = array ( "table_open" => "<table id='tabel_S7D' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S7D" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
                                    'No.',
                                    'Tgl Catat',
                                    'Tahun Di Bangun',
                                    'Jenis Fasilitas', 
                                    'Nama Dermaga / Jetty',
                                    'Jenis Dermaga / Jetty',
                                    'Tipe Dermaga / Jetty',
                                    'Lampu',
                                    'Posisi Tambat',
                                    'Jenis Fender',
                                    'Jenis Bolard',
                                    'Deskripsi',
                                    'Ket Rehabilitas',
                                    'Jenis Konstruksi',
                                    'Panjang',
                                    'Lebar',
                                    'Elevasi',
                                    'Sumber Dana',
                                    'Nilai',
                                    'Ket Sumber Dana',
                                    'Gambar',
                                    'Aksi'
                                )
                              );

    $counter = 1;
    if ($list_S7D){
      foreach ($list_S7D as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S7D/hapus/'.$item->id_dermaga).'">Hapus</a>';
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_dermaga.'">Edit</button>';
        $item_gambar = '-';
        if( !empty($item->gambar) )
        {
          $html_img = "<img class='img-thumbnail' src='".$paths["pipp_uploads"]."/".$item->gambar."'>";
          $item_gambar = '<button type="button" class="btn btn-default gambar-pop" data-container="body" data-toggle="popover" data-placement="left" data-content="'.$html_img.'">
                            Gambar 
                          </button>';
        }
        $this->table->add_row(
                                $counter,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $item->tahun,
                                $item->nama_fas,
                                $item->nama_dermaga,
                                $item->jns_dermaga,
                                $item->tipe_dermaga,
                                $item->is_lampu,
                                $item->posisi_tambat,
                                $item->jns_fender,
                                $item->jns_bolard,
                                $item->desk_dermaga,
                                $item->ket_rehab, 
                                $item->jns_konstruksi,
                                $item->panjang,
                                $item->lebar, 
                                $item->elevasi, 
                                $item->sumber_dana, 
                                $item->nilai,
                                $item->ket_sumber_dana,
                                $item_gambar,
                                $btn_edit."<br>".$link_delete

                              );

        $counter++;  
      }
    }
    $tabel_S7D = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S7D; ?>
      </div>
    </div>
  </div>
</div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s7d',
                             'modal_title' => 'Edit S7D',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S7D/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S7D').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "30%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>