<?php 

    //$template = array ( "table_open" => "<table id='tabel_S12' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S12" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array('No.','Nama Lembaga','Alamat','Nama Pimpinan','Jumlah Tenaga Kerja','Tgl Catat','Aksi')); 

    $counter = 1;
    if ($list_S12){
      foreach ($list_S12 as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S12/hapus/'.$item->id_lembaga).'">Hapus</a>';
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_lembaga.'">Edit</button>';
        $this->table->add_row(
                                $counter,
                                $item->nama_lembaga,
                                $item->alamat, 
                                $item->nama_pimpinan,
                                $item->jml_tenaga,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $btn_edit."<br>".$link_delete
                              );

        $counter++;  
      }
    }
    $tabel_S12 = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S12; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s12',
                             'modal_title' => 'Edit S12',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S12/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>
<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S12').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>