<div class="row">
  <div class="panel panel-text-referensi">
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-7">
        <?php  
          $number = 1;
          $temp_gambar = '';
          if ($data_terbaru){
            // var_dump($data_terbaru);
              $array_info_kapal = array( 
                                              'Tanggal Catat : ' => array( 
                                                                      'id' => 'id_tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terbaru['tgl_catat']) ),
                                              'Tahun Pembuatan :' => array( 
                                                                      'id' => 'tahun',
                                                                      'value' => $data_terbaru['tahun'] ),
                                              'Jenis Garasi :' => array( 
                                                                      'id' => 'nama_jns_garasi',
                                                                      'value' => $data_terbaru['nama_jns_garasi'] ),
                                              'Keterangan Rehabilitasi :' => array(
                                                                      'id' => 'ket_rehab',
                                                                      'value' => $data_terbaru['ket_rehab'] ),
                                              'Jenis Konstruksi : ' => array( 
                                                                      'id' => 'jns_konstruksi',
                                                                      'value' => $data_terbaru['jns_konstruksi'] ),
                                              'Luas(m2) :' => array(
                                                                      'id' => 'luas',
                                                                      'value' => $data_terbaru['luas'] ),
                                              'Ket Konstruksi :' => array( 
                                                                      'id' => 'nama_jns_ket_konstruksi',
                                                                      'value' => $data_terbaru['nama_jns_ket_konstruksi'] ),
                                              'Sumber Dana :' => array( 
                                                                      'id' => 'sumber_dana',
                                                                      'value' => $data_terbaru['sumber_dana'] ),
                                              'Nilai(Rp) : ' => array( 
                                                                      'id' => 'nilai',
                                                                      'value' => $data_terbaru['nilai'] ),
                                              'Ket Sumber Dana : ' => array( 
                                                                      'id' => 'ket',
                                                                      'value' => $data_terbaru['ket'] )
                                           );
              $temp_gambar = $data_terbaru['gambar']; 
              echo $this->mkform->ref_text($array_info_kapal);
          }

         ?> 
        </div>
        <div class="col-lg-5">
          <div class="text-center">
          <?php if( !empty($temp_gambar) )
                {
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_uploads']."/".$temp_gambar ?>">
          <?php
                }else{
          ?>
              <img class="img-thumbnail image-reftext" src="<?php echo $paths['pipp_images']."/blank.png"; ?>">
          <?php
                }
          ?> 
            
          </div>
        </div>
      </div>
    </div>

  </div>
<?php 

    //$template = array ( "table_open" => "<table id='tabel_S8S' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S8S" class="table table-bordered">' );
    $this->table->set_template($template);
      $this->table->set_heading(array('No.','Jenis Garasi','Tahun', 'Ket Rehabilitasi','Jenis Perkerasan','Luas(m2)',
        'Keterangan Konstruksi','Sumber Dana','Nilai (Rp)','Keterangan Sumber Dana','Tgl Catat','Gambar','Aksi')); 

    $counter = 1;
    if ($list_S8S){
      foreach ($list_S8S as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S8S/hapus/'.$item->id_garasi).'">Hapus</a>';
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_garasi.'">Edit</button>';
        $item_gambar = '-';
        if( !empty($item->gambar) )
        {
          $html_img = "<img class='img-thumbnail' src='".$paths["pipp_uploads"]."/".$item->gambar."'>";
          $item_gambar = '<button type="button" class="btn btn-default gambar-pop" data-container="body" data-toggle="popover" data-placement="left" data-content="'.$html_img.'">
                            Gambar 
                          </button>';
        }
        $this->table->add_row(
          $counter,
          $item->nama_jns_garasi, 
          $item->tahun,
          $item->ket_rehab,
          $item->jns_konstruksi,
          $item->luas, 
          $item->nama_jns_ket_konstruksi, 
          $item->sumber_dana,
          $item->nilai,
          $item->ket,
          $this->pipp_lib->fmt_tgl($item->tgl_catat),
          $item_gambar,
          $btn_edit." ".$link_delete
          );
        $counter++;
      }
    }
    $tabel_S8S = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S8S; ?>
      </div>
    </div>
  </div>
</div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s8s',
                             'modal_title' => 'Edit S8S',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S8S/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S8S').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>