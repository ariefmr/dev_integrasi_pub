<?php 

    //$template = array ( "table_open" => "<table id='tabel_s6d' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_s6d" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
                                      'No.',
                                      'Tgl Catat',
                                      'Jenis Fasilitas Transportasi',
                                      'Nama Fasilitas',
                                      'Jenis Sarana Transportasi',
                                      'Jarak (km)',
                                      'Kota Tujuan',
                                      'Frekuensi',
                                      'Aksi'
                                      )
                              );

    $counter = 1;
    if ($list_S6D){
      foreach ($list_S6D as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S6D/hapus/'.$item->id_fas_transportasi).'">Hapus</a>';
        /*$link_edit = '<a class="edit_button btn btn-warning" 
                                                data-id-fas-transportasi="'.$item->id_fas_transportasi.'"
                                                data-id-jns-transportasi="'.$item->id_jns_transportasi.'"
                                                data-nama="'.$item->nama.'"
                                                data-jenis="'.$item->jenis.'"
                                                data-jarak="'.$item->jarak.'"
                                                data-kota-tujuan="'.$item->kota_tujuan.'"
                                                data-frekuensi="'.$item->frekuensi.'"
                            href="#">Edit</a>';*/
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_fas_transportasi.'">Edit</button>';
        $this->table->add_row(
                                $counter,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $item->nama_jns_transportasi,
                                $item->nama, 
                                $item->jenis,
                                $item->jarak,
                                $item->kota_tujuan,
                                $item->frekuensi, 
                                $btn_edit.' '.$link_delete
                              );

        $counter++;  
      }
    }
    $tabel_s6d = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_s6d; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s6d',
                             'modal_title' => 'Edit S6D',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S6D/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

<!-- Modal -->
  <div id="modal-edit-s6d" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Pelabuhan : <?php //echo $text_nama_industri?></h4>
    </div>
        <div class="modal-body">
     <!-- FORM OPEN -->
    <?php
    echo form_open('entry/S6D/ubah', 'id="form_entry" class="form-horizontal" role="form"');

    $id_fas_transportasi = array(
                                'input_id' => 'id_fas_transportasi', 
                                'input_name' => 'id_fas_transportasi' , 
                                'label_text' => '', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'hide' 
                                );
    echo $this->mkform->input($id_fas_transportasi);

    $opsi = array(
                   '1' => 'Kereta Api',
                   '2' => 'Bus Antar Kota',
                   '3' => 'Pesawat Terbang'
                  );
    $attr_nama_jns_transportasi = array('input_id' => 'id_jns_transportasi', 'input_name' => 'id_jns_transportasi', 'label_text' => 'Jenis Fasilitas Transportasi', 'array_opsi' => $opsi, 'opsi_selected' => '', 'input_width' => 'col-lg-6', 'input_class' => 'form-control', 'label_class' => 'col-lg-4 control-label');
    echo $this->mkform->dropdown($attr_nama_jns_transportasi);


    $attr_nama_fasilitas = array(
                                'input_id' => 'nama', 
                                'input_name' => 'nama' , 
                                'label_text' => 'Nama Fasilitas :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_nama_fasilitas);

    $attr_jenis = array(
                                'input_id' => 'jenis', 
                                'input_name' => 'jenis' , 
                                'label_text' => 'Jenis :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jenis);
    
    $attr_jarak = array(
                                'input_id' => 'jarak', 
                                'input_name' => 'jarak' , 
                                'label_text' => 'Jarak :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_jarak);
    
    $attr_kota_tujuan = array(
                                'input_id' => 'kota_tujuan', 
                                'input_name' => 'kota_tujuan' , 
                                'label_text' => 'Kota Tujuan :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_kota_tujuan);

    $attr_frekuensi = array(
                                'input_id' => 'frekuensi', 
                                'input_name' => 'frekuensi' , 
                                'label_text' => 'Frekuensi :', 
                                'input_value' => '', 
                                'input_placeholder' => '',
                                'input_type' => 'text', 
                                'input_width' => 'col-lg-6 manual_input', 
                                'label_class' => 'col-lg-4 manual_input', 
                                'input_class' => 'form-control' 
                                );
    echo $this->mkform->input($attr_frekuensi);

 ?>
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
      <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </div>
    </form>
  </div><!-- /.modal-content -->
    <!-- FORM CLOSE -->

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $(".edit_button").on("click", function() {
            
            var idJnsTransportasi = $(this).data('idJnsTransportasi'),
                idFasTransportasi = $(this).data('idFasTransportasi');
                nama = $(this).data('nama');
                jenis = $(this).data('jenis');
                jarak = $(this).data('jarak');
                kotaTujuan = $(this).data('kotaTujuan');
                frekuensi = $(this).data('frekuensi');

                $("#id_fas_transportasi").val(idFasTransportasi);
                $("#id_jns_transportasi").val(idJnsTransportasi);
                $("#nama").val(nama);
                $("#jenis").val(jenis);
                $("#jarak").val(jarak);
                $("#kota_tujuan").val(kotaTujuan);
                $("#frekuensi").val(frekuensi);


            $("#modal-edit-s6d").modal("show");
              
        });

      $('#tabel_s6d').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "5%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>