<?php 

    //$template = array ( "table_open" => "<table id='tabel_S1' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_S1" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
      'No.',
      'Nama Industri', 
      'Nama Pemilik', 
      'Jenis Badan Usaha', 
      'Alamat',
      'Nilai Investasi Total', 
      'Jumlah Tenaga Kerja',
      'Aksi')
    );

    $counter = 1;
    if ($list_S1){
      foreach ($list_S1 as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S1/hapus/'.$item->id_industri).'">Hapus</a>';
        $this->table->add_row(
                                $counter,
                                $item->nama_industri, 
                                $item->nama_pemilik,
                                $item->nama_jns_badan_usaha, 
                                $item->alamat,
                                $item->nilai_investasi_produsen, 
                                $item->jml_tenaga_kerja, 
                                $link_delete
                              );

        $counter++;  
      }
    }
    $tabel_S1 = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_S1; ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S1').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>