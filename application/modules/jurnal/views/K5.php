<div class="row">
<?php 

    //$template = array ( "table_open" => "<table id='tabel_K5' border='0' cellpadding='4' cellspacing='0'>" );
    $template = array ( 'table_open' => '<table id="tabel_K5" class="table table-bordered">' );
    $this->table->set_template($template);
    $this->table->set_heading(array(
                                    'No.',
                                    'Tgl Catat',
                                    'Tipe K5', 
                                    'Kategori',
                                    'Uraian',
                                    'Hasil Pengamatan',
                                    'Permasalahan',
                                    'Tindak Lanjut',
                                    'Aksi'
                                )
                              );

    $counter = 1;
    if ($list_K5){
      foreach ($list_K5 as $item) {
        $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/K5/hapus/'.$item->id_k5).'">Hapus</a>';
        $btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_k5.'">Edit</button>';
        
        $this->table->add_row(
                                $counter,
                                $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                $item->tipe,
                                $item->kategori, 
                                $item->uraian,
                                $item->hasil_pengamatan,
                                $item->permasalahan, 
                                $item->tindak_lanjut,
                                $link_delete.' '.$btn_edit
                              );

        $counter++;  
      }
    }
    $tabel_K5 = $this->table->generate();
    $this->table->clear();

 ?> 

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_catat); ?></h3>
      </div>
      <div class="panel-body overflowed">
        <?php echo $tabel_K5; ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_k5',
                             'modal_title' => 'Edit K5',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/K5/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>
<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });

      $('#tabel_K5').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>