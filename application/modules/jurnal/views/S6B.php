<div class="row">

      <?php 
      if ($data_terakhir){
              // var_dump($data_terakhir);
              $array_info_kapal_1 = array( 
                                             'Tanggal Catat : ' => array( 'id' => 'tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terakhir['tgl_catat']) ), 
                                             'Luas Perairan :' => array( 'id' => 'luas_perairan',
                                                                      'value' => $data_terakhir['luas_perairan'].' Ha'),
                                             'Luas Lahan Daratan :' => array( 'id' => 'luas_daratan',
                                                                      'value' => $data_terakhir['luas_daratan'].' Ha'), 
                                           );
              $array_info_kapal_2 = array( 
                                              'Tanggal Catat : ' => array( 'id' => 'tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terakhir['tgl_catat']) ),
                                              '-Kondisi Siap bangun: ' => array( 'id' => 'luas_industri_siap',
                                                                      'value' => $data_terakhir['luas_industri_siap'].' m2' ),
                                              '-Sudah Terpakai: ' => array( 'id' => 'luas_industri_terpakai',
                                                                      'value' => $data_terakhir['luas_industri_terpakai'].' m2' ),
                                              '-Belum Siap Bangun: ' => array( 'id' => 'luas_industri_belum',
                                                                      'value' => $data_terakhir['luas_industri_belum'].' m2' )
                                           );
              $array_info_kapal_3 = array( 
                                              'Tanggal Catat : ' => array( 'id' => 'tgl_catat',
                                                                      'value' => $this->pipp_lib->fmt_tgl($data_terakhir['tgl_catat']) ),
                                              '-Kondisi Siap Bangun: ' => array( 'id' => 'luas_fas_siap',
                                                                      'value' => $data_terakhir['luas_fas_siap'].' m2' ),
                                              '-Sudah terpakai: ' => array( 'id' => 'luas_fas_terpakai',
                                                                      'value' => $data_terakhir['luas_fas_terpakai'].' m2' ),
                                              'Belum Siap Bangun: ' => array( 'id' => 'luas_fas_belum',
                                                                      'value' => $data_terakhir['luas_fas_belum'].' m2' )
                                           );
              $lahan_industri = $data_terakhir['luas_industri_siap'] + $data_terakhir['luas_industri_belum'];
              $lahan_fasilitas = $data_terakhir['luas_fas_siap'] + $data_terakhir['luas_fas_belum'];

?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Luas Perairan dan Daratan</h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-12">
        <?php           
          echo $this->mkform->ref_text($array_info_kapal_1);
        ?> 
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Luas Lahan untuk Industri: <?php echo $lahan_industri." m2"; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-12">
        <?php
          echo $this->mkform->ref_text($array_info_kapal_2);
        ?> 
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Luas Lahan untuk Fasilitas: <?php echo $lahan_fasilitas." m2"; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-12">
        <?php
          echo $this->mkform->ref_text($array_info_kapal_3);
        ?> 
        </div>
      </div>
    </div>

  </div>
<?php
}else{
?> 
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Luas Perairan dan Daratan</h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-lg-12">
        <?php           
          //echo $this->mkform->ref_text($array_info_kapal_1);
        ?> 
        </div>
      </div>
    </div>
  </div>
<?php
}
  
            $tmpl = array ( 'table_open'  => '<table id="table_S6B" class="table table-bordered">' );
            $this->table->set_template($tmpl);

            $no = array('data' => 'No.', 'rowspan' => 2);
            $cell_1 = array('data' => 'Luas Daerah Perairan (m2)', 'rowspan' => 2);
            $cell_2 = array('data' => 'Luas Lahan Daratan (m2)', 'rowspan' => 2);
            
            /*$colspan_1 = array( 
                                // $field,
                                'data' => 'Luas Lahan Untuk Industri', 
                                'colspan' => 3,
                                $this->table->add_row(
                                  'Kondisi Siap Bangun (m2)',
                                  'Sudah Terpakai (m2)',
                                  'Kondisi Siap Bangun (m2)'
                                  )
                            );*/
            /*$colspan_2 = array(
                                // $field,
                                'data' => array(
                                                '1' => 'testing', 
                                                '2' => $this->table->add_row(
                                                    'Kondisi Siap Bangun (m2)',
                                                    'Sudah Terpakai (m2)',
                                                    'Kondisi Siap Bangun (m2)'
                                                )
                                          ), 
                                'colspan' => 3
                            );*/

           /* $this->table->set_heading(
              $no, 
              $cell_1, 
              $cell_2,
              array(
                  $field = $this->table->add_row(
                    'Kondisi Siap Bangun (m2)',
                    'Sudah Terpakai (m2)',
                    'Kondisi Siap Bangun (m2)'
                    ),
                  'data' => 'Luas Lahan Untuk Fasilitas', 
                  'colspan' => 3
              ),
              array(
                  $field = $this->table->add_row(
                    'Kondisi Siap Bangun (m2)',
                    'Sudah Terpakai (m2)',
                    'Kondisi Siap Bangun (m2)'
                    ),
                  'data' => 'Luas Lahan Untuk Fasilitas', 
                  'colspan' => 3
              )
              );*/
            // $this->table->set_heading($colspan_1, $colspan_2);
            // $this->table->set_heading($no, $cell_1, $cell_2, $cell_4, $cell_5, $cell_6, $cell_4, $cell_5, $cell_6 );
            $this->table->set_heading('No.', 'Tanggal Catat', 'Luas Daerah Perairan (m2)', 'Luas Lahan Daratan (m2)', 'Kondisi Siap Bangun (m2)', 'Sudah Terpakai (m2)', 
              'Belum Siap Bangun (m2)', 'Kondisi Siap Bangun (m2)', 'Sudah Terpakai (m2)', 'Belum Siap Bangun (m2)', 'Aksi');

            $number = 1;

            if ($list_S6B){
                foreach ($list_S6B as $item) {
                    
                    $link_delete = '<a class="btn btn-danger" href="'.base_url('entry/S6B/hapus/'.$item->id_luas_pelabuhan).'">Hapus</a>';
                    //$btn_edit = '<button class="edit_this btn btn-warning" data-id-record="'.$item->id_luas_pelabuhan.'">Edit</button>';
        
                    $this->table->add_row(
                                          $number.'. ',  
                                          $this->pipp_lib->fmt_tgl($item->tgl_catat), 
                                          $item->luas_perairan, 
                                          $item->luas_daratan, 
                                          $item->luas_industri_siap,
                                          $item->luas_industri_terpakai,
                                          $item->luas_industri_belum,
                                          $item->luas_fas_siap,
                                          $item->luas_fas_terpakai,
                                          $item->luas_fas_belum,
                                          $link_delete    
                                          );
                    $number++;
                }
            }

            $table_S6B = $this->table->generate();
            $this->table->clear();
          ?>



      <!-- ROW table -->
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Histori</h3>
            </div>
      <div class="panel-body overflowed">
            <?php

                echo $table_S6B;

            ?>
      </div>
    </div>
  </div>
</div>

<?php
    $attr_modal_edit = array('modal_id' => 'modal_edit_s6b',
                             'modal_title' => 'Edit S6B',
                             'modal_width' => '760',
                             'link_view_form' => base_url('entry/S6B/edit')
                            ); 
    echo $this->mkform->modal_edit($attr_modal_edit);
?>

</div>

<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#table_S6B').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "10%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"},
                        { "sWidth": "20%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>