<?php 	
	$tmpl = array ( 'table_open'  => '<table id="table_h2" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.', 'Nama Kapal', 'Hasil Tangkapan', 'Total Tangkapan','Tanggal Bongkar','Aksi');

    $number = 1;
    	$list_per_kapal= array();
    if($list_h2) {
		foreach ($list_h2 as $item) {
			$id_kapal = $item->id_kapal;
			$id_produksi = $item->id_produksi;
			$list_per_kapal[$id_kapal][$id_produksi] = $item;
		}
		
		foreach ($list_per_kapal as $id_kapal => $array_produksi_per_kapal) {
			$tmp_nama_kapal = '';
			$tmp_id_kapal = $id_kapal;
			
			$tmp_array_tangkapan = array();
			$tmp_total = 0;
				foreach ($array_produksi_per_kapal as $per_record) {
					$tmp_nama_kapal = empty($per_record->nama_kapal_entry) ? $per_record->nama_kapal_ref : $per_record->nama_kapal_entry;
					$link_edit = '<a href="#" class="btn btn-warning btn_edit" data-nama-kapal="'.$tmp_nama_kapal.'" data-index-number="'.$number.'" data-nama-pengguna="'.$per_record->nama_pengguna.'">Edit</a>';					
					$per_ikan = '<a href="#" class="btn btn-small btn-default per_ikan produksiBaris_'.$number.'" '.
								'data-id-produksi="'.$per_record->id_produksi.'"'.
								'data-nama-jenis-ikan="'.$per_record->nama_jenis_ikan.'"'.
                'data-nama-jenis-ikan-inggris="'.$per_record->nama_jenis_ikan_inggris.'"'.
								'data-id-jenis-ikan="'.$per_record->id_jenis_ikan.'"'.
								'data-id-jenis-kondisi-ikan="'.$per_record->id_jenis_kondisi_ikan.'"'.
								'data-jml-ikan="'.$per_record->jml_ikan.'"'.
								'data-harga-produsen="'.$per_record->harga_produsen.'"'.
								'data-harga-pedagang="'.$per_record->harga_pedagang.'"'
								.'>'.$per_record->nama_jenis_ikan.' ( '.$per_record->jml_ikan.' kg) '
								.'</a>';
					array_push($tmp_array_tangkapan, $per_ikan);
					$tmp_total += $per_record->jml_ikan;
          $tmp_koef_koreksi = $per_record->koefisien_koreksi;
				}

			$this->table->add_row($number.'. ',$tmp_nama_kapal, join(" ",$tmp_array_tangkapan), $tmp_total.' Kg <br><small><em>*</em>('.$tmp_koef_koreksi.'% )<small>',fmt_tgl($per_record->tgl_aktivitas), $link_edit);
			$number++;
		}
	}

    $table_h2 = $this->table->generate();
    $this->table->clear();
	
    
 ?>
<!-- ROW table -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Tanggal : <?php echo $this->pipp_lib->fmt_tgl($tmp_tgl_aktivitas); ?></h3>
		  </div>
		  <div class="panel-body">
          <?php 
          $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                               'datepick_id' => 'datepicker_jurnal',
                                               'default_text' => fmt_bulan_tahun($tmp_tgl_aktivitas),
                                               'input_name' => 'tgl_aktivitas',
                                               'input_value' => $tmp_tgl_aktivitas.'-01'
                                              );
          echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
          ?>

        <button type="button" id="start_filter" class="btn btn-success">Ganti</button> 
		    <?php echo $table_h2; ?>
		  </div>
		</div>
	</div>
</div>

<div id="modal-edit-h2" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Produksi Kapal <text id="nama_kapal_edit"></text></h4>
    <small>(operator: <text id="nama_pengguna_edit">FULAN</text> )</small>
  </div>
  <div class="modal-body">
  			<?php 
  			echo form_open('entry/H2/ubah', 'id="form_edit"');
  			$tmpl = array ( 'table_open'  => '<table id="table_h2_edit" class="table table-bordered">' );
		    $this->table->set_template($tmpl);
		    $this->table->set_heading('Jenis Ikan', 'Kondisi','Jumlah (Kg)', 'Harga Produsen (Rp)','Harga Pedagang (Rp)','Hapus');

		    $table_h2_edit = $this->table->generate();
		    echo $table_h2_edit;
  			 ?>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default pull-left">Batal</button>
    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </form>
  </div>
</div>


<div id="template_inputs" class="hide">0
.<?php
    /* Elemen ini dipakai javascript untuk di clone atau diduplikasi
     */
    //heading('No.', 'Jenis Ikan', 'Kondisi','Jumlah (KG)','Harga Produsen','Harga Pedagang');

    //dropdown untuk kondisi ikan
    // $opsi_kondisi_ikan = array('1' => 'Segar',
    //                            '2' => 'Beku',
    //                            '3' => 'Hidup',
    //                            '4' => 'Asin' );
    $attr_opsi_kondisi_ikan = array('input_id' => 'kondisi_1', 'input_name' => 'kondisi_1',
                                   'label_text' => '', 'array_opsi' => '', 'opsi_selected' => 'segar', 
                                  'input_width' => '', 'input_class' => 'form-control test', 'label_class' => 'none',
                                  'from_table' => 'mst_jenis_kondisi_ikan', 'field_value' => 'id_jenis_kondisi_ikan',
                                  'field_text' => 'nama_jenis_kondisi_ikan');
    echo $this->mkform->dropdown($attr_opsi_kondisi_ikan);
    
    //content input Jumlah(Kg)
    $attr_jml = array('input_id' => 'jml_1', 'input_name' => 'jml_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_jml);

    //content input Harga Produsen
    $attr_hrg_produsen = array('input_id' => 'hrg_produsen_1', 'input_name' => 'hrg_produsen_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_hrg_produsen);

    //content input Harga Pedagang
    $attr_hrg_pedagang = array('input_id' => 'hrg_pedagang_1', 'input_name' => 'hrg_pedagang_1' , 'label_text' => '', 'input_value' => '', 'input_placeholder' => '',
                     'input_type' => 'text', 'input_width' => '', 'label_class' => '', 'input_class' => 'form-control' );
    echo $this->mkform->input($attr_hrg_pedagang);

 ?>
 <input id="pilih_ikan" name="pilih_ikan" type="text" placeholder="">
 </div>

 <script>
 	var list_opsi_ikan = <?php echo Modules::run('mst_jenis_ikan/json_ikan'); ?>,
 		rowCount = 0,
 		list_to_delete = [],
    current_filter = function() { return 'all'},
    array_uri = ['jurnal','H2','views'];

  function start_filter()
  {
      var new_segments = array_uri.join("/"),
          new_url = site_url+new_segments+"/"+current_filter()+"/",
          new_tanggal = $("#ganti_tanggal").data('tahun')+"/"+$("#ganti_tanggal").data('bulan'),
          url_redirect = new_url+new_tanggal; 
          
      // console.log(url_redirect);
      window.open(url_redirect,'_self');
  }

 	function deleteProduksi(to_delete)
 	{
 		$.ajax({
              dataType: "json",
              url: "<?php echo base_url('entry/H2/hapus'); ?>", //
              data: to_delete, // 
              success: laporDelete // 
            });
 	}

 	function laporDelete()
 	{
 		console.log('Deletedet');
 	}

 	function initModalEdit(data)
 	{
 		fnTableAddRow(data);
 	}

 	function cloner(targetClone, cloneName, cloneValue)
	  {
	      var cloneItem = $(targetClone).clone();
	        cloneItem.attr("id", cloneName).attr("name", cloneName).attr("value", cloneValue);
	      return cloneItem[0].outerHTML;
	  }

 	function fnTableDelRow(rowNumber) {
        $('#table_h2_edit').dataTable().fnDeleteRow(rowNumber);	
    }

  function fnTableAddRow(data) {
        $('#table_h2_edit').dataTable().fnAddData( [
                cloner("#template_inputs #pilih_ikan", "ikan_"+data.idProduksi,'')+
                cloner("#template_inputs #pilih_ikan", "idProduksi_"+data.idProduksi, data.idProduksi),
                cloner("#template_inputs #kondisi_1", "kondisi_"+data.idProduksi,''),
                cloner("#template_inputs #jml_1", "jml_"+data.idProduksi, data.jmlIkan),
                cloner("#template_inputs #hrg_produsen_1", "hrgProdusen_"+data.idProduksi, data.hargaProdusen),
                cloner("#template_inputs #hrg_pedagang_1", "hrgPedagang_"+data.idProduksi, data.hargaPedagang),
                '<a href="#" class="btn btn-danger btn_hapus" data-nama-ikan="'+data.namaJenisIkan+'" data-row-number="'+rowCount+'"data-id-produksi="'+data.idProduksi+'">Hapus</a>'
                ] );
            $("#ikan_"+data.idProduksi).select2({  width: "100%",
                                              placeholder: "Mulai ketik nama ikan..",
                                              data: list_opsi_ikan
                                            });
            $("#ikan_"+data.idProduksi).select2("val",data.idJenisIkan);
            $("#ikan_"+data.idProduksi).select2("enable",false);
            $("#kondisi_"+data.idProduksi).val(data.idJenisKondisiIkan);
            $("#idProduksi_"+data.idProduksi).prop('type','hidden');
            rowCount++;
  }

 	$(document).ready(function() {
      $("#start_filter").click(function(){
          start_filter();
      });

     	$('#table_h2').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "35%", "sClass": "text-left"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"}
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 50,
            "bSort": true
        } );

     	$('#table_h2_edit').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "30%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "20%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center"}

                          ],
            "bFilter": false,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": false,
            "bSort": true
        } );

     	$('.per_ikan').each(function(){
     		var hargaPedagang = $(this).data('hargaPedagang'),
     			hargaProdusen = $(this).data('hargaProdusen'),
     			idJenisKondisiIkan = $(this).data('idJenisKondisiIkan'),
          namaJenisIkan = $(this).data('namaJenisIkan'),
          namaJenisIkanInggris = $(this).data('namaJenisIkanInggris'),
     			namaJenisKondisiIkan = $('#kondisi_1 option[value='+idJenisKondisiIkan+']').text(),
     			preview;
     		// console.log();
     		preview = 'Kondisi : <strong>'+namaJenisKondisiIkan+'</strong> <br>'+
         					'Harga Pedagang : Rp <strong>'+hargaPedagang+'</strong> <br>'+
         					'Harga Produsen : Rp <strong>'+hargaProdusen+'</strong>';
     		$(this).popover({
     			placement: 'top',
     			trigger: 'hover',
     			title: '<strong>'+namaJenisIkan+'</strong> <em>'+namaJenisIkanInggris+'</em>',
     			html: true,
     			content: preview
     		});
     	});

     	$('.btn_edit').on('click', function(){
     		var indexNumber = $(this).data('indexNumber'),
     			namaKapal = $(this).data('namaKapal'),
          namaPengguna = $(this).data('namaPengguna'),
     			array_produksi = $('.produksiBaris_'+indexNumber);

     		rowCount = 0;

     		list_to_delete = [];

     		$("#table_h2_edit").dataTable().fnClearTable();
     		array_produksi.each(function(index, thisData){
     				var per_ikan = $(this).data();
     				initModalEdit(per_ikan);
     		});
     		
     		$('#nama_kapal_edit').text(namaKapal);
        $('#nama_pengguna_edit').text(namaPengguna);
     		$('#modal-edit-h2').modal();

     		$('.btn_hapus').on('click', function(){
    	 		var rowNumber = $(this).data('rowNumber'),
    	 			namaIkan = $(this).data('namaIkan'),
    	 			idProduksi = $(this).data('idProduksi');

    	 		if(confirm('Hapus data produksi ikan '+namaIkan+'?'))
    	 		{	
    	 			fnTableDelRow(rowNumber);
    	 			list_to_delete.push(idProduksi);
    	 			console.log(list_to_delete);	
    	 		}
    	 		
     		});

     		$('#form_edit').submit(function(){
     			var check_list_to_delete = jQuery.isEmptyObject(list_to_delete),
     				data_to_delete = {};

     			if(!check_list_to_delete)
     			{
     				alert('ada yang dihapus!');
     				list_to_delete.forEach(function(d,i){
     									data_to_delete['num'+i] = d;
     								});
     				deleteProduksi(data_to_delete);
     			}
     		});
     	});
 	});
 </script>