<?php 
    if($detail_pegawai)
    {
      ?>
  <div class="row">
    <div class="panel panel-text-referensi">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-8">
      <?php
      // var_dump($detail_pegawai);
      foreach ($detail_pegawai as $item) {
        $array_info_kapal = array( 
                                              //'ID : ' => array( 'id' => 'id_wpp',
                                              //                        'value' => $item->id_wpp ),
                                              'Nama : ' => array( 'id' => 'nama_pegawai',
                                                                      'value' => $item->nama_pegawai ),
                                              'NIP : ' => array( 'id' => 'nip',
                                                                      'value' => $item->nip ),
                                              'Status Pegawai: ' => array( 'id' => 'status',
                                                            'value' => $item->status ),
                                              'Tempat, Tanggal Lahir : ' => array( 'id' => 'tempat_lahir',
                                                                      'value' => $item->tempat_lahir.", ".$this->pipp_lib->fmt_tgl($item->tgl_lahir) ),
                                              'Golongan / TMT : ' => array( 'id' => 'gol',
                                                                      'value' => $item->gol." / ".$this->pipp_lib->fmt_tgl($item->gol_tmt) ),
                                              // 'Golongan TMT : ' => array( 'id' => 'gol_tmt',
                                              //                         'value' => $item->gol_tmt ),
                                              'Pangkat : ' => array( 'id' => 'pangkat',
                                                                      'value' => $item->pangkat ),
                                              'Jabatan / TMT : ' => array( 'id' => 'jabatan',
                                                                      'value' => $item->jabatan." / ".$this->pipp_lib->fmt_tgl($item->jabatan_tmt)),
                                              // 'Jabatan TMT : ' => array( 'id' => 'jabatan_tmt',
                                              //                         'value' => $item->jabatan_tmt ),
                                              'Sesie Bidang : ' => array( 'id' => 'seksi_bidang',
                                                                      'value' => $item->seksi_bidang ),
                                              'Mulai Kerja : ' => array( 'id' => 'id_mulai_kerja', //nama id mulai_kerja harus beda dengan entry views
                                                                      'value' => $this->pipp_lib->fmt_tgl($item->mulai_kerja) ),
                                              'Penempatan : ' => array( 'id' => 'penempatan',
                                                                      'value' => $item->penempatan )
                                           ); 
            }
            echo $this->mkform->ref_text($array_info_kapal);

            $btn_edit = '<button class="edit_this btn btn-warning btn-lg btn-block" data-id-record="'.$item->id_pegawai.'">Edit</button>';

?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="well" style="max-width: 400px; margin: 0 auto 10px;">
    <?php echo $btn_edit; ?>
</div>

<?php
      $attr_modal_edit = array('modal_id' => 'modal_edit_s11',
                               'modal_title' => 'Edit S11',
                               'modal_width' => '760',
                               'link_view_form' => base_url('entry/S11/edit')
                              ); 
      echo $this->mkform->modal_edit($attr_modal_edit);
    }else
    {
      $template = array ( 'table_open' => '<table id="tabel_S11" class="table table-bordered">' );
      $this->table->set_template($template);
      $this->table->set_heading('No.','NIP','Nama');

      $counter = 1;
      if ($list_S11){
        foreach ($list_S11 as $item) {
          $view_detail = '<a href="'.base_url('jurnal/S11/detail/'.$item->id_pegawai).'">'.$item->nama_pegawai.'</a>';
          $this->table->add_row($counter.'.', $item->nip, $view_detail);

          $counter++;
        }
      }

      $tabel_S11 = $this->table->generate();
      $this->table->clear();

      ?>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body overflowed">
              <?php echo $tabel_S11; ?>
            </div>
          </div>
        </div>
      </div>
    
    <?php
    }
    ?> 


<script>
  $(document).ready(function() {
    $('.gambar-pop').popover({ html: true, delay: 500, placement: 'right' });
      $('#tabel_S11').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"},
                        { "sWidth": "15%", "sClass": "text-center"}
                      ],
        "bFilter": true,
        "bAutoWidth": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": true
      } );
  });
</script>