<?php
/*
 * class Mdl_konfigurasi
 */

class Mdl_konfigurasi extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    private $db_dss = NULL;
    private $db_pipp = NULL;
    
    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_pipp = $this->load->database('default', TRUE);
    }
    
    public function list_konfigurasi()
    {
        $query = $this->db_pipp->get('mst_konfigurasi');
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function info_aplikasi_pipp()
    {
        $this->db_dss->select('id_aplikasi');
        $this->db_dss->where('nama_aplikasi', 'PIPP');
        $query = $this->db_dss->get('mst_aplikasi');

        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_konfigurasi($kode)
    {
        $query = $this->db_pipp->get_where('mst_konfigurasi',array('kode' => $kode));
        
        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_konfigurasi($id_konfigurasi,$nilai)
    {

        $datenow = date("Y-m-d H:i:s");

            $data = array(  
                            'nilai' => $nilai,
                            'tanggal_ubah' => $datenow 
                        );
        $this->db_pipp->where('id_konfigurasi', $id_konfigurasi);
        $this->db_pipp->update('mst_konfigurasi', $data);

        /*
        $data = array();
        $counter = 0;
        $get_date = new DateTime("now");
        $datenow = date("Y-m-d H:i:s", $get_date);
        foreach ($update_items as $id_konfigurasi => $nilai) {
            $data[$counter] = array(
                                    'id_konfigurasi' => $id_konfigurasi,
                                    'nilai' => $nilai,
                                    'tanggal_ubah' => $datenow
                                    );
            $counter++;
        }

        $this->db_pipp->update_batch('mst_konfigurasi', $data, 'id_konfigurasi');
        */
       
    }
}
?>