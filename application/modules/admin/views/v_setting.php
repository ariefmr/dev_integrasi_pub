<?php
	//OLAH DATA TAMPIL
	if($list_konfigurasi !== FALSE)
	{
		// TODO opsi untuk boolean masih hardcode.
		$opsi_boolean = array(
                  'true'  => 'TRUE',
                  'false'    => 'FALSE',
                );

		$template = array( "table_open" => "<table id='table_daftar_konfigurasi'
											 class='table table-hover table-bordered'>");
		$this->table->set_template($template);
		$this->table->set_heading('Konfigurasi', 'Nilai','Edit');


		//$hidden_inputs = '';
		foreach ($list_konfigurasi as $item) {
				switch ($item->tipe_nilai) {
					case 'text':
						$cell_nilai = array('data' => $item->nilai, 'data-id-konfigurasi' => $item->id_konfigurasi);
						$cell_edit = array('data' => '<button ype="button" class="edit btn btn-xs btn-info">Edit</button>', 'title' => 'Terakhir diubah : '.$item->tanggal_ubah);
						$this->table->add_row($item->kode, $cell_nilai, $cell_edit);
						//$hidden_inputs .= form_input($item->id_konfigurasi, $item->nilai, 'class="hide hidden_inputs"');
						break;
					case 'boolean':
						$cell_nilai = array('data' => form_dropdown($item->id_konfigurasi, $opsi_boolean, $item->nilai,' disabled="disabled"'), 'data-id-konfigurasi' => $item->id_konfigurasi);
						$cell_edit = array('data' => '<button ype="button" class="edit opsi btn btn-xs btn-info">Edit</button>', 'title' => 'Terakhir diubah : '.$item->tanggal_ubah);
						$this->table->add_row($item->kode, $cell_nilai, $cell_edit);
						//$hidden_inputs .= form_input($item->id_konfigurasi, $item->nilai, 'class="hide hidden_inputs"');
						break;
				}
		}

		$table_list_konfigurasi = $this->table->generate();
	}else{
		$table_list_konfigurasi = 'Empty.';
	}
	

	
?>
<!-- TAMPIL DATA -->
<div class="row">
        <div class="col-lg-3">
        	<ul class="list-group">
        		<li class="list-group-item">
          			User: [Pengguna]
        		</li>
        		<li class="list-group-item">
          			Pelabuhan: [Pelabuhan]
        		</li>
      		</ul>
		</div>
		<div class="col-lg-9">
        	<div class="panel">
			  <div class="panel-heading">
			    <h3 class="panel-title">Konfigurasi Variable Sistem PIPP</h3>
			  </div>
			  <div class="panel-body">
			  <?php 
			  		$attributes = array('id' => 'post_update');
			  		echo form_open('admin/konfigurasi/update', $attributes); ?>
			    <?php echo $table_list_konfigurasi; ?>
			    	<hr>
			    	<!--
			    	<div class="well" style="max-width: 400px; margin: 0 auto 10px;">
				        <button id="submit-button" type="submit" class="btn btn-primary btn-lg btn-block">Simpan Perubahan</button>
				    </div>
				    -->
			  <?php 
			  //echo $hidden_inputs;
			  echo form_close(); ?>
			  </div>
			</div>
		</div>
</div>

<script>
var link_update_konfigurasi = 'update_konfigurasi';
function restoreRow ( oTable, nRow, isOpsi )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);
	
	for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
		oTable.fnUpdate( aData[i], nRow, i, false );
	}
	
	oTable.fnDraw();
}

function ajax_post ( targetUrl, datas, runFunction)
{
	// Assign handlers immediately after making the request,
	// and remember the jqxhr object for this request

	
	var jqxhr = $.post(targetUrl, datas)
	.done(function() {
					 console.log('done',jqxhr.status);
					 runFunction();
					 })
	.fail(function(xhr, textStatus, errorThrown) { 
					console.log('fail',jqxhr.status);
					alert('gagal');
					});
	
	/*
	var xhr = $.ajax({
	  type: "POST",
	  url: targetUrl,
	  data: datas,
	  success: function(msg){
	        status = 'ok';
	  },
	  error: function(XMLHttpRequest, textStatus, errorThrown) {
	     //alert("some error "+errorThrown);
	     status = 'error';
	  }
	});
	*/
}

function editRow ( oTable, nRow, isOpsi )
{
	var aData = oTable.fnGetData(nRow);
	var jqTds = $('>td', nRow);
	//jqTds[0].innerHTML = '<input type="text" value="'+aData[0]+'" disabled>';
	if(isOpsi)
	{
		var form_name = $(jqTds[1]).data('idKonfigurasi');
		$('select[name='+form_name+']').prop('disabled',false);
		jqTds[2].innerHTML = '<button type="button" class="edit opsi Simpan btn btn-xs btn-info">Simpan</button>';
	}else{
		var form_name = $(jqTds[1]).data('idKonfigurasi');
		jqTds[1].innerHTML = '<input name="'+form_name+'" type="text" value="'+aData[1]+'">';
		jqTds[2].innerHTML = '<button type="button" class="edit btn Simpan btn-xs btn-info">Simpan</button>';
	}	
}

/*
function update_hidden_input (form_name, form_value)
{
	$('input[name='+form_name+'].hidden_inputs').attr("value",form_value).addClass("changed");
}
*/
//TODO saveRow, ngesave juga ke input hidden
function saveRow ( oTable, nRow, isOpsi )
{
	var jqTds = $('>td', nRow);
	if(isOpsi)
	{
		var form_name = $(jqTds[1]).data('idKonfigurasi'),
			form_value = $(jqTds[1]).children().val(), 
			datas = { id_konfigurasi: form_name, nilai: form_value};

		ajax_post(link_update_konfigurasi ,datas, function() {
								$('select[name='+form_name+']').prop('disabled',true);
								oTable.fnUpdate( '<button type="button" class="edit opsi btn btn-xs btn-info">Edit</button>', nRow, 2, false );
											});
		
	}else{
		var jqInputs = $('input', nRow);
		//oTable.fnUpdate( jqInputs[0].value, nRow, 0, false );

		var form_name = $(jqTds[1]).data('idKonfigurasi'),
			form_value = $(jqTds[1]).children().val(),
			datas = { id_konfigurasi: form_name, nilai: form_value};
 		
 		xhrStatus = ajax_post(link_update_konfigurasi,datas, function() {
 											oTable.fnUpdate( jqInputs[0].value, nRow, 1, false );
											oTable.fnUpdate( '<button type="button" class="edit btn btn-xs btn-info">Edit</button>', nRow, 2, false );
 												});
	}

	
	oTable.fnDraw();
}

$(document).ready(function() {
	var oTable = $('#table_daftar_konfigurasi').dataTable({
					"bPaginate": false,
					"bInfo": false,
					"bFilter": false,
				 });
	var nEditing = null;
	
	$('#new').click( function (e) {
		e.preventDefault();
		
		var aiNew = oTable.fnAddData( [ '', '', '', '', '', 
			'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Delete</a>' ] );
		var nRow = oTable.fnGetNodes( aiNew[0] );
		editRow( oTable, nRow );
		nEditing = nRow;
	} );
	
	$('#table_daftar_konfigurasi').on('click','.edit', function (e) {
		e.preventDefault();
		/* Check if button has class opsi */
		var isOpsi = $(this).prop('class').split(" ").indexOf("opsi") > -1;

		/* Get the row as a parent of the link that was clicked on */
		var nRow = $(this).parents('tr')[0];

		if ( nEditing !== null && nEditing != nRow ) {
			/* Currently editing - but not this row - restore the old before continuing to edit mode */
			restoreRow( oTable, nEditing, isOpsi );
			editRow( oTable, nRow, isOpsi );
			nEditing = nRow;
		}
		else if ( nEditing == nRow && this.innerHTML == "Simpan" ) {
			/* Editing this row and want to save it */
			saveRow( oTable, nEditing, isOpsi );
			nEditing = null;
		}
		else {
			/* No edit in progress - let's start one */
			editRow( oTable, nRow, isOpsi );
			nEditing = nRow;
		}
	} );

	/*
	$('#post_update').submit(function() {
		var datas = $('.hidden_inputs').serialize(),
			targetUrl = $(this).attr("action");
			
			//Hitung jumlah button Simpan yang masih aktif. (.Simpan > 1 berarti masih editing)
			console.log($('.Simpan').length);
			if( $('.Simpan').length > 0 ){
				alert('Masih ada form dalam mode edit.');
			}else{
				ajax_post(targetUrl, datas);
			}
			
		return false;
	});
	*/

} );
</script>