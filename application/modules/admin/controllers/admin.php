<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */

		function __construct()
		{
			parent::__construct();
			//TODO: LOGIN CHECK HERE
			$this->load->model('mdl_konfigurasi');
			$this->load->helper('form');
		}

	public function index()
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js','DT_bootstrap.js');
		$data['additional_css'] = Array('jquery.dataTables.css','DT_bootstrap.css');
		$data['page_title'] = 'Halaman Admin';
		$data['content_title'] = 'Halaman Admin';
		$data['module'] = 'admin';
		$data['view_file'] = 'v_web_admin';

		$data['list_konfigurasi'] = $this->mdl_konfigurasi->list_konfigurasi();

		
		echo Modules::run('templates/type/default_template', $data);
	}
	
	public function konfigurasi()
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js','DT_bootstrap.js');
		$data['additional_css'] = Array('jquery.dataTables.css','DT_bootstrap.css');
		$data['page_title'] = 'Konfigurasi';
		$data['content_title'] = 'Halaman Konfigurasi';
		$data['module'] = 'admin';
		$data['view_file'] = 'v_setting';

		$data['list_konfigurasi'] = $this->mdl_konfigurasi->list_konfigurasi();

		
		echo Modules::run('templates/type/default_template', $data);
	}

	public function update_konfigurasi()
	{
		$id_konfigurasi = $this->input->post('id_konfigurasi',FALSE);
        $nilai = $this->input->post('nilai',FALSE);
		$this->mdl_konfigurasi->update_konfigurasi($id_konfigurasi,$nilai);
	}

	public function ganti_pelabuhan()
	{
		$id_pelabuhan_new = $this->input->get('id_pelabuhan');
		$url_btoa = $this->input->get('url');

		$this->session->set_userdata('id_pelabuhan_temp', $id_pelabuhan_new);
		$nama_pelabuhan_temp =  $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_new, array('nama_pelabuhan'))->nama_pelabuhan;
		$this->session->set_userdata('nama_pelabuhan_temp', $nama_pelabuhan_temp);

		$url_redir = base64_decode($url_btoa);
		//var_dump($this->session->userdata('id_pelabuhan_temp'));
		redirect($url_redir);
	}
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */