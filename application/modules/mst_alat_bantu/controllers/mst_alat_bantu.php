<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_alat_bantu extends MX_Controller {

	/**
	 * Controller Mst alat_bantu
	 * created by ariefmr
 	 * at McD
 	 * 25-09-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_alat_bantu');
		}

	public function index()
	{

		$data['list_alat_bantu'] = $this->mdl_alat_bantu->list_alat_bantu();
		$this->load->view('daftar_alat_bantu', $data);
	}

	public function daftar_alat_bantu($aktif = '') 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis alat_bantu';
		$data['content_title'] = 'Daftar Jenis alat_bantu';
		$data['module'] = 'mst_alat_bantu';
		$data['view_file'] = 'daftar_alat_bantu';
		if($aktif === "aktif"){
			$data['list_alat_bantu'] = $this->mdl_alat_bantu->list_alat_bantu(TRUE);
		}else{
			$data['list_alat_bantu'] = $this->mdl_alat_bantu->list_alat_bantu();

		}
		echo Modules::run('templates/type/default_template', $data);	
	}

	// Wigdet Pencarian Alat Tangkap untuk keperluan entry form 
	public function wgt_pilih_alat_bantu()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_alat_bantu'] = FALSE;
		$this->load->view('pilih_alat_bantu', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar alat_bantu
	public function select_alat_bantu()
	{
		$data['list_alat_bantu'] = $this->mdl_alat_bantu->list_alat_bantu();
		$this->load->view('select_alat_bantu', $data);
	}

	public function json_alat_bantu()
	{
		$array_of_alat_bantu = (Array) $this->mdl_alat_bantu->list_opsi_alat_bantu();

		echo json_encode($array_of_alat_bantu);
	}

	public function table_alat_bantu()
	{
		$data['list_alat_bantu'] = $this->mdl_alat_bantu->list_alat_bantu();	
		$this->load->view('daftar_alat_bantu', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}

	public function search_alat_bantu()
	{
		$get_nama_alat_bantu = $this->input->get('q', FALSE);
		$get_limit_result = $this->input->get('limit', FALSE);
		$search_result = $this->mdl_alat_bantu->search_alat_bantu($get_nama_alat_bantu, $get_limit_result);
		$array_result = Array( 'total' => count($search_result),'list_alat_bantu' => $search_result );
		echo json_encode($array_result);
	}

}

/* End of file mst_alat_bantu.php */
/* Location: ./application/modules/mst_alat_bantu/controllers*/