<?php
	/*
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_pilih_alat_bantu' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('Nama alat_bantu','SIPI','Perusahaan','Alat Tangkap','Tonase');

	
	$counter = 1;
	foreach ($list_alat_bantu as $item) {
		$this->table->add_row($counter.'.', $item->nama_alat_bantu, $item->no_sipi, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_alat_bantu, $item->gt_alat_bantu);
		$counter++;
	}
	
	$table_list_alat_bantu = $this->table->generate();
	*/
?>

<!-- TAMPIL DATA -->
	<div class="form-group">
					<label for="id_alat_bantu" class="col-lg-4 control-label">Pilih Alat Bantu Tangkap :</label>
					<div class="col-lg-8">
                         <input id="start_search" name="id_alat_bantu" type="hidden" class="bigdrop">
                    </div>
    </div>
	


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null;

	function formatListalat_bantuResult(alat_bantu)
	{
		//var markup = "<table class='alat_bantu-result'><tr>";
        //markup += "<td class='alat_bantu-info'><div class='alat_bantu-nama'>" + alat_bantu.nama_alat_bantu + "</div>";
        //markup += "</td></tr></table>";
        //return markup;
		//return "<div class='result_alat_bantu' id='"+alat_bantu.id_alat_bantu+"'>"+alat_bantu.nama_alat_bantu+" / "+alat_bantu.no_sipi+"</div>";
		return "<div class='result_alat_bantu' id='"+alat_bantu.id_alat_bantu+"'><strong>"+alat_bantu.nama_alat_bantu+"</strong> / <small>"+alat_bantu.tanda_selar+"</small></div>";

	}

	function formatListalat_bantuSelection(alat_bantu)
	{
		return alat_bantu.nama_alat_bantu;
	}

	$(document).ready( function () {
		


		$("#start_search").select2({
									id: function(e) { return e.id_alat_bantu },  	
									placeholder: "Mulai ketik nama alat_bantu..",
									width: "100%",
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: "<?php echo base_url('mst_alat_bantu/search_alat_bantu'); ?>",
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_alat_bantu};
									        }
									},
                                    formatResult: formatListAlatTangkapResult,
                                    formatSelection: formatListAlatTangkapSelection
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_alat_bantu(e.val);
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_alat_bantu/json_alat_bantu'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_alat_bantu(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>