<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_alat_bantu' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'ID Alat Bantu', 'Nama Alat Bantu','Desk Alat Bantu', 'ID Alat Bantu Old');
	$counter = 1;

	if($list_alat_bantu !== FALSE){
		foreach ($list_alat_bantu as $item) {
			$this->table->add_row($counter.'.', $item->id_alat_bantu, $item->nama_alat_bantu, $item->desk_alat_bantu, $item->id_alat_bantu_old);
			$counter++;
		}
	}	

	$table_list_alat_bantu = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_alat_bantu;

		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_alat_bantu').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>