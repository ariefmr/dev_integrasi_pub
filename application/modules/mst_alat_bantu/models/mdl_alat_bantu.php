<?php
/*
 * class Mdl_alat_bantu
 * created by ariefmr
 * at McD
 * 25-09-2013
 */

class Mdl_alat_bantu extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_pipp = NULL;

    function __construct()
    {
        $this->db_pipp = $this->load->database('default', TRUE);
    }
    
    public function list_alat_bantu($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_pipp->like('aktif', 'ya');
        }

        $query = $this->db_pipp->get('mst_alat_bantu');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_alat_bantu()
    {
        $query = 'SELECT id_alat_bantu AS id, nama_alat_bantu AS text FROM mst_alat_bantu';
        
        $run_query = $this->db_pipp->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>