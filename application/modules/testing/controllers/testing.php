<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testing extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */

	private $list_izin_pangkalan = null;
		function __construct()
		{
			parent::__construct();
			$this->load->dbforge();
			$this->load->model('mdl_testing');
			$this->list_izin_pangkalan = $this->mdl_testing->get_izin_pangkalan();
			//TODO: LOGIN CHECK HERE
		}

	public function index()
	{
			//$template = array ( "table_open" => "<table id='tabel_S7A' border='0' cellpadding='4' cellspacing='0'>" );
	    $template = array ( 'table_open' => '<table id="tabel_S7A" class="table table-bordered">' );
	    $this->table->set_template($template);
	    $this->table->set_heading(array('IDPLB','Nama Pelabuhan', 'Jumlah'));

		$test_idplb = Modules::run('mst_pelabuhan/array_pelabuhan');
		///$counter = 0;
		foreach ($test_idplb as $item) {
			//if($counter < 11){
				$jml = $this->count_plb($item->id);
				$this->table->add_row($item->id, $item->text, $jml);
				//$counter++;
			//}
		}

		echo $this->table->generate();
	}
	
	public function exceltest()
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		//$array_test = array
		for ($index=0; $index < 10 ; $index++) { 
			$this->excel->getActiveSheet()->setCellValue('A'.$index ,'Baris Ke: '.$index);
		}
		
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		             
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}


	public function count_plb($id_pelabuhan)
	{
		$counter = 0;
		foreach ($this->list_izin_pangkalan as $value) {
			$tmp_array = explode(",",$value->id_pelabuhan_pangkalan);
			if(in_array($id_pelabuhan, $tmp_array))
			{
				$counter++;
			}
		}
		return $counter;
	}

}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */