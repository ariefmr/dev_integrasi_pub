<?php
/*
 * class Mdl_konfigurasi
 */

class Mdl_testing extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss;
    private $db_pipp;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_pipp = $this->load->database('default', TRUE);        
    }

    function get_izin_pangkalan()
    {
        $sql = "SELECT id_pelabuhan_pangkalan FROM mst_izin";

        return $this->db_dss->query($sql)->result();

    }
}
?>