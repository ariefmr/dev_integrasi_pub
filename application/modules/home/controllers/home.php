<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
	private $global_config = array(); // Rujuk ke custom_constants.php
	private $id_aplikasi_otoritas_pipp = "75";
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');
			$this->global_config = $this->config->item('global_config');
			$this->load->model('berita/mdl_berita');

		}

	public function index()
	{
		$this->init_task();

		$data['additional_js'] = Array('d3.min.js');
		
		$data['page_title'] = 'Selamat Datang di Data Entry PIPP';
		$data['content_title'] = 'Data Entry PIPP';
		$data['module'] = 'home';
		$data['view_file'] = 'homepage';

		$id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();

		echo Modules::run('templates/type/default_template', $data);
	}

	public function phpinfo()
	{
		echo phpinfo();
	}

	function init_temp_data()
		{
			if ($this->session->userdata('id_pelabuhan_temp') === FALSE) {
				$this->load->model('mdl_pelabuhan');
				$data_otoritas_array = $this->session->userdata('data_otoritas_array');
				$id_pelabuhan_temp = 1;
				$nama_pelabuhan_temp = "";

				foreach ($data_otoritas_array as $item) {
					if($item['id_aplikasi_otoritas'] === $this->id_aplikasi_otoritas_pipp)
					{
						if($item['id_grup_otoritas'] === "8")
						{
							$id_pelabuhan_temp = 1;

						}else{
							$id_pelabuhan_temp = $item['id_pelabuhan_otoritas'];
						}
					}
				}

				$nama_pelabuhan_temp =  $this->mdl_pelabuhan->detail_pelabuhan($id_pelabuhan_temp, array('nama_pelabuhan'))->nama_pelabuhan;
				$this->session->set_userdata('id_pelabuhan_temp', $id_pelabuhan_temp );
				$this->session->set_userdata('nama_pelabuhan_temp', $nama_pelabuhan_temp);
			}
		}

		function init_task()
		{	
			//Check if in dev_mode
			$this->load->database();
			$this->db->select('nilai');
			$this->db->where('kode','dev_mode');


			$is_dev_mode = $this->db->get('mst_konfigurasi')->row()->nilai;

			$requested_controller = $this->router->fetch_class();
			// var_dump('init_task : ', $is_dev_mode );

			$user_data = array();
			$isLoggedIn = $this->session->userdata('logged_in');

			if(!$isLoggedIn) {
				if($is_dev_mode === 'true')
				{
					$user_data = array (
									'username' => 'charlie',
									'kode_pengguna' => 'charlie',
									'id_pengguna' => 38,
									'is_super_admin' => 0,
									'is_admin' => 1,
									'nama_pengguna' => 'charlie',
									'id_divisi' => 12,
									'id_lokasi' =>  1,
									'id_grup_pengguna' => 8,
									'fullname' => 'charlie',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1305"
													)
												)
								);
					$this->session->set_userdata($user_data);

					$this->init_temp_data();
				}
			}
		}
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */