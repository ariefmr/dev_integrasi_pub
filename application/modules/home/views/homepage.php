<!-- TAMPIL DATA -->
<div class="row">
		<div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6 ">
              <p class="lead">
                <h3>Selamat datang!</h3>
              </p>
            </div>
            <div class="col-lg-6 text-right">
              <p class="lead">
                <h3><?php echo $nama_pengguna; ?> <small>( <?php echo $nama_pelabuhan; ?> )</small></h3>
              </p>
            </div>
        </div>
        <div class="row">
          <?php echo Modules::run('report/grafik/produksi_pelabuhan_bulan_ini'); ?>  
        </div>
        <div class="row">
          <?php echo Modules::run('berita/wgt_berita_terkini',10); ?>  
        </div>
		</div>
</div>

<!--
<?php var_dump($this->session->all_userdata() );

var_dump($this->pipp_lib->id_pelabuhan_pengguna() ); ?>
-->