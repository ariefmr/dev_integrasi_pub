<?php 

    $tmpl = array ( 'table_open'  => '<table id="table_alat_tangkap" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Nama Alat Tangkap','Unit');

  if($jumlah_kapal !== FALSE)
  {
    $index_number = 1;
      foreach ($jumlah_kapal as $item) {
          $this->table->add_row($index_number.'. ', $item->nama_alat_tangkap, $item->jumlah_kapal);
          $index_number++;
      }
  }

  $table_alat_tangkap = $this->table->generate();

  $this->table->clear();

?>  
            <?php  echo form_open('report/alat_tangkap/index','id="form_filter" class="form-horizontal" role="form" '); ?>

            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Filter</h3>
                      </div>
                      <div class="panel-body">
                        <?php 
                          $attr_datepick_range_tgl = array('input_id_1' => 'tgl_awal',
                                                           'input_name_1' => 'tgl_awal' ,
                                                           'input_id_2' => 'tgl_akhir',
                                                           'input_name_2' => 'tgl_akhir',
                                                           'placeholder_1' => 'Tanggal awal',
                                                           'placeholder_2' => 'Tanggal akhir',
                                                           'label_text' => 'Tanggal :',
                                                           'input_value_1' => kos($pre_filters['tgl_awal']), 
                                                           'input_value_2' => kos($pre_filters['tgl_akhir']), 
                                                           'input_placeholder' => '',
                                                           'input_type' => 'text',
                                                           'input_width' => 'col-lg-2',
                                                           'label_class' => 'col-lg-2',
                                                           'input_class' => 'form-control validate[required] datepicker' );
                          echo $this->mkform->datepick_range($attr_datepick_range_tgl);

                          if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){    
                            $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_selects',
                                                         'input_name' => 'id_pelabuhan_selects[]',
                                                         'label_text' => 'Pelabuhan :',
                                                         'max_select' => '20',
                                                         'array_opsi' => '',
                                                         'opsi_selected' => kos($pre_filter['id_pelabuhan_selects']), 
                                                         'input_width' => 'col-lg-6',
                                                         'input_class' => '',
                                                         'label_class' => 'col-lg-2 control-label');
                            echo $this->mkform->pilih_pelabuhan_multiple($attr_opsi_pelabuhan);
                          }
                        ?>
                      </div>
                       <div class="panel-footer">
                        <button id="submit_preview" type="submit" class="btn btn-default">Filter</button>
                        <button id="submit_xls" type="button" class="btn btn-default">Export</button>
                      </div>
                  </div>
              </div>
            </div>
            <?php echo form_close(); ?>


            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">Data Alat Tangkap </div>
                    <div class="panel-body">
                        <?php echo $table_alat_tangkap; ?>
                    </div>
                  </div>
              </div>
            </div>
<script>
     var export_url = '<?php echo base_url('report/alat_tangkap/xls'); ?>',
        default_submit = '<?php echo base_url('report/alat_tangkap/index'); ?>';

    $(document).ready(function(){
      $('#table_alat_tangkap').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "50%" , "sClass": "text-left"},
                            { "sWidth": "45%", "sClass": "text-center"}            
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $("#submit_xls").on("click",function(){
        $("#form_filter").attr("action", export_url);
        $("#form_filter").submit();
        $("#form_filter").attr("action", default_submit);
      });
    });
</script>