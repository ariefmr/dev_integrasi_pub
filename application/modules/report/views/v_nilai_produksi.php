<?php $tmpl = array ( 'table_open'  => '<table id="table_nilai_produksi" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Pelabuhan','Ikan','Volume (Kg)', 'Nilai Produksi (Rp)', 'Harga Rata-Rata Produsen (Rp)','Harga Rata-Rata Pedagang (Rp)');
    
    $number = 1;
    if($data_nilai_produksi !== FALSE)
    {
      foreach ($data_nilai_produksi as $item) {
      $this->table->add_row($number.". ",
                            $item->nama_pelabuhan,
                            $item->nama_jenis_ikan." <small>(".$item->nama_jenis_ikan_inggris.")</small>",
                            fmt_angka($item->volume),
                            fmt_rupiah($item->nilai_produksi),
                            fmt_rupiah($item->harga_rata_produsen),
                            fmt_rupiah($item->harga_rata_pedagang)
                            );
        $number++;
      }
      $table_nilai_produksi = $this->table->generate();
    }else{
      $table_nilai_produksi = $this->table->generate();
    }
    
    $this->table->clear();

    $tmpl = array ( 'table_open'  => '<table id="table_nilai_produksi_luar" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Pelabuhan','Ikan','Volume (Kg)', 'Nilai Produksi (Rp)', 'Harga Rata-Rata (Rp)');
    
    $number = 1;
    if($data_nilai_produksi_luar !== FALSE)
    {
      foreach ($data_nilai_produksi_luar as $item) {
      $this->table->add_row($number.". ",
                            $item->nama_pelabuhan,
                            $item->nama_jenis_ikan." <small>(".$item->nama_jenis_ikan_inggris.")</small>",
                            fmt_angka($item->volume),
                            fmt_rupiah($item->nilai_produksi),
                            fmt_rupiah($item->harga_rata)
                            );
        $number++;
      }
      $table_nilai_produksi_luar = $this->table->generate();
    }else{
      $table_nilai_produksi_luar = $this->table->generate();
    }
    
    $this->table->clear();
 ?>

  <div class="row">
    <div class="col-lg-12">
            <?php  echo form_open('report/produksi/index','id="form_filter" class="form-horizontal" role="form" '); ?>
      
    <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Filter</h3>
          </div>
          <div class="panel-body">
            <?php 
                $attr_datepick_range_tgl = array('input_id_1' => 'tgl_awal',
                                                 'input_name_1' => 'tgl_awal' ,
                                                 'input_id_2' => 'tgl_akhir',
                                                 'input_name_2' => 'tgl_akhir',
                                                 'placeholder_1' => 'Tanggal awal',
                                                 'placeholder_2' => 'Tanggal akhir',
                                                 'label_text' => 'Tanggal :',
                                                 'input_value_1' => kos($pre_filters['tgl_awal']), 
                                                 'input_value_2' => kos($pre_filters['tgl_akhir']), 
                                                 'input_placeholder' => '',
                                                 'input_type' => 'text',
                                                 'input_width' => 'col-lg-2',
                                                 'label_class' => 'col-lg-2',
                                                 'input_class' => 'form-control validate[required] datepicker' );
                echo $this->mkform->datepick_range($attr_datepick_range_tgl);
              if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){   
                $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_selects',
                                             'input_name' => 'id_pelabuhan_selects[]',
                                             'label_text' => 'Pelabuhan :',
                                             'max_select' => '20',
                                             'array_opsi' => '',
                                             'opsi_selected' => kos($pre_filters['id_pelabuhan_selects']), 
                                             'input_width' => 'col-lg-6',
                                             'input_class' => '',
                                             'label_class' => 'col-lg-2 control-label');
                echo $this->mkform->pilih_pelabuhan_multiple($attr_opsi_pelabuhan);
              }  
              $attr_opsi_ikan = array('input_id' => 'id_ikan_selects',
                                           'input_name' => 'id_ikan_selects[]',
                                           'label_text' => 'Jenis Ikan :',
                                           'max_select' => '20',
                                           'array_opsi' => '',
                                           'opsi_selected' => kos($pre_filters['id_ikan_selects']), 
                                           'input_width' => 'col-lg-6',
                                           'input_class' => '',
                                           'label_class' => 'col-lg-2 control-label');
              echo $this->mkform->pilih_ikan_multiple($attr_opsi_ikan);
             ?>

            
          </div>
              <div class="panel-footer">
                <button id="submit_preview" type="submit" class="btn btn-default">Filter</button>
                <button id="submit_xls" type="button" class="btn btn-default">Export</button>
              </div>
      </div>
 </form>
    </div>
  </div>

 <div class="row">
   <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">Dari Kapal</div>
          <?php echo $table_nilai_produksi; ?>
        </div>
   </div>
 </div>
  <div class="row">
   <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">Dari Luar Pelabuhan</div>
          <?php echo $table_nilai_produksi_luar; ?>
        </div>
   </div>
 </div>
 <script>
    var export_url = '<?php echo base_url('report/produksi/xls'); ?>';
    $(document).ready(function(){
      $('#table_nilai_produksi').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "5%", "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center","sType": "formatted-num"},
                            { "sWidth": "20%", "sClass": "text-center","sType": "formatted-currency"},
                            { "sWidth": "25%", "sClass": "text-center","sType": "formatted-currency"},  
                            { "sWidth": "25%", "sClass": "text-center","sType": "formatted-currency"}                    
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $('#table_nilai_produksi_luar').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "15%" , "sClass": "text-center"},
                            { "sWidth": "15%", "sClass": "text-center"},
                            { "sWidth": "10%", "sClass": "text-center","sType": "formatted-num"},
                            { "sWidth": "20%", "sClass": "text-center","sType": "formatted-currency"},
                            { "sWidth": "25%", "sClass": "text-center","sType": "formatted-currency"},                      
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $("#submit_xls").on("click",function(){
        $("#form_filter").attr("action", export_url);
        $("#form_filter").submit();
      });
    });
 </script>