<?php 

 ?>

  <div class="row">
    <div class="col-lg-12">
            <?php  echo form_open('report/situasional_pelabuhan/index','id="form_filter" class="form-horizontal" role="form" '); ?>
      
    <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Filter</h3>
          </div>
          <div class="panel-body">
            <?php 
                // $attr_datepick_range_tgl = array('input_id_1' => 'tgl_awal',
                //                                  'input_name_1' => 'tgl_awal' ,
                //                                  'input_id_2' => 'tgl_akhir',
                //                                  'input_name_2' => 'tgl_akhir',
                //                                  'placeholder_1' => 'Tanggal awal',
                //                                  'placeholder_2' => 'Tanggal akhir',
                //                                  'label_text' => 'Tanggal :',
                //                                  'input_value_1' => kos($pre_filter['tgl_awal']), 
                //                                  'input_value_2' => kos($pre_filter['tgl_akhir']), 
                //                                  'input_placeholder' => '',
                //                                  'input_type' => 'text',
                //                                  'input_width' => 'col-lg-2',
                //                                  'label_class' => 'col-lg-2',
                //                                  'input_class' => 'form-control validate[required] datepicker' );
                // echo $this->mkform->datepick_range($attr_datepick_range_tgl);

              if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){    
                $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_selects',
                                             'input_name' => 'id_pelabuhan_selects[]',
                                             'label_text' => 'Pelabuhan :',
                                             'max_select' => '20',
                                             'array_opsi' => '',
                                             'opsi_selected' => kos($pre_filter['id_pelabuhan_selects']), 
                                             'input_width' => 'col-lg-6',
                                             'input_class' => '',
                                             'label_class' => 'col-lg-2 control-label');
                echo $this->mkform->pilih_pelabuhan_multiple($attr_opsi_pelabuhan);
              }
             ?>

            
          </div>
              <div class="panel-footer">
                <button id="submit_preview" type="submit" class="btn btn-default">Filter</button>
                <button id="submit_xls" type="button" class="btn btn-default">Export</button>
              </div>
      </div>
 </form>
    </div>
  </div>

    <?php if ($is_multiple): ?>
    <div class="row">
        <div class="col-lg-12">  
          <div class="btn-group">
      <?php $index_btn = 0;
            foreach ($situasional as $nama_pelabuhan => $arr_data): ?>
            <button type="button" class="btn btn-info toggle_situasional" data-target-row="<?php echo 'plb-row-'.$index_btn; ?>">
            <?php echo $nama_pelabuhan; ?></button>
      <?php $index_btn++;
            endforeach ?>
          </div>
        </div>
    </div>
    <hr>
    <?php endif ?>

    <?php if ($is_multiple): ?>
      <?php $index_row = 0; 
        foreach ($situasional as $nama_pelabuhan => $data_item): ?>
        <div class="row plb-row" id="<?php echo 'plb-row-'.$index_row; ?>">
            <div class="col-lg-12">
                  <p class="lead">
                    <?php echo $nama_pelabuhan; ?>
                  </p>
                  <div class="panel-group" id="<?php echo 'panel-situasional-'.$index_row; ?>" data-target='.collapse'>
                  <?php $first = TRUE; ?>
                  <?php foreach ($data_item as $nama_tabel => $data_result): 
                        $class_collapse = $first ? 'panel-collapse collapse in' : 'panel-collapse collapse';
                        $first = FALSE;
                  ?>

                    <div class="panel panel-default">
                      <div class="panel-heading">
                          <a data-toggle="collapse" data-parent="#<?php echo 'panel-situasional-'.$index_row; ?>"
                             href="#<?php echo $nama_tabel.'-'.$index_row; ?>">

                        <h3 class="panel-title">
                            <?php echo fmt_field_name($nama_tabel); ?>
                            <small> <?php echo kos($data_result[0], 'Tidak ada data.'); ?></small>
                        </h3>
                          </a>

                      </div>
                      <div id="<?php echo $nama_tabel.'-'.$index_row; ?>" class="<?php echo $class_collapse; ?>">
                        <div class="panel-body">
                              <?php echo $this->mkform->tbl_ref_text($data_result); ?>
                        </div>
                      </div>
                    </div>

                  <?php endforeach ?>
                  </div>
              </div>
            </div>           
      <?php $index_row++; 
        endforeach ?>
          
    <?php else: ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group" id="panel-situasional" data-target='.collapse'>
                <?php $first = TRUE; ?>
                <?php foreach ($situasional as $nama_tabel => $data_result): 
                      $class_collapse = $first ? 'panel-collapse collapse in' : 'panel-collapse collapse';
                      $first = FALSE;
                ?>
              
              <?php if ($nama_tabel !== "pegawai"): ?>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse"  data-parent="#panel-situasional" href="#<?php echo $nama_tabel; ?>">


                      <h3 class="panel-title">
                          <?php echo fmt_field_name($nama_tabel); ?>
                          <small> <?php echo kos($data_result[0], 'Tidak ada data.'); 
                          //vdump($nama_tabel, false);?></small>
                      </h3>
                        </a>

                    </div>
                    <div id="<?php echo $nama_tabel; ?>" class="<?php echo $class_collapse; ?>">
                      <div class="panel-body">
                            <?php echo $this->mkform->tbl_ref_text($data_result); ?>
                      </div>
                    </div>

              <?php endif ?>

                  </div>

                <?php endforeach; 
                ?>
                </div>
            </div>
          </div>           
    <?php endif ?>
          








 <script>
    var export_url = '<?php echo base_url('report/situasional_pelabuhan/xls'); ?>',
        default_submit = '<?php echo base_url('report/situasional_pelabuhan/index'); ?>';
    $(document).ready(function(){
      // $('.table_operasional_kapal').dataTable( {
      //       "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      //       "aoColumns":  [
      //                       { "sWidth": "5%" , "sClass": "text-center"},
      //                       { "sWidth": "25%" , "sClass": "text-center"},
      //                       { "sWidth": "35%", "sClass": "text-center","sType": "formatted-num"},
      //                       { "sWidth": "35%", "sClass": "text-center"},              
      //                     ],
      //       "bFilter": true,
      //       "bAutoWidth": false,
      //       "bInfo": false,
      //       "bPaginate": true,
      //       "iDisplayLength": 100,
      //       "bSort": true
      //     } );
      $("#submit_xls").on("click",function(){
        $("#form_filter").attr("action", export_url);
        $("#form_filter").submit();
        $("#form_filter").attr("action", default_submit);
      });
    <?php if ($is_multiple): ?>
      $('.toggle_situasional').on("click", function(){
        $(".plb-row").hide();

        var target = $(this).data('targetRow');
        console.log(target);
        $("#"+target).show();
      });
    <?php endif ?>
    });
 </script>