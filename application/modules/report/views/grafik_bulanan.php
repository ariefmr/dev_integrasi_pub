<style>
.chart-produksi-harga {
	font-size: 10px;
}
.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: orange;
}

.bar:hover {
  fill: orangered ;
}

.x.axis path {
  display: none;
}

.d3-tip {
  line-height: 1;
  font-size: 11px;
  font-weight: bold;
  padding: 12px;
  background: rgba(0, 0, 0, 0.8);
  color: #fff;
  border-radius: 2px;
}

/* Creates a small triangle extender for the tooltip */
.d3-tip:after {
  box-sizing: border-box;
  display: inline;
  font-size: 10px;
  width: 100%;
  line-height: 1;
  color: rgba(0, 0, 0, 0.8);
  content: "\25BC";
  position: absolute;
  text-align: center;
}

.label-nama-plb {
	background-color: white;
	border: 1px solid gray;
}

/* Style northward tooltips differently */
.d3-tip.n:after {
  margin: -1px 0 0 0;
  top: 100%;
  left: 0;
}

</style>
<div class="container chart-produksi-harga">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
              Grafik Produksi Ikan Pelabuhan PIPP Bulan <?php echo $this->pipp_lib->bulan_ind(); ?> 
					</h3>
				</div>
				<div class="panel-body" id="produksiharga">
				</div>
			</div>
</div>
<script src="<?php echo $paths['misc_js'];?>/d3.tip.min.js"></script>
<script>
	$("#produksiharga").height("20em");
// var uriPerbulan = '{{ url:anchor segments="produksidanharga/perbulan" title="perbulan" class="login" }}',
// 	urlPerbulan = $(uriPerbulan).attr("href");
var data_grafik_produksi_bulanan = <?php echo $data_grafik_bulanan; ?>,
	width_panel_produksiharga = $("#produksiharga").width(),
	height_panel_produksiharga = $("#produksiharga").height();


var margin = {top: 40, right: 20, bottom: 10, left: 60},
    width = width_panel_produksiharga - margin.left - margin.right,
    height = height_panel_produksiharga - margin.top - margin.bottom;

var formatPercent = d3.format(".0%");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(formatAngka);

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
      return "<strong>Jumlah Produksi "+d.nama_pelabuhan+" :</strong> "
      		+"<span style='color:red'>" + formatAngka(d.jumlah) + " Kg</span>";
  });

var svg = d3.select("#produksiharga").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.call(tip);

var build_grafik_bulanan = function (data_ori){
  var data = data_ori.filter(function(d){ return (d.aktif === 'Ya' || d.aktif === 'Tidak'); });
  

  data.forEach(function(d) {d.jumlah = +d.jumlah; });


  x.domain(data.map(function(d) { return d.nama_pelabuhan; }));
  y.domain([0, d3.max(data, function(d) { return d.jumlah; })]);

  // svg.append("g")
  //     .attr("class", "x axis")
  //     .attr("transform", "translate(0," + height + ")")
  //     .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Jumlah Produksi");
  
  var svgplacebar = svg.append("g")
      .attr("class", "placebar")
    .selectAll(".placebar")
      .data(data);

  var svgbar = svgplacebar.enter().append("g");
 
  svgbar.append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.nama_pelabuhan); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.jumlah); })
      .attr("height", function(d) { return height - y(d.jumlah); })
      .on('click', detailBulanan)
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide);

  svgbar.append("text")
 	  .attr("transform", function(d){ return "translate("+(x(d.nama_pelabuhan)+x.rangeBand()-2)+","+(height)+") rotate(-90)";})
      //.attr("x", function(d) { return x(d.nama_pelabuhan); })
      //.attr("y", function(d) { return (height - y(d.jumlah) -margin.right);})
      .style("text-anchor", "center")
      .style("fill", function(d){ return warna = d.aktif === 'Ya' ? "#000000" : "#FF0000";  })
      .text(function(d){ return d.nama_pelabuhan;})
 
 
}

function formatAngka(angka)
{
	return angka.toLocaleString('id');
}

function detailBulanan(data)
{
	// var urlPerbulanResult = urlPerbulan+"/"+data.id_pelabuhan+"/"+data.nama_pelabuhan,
	// 	pesanConfirm = "Anda akan membuka grafik produksi pelabuhan "+data.nama_pelabuhan+". Lanjutkan?";
	// if(confirm(pesanConfirm)){
	// 	window.open(urlPerbulanResult,'_blank');
	// }
}

$(document).ready(function(){
  if(data_grafik_produksi_bulanan !== false)
  {
    build_grafik_bulanan(data_grafik_produksi_bulanan);
  }
});

</script>