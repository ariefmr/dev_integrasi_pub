 
            <?php  echo form_open('report/pendapatan/index','id="form_filter" class="form-horizontal" role="form" '); ?>

            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Filter</h3>
                      </div>
                      <div class="panel-body">

                       
                          <label  class="col-lg-2 control-label">Bulan/Tahun :</label>
                            <?php 
                              $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                                                   'datepick_id' => 'datepicker_jurnal',
                                                                   'default_text' => fmt_bulan_tahun($tmp_tgl_catat),
                                                                   'input_value' => $tmp_tgl_catat.'-01',
                                                                   'input_name' => 'bulan_tahun'
                                                                  );
                              echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
                          ?>
                        <p></p>
                        <div class="row">
                                  <?php
                                      if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){    
                                                $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_selects',
                                                                             'input_name' => 'id_pelabuhan_selects[]',
                                                                             'label_text' => 'Pelabuhan :',
                                                                             'max_select' => '20',
                                                                             'array_opsi' => '',
                                                                             'opsi_selected' => kos($pre_filter['id_pelabuhan_selects']), 
                                                                             'input_width' => 'col-lg-6',
                                                                             'input_class' => '',
                                                                             'label_class' => 'col-lg-2 control-label');
                                                echo $this->mkform->pilih_pelabuhan_multiple($attr_opsi_pelabuhan);
                                    }



                                    ?>
                        </div>
                    </div>
                       <div class="panel-footer">
                        <button id="submit_preview" type="submit" class="btn btn-default">Filter</button>
                        <button id="submit_xls" type="button" class="btn btn-default">Export</button>
                      </div>
                  </div>
              </div>
            </div>
            <?php echo form_close(); ?>


            <!-- table -->
            <?php 
              //table
              


              foreach ($pendapatan AS $index_number => $parent_item) {
                    $tmpl = array ( 'table_open'  => '<table class="table_jasa table table-bordered">' );
                    $this->table->set_template($tmpl);
                    $this->table->set_heading('No.','Nama Jasa','Nilai (Rp.1000)');


                    $index = 1;
                    foreach ($parent_item['pendapatan_jasa'] as $key => $item) {
                        $this->table->add_row($index.'. ', $item->nama_jns_jasa, number_format( kos($item->jml,'0')) );
                        $index++;
                    }

                  $table_jasa = $this->table->generate();
                  $this->table->clear();

                  $tmpl = array ( 'table_open'  => '<table class="table_perbekalan table table-bordered">' );
                    $this->table->set_template($tmpl);
                    $this->table->set_heading('No.','Jenis Bahan Penyaluran','Nilai (Rp.1000)');

                    $this->table->add_row('1.', "Es", " ");
                    $this->table->add_row('2.', "Solar", " ");
                    $this->table->add_row('3.', "Olie", " ");
                    $this->table->add_row('4.', "AIr", " " );
                    $this->table->add_row('5.', "Garam", " ");
                    $this->table->add_row('6.', "Minyak Tanah", " " );


                  $table_perbekalan = $this->table->generate();
                  $this->table->clear();

                  $tmpl = array ( 'table_open'  => '<table class="table_non_jasa table table-bordered">' );
                  $this->table->set_template($tmpl);
                  $this->table->set_heading('No.','Jenis Retribusi', 'Satuan', 'Nilai');

                  $index = 1;
                  foreach ($parent_item['pendapatan_non_jasa'] as $key => $item) {

                      $this->table->add_row($index.'.', $item->nama_jns_non_jasa, $item->satuan, kos($item->jml,'0') );
                      $index ++;
                  }

                  $table_non_jasa = $this->table->generate();
                  $this->table->clear();

            ?> 
            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $parent_item['nama_pelabuhan'][0]->nama_pelabuhan; ?></div>
                    <div class="panel-body">
                        A. PENDAPATAN DARI PELAYANAN JASA-JASA
                          <?php echo $table_jasa; ?> 
                        </br>
                        </br>
                        B. PENDAPATAN DARI PENYALURAN PERBEKALAN
                          <?php echo $table_perbekalan; ?>
                        </br>
                        </br>
                        C. RETRIBUSI LELANG
                         <?php echo $table_non_jasa; ?>
                    </div>
                  </div>
              </div>
            </div>
            <?php
              }
            ?>
<script>
     var export_url = '<?php echo base_url('report/pendapatan/xls'); ?>',
        default_submit = '<?php echo base_url('report/pendapatan/index'); ?>';


    $(document).ready(function(){
      
      $('#submit_preview').click( function(){
          filter_date();
      });

      $('.table_jasa').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "50%" , "sClass": "text-left"},
                            { "sWidth": "45%", "sClass": "text-center"}            
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $('.table_non_jasa').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "30%" , "sClass": "text-left"},
                            { "sWidth": "35%" , "sClass": "text-left"},
                            { "sWidth": "45%", "sClass": "text-center"}            
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $('.table_perbekalan').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "50%" , "sClass": "text-left"},
                            { "sWidth": "45%", "sClass": "text-center"}            
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $("#submit_xls").on("click",function(){
        $("#form_filter").attr("action", export_url);
        $("#form_filter").submit();
        $("#form_filter").attr("action", default_submit);
      });
    });
</script>