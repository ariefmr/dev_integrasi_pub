<?php 
    
    // vdump($data_tenaga_kerja, true);
    $arr_table_tenaga_kerja = array();
    $number = 1;
    foreach ($data_tenaga_kerja as $index_number => $parent_item) {
      $tmpl = array ( 'table_open'  => '<table id="table_tenaga_kerja'.$index_number.'" class="table table-bordered table_tenaga_kerja">' );
      $this->table->set_template($tmpl);
      $this->table->set_heading('No.', 'Non Nelayan', 'Jumlah');

      if($parent_item['data_tenaga_kerja_non'] !== FALSE)
      {
        foreach ($parent_item['data_tenaga_kerja_non'] as $key => $data_per_jenis) {
          $this->table->add_row($number,
                                  $data_per_jenis->profesi,
                                  $data_per_jenis->jumlah
                                  );
          $number++;
        }
      }
      

      $arr_table_tenaga_kerja[$index_number] = array( 'nama_pelabuhan' => $parent_item['nama_pelabuhan'],
                                                        'table' => $this->table->generate(),
                                                        'nelayan' => kos($parent_item['data_tenaga_kerja_nelayan'][0]->jum_abk,'0'));
      $this->table->clear();
      $number = 1; 
    }
    
 ?>

  <div class="row">
    <div class="col-lg-12">
            <?php  echo form_open('report/tenaga_kerja/index','id="form_filter" class="form-horizontal" role="form" '); ?>
      
    <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Filter</h3>
          </div>
          <div class="panel-body">
            <div class="row">
            <?php 
                // $attr_datepick_range_tgl = array('input_id_1' => 'tgl_awal',
                //                                  'input_name_1' => 'tgl_awal' ,
                //                                  'input_id_2' => 'tgl_akhir',
                //                                  'input_name_2' => 'tgl_akhir',
                //                                  'placeholder_1' => 'Tanggal awal',
                //                                  'placeholder_2' => 'Tanggal akhir',
                //                                  'label_text' => 'Tanggal :',
                //                                  'input_value_1' => kos($pre_filter['tgl_awal']), 
                //                                  'input_value_2' => kos($pre_filter['tgl_akhir']), 
                //                                  'input_placeholder' => '',
                //                                  'input_type' => 'text',
                //                                  'input_width' => 'col-lg-2',
                //                                  'label_class' => 'col-lg-2',
                //                                  'input_class' => 'form-control validate[required] datepicker' );
                // echo $this->mkform->datepick_range($attr_datepick_range_tgl);
              if($this->pipp_lib->info_is_admin('is_super_admin') || $this->pipp_lib->info_is_admin('is_admin_session') ){    
                $attr_opsi_pelabuhan = array('input_id' => 'id_pelabuhan_selects',
                                             'input_name' => 'id_pelabuhan_selects[]',
                                             'label_text' => 'Pelabuhan :',
                                             'max_select' => '20',
                                             'array_opsi' => '',
                                             'opsi_selected' => kos($pre_filter['id_pelabuhan_selects']), 
                                             'input_width' => 'col-lg-6',
                                             'input_class' => '',
                                             'label_class' => 'col-lg-2 control-label');
                echo $this->mkform->pilih_pelabuhan_multiple($attr_opsi_pelabuhan);
              }
             ?>

            </div>
          </div>
              <div class="panel-footer">
                <button id="submit_preview" type="submit" class="btn btn-default">Filter</button>
                <button id="submit_xls" type="button" class="btn btn-default">Export</button>
              </div>
      </div>
 </form>
    </div>
  </div>

 
         <?php foreach ($arr_table_tenaga_kerja as $key => $values): ?>
            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">Data Jumlah Kapal <?php echo $values['nama_pelabuhan'] ?></div>
                    <div class="panel-body">
                        <b>JUMLAH NELAYAN = </b><?php echo $values['nelayan']?>
                        </br>
                        <?php echo $values['table']; ?>
                    </div>
                  </div>
              </div>
            </div>
         <?php endforeach ?>

 <script>
    var export_url = '<?php echo base_url('report/tenaga_kerja/xls'); ?>',
        default_submit = '<?php echo base_url('report/tenaga_kerja/index'); ?>';
    $(document).ready(function(){
      $('.table_tenaga_kerja').dataTable( {
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aoColumns":  [
                            { "sWidth": "5%" , "sClass": "text-center"},
                            { "sWidth": "25%" , "sClass": "text-left"},
                            { "sWidth": "14%", "sClass": "text-center","sType": "formatted-num"},             
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          } );
      $("#submit_xls").on("click",function(){
        $("#form_filter").attr("action", export_url);
        $("#form_filter").submit();
        $("#form_filter").attr("action", default_submit);
      });
    });
 </script>