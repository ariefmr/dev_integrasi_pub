<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grafik extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_web_report');

			//TODO: LOGIN CHECK HERE
		}

	public function index()
	{

	}

	public function produksi_pelabuhan_bulan_ini()
	{
		$data_grafik_bulanan = $this->mdl_web_report->summary_produksi_pelabuhan_bulanan();
		$data['data_grafik_bulanan'] = json_encode($data_grafik_bulanan);
		$this->load->view('grafik_bulanan', $data);
	}

	// public function view_nilai_produksi()
	// {

	// }
	
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */