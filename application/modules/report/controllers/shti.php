<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shti extends MX_Controller {

	/**
	 * Controller Mst WPP
	 * created by ariefmr
 	 * at McD
 	 * 25-09-2013
	 * 
	 */
	private $assets_paths = '';
	function __construct()
	{
		parent::__construct();
		//$this->load->config('custom_constants');

		$this->assets_paths = $this->config->item('assets_paths');
		$this->load->model('mdl_view_shti');
	}

	public function index()
	{

			
	}

	public function json_jenis_shti()
	{
		$get_result = $this->mdl_view_shti->jenis_shti();
		$array_result = array();
		
		foreach ($get_result as $key => $value) {
			array_push($array_result, array($key, $value));
		}

		echo json_encode($array_result);
	}

	public function search_shti()
	{
		$q_shti = $this->input->post('no_shti');

		$search_result = (Array) $this->mdl_view_shti->search_shti($q_shti);

		echo json_encode($search_result);
	}

}

/* End of file mst_wpp.php */
/* Location: ./application/modules/mst_wpp/controllers*/