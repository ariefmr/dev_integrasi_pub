<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pendapatan extends MX_Controller {

  /**
   * Nama File: 
   * Author: Frendhi
   * Deskripsi: 
   * 
   */
    private $list_report = array();
    function __construct()
    {
      parent::__construct();
      $this->load->library('entry/mkform');
      //$this->load->config('custom_constants');
      $this->list_report = $this->config->item('list_report');
      $this->load->model('jurnal/mdl_view_b1');

      //TODO: LOGIN CHECK HERE
    }

  private function _filtering()
  {
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }

    $input_filter = $this->input->post(NULL, TRUE);
    // vdump($input_filter);
    // $input_filter = array{
    //                 "tgl_awal" => "2014-02-01",
    //                 "tgl_awal_display"=> "01/02/2014",
    //                 "tgl_akhir" => "2014-02-28",
    //                 "tgl_akhir_display" => "28/02/2014",
    //                 "id_pelabuhan_selects"=> array {
    //                   [0]=> "1172",
    //                   [1]=> "1291"
    //                 }
    //               };
    
    $data['is_multiple'] = FALSE;

    if(empty($input_filter['bulan_tahun']))
    {
      $tgl_catat = array( 'bulan' => date("m", time() ), 
                              'tahun' => date("Y", time() )
                          );
    }
    else {
      $tgl_ex = explode("-", $input_filter['bulan_tahun']);
      $tgl_catat = array( 'tahun' => $tgl_ex[0] , 
                              'bulan' => $tgl_ex[1] 
                          );
    }
    $data['tmp_tgl_catat'] = $tgl_catat['tahun'].'-'.$tgl_catat['bulan'];
    $data['tmp_bulan'] = $tgl_catat['bulan'];
    $data['tmp_tahun'] = $tgl_catat['tahun'];

    $data_pendapatan = array();
    if(empty($input_filter['id_pelabuhan_selects']))
    {
      $tmp['nama_pelabuhan'] = $this->mdl_view_b1->nama_pelabuhan($id_pelabuhan);
      $tmp['pendapatan_jasa'] = $this->mdl_view_b1->pendapatan_jasa_pelayanan($id_pelabuhan, $tgl_catat['tahun'], $tgl_catat['bulan']);
      $tmp['pendapatan_non_jasa'] = $this->mdl_view_b1->pendapatan_non_jasa_pelayanan($id_pelabuhan, $tgl_catat['tahun'], $tgl_catat['bulan']);

          
       array_push($data_pendapatan , $tmp);    
    }
    elseif(is_array($input_filter) && count($input_filter['id_pelabuhan_selects']) === 1){
      $id_pelabuhan = $input_filter['id_pelabuhan_selects'][0];
      $tmp['nama_pelabuhan'] = $this->mdl_view_b1->nama_pelabuhan($id_pelabuhan);
      $tmp['pendapatan_jasa'] = $this->mdl_view_b1->pendapatan_jasa_pelayanan($id_pelabuhan, $tgl_catat['tahun'], $tgl_catat['bulan']);
      $tmp['pendapatan_non_jasa'] = $this->mdl_view_b1->pendapatan_non_jasa_pelayanan($id_pelabuhan, $tgl_catat['tahun'], $tgl_catat['bulan']);
      array_push($data_pendapatan , $tmp);
    }
    else{
      foreach ($input_filter['id_pelabuhan_selects'] as $id_pelabuhan_selects) {
          $tmp['nama_pelabuhan'] = $this->mdl_view_b1->nama_pelabuhan($id_pelabuhan_selects);
         $tmp['pendapatan_jasa'] = $this->mdl_view_b1->pendapatan_jasa_pelayanan($id_pelabuhan_selects, $tgl_catat['tahun'], $tgl_catat['bulan']);
         $tmp['pendapatan_non_jasa'] = $this->mdl_view_b1->pendapatan_non_jasa_pelayanan($id_pelabuhan_selects, $tgl_catat['tahun'], $tgl_catat['bulan']);
         array_push($data_pendapatan , $tmp);
      }
        $data['is_multiple'] = TRUE;
    }

    $data['id_pelabuhan'] = $id_pelabuhan;
    $data['pendapatan'] = $data_pendapatan;
    $data['pre_filter'] = empty($input_filter) ? FALSE : $input_filter;


    return $data;
  }



  public function index($tahun = '', $bulan = '')
  {
    //$bulanan, dll (data)
    $data = $this->_filtering();

    $data['module'] = 'report';
    $data['view_file'] = 'v_pendapatan';
    $data['page_title'] = 'LAPORAN PENDAPATAN BULANAN PELABUHAN';
    $data['content_title'] = 'PENDAPATAN BULANAN PELABUHAN';
    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js','d3.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
    $data['breadcrumbs'] = 'Laporan > PENDAPATAN BULANAN PELABUHAN';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan(1343);
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);
   
    $data['list_report'] = $this->list_report;
    echo Modules::run('templates/type/reporting', $data);
  }

  public function xls()
  {
    $data = $this->_filtering();
    
    // if($data['is_multiple'] === FALSE)
    // {
    //   $arr_bulanan_jasa = array(0 => $data['bulanan']);
    // }else{
    //   $arr_bulanan = $data['bulanan'];
    // }

    $this->load->library('excel');
    $objPHPExcel = $this->excel;

    $filename = "DATA PENDAPATAN.xlsx";
// die;
    foreach ($data['pendapatan'] as $index_number => $parent_item) {
      $objPHPExcel->setActiveSheetIndex($index_number);
      $objPHPExcel->getActiveSheet()->setTitle($parent_item['nama_pelabuhan'][0]->nama_pelabuhan);

      // echo "makan";die;
      // Set document properties
      $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                 ->setLastModifiedBy("Maarten Balliauw")
                 ->setTitle("Office 2007 XLSX Test Document")
                 ->setSubject("Office 2007 XLSX Test Document")
                 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                 ->setKeywords("office 2007 openxml php")
                 ->setCategory("Test result file");


        // JUDUL
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      -> setCellValue('A1', 'LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN');
                          $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                          $sheet->getStyle('A1')->applyFromArray($styleArray);
                          $sheet->mergeCells('A1:H1');

        // SUBJUDUL
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('A3', '16. PENDAPATAN PELABUHAN PERIKANAN');
                          $styleArray = array('font' => array('bold' => true));
                          $sheet->getStyle('A3')->applyFromArray($styleArray);
                          $sheet->mergeCells('A3:B3');
        $sheet      ->setCellValue('A6', 'A. PENDAPATAN DARI PELAYANAN JASA-JASA');
                          $styleArray = array('font' => array('bold' => true));
                          $sheet->getStyle('A6')->applyFromArray($styleArray);
                          $sheet->mergeCells('A6:C6');
        $sheet      ->setCellValue('E6', 'B. PENDAPATAN DARI PENYALURAN PERBEKALAN');
                          $styleArray = array('font' => array('bold' => true));
                          $sheet->getStyle('E6')->applyFromArray($styleArray);
                          $sheet->mergeCells('E6:G6');
        // $sheet      ->setCellValue('E6', 'C. PENDAPATAN DARI PELAYANAN JASA-JASA');
        //                   $styleArray = array('font' => array('bold' => true));
        //                   $sheet->getStyle('E6')->applyFromArray($styleArray);
        //                   $sheet->mergeCells('E6:G6');
        $sheet      ->setCellValue('E19', 'C. RETRIBUSI LELANG');
                          $styleArray = array('font' => array('bold' => true));
                          $sheet->getStyle('E19')->applyFromArray($styleArray);
                          $sheet->mergeCells('E19:F19');
        // $sheet      ->setCellValue('A35', 'TOTAL JUMLAH PENDAPATAN PELABUHAN PERIKANAN : JUMLAH (A) +  JUMLAH (B) + JUMLAH (C) + JUMLAH  (D)');
        //                   $styleArray = array('font' => array('bold' => true, 'size' => 14));
        //                   $sheet->getStyle('A35:H35')->applyFromArray($styleArray);
        //                   $sheet->mergeCells('A35:G35');
        $sheet      ->setCellValue('F3', 'Bulan :'.$data['tmp_bulan']);
        $sheet      ->setCellValue('F4', 'Tahun :'.$data['tmp_tahun']);

        $styleArray = array('font' => array('name' => 'Trebuchet MS','size' => 11));
        $sheet->getStyle('A3:H34')->applyFromArray($styleArray);
        $sheet->getStyle('H35')->applyFromArray($styleArray);

        //DATA 1
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('A8', 'NO.');
        $sheet      ->setCellValue('E8', 'NO.');
        $sheet      ->setCellValue('E9', '1. ');
        $sheet      ->setCellValue('E10', '2. ');
        $sheet      ->setCellValue('E11', '3. ');
        $sheet      ->setCellValue('E12', '4. ');
        $sheet      ->setCellValue('E13', '5. ');
        $sheet      ->setCellValue('E14', '6. ');
        $sheet      ->setCellValue('E15', '7. ');
        // $sheet      ->setCellValue('E8', 'NO.');
        // $sheet      ->setCellValue('E9', '1. ');
        $sheet      ->setCellValue('E23', 'NO.');
        // $sheet      ->setCellValue('E24', '1. ');
        // $sheet      ->setCellValue('E26', '2. ');
        // $sheet      ->setCellValue('E21', 'Sudah melaksanakan lelang (Ya/Tidak)');

                                $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER ));
                                $sheet->getStyle('A8:A20')->applyFromArray($styleArray);
                                $sheet->getStyle('A8:C8')->applyFromArray($styleArray);
                                $sheet->getStyle('F8:G8')->applyFromArray($styleArray);
                                $sheet->getStyle('F23:H23')->applyFromArray($styleArray);
                                // $sheet->getStyle('B25:C25')->applyFromArray($styleArray);
                                // $sheet->getStyle('A25:A32')->applyFromArray($styleArray);
                                $sheet->getStyle('E8:E16')->applyFromArray($styleArray);
                                $sheet->getStyle('E23:E27')->applyFromArray($styleArray);
                                $sheet->getStyle('H35')->applyFromArray($styleArray);
                                //Bold huruf
                                $styleArray2 = array('font' => array('bold' => true));
                                $sheet->getStyle('A8:C8')->applyFromArray($styleArray2);
                                // $sheet->getStyle('A25:C25')->applyFromArray($styleArray2);
                                $sheet->getStyle('E8:G8')->applyFromArray($styleArray2);
                                $sheet->getStyle('E23:H23')->applyFromArray($styleArray2);
                                $sheet->getStyle('H35')->applyFromArray($styleArray2);

        // DATA 2
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('B8', 'JASA-JASA PELAYANAN ');
        $sheet      ->setCellValue('F8', 'JENIS BAHAN PENYALURAN');
        $sheet      ->setCellValue('F9', 'Es');
        $sheet      ->setCellValue('F10', 'S o l a r');
        $sheet      ->setCellValue('F11', 'Olie ');
        $sheet      ->setCellValue('F12', 'A i r');
        $sheet      ->setCellValue('F13', 'G a r a m');
        $sheet      ->setCellValue('F14', 'Minyak Tanah');
        $sheet      ->setCellValue('F15', 'Lain-lain (Sebutkan)');
        $sheet      ->setCellValue('F16', 'JUMLAH (B)');
        // $sheet      ->setCellValue('F8', 'JENIS PUNGUTAN');
        // $sheet      ->setCellValue('F9', 'Pas Masuk Pelabuhan');
        $sheet      ->setCellValue('F23', 'JENIS RETRIBUSI');
        
        // $sheet      ->setCellValue('F28', 'JUMLAH (D)');

          //tabel jasa 
          $index = 1;
          $cell_numbber_jasa = 9;
          $total_jasa = 0;
          foreach ($parent_item['pendapatan_jasa'] as $key => $item) {
             $sheet     ->setCellValue('A'.$cell_numbber_jasa, $index.'.')
                        ->setCellValue('B'.$cell_numbber_jasa, $item->nama_jns_jasa)
                        ->setCellValue('C'.$cell_numbber_jasa, number_format( kos($item->jml,'0') ));
              $total_jasa += $item->jml;
            $index++;
            $cell_numbber_jasa++;
          }
          $sheet      ->setCellValue('B'.$cell_numbber_jasa, 'JUMLAH (A)');
          $sheet      ->setCellValue('C'.$cell_numbber_jasa, number_format($total_jasa));
          
          //table non jasa
          $cell_numbber_non_jasa = 24;
          $total_non_jasa= 0;
          $index = 1;
          foreach ($parent_item['pendapatan_non_jasa'] as $key => $item) {

             $sheet     ->setCellValue('E'.$cell_numbber_non_jasa, $index.'.')
                        ->setCellValue('F'.$cell_numbber_non_jasa, $item->nama_jns_non_jasa)
                        ->setCellValue('G'.$cell_numbber_non_jasa, $item->satuan)
                        ->setCellValue('H'.$cell_numbber_non_jasa,  kos($item->jml,'0') );
              $total_non_jasa += $item->jml;
              $cell_numbber_non_jasa++;
              $index ++;
          }
          $sheet      ->setCellValue('F'.$cell_numbber_non_jasa, 'JUMLAH (C)');;
  // die;
          // $sheet      ->setCellValue('F24', $data['bulanan_non_jasa'][0]->nama_jns_non_jasa);
          // $sheet      ->setCellValue('F26', $data['bulanan_non_jasa'][1]->nama_jns_non_jasa);
          // $sheet      ->setCellValue('G24', $data['bulanan_non_jasa'][0]->jml);
          // $sheet      ->setCellValue('G26', $data['bulanan_non_jasa'][1]->jml);
          // $sheet      ->setCellValue('G28', $data['bulanan_non_jasa'][1]->jml+$data['bulanan_non_jasa'][0]->jml);


                                
                                $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
                                      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
                                $styleArray_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
                                      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
                                $sheet->getStyle('C9:C'.$cell_numbber_jasa)->applyFromArray($styleArray_left);
                                $sheet->getStyle('A9:A'.$cell_numbber_jasa)->applyFromArray($styleArray);
                                // $sheet->getStyle('9:A'.$cell_numbber_jasa)->applyFromArray($styleArray);
                                $sheet->getStyle('G9:G16')->applyFromArray($styleArray);
                                $sheet->getStyle('G24:G'.$cell_numbber_non_jasa)->applyFromArray($styleArray);
                                //Bold huruf
                                $sheet->getStyle('B'.$cell_numbber_jasa)->applyFromArray($styleArray2);
                                $sheet->getStyle('F'.$cell_numbber_non_jasa)->applyFromArray($styleArray2);
                                $sheet->getStyle('F28')->applyFromArray($styleArray2);
                                $sheet->getStyle('F16')->applyFromArray($styleArray2);
                          
        // DATA 3
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('C8', 'NILAI (Rp. 1.000)');
        // $sheet      ->setCellValue('C25', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('G8', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('G23', 'SATUAN');
        $sheet      ->setCellValue('H23', 'NILAI');

        //MENGATUR UKURAN KOLOM
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(5);
        $sheet->getColumnDimension('E')->setWidth(5);
        $sheet->getColumnDimension('F')->setWidth(32);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(10);
        // Redirect output to a client’s web browser (Excel2007)
        

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        // $objPHPExcel->setActiveSheetIndex(0);
        // $objPHPExcel->getActiveSheet()->setTitle('Pendapatan');

        //MENGATUR BORDER Table 1
        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $objPHPExcel->getActiveSheet('A8:C'.$cell_numbber_jasa)->getStyle('A8:C'.$cell_numbber_jasa)->applyFromArray($styleArray);
        unset($styleArray);

        //MENGATUR BORDER Table 2
        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $objPHPExcel->getActiveSheet('E8:G16')->getStyle('E8:G16')->applyFromArray($styleArray);
        unset($styleArray);

        //MENGATUR BORDER Table 3
        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $objPHPExcel->getActiveSheet('E23:H'.$cell_numbber_non_jasa)->getStyle('E23:H'.$cell_numbber_non_jasa)->applyFromArray($styleArray);
        unset($styleArray);

          // Save Excel 2007 fil
      $objPHPExcel->createSheet();
      // echo "makan"; die;
    }
    // die;
    $callStartTime = microtime(true);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
  }
}