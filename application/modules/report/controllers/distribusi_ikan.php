<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distribusi_ikan extends MX_Controller {

  /**
   * Nama File: 
   * Author: Frendhi
   * Deskripsi: 
   * 
   */
    private $list_report = array();

    function __construct()
    {
      parent::__construct();
      $this->load->library('entry/mkform');
      //$this->load->config('custom_constants');
      $this->list_report = $this->config->item('list_report');
      $this->load->model('mdl_distribusi_ikan');

      //TODO: LOGIN CHECK HERE
    }
    
  private function _filtering()
  {
      $data['is_admin'] = FALSE;
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
      $data['is_admin'] = TRUE;

    }
    $input_filter = $this->input->post(NULL, TRUE);

    $data['pre_filter'] = FALSE;
    $data_distribusi_ikan = array();
    if(empty($input_filter) )
    {
      $array_filter = array( 'id_pelabuhan' => $id_pelabuhan,
                              'bulan' => date("m", time()), 
                              'tahun' => date("Y", time()), 
                              'filters' => FALSE
                           );
      $tmp_arr_data = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_tujuan' => $this->mdl_distribusi_ikan->tujuan($array_filter),
                                       'data_distribusi' => $this->mdl_distribusi_ikan->distribusi_ikan($array_filter)
                                       );
      array_push($data_distribusi_ikan , $tmp_arr_data);
    }else{
      $array_filter['id_pelabuhan'] = $id_pelabuhan;
      $array_filter['filters'] = $input_filter;
      $data['pre_filter'] = $input_filter;

      if( empty($input_filter['id_pelabuhan_selects']) || count( $input_filter['id_pelabuhan_selects'] ) === 1 )
      {
        $tmp_arr_data = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_tujuan' => $this->mdl_distribusi_ikan->tujuan($array_filter),
                                       'data_distribusi' => $this->mdl_distribusi_ikan->distribusi_ikan($array_filter)
                                        );
        array_push($data_distribusi_ikan , $tmp_arr_data);
      }else{
        foreach ($input_filter['id_pelabuhan_selects'] as $id_pelabuhan_selects) {
          $array_filter['id_pelabuhan'] = $id_pelabuhan_selects;
          $tmp_arr_data = array( 'id_pelabuhan' => $id_pelabuhan_selects,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan_selects),
                                       'data_tujuan' => $this->mdl_distribusi_ikan->tujuan($array_filter),
                                       'data_distribusi' => $this->mdl_distribusi_ikan->distribusi_ikan($array_filter)
                                       );
          array_push($data_distribusi_ikan , $tmp_arr_data);
        }
      }
    }
    $data['id_pelabuhan'] = $id_pelabuhan;
     
    $data['data_distribusi_ikan'] = $data_distribusi_ikan;

    return $data;
  }

  public function index()
  {
    $data = $this->_filtering();
    
    $data['module'] = 'report';
    $data['view_file'] = 'v_distribusi_ikan';
    $data['page_title'] = 'LAPORAN DATA DISTRIBUSI IKAN';
    $data['content_title'] = 'DATA DISTRIBUSI IKAN';
    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js','d3.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
    $data['breadcrumbs'] = 'Laporan > DATA DISTRIBUSI IKAN';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);

    $data['list_report'] = $this->list_report;
    echo Modules::run('templates/type/reporting', $data);
  }

  
  public function xls()
  {
    $data = $this->_filtering();

    $data_distribusi_ikan = $data['data_distribusi_ikan'];


    $this->load->library('excel');
    $objPHPExcel = $this->excel;
    $filename = "DATA DISTRIBUSI IKAN.xlsx";
        // Set document properties
      // $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
      //                ->setLastModifiedBy("Maarten Balliauw")
      //                ->setTitle("Office 2007 XLSX Test Document")
      //                ->setSubject("Office 2007 XLSX Test Document")
      //                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
      //                ->setKeywords("office 2007 openxml php")
      //                ->setCategory("LAPORAN");
    // vdump($data_distribusi_ikan[0]['data_distribusi']);
    $arr_table_distribusi = array();
    foreach ($data_distribusi_ikan as $index_number => $parent_item) {
              // JUDUL
        $objPHPExcel->setActiveSheetIndex($index_number);
        $objPHPExcel->getActiveSheet()->setTitle($parent_item['nama_pelabuhan']);

        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      -> setCellValue('A1', 'LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN');
                          $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                          $sheet->getStyle('A1')->applyFromArray($styleArray);
                          $sheet->mergeCells('A1:K1');

        // SUBJUDUL
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('A3', '08. PEMASARAN IKAN');
                          $styleArray = array('font' => array('bold' => true));
                          $sheet->getStyle('A3')->applyFromArray($styleArray);
                          $sheet->mergeCells('A3:B3');
        $sheet      ->setCellValue('J3', 'Bulan :');
        $sheet      ->setCellValue('J4', 'Tahun :');

        $styleArray = array('font' => array('name' => 'Trebuchet MS','size' => 11));
        $sheet->getStyle('A3:K100')->applyFromArray($styleArray);

        //DATA 1
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('A5', 'I.');
        $sheet      ->setCellValue('A7', '1. ');
        $sheet      ->setCellValue('A13', '2. ');
        $sheet      ->setCellValue('A19', '3. ');
        $sheet      ->setCellValue('A25', 'II.');

        $sheet      ->setCellValue('C9', ': ');
                          $sheet->mergeCells('C9:D9');
        $sheet      ->setCellValue('C10', ': ');
                          $sheet->mergeCells('C10:D10');
        $sheet      ->setCellValue('C11', ': ');
                          $sheet->mergeCells('C11:D11');
        $sheet      ->setCellValue('C15', ': ');
                          $sheet->mergeCells('C15:D15');
        $sheet      ->setCellValue('C16', ': ');
                          $sheet->mergeCells('C16:D16');
        $sheet      ->setCellValue('C18', ': ');
                          $sheet->mergeCells('C18:D18');
        $sheet      ->setCellValue('C21', ': ');
                          $sheet->mergeCells('C21:D21');
        $sheet      ->setCellValue('C22', ': ');
                          $sheet->mergeCells('C22:D22');
        $sheet      ->setCellValue('C23', ': ');
                          $sheet->mergeCells('C23:D23');

        $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $sheet->getStyle('A5:A40')->applyFromArray($styleArray);
        //Bold huruf
        $styleArray2 = array('font' => array('bold' => true));
        $sheet->getStyle('A5')->applyFromArray($styleArray2);
        $sheet->getStyle('A25')->applyFromArray($styleArray2);
        $sheet->getStyle('A27')->applyFromArray($styleArray2);

        // DATA 2
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('B5', 'TUJUAN PEMASARAN IKAN ');
                          $sheet->mergeCells('B5:C5');
        $sheet      ->setCellValue('B7', 'Ikan Segar');
        $sheet      ->setCellValue('B8', 'a. Lokal');
        $sheet      ->setCellValue('B9', 'b. Antar Kabupaten');
        $sheet      ->setCellValue('B10', 'c. Antar Provinsi');
        $sheet      ->setCellValue('B11', 'd. Ekspor');
        $sheet      ->setCellValue('B13', 'Ikan Olahan');
        $sheet      ->setCellValue('B14', 'a. Lokal');
        $sheet      ->setCellValue('B15', 'b. Antar Kabupaten');
        $sheet      ->setCellValue('B16', 'c. Antar Provinsi');
        $sheet      ->setCellValue('B17', 'd. Ekspor');
        $sheet      ->setCellValue('B19', 'Ikan Beku');
        $sheet      ->setCellValue('B20', 'a. Lokal');
        $sheet      ->setCellValue('B21', 'b. Antar Kabupaten');
        $sheet      ->setCellValue('B22', 'c. Antar Provinsi');
        $sheet      ->setCellValue('B23', 'd. Ekspor');



        $sheet      ->setCellValue('B25', 'DISTRIBUSI PEMASARAN');
                          $sheet->mergeCells('B25:C25');

        $sheet      ->setCellValue('A27', 'NO. ');
                  $sheet->mergeCells('A27:A28');

        $sheet      ->setCellValue('B27', 'JENIS IKAN');
                          $sheet->mergeCells('B27:B28');
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet      ->setCellValue('C27', 'KESELURUHAN');
                                $sheet->mergeCells('C27:E27');
        $sheet      ->setCellValue('C28', 'VOLUME (KG)');
        $sheet      ->setCellValue('D28', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('E28', 'HARGA RATA2/KG (Rp)');


        $sheet      ->setCellValue('G27', 'NO. ');
                  $sheet->mergeCells('G27:G28');

        $sheet      ->setCellValue('H27', 'JENIS IKAN');
                  $sheet->mergeCells('H27:H28');
        // $sheet      ->setCellValue('I27', 'DALAM PROVINSI');
                                $sheet->mergeCells('I27:K27');
        $sheet      ->setCellValue('I28', 'VOLUME (KG)');
        $sheet      ->setCellValue('J28', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('K28', 'HARGA RATA2/KG (Rp)');

        $sheet      ->setCellValue('M27', 'NO. ');
                  $sheet->mergeCells('M27:M28');

        $sheet      ->setCellValue('N27', 'JENIS IKAN');
                  $sheet->mergeCells('N27:N28');                                
        // $sheet      ->setCellValue('O27', 'ANTAR PROVINSI');
                                $sheet->mergeCells('O27:Q27');
        $sheet      ->setCellValue('O28', 'VOLUME (KG)');
        $sheet      ->setCellValue('P28', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('Q28', 'HARGA RATA2/KG (Rp)');


        $sheet      ->setCellValue('S27', 'NO. ');
                  $sheet->mergeCells('S27:S28');

        $sheet      ->setCellValue('T27', 'JENIS IKAN');
                  $sheet->mergeCells('T27:T28');                                
        // $sheet      ->setCellValue('U27', 'EKSPOR');
                                $sheet->mergeCells('U27:W27');
        $sheet      ->setCellValue('U28', 'VOLUME (KG)');
        $sheet      ->setCellValue('V28', 'NILAI (Rp. 1.000)');
        $sheet      ->setCellValue('W28', 'HARGA RATA2/KG (Rp)');

        $array_cell_position = array('C27', 'I27', 'O27', 'U27');
        $array_value_position = array('B_C_D_E', 'H_I_J_K', 'N_O_P_Q', 'T_U_V_W');
        $index_table_num = 0;
        $index_cell_row = 29;
        foreach ($parent_item['data_distribusi'] as $nama_kategori => $per_kategori) {
          
          $sheet->setCellValue($array_cell_position[$index_table_num],
                                 $nama_kategori);

          $kolom = explode('_', $array_value_position[$index_table_num]);
          foreach ($per_kategori as $per_data) {
            $sheet->setCellValue($kolom[0].$index_cell_row, $per_data->nama_jenis_ikan );
            $sheet->setCellValue($kolom[1].$index_cell_row, $per_data->jumlah_ikan );
            $sheet->setCellValue($kolom[2].$index_cell_row, ($per_data->nilai_produksi/1000) );
            $sheet->setCellValue($kolom[3].$index_cell_row, $per_data->harga_rata );
            $index_cell_row++;
          }

          $index_table_num++;
          $index_cell_row = 29;


      }






      $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
      $sheet->getStyle('B27:W28')->applyFromArray($styleArray);
      //Bold huruf
    

        //MENGATUR UKURAN KOLOM
                $sheet = $objPHPExcel->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
                $sheet->getColumnDimension('F')->setWidth(5);
        $sheet->getColumnDimension('G')->setWidth(5);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(15);
        $sheet->getColumnDimension('J')->setWidth(20);
        $sheet->getColumnDimension('K')->setWidth(25);
                $sheet->getColumnDimension('L')->setWidth(5);
        $sheet->getColumnDimension('M')->setWidth(5);
        $sheet->getColumnDimension('N')->setWidth(20);
        $sheet->getColumnDimension('O')->setWidth(15);
        $sheet->getColumnDimension('P')->setWidth(20);
        $sheet->getColumnDimension('Q')->setWidth(25);
                $sheet->getColumnDimension('R')->setWidth(5);
        $sheet->getColumnDimension('S')->setWidth(5);
        $sheet->getColumnDimension('T')->setWidth(20);
        $sheet->getColumnDimension('U')->setWidth(15);
        $sheet->getColumnDimension('V')->setWidth(20);
        $sheet->getColumnDimension('W')->setWidth(25);


       
        //MENGATUR BORDER Table 1

        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        // $objPHPExcel->getActiveSheet('A27:K45')->getStyle('A27:K45')->applyFromArray($styleArray);
        unset($styleArray);
        $objPHPExcel->createSheet();
    }
     
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');

      exit;

  }


}