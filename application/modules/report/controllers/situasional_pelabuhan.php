<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Situasional_pelabuhan extends MX_Controller {

  /**
   * Nama File: 
   * Author: Frendhi
   * Deskripsi: 
   * 
   */
    private $list_report = array();
    function __construct()
    {
      parent::__construct();
      $this->load->library('entry/mkform');
      //$this->load->config('custom_constants');
      $this->list_report = $this->config->item('list_report');
      $this->load->model('mdl_situasional_pelabuhan');
      $this->load->model('jurnal/mdl_view_s6a');//detail pelabuhan
      $this->load->model('jurnal/mdl_view_s6b');//luas pelabuhan
      $this->load->model('mst_pelabuhan/mdl_pelabuhan');
      $this->load->model('jurnal/mdl_view_s7a');//breakwater
      $this->load->model('jurnal/mdl_view_s7b');//revetment
      $this->load->model('jurnal/mdl_view_s7c');//groin
      $this->load->model('jurnal/mdl_view_s7d');//dermaga
      $this->load->model('jurnal/mdl_view_s7e');//kolam_pelabuhan
      $this->load->model('jurnal/mdl_view_s8a');//tempat pelelangan ikan
      $this->load->model('jurnal/mdl_view_s8b');//lampu suar , rambu
      $this->load->model('jurnal/mdl_view_s8c');//sumber air
      $this->load->model('jurnal/mdl_view_s8d');//sumber air
      $this->load->model('jurnal/mdl_view_s8f');//layanan air penampungan/water treatmen
      $this->load->model('jurnal/mdl_view_s8g');//fire hydrant
      $this->load->model('jurnal/mdl_view_s8h');//layanan es
      $this->load->model('jurnal/mdl_view_s8j');//layanan listrik
      $this->load->model('jurnal/mdl_view_s8l');//layanan bbm / spbn
      $this->load->model('jurnal/mdl_view_s8m');//docking
      $this->load->model('jurnal/mdl_view_s8n');//fasiltas pelihara
      $this->load->model('jurnal/mdl_view_s8p');//fasiltas perikanan
      $this->load->model('jurnal/mdl_view_s8q');//kantor
      $this->load->model('jurnal/mdl_view_s8r');//kendaraan
      $this->load->model('jurnal/mdl_view_s8t');//olah limbah
      $this->load->model('jurnal/mdl_view_s9a');//balai
      $this->load->model('jurnal/mdl_view_s9b');//fasilitas pengelola
      $this->load->model('jurnal/mdl_view_s9c');//fasilitas umum

      //TODO: LOGIN CHECK HERE
    }

  private function _filtering()
  {
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
    }
    $input_filter = $this->input->post(NULL, TRUE);
    $data['pre_filter'] = empty($input_filter) ? FALSE : $input_filter;
    
    $data_situasional_pelabuhan = array();
    $data['is_multiple'] = FALSE;

    if(empty($input_filter['id_pelabuhan_selects']))
    {
      $data['situasional'] = $this->data_situasional($id_pelabuhan);
    }elseif(is_array($input_filter) && count($input_filter['id_pelabuhan_selects']) === 1){
      $id_pelabuhan = $input_filter['id_pelabuhan_selects'][0];
      $data['situasional'] = $this->data_situasional($id_pelabuhan);
    }else{
      $data['situasional'] = $this->data_situasional($input_filter['id_pelabuhan_selects']);
      $data['is_multiple'] = TRUE;
    }

    $data['id_pelabuhan'] = $id_pelabuhan;


    return $data;
  }

  public function data_situasional($select_id_pelabuhan='')
  {
    $data = array();
    $nama_pelabuhan = '';
    if(is_array($select_id_pelabuhan))
    {
      foreach ($select_id_pelabuhan as $id_pelabuhan) {
        $data_umum = $this->mdl_pelabuhan->list_per_pelabuhan($id_pelabuhan);
        $data[$data_umum->nama_pelabuhan] = array(
                  'data_umum' => (array)  $data_umum,

                  'luas_pelabuhan' => (array)  $this->mdl_view_s6b->get_data_terakhir($id_pelabuhan),

                  'breakwater' => (array) $this->mdl_view_s7a->data_terbaru($id_pelabuhan) ,


                 'revetment' => (array) $this->mdl_view_s7b->data_terbaru($id_pelabuhan),


                 'groin' => (array) $this->mdl_view_s7c->data_terbaru($id_pelabuhan),


                 'dermaga' => (array) $this->mdl_view_s7d->data_terbaru_spesifik($id_pelabuhan, "Warf/Dermaga"),


                 'jetty' => (array) $this->mdl_view_s7d->data_terbaru_spesifik($id_pelabuhan, "Jetty"),


                 'kolam_pelabuhan' => (array) $this->mdl_view_s7e->data_terbaru($id_pelabuhan),


                 'tpi' =>  (array) $this->mdl_view_s8a->data_terbaru($id_pelabuhan),   


                 'lampu_suar' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'rambu' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'menara_pengawas' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'sumber_air' => (array) $this->mdl_view_s8c->data_terbaru($id_pelabuhan),


                 'tangki_air' => (array) $this->mdl_view_s8f->data_terbaru($id_pelabuhan),


                 'cold_storage' => (array) $this->mdl_view_s8p->data_terbaru($id_pelabuhan),


                 'hydrant' => (array) $this->mdl_view_s8g->data_terbaru($id_pelabuhan),


                 'pabrik_es' => (array) $this->mdl_view_s8h->data_terbaru($id_pelabuhan),


                 'daya_listrik' => (array) $this->mdl_view_s8j->data_terbaru($id_pelabuhan),


                 'genset' => (array) $this->mdl_view_s8j->data_terbaru($id_pelabuhan),


                 'tangki_bbm' => (array) $this->mdl_view_s8l->data_terbaru($id_pelabuhan),


                 'spbn' => (array) $this->mdl_view_s8l->data_terbaru($id_pelabuhan),


                 'docking' => (array) $this->mdl_view_s8m->data_terbaru($id_pelabuhan),


                 'bengkel' =>  (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'penjemur_jaring' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'perbaikan_jaring' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'gudang_peralatan' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'kantor' => (array) $this->mdl_view_s8q->data_terbaru($id_pelabuhan),


                 'olah_limbah' => (array) $this->mdl_view_s8t->data_terbaru($id_pelabuhan),


                 'balai' => (array) $this->mdl_view_s9a->data_terbaru($id_pelabuhan),


                 'rumah_kepala'=> (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 1),


                 'rumah_karyawan'=> (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 2),


                 'mess_karyawan' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 3),


                 'pos_jaga' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 4),


                 'posyandu' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 5),


                 'tempat_peribadatan' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 1),


                 'klinik' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 4),


                 'mck' => (array)  $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 5),


                 'waserda' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 6),

                 'pegawai' => $this->mdl_pelabuhan->jumlah_pegawai($id_pelabuhan),
                 
                 'forklip' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Forklip"),
                 
                 'alat_berat' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Alat Berat"),
                 
                 'kapal_pengawas' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Pengawas"),
                 
                 'kapal_tunda' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Tunda"),

                 'kapal_keruk' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Keruk")
                 );
      }
    }else{
      //kalo bukan multiple pelabuhan
      $id_pelabuhan = $select_id_pelabuhan;
      $data_umum = $this->mdl_pelabuhan->list_per_pelabuhan($id_pelabuhan);
      $data = array(
                  'data_umum' => (array)  $data_umum[0],

                  'luas_pelabuhan' => (array)  $this->mdl_view_s6b->get_data_terakhir($id_pelabuhan),

                  'breakwater' => (array) $this->mdl_view_s7a->data_terbaru($id_pelabuhan) ,


                 'revetment' => (array) $this->mdl_view_s7b->data_terbaru($id_pelabuhan),


                 'groin' => (array) $this->mdl_view_s7c->data_terbaru($id_pelabuhan),


                 'dermaga' => (array) $this->mdl_view_s7d->data_terbaru_spesifik($id_pelabuhan, "Warf/Dermaga"),


                 'jetty' => (array) $this->mdl_view_s7d->data_terbaru_spesifik($id_pelabuhan, "Jetty"),


                 'kolam_pelabuhan' => (array) $this->mdl_view_s7e->data_terbaru($id_pelabuhan),


                 'tpi' =>  (array) $this->mdl_view_s8a->data_terbaru($id_pelabuhan),   


                 'lampu_suar' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'rambu' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'menara_pengawas' => (array) $this->mdl_view_s8b->data_terbaru($id_pelabuhan),


                 'sumber_air' => (array) $this->mdl_view_s8c->data_terbaru($id_pelabuhan),


                 'tangki_air' => (array) $this->mdl_view_s8f->data_terbaru($id_pelabuhan),


                 'cold_storage' => (array) $this->mdl_view_s8p->data_terbaru($id_pelabuhan),


                 'hydrant' => (array) $this->mdl_view_s8g->data_terbaru($id_pelabuhan),


                 'pabrik_es' => (array) $this->mdl_view_s8h->data_terbaru($id_pelabuhan),


                 'daya_listrik' => (array) $this->mdl_view_s8j->data_terbaru($id_pelabuhan),


                 'genset' => (array) $this->mdl_view_s8j->data_terbaru($id_pelabuhan),


                 'tangki_bbm' => (array) $this->mdl_view_s8l->data_terbaru($id_pelabuhan),


                 'spbn' => (array) $this->mdl_view_s8l->data_terbaru($id_pelabuhan),


                 'docking' => (array) $this->mdl_view_s8m->data_terbaru($id_pelabuhan),


                 'bengkel' =>  (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'penjemur_jaring' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'perbaikan_jaring' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'gudang_peralatan' => (array) $this->mdl_view_s8n->data_terbaru($id_pelabuhan),


                 'kantor' => (array) $this->mdl_view_s8q->data_terbaru($id_pelabuhan),


                 'olah_limbah' => (array) $this->mdl_view_s8t->data_terbaru($id_pelabuhan),


                 'balai' => (array) $this->mdl_view_s9a->data_terbaru($id_pelabuhan),

                 
                 'rumah_kepala'=> (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 1),


                 'rumah_karyawan'=> (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 2),


                 'mess_karyawan' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 3),


                 'pos_jaga' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 4),


                 'posyandu' => (array) $this->mdl_view_s9b->data_terbaru_spesifik($id_pelabuhan, 5),


                 'tempat_peribadatan' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 1),


                 'klinik' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 4),


                 'mck' => (array)  $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 5),


                 'waserda' => (array) $this->mdl_view_s9c->data_terbaru_spesifik($id_pelabuhan, 6),


                 'pegawai' => $this->mdl_pelabuhan->jumlah_pegawai($id_pelabuhan),
                 
                 'forklip' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Forklip"),
                 
                 'alat_berat' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Alat Berat"),
                 
                 'kapal_pengawas' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Pengawas"),
                 
                 'kapal_tunda' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Tunda"),

                 'kapal_keruk' => (array) $this->mdl_view_s8r->data_terbaru_spesifik($id_pelabuhan, "Kapal Keruk")

                 );
      

      // foreach ($data['pegawai'] as $item) {
      //   echo $item->bidang. " ";
      //   echo $item->jumlah;
      //   echo "<br>";
      // }

      // vdump($data['pegawai']);

      // vdump($data, true);
  
    }

     // $test = (array) $this->mdl_view_s9c->data_terbaru($id_pelabuhan);
     // var_dump($test);

    

    return $data;
  }



  public function index()
  {
    $data = $this->_filtering();
    // vdump($data);

    $data['module'] = 'report';
    $data['view_file'] = 'v_situasional_pelabuhan';
    $data['page_title'] = 'LAPORAN DATA UMUM PELABUHAN';
    $data['content_title'] = 'DATA UMUM PELABUHAN';
    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js','d3.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
    $data['breadcrumbs'] = 'Laporan > DATA UMUM PELABUHAN';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);
   
    $data['list_report'] = $this->list_report;
    echo Modules::run('templates/type/reporting', $data);
  }
  
  public function xls()
  {
    $data = $this->_filtering();
    
    if($data['is_multiple'] === FALSE)
    {
      $arr_situasional = array(0 => $data['situasional']);

    }else{
      $arr_situasional = $data['situasional'];
    }

    $this->load->library('excel');
    $objPHPExcel = $this->excel;

    $filename = "DATA UMUM PELABUHAN.xlsx";

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                   ->setLastModifiedBy("Maarten Balliauw")
                   ->setTitle("LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN")
                   ->setSubject("LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN")
                   ->setDescription("LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN.")
                   ->setKeywords("LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN")
                   ->setCategory("LAPORAN");
          $sheet_index = 0;         
          foreach ($arr_situasional as $key => $situasional) {
                              $objPHPExcel->setActiveSheetIndex($sheet_index);
                              // Rename worksheet
                              $objPHPExcel->getActiveSheet()->setTitle($situasional['data_umum']['nama_pelabuhan']);
                              
                              $current_sheet = $objPHPExcel->getActiveSheet();
                
                                        // Add some data
                                        //==================================poin 02==================================================
                                        $current_sheet->getStyle('B10:B18')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B10:B18')->getFont()->setSize(11);
                
                                        $current_sheet->getStyle('C7:E18')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('C7:E18')->getFont()->setSize(11);
                
                
                                        $current_sheet
                                                    ->setCellValue('C7', ': '.kos( $situasional['data_umum']['nama_pelabuhan'] ) ) 
                                                    ->setCellValue('C9', ':')
                                                    ->setCellValue('B10', '- Alamat')
                                                    ->setCellValue('C10', ': '.kos( $situasional['data_umum']['alamat_pelabuhan'] ) )
                                                    ->setCellValue('B11', '- Nomor Telepon')
                                                    ->setCellValue('C11', ': '.kos( $situasional['data_umum']['no_telp1'] ) )
                                                    ->setCellValue('B12', '- Nomor Faksimile')
                                                    ->setCellValue('C12', ': '.kos( $situasional['data_umum']['no_faksimile'] ) )
                                                    ->setCellValue('B13', '- Email/Website')
                                                    ->setCellValue('C13', ': '.kos( $situasional['data_umum']['email'], '-' ) )
                                                    ->setCellValue('B14', '- Desa/Kelurahan')
                                                    ->setCellValue('C14', ': '.kos( $situasional['data_umum']['desa'], '-' ) )
                                                    ->setCellValue('B15', '- Kecamatan')
                                                    ->setCellValue('C15', ': '.kos( $situasional['data_umum']['kecamatan'], '-' ) )
                                                    ->setCellValue('B16', '- Kabupaten/Kota')
                                                    ->setCellValue('C16', ': '.kos( $situasional['data_umum']['nama_kabupaten_kota'] ) )
                                                    ->setCellValue('B17', '- Propinsi')
                                                    ->setCellValue('C17', ': '.kos( $situasional['data_umum']['nama_propinsi'] ) )
                                                    ->setCellValue('B18', '- Titik Koordinat')
                                                    ->setCellValue('C18', ': '.kos( $situasional['data_umum']['lintang'] ) .','.kos( $situasional['data_umum']['bujur'] ) );
                
                                        //================================================tabel 03.a==================================
                                        $current_sheet->getStyle('B23:E30')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B23:E30')->getFont()->setSize(10);
                
                                        $current_sheet            
                                                    ->setCellValue('B23', 'Jenis Fasilitas *)')
                                                    ->setCellValue('C23', 'Kapasitas/Ukuran')
                                                    ->setCellValue('D23', 'Kondisi*')
                                                    ->setCellValue('E23', 'Tahun Pengadaan')
                                                    ->setCellValue('B24', '- Areal Pelabuhan ')
                                                    ->setCellValue('C24', kos( $situasional['luas_pelabuhan']['luas_daratan'] ).' Ha' )
                                                    ->setCellValue('D24', '-')
                                                    ->setCellValue('E24', '-')
                                                    ->setCellValue('B25', '- Dermaga')
                                                    ->setCellValue('C25', 'P: '.kos( $situasional['dermaga']['panjang']).', L: '.kos( $situasional['dermaga']['lebar'] ) )
                                                    ->setCellValue('D25', kos( $situasional['dermaga']['ket_rehab'] ) )
                                                    ->setCellValue('E25', kos( $situasional['dermaga']['tahun'] ) )
                                                    ->setCellValue('B26', '- Jetty')
                                                    ->setCellValue('C26', 'P: '.kos( $situasional['jetty']['panjang']).', L: '.kos( $situasional['jetty']['lebar'] ) )
                                                    ->setCellValue('D26', kos( $situasional['jetty']['ket_rehab'] ) )
                                                    ->setCellValue('E26', kos( $situasional['jetty']['tahun'] ) )
                                                    ->setCellValue('B27', '- Pemecah gelombang(break water) ')
                                                    ->setCellValue('C27', '')
                                                    ->setCellValue('D27', kos( $situasional['breakwater']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E27', kos( $situasional['breakwater']['tahun'] ) )
                                                    ->setCellValue('B28', '- Kolam Pelabuhan')
                                                    ->setCellValue('C28', kos( $situasional['kolam_pelabuhan']['luas_kolam'] ) )
                                                    ->setCellValue('D28', kos( $situasional['kolam_pelabuhan']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E28', kos( $situasional['kolam_pelabuhan']['tahun'] ) )
                                                    ->setCellValue('B29', '- Dinding Penahan Tanah (Revetment) ')
                                                    ->setCellValue('C29', ' ')
                                                    ->setCellValue('D29', kos( $situasional['revetment']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E29', kos( $situasional['revetment']['tahun'] ) )
                                                    ->setCellValue('B30', '- Penahan Sedimen (Groin) ')
                                                    ->setCellValue('C30', 'P: '.kos( $situasional['groin']['panjang']).', L: '.kos( $situasional['groin']['lebar'] ) )
                                                    ->setCellValue('D30', kos( $situasional['groin']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E30', kos( $situasional['groin']['tahun'] ) );
                
                
                                        //===================================tabel 03.c=======================================
                                        $current_sheet->getStyle('B33:E33')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B33:E33')->getFont()->setSize(10);
                
                                        $current_sheet->getStyle('B34:E43')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B33:E43')->getFont()->setSize(11);
                
                
                                        $current_sheet            
                                                    ->setCellValue('B33', 'Jenis Fasilitas *)')
                                                    ->setCellValue('C33', 'Kapasitas/Ukuran')
                                                    ->setCellValue('D33', 'Kondisi*')
                                                    ->setCellValue('E33', 'Tahun Pengadaan')
                                                    ->setCellValue('B34', '- Balai Pertemuan Nelayan ')
                                                    ->setCellValue('C34', kos( $situasional['balai']['luas'] ) )
                                                    ->setCellValue('D34', kos( $situasional['balai']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E34', kos( $situasional['balai']['tahun'] ) )
                                                    ->setCellValue('B35', '- Rumah Kepala Pelabuhan')
                                                    ->setCellValue('C35', kos( $situasional['rumah_kepala']['luas'] ) )
                                                    ->setCellValue('D35', kos( $situasional['rumah_kepala']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E35', kos( $situasional['rumah_kepala']['tahun'] ) )
                                                    ->setCellValue('B36', '- Rumah Karyawan')
                                                    ->setCellValue('C36', kos( $situasional['rumah_karyawan']['luas'] ) )
                                                    ->setCellValue('D36', kos( $situasional['rumah_karyawan']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E36', kos( $situasional['rumah_karyawan']['tahun'] ) )
                                                    ->setCellValue('B37', '- Mess Karyawan')
                                                    ->setCellValue('C37', kos( $situasional['mess_karyawan']['luas'] ) )
                                                    ->setCellValue('D37', kos( $situasional['mess_karyawan']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E37', kos( $situasional['mess_karyawan']['tahun'] ) )
                                                    ->setCellValue('B38', '- Pos Jaga ')
                                                    ->setCellValue('C38', kos( $situasional['pos_jaga']['luas'] ) )
                                                    ->setCellValue('D38', kos( $situasional['pos_jaga']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E38', kos( $situasional['pos_jaga']['tahun'] ) )
                                                    ->setCellValue('B39', '- Pos Pelayanan Terpadu')
                                                    ->setCellValue('C39', kos( $situasional['posyandu']['luas'] ) )
                                                    ->setCellValue('D39', kos( $situasional['posyandu']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E39', kos( $situasional['posyandu']['tahun'] ) )
                                                    ->setCellValue('B40', '- Tempat Peribadatan ')
                                                    ->setCellValue('C40', kos( $situasional['tempat_peribadatan']['luas'] ) )
                                                    ->setCellValue('D40', kos( $situasional['tempat_peribadatan']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E40', kos( $situasional['tempat_peribadatan']['tahun'] ) )
                                                    ->setCellValue('B41', '- Klinik Kesehatan ')
                                                    ->setCellValue('C41', kos( $situasional['klinik']['luas'] ) )
                                                    ->setCellValue('D41', kos( $situasional['klinik']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E41', kos( $situasional['klinik']['tahun'] ) )
                                                    ->setCellValue('B42', '- MCK ')
                                                    ->setCellValue('C42', kos( $situasional['mck']['luas'] ) )
                                                    ->setCellValue('D42', kos( $situasional['mck']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E42', kos( $situasional['mck']['tahun'] ) )
                                                    ->setCellValue('B43', '- Waserda/Toko ')
                                                    ->setCellValue('C43', kos( $situasional['waserda']['luas'] ) )
                                                    ->setCellValue('D43', kos( $situasional['waserda']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('E43', kos( $situasional['waserda']['tahun'] ) );
                
                                        //===================================tabel 03.b=======================================
                                        $current_sheet->getStyle('G8:J8')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G8:J8')->getFont()->setSize(10);
                
                                        $current_sheet->getStyle('G9:G37')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G9:G37')->getFont()->setSize(10);
                
                                        $current_sheet->getStyle('H9:J37')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('H9:J37')->getFont()->setSize(11);
                
                                        $current_sheet            
                                                    ->setCellValue('G8', 'Jenis Fasilitas *)')
                                                    ->setCellValue('H8', 'Kapasitas/Ukuran')
                                                    ->setCellValue('I8', 'Kondisi*')
                                                    ->setCellValue('J8', 'Tahun Pengadaan')
                                                    ->setCellValue('G9', '- Tempat Pelelangan Ikan (TPI)')
                                                    ->setCellValue('H9', kos( $situasional['tpi']['luas'] ) )
                                                    ->setCellValue('I9', kos( $situasional['tpi']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J9', kos( $situasional['tpi']['tahun'] ) )
                                                    ->setCellValue('G10','- Pasar Ikan')
                                                    ->setCellValue('H10', ' ')
                                                    ->setCellValue('I10', ' ')
                                                    ->setCellValue('J10', ' ')
                                                    ->setCellValue('G11', '- Rambu-rambu Navigasi')
                                                    ->setCellValue('H11', ' ')
                                                    ->setCellValue('I11', kos( $situasional['rambu']['ket_teknis'] ) )
                                                    ->setCellValue('J11', kos( $situasional['rambu']['tahun'] ) )
                                                    ->setCellValue('G12', '- Lampu Suar')
                                                    ->setCellValue('H12', ' ')
                                                    ->setCellValue('I12', kos( $situasional['lampu_suar']['ket_teknis'] ) )
                                                    ->setCellValue('J12', kos( $situasional['lampu_suar']['tahun'] ) )
                                                    ->setCellValue('G13', '- Menara Pengawas')
                                                    ->setCellValue('H13', ' ')
                                                    ->setCellValue('I13', kos( $situasional['menara_pengawas']['ket_teknis'] ) )
                                                    ->setCellValue('J13', kos( $situasional['menara_pengawas']['tahun'] ) )
                                                    ->setCellValue('G14', '- Sumber Air')
                                                    ->setCellValue('H14', ' ')
                                                    ->setCellValue('I14', kos( $situasional['sumber_air']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J14', kos( $situasional['sumber_air']['tahun'] ) )
                                                    ->setCellValue('G15', '- Penampung/Tangki Air')
                                                    ->setCellValue('H15', ' ')
                                                    ->setCellValue('I15', kos( $situasional['tangki_air']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J15', kos( $situasional['tangki_air']['tahun'] ) )
                                                    ->setCellValue('G16', '- Pengolahan Air')
                                                    ->setCellValue('H16', ' ')
                                                    ->setCellValue('I16', ' ')
                                                    ->setCellValue('J16', ' ')
                                                    ->setCellValue('G17', '- Fire Safety/Hydrant')
                                                    ->setCellValue('H17', kos( $situasional['hydrant']['luas'] ) )
                                                    ->setCellValue('I17', kos( $situasional['hydrant']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J17', kos( $situasional['hydrant']['tahun'] ) )
                                                    ->setCellValue('G18', '- Pabrik ES')
                                                    ->setCellValue('H18', kos( $situasional['pabrik_es']['luas'] ) )
                                                    ->setCellValue('I18', kos( $situasional['pabrik_es']['ket_teknis'] ) )
                                                    ->setCellValue('J18', kos( $situasional['pabrik_es']['tahun'] ) )
                                                    ->setCellValue('G19', '- Cold Storage')
                                                    ->setCellValue('H19', kos( $situasional['cold_storage']['luas'] ) )
                                                    ->setCellValue('I19', kos( $situasional['cold_storage']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J19', kos( $situasional['cold_storage']['tahun'] ) )
                                                    ->setCellValue('G20', '- Daya Listrik')
                                                    ->setCellValue('H20', kos( $situasional['daya_listrik']['daya_total'] ) )
                                                    ->setCellValue('I20', kos( $situasional['daya_listrik']['ket_teknis'] ) )
                                                    ->setCellValue('J20', kos( $situasional['daya_listrik']['tahun'] ) )
                                                    ->setCellValue('G21', '- Genset')
                                                    ->setCellValue('H21', ' ')
                                                    ->setCellValue('I21', kos( $situasional['genset']['ket_teknis'] ) )
                                                    ->setCellValue('J21', kos( $situasional['genset']['tahun'] ) )
                                                    ->setCellValue('G22', '- SPBN')
                                                    ->setCellValue('H22', ' ')
                                                    ->setCellValue('I22', kos( $situasional['spbn']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J22', kos( $situasional['spbn']['tahun'] ) )
                                                    ->setCellValue('G23', '- Tangki BBM')
                                                    ->setCellValue('H23', ' ')
                                                    ->setCellValue('I23', kos( $situasional['tangki_bbm']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J23', kos( $situasional['tangki_bbm']['tahun'] ) )
                                                    ->setCellValue('G24', '- Docking')
                                                    ->setCellValue('H24', ' ')
                                                    ->setCellValue('I24', kos( $situasional['docking']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J24', kos( $situasional['docking']['tahun'] ) )
                                                    ->setCellValue('G25', '- Slipaway')
                                                    ->setCellValue('H25', ' ')
                                                    ->setCellValue('I25', ' ')
                                                    ->setCellValue('J25', ' ')
                                                    ->setCellValue('G26', '- Bengkel')
                                                    ->setCellValue('H26', kos( $situasional['bengkel']['luas'] ) )
                                                    ->setCellValue('I26', kos( $situasional['bengkel']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J26', kos( $situasional['bengkel']['tahun'] ) )
                                                    ->setCellValue('G27', '- Penjemur Jaring')
                                                    ->setCellValue('H27', kos( $situasional['penjemur_jaring']['luas'] ) )
                                                    ->setCellValue('I27', kos( $situasional['penjemur_jaring']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J27', kos( $situasional['penjemur_jaring']['tahun'] ) )
                                                    ->setCellValue('G28', '- Perbaikan Jaring')
                                                    ->setCellValue('H28', kos( $situasional['perbaikan_jaring']['luas'] ) )
                                                    ->setCellValue('I28', kos( $situasional['perbaikan_jaring']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J28', kos( $situasional['perbaikan_jaring']['tahun'] ) )
                                                    ->setCellValue('G29', '- Gudang Peralatan')
                                                    ->setCellValue('H29', kos( $situasional['gudang_peralatan']['luas'] ) )
                                                    ->setCellValue('I29', kos( $situasional['gudang_peralatan']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J29', kos( $situasional['gudang_peralatan']['tahun'] ) )
                                                    ->setCellValue('G30', '- Kantor')
                                                    ->setCellValue('H30', kos( $situasional['kantor']['luas'] ) )
                                                    ->setCellValue('I30', kos( $situasional['kantor']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J30', kos( $situasional['kantor']['tahun'] ) )
                                                    ->setCellValue('G31', '- Forklip')
                                                    ->setCellValue('H31', kos( $situasional['forklip']['jml'] ) )
                                                    ->setCellValue('I31', kos( $situasional['forklip']['ket'] ) )
                                                    ->setCellValue('J31', kos( $situasional['forklip']['tahun'] ) )
                                                    ->setCellValue('G32', '- Alat Berat')
                                                    ->setCellValue('H32', kos( $situasional['alat_berat']['jml'] ) )
                                                    ->setCellValue('I32', kos( $situasional['alat_berat']['ket'] ) )
                                                    ->setCellValue('J32', kos( $situasional['alat_berat']['tahun'] ) )
                                                    ->setCellValue('G33', '- Kapal Pengawas')
                                                    ->setCellValue('H33', kos( $situasional['kapal_pengawas']['jml'] ) )
                                                    ->setCellValue('I33', kos( $situasional['kapal_pengawas']['ket'] ) )
                                                    ->setCellValue('J33', kos( $situasional['kapal_pengawas']['tahun'] ) )
                                                    ->setCellValue('G34', '- Kapal Tunda')
                                                    ->setCellValue('H34', kos( $situasional['kapal_tunda']['jml'] ) )
                                                    ->setCellValue('I34', kos( $situasional['kapal_tunda']['ket'] ) )
                                                    ->setCellValue('J34', kos( $situasional['kapal_tunda']['tahun'] ) )
                                                    ->setCellValue('G35', '- Kapal Keruk')
                                                    ->setCellValue('H35', kos( $situasional['kapal_keruk']['jml'] ) )
                                                    ->setCellValue('I35', kos( $situasional['kapal_keruk']['ket'] ) )
                                                    ->setCellValue('J35', kos( $situasional['kapal_keruk']['tahun'] ) )
                                                    ->setCellValue('G36', '- Instalasi Pengolahan Limbah')
                                                    ->setCellValue('H36', kos( $situasional['olah_limbah']['luas'] ) )
                                                    ->setCellValue('I36', kos( $situasional['olah_limbah']['nama_jns_ket_konstruksi'] ) )
                                                    ->setCellValue('J36', kos( $situasional['olah_limbah']['tahun'] ) )
                                                    ->setCellValue('G37', '- Tempat Pembuangan Sampah')
                                                    ->setCellValue('H37', ' ')
                                                    ->setCellValue('I37', ' ')
                                                    ->setCellValue('J37', ' ');
                
                                        //===================================tabel 04=======================================
                                        $current_sheet->mergeCells('B47:B48');
                                        $current_sheet->mergeCells('C47:F47');
                                        $current_sheet->mergeCells('G47:G48');
                                        $current_sheet->mergeCells('H47:H48');    
                                        $current_sheet->getStyle('B47:H48')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                                        $current_sheet->getStyle('B54')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                                        $current_sheet->getStyle('B47:F48')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B47:F48')->getFont()->setSize(11);
                
                                        $current_sheet->getStyle('B49:B53')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B49:B53')->getFont()->setSize(10);
                
                                        $current_sheet->getStyle('G47:H47')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G47:H47')->getFont()->setSize(9);
                
                                        $current_sheet->getStyle('C49:H53')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('C49:H53')->getFont()->setSize(11);
                
                                        $current_sheet->getStyle('C49:H53')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('C49:H53')->getFont()->setSize(11);
                
                                        $current_sheet->getStyle('B54:H54')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B54:H54')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B54:H54')->getFont()->setBold(true);
                
                                        /*
                                                    ->setCellValue('B50', '2. Bagian Tata Usaha')
                                                    ->setCellValue('C50', ' ')
                                                    ->setCellValue('D50', ' ')
                                                    ->setCellValue('E50', ' ')
                                                    ->setCellValue('F50', ' ')
                                                    ->setCellValue('G50', ' ')
                                                    ->setCellValue('H50', ' ')
                                                    ->setCellValue('B51', '3. Bidang Pengembangan')
                                                    ->setCellValue('C51', ' ')
                                                    ->setCellValue('D51', ' ')
                                                    ->setCellValue('E51', ' ')
                                                    ->setCellValue('F51', ' ')
                                                    ->setCellValue('G51', ' ')
                                                    ->setCellValue('H51', ' ')
                                                    ->setCellValue('B52', '4. Bidang Tata Operasional')
                                                    ->setCellValue('C52', ' ')
                                                    ->setCellValue('D52', ' ')
                                                    ->setCellValue('E52', ' ')
                                                    ->setCellValue('F52', ' ')
                                                    ->setCellValue('G52', ' ')
                                                    ->setCellValue('H52', ' ')
                                                    ->setCellValue('B53', '5. Lainnya (Sebutkan)')
                                                    ->setCellValue('C53', ' ')
                                                    ->setCellValue('D53', ' ')
                                                    ->setCellValue('E53', ' ')
                                                    ->setCellValue('F53', ' ')
                                                    ->setCellValue('G53', ' ')
                                                    ->setCellValue('H53', ' ')*/
                                                    // MASUKKAN DATA
    
                                         
                
                
                
                                        //============================================================================================================
                
                                        //=================================================judul===================================================
                                        $current_sheet->mergeCells('A2:J2');
                                        $current_sheet->setCellValue('A2',"LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN");
                
                                        $current_sheet->getStyle('A2:J2')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A2:J2')->getFont()->setSize(16);
                                        $current_sheet->getStyle('A2:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                
                                        //sub judul
                                        $current_sheet->setCellValue('A4',"DIREKTORAT JENDERAL PERIKANAN TANGKAP");
                                        $current_sheet->getStyle('A4')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A4')->getFont()->setSize(12);
                                        $current_sheet->getStyle('A4')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('A5',"DIREKTORAT PELABUHAN PERIKANAN");
                                        $current_sheet->getStyle('A5')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A5')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('I4',"Bulan :");
                                        $current_sheet->getStyle('I4')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('I4')->getFont()->setSize(11);
                
                                        $current_sheet->setCellValue('I5',"Tahun :");
                                        $current_sheet->getStyle('I5')->getFont()->setName('Calibri');
                                        $current_sheet->getStyle('I5')->getFont()->setSize(11);
                
                                        $current_sheet->getStyle('J4:J6')->getFont()->setName('Calibri');
                                        $current_sheet->getStyle('J4:J6')->getFont()->setSize(11);
                
                                        $current_sheet->setCellValue('I6',"Kode PP :");
                                        $current_sheet->getStyle('I6')->getFont()->setName('Calibri');
                                        $current_sheet->getStyle('I6')->getFont()->setSize(11);
                
                                        $current_sheet->setCellValue('A7', "01. ");
                                        $current_sheet->getStyle('A7')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A7')->getFont()->setSize(11);
                                        $current_sheet->getStyle('A7')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('B7', 'NAMA PELABUHAN');
                                        $current_sheet->getStyle('B7')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B7')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B7')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('A9', "02. ");
                                        $current_sheet->getStyle('A9')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A9')->getFont()->setSize(11);
                                        $current_sheet->getStyle('A9')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('B9', 'LOKASI PELABUHAN');
                                        $current_sheet->getStyle('B9')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B9')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B9')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('A20', "03. ");
                                        $current_sheet->getStyle('A20')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A20')->getFont()->setSize(11);
                                        $current_sheet->getStyle('A20')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('B20', 'KEADAAN FASILITAS PELABUHAN');
                                        $current_sheet->getStyle('B20')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B20')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B20')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('B22', 'a. Fasilitas Pokok ');
                                        $current_sheet->getStyle('B22')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B22')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B22')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('G7', 'b. Fasilitas Fungsional ');
                                        $current_sheet->getStyle('G7')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G7')->getFont()->setSize(9);
                                        $current_sheet->getStyle('G7')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('B32', 'c. Fasilitas Penunjang ');
                                        $current_sheet->getStyle('B32')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B32')->getFont()->setSize(11);
                                        $current_sheet->getStyle('B32')->getFont()->setBold(true);
                
                                        $current_sheet->setCellValue('A45', "04. ");
                                        $current_sheet->getStyle('A45')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('A45')->getFont()->setSize(11);
                                        $current_sheet->getStyle('A45')->getFont()->setBold(true);
                
                                        // $current_sheet->setCellValue('B45', "KEPEGAWAIAN");
                                        // $current_sheet->getStyle('B45')->getFont()->setName('Trebuchet MS');
                                        // $current_sheet->getStyle('B45')->getFont()->setSize(11);
                                        // $current_sheet->getStyle('B45')->getFont()->setBold(true);
                                        //=================================================================================
                
                                        //==================================border tabel=========================================== 
                                        $styleArray = array(
                                          'borders' => array(
                                            'allborders' => array(
                                              'style' => PHPExcel_Style_Border::BORDER_THIN
                                            )
                                          )
                                        );
                
                
                                        //===========================03. a fasilitas pokok=====================================
                                        $current_sheet->getStyle('B23:E30')->applyFromArray($styleArray);
                
                                        //===========================03. b fasilitas FUngsional =================================
                                        $current_sheet->getStyle('G8:J37')->applyFromArray($styleArray);
                                        //===========================03. c fasilitas penunjang==================================
                                        $current_sheet->getStyle('B33:E43')->applyFromArray($styleArray);
                                        //===========================04. Kepegawaian ======================================
                                        // $current_sheet->getStyle('B47:H54')->applyFromArray($styleArray);
                                        // $current_sheet->getStyle('J3')->applyFromArray($styleArray);
                                        unset($styleArray);
                                        //========================================================================================
                
                
                                        //==================================keterangan=================================
                                        $current_sheet->setCellValue('B44', 'Ket :  *) Disesuaikan dengan jenis fasilitas yang ada');
                                        $current_sheet->getStyle('B44')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B44')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G39', 'Keterangan   : **)');
                                        $current_sheet->getStyle('G39')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G39')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G40', '- Bila Belum dimiliki ');
                                        $current_sheet->getStyle('G40')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G40')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G41', ' - Bila Sudah Berfungsi Dalam Keadaan baik');
                                        $current_sheet->getStyle('G41')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G41')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G42', '- Bila Sudah Berfungsi Dalam Keadaan rusak');
                                        $current_sheet->getStyle('G42')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G42')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G43', ' - Bila belum Berfungsi Dalam Keadaan baik');
                                        $current_sheet->getStyle('G43')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G43')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G44', ' - Bila belum Berfungsi Dalam Keadaan rusak');
                                        $current_sheet->getStyle('G44')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G44')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('G45', ' - Bila belum Berfungsi karena dalam tahap pembangunan)');
                                        $current_sheet->getStyle('G45')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('G45')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J40', 'Kode : 0');
                                        $current_sheet->getStyle('J40')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J40')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J41', 'Kode : 1');
                                        $current_sheet->getStyle('J41')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J41')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J42', 'Kode : 2');
                                        $current_sheet->getStyle('J42')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J42')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J43', 'Kode : 3');
                                        $current_sheet->getStyle('J43')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J43')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J44', 'Kode : 4');
                                        $current_sheet->getStyle('J44')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J44')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('J45', 'Kode : 5');
                                        $current_sheet->getStyle('J45')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('J45')->getFont()->setSize(10);
                
                                        $current_sheet->setCellValue('B56', 'Ket :  ***) Isi menurut Klas Pelabuhan');
                                        $current_sheet->getStyle('B56')->getFont()->setName('Trebuchet MS');
                                        $current_sheet->getStyle('B56')->getFont()->setSize(10);
                
                                        //=============================================================================================
                
                                        //====================================atur  kolom Dan baris==============================
                                        $current_sheet->getColumnDimension('A')->setWidth(5);
                                        $current_sheet->getColumnDimension('B')->setWidth(40);
                                        $current_sheet->getColumnDimension('C')->setWidth(20);
                                        $current_sheet->getColumnDimension('D')->setWidth(40);
                                        $current_sheet->getColumnDimension('E')->setWidth(20);
                                        $current_sheet->getColumnDimension('F')->setWidth(10);
                                        $current_sheet->getColumnDimension('G')->setWidth(30);
                                        $current_sheet->getColumnDimension('H')->setWidth(20);
                                        $current_sheet->getColumnDimension('J')->setWidth(20);

                                        //------------------------------buat tabel untuk kepegawaian--------
                                        $cell_honorer;
                                        $current_sheet       
                                                    ->setCellValue('B47', 'Bagian***')
                                                    ->setCellValue('C47', 'PNS / Golongan')
                                                    ->setCellValue('C48', 'I')
                                                    ->setCellValue('D48', 'II')
                                                    ->setCellValue('E48', 'III')
                                                    ->setCellValue('F48', 'IV')
                                                    ->setCellValue('G47', 'Honorer')
                                                    ->setCellValue('H47', 'Jumlah');

                                        $current_sheet->setCellValue('B45', "KEPEGAWAIAN");
                                                    $current_sheet->getStyle('B45')->getFont()->setName('Trebuchet MS');
                                                    $current_sheet->getStyle('B45')->getFont()->setSize(11);
                                                    $current_sheet->getStyle('B45')->getFont()->setBold(true);

                                        $styleArray = array(
                                              'borders' => array(
                                                'allborders' => array(
                                                  'style' => PHPExcel_Style_Border::BORDER_THIN
                                                )
                                              )
                                            );

                                        //jika data pegawai ada
                                        if($situasional['pegawai']){


                                            // vdump($situasional['pegawai'],true);
                                            $cell_number = 50;
                                            $index_num = 2;
                                                        // vdump($situasional['pegawai'],TRUE);
                                            foreach (kos($situasional['pegawai']) as $item => $key) {
                                              if( strcmp(strtoupper($key->bidang), 'KEPALA PELABUHAN') == 0)
                                              {
                                                   $current_sheet 
                                                        ->setCellValue('B49', '1. Kepala Pelabuhan')
                                                        ->setCellValue('C49', kos($key->I))
                                                        ->setCellValue('D49', kos($key->II))
                                                        ->setCellValue('E49', kos($key->III))
                                                        ->setCellValue('F49', kos($key->IV))
                                                        ->setCellValue('G49', kos($key->Honorer))
                                                        ->setCellValue('H49', kos($key->Jumlah));
                                              }
                                              else {
                                                $current_sheet 
                                                        ->setCellValue('B'.$cell_number, $index_num.'. '.$key->bidang)
                                                        ->setCellValue('C'.$cell_number, kos($key->I))
                                                        ->setCellValue('D'.$cell_number, kos($key->II))
                                                        ->setCellValue('E'.$cell_number, kos($key->III))
                                                        ->setCellValue('F'.$cell_number, kos($key->IV))
                                                        ->setCellValue('G'.$cell_number, kos($key->Honorer))
                                                        ->setCellValue('H'.$cell_number, kos($key->Jumlah));
                                                $cell_number++;
                                                $index_num++;             
                                              }
                                            }

                                            $current_sheet->getStyle('B47:H'.($cell_number-1))->applyFromArray($styleArray);
                                            $current_sheet->getStyle('J3')->applyFromArray($styleArray);
                                        }else{
                                            //Jika kosong bikin tabel kosong ajah
                                            $current_sheet 
                                                        ->setCellValue('B49', '1. Kepala Pelabuhan')
                                                        ->setCellValue('C49', '')
                                                        ->setCellValue('D49', '')
                                                        ->setCellValue('E49', '')
                                                        ->setCellValue('F49', '')
                                                        ->setCellValue('G49', '')
                                                        ->setCellValue('H49', '')
                                                        ->setCellValue('B50', '2. Bagian Tata Usaha')
                                                        ->setCellValue('C50', ' ')
                                                        ->setCellValue('D50', ' ')
                                                        ->setCellValue('E50', ' ')
                                                        ->setCellValue('F50', ' ')
                                                        ->setCellValue('G50', ' ')
                                                        ->setCellValue('H50', ' ')
                                                        ->setCellValue('B51', '3. Bidang Pengembangan')
                                                        ->setCellValue('C51', ' ')
                                                        ->setCellValue('D51', ' ')
                                                        ->setCellValue('E51', ' ')
                                                        ->setCellValue('F51', ' ')
                                                        ->setCellValue('G51', ' ')
                                                        ->setCellValue('H51', ' ')
                                                        ->setCellValue('B52', '4. Bidang Tata Operasional')
                                                        ->setCellValue('C52', ' ')
                                                        ->setCellValue('D52', ' ')
                                                        ->setCellValue('E52', ' ')
                                                        ->setCellValue('F52', ' ')
                                                        ->setCellValue('G52', ' ')
                                                        ->setCellValue('H52', ' ')
                                                        ->setCellValue('B53', '5. Lainnya (Sebutkan)')
                                                        ->setCellValue('C53', ' ')
                                                        ->setCellValue('D53', ' ')
                                                        ->setCellValue('E53', ' ')
                                                        ->setCellValue('F53', ' ')
                                                        ->setCellValue('G53', ' ')
                                                        ->setCellValue('H53', ' ');

                                            // ===========================04. Kepegawaian ======================================
                                            $current_sheet->getStyle('B47:H54')->applyFromArray($styleArray);
                                            $current_sheet->getStyle('J3')->applyFromArray($styleArray); 
                                        }
                                        unset($styleArray);
                                        //------------------------------tutup tabel untuk kepegawaian--------

                $objPHPExcel->createSheet();
                $sheet_index++;
                }
 


    // Save Excel 2007 file
    $callStartTime = microtime(true);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
  }


  
}