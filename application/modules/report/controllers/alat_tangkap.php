<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alat_tangkap extends MX_Controller {

  /**
   * Controller Mst WPP
   * created by ariefmr
   * at McD
   * 25-09-2013
   * 
   */


  function __construct()
  {
    parent::__construct();
      $this->load->library('entry/mkform');

    //$this->load->config('custom_constants');

    $this->assets_paths = $this->config->item('assets_paths');
    $this->load->model('mdl_alat_tangkap');
  }

  private function _filtering()
  {
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
      $data['is_admin'] = FALSE;
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
      $data['is_admin'] = TRUE;
    }

    $input_filter = $this->input->post(NULL, TRUE);
    //BUAT NGAMBIL SEMUA INPUT POST
    
    $data['pre_filter'] = FALSE;

    if ( empty($input_filter) ) {
        $array_filter = array('id_pelabuhan' => $id_pelabuhan,
                              'bulan' => date("m", time()), 
                              'tahun' => date("Y", time()), 
                              'filters' => FALSE
                              );
    }else{
        $array_filter = array('id_pelabuhan' => $id_pelabuhan,
                              'filters' => $input_filter
                              );
    }


    // var_dump($input_filter);
    $data['pre_filters'] = $input_filter;
    $data['jumlah_kapal'] = $this->mdl_alat_tangkap->alat_tangkap($array_filter);
    $data['id_pelabuhan'] = $id_pelabuhan;
    return $data;
  }

  public function index()
  {
    
    $data = $this->_filtering();

    /* WAJIB ADA <---- */
    
    $data['module'] = 'report';
    $data['view_file'] = 'v_alat_tangkap';
    $data['page_title'] = 'LAPORAN DATA ALAT TANGKAP';
    $data['content_title'] = 'DATA ALAT TANGKAP';

    $data['additional_js'] = Array('jquery.dataTables.min.js','select2.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');

    $data['breadcrumbs'] = 'Laporan > DATA ALAT TANGKAP';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);
    /* ---> WAJIB ADA*/

    echo Modules::run('templates/type/reporting', $data);
      
  }

  public function xls()
  {
    $data = $this->_filtering();

    $jumlah_kapal = $data['jumlah_kapal'];

    $this->load->library('excel');

    $filename = "DATA ALAT TANGKAP.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Alat Tangkap');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    // SUBJUDUL
    $sheet      ->setCellValue('A3', '06. DATA ALAT TANGKAP');
                      $styleArray = array('font' => array('bold' => true));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:B3');
    $sheet      ->setCellValue('F3', 'Bulan :');
    $sheet      ->setCellValue('f4', 'Tahun :');

    $styleArray = array('font' => array('name' => 'Trebuchet MS','size' => 11));
    $sheet->getStyle('A3:G100')->applyFromArray($styleArray);

    //DATA 1
    $sheet      ->setCellValue('A5', 'NO.');


                            $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                            $sheet->getStyle('A5:A55')->applyFromArray($styleArray);
                            //Bold huruf
                            $styleArray2 = array('font' => array('bold' => true));
                            $sheet->getStyle('A5')->applyFromArray($styleArray2);
                            $sheet->getStyle('A6')->applyFromArray($styleArray2);
                            $sheet->getStyle('A11')->applyFromArray($styleArray2);
                            $sheet->getStyle('A20')->applyFromArray($styleArray2);
                            $sheet->getStyle('A23')->applyFromArray($styleArray2);
                            $sheet->getStyle('A26')->applyFromArray($styleArray2);
                            $sheet->getStyle('A32')->applyFromArray($styleArray2);
                            $sheet->getStyle('A35')->applyFromArray($styleArray2);
                            $sheet->getStyle('A42')->applyFromArray($styleArray2);
                            
                           
                            

    // DATA 2
    $sheet      ->setCellValue('B5', 'ALAT TANGKAP');

                            $styleArray = array('font' => array('bold' => true),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                            $sheet->getStyle('B5')->applyFromArray($styleArray);
                            //Bold huruf
                            $sheet->getStyle('B5')->applyFromArray($styleArray2);
                            
                      
    // DATA 3
    $sheet      ->setCellValue('C5', 'UNIT');

                            $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                            $sheet->getStyle('C5:C55')->applyFromArray($styleArray);
                            //Bold huruf
                            $sheet->getStyle('C5')->applyFromArray($styleArray2);
                            
    // MASUKKAN DATA
    $cell_number = 6;
    $index_num = 1;

                foreach ($jumlah_kapal as $item) {
                  $sheet->setCellValue('A'.$cell_number, $index_num.'. ');
                  $sheet->setCellValue('B'.$cell_number, $item->nama_alat_tangkap);
                  $sheet->setCellValue('C'.$cell_number, $item->jumlah_kapal); 
                  $cell_number++;
                  $index_num++;             
                }



                            

    //MENGATUR UKURAN KOLOM
    $sheet->getColumnDimension('A')->setWidth(5);
    $sheet->getColumnDimension('B')->setWidth(82);
    $sheet->getColumnDimension('C')->setWidth(15);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Alat-Tangkap.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN
        )
      )
    );

    $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }

}

/* End of file mst_wpp.php */
/* Location: ./application/modules/mst_wpp/controllers*/