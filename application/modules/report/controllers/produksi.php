<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produksi extends MX_Controller {

  /**
   * Nama File: 
   * Author: Frendhi
   * Deskripsi: 
   * 
   */
    private $list_report = array();
    function __construct()
    {
      parent::__construct();
      $this->load->library('entry/mkform');
      //$this->load->config('custom_constants');
      $this->list_report = $this->config->item('list_report');
      $this->load->model('mdl_produksi');

      //TODO: LOGIN CHECK HERE
    }
  private function _filtering()
  {
    $data['is_admin'] = FALSE;

    // $id_pelabuhan = $this->input->get('id_pelabuhan');
    if( !$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') )
    {
        $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
        $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
        $data['is_admin'] = TRUE;
    }
    $input_filter = $this->input->post(NULL, TRUE);

    $data['pre_filter'] = FALSE;
    if(empty($input_filter) )
    {
      $array_filter = array( 'id_pelabuhan' => $id_pelabuhan,
                              'bulan' => date("m", time()), 
                              'tahun' => date("Y", time()), 
                              'filters' => FALSE
                           );
    }else{
      $array_filter['id_pelabuhan'] = $id_pelabuhan;
      $array_filter['filters'] = $input_filter;
      $data['pre_filters'] = $input_filter;
    }

    $data_nilai_produksi = $this->mdl_produksi->nilai_produksi($array_filter);
    $data_nilai_produksi_luar_pelabuhan = $this->mdl_produksi->nilai_produksi_luar_pelabuhan($array_filter);
    $data['data_nilai_produksi'] = $data_nilai_produksi;
    $data['data_nilai_produksi_luar'] = $data_nilai_produksi_luar_pelabuhan;
    $data['id_pelabuhan'] = $id_pelabuhan;

    return $data;
  }

  public function index()
  {
    $data = $this->_filtering();

    $data['module'] = 'report';
    $data['view_file'] = 'v_nilai_produksi';
    $data['page_title'] = 'LAPORAN PRODUKSI DAN NILAI';
    $data['content_title'] = 'LAPORAN PERKEMBANGAN PRODUKSI DAN NILAI PRODUKSI IKAN';
    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js','d3.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
    $data['breadcrumbs'] = 'Laporan > Perkembangan Produksi dan Nilai Produksi Ikan';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);

    $data['list_report'] = $this->list_report;
    echo Modules::run('templates/type/reporting', $data);
  }

  

  public function xls()
  {
    $data = $this->_filtering();
  
    $data_nilai_produksi = $data['data_nilai_produksi'];
    $data_nilai_produksi_luar_pelabuhan = $data['data_nilai_produksi_luar'];

    

    $arr_pelabuhan = array();
    if(empty($data['pre_filters']['id_pelabuhan_selects']))
    {
      $temp_arr_pelabuhan['id_pelabuhan'] = $data['id_pelabuhan'];
      $temp_arr_pelabuhan['nama_pelabuhan'] = $nama_pelabuhan = $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);
      array_push($arr_pelabuhan, $temp_arr_pelabuhan);
    }else{
        foreach ($data['pre_filters']['id_pelabuhan_selects'] as $id_plb) {
          $temp_arr_pelabuhan['id_pelabuhan'] = $id_plb;
          $temp_arr_pelabuhan['nama_pelabuhan'] = $nama_pelabuhan = $this->pipp_lib->str_nama_pelabuhan($id_plb);
          array_push($arr_pelabuhan, $temp_arr_pelabuhan);
        }
    }


    $arr_data_pelabuhan = array();

    foreach ($arr_pelabuhan as $index_plb) {
      $tmp_arr_data_pelabuhan = array('id_pelabuhan' => $index_plb['id_pelabuhan'],
                                      'nama_pelabuhan' => $index_plb['nama_pelabuhan'],
                                      'data_nilai_produksi' => NULL,
                                      'data_nilai_produksi_luar_pelabuhan' => NULL
                                    );
      $tmp_data_nilai_produksi = array();
        if($data_nilai_produksi)
        {
          foreach ($data_nilai_produksi as $item_1) {
            if($item_1->id_pelabuhan === $index_plb['id_pelabuhan'])
            {
              array_push($tmp_data_nilai_produksi,$item_1);
            }
          }
        }
      $tmp_data_nilai_produksi_luar = array();
        if($data_nilai_produksi_luar_pelabuhan)
        {
          foreach ($data_nilai_produksi_luar_pelabuhan as $item_2) {
            if($item_2->id_pelabuhan === $index_plb['id_pelabuhan'])
            {
              array_push($tmp_data_nilai_produksi_luar, $item_2);

            }
          }
        }
      $tmp_arr_data_pelabuhan['data_nilai_produksi_luar_pelabuhan'] = $tmp_data_nilai_produksi_luar;
      $tmp_arr_data_pelabuhan['data_nilai_produksi'] = $tmp_data_nilai_produksi;

      array_push($arr_data_pelabuhan, $tmp_arr_data_pelabuhan);
      // var_dump($tmp_arr_data_pelabuhan['data_nilai_produksi']);
      
      // echo "BLOCK<hr><hr>";
    }


    
    //load our new PHPExcel library
    $this->load->library('excel');
              // HEADER DLL
            // $this->excel->getActiveSheet()->getColumnDimension("A")->set_width("1");
            // $this->excel->getActiveSheet()->setCellValue('G3' ,'Bulan :')
            //                               ->setCellValue('H3' ,bulan_ind($bulan) )
            //                               ->setCellValue('G4' ,'Tahun :')
            //                               ->setCellValue('H4' ,$tahun);

    foreach ($arr_data_pelabuhan as $index_plb => $items) {

                $this->excel->setActiveSheetIndex($index_plb);
                //name the worksheet

                $this->excel->getActiveSheet()->setTitle($items['nama_pelabuhan']);

                $this->excel->getActiveSheet()->setCellValue('A7' ,'Dari Kapal')
                                          ->setCellValue('A9' ,'No.')
                                          ->setCellValue('B9' ,'Jenis Ikan')
                                          ->setCellValue('C9','Volume')
                                          ->setCellValue('D9','Nilai Produksi')
                                          ->setCellValue('E9','Harga Rata-Rata Produsen')
                                          ->setCellValue('F9','Harga Rata-Rata Pedagang');

                $this->excel->getActiveSheet()->setCellValue('I7' ,'Dari Luar Pelabuhan')
                                          ->setCellValue('I9' ,'No.')
                                          ->setCellValue('J9' ,'Jenis Ikan')
                                          ->setCellValue('K9','Volume')
                                          ->setCellValue('L9','Nilai Produksi')
                                          ->setCellValue('M9','Harga Rata-Rata');
        $prod_kapal_last_index = 11;                                   
        if($items['data_nilai_produksi'])
        {
            $index_number = 10;
            $number = 1;

              foreach ($items['data_nilai_produksi'] as $item) {
                      // var_dump($data_item);
                      // echo "<hr>";
                      $this->excel->getActiveSheet()->setCellValue('A'.$index_number , $number.'.');
                      $this->excel->getActiveSheet()->setCellValue('B'.$index_number ,$item->nama_jenis_ikan.'('.$item->nama_jenis_ikan_inggris.')');
                      $this->excel->getActiveSheet()->setCellValue('C'.$index_number ,$item->volume );
                      $this->excel->getActiveSheet()->setCellValue('D'.$index_number ,$item->nilai_produksi );
                      $this->excel->getActiveSheet()->setCellValue('E'.$index_number ,$item->harga_rata_produsen );
                      $this->excel->getActiveSheet()->setCellValue('F'.$index_number ,$item->harga_rata_pedagang );
                    $index_number++;
                    $number++;
              }
          $prod_kapal_last_index = $index_number+1;    
        }
        $prod_luar_last_index = 11;
        if($items['data_nilai_produksi_luar_pelabuhan'])
        {
            $index_number = 10;
            $number = 1;

              foreach ($items['data_nilai_produksi_luar_pelabuhan'] as $item) {
                      $this->excel->getActiveSheet()->setCellValue('I'.$index_number , $number.'.');
                      $this->excel->getActiveSheet()->setCellValue('J'.$index_number ,$item->nama_jenis_ikan.'('.$item->nama_jenis_ikan_inggris.')');
                      $this->excel->getActiveSheet()->setCellValue('K'.$index_number ,$item->volume );
                      $this->excel->getActiveSheet()->setCellValue('L'.$index_number ,$item->nilai_produksi );
                      $this->excel->getActiveSheet()->setCellValue('M'.$index_number ,$item->harga_rata );
                    $index_number++;
                    $number++;
              }
            $prod_luar_last_index = $index_number + 1;
        }

        //MENGATUR UKURAN KOLOM
                $sheet = $this->excel->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(30);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(30);
        $sheet->getColumnDimension('G')->setWidth(5);
        $sheet->getColumnDimension('H')->setWidth(5);
        $sheet->getColumnDimension('I')->setWidth(5);
        $sheet->getColumnDimension('J')->setWidth(30);
        $sheet->getColumnDimension('K')->setWidth(20);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(20);
        $sheet->getColumnDimension('O')->setWidth(20);

         $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $this->excel->getActiveSheet('A9:F'.$prod_kapal_last_index)->getStyle('A9:F'.$prod_kapal_last_index)->applyFromArray($styleArray);
        $this->excel->getActiveSheet('I9:M'.$prod_luar_last_index)->getStyle('I9:M'.$prod_luar_last_index)->applyFromArray($styleArray);

        unset($styleArray);
        $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $this->excel->getActiveSheet('A9:F9')->getStyle('A9:F9')->applyFromArray($styleArray);
        $this->excel->getActiveSheet('I9:M9')->getStyle('I9:M9')->applyFromArray($styleArray);
        
        unset($styleArray);
        $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 15));

        $this->excel->getActiveSheet('A7')->getStyle('A7')->applyFromArray($styleArray);
        $this->excel->getActiveSheet('I7')->getStyle('I7')->applyFromArray($styleArray);

        unset($styleArray);

        $this->excel->createSheet();
    }
    // die;
     
    $filename='PERKEMBANGAN PRODUKSI DAN NILAI PRODUKSI IKAN.xls'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache
                 
    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //if you want to save it as .XLSX Excel 2007 format
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
    //force user to download the Excel file without writing it to server's HD
    $objWriter->save('php://output');
  }


}