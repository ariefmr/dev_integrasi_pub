<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class usaha_pengolahan extends MX_Controller {

  /**
   * Controller Mst WPP
   * created by ariefmr
   * at McD
   * 25-09-2013
   * 
   */


  function __construct()
  {
    parent::__construct();
      $this->load->library('entry/mkform');

    //$this->load->config('custom_constants');

    $this->assets_paths = $this->config->item('assets_paths');
    $this->load->model('mdl_usaha_pengolahan');
  }

  private function _filtering()
  {
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
      $data['is_admin'] = FALSE;
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
      $data['is_admin'] = TRUE;
    }

    $input_filter = $this->input->post(NULL, TRUE);
    //BUAT NGAMBIL SEMUA INPUT POST
    
    $data['pre_filter'] = FALSE;

    //bulan dan tahun
    if(empty($input_filter['bulan_tahun']))
    {
      $tgl_catat = array( 'bulan' => date("m", time() ), 
                              'tahun' => date("Y", time() )
                          );
    }
    else {
      $tgl_ex = explode("-", $input_filter['bulan_tahun']);
      $tgl_catat = array( 'tahun' => $tgl_ex[0] , 
                              'bulan' => $tgl_ex[1] 
                          );
    }
    $data['tmp_tgl_catat'] = $tgl_catat['tahun'].'-'.$tgl_catat['bulan'];
    $data['tmp_bulan'] = $tgl_catat['bulan'];
    $data['tmp_tahun'] = $tgl_catat['tahun'];

   $data_usaha_pengolahan = array();
    if(empty($input_filter) )
    {
      $array_filter = array( 'id_pelabuhan' => $id_pelabuhan, 
                              'filters' => FALSE
                           );
      $tmp_arr_data_usaha_pengolahan = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_usaha_pengolahan' => $this->mdl_usaha_pengolahan->usaha_pengolahan($array_filter, $data['tmp_tahun'], $data['tmp_bulan']),
                                        );
      array_push($data_usaha_pengolahan , $tmp_arr_data_usaha_pengolahan);
    }else{
      $array_filter['id_pelabuhan'] = $id_pelabuhan;
      $array_filter['filters'] = $input_filter;
      $data['pre_filter'] = $input_filter;

      if( empty($input_filter['id_pelabuhan_selects']) || count( $input_filter['id_pelabuhan_selects'] ) === 1 )
      {
        $tmp_arr_data_usaha_pengolahan = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_usaha_pengolahan' => $this->mdl_usaha_pengolahan->usaha_pengolahan($array_filter, $data['tmp_tahun'], $data['tmp_bulan']),
                                        );
        array_push($data_usaha_pengolahan , $tmp_arr_data_usaha_pengolahan);
      }else{
        foreach ($input_filter['id_pelabuhan_selects'] as $id_pelabuhan_selects) {
          $array_filter['id_pelabuhan'] = $id_pelabuhan_selects;
          $tmp_arr_data_usaha_pengolahan = array( 'id_pelabuhan' => $id_pelabuhan_selects,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan_selects),
                                       'data_usaha_pengolahan' => $this->mdl_usaha_pengolahan->usaha_pengolahan($array_filter, $data['tmp_tahun'], $data['tmp_bulan']),
                                       );
          array_push($data_usaha_pengolahan , $tmp_arr_data_usaha_pengolahan);
        }
      }
    }
    $data['id_pelabuhan'] = $id_pelabuhan;
     
    $data['data_usaha_pengolahan'] = $data_usaha_pengolahan;

    return $data;
  }

  public function index()
  {
    
    $data = $this->_filtering();

    /* WAJIB ADA <---- */
    
    $data['module'] = 'report';
    $data['view_file'] = 'v_usaha_pengolahan';
    $data['page_title'] = 'LAPORAN USAHA PENGOLAHAN';
    $data['content_title'] = 'DATA USAHA PENGOLAHAN';

    $data['additional_js'] = Array('jquery.dataTables.min.js','select2.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');

    $data['breadcrumbs'] = 'Laporan > DATA USAHA PENGOLAHAN';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);
    /* ---> WAJIB ADA*/

    echo Modules::run('templates/type/reporting', $data);    
  }

  public function xls()
  {
    $data = $this->_filtering();

    $data_usaha_pengolahan = $data['data_usaha_pengolahan'];

    $this->load->library('excel');

    $filename = "DATA UNIT USAHA.xlsx";


    foreach ($data_usaha_pengolahan as $index_number => $parent_item) {
      // JUDUL
          // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $this->excel->setActiveSheetIndex($index_number);
      $this->excel->getActiveSheet()->setTitle($parent_item['nama_pelabuhan']);

      $sheet = $this->excel->getActiveSheet();

      $sheet      -> setCellValue('A1', 'LAPORAN MONITORING OPERASIONAL PELABUHAN PERIKANAN');
                        $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                        $sheet->getStyle('A1')->applyFromArray($styleArray);
                        $sheet->mergeCells('A1:G1');

      // SUBJUDUL
      $sheet      ->setCellValue('A3', '12. USAHA PENGOLAHAN DI PELABUHAN PERIKANAN');
                        $styleArray = array('font' => array('bold' => true));
                        $sheet->getStyle('A3')->applyFromArray($styleArray);
                        $sheet->mergeCells('A3:D3');
      $sheet      ->setCellValue('F3', 'Bulan');
      $sheet      ->setCellValue('G3', ': '.bulan_ind($data['tmp_bulan'] + 1));
      $sheet      ->setCellValue('F4', 'Tahun :');
      $sheet      ->setCellValue('G4', ': '.$data['tmp_tahun']);

      $styleArray = array('font' => array('name' => 'Trebuchet MS','size' => 11));
      $sheet->getStyle('A3:G100')->applyFromArray($styleArray);

      //DATA 1
      $sheet      ->setCellValue('A5', 'NO.');
      $sheet      ->mergeCells('A5:A6');
      //center secara horizontal
      $sheet->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      //center secara vertical
      $sheet->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                              $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                              $sheet->getStyle('A5:A55')->applyFromArray($styleArray);
                              //Bold huruf
                              $styleArray2 = array('font' => array('bold' => true));
                              $sheet->getStyle('A5')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A6')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A11')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A20')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A23')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A26')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A32')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A35')->applyFromArray($styleArray2);
                              // $sheet->getStyle('A42')->applyFromArray($styleArray2);
                              
                             
                              

      // DATA 2 
      $sheet      ->setCellValue('B5', 'JENIS PERBEKALAN');
      $sheet      ->mergeCells('B5:B6');
      //center secara horizontal
      $sheet->getStyle('A5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      //center secara vertical
      $sheet->getStyle('A5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                              $styleArray = array('font' => array('bold' => true),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                              $sheet->getStyle('B5')->applyFromArray($styleArray);
                              //Bold huruf
                              $sheet->getStyle('B5')->applyFromArray($styleArray2);

      $sheet      ->setCellValue('C5', 'Jumlah');
      $sheet      ->mergeCells('C5:D5');
      $sheet      ->setCellValue('C6', 'Volume');
      $sheet      ->setCellValue('D6', 'Nilai');

                              $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                              $sheet->getStyle('C5:G55')->applyFromArray($styleArray);
                              //Bold huruf
                              $sheet->getStyle('C5:G6')->applyFromArray($styleArray2);

      $cell_number = 7;
      if($parent_item['data_usaha_pengolahan'] !== FALSE)
      {                    
        // MASUKKAN DATA
        $data_usaha_pengolahan = $parent_item['data_usaha_pengolahan'];
        $index_num = 1;
        $pengo =0;
        foreach ($data_usaha_pengolahan as $item) {

          $sheet->setCellValue('A'.$cell_number, $index_num);
          $sheet->setCellValue('B'.$cell_number, $item->jenis_usaha);
          $sheet->setCellValue('C'.$cell_number, $item->volume); 
          $sheet->setCellValue('D'.$cell_number, $item->nilai);
          $cell_number++;
          $index_num++;             
        }
      }


                              

      //MENGATUR UKURAN KOLOM
      $sheet->getColumnDimension('A')->setWidth(5);
      $sheet->getColumnDimension('B')->setWidth(30);
      $sheet->getColumnDimension('C')->setWidth(15);
      $sheet->getColumnDimension('D')->setWidth(15);
      $sheet->getColumnDimension('E')->setWidth(15);
      $sheet->getColumnDimension('F')->setWidth(15);
      $sheet->getColumnDimension('G')->setWidth(15);

      



      //MENGATUR BORDER Table 1

      $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );

      $this->excel->getActiveSheet('A5:D'.$cell_number)->getStyle('A5:D'.$cell_number)->applyFromArray($styleArray);
      unset($styleArray);
      $this->excel->createSheet();
    }

    // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="Unit Usaha.xlsx"');
      header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');

  }

}

/* End of file mst_wpp.php */
/* Location: ./application/modules/mst_wpp/controllers*/