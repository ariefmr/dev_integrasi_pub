<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operasional_kapal extends MX_Controller {

  /**
   * Nama File: 
   * Author: Frendhi
   * Deskripsi: 
   * 
   */
    private $list_report = array();
    private $list_range_gt = array(
                                    0 => array('text' => '< 5GT',
                                                'sql_between' => 'BETWEEN 0 AND 5',
                                                'filter_gt' => 'below'
                                        ),
                                    1 => array('text' => '5 - 10 GT',
                                                'sql_between' => 'BETWEEN 5 AND 10',
                                                'filter_gt' => 'below'
                                        ),
                                    2 => array('text' => '10 - 20 GT',
                                                'sql_between' => 'BETWEEN 10 AND 20',
                                                'filter_gt' => 'below'
                                        ),
                                    3 => array('text' => '20 - 30 GT',
                                                'sql_between' => 'BETWEEN 20 AND 30',
                                                'filter_gt' => 'below'
                                        ),
                                    4 => array('text' => '30 - 50 GT',
                                                'sql_between' => 'BETWEEN 30 AND 50',
                                                'filter_gt' => 'above'
                                        ),
                                    5 => array('text' => '50 - 100 GT',
                                                'sql_between' => 'BETWEEN 50 AND 100',
                                                'filter_gt' => 'above'
                                        ),
                                    6 => array('text' => '100 - 200 GT',
                                                'sql_between' => 'BETWEEN 100 AND 200',
                                                'filter_gt' => 'above'
                                        ),
                                    7 => array('text' => '200 - 300 GT',
                                                'sql_between' => 'BETWEEN 200 AND 300',
                                                'filter_gt' => 'above'
                                        ),
                                    8 => array('text' => '300 - 500 GT',
                                                'sql_between' => 'BETWEEN 300 AND 500',
                                                'filter_gt' => 'above'
                                        ),
                                    9 => array('text' => '500 - 1000 GT',
                                                'sql_between' => 'BETWEEN 500 AND 1000',
                                                'filter_gt' => 'above'
                                        ),
                                    10 => array('text' => '> 1000 GT',
                                                'sql_between' => '> 1000',
                                                'filter_gt' => 'above'
                                        )
                                  );
    function __construct()
    {
      parent::__construct();
      $this->load->library('entry/mkform');
      //$this->load->config('custom_constants');
      $this->list_report = $this->config->item('list_report');
      $this->load->model('mdl_operasional_kapal');

      //TODO: LOGIN CHECK HERE
    }
    
  private function _filtering()
  {
      $data['is_admin'] = FALSE;
    // $id_pelabuhan = $this->input->get('id_pelabuhan');
    if(!$this->pipp_lib->info_is_admin('is_super_admin') && !$this->pipp_lib->info_is_admin('is_admin_session') ){
      $id_pelabuhan = $this->pipp_lib->id_pelabuhan_pengguna();
    }else{
      $id_pelabuhan = $this->pipp_lib->info_admin_pipp('id_pelabuhan_temp');
      $data['is_admin'] = TRUE;

    }
    $input_filter = $this->input->post(NULL, TRUE);

    $data['pre_filter'] = FALSE;
    $data_operasional_kapal = array();
    if(empty($input_filter) )
    {
      $array_filter = array( 'id_pelabuhan' => $id_pelabuhan,
                              'bulan' => date("m", time()), 
                              'tahun' => date("Y", time()), 
                              'filters' => FALSE
                           );
      $tmp_arr_data_operasional = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_kunjungan' => $this->mdl_operasional_kapal->operasional_kapal($array_filter, $this->list_range_gt),
                                       'data_bongkar' => $this->mdl_operasional_kapal->kapal_bongkar($array_filter, $this->list_range_gt)
                                       );
      array_push($data_operasional_kapal , $tmp_arr_data_operasional);
    }else{
      $array_filter['id_pelabuhan'] = $id_pelabuhan;
      $array_filter['filters'] = $input_filter;
      $data['pre_filter'] = $input_filter;

      if( empty($input_filter['id_pelabuhan_selects']) || count( $input_filter['id_pelabuhan_selects'] ) === 1 )
      {
        $tmp_arr_data_operasional = array( 'id_pelabuhan' => $id_pelabuhan,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan),
                                       'data_kunjungan' => $this->mdl_operasional_kapal->operasional_kapal($array_filter, $this->list_range_gt),
                                       'data_bongkar' => $this->mdl_operasional_kapal->kapal_bongkar($array_filter, $this->list_range_gt)
                                        );
        array_push($data_operasional_kapal , $tmp_arr_data_operasional);
      }else{
        foreach ($input_filter['id_pelabuhan_selects'] as $id_pelabuhan_selects) {
          $array_filter['id_pelabuhan'] = $id_pelabuhan_selects;
          $tmp_arr_data_operasional = array( 'id_pelabuhan' => $id_pelabuhan_selects,
                                       'nama_pelabuhan' => $this->pipp_lib->str_nama_pelabuhan($id_pelabuhan_selects),
                                       'data_kunjungan' => $this->mdl_operasional_kapal->operasional_kapal($array_filter, $this->list_range_gt),
                                       'data_bongkar' => $this->mdl_operasional_kapal->kapal_bongkar($array_filter, $this->list_range_gt)
                                       );
          array_push($data_operasional_kapal , $tmp_arr_data_operasional);
        }
      }
    }
    $data['id_pelabuhan'] = $id_pelabuhan;
     
    $data['data_operasional_kapal'] = $data_operasional_kapal;

    return $data;
  }

  public function index()
  {
    $data = $this->_filtering();
    
    // var_dump($data_operasional_kapal[0]);
    // die;
    $data['module'] = 'report';
    $data['view_file'] = 'v_operasional_kapal';
    $data['page_title'] = 'LAPORAN DATA JUMLAH KAPAL';
    $data['content_title'] = 'DATA JUMLAH KAPAL';
    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js','select2.min.js','d3.min.js');
    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
    $data['breadcrumbs'] = 'Laporan > DATA JUMLAH KAPAL';
    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan($data['id_pelabuhan']);

    $data['list_report'] = $this->list_report;
    echo Modules::run('templates/type/reporting', $data);
  }

  
  public function xls()
  {
    $data = $this->_filtering();

    $data_operasional_kapal = $data['data_operasional_kapal'];


    $this->load->library('excel');
    $objPHPExcel = $this->excel;
    $filename = "DATA JUMLAH KAPAL.xlsx";
        // Set document properties
      // $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
      //                ->setLastModifiedBy("Maarten Balliauw")
      //                ->setTitle("Office 2007 XLSX Test Document")
      //                ->setSubject("Office 2007 XLSX Test Document")
      //                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
      //                ->setKeywords("office 2007 openxml php")
      //                ->setCategory("LAPORAN");

      foreach ($data_operasional_kapal as $index_number => $parent_item) {
            // JUDUL
            $objPHPExcel->setActiveSheetIndex($index_number);
            $objPHPExcel->getActiveSheet()->setTitle($parent_item['nama_pelabuhan']);

            $sheet = $objPHPExcel->getActiveSheet();
            $sheet      -> setCellValue('A1', 'LAPORAN MONITORING OPERASIOAL PELABUHAN');
                              $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                              $sheet->getStyle('A1')->applyFromArray($styleArray);
                              $sheet->mergeCells('A1:E1');

            // SUBJUDUL
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet      ->setCellValue('A3', '05. DATA JUMLAH KAPAL');
                              $styleArray = array('font' => array('bold' => true));
                              $sheet->getStyle('A3')->applyFromArray($styleArray);
                              $sheet->mergeCells('A3:B3');

            //DATA 1
            $sheet = $objPHPExcel->getActiveSheet();
                              $styleArray = array('font' => array('name' => 'Trebuchet MS','size' => 11));
                              $sheet->getStyle('A1:E100')->applyFromArray($styleArray);
            $sheet      ->setCellValue('A7', 'NO.');
            $sheet      ->setCellValue('B7', 'KATEGORI KAPAL PENANGKAP IKAN');
            $sheet      ->setCellValue('C7', 'FREKUENSI KUNJUNGAN KAPAL (KALI)');
            $sheet      ->setCellValue('D7', 'MENDARATKAN HASIL TANGKAPAN (KALI)');
            $sheet      ->setCellValue('B8', 'Perahu Papan (Perahu Tanpa Motor)');
            $sheet      ->setCellValue('B10', 'Motor Tempel');
            $sheet      ->setCellValue('B12', 'Kapal Motor');
            $sheet      ->setCellValue('B24', 'Sub Jumlah');
            $sheet      ->setCellValue('B25', 'JUMLAH TOTAL');
                              $styleArray = array('font' => array('bold' => true),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
                              $sheet->getStyle('A7')->applyFromArray($styleArray);
                              $sheet->getStyle('B7')->applyFromArray($styleArray);
                              $sheet->getStyle('C7')->applyFromArray($styleArray);
                              $sheet->getStyle('D7')->applyFromArray($styleArray);
                              $sheet->getStyle('A8')->applyFromArray($styleArray);
                              $sheet->getStyle('A10')->applyFromArray($styleArray);
                              $sheet->getStyle('A12')->applyFromArray($styleArray);
                              $sheet->getStyle('B8')->applyFromArray($styleArray);
                              $sheet->getStyle('B10')->applyFromArray($styleArray);
                              $sheet->getStyle('B12')->applyFromArray($styleArray);
                              $sheet->getStyle('B24')->applyFromArray($styleArray);
                              $sheet->getStyle('B25')->applyFromArray($styleArray);

                              $sheet->getStyle('A8:B8')->getFill()->applyFromArray(
                              array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'E1E0F7'))
                              );
                              $sheet->getStyle('A10:B10')->getFill()->applyFromArray(
                              array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'E1E0F7'))
                              );
                              $sheet->getStyle('A12:B12')->getFill()->applyFromArray(
                              array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'E1E0F7'))
                              );
                              $sheet->getStyle('B25')->getFill()->applyFromArray(
                              array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'E1E0F7'))
                              );

            // DATA 2
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet      ->setCellValue('A8', 'I.');
            $sheet      ->setCellValue('A10', 'II.');
            $sheet      ->setCellValue('A12', 'III.');
            $sheet      ->setCellValue('A13', '1');
            $sheet      ->setCellValue('A14', '2');
            $sheet      ->setCellValue('A15', '3');
            $sheet      ->setCellValue('A16', '4');
            $sheet      ->setCellValue('A17', '5');
            $sheet      ->setCellValue('A18', '6');
            $sheet      ->setCellValue('A19', '7');
            $sheet      ->setCellValue('A20', '8');
            $sheet      ->setCellValue('A21', '9');
            $sheet      ->setCellValue('A22', '10');
            $sheet      ->setCellValue('A23', '11');
                              $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                              $sheet->getStyle('A8:A23')->applyFromArray($styleArray);

            // DATA 3
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet      ->setCellValue('B13', '< 5 GT');
            $sheet      ->setCellValue('B14', '5 - 10 GT');
            $sheet      ->setCellValue('B15', '10 - 20 GT');
            $sheet      ->setCellValue('B16', '20 - 30 GT');
            $sheet      ->setCellValue('B17', '30 - 50 GT');
            $sheet      ->setCellValue('B18', '50 - 100 GT');
            $sheet      ->setCellValue('B19', '100 - 200 GT');
            $sheet      ->setCellValue('B20', '200 - 300 GT');
            $sheet      ->setCellValue('B21', '300 - 500 GT');
            $sheet      ->setCellValue('B22', '500 - 1000 GT');
            $sheet      ->setCellValue('B23', '> 1000 GT');
            $sheet      ->setCellValue('E4', 'Bulan   :');
            $sheet      ->setCellValue('E5', 'Tahun   :');

            // // Insert data hasil query
            if($parent_item['data_kunjungan'] !== FALSE)
            {
              $data_result_kunjungan = $parent_item['data_kunjungan'];
              $data_result_bongkar = $parent_item['data_bongkar'];
              $number_cell = 13;
              $tmp_total_kunjungan = 0;
              $tmp_total_bongkar = 0;

              foreach ($data_result_kunjungan as $key_index => $item) {
                $tmp_total_kunjungan = $tmp_total_kunjungan + $item->frekuensi_kunjungan;
                $tmp_total_bongkar = $tmp_total_bongkar + $data_result_bongkar[$key_index]->frekuensi_bongkar;
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$number_cell, $item->frekuensi_kunjungan );
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$number_cell, $data_result_bongkar[$key_index]->frekuensi_bongkar );
                $number_cell++;
              }
            }

            //MENGATUR UKURAN KOLOM
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->getColumnDimension('A')->setWidth(8);
            $sheet->getColumnDimension('B')->setWidth(40);
            $sheet->getColumnDimension('C')->setWidth(45);
            $sheet->getColumnDimension('D')->setWidth(45);
            $sheet->getColumnDimension('E')->setWidth(45);



            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            

            //MENGATUR BORDER
            $objPHPExcel->getActiveSheet()->setCellValue('A7','NO');
            $objPHPExcel->getActiveSheet()->setCellValue('B7','KATEGORI KAPAL PENANGKAP IKAN');
            $objPHPExcel->getActiveSheet()->setCellValue('C7','FREKUENSI KUNJUNGAN KAPAL (KALI)');
            $objPHPExcel->getActiveSheet()->setCellValue('D7','MENDARATKAN HASIL TANGKAPAN (KALI)');
            $objPHPExcel->getActiveSheet()->setCellValue('A8','I');
            $objPHPExcel->getActiveSheet()->setCellValue('A10','II');
            $objPHPExcel->getActiveSheet()->setCellValue('A12','III');
            $objPHPExcel->getActiveSheet()->setCellValue('B8','Perahu Papan (Perahu Tanpa Motor)');
            $objPHPExcel->getActiveSheet()->setCellValue('B10','Motor Tempel');
            $objPHPExcel->getActiveSheet()->setCellValue('B12','Kapal Motor');
            $objPHPExcel->getActiveSheet()->setCellValue('B24','Sub Jumlah');
            $objPHPExcel->getActiveSheet()->setCellValue('B25','JUMLAH TOTAL');
            $objPHPExcel->getActiveSheet()->setCellValue('A13', '1');
            $objPHPExcel->getActiveSheet()->setCellValue('A14', '2');
            $objPHPExcel->getActiveSheet()->setCellValue('A15', '3');
            $objPHPExcel->getActiveSheet()->setCellValue('A16', '4');
            $objPHPExcel->getActiveSheet()->setCellValue('A17', '5');
            $objPHPExcel->getActiveSheet()->setCellValue('A18', '6');
            $objPHPExcel->getActiveSheet()->setCellValue('A19', '7');
            $objPHPExcel->getActiveSheet()->setCellValue('A20', '8');
            $objPHPExcel->getActiveSheet()->setCellValue('A21', '9');
            $objPHPExcel->getActiveSheet()->setCellValue('A22', '10');
            $objPHPExcel->getActiveSheet()->setCellValue('A23', '11');
            $objPHPExcel->getActiveSheet()->setCellValue('B13', '< 5 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B14', '5 - 10 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B15', '10 - 20 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B16', '20 - 30 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B17', '30 - 50 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B18', '50 - 100 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B19', '100 - 200 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B20', '200 - 300 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B21', '300 - 500 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B22', '500 - 1000 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('B23', '> 1000 GT');
            $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Bulan   :');
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tahun   :');

            $styleArray = array(
              'borders' => array(
                'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A7:D25')->applyFromArray($styleArray);
            unset($styleArray);
            $objPHPExcel->createSheet();
      }   
                        // Redirect output to a client’s web browser (Excel2007)

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');

      exit;

  }


}