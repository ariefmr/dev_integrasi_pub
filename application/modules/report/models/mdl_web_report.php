<?php
/*
 * class model_wpp
 */

class Mdl_web_report extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }
    
    public function summary_produksi_harian()
    {
        // $sql = "SELECT * FROM summary_produksi_harian WHERE run_time = (SELECT MAX(run_time) FROM summary_produksi_harian) ";

        // $query = $this->db->query($sql);
        
        // if($query->num_rows() > 0){
        //     $result = $query->result();
        // }else{
        //     $result = false;
        // }
        // return $result;
    }

    public function summary_produksi_tahunan()
    {
        // $query = $this->db->get('summary_produksi_tahunan');
        
        // if($query->num_rows() > 0){
        //     $result = $query->row();
        // }else{
        //     $result = false;
        // }
        
        // return $result;
    }

    public function summary_aktivitas_kapal($aktivitas)
    {   
        // $array_bulan = $this->pipp_element->nama_bulan;
        // $tabel_aktivitas = $aktivitas === "masuk" ? "kapal_masuk" : "kapal_keluar";
        // $field_tgl = $aktivitas === "masuk" ? "tgl_masuk" : "tgl_keluar";
        // $last_month = end(array_keys($array_bulan));
        // $current_year = date("Y");
        // $sql = "SELECT ";
        //     foreach($array_bulan as $index => $nama_bulan)
        //     {
        //         if ($nama_bulan !== reset($array_bulan))
        //         {
        //             $sql .= '(SELECT COUNT(*) FROM '.$tabel_aktivitas.' 
        //                         WHERE EXTRACT(month FROM "'.$field_tgl.'") = '.$index.' 
        //                             AND EXTRACT(year FROM "'.$field_tgl.'") = EXTRACT(year FROM current_date)) AS '.$nama_bulan.' ';
        //             if($index !== $last_month){
        //                 $sql .= ',';
        //             }
        //         }
                
        //     }
        
        // $query = $this->db->query($sql);
        
        // if($query->num_rows() > 0){
        //     $result = $query->row();
        // }else{
        //     $result = false;
        // }
        // return $result;
    }

    public function summary_produksi_pelabuhan_bulanan()
    {
        // SQL dengan pelabuhan produksi kosong
        // $sql = 'SELECT IFNULL(sum_pr.sub_jml_ikan,0) AS jumlah, plb.nama_pelabuhan, plb.id_pelabuhan FROM db_master.mst_pelabuhan plb
        //             LEFT OUTER JOIN 
        //             (SELECT id_pelabuhan,SUM(pr.jml_ikan) sub_jml_ikan 
        //                 FROM db_pipp.trs_produksi pr 
        //                 WHERE YEAR(pr.tgl_catat) = YEAR(CURDATE()) AND MONTH(pr.tgl_catat) = MONTH(CURDATE())
        //                 GROUP BY id_pelabuhan) sum_pr
        //             ON plb.id_pelabuhan = sum_pr.id_pelabuhan
        //             WHERE plb.aktif = "Ya"
        //             GROUP BY plb.id_pelabuhan
        //             ORDER BY jumlah DESC';

        $sql = 'SELECT 
                    pr.id_pelabuhan,
                    mpb.nama_pelabuhan,
                    SUM( ( ((pr.koefisien_koreksi / 100)* pr.jml_ikan) + pr.jml_ikan) ) AS jumlah,
                    mpb.aktif
                FROM
                    db_pipp.trs_produksi pr
                        LEFT JOIN
                    db_master.mst_pelabuhan mpb ON mpb.id_pelabuhan = pr.id_pelabuhan
                WHERE
                    YEAR(pr.tgl_aktivitas) = YEAR(CURDATE())
                AND MONTH(pr.tgl_aktivitas) = MONTH(CURDATE())
                AND pr.aktif = "Ya"
                GROUP BY id_pelabuhan
                ORDER BY jumlah DESC';
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function summary_produksi_pelabuhan_perbulan($id_pelabuhan)
    {
        // $first_date_month = date("Y-m-1", strtotime("now") );        
        // $last_date_month = date("Y-m-t", strtotime("now") );
        // // $first_date_month = "2012-11-1";
        // // $last_date_month = "2012-11-30";


        // $sql = "SELECT listtgl.itgl as tanggal, pr.jumlah
        //             FROM (SELECT tgl,SUM(jml_ikan) AS jumlah 
        //                         FROM produksi WHERE id_pelabuhan = '".$id_pelabuhan."'
        //                         GROUP BY produksi.tgl) pr
        //             RIGHT JOIN (select i::date as itgl from generate_series('".$first_date_month."', 
        //                                                  '".$last_date_month."', '1 day'::interval) i) listtgl
        //                                                  ON pr.tgl = listtgl.itgl
        //             GROUP BY listtgl.itgl, pr.jumlah
        //             ORDER BY listtgl.itgl ASC";

        // $query = $this->db->query($sql);
        
        // if($query->num_rows() > 0){
        //     $result = $query->result();
        // }else{
        //     $result = false;
        // }
        // return $result;
    }

}
?>