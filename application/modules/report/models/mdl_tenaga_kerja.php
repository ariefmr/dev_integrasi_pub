<?php
/*
 * class Mdl_H3
 */

class Mdl_tenaga_kerja extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function non_nelayan($array_fileter, $date_awal, $date_akhir)
    {
        $this->db_pipp->select(' UPPER(profesi) AS profesi,
                                COUNT(*) AS "jumlah"');
        $this->db_pipp->from('mst_masy_perikanan');
        $this->db_pipp->where('aktif', 'Ya');
        $this->db_pipp->where('id_pelabuhan', $array_fileter['id_pelabuhan']);
        // $this->db_pipp->where('tgl_catat >=', $date_awal);
        // $this->db_pipp->where('tgl_catat <=', $date_akhir);
        $this->db_pipp->group_by('UPPER(profesi)');

       $query = $this->db_pipp->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    function nelayan_abk($array_fileter, $date_awal, $date_akhir)
    {
        $this->db_pipp->select('SUM(jumlah_abk) / COUNT(DISTINCT(id_kapal)) as "jum_abk"
            ');
        $this->db_pipp->from('trs_aktivitas_kapal tak');
        $this->db_pipp->where('tak.id_pelabuhan', $array_fileter['id_pelabuhan']);
        $this->db_pipp->where('tak.aktivitas', 'masuk');
        $this->db_pipp->where('tak.aktif', 'Ya');
        // $this->db_pipp->where('tak.tgl_catat >=', $date_awal);
        // $this->db_pipp->where('tak.tgl_catat <=', $date_akhir);
        $this->db_pipp->group_by('aktivitas');

        $query = $this->db_pipp->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}