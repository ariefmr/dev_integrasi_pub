<?php
/*
 * class Mdl_H3
 */

class Mdl_Alat_tangkap extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function alat_tangkap($array_filter = '')
    {   

        $id_pelabuhan = $array_filter['id_pelabuhan'];
            if($array_filter['filters'] === FALSE)
                 {
                    $bulan = $array_filter['bulan'];
                    $tahun = $array_filter['tahun'];
                 }else {
                    $tanggal_awal = $array_filter['filters']['tgl_awal'];
                    $tanggal_akhir = $array_filter['filters']['tgl_akhir'];
                 }

        $build_sql = "SELECT alt.nama_alat_tangkap, 
                                COUNT(alt.nama_alat_tangkap) AS jumlah_kapal
                            FROM db_pipp.trs_aktivitas_kapal akp
                            LEFT JOIN db_master.mst_kapal kpl
                                ON akp.id_kapal = kpl.id_kapal
                            LEFT JOIN db_master.mst_alat_tangkap alt
                                ON kpl.id_alat_tangkap = alt.id_alat_tangkap
                        WHERE akp.filter_gt = 'above'
                            AND akp.aktivitas = 'masuk'
                            AND akp.id_pelabuhan = $id_pelabuhan";
                            
        if($array_filter['filters'] === FALSE)
        {
            $build_sql .= " AND YEAR(akp.tgl_aktivitas) = $tahun
                                    AND MONTH(akp.tgl_aktivitas) = $bulan ";
        }else{
            $build_sql .= " AND akp.tgl_aktivitas BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ";          
        }               

        $build_sql .= " AND akp.aktif = 'Ya'
                            GROUP BY alt.nama_alat_tangkap
                            ORDER BY alt.nama_alat_tangkap ASC";

        // var_dump($build_sql);
        // die;

        $query_alat_tangkap = $this->db->query($build_sql);                                
        

        if($query_alat_tangkap->num_rows() > 0){
            $result = $query_alat_tangkap->result();
        }else{
            $result = false;
        }
        return $result;
    } 
}