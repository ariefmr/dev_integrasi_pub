<?php
/*
 * class Mdl_H3
 */

class Mdl_instansi_lembaga extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function instansi_lembaga($array_filter, $tahun = "", $bulan = "")
    {
        $tahun = empty($tahun) ? date("Y") : $tahun;
        $this->db_pipp->select('mlb.nama_lembaga, mlb.jml_tenaga');
        $this->db_pipp->from('mst_lembaga mlb');
        // $this->db_pipp->join('db_master.mst_pelabuhan mpb', 'mlb.id_pelabuhan = mpb.id_pelabuhan');
        $this->db_pipp->where('id_pelabuhan', $array_filter['id_pelabuhan']);
        $this->db_pipp->where('YEAR(tgl_catat)', $tahun);
        $this->db_pipp->where('aktif', 'Ya');
        $this->db_pipp->order_by('nama_lembaga');

       $query = $this->db_pipp->get();
        // echo $this->db_pipp->last_query();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}