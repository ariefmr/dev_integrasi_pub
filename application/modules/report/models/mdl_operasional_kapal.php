<?php
/*
 * class Mdl_H3
 */

class Mdl_operasional_kapal extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function operasional_kapal($array_filter, $list_range_gt)
    {   
        $sql_operasional_kunjungan = "(SELECT 
                          ('insert_ranges') AS ranges,
                            COUNT(ak.id_aktivitas_kapal) AS frekuensi_kunjungan
                        FROM
                            db_pipp.trs_aktivitas_kapal ak
                                LEFT JOIN
                            db_master.mst_kapal mk ON mk.id_kapal = ak.id_kapal";

        if($array_filter['filters'] === FALSE)
        {
            $id_pelabuhan = $array_filter['id_pelabuhan'];
            $tahun = $array_filter['tahun'];
            $bulan = $array_filter['bulan'];
            $sql_operasional_kunjungan .= "
                            WHERE
                                YEAR(ak.tgl_aktivitas) = $tahun
                                    AND MONTH(ak.tgl_aktivitas) = $bulan
                                    ";
        }else{
          $id_pelabuhan = $array_filter['id_pelabuhan'];
          $filters = $array_filter['filters'];
                $tgl_awal = $filters['tgl_awal'];
                $tgl_akhir = $filters['tgl_akhir'];

                $sql_operasional_kunjungan .= "
                            WHERE
                                (ak.tgl_aktivitas BETWEEN '$tgl_awal' AND '$tgl_akhir') ";

        }
        $sql_operasional_kunjungan .= " AND (mk.gt_kapal insert_between)
                                        AND ak.filter_gt = 'insert_filter_gt'
                                        AND ak.id_pelabuhan = insert_id_pelabuhan
                                        AND ak.aktivitas = 'masuk'
                                        AND ak.aktif = 'Ya') ";
        $arr_query = array();
        foreach ($list_range_gt as $index_number => $ranges) {
            $searches = array('insert_ranges','insert_between','insert_filter_gt');
            $replacements = array($ranges['text'], $ranges['sql_between'],$ranges['filter_gt']);
              if($ranges['filter_gt'] === 'below')
              {
                array_push($searches, 'mst_kapal');
                array_push($searches, 'mk.id_kapal');

                array_push($replacements, 'mst_kapal_daerah');
                array_push($replacements, 'mk.id_kapal_daerah');
              }
            $tmp_build_sql = str_replace($searches, $replacements, $sql_operasional_kunjungan); 

            array_push($arr_query, $tmp_build_sql);
        }
        $build_sql = implode(" UNION ", $arr_query);

        $build_sql = str_replace('insert_id_pelabuhan', $array_filter['id_pelabuhan'], $build_sql); 

        $query_operasional_kunjungan = $this->db->query($build_sql);                                
        

        if($query_operasional_kunjungan->num_rows() > 0){
            $result = $query_operasional_kunjungan->result();
        }else{
            $result = false;
        }
        return $result;
    } 

    public function kapal_bongkar($array_filter, $list_range_gt)
    {   
        $sql_kapal_bongkar = "(SELECT 
                          ('insert_ranges') AS ranges,
                            COUNT(ak.id_aktivitas_kapal) AS frekuensi_bongkar
                        FROM
                            db_pipp.trs_aktivitas_kapal ak
                                LEFT JOIN
                            db_master.mst_kapal mk ON mk.id_kapal = ak.id_kapal
                                LEFT JOIN 
                            db_pipp.trs_maksud_kunjungan mkj ON mkj.id_aktivitas_kapal = ak.id_aktivitas_kapal";

        if($array_filter['filters'] === FALSE)
        {
            $id_pelabuhan = $array_filter['id_pelabuhan'];
            $tahun = $array_filter['tahun'];
            $bulan = $array_filter['bulan'];
            $sql_kapal_bongkar .= "
                            WHERE
                                YEAR(ak.tgl_aktivitas) = $tahun
                                    AND MONTH(ak.tgl_aktivitas) = $bulan
                                    ";
        }else{
          $id_pelabuhan = $array_filter['id_pelabuhan'];
          $filters = $array_filter['filters'];
                $tgl_awal = $filters['tgl_awal'];
                $tgl_akhir = $filters['tgl_akhir'];

                $sql_kapal_bongkar .= "
                            WHERE
                                (ak.tgl_aktivitas BETWEEN '$tgl_awal' AND '$tgl_akhir') ";

        }
        $sql_kapal_bongkar .= " AND (mk.gt_kapal insert_between)
                                        AND ak.filter_gt = 'insert_filter_gt'
                                        AND ak.id_pelabuhan = insert_id_pelabuhan
                                        AND ak.aktivitas = 'masuk'
                                        AND mkj.is_bongkar IS NOT NULL
                                        AND ak.aktif = 'Ya') ";
        $arr_query = array();
        foreach ($list_range_gt as $index_number => $ranges) {
            $searches = array('insert_ranges','insert_between','insert_filter_gt');
            $replacements = array($ranges['text'], $ranges['sql_between'],$ranges['filter_gt']);
              if($ranges['filter_gt'] === 'below')
              {
                array_push($searches, 'mst_kapal');
                array_push($searches, 'mk.id_kapal');

                array_push($replacements, 'mst_kapal_daerah');
                array_push($replacements, 'mk.id_kapal_daerah');
              }
            $tmp_build_sql = str_replace($searches, $replacements, $sql_kapal_bongkar); 

            array_push($arr_query, $tmp_build_sql);
        }
        $build_sql = implode(" UNION ", $arr_query);

        $build_sql = str_replace('insert_id_pelabuhan', $array_filter['id_pelabuhan'], $build_sql); 

        $query_kapal_bongkar = $this->db->query($build_sql);                                
        

        if($query_kapal_bongkar->num_rows() > 0){
            $result = $query_kapal_bongkar->result();
        }else{
            $result = false;
        }
        return $result;
    } 
}