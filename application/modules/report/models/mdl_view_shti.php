<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_view_shti extends MX_Controller {

    /**
     * Controller Mst WPP
     * created by ariefmr
     * at McD
     * 25-09-2013
     * 
     */
    private $db_dss = NULL;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }
    
    public function jenis_shti()
    {
        $sql = "SELECT (SELECT COUNT(*) AS LA FROM vw_shti_terbit WHERE jenis_shti = 'LA') AS LA,
                (SELECT COUNT(*) FROM vw_shti_terbit WHERE jenis_shti = 'LT') AS LT,
                (SELECT COUNT(*) FROM vw_shti_terbit WHERE jenis_shti = 'LTS') AS LTS,
                (SELECT COUNT(*) FROM vw_shti_terbit WHERE jenis_shti = 'LA Import') AS Import";

        $query = $this->db_dss->query($sql);
        if($query->num_rows() > 0){
            $result = $query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function search_shti($no_shti)
    {
        $run_query = $this->db_dss->get_where('vw_shti_terbit', array('no_shti' => $no_shti));

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

}

/* End of file mst_wpp.php */
/* Location: ./application/modules/mst_wpp/controllers*/