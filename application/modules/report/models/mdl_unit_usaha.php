<?php
/*
 * class Mdl_H3
 */

class MDL_Unit_Usaha extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function unit_usaha($array_filter, $tahun = "", $bulan = "")
    {
        $sql = '
                SELECT 
                    CONCAT(mjp.nama_jns_perbekalan,"(",mjp.satuan,")") AS jenis_usaha,
                    industri.perorangan,
                    industri.pt,
                    industri.ud,
                    industri.kud,
                    industri.total
                FROM mst_jenis_perbekalan mjp
                LEFT JOIN (
                        SELECT 
                            tp.id_jns_perbekalan,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "1" THEN mi.id_industri END)) AS perorangan,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "2" THEN mi.id_industri END)) AS pt,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "3" THEN mi.id_industri END)) AS ud,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "4" THEN mi.id_industri END)) AS kud,
                            COUNT(DISTINCT( mi.id_industri)) AS total
                        FROM trs_perbekalan tp
                        INNER JOIN mst_industri mi
                            ON mi.id_industri = tp.id_industri
                                AND mi.aktif = "Ya"
                        WHERE tp.aktif = "Ya"
                            AND tp.id_pelabuhan = '.$array_filter['id_pelabuhan'].'
                        GROUP BY tp.id_jns_perbekalan
                    ) industri
                    ON industri.id_jns_perbekalan = mjp.id_jns_perbekalan
                WHERE mjp.id_jns_perbekalan <> "10"

                UNION ALL

                SELECT 
                    "Pengolahan "AS jenis_usaha,
                    "" AS perorangan,
                    "" AS pt,
                    "" AS ud,
                    "" AS kud,
                    "" AS total

                UNION ALL

                SELECT 
                    CONCAT("- ", mjho.nama_jns_hasil_olah) AS jenis_usaha,
                    industri.perorangan,
                    industri.pt,
                    industri.ud,
                    industri.kud,
                    industri.total
                FROM mst_jenis_hasil_olah mjho
                LEFT JOIN (
                        SELECT
                            moi.id_jns_hasil_olah,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "1" THEN mi.id_industri END)) AS Perorangan,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "2" THEN mi.id_industri END)) AS PT,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "3" THEN mi.id_industri END)) AS UD,
                            COUNT(DISTINCT( CASE mi.id_jns_badan_usaha WHEN "4" THEN mi.id_industri END)) AS KUD,
                            COUNT(DISTINCT( mi.id_industri)) AS Total
                        FROM mst_olah_ikan moi
                        INNER JOIN mst_industri mi
                            ON mi.id_industri = moi.id_industri
                                AND mi.aktif = "Ya"
                        WHERE moi.aktif = "Ya"
                            AND moi.id_pelabuhan = '.$array_filter['id_pelabuhan'].'
                        GROUP BY moi.id_jns_hasil_olah
                    ) industri
                    ON industri.id_jns_hasil_olah = mjho.id_jns_hasil_olah
                WHERE mjho.id_jns_hasil_olah <> "10"

            ';
        $query = $this->db_pipp->query($sql);
        // echo $this->db_pipp->last_query();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }


}