<?php
/*
 * class Mdl_H3
 */

class MDL_Penyalur_Perbekalan extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function penyalur_perbekalan($array_filter, $tahun, $bulan)
    {
        $sql = '
            SELECT
                CONCAT(mjp.nama_jns_perbekalan,"(",mjp.satuan,")") AS jenis_usaha,
                SUM(CASE tp.is_luar_pelabuhan WHEN 1 THEN tp.jml END ) AS luar_volume,
                SUM(CASE tp.is_luar_pelabuhan WHEN 1 THEN tp.harga END ) AS luar_nilai,
                SUM(CASE tp.is_luar_pelabuhan WHEN 2 THEN tp.jml END ) AS dalam_volume,
                SUM(CASE tp.is_luar_pelabuhan WHEN 2 THEN tp.harga END ) AS dalam_nilai,
                SUM(tp.jml) AS total_volume,
                SUM(tp.harga) AS total_nilai
            FROM trs_perbekalan tp
            LEFT JOIN mst_jenis_perbekalan mjp 
                ON mjp.id_jns_perbekalan = tp.id_jns_perbekalan AND mjp.aktif = "Ya"
            WHERE tp.id_pelabuhan = '.$array_filter['id_pelabuhan'].'
                AND YEAR(tp.tgl_catat) = "'.$tahun.'"
                AND MONTH(tp.tgl_catat) = "'.$bulan.'"
                AND tp.aktif = "Ya"
            GROUP BY tp.id_jns_perbekalan

            ';
        $query = $this->db_pipp->query($sql);
        // echo $this->db_pipp->last_query();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}