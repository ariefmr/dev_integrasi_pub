<?php
/*
 * class Mdl_H3
 */

class Mdl_produksi extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function nilai_produksi($array_filter)
    {   
        $sql_produksi = "SELECT pr.id_pelabuhan,
                                mpb.nama_pelabuhan,
                                mji.nama_jenis_ikan,
                                mji.nama_jenis_ikan_inggris,
                                mji.id_jenis_ikan,
                                SUM( ( ((koefisien_koreksi / 100)* jml_ikan) + jml_ikan) ) AS volume,
                                SUM( jml_ikan ) AS volume_no_koef,
                                (SUM(jml_ikan) * AVG(DISTINCT harga_pedagang)) nilai_produksi_no_koef,
                                (SUM( ( ((koefisien_koreksi / 100)* jml_ikan) + jml_ikan) ) * AVG(DISTINCT harga_pedagang)) nilai_produksi,
                                AVG(DISTINCT harga_produsen) AS harga_rata_produsen,
                                AVG(DISTINCT harga_pedagang) AS harga_rata_pedagang,
                                AVG(DISTINCT koefisien_koreksi) AS rata_koefisien_koreksi
                            FROM
                                db_pipp.trs_produksi pr
                                JOIN db_master.mst_jenis_ikan mji
                                    ON mji.id_jenis_ikan = pr.id_jenis_ikan 
                                JOIN db_master.mst_pelabuhan mpb
                                    ON mpb.id_pelabuhan = pr.id_pelabuhan ";

        if($array_filter['filters'] === FALSE)
        {
            $id_pelabuhan = $array_filter['id_pelabuhan'];
            $tahun = $array_filter['tahun'];
            $bulan = $array_filter['bulan'];
            $sql_produksi .= "
                            WHERE
                                YEAR(pr.tgl_aktivitas) = $tahun
                                    AND MONTH(pr.tgl_aktivitas) = $bulan
                                    ";
        }else{
          $id_pelabuhan = $array_filter['id_pelabuhan'];
          $filters = $array_filter['filters'];
                $tgl_awal = $filters['tgl_awal'];
                $tgl_akhir = $filters['tgl_akhir'];

                $sql_produksi .= "
                            WHERE
                                (pr.tgl_aktivitas BETWEEN '$tgl_awal' AND '$tgl_akhir') ";

        }
        if(!empty($filters['id_ikan_selects']))
        {
            $id_ikan = implode(",", $filters['id_ikan_selects']);
            $sql_produksi .= " AND pr.id_jenis_ikan IN (".$id_ikan.") ";
        }
        

        $sql_produksi .= "AND pr.id_pelabuhan = insert_id_pelabuhan";
        $sql_produksi .= "  AND pr.aktif = 'Ya'
                            GROUP BY mji.id_jenis_ikan";
        if( empty($filters['id_pelabuhan_selects']) || count( $filters['id_pelabuhan_selects'] ) === 1 )
        {   
            if( empty($filters['id_pelabuhan_selects']) ){
                $build_sql = str_replace('insert_id_pelabuhan', $array_filter['id_pelabuhan'], $sql_produksi); 
            }else{
                $build_sql = str_replace('insert_id_pelabuhan', $filters['id_pelabuhan_selects'][0], $sql_produksi); 
            }
        }else{
            $arr_query = array();
            foreach ($filters['id_pelabuhan_selects'] as $id_pelabuhan) {
                $tmp_build_sql = str_replace('insert_id_pelabuhan', $id_pelabuhan, $sql_produksi); 
                array_push($arr_query, $tmp_build_sql);
            }
            $build_sql = implode(" UNION ", $arr_query);
        }
        $build_sql .= " ORDER BY nama_pelabuhan ASC";
        // var_dump($build_sql);
        // die;
        $query_produksi = $this->db->query($build_sql);                                
        

        if($query_produksi->num_rows() > 0){
            $result = $query_produksi->result();
        }else{
            $result = false;
        }
        return $result;
    } 

      public function nilai_produksi_luar_pelabuhan($array_filter)
    {   
        $sql_produksi = "SELECT pr.id_pelabuhan,
                                mpb.nama_pelabuhan,
                                mji.nama_jenis_ikan,
                                mji.nama_jenis_ikan_inggris,
                                mji.id_jenis_ikan,
                                SUM( jml_ikan ) AS volume,
                                (SUM(jml_ikan) * AVG(DISTINCT harga_ikan)) nilai_produksi,
                                AVG(DISTINCT harga_ikan) AS harga_rata
                            FROM
                                db_pipp.trs_pemasaran_masuk pr
                                JOIN db_master.mst_jenis_ikan mji
                                    ON mji.id_jenis_ikan = pr.id_jenis_ikan 
                                JOIN db_master.mst_pelabuhan mpb
                                    ON mpb.id_pelabuhan = pr.id_pelabuhan ";

        if($array_filter['filters'] === FALSE)
        {
            $id_pelabuhan = $array_filter['id_pelabuhan'];
            $tahun = $array_filter['tahun'];
            $bulan = $array_filter['bulan'];
            $sql_produksi .= "
                            WHERE
                                YEAR(pr.tgl_catat) = $tahun
                                    AND MONTH(pr.tgl_catat) = $bulan
                                    ";
        }else{
          $id_pelabuhan = $array_filter['id_pelabuhan'];
          $filters = $array_filter['filters'];
                $tgl_awal = $filters['tgl_awal'];
                $tgl_akhir = $filters['tgl_akhir'];

                $sql_produksi .= "
                            WHERE
                                (pr.tgl_catat BETWEEN '$tgl_awal' AND '$tgl_akhir') ";

        }
        if(!empty($filters['id_ikan_selects']))
        {
            $id_ikan = implode(",", $filters['id_ikan_selects']);
            $sql_produksi .= " AND pr.id_jenis_ikan IN (".$id_ikan.") ";
        }

        $sql_produksi .= "AND pr.id_pelabuhan = insert_id_pelabuhan";
        $sql_produksi .= "  AND pr.aktif = 'Ya'
                            GROUP BY mji.id_jenis_ikan";

        if( empty($filters['id_pelabuhan_selects']) || count( $filters['id_pelabuhan_selects'] ) === 1 )
        {   
            if( empty($filters['id_pelabuhan_selects']) ){
                $build_sql = str_replace('insert_id_pelabuhan', $array_filter['id_pelabuhan'], $sql_produksi); 
            }else{
                $build_sql = str_replace('insert_id_pelabuhan', $filters['id_pelabuhan_selects'][0], $sql_produksi); 
            }
        }else{
            $arr_query = array();
            foreach ($filters['id_pelabuhan_selects'] as $id_pelabuhan) {
                $tmp_build_sql = str_replace('insert_id_pelabuhan', $id_pelabuhan, $sql_produksi); 
                array_push($arr_query, $tmp_build_sql);
            }
            $build_sql = implode(" UNION ", $arr_query);
        }
        $build_sql .= " ORDER BY nama_pelabuhan ASC";
        // var_dump($build_sql);
        // die;
        $query_produksi = $this->db->query($build_sql);                                                              
        

        if($query_produksi->num_rows() > 0){
            $result = $query_produksi->result();
        }else{
            $result = false;
        }
        return $result;
    } 

    // TODO data produksi  (View H3)
    // TODO delete produksi (Delete H3)
    // TODO edit produksi (Edit H3)
}