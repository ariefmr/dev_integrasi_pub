<?php
/*
 * class Mdl_H3
 */

class Mdl_usaha_pengolahan extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function usaha_pengolahan($array_filter, $tahun, $bulan)
    {
        $sql='
            SELECT mjho.nama_jns_hasil_olah AS "jenis_usaha",
            moi.volume,
            moi.nilai
            FROM mst_jenis_hasil_olah mjho
            LEFT JOIN(
                SELECT SUM(moi.jml) AS "volume",
                    SUM(moi.harga) AS "nilai",
                    moi.id_jns_hasil_olah
                FROM mst_olah_ikan moi
                WHERE moi.aktif = "Ya"
                    AND YEAR(moi.tgl_catat) = '.$tahun.'
                    AND MONTH(moi.tgl_catat) = '.$bulan.'
                    AND id_pelabuhan = '.$array_filter['id_pelabuhan'].'
                GROUP BY moi.id_jns_hasil_olah
                ) moi
                ON moi.id_jns_hasil_olah = mjho.id_jns_hasil_olah
            WHERE mjho.aktif = "Ya"
        ';
       $query = $this->db_pipp->query($sql);
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}