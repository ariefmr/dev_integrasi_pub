<?php
/*
 * class Mdl_H3
 */

class Mdl_distribusi_ikan extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    }

    public function distribusi_ikan($array_filter)
    {   
        $sql_distribusi_ikan = "SELECT 
                                        mpb.nama_pelabuhan,
                                        mji.nama_jenis_ikan,
                                        mji.nama_jenis_ikan_inggris,
                                        mjki.nama_jenis_kondisi_ikan,
                                        SUM(tpk.jml_ikan) AS jumlah_ikan,
                                        ( SUM(tpk.jml_ikan) * AVG(tpk.harga_ikan) )  AS nilai_produksi,
                                        AVG(tpk.harga_ikan) AS harga_rata
                                        FROM
                                        db_pipp.trs_pemasaran_keluar tpk
                                            LEFT JOIN
                                        db_master.mst_pelabuhan mpb ON mpb.id_pelabuhan = tpk.id_pelabuhan
                                            LEFT JOIN
                                        db_pipp.mst_jenis_kondisi_ikan mjki ON mjki.id_jenis_kondisi_ikan = tpk.id_jenis_kondisi_ikan
                                            INNER JOIN
                                        db_master.mst_jenis_ikan mji ON mji.id_jenis_ikan = tpk.id_jenis_ikan";

        if($array_filter['filters'] === FALSE)
        {
            $id_pelabuhan = $array_filter['id_pelabuhan'];
            $tahun = $array_filter['tahun'];
            $bulan = $array_filter['bulan'];
            $sql_distribusi_ikan .= "
                            WHERE
                                YEAR(tpk.tgl_catat) = $tahun
                                    AND MONTH(tpk.tgl_catat) = $bulan
                                    ";
        }else{
          $id_pelabuhan = $array_filter['id_pelabuhan'];
          $filters = $array_filter['filters'];
                $tgl_awal = $filters['tgl_awal'];
                $tgl_akhir = $filters['tgl_akhir'];

                $sql_distribusi_ikan .= "
                            WHERE
                                (tpk.tgl_catat BETWEEN '$tgl_awal' AND '$tgl_akhir') ";

        }
        $sql_distribusi_ikan .= " AND tpk.aktif = 'Ya'
                                        AND tpk.id_pelabuhan = 'insert_id_pelabuhan'
                                        AND tpk.id_jenis_kondisi_ikan IN (mjki.id_jenis_kondisi_ikan)
                                        and_where_kateg_tujuan
                                    GROUP BY mji.nama_jenis_ikan
                                    ORDER BY mji.nama_jenis_ikan ASC";
        $arr_query = array();
        $arr_result = array();
        $query_kateg_tujuan = $this->db->query('SELECT id_kateg_tujuan, nama_kateg_tujuan FROM mst_kateg_tujuan');
        $kateg_tujuan = $query_kateg_tujuan->result();

                       

        foreach ($kateg_tujuan as $kategori) {
            $searches = array('and_where_kateg_tujuan','insert_id_pelabuhan');
            $replacements = array(" AND tpk.id_kateg_tujuan = '".$kategori->id_kateg_tujuan."' ", $array_filter['id_pelabuhan']);            
            $tmp_build_sql = str_replace($searches , $replacements , $sql_distribusi_ikan);     
            $tmp_result = $this->db->query($tmp_build_sql);
            $arr_query[$kategori->nama_kateg_tujuan] = $tmp_result->result();                        
        }

            $searches = array('and_where_kateg_tujuan','insert_id_pelabuhan');
            $replacements = array( '' , $array_filter['id_pelabuhan']);            
            $tmp_build_sql = str_replace($searches , $replacements , $sql_distribusi_ikan);     
            $tmp_result = $this->db->query($tmp_build_sql);
            $arr_query['Total'] = $tmp_result->result();         
       

        if($arr_query){
            $result = $arr_query;
        }else{
            $result = false;
        }
        return $result;
    }  

    public function tujuan($array_filter)
    {
        return FALSE;
    }
}