<?php
/*
 * class Mdl_H3
 */

class Mdl_Kebutuhan_BBM extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function kebutuhan_bbm($array_filter, $tahun = "", $bulan = "")
    {
        $tahun = empty($tahun) ? date("Y") : $tahun;
        $sql = '
            SELECT
                "1" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "1"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "2" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "2"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "3" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "3"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "4" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "4"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "5" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "5"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "6" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "6"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "7" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "7"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "8" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "8"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "9" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "9"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "10" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "10"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "11" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "11"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")

            UNION ALL
            SELECT
                "12" AS bulan,
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "1" THEN tpkk.jml_perbekalan END) as "solar",
                SUM(CASE WHEN  tpkk.id_jns_perbekalan = "9" THEN tpkk.jml_perbekalan END) as "bensin"
            FROM trs_perbekalan_kapal_keluar tpkk
            WHERE YEAR(tpkk.tgl_aktivitas) = "'.$tahun.'"
                AND MONTH(tpkk.tgl_aktivitas) = "12"
                AND tpkk.id_pelabuhan = "'.$array_filter['id_pelabuhan'].'"
                AND tpkk.aktif = "Ya"
                AND tpkk.id_jns_perbekalan IN ("1", "9")
        ';
        // $this->db_pipp->select('
        //                         MONTH(tgl_aktivitas) as bulan,
        //                         SUM(CASE WHEN  id_jns_perbekalan = "1" THEN jml_perbekalan END) as "solar",
        //                         SUM(CASE WHEN  id_jns_perbekalan = "9" THEN jml_perbekalan END) as "bensin"
        //                         ');
        // $this->db_pipp->from('trs_perbekalan_kapal_keluar tpkk');
        // $this->db_pipp->where('id_pelabuhan', $array_filter['id_pelabuhan']);
        // $this->db_pipp->where('YEAR(tgl_aktivitas)', $tahun);
        // $this->db_pipp->where('aktif', 'Ya');
        // $this->db_pipp->where_in('id_jns_perbekalan', array('1', '9'));
        // $this->db_pipp->group_by('id_pelabuhan, MONTH(tgl_aktivitas)');

       $query = $this->db_pipp->query($sql);
        // echo $this->db_pipp->last_query();
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}