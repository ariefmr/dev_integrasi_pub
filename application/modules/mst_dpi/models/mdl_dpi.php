<?php
/*
 * class Mdl_dpi
 * created by ariefmr
 * at Kamar
 * 25-09-2013
 */

class Mdl_dpi extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_master = NULL;

    function __construct()
    {
        $this->db_master = $this->load->database('db_dss', TRUE);
    }
    
    public function list_dpi($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_master->like('aktif', 'ya');
        }

        $query = $this->db_master->get('mst_dpi');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_dpi()
    {
        $query = 'SELECT id_dpi AS id, nama_dpi AS text FROM mst_dpi';
        
        $run_query = $this->db_master->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_dpi_join_wpp()
    {
        $query = 'SELECT mst_dpi.id_dpi, mst_dpi.nama_dpi,
                    mst_wpp.nama_wpp, mst_wpp.no_wpp , mst_wpp.id_wpp 
                    FROM mst_dpi,mst_wpp WHERE mst_dpi.id_wpp = mst_wpp.id_wpp
                    GROUP BY mst_dpi.id_dpi';

        $run_query = $this->db_master->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function search_dpi($id_wpp)
    {
        $query = 'SELECT id_dpi AS id, nama_dpi AS text FROM mst_dpi WHERE id_wpp = '.$id_wpp;

        $run_query = $this->db_master->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>