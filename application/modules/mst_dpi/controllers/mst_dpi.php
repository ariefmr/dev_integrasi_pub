<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_dpi extends MX_Controller {

	/**
	 * Controller Mst DPI
	 * created by ariefmr
 	 * at Kamar
 	 * 25-09-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_dpi');
		}

	public function index()
	{

		$data['list_dpi'] = $this->mdl_dpi->list_dpi();
		$this->load->view('daftar_dpi', $data);
	}

	public function daftar_dpi($aktif = '') 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis dpi';
		$data['content_title'] = 'Daftar Jenis dpi';
		$data['module'] = 'mst_dpi';
		$data['view_file'] = 'daftar_dpi';
		if($aktif === "aktif"){
			$data['list_dpi'] = $this->mdl_dpi->list_dpi(TRUE);
		}else{
			$data['list_dpi'] = $this->mdl_dpi->list_dpi();

		}
		echo Modules::run('templates/type/default_template', $data);	
	}

	public function list_dpi_wpp()
	{
		$result_list = $this->mdl_dpi->list_dpi_join_wpp();		
		$list_dpi_wpp = array();
			foreach ($result_list as $item) {
				$list_dpi_wpp[ $item->nama_wpp.' ( '.$item->no_wpp.' )' ][ $item->id_dpi ] = array('id_dpi' => $item->id_dpi,
																		'id_wpp' => $item->id_wpp,
																		'nama_wpp' => $item->nama_wpp,
																		'nama_dpi' => $item->nama_dpi );
			}
		return $list_dpi_wpp;
	}
	// Wigdet Pencarian Alat Tangkap untuk keperluan entry form 
	public function wgt_pilih_dpi()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_dpi'] = FALSE;
		$this->load->view('pilih_dpi', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar dpi
	public function select_dpi()
	{
		$data['list_dpi'] = $this->mdl_dpi->list_dpi();
		$this->load->view('select_dpi', $data);
	}

	public function json_dpi()
	{
		$array_of_dpi = (Array) $this->mdl_dpi->list_opsi_dpi();

		echo json_encode($array_of_dpi);
	}

	public function table_dpi()
	{
		$data['list_dpi'] = $this->mdl_dpi->list_dpi();	
		$this->load->view('daftar_dpi', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}

	public function search_dpi()
	{
		$get_id_wpp = $this->input->get('id_wpp', FALSE);
		$search_result = $this->mdl_dpi->search_dpi($get_id_wpp);
		$array_result = Array( 'total' => count($search_result),'list_dpi' => $search_result );
		echo json_encode($array_result);
	}

}

/* End of file mst_dpi.php */
/* Location: ./application/modules/mst_dpi/controllers*/