<?php
	/*
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_pilih_dpi' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('Nama dpi','SIPI','Perusahaan','Alat Tangkap','Tonase');

	
	$counter = 1;
	foreach ($list_dpi as $item) {
		$this->table->add_row($counter.'.', $item->nama_dpi, $item->no_sipi, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_dpi, $item->gt_dpi);
		$counter++;
	}
	
	$table_list_dpi = $this->table->generate();
	*/
?>

<!-- TAMPIL DATA -->
	<div class="form-group">
					<label for="id_dpi" class="col-lg-4 control-label">Pilih Alat Tangkap :</label>
					<div class="col-lg-8">
                         <input id="start_search" name="id_dpi" type="hidden" class="bigdrop">
                    </div>
    </div>
	


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null;

	function formatListdpiResult(dpi)
	{
		//var markup = "<table class='dpi-result'><tr>";
        //markup += "<td class='dpi-info'><div class='dpi-nama'>" + dpi.nama_dpi + "</div>";
        //markup += "</td></tr></table>";
        //return markup;
		//return "<div class='result_dpi' id='"+dpi.id_dpi+"'>"+dpi.nama_dpi+" / "+dpi.no_sipi+"</div>";
		return "<div class='result_dpi' id='"+dpi.id_dpi+"'><strong>"+dpi.nama_dpi+"</strong> / <small>"+dpi.tanda_selar+"</small></div>";

	}

	function formatListdpiSelection(dpi)
	{
		return dpi.nama_dpi;
	}

	$(document).ready( function () {
		


		$("#start_search").select2({
									id: function(e) { return e.id_dpi },  	
									placeholder: "Mulai ketik nama DPI..",
									width: "100%",
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: "<?php echo base_url('mst_dpi/search_dpi'); ?>",
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_dpi};
									        }
									},
                                    formatResult: formatListAlatTangkapResult,
                                    formatSelection: formatListAlatTangkapSelection
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_dpi(e.val);
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_dpi/json_dpi'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_dpi(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>