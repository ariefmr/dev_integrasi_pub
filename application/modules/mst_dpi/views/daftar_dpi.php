<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_dpi' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'ID dpi', 'ID WPP', 'Nama DPI','Nama DPI Inggris', 'Lintang', 'Bujur', 'ID Pengguna Buat', 'Tanggal Buat', 'ID Tanggal Ubah', 'Tanggal Ubah', 'Aktif');
	$counter = 1;

	if($list_dpi !== FALSE){
		foreach ($list_dpi as $item) {
			$this->table->add_row($counter.'.', $item->id_dpi, $item->id_wpp, $item->nama_dpi, $item->nama_dpi_inggris, $item->lintang, $item->bujur, $item->id_pengguna_buat, $item->tanggal_buat, $item->id_pengguna_ubah, $item->tanggal_ubah, $item->aktif);
			$counter++;
		}
	}	

	$table_list_dpi = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_dpi;

		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_dpi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>