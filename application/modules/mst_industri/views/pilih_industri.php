<?php
	/*
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_pilih_industri' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('Nama industri','SIPI','Perusahaan','Alat Tangkap','Tonase');

	
	$counter = 1;
	foreach ($list_industri as $item) {
		$this->table->add_row($counter.'.', $item->nama_industri, $item->no_sipi, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_industri, $item->gt_industri);
		$counter++;
	}
	
	$table_list_industri = $this->table->generate();
	*/
?>

<!-- TAMPIL DATA -->
	<div class="form-group">
					<label for="id_industri" class="col-lg-4 control-label">Pilih Industri Tangkap :</label>
					<div class="col-lg-8">
                         <input id="start_search" name="id_industri" type="hidden" class="bigdrop">
                    </div>
    </div>
	


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null;

	function formatListindustriResult(industri)
	{
		//var markup = "<table class='industri-result'><tr>";
        //markup += "<td class='industri-info'><div class='industri-nama'>" + industri.nama_industri + "</div>";
        //markup += "</td></tr></table>";
        //return markup;
		//return "<div class='result_industri' id='"+industri.id_industri+"'>"+industri.nama_industri+" / "+industri.no_sipi+"</div>";
		return "<div class='result_industri' id='"+industri.id_industri+"'><strong>"+industri.nama_industri+"</strong> / <small>"+industri.tanda_selar+"</small></div>";

	}

	function formatListindustriSelection(industri)
	{
		return industri.nama_industri;
	}

	$(document).ready( function () {
		


		$("#start_search").select2({
									id: function(e) { return e.id_industri },  	
									placeholder: "Mulai ketik nama industri..",
									width: "100%",
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: "<?php echo base_url('mst_industri/search_industri'); ?>",
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_industri};
									        }
									},
                                    formatResult: formatListAlatTangkapResult,
                                    formatSelection: formatListAlatTangkapSelection
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_industri(e.val);
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_industri/json_industri'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_industri(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>