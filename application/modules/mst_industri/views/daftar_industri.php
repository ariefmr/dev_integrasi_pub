<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_industri' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'ID Industri', 'Nama Industri','Nama Pemilik', 'Alamat');
	$counter = 1;

	if($list_industri !== FALSE){
		foreach ($list_industri as $item) {
			$this->table->add_row($counter.'.', $item->id_industri, $item->nama_industri, $item->nama_pemilik, $item->alamat);
			$counter++;
		}
	}	

	$table_list_industri = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_industri;

		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_industri').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>