<?php
/*
 * class Mdl_industri
 * created by ariefmr
 * at Celebes
 * 2-10-2013
 */

class Mdl_industri extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_pipp = NULL;

    function __construct()
    {
        $this->db_pipp = $this->load->database('default', TRUE);
    }
    
    public function list_industri($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_pipp->like('aktif', 'ya');
        }

        $query = $this->db_pipp->get('mst_industri');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_industri($id_pelabuhan)
    {
        $query = "SELECT db_pipp.mst_industri.id_industri AS id, nama_industri AS text FROM db_pipp.mst_industri
                    WHERE db_pipp.mst_industri.aktif = 'Ya' and db_pipp.mst_industri.id_pelabuhan = $id_pelabuhan   ";
        
        $run_query = $this->db_pipp->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>