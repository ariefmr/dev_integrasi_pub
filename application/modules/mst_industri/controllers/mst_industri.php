<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_industri extends MX_Controller {

	/**
	 * Controller Mst industri
	 * created by ariefmr
 	 * at Celebes
 	 * 2-10-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_industri');
		}

	public function index()
	{

		$data['list_industri'] = $this->mdl_industri->list_industri();
		$this->load->view('daftar_industri', $data);
	}

	public function daftar_industri($aktif = '') 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis industri';
		$data['content_title'] = 'Daftar Jenis industri';
		$data['module'] = 'mst_industri';
		$data['view_file'] = 'daftar_industri';
		if($aktif === "aktif"){
			$data['list_industri'] = $this->mdl_industri->list_industri(TRUE);
		}else{
			$data['list_industri'] = $this->mdl_industri->list_industri();

		}
		echo Modules::run('templates/type/default_template', $data);	
	}

	// Wigdet Pencarian Alat Tangkap untuk keperluan entry form 
	public function wgt_pilih_industri()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_industri'] = FALSE;
		$this->load->view('pilih_industri', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar industri
	public function select_industri()
	{
		$data['list_industri'] = $this->mdl_industri->list_industri();
		$this->load->view('select_industri', $data);
	}

	public function array_industri($id_pelabuhan)
	{
		$array_industri = $this->mdl_industri->list_opsi_industri($id_pelabuhan);
		return $array_industri;
		//var_dump($array_industri);
	}

	public function json_industri($id_pelabuhan)
	{
		$array_of_industri = (Array) $this->mdl_industri->list_opsi_industri($id_pelabuhan);

		echo json_encode($array_of_industri);
	}

	public function table_industri()
	{
		$data['list_industri'] = $this->mdl_industri->list_industri();	
		$this->load->view('daftar_industri', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}

	public function search_industri()
	{
		$get_nama_industri = $this->input->get('q', FALSE);
		$get_limit_result = $this->input->get('limit', FALSE);
		$search_result = $this->mdl_industri->search_industri($get_nama_industri, $get_limit_result);
		$array_result = Array( 'total' => count($search_result),'list_industri' => $search_result );
		echo json_encode($array_result);
	}

}

/* End of file mst_industri.php */
/* Location: ./application/modules/mst_industri/controllers*/