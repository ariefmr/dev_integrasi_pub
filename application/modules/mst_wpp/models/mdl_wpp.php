<?php
/*
 * class Mdl_wpp
 * created by ariefmr
 * at McD
 * 25-09-2013
 */

class Mdl_wpp extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_master = NULL;

    function __construct()
    {
        $this->db_master = $this->load->database('db_dss', TRUE);
    }
    
    public function list_wpp($is_aktif = FALSE)
    {
        if($is_aktif){
            $this->db_master->like('aktif', 'ya');
        }

        $query = $this->db_master->get('mst_wpp');
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_wpp()
    {
        $query = 'SELECT id_wpp AS id, nama_wpp AS text FROM mst_wpp';
        
        $run_query = $this->db_master->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
?>