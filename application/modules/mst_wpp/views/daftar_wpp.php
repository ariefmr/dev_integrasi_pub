<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_wpp' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('No', 'ID wpp', 'No WPP', 'Nama wpp','Nama wpp Inggris', 'Lintang', 'Bujur', 'WPP Perizinan', 'ID Pengguna Buat', 'Tanggal Buat', 'ID Tanggal Ubah', 'Tanggal Ubah', 'Aktif');
	$counter = 1;

	if($list_wpp !== FALSE){
		foreach ($list_wpp as $item) {
			$this->table->add_row($counter.'.', $item->id_wpp, $item->no_wpp, $item->nama_wpp, $item->nama_wpp_inggris, $item->lintang, $item->bujur, $item->wpp_perizinan, $item->id_pengguna_buat, $item->tanggal_buat, $item->id_pengguna_ubah, $item->tanggal_ubah, $item->aktif);
			$counter++;
		}
	}	

	$table_list_wpp = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_wpp;

		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_wpp').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
		} );
	} );
</script>