<?php
	/*
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_pilih_wpp' class='table table-hover table-condensed'>");
	$this->table->set_template($template);
	$this->table->set_heading('Nama wpp','SIPI','Perusahaan','Alat Tangkap','Tonase');

	
	$counter = 1;
	foreach ($list_wpp as $item) {
		$this->table->add_row($counter.'.', $item->nama_wpp, $item->no_sipi, $item->nama_perusahaan.'/'.$item->nama_penanggung_jawab, $item->nama_wpp, $item->gt_wpp);
		$counter++;
	}
	
	$table_list_wpp = $this->table->generate();
	*/
?>

<!-- TAMPIL DATA -->
	<div class="form-group">
					<label for="id_wpp" class="col-lg-4 control-label">Pilih Alat Tangkap :</label>
					<div class="col-lg-8">
                         <input id="start_search" name="id_wpp" type="hidden" class="bigdrop">
                    </div>
    </div>
	


<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var search_response_time = 2000, //2 Detik
		thread = null;

	function formatListwppResult(wpp)
	{
		//var markup = "<table class='wpp-result'><tr>";
        //markup += "<td class='wpp-info'><div class='wpp-nama'>" + wpp.nama_wpp + "</div>";
        //markup += "</td></tr></table>";
        //return markup;
		//return "<div class='result_wpp' id='"+wpp.id_wpp+"'>"+wpp.nama_wpp+" / "+wpp.no_sipi+"</div>";
		return "<div class='result_wpp' id='"+wpp.id_wpp+"'><strong>"+wpp.nama_wpp+"</strong> / <small>"+wpp.tanda_selar+"</small></div>";

	}

	function formatListwppSelection(wpp)
	{
		return wpp.nama_wpp;
	}

	$(document).ready( function () {
		


		$("#start_search").select2({
									id: function(e) { return e.id_wpp },  	
									placeholder: "Mulai ketik nama wpp..",
									width: "100%",
									minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
									        url: "<?php echo base_url('mst_wpp/search_wpp'); ?>",
									        dataType: "json",
									        quietMillis: 2000,
									        data: function(term, page){
									                       return {
																	q: term,
																	limit: 100 // TODO : tentuin limit result
															       };
											},
											results: function(data, page){
									                 return {results: data.list_wpp};
									        }
									},
                                    formatResult: formatListAlatTangkapResult,
                                    formatSelection: formatListAlatTangkapSelection
                                    });

		$("#start_search").on("change",function(e) { 
										//console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
									  	get_detail_wpp(e.val);
									  });
		/*

		 ajax: {
                                      		url: "<?php echo base_url('mst_wpp/json_wpp'); ?>",
                                      		dataType: "jsonp",
                                      		data: function(term, page){
                                      			return {
                                      				q: "term",
                                      				limit: 100 // TODO : tentuin limit result
                                      			};
                                      		},
                                      		results: function(data, page){
                                      			return {results: data}
                                      		}
		$('#start_search').keyup(function(e){
			clearTimeout(thread);
			
			var keyword = $(this).val();

			thread = setTimeout(function(){
				update_result_wpp(keyword);
			} ,search_response_time);
		});
		*/	
	} );
</script>