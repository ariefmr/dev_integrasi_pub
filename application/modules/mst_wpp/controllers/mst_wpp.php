<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_wpp extends MX_Controller {

	/**
	 * Controller Mst WPP
	 * created by ariefmr
 	 * at McD
 	 * 25-09-2013
	 * 
	 */
	private $assets_paths = '';
		function __construct()
		{
			parent::__construct();
			//$this->load->config('custom_constants');

			$this->assets_paths = $this->config->item('assets_paths');
			$this->load->model('mdl_wpp');
		}

	public function index()
	{

		$data['list_wpp'] = $this->mdl_wpp->list_wpp();
		$this->load->view('daftar_wpp', $data);
	}

	public function daftar_wpp($aktif = '') 
	{
		$data['additional_js'] = Array('jquery.dataTables.min.js');
		$data['additional_css'] = Array('jquery.dataTables.css');

		$data['page_title'] = 'Daftar Jenis wpp';
		$data['content_title'] = 'Daftar Jenis wpp';
		$data['module'] = 'mst_wpp';
		$data['view_file'] = 'daftar_wpp';
		if($aktif === "aktif"){
			$data['list_wpp'] = $this->mdl_wpp->list_wpp(TRUE);
		}else{
			$data['list_wpp'] = $this->mdl_wpp->list_wpp();

		}
		echo Modules::run('templates/type/default_template', $data);	
	}

	// Wigdet Pencarian Alat Tangkap untuk keperluan entry form 
	public function wgt_pilih_wpp()
	{
		//$data['list_kapal'] = $this->mdl_kapal->search_kapal($nama_kapal);
		$data['list_wpp'] = FALSE;
		$this->load->view('pilih_wpp', $data);
	}

	// Menghasilkan elemen dropdown select dengan opsi daftar wpp
	public function select_wpp()
	{
		$data['list_wpp'] = $this->mdl_wpp->list_wpp();
		$this->load->view('select_wpp', $data);
	}

	public function json_wpp()
	{
		$array_of_wpp = (Array) $this->mdl_wpp->list_opsi_wpp();

		echo json_encode($array_of_wpp);
	}

	public function table_wpp()
	{
		$data['list_wpp'] = $this->mdl_wpp->list_wpp();	
		$this->load->view('daftar_wpp', $data);
	}

	public function test()
	{
		echo Modules::run('templates/type/test');
	}

	public function search_wpp()
	{
		$get_nama_wpp = $this->input->get('q', FALSE);
		$get_limit_result = $this->input->get('limit', FALSE);
		$search_result = $this->mdl_wpp->search_wpp($get_nama_wpp, $get_limit_result);
		$array_result = Array( 'total' => count($search_result),'list_wpp' => $search_result );
		echo json_encode($array_result);
	}

	public function array_wpp()
	{
		$array = $this->mdl_wpp->list_wpp();
		return $array;
	}

}

/* End of file mst_wpp.php */
/* Location: ./application/modules/mst_wpp/controllers*/