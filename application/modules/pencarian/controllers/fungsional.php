<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fungsional extends MX_Controller {

	/**
	 * Nama File: 
	 * Author: Frendhi
	 * Deskripsi: 
	 * 
	 */
		function __construct()
		{
			parent::__construct();
			$this->load->library('entry/mkform');
			$this->load->model('mdl_fungsional');
			$this->load->model('mdl_dropdown');
			$this->list_report = $this->config->item('list_report');
			//TODO: LOGIN CHECK HERE
		}

	public function index()
	{
		$input_filter = $this->input->post(NULL, TRUE);
			if(empty($input_filter))
			{
				$data['data_laporan'] = FALSE;
			}else{
				if($input_filter['jenis_fasilitas'] == 0)
				{
					$data['data_laporan'] = $this->mdl_fungsional->data_pokok($input_filter);
				}
			}
			// vdump($data['data_laporan']);
	    $data['module'] = 'pencarian';
	    $data['view_file'] = 'v_fungsional';
	    $data['page_title'] = 'PENCARIAN > FUNGSIONAL';
	    $data['content_title'] = 'PENCARIAN FUNGSIONAL';
	    $data['additional_js'] = Array('jquery.dataTables.min.js','jquery.jkey.min.js', 'select2.min.js');
	    $data['additional_css'] = Array('jquery.dataTables.css','select2.css');
	    $data['breadcrumbs'] = 'Pencarian->Fungsional';
	    $data['nama_pelabuhan_info'] =  $this->pipp_lib->str_nama_pelabuhan('3');

	    $data['pre_filter'] = $input_filter;
	    $data['opsi_alat_tangkap'] = Modules::run('mst_alat_tangkap/json_alat_tangkap');
	    $data['opsi_pelabuhan'] = Modules::run('mst_pelabuhan/json_pelabuhan_lama');
	    $data['opsi_jenis_ikan'] = Modules::run('mst_jenis_ikan/json_ikan');
	    $data['opsi_tipe_kapal'] = Modules::run('tables/dss_tbl_json','mst_jenis_kapal','id_jenis_kapal','nama_jenis_kapal');

	    //====
	    $data['opsi_ket_konstruksi'] = json_encode((array) $this->mdl_dropdown->list_ket_konstruksi());
	    $data['opsi_deskripsi_status'] = json_encode((array) $this->mdl_dropdown->list_deskripsi_status());
	    $data['opsi_pengelola_pelabuhan'] = json_encode((array) $this->mdl_dropdown->list_pengelola_pelabuhan());
	    // vdump($data);
	    $data['list_report'] = $this->list_report;
	    echo Modules::run('templates/type/reporting', $data);
	}
	
}
// TODO BETULIN KETERANGAN
/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */