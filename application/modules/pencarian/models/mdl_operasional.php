<?php
/*
 * class Mdl_H3
 */

class Mdl_operasional extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    
    }

    //Mode Pencarian
    /*
        model pencarian query
        $data['nama_kapal'] => pencarian nama kapal berdasarkan "Like" nama_kpal
                            => variabel string
        $data['tipe_kapal'] => pencarian jenis kapal berdasarkan id_jenis_kapal
                            => variabel int
                            => id_jenis_kapal dari mst_jenis_kapal
        $data['alat_tangkap']   => pencarian alat tangkap berdasarkan id_alat_tangkap
                                => variabel int
                                => id_alat_tangkap
        $data['pelabuhan_pangkalan']    => pencarian pelabhuan berdasarkan id_pelabuhan
                                        => variabel int
        $data['aktivitas']  => pencarian masuk atau keluar bedsarkan aktivitas kapal
                            => string "masuk"/"keluar"
        $data['tgl_aktivitas_bln']  => percarian berdasarkan bulan tgl_aktivitas_kapal
                                    => variabel int
                                    => bulang
        $data['tgl_aktivitas_thn']  => percarian berdasarkan tahun tgl_aktivitas_kapal
                                    => variabel int
                                    => tahun
        $data['jenis_ikan'] => percarian jenis ikan bedasarkan id_jenis_ikan
                            => variabel int
                            => atau array contoh -> array(1,24,144)
        $data['jumlah_abk'] => percarian berdasarkan jumlah ABK
            index select    0 => -/all
                            1 => <=5
                            2 => 6-10
                            3 => 11-20
                            4 => 21-30
                            5 => <30
        $data['jumlah_perbekalan']
            index select    0 => -/all
                            1 => <=100
                            2 => 101-1000
                            3 => 1001-25000
                            4 => >25000
        $data['biaya_operasional'] => percarian berdasarkan biaya_oprasional
            index select    0 => <=1 juta
                            1 => 1 juta - 10 juta
                            2 => 10 juta - 50 juta
                            3 => 50 juta - 100 juta
                            4 => >100juta
 */
    // function data_pencarian_view($filter, $limit = 200)
    // {
        
    //     foreach ($filter as $key => $value) {
    //         if(!empty($value))
    //         {   
    //             $explode_key = explode("0", $key);
    //             $field_name = $explode_key[1];
    //             $filter_use = $explode_key[0];
    //             switch ($filter_use) {
    //                 case 'like':
    //                     $this->db->like($field_name, $value);
    //                     // echo $field_name." LIKE ".$value;
    //                     break;
    //                 case 'where':
    //                     $this->db->where($field_name, $value);
    //                     // echo $field_name." WHERE ".$value;
    //                     break;
    //                 case 'morethan':
    //                     $this->db->where($field_name.' >= ', $value);
    //                     // echo $field_name." >= ".$value;
    //                     break;
    //             }
    //         }
    //     }
    //     // die;
    //     $query = $this->db->get('v_pencarian_operasional');

    //     // var_dump($this->db->last_query());
    //     if($query->num_rows() > 0){
    //         $result = $query->result();
    //     }else{
    //         $result = false;
    //     }
    //     return $result;
    // }

    function data_pencarian($data)
    {
        //kondisi pencarian nama kapal
        if( empty($data['nama_kapal']) === FALSE )
        {
            $nama_kapal = 'AND ( mmk.nama_kapal LIKE "%'.$data['nama_kapal'].'%"';
            $nama_kapal .= 'OR mmkd.nama_kapal LIKE "%'.$data['nama_kapal'].'%" )';
        } else $nama_kapal = '';

        //kondisi pencarian tipe kapal
        if ( empty($data['tipe_kapal']) === FALSE ) 
        {
            $tipe_kapal = 'AND ( mmk.id_jenis_kapal = "'.$data['tipe_kapal'].'"';
            $tipe_kapal .= 'OR mmkd.id_jenis_kapal = "'.$data['tipe_kapal'].'" )';
        } else $tipe_kapal = '';

        //kondisi pencarian alat tangkap
        if( empty($data['alat_tangkap']) === FALSE )
        {
            $alat_kapal = 'AND ( mmk.id_alat_kapal = "'.$data['alat_kapal'].'"';
            $alat_kapal .= 'OR mmkd.id_alat_kapal = "'.$data['alat_kapal'].'" )';
        } else $alat_kapal = '';

        //kondisi pencarian pelabuhan pangkalan
        if( empty($data['pelabuhan_pangkalan']) === FALSE )
        {
            $pelabuhan_pangkalan = 'AND ptak.id_pelabuhan = "'.$data['pelabuhan_pangkalan'].'"';
        } else $pelabuhan_pangkalan = '';

        //kondisi pencarian aktivitas
        if( empty($data['aktivitas']) === FALSE )
        {
            $aktivitas = 'AND ptak.aktivitas = "'.$data['aktivitas'].'"';
        } else $aktivitas = '';

        //kondisi pencarian produksi nilai bulan
        if( empty($data['tgl_aktivitas_bln']) === FALSE )
        {
            $tgl_aktivitas_bln = 'AND MONTH(ptak.tgl_aktivitas) = "'.$data['tgl_aktivitas_bln'].'"';
        } else $tgl_aktivitas_bln = '';

        //kondisi pencarian produksi nilai tahun
        if( empty($data['tgl_aktivitas_thn']) === FALSE )
        {
            $tgl_aktivitas_thn = 'AND YEAR(ptak.tgl_aktivitas) = "'.$data['tgl_aktivitas_thn'].'"';
        } else $tgl_aktivitas_thn = '';

        //kondisi pencarian jenis ikan
        if( empty($data['jenis_ikan']) === FALSE )
        {
            if( is_array($data['jenis_ikan']) )
            {
                $index = 0;
                $jenis_ikan = '';
                foreach ($data['jenis_ikan'] as $key) {
                    if( $index == 0) $jenis_ikan .= ' AND (';
                    if( $index != 0) $jenis_ikan .= ' OR ';
                    $jenis_ikan .= 'FIND_IN_SET("'.$key.'", ptp.id_jenis_ikan)';
                    $index++;
                }
                $jenis_ikan .= ')';
            } else $jenis_ikan = 'AND FIND_IN_SET("'.$data['jenis_ikan'].'", ptp.id_jenis_ikan)';
        } else $jenis_ikan = '';

        //kondisi pencarian jumlah abk
        if (empty( $data['jumlah_abk']) === FALSE )
        {
            if( $data['jumlah_abk'] === 1 )
            {
                $jumlah_abk = 'AND ptak.jumlah_abk <= 5 ';
            }elseif ( $data['jumlah_abk'] === 2) 
            {
               $jumlah_abk = 'AND ptak.jumlah_abk > 5 ';
               $jumlah_abk .= 'AND ptak.jumlah_abk <=  10 ';
            }elseif ( $data['jumlah_abk'] === 3) 
            {
               $jumlah_abk = 'AND ptak.jumlah_abk > 10 ';
               $jumlah_abk .= 'AND ptak.jumlah_abk <=  20 ';
            }elseif ( $data['jumlah_abk'] === 4) 
            {
               $jumlah_abk = 'AND ptak.jumlah_abk > 20 ';
               $jumlah_abk .= 'AND ptak.jumlah_abk <=  30 ';
            }elseif ( $data['jumlah_abk'] === 5) 
            {
                $jumlah_abk = 'AND ptak.jumlah_abk > 30 ';
            } else $jumlah_abk = '';
        } else $jumlah_abk = '';

        //kondisi pencarian jumlah_perbekalan
        if (empty( $data['jumlah_perbekalan']) === FALSE )
        {
            if( $data['jumlah_perbekalan'] === 1 )
            {
                $jumlah_perbekalan = 'AND ptpkk.jumlah_perbekalan <= 100 ';
            }elseif ( $data['jumlah_perbekalan'] === 2) 
            {
               $jumlah_perbekalan = 'AND ptpkk.jumlah_perbekalan > 100 ';
               $jumlah_perbekalan .= 'AND ptpkk.jumlah_perbekalan <=  1000 ';
            }elseif ( $data['jumlah_perbekalan'] === 3) 
            {
               $jumlah_perbekalan = 'AND ptpkk.jumlah_perbekalan > 1000 ';
               $jumlah_perbekalan .= 'AND ptpkk.jumlah_perbekalan <=  25000 ';
            }elseif ( $data['jumlah_perbekalan'] === 4) 
            {
               $jumlah_perbekalan = 'AND ptpkk.jumlah_perbekalan > 25000 ';
            } else $jumlah_perbekalan = '';
        } else $jumlah_perbekalan = '';

        //kondisi pencarian biaya_operasional
        if (empty( $data['biaya_operasional']) === FALSE )
        {
            if( $data['biaya_operasional'] === 1 )
            {
                $biaya_operasional = 'AND ptak.biaya_operasional <= 1000000 ';
            }elseif ( $data['biaya_operasional'] === 2) 
            {
               $biaya_operasional = 'AND ptak.biaya_operasional > 1000000 ';
               $biaya_operasional .= 'AND ptak.biaya_operasional <=  10000000 ';
            }elseif ( $data['biaya_operasional'] === 3) 
            {
               $biaya_operasional = 'AND ptak.biaya_operasional > 10000000 ';
               $biaya_operasional .= 'AND ptak.biaya_operasional <=  50000000 ';
            }elseif ( $data['biaya_operasional'] === 4) 
            {
               $biaya_operasional = 'AND ptak.biaya_operasional > 50000000 ';
               $biaya_operasional .= 'AND ptak.biaya_operasional <=  100000000 ';
            }elseif ( $data['biaya_operasional'] === 5) 
            {
                $biaya_operasional = 'AND ptak.biaya_operasional > 100000000 ';
            } else $biaya_operasional = '';
        } else $biaya_operasional = '';

        $sql = '
            SELECT 
                ak.*,
                ptpkk.*,
                ptp.*,
                mmp.nama_pelabuhan AS "nama_pelabuhan_pangkalan",
                mmjk.nama_jenis_kapal,
                mmat.nama_alat_tangkap
            FROM(
                    SELECT
                        ptak.id_aktivitas_kapal,
                        ptak.id_pelabuhan,
                        ptak.id_pelabuhan_tujuan,
                        ptak.id_pelabuhan_masuk,
                        ptak.id_kapal,
                        CASE ptak.filter_gt 
                            WHEN "above" THEN "non-Daerah" 
                            WHEN "below" THEN "Daerah" 
                            END AS "type",
                       CASE ptak.filter_gt 
                            WHEN "above" THEN mmk.nama_kapal 
                            WHEN "below" THEN mmkd.nama_kapal
                            END AS "nama_kapal",
                        CASE ptak.filter_gt 
                            WHEN "above" THEN mmk.id_jenis_kapal 
                            WHEN "below" THEN mmkd.id_jenis_kapal
                            END AS "id_jenis_kapal",
                        CASE ptak.filter_gt 
                            WHEN "above" THEN mmk.id_alat_tangkap 
                            WHEN "below" THEN mmkd.id_alat_tangkap
                            END AS "id_alat_tangkap",
                        ptak.filter_gt,
                        ptak.aktivitas,
                        ptak.tgl_aktivitas,
                        CONCAT(ptak.aktivitas, ", ", ptak.tgl_aktivitas) as "tgl_aktivitas_2",
                        ptak.jumlah_abk
                    FROM 
                        db_pipp.trs_aktivitas_kapal ptak
                    LEFT JOIN db_master.mst_kapal mmk
                        ON mmk.id_kapal = ptak.id_kapal 
                            AND ptak.filter_gt = "above" 
                            AND mmk.aktif = "Ya"
                    LEFT JOIN db_master.mst_kapal_daerah mmkd
                        ON mmkd.id_kapal_daerah = ptak.id_kapal 
                            AND ptak.filter_gt = "below"
                            AND mmkd.aktif = "Ya"
                    WHERE 
                        ptak.aktif = "Ya"
                        AND ptak.filter_gt <> ""
                        '.$nama_kapal.'
                        '.$tipe_kapal.'
                        '.$alat_kapal.'
                        '.$pelabuhan_pangkalan.'
                        '.$aktivitas.'
                        '.$tgl_aktivitas_bln.'
                        '.$tgl_aktivitas_thn.'
                        '.$jumlah_abk.'
                    ORDER BY ptak.tgl_aktivitas
                ) ak
            LEFT JOIN db_master.mst_pelabuhan mmp
                ON mmp.id_pelabuhan = ak.id_pelabuhan AND mmp.aktif = "Ya"
            LEFT JOIN db_master.mst_jenis_kapal mmjk 
                ON mmjk.id_jenis_kapal = ak.id_jenis_kapal
            LEFT JOIN db_master.mst_alat_tangkap mmat
                ON mmat.id_alat_tangkap = ak.id_alat_tangkap
            LEFT JOIN 
                    (SELECT ptpkk.id_aktivitas_kapal,
                                SUM(CASE ptpkk.id_jns_perbekalan WHEN 2 THEN ptpkk.jml_perbekalan END) AS "biaya_oprasional",
                                (SUM(CASE ptpkk.id_jns_perbekalan WHEN 1 THEN ptpkk.jml_perbekalan END) +
                                    SUM(CASE ptpkk.id_jns_perbekalan WHEN 4 THEN ptpkk.jml_perbekalan END)) AS "jumlah_perbekalan"
                    FROM db_pipp.trs_perbekalan_kapal_keluar ptpkk
                    WHERE ptpkk.aktif = "Ya"
                    GROUP BY ptpkk.id_aktivitas_kapal
                    ) ptpkk
                ON ptpkk.id_aktivitas_kapal = ak.id_aktivitas_kapal
            LEFT JOIN 
                    (SELECT 
                        SUM(ptp.jml_ikan) AS "jml_ikan", 
                        SUM(ptp.harga_pedagang) AS "harga_pedagang",
                        group_concat(mmji.nama_jenis_ikan separator ", ") AS "nama_jenis_ikan",
                        group_concat(ptp.id_jenis_ikan separator ",") AS "id_jenis_ikan",
                        ptp.id_aktivitas_referensi
                    FROM db_pipp.trs_produksi ptp
                    LEFT JOIN db_master.mst_jenis_ikan mmji
                        ON ptp.id_jenis_ikan = mmji.id_jenis_ikan
                    WHERE   ptp.aktif = "Ya"
                            AND ptp.id_aktivitas_referensi <> "0"
                            AND mmji.aktif = "Ya"
                    GROUP BY ptp.id_aktivitas_referensi) ptp
                ON ak.id_aktivitas_kapal = ptp.id_aktivitas_referensi
            WHERE 1
            '.$biaya_operasional.'
            '.$jumlah_perbekalan.' 
            '.$jenis_ikan.' 
        ';
        




        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}