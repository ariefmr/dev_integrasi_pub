<?php
/*
 * class Mdl_H3
 */

class Mdl_fungsional extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
    
    }


    /*  == Model Pencarian Funsgional == */

    //OUTPUT
    /*
        nama_fasilitas      => Nama Fasilitas
        jns_fasilitas       => Jenis Fasilitas (Pokok/Fungsional/Penunjang)
        jns_konstruksi      => Jenis Konstruksi
        ket_konstruksi      => Kondisi Konstruksi
        sumber_dana         => Sumber Dana
        tahun               => Tahun Pembuatan
        nilai               => Nilai Fasilitas
        ket_rehap           => Keterangan Rehabilitasi
        id_pelabuhan        => id_pelabuhan
        lokasi              => Lokasi Pelabuhan
        deskripsi_status    => Status Pelabuhan dari db_master.mst_deskripsi_status
        pengelola_pelabuhan => Pengeolola Pelabuhan dari db_master.mst_pengelola_pelabuhan
    */

    //Mode Pencarian
    /*
        === tidak ada jenis konstruksi dan rehap ===
        model pencarian query
        $data['nama_fasilitas'] => pencarian nama fasilitas berdasarkan "Like" nama_fasilitas
                                => variabel string
        $data['tahun']  => pencarian tahun pembuatan berdasarkan tahun
                        => variabel int
        $data['pelabuhan']  => pencarian lokasi pelabuhan berdasarkan id_pelabuhan
                        => variabel int
        $data['ket_konstruksi']  => pencarian keterangan konstruksi berdasarkan id_jns_ket_konstruksi
                        => variabel int
        $data['deskripsi_status']  => pencarian deskripsi_status berdasarkan id_deskripsi_status
                        => variabel int
        $data['pengelola_pelabuhan']  => pencarian pengelola_pelabuhan berdasarkan id_deskripsi_status
                        => variabel int
        $data['nilai'] => percarian berdasarkan nilai fasilitas
            index select    0 => -/all
                            1 => <100jt
                            2 => 100jt - 1M
                            3 => 1M - 5M
                            4 => >5M
        $data['sumber_dana'] => percarian berdasarkan sumber dana
            index select    0 => APBN
                            1 => APBD
                            2 => TP
                            3 => DEKON
                            4 => LAINNYA
    */
    function data_pokok($data)
    {

        $data['sumber_dana'] = 1;
        //kondisi pencarian nama fasilitas
        if( empty($data['nama_fasilitas']) === FALSE )
        {
            $nama_fasilitas = 'AND fasilitas.nama_fasilitas LIKE "%'.$data['nama_fasilitas'].'%"';
        } else $nama_fasilitas = '';

        //kondisi pencarian tahun
        if( empty($data['tahun']) === FALSE )
        {
            $tahun = 'AND fasilitas.tahun = "'.$data['tahun'].'"';
        } else $tahun = '';

        //kondisi pencarian tahun
        if (empty( $data['nilai']) === FALSE )
        {
            if( $data['nilai'] === 1 )
            {
                $nilai = 'AND fasilitas.nilai <= 100000000 ';
            }elseif ( $data['nilai'] === 2) 
            {
               $nilai = 'AND fasilitas.nilai > 100000000 ';
               $nilai .= 'AND fasilitas.nilai <=  10000000000 ';
            }elseif ( $data['nilai'] === 3) 
            {
               $nilai = 'AND fasilitas.nilai > 1000000000 ';
               $nilai .= 'AND fasilitas.nilai <=  5000000000 ';
            }elseif ( $data['nilai'] === 4) 
            {
               $nilai = 'AND fasilitas.nilai > 5000000000 ';;
            } else $nilai = '';
        } else $nilai = '';

        //kondisi sumber dana
        if (empty( $data['sumber_dana']) === FALSE )
        {
            if( $data['sumber_dana'] === 0 )
            {
                $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBN%" ';
            }elseif ( $data['sumber_dana'] === 1) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBD%" ';
            }elseif ( $data['sumber_dana'] === 2) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%TP%" ';
            }elseif ( $data['sumber_dana'] === 3) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%DEKON%" ';
            }else 
            {
               $sumber_dana = '
                            AND UPPER(fasilitas.sumber_dana) <> "APBN" 
                            AND UPPER(fasilitas.sumber_dana) <> "APBD"
                            AND UPPER(fasilitas.sumber_dana) <> "TP"
                            AND UPPER(fasilitas.sumber_dana) <> "DEKON" ';
            }
        } else $sumber_dana = '';

        //kondisi pencarian ket_konstruksi 
        if( empty($data['ket_konstruksi']) === FALSE )
        {
            $ket_konstruksi = 'AND fasilitas.ket_konstruksi = "'.$data['ket_konstruksi'].'"';
        } else $ket_konstruksi = '';

        //kondisi pencarian pelabuhan 
        if( empty($data['pelabuhan']) === FALSE )
        {
            $pelabuhan = 'AND fasilitas.id_pelabuhan = "'.$data['pelabuhan'].'"';
        } else $pelabuhan = '';

        //kondisi pencarian deskripsi_status 
        if( empty($data['deskripsi_status']) === FALSE )
        {
            $deskripsi_status = 'AND mmp.id_deskripsi_status = "'.$data['deskripsi_status'].'"';
        } else $deskripsi_status = '';

        //kondisi pencarian pengelola_pelabuhan 
        if( empty($data['pengelola_pelabuhan']) === FALSE )
        {
            $pengelola_pelabuhan = 'AND mmp.pengelola_pelabuhan = "'.$data['pengelola_pelabuhan'].'"';
        } else $pengelola_pelabuhan = '';

        $sql = '
            SELECT 
                fasilitas.nama_fasilitas,
                fasilitas.jns_fasilitas,
                fasilitas.jns_konstruksi,
                CASE 
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NULL THEN "-"
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NOT NULL THEN pmjkk.nama_jns_ket_konstruksi
                END AS ket_konstruksi,
                fasilitas.sumber_dana,
                fasilitas.tahun,
                fasilitas.nilai,
                fasilitas.ket_rehap,
                mmp.nama_pelabuhan AS "lokasi",
                mmds.nama_deskripsi_status AS "deskripsi_status",
                mmpp.nama_pengelola_pelabuhan AS "pengelola_pelabuhan"

            FROM (
                    SELECT
                        "Breakweater" AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        pmb.jns_konstruksi,
                        pmb.ket_konstruksi,
                        pmb.sumber_dana,
                        pmb.tahun,
                        pmb.nilai,
                        pmb.ket_rehab AS "ket_rehap",
                        pmb.id_pelabuhan
                    FROM db_pipp.mst_breakwater pmb
                    WHERE pmb.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        "Revetment" AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        pmr.jns_konstruksi,
                        pmr.ket_konstruksi,
                        pmr.sumber_dana,
                        pmr.tahun,
                        pmr.nilai,
                        pmr.ket_rehab AS "ket_rehap",
                        pmr.id_pelabuhan
                    FROM db_pipp.mst_revetment pmr
                    WHERE pmr.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        "Groin" AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        pmg.jns_konstruksi,
                        pmg.ket_konstruksi,
                        pmg.sumber_dana,
                        pmg.tahun,
                        pmg.nilai,
                        pmg.ket_rehab AS "ket_rehap",
                        pmg.id_pelabuhan
                    FROM db_pipp.mst_groin pmg
                    WHERE pmg.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        CASE 
                            WHEN pmd.nama_fas IS NULL THEN "Dermaga"
                            WHEN pmd.nama_fas IS NOT NULL THEN pmd.nama_fas
                        END AS nama_fasilitas,
                        "Pokok" AS "jns_fasilitas",
                        pmd.jns_konstruksi,
                        "-" AS ket_konstruksi,
                        pmd.sumber_dana,
                        pmd.tahun,
                        pmd.nilai,
                        pmd.ket_rehab AS "ket_rehap",
                        pmd.id_pelabuhan
                    FROM db_pipp.mst_dermaga pmd
                    WHERE pmd.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        "Kolam Pelabuhan" AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        "-" AS jns_konstruksi,
                        pmkp.ket_konstruksi,
                        pmkp.sumber_dana,
                        pmkp.tahun,
                        pmkp.nilai,
                        pmkp.ket_rehab AS "ket_rehap",
                        pmkp.id_pelabuhan
                    FROM db_pipp.mst_kolam_pelabuhan pmkp
                    WHERE pmkp.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        "Alur Pelayaran" AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        "-" AS jns_konstruksi,
                        pmap.ket_konstruksi,
                        pmap.sumber_dana,
                        pmap.tahun,
                        pmap.nilai,
                        pmap.ket_rehab AS "ket_rehap",
                        pmap.id_pelabuhan
                    FROM db_pipp.mst_alur_pelayaran pmap
                    WHERE pmap.aktif = "Ya"

                    UNION ALL 

                    SELECT
                        CASE pmfh.id_jns_fas_hubung
                            WHEN 5 THEN "Pagar Keliling"
                            WHEN 3 THEN "Penghubung"
                            WHEN 1 THEN "Jalan"
                            END AS "nama_fasilitas",
                        "Pokok" AS "jns_fasilitas",
                        pmfh.jns_konstruksi,
                        pmfh.ket_konstruksi,
                        pmfh.sumber_dana,
                        pmfh.tahun,
                        pmfh.nilai,
                        pmfh.ket_rehab AS "ket_rehap",
                        pmfh.id_pelabuhan
                    FROM db_pipp.mst_fas_hubung pmfh
                    WHERE pmfh.aktif = "Ya"
                ) fasilitas
            INNER JOIN db_master.mst_pelabuhan mmp
                    ON mmp.id_pelabuhan = fasilitas.id_pelabuhan AND mmp.aktif="Ya"
            LEFT JOIN db_master.mst_deskripsi_status mmds
                ON mmds.id_deskripsi_status = mmp.id_deskripsi_status
            LEFT JOIN db_master.mst_pengelola_pelabuhan mmpp
                ON mmp.pengelola_pelabuhan = mmpp.id_pengelola_pelabuhan
            LEFT JOIN db_pipp.mst_jenis_ket_konstruksi  pmjkk
                ON pmjkk.id_jns_ket_konstruksi = fasilitas.jns_konstruksi
            WHERE 1
                '.$nama_fasilitas.'
                '.$tahun.'
                '.$nilai.'
                '.$pelabuhan.'
                '.$deskripsi_status.'
                '.$pengelola_pelabuhan.'
                '.$ket_konstruksi.'
                '.$sumber_dana.'                
        ';
        echo $deskripsi_status;
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    function data_fungsional($data)
    {
        //kondisi pencarian nama fasilitas
        if( empty($data['nama_fasilitas']) === FALSE )
        {
            $nama_fasilitas = 'AND fasilitas.nama_fasilitas LIKE "%'.$data['nama_fasilitas'].'%"';
        } else $nama_fasilitas = '';

        //kondisi pencarian tahun
        if( empty($data['tahun']) === FALSE )
        {
            $tahun = 'AND fasilitas.tahun = "'.$data['tahun'].'"';
        } else $tahun = '';

        //kondisi pencarian tahun
        if (empty( $data['nilai']) === FALSE )
        {
            if( $data['nilai'] === 1 )
            {
                $nilai = 'AND fasilitas.nilai <= 100000000 ';
            }elseif ( $data['nilai'] === 2) 
            {
               $nilai = 'AND fasilitas.nilai > 100000000 ';
               $nilai .= 'AND fasilitas.nilai <=  10000000000 ';
            }elseif ( $data['nilai'] === 3) 
            {
               $nilai = 'AND fasilitas.nilai > 1000000000 ';
               $nilai .= 'AND fasilitas.nilai <=  5000000000 ';
            }elseif ( $data['nilai'] === 4) 
            {
               $nilai = 'AND fasilitas.nilai > 5000000000 ';;
            } else $nilai = '';
        } else $nilai = '';

        //kondisi sumber dana
        if (empty( $data['sumber_dana']) === FALSE )
        {
            if( $data['sumber_dana'] === 0 )
            {
                $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBN%" ';
            }elseif ( $data['sumber_dana'] === 1) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBD%" ';
            }elseif ( $data['sumber_dana'] === 2) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%TP%" ';
            }elseif ( $data['sumber_dana'] === 3) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%DEKON%" ';
            }else 
            {
               $sumber_dana = '
                            AND UPPER(fasilitas.sumber_dana) <> "APBN" 
                            AND UPPER(fasilitas.sumber_dana) <> "APBD"
                            AND UPPER(fasilitas.sumber_dana) <> "TP"
                            AND UPPER(fasilitas.sumber_dana) <> "DEKON" ';
            }
        } else $sumber_dana = '';

        //kondisi pencarian ket_konstruksi 
        if( empty($data['ket_konstruksi']) === FALSE )
        {
            $ket_konstruksi = 'AND fasilitas.ket_konstruksi = "'.$data['ket_konstruksi'].'"';
        } else $ket_konstruksi = '';

        //kondisi pencarian pelabuhan 
        if( empty($data['pelabuhan']) === FALSE )
        {
            $pelabuhan = 'AND fasilitas.id_pelabuhan = "'.$data['pelabuhan'].'"';
        } else $pelabuhan = '';

        //kondisi pencarian deskripsi_status 
        if( empty($data['deskripsi_status']) === FALSE )
        {
            $deskripsi_status = 'AND mmp.id_deskripsi_status = "'.$data['deskripsi_status'].'"';
        } else $deskripsi_status = '';

        //kondisi pencarian pengelola_pelabuhan 
        if( empty($data['pengelola_pelabuhan']) === FALSE )
        {
            $pengelola_pelabuhan = 'AND mmp.pengelola_pelabuhan = "'.$data['pengelola_pelabuhan'].'"';
        } else $pengelola_pelabuhan = '';

        $sql = '
            SELECT 
                fasilitas.nama_fasilitas,
                fasilitas.jns_fasilitas,
                fasilitas.jns_konstruksi,
                CASE 
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NULL THEN "-"
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NOT NULL THEN pmjkk.nama_jns_ket_konstruksi
                END AS ket_konstruksi,
                fasilitas.sumber_dana,
                fasilitas.tahun,
                fasilitas.nilai,
                fasilitas.ket_rehap,
                mmp.nama_pelabuhan AS "lokasi",
                mmds.nama_deskripsi_status AS "deskripsi_status",
                mmpp.nama_pengelola_pelabuhan AS "pengelola_pelabuhan"

            FROM (
                SELECT
                    pmfp.nama_fas_pemasaran AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmfp.ket_konstruksi,
                    pmfp.sumber_dana,
                    pmfp.tahun,
                    pmfp.nilai,
                    "-" AS "ket_rehap",
                    pmfp.id_pelabuhan
                FROM db_pipp.mst_fas_pemasaran pmfp
                WHERE pmfp.aktif = "Ya"

                UNION ALL

                SELECT
                    jfnk.nama_jns_fas_nav_kom AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    "-" AS ket_konstruksi,
                    "-" AS sumber_dana,
                    pmfnk.tahun,
                    "-" AS nilai,
                    "-" AS "ket_rehap",
                    pmfnk.id_pelabuhan
                FROM db_pipp.mst_fas_nav_kom pmfnk
                LEFT JOIN db_pipp.mst_jenis_fas_nav_kom jfnk
                    ON jfnk.id_jns_fas_nav_kom = pmfnk.id_jns_fas_nav_kom AND jfnk.aktif = "Ya"
                WHERE pmfnk.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjsa.nama_jns_sumber_air AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmsa.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmsa.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmsa.id_pelabuhan
                FROM db_pipp.mst_sumber_air pmsa
                LEFT JOIN db_pipp.mst_jenis_sumber_air pmjsa
                    ON pmjsa.id_jns_sumber_air = pmsa.id_jns_sumber_air AND pmjsa.aktif = "Ya"
                WHERE pmsa.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjla.nama_jns_layanan_air AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmla.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmla.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmla.id_pelabuhan
                FROM db_pipp.mst_layanan_air pmla
                LEFT JOIN db_pipp.mst_jenis_layanan_air pmjla
                    ON pmjla.id_jns_layanan_air = pmla.id_jns_layanan_air AND pmjla.aktif = "Ya"
                WHERE pmla.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjle.nama_jns_layanan_es AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    "-" AS "ket_konstruksi",
                    "-" AS "sumber_dana",
                    pmle.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmle.id_pelabuhan
                FROM db_pipp.mst_layanan_es pmle
                LEFT JOIN db_pipp.mst_jenis_layanan_es pmjle
                    ON pmjle.id_jns_layanan_es = pmle.id_jns_layanan_es AND pmjle.aktif = "Ya"
                WHERE pmle.aktif = "Ya"

                UNION ALL

                SELECT
                    pmll.nama_layanan_listrik AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    "-" AS "ket_konstruksi",
                    "-" AS "sumber_dana",
                    pmll.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmll.id_pelabuhan
                FROM db_pipp.mst_layanan_listrik pmll
                WHERE pmll.aktif = "Ya"

                UNION ALL

                SELECT
                    "Rumah Genset" AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmrg.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmrg.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmrg.id_pelabuhan
                FROM db_pipp.mst_rumah_genset pmrg
                WHERE pmrg.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjlb.nama_jns_layanan_bbm AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmlb.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmlb.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmlb.id_pelabuhan
                FROM db_pipp.mst_layanan_bbm pmlb
                LEFT JOIN db_pipp.mst_jenis_layanan_bbm pmjlb
                    ON pmjlb.id_jns_layanan_bbm = pmlb.id_jns_layanan_bbm AND pmjlb.aktif = "Ya"
                WHERE pmlb.aktif = "Ya"

                UNION ALL

                SELECT
                    "Dock" AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    pmdo.jns_konstruksi,
                    pmdo.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmdo.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmdo.id_pelabuhan
                FROM db_pipp.mst_dock pmdo
                WHERE pmdo.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjfp.nama_jns_fas_pelihara AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    pmfp.jns_konstruksi,
                    pmfp.ket_konstruksi,
                    "-" AS "sumber_dana",
                    pmfp.tahun,
                    "-" AS "nilai",
                    "-" AS "ket_rehap",
                    pmfp.id_pelabuhan
                FROM db_pipp.mst_fas_pelihara pmfp
                LEFT JOIN db_pipp.mst_jenis_fas_pelihara pmjfp
                    ON pmjfp.id_jns_fas_pelihara = pmfp.id_jns_fas_pelihara AND pmjfp.aktif = "Ya"
                WHERE pmfp.aktif = "Ya"

                UNION ALL 

                SELECT
                    "Laboratorium Pembinaan dan Pengujian Hasil Mutu " AS "nama_fasilitas",
                    "Pokok" AS "jns_fasilitas",
                    pmlm.jns_konstruksi,
                    pmlm.ket_konstruksi,
                    pmlm.sumber_dana,
                    pmlm.tahun,
                    pmlm.nilai,
                    pmlm.ket_rehab AS "ket_rehap",
                    pmlm.id_pelabuhan
                FROM db_pipp.mst_lab_mutu pmlm
                WHERE pmlm.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjfp.nama_jns_fas_perikanan AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    pmfp.jns_konstruksi,
                    pmfp.ket_konstruksi,
                    pmfp.sumber_dana,
                    pmfp.tahun,
                    pmfp.nilai,
                    pmfp.ket_rehab AS "ket_rehap",
                    pmfp.id_pelabuhan
                FROM db_pipp.mst_fas_perikanan pmfp
                LEFT JOIN db_pipp.mst_jenis_fas_perikanan pmjfp
                    ON pmjfp.id_jns_fas_perikanan = pmfp.id_jns_fas_perikanan AND pmjfp.aktif = "Ya"
                WHERE pmfp.aktif = "Ya"

                UNION ALL

                SELECT
                    "Perkantoran" AS "nama_fasilitas",
                    "Pokok" AS "jns_fasilitas",
                    pmk.jns_konstruksi,
                    pmk.ket_konstruksi,
                    pmk.sumber_dana,
                    pmk.tahun,
                    pmk.nilai,
                    pmk.ket_rehab AS "ket_rehap",
                    pmk.id_pelabuhan
                FROM db_pipp.mst_kantor pmk
                WHERE pmk.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjfk.nama_jns_kendaraan AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    "-" AS "ket_konstruksi",
                    pmk.sumber_dana,
                    pmk.tahun,
                    pmk.nilai,
                    pmk.ket_rehab AS "ket_rehap",
                    pmk.id_pelabuhan
                FROM db_pipp.mst_kendaraan pmk
                LEFT JOIN db_pipp.mst_jenis_kendaraan pmjfk
                    ON pmjfk.id_jns_kendaraan = pmk.id_jns_kendaraan AND pmjfk.aktif = "Ya"
                WHERE pmk.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjg.nama_jns_garasi AS "nama_fasilitas",
                    "Fungsional" AS "jns_fasilitas",
                    pmg.jns_konstruksi,
                    pmg.ket_konstruksi,
                    pmg.sumber_dana,
                    pmg.tahun,
                    pmg.nilai,
                    pmg.ket_rehab AS "ket_rehap",
                    pmg.id_pelabuhan
                FROM db_pipp.mst_garasi pmg
                LEFT JOIN db_pipp.mst_jenis_garasi pmjg
                    ON pmjg.id_jns_garasi = pmg.id_jns_garasi AND pmjg.aktif = "Ya"
                WHERE pmg.aktif = "Ya"

                UNION ALL

                SELECT
                    pmol.nama_olah_limbah AS "nama_fasilitas",
                    "Pokok" AS "jns_fasilitas",
                    "-" AS "jns_konstruksi",
                    pmol.ket_konstruksi,
                    pmol.sumber_dana,
                    pmol.tahun,
                    pmol.nilai,
                    pmol.ket_rehab AS "ket_rehap",
                    pmol.id_pelabuhan
                FROM db_pipp.mst_olah_limbah pmol
                WHERE pmol.aktif = "Ya"
            ) fasilitas
                INNER JOIN db_master.mst_pelabuhan mmp
                        ON mmp.id_pelabuhan = fasilitas.id_pelabuhan AND mmp.aktif="Ya"
                LEFT JOIN db_master.mst_deskripsi_status mmds
                    ON mmds.id_deskripsi_status = mmp.id_deskripsi_status
                LEFT JOIN db_master.mst_pengelola_pelabuhan mmpp
                    ON mmp.pengelola_pelabuhan = mmpp.id_pengelola_pelabuhan
                LEFT JOIN db_pipp.mst_jenis_ket_konstruksi  pmjkk
                    ON pmjkk.id_jns_ket_konstruksi = fasilitas.jns_konstruksi
                WHERE 1
                '.$nama_fasilitas.'
                '.$tahun.'
                '.$nilai.'
                '.$pelabuhan.'
                '.$deskripsi_status.'
                '.$pengelola_pelabuhan.'
                '.$ket_konstruksi.'
                '.$sumber_dana.'
        ';

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    function data_penunjang()
    {
        //kondisi pencarian nama fasilitas
        if( empty($data['nama_fasilitas']) === FALSE )
        {
            $nama_fasilitas = 'AND fasilitas.nama_fasilitas LIKE "%'.$data['nama_fasilitas'].'%"';
        } else $nama_fasilitas = '';

        //kondisi pencarian tahun
        if( empty($data['tahun']) === FALSE )
        {
            $tahun = 'AND fasilitas.tahun = "'.$data['tahun'].'"';
        } else $tahun = '';

        //kondisi pencarian tahun
        if (empty( $data['nilai']) === FALSE )
        {
            if( $data['nilai'] === 1 )
            {
                $nilai = 'AND fasilitas.nilai <= 100000000 ';
            }elseif ( $data['nilai'] === 2) 
            {
               $nilai = 'AND fasilitas.nilai > 100000000 ';
               $nilai .= 'AND fasilitas.nilai <=  10000000000 ';
            }elseif ( $data['nilai'] === 3) 
            {
               $nilai = 'AND fasilitas.nilai > 1000000000 ';
               $nilai .= 'AND fasilitas.nilai <=  5000000000 ';
            }elseif ( $data['nilai'] === 4) 
            {
               $nilai = 'AND fasilitas.nilai > 5000000000 ';;
            } else $nilai = '';
        } else $nilai = '';

        //kondisi sumber dana
        if (empty( $data['sumber_dana']) === FALSE )
        {
            if( $data['sumber_dana'] === 0 )
            {
                $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBN%" ';
            }elseif ( $data['sumber_dana'] === 1) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%APBD%" ';
            }elseif ( $data['sumber_dana'] === 2) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%TP%" ';
            }elseif ( $data['sumber_dana'] === 3) 
            {
               $sumber_dana = 'AND UPPER(fasilitas.sumber_dana) LIKE "%DEKON%" ';
            }else 
            {
               $sumber_dana = '
                            AND UPPER(fasilitas.sumber_dana) <> "APBN" 
                            AND UPPER(fasilitas.sumber_dana) <> "APBD"
                            AND UPPER(fasilitas.sumber_dana) <> "TP"
                            AND UPPER(fasilitas.sumber_dana) <> "DEKON" ';
            }
        } else $sumber_dana = '';

        //kondisi pencarian ket_konstruksi 
        if( empty($data['ket_konstruksi']) === FALSE )
        {
            $ket_konstruksi = 'AND fasilitas.ket_konstruksi = "'.$data['ket_konstruksi'].'"';
        } else $ket_konstruksi = '';

        //kondisi pencarian pelabuhan 
        if( empty($data['pelabuhan']) === FALSE )
        {
            $pelabuhan = 'AND fasilitas.id_pelabuhan = "'.$data['pelabuhan'].'"';
        } else $pelabuhan = '';

        //kondisi pencarian deskripsi_status 
        if( empty($data['deskripsi_status']) === FALSE )
        {
            $deskripsi_status = 'AND mmp.id_deskripsi_status = "'.$data['deskripsi_status'].'"';
        } else $deskripsi_status = '';

        //kondisi pencarian pengelola_pelabuhan 
        if( empty($data['pengelola_pelabuhan']) === FALSE )
        {
            $pengelola_pelabuhan = 'AND mmp.pengelola_pelabuhan = "'.$data['pengelola_pelabuhan'].'"';
        } else $pengelola_pelabuhan = '';

        $sql = '
        SELECT 
                fasilitas.nama_fasilitas,
                fasilitas.jns_fasilitas,
                fasilitas.jns_konstruksi,
                CASE 
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NULL THEN "-"
                    WHEN pmjkk.nama_jns_ket_konstruksi IS NOT NULL THEN pmjkk.nama_jns_ket_konstruksi
                END AS ket_konstruksi,
                fasilitas.sumber_dana,
                fasilitas.tahun,
                fasilitas.nilai,
                fasilitas.ket_rehap,
                mmp.nama_pelabuhan AS "lokasi",
                mmds.nama_deskripsi_status AS "deskripsi_status",
                mmpp.nama_pengelola_pelabuhan AS "pengelola_pelabuhan"

            FROM (
                SELECT
                    pmb.nama_balai AS "nama_fasilitas",
                    "Penunjang" AS "jns_fasilitas",
                    pmb.jns_konstruksi,
                    pmb.ket_konstruksi,
                    pmb.sumber_dana,
                    pmb.tahun,
                    pmb.nilai,
                    pmb.ket_rehab AS "ket_rehap",
                    pmb.id_pelabuhan
                FROM db_pipp.mst_balai pmb
                WHERE pmb.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjfp.nama_jns_fas_pengelola AS "nama_fasilitas",
                    "Penunjang" AS "jns_fasilitas",
                    pmfp.jns_konstruksi,
                    pmfp.ket_konstruksi,
                    pmfp.sumber_dana,
                    pmfp.tahun,
                    pmfp.nilai,
                    pmfp.ket_rehab AS "ket_rehap",
                    pmfp.id_pelabuhan
                FROM db_pipp.mst_fas_pengelola pmfp
                LEFT JOIN db_pipp.mst_jenis_fas_pengelola pmjfp
                    ON pmjfp.id_jns_fas_pengelola = pmfp.id_jns_fas_pengelola AND pmjfp.aktif = "Ya"
                WHERE pmfp.aktif = "Ya"

                UNION ALL

                SELECT
                    pmjfu.nama_jns_fas_umum AS "nama_fasilitas",
                    "Penunjang" AS "jns_fasilitas",
                    pmfu.jns_konstruksi,
                    pmfu.ket_konstruksi,
                    pmfu.sumber_dana,
                    pmfu.tahun,
                    pmfu.nilai,
                    pmfu.ket_rehab AS "ket_rehap",
                    pmfu.id_pelabuhan
                FROM db_pipp.mst_fas_umum pmfu
                LEFT JOIN db_pipp.mst_jenis_fas_umum pmjfu
                    ON pmjfu.id_jns_fas_umum = pmfu.id_jns_fas_umum AND pmjfu.aktif = "Ya"
                WHERE pmfu.aktif = "Ya"
            ) fasilitas
                INNER JOIN db_master.mst_pelabuhan mmp
                        ON mmp.id_pelabuhan = fasilitas.id_pelabuhan AND mmp.aktif="Ya"
                LEFT JOIN db_master.mst_deskripsi_status mmds
                    ON mmds.id_deskripsi_status = mmp.id_deskripsi_status
                LEFT JOIN db_master.mst_pengelola_pelabuhan mmpp
                    ON mmp.pengelola_pelabuhan = mmpp.id_pengelola_pelabuhan
                LEFT JOIN db_pipp.mst_jenis_ket_konstruksi  pmjkk
                    ON pmjkk.id_jns_ket_konstruksi = fasilitas.jns_konstruksi
                WHERE 1
                '.$nama_fasilitas.'
                '.$tahun.'
                '.$nilai.'
                '.$pelabuhan.'
                '.$deskripsi_status.'
                '.$pengelola_pelabuhan.'
                '.$ket_konstruksi.'
                '.$sumber_dana.'
            ';

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    /*  == END Model Pencarian Funsgional == */

}