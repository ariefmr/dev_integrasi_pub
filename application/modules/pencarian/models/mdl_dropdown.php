<?php
/*
 * class Mdl_H3
 */

class Mdl_dropdown extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    function __construct()
    {
        $this->load->database();
        $this->db_pipp = $this->load->database('default', TRUE);
    
    }

    function list_ket_konstruksi()
    {

        $this->db_pipp->select('id_jns_ket_konstruksi AS "id", nama_jns_ket_konstruksi AS "text"');
        $this->db_pipp->from('mst_jenis_ket_konstruksi');
        $this->db_pipp->where('aktif', 'Ya');

        $query = $this->db_pipp->get();

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    function list_deskripsi_status()
    {

        $sql ='SELECT mds.id_deskripsi_status AS "id", mds.nama_deskripsi_status AS "text"
                FROM db_master.mst_deskripsi_status mds
                WHERE mds.aktif="Ya"
        ';

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    function list_pengelola_pelabuhan()
    {

        $sql ='SELECT mds.id_pengelola_pelabuhan AS "id", mds.nama_pengelola_pelabuhan AS "text"
                FROM db_master.mst_pengelola_pelabuhan mds
                WHERE mds.aktif="Ya"
        ';

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}