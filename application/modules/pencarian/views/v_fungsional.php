<?php
    $tmpl = array ( 'table_open'  => '<table id="table_operasional" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading(	'No.',
    							'Nama Fasilitas',
    							'Jenis Fasilitas',
    							'Jenis Konstruksi',
    							'Sumber Dana',
    							'Tahun Pembuatan',
    							'Nilai',
    							'Keterangan Rehap',
    							'Lokasi',
    							'Status Pelabuhan',
    							'Pengelola Pelabuhan'
    							);

    $index = 1;
    $total_result = 0;
    if($data_laporan !== FALSE)
    {
        $total_result = count($data_laporan);
        foreach ($data_laporan as $key => $item) {
        $kolom_jenis_ikan = "<text title='".kos($item->nama_jenis_ikan)."'>".kos( character_limiter($item->nama_jenis_ikan,50), "-")."</text>";
            $this->table->add_row(  $index.'. ', 
                                    kos( $item->nama_fasilitas, ""), 
                                    kos( $item->jns_fasilitas, ""),
                                    kos( $item->jns_konstruksi, ""),
                                    kos( $item->sumber_dana, ""),
                                    kos( $item->tahun, ""),
                                    kos( fmt_angka($item->nilai), "-"),
                                    kos( $item->ket_rehap, ""),
                                    kos( $item->lokasi, ""),
                                    kos( $item->deskripsi_status, ""),
                                    kos( $item->pengelola_pelabuhan, ""));
            $index++;
        }
    }
		

    $table_jasa = $this->table->generate();
    $this->table->clear();
?>

<div class="panel panel-default">
    <div class="panel-body">
            <?php  echo form_open('pencarian/fungsional/index','id="form_filter" role="form" '); ?>
           <legend>Pencarian Data Fungsional</legend>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                       <label for="">Nama Fasilitas</label>
                       <input type="text" name="nama_fasilitas" class="form-control" id="nama_fasilitas" placeholder="Nama Fasilitas" value="<?php echo kos($pre_filter["like0nama_fasilitas"],''); ?>">
                    </div>
                    <div class="form-group">
                       <label for="">Jenis Fasilitas</label>
                       <input type="text" name="jenis_fasilitas" id="jenis_fasilitas" >
                    </div>
                    <div class="form-group">
                       <label for="">Jenis Konstruksi</label>
                       <input type="text" name="jenis_konstruksi" id="jenis_konstruksi" >
                       <!-- <input type="text" name="alat_tangkap" id="alat_tangkap" placeholder="Input field"> -->
                    </div>
                    <div class="form-group">
                       <label for="">Kondisi Konstruksi</label>
                       <input type="text" name="ket_konstruksi" id="ket_konstruksi" placeholder="Input field">
                    </div>
                    <div class="form-group">
                       <label for="">Sumber Dana</label>
                       <input type="text" name="sumber_dana" id="sumber_dana" placeholder="Input field">
                    </div>
                        
                </div>
                <div class="col-lg-6">
                   <div class="form-group">
                       <label for="">Tahun</label>
                       <input type="text" name="tahun" class="form-control" id="tahun" placeholder="Tahun" value="<?php echo kos($pre_filter["like0tahun"],''); ?>">
                       <!-- <input type="text" name="tahun" class="form-control" id="tahun" >
                       <input type="hidden" name="tahun_bln" id="tahun_bln" >
                       <input type="hidden" name="tahun_thn" id="tahun_thn" > -->
                    </div>
                    <div class="form-group">
                       <label for="">Nilai</label>
                       <input type="text" name="nilai" id="nilai" >
                    </div>
                    <div class="form-group">
                       <label for="">Lokasi Pelabuhan </label>
                       <input type="text" name="pelabuhan" id="pelabuhan"  value="<?php echo kos($pre_filter["morethan0pelabuhan"],''); ?>" placeholder="Lokasi Pelabuhan">
                    </div>       

                    <div class="form-group">
                       <label for="">Status Pelabuhan </label>
                       <input type="text" name="deskripsi_status" id="deskripsi_status"  value="<?php echo kos($pre_filter["morethan0deskripsi_status"],''); ?>">
                    </div> 
                    <div class="form-group">
                       <label for="">Pengelola Pelabuhan </label>
                       <input type="text" name="pengelola_pelabuhan" id="pengelola_pelabuhan"  value="<?php echo kos($pre_filter["morethan0pengelola_pelabuhan"],''); ?>">
                    </div>          
                </div>
            </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="btn btn-primary">Cari</button>
    </div>
    </form>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
    Total Pencarian: <?php echo fmt_angka($total_result); ?> data.
    </div>
    <div class="panel-body overflowed">
       <?php echo $table_jasa; ?>
    </div>
</div>

<?php 
  $opsi_jenis_fasilitas = array(
                        array('id' => '0' , 'text' => 'Semua'),
                        array('id' => '1' , 'text' => 'Fasilitas Pokok'),
                        array('id' => '2' , 'text' => 'Fasilitas Fungsional '),
                        array('id' => '3' , 'text' => 'Fasilitas Penunjang'),
                    );
  $opsi_jenis_konstruksi = array(
                        array('id' => '0' , 'text' => 'Semua'),
                        array('id' => '1' , 'text' => 'Baja'),
                        array('id' => '2' , 'text' => 'Kayu'),
                        array('id' => '2' , 'text' => 'Filber'),
                    );
  $opsi_sumber_dana = array(
                        array('id' => '0' , 'text' => 'APBN'),
                        array('id' => '1' , 'text' => 'APBD'),
                        array('id' => '2' , 'text' => 'TP'),
                        array('id' => '3' , 'text' => 'DEKON'),
                        array('id' => '4' , 'text' => 'Lainnya'),
                    );
  $opsi_nilai = array(
                        array('id' => '0' , 'text' => 'Semua'),
                        array('id' => '1' , 'text' => '< 100jt'),
                        array('id' => '2' , 'text' => '100jt - 1M'),
                        array('id' => '3' , 'text' => '1M - 5M'),
                        array('id' => '4' , 'text' => '> 5M'),
                    );
 ?> 


<script>
    var opsi_ket_konstruksi = <?php echo $opsi_ket_konstruksi; ?>;

    var opsi_pelabuhan = <?php echo $opsi_pelabuhan; ?>;
    var opsi_pengelola_pelabuhan = <?php echo $opsi_pengelola_pelabuhan; ?>;
        opsi_pengelola_pelabuhan.push({id:"0",text:"Semua"});
    var opsi_deskripsi_status = <?php echo $opsi_deskripsi_status; ?>;
        opsi_deskripsi_status.push({id:"0",text:"Semua"});

    var opsi_jenis_fasilitas = <?php echo json_encode($opsi_jenis_fasilitas); ?>;
    var opsi_jenis_konstruksi = <?php echo json_encode($opsi_jenis_konstruksi); ?>;
    var opsi_sumber_dana = <?php echo json_encode($opsi_sumber_dana); ?>;
    var opsi_nilai = <?php echo json_encode($opsi_nilai); ?>;






    $(document).ready(function(){

      $("#nama_fasilitas").val("<?php echo kos($pre_filter['nama_fasilitas']); ?>");

      $("#jenis_fasilitas").select2({ data: opsi_jenis_fasilitas, width: "100%" });
      $("#jenis_fasilitas").select2("val", "<?php echo kos($pre_filter['jenis_fasilitas'], '0'); ?>");

      $("#jenis_konstruksi").select2({ data: opsi_jenis_konstruksi, width: "100%" });
      $("#jenis_konstruksi").select2("val", "<?php echo kos($pre_filter['jenis_konstruksi'], '0'); ?>");

      $("#ket_konstruksi").select2({ data: opsi_ket_konstruksi, width: "100%" });
      $("#ket_konstruksi").select2("val", "<?php echo kos($pre_filter['ket_konstruksi'], '0'); ?>");

      $("#sumber_dana").select2({ data: opsi_sumber_dana, width: "100%" });
      $("#sumber_dana").select2("val", "<?php echo kos($pre_filter['sumber_dana'], '0'); ?>");

      $("#nilai").select2({ data: opsi_nilai, width: "100%" });
      $("#nilai").select2("val", "<?php echo kos($pre_filter['nilai'], '0'); ?>");

      $("#pelabuhan").select2({ data: opsi_pelabuhan, width: "100%" });
      $("#pelabuhan").select2("val", "<?php echo kos($pre_filter['pelabuhan'], '0'); ?>");

      $("#deskripsi_status").select2({ data: opsi_deskripsi_status, width: "100%" });
      $("#deskripsi_status").select2("val", "<?php echo kos($pre_filter['deskripsi_status'], '0'); ?>");

      $("#pengelola_pelabuhan").select2({ data: opsi_pengelola_pelabuhan, width: "100%" });
      $("#pengelola_pelabuhan").select2("val", "<?php echo kos($pre_filter['pengelola_pelabuhan'], '0'); ?>");

      // $("#aktivitas").attr("placeholder","Belum Berfungsi").prop("disabled",true);
      // $("#tahun").attr("placeholder","Belum Berfungsi").prop("disabled",true);
      


      // $('#tahun').datepicker({                
      //       changeMonth: true,
      //       changeYear: true,
      //       showButtonPanel: true,
      //       maxDate: new Date(),
      //       defaultDate: new Date(),
      //       dateFormat: 'mm/yy',
      //       monthNamesShort: nama_bulan_short,
      //       onClose: function(dateText, inst) { 
      //           var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
      //           var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
      //           var newDate = new Date(year, month, 1);
      //           var valDate = $(this).val();
      //           var fmtDate = format_thnbln_to_str(newDate);
      //           $(this).datepicker('setDate', newDate);  
      //           $('#tahun').text(fmtDate);
      //           $('#tahun_bln').val((+month)+1);
      //           $('#tahun_thn').val(year);  
      //       },
      //       beforeShow: function (input, inst) {
      //           $('#ui-datepicker-div').addClass('datepick-bulan-tahun');
      //       }
      //   });

      // $('#tahun').click(function() {
      //       $('#tahun').datepicker('show').datepicker('widget').show().position({
      //                                           my: 'right bottom',
      //                                           at: 'right bottom',
      //                                           of: this 
      //                                       });
      // });
                

      $('#table_operasional').dataTable({
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
             "aoColumns":  [
                            { "sWidth": "8%" , "sClass": "text-center"},
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          });
      });
</script>