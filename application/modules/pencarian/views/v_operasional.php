<?php
    $tmpl = array ( 'table_open'  => '<table class="table_operasional table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading(	'No.',
    							'Nama Kapal',
    							'Tipe Kapal',
    							'Alat Tangkap',
    							'Pelabuhan Pangkalan',
    							'Jadwal Keluar/Masuk Pelabhuan',
    							'Jumlah Produksi (Ton)',
    							'Nilai Produksi (Rp)',
    							'Jenis Ikan',
    							'Jumlah ABK',
    							'Jumlah BBM (liter)',
    							'Biaya Operasional (Rp)');

    $index = 1;
    $total_result = 0;
    if($data_laporan !== FALSE)
    {
        $total_result = count($data_laporan);

        foreach ($data_laporan as $key => $item) {
        $kolom_jenis_ikan = "<text title='".kos($item->nama_jenis_ikan)."'>".kos( character_limiter($item->nama_jenis_ikan,50), "-")."</text>";
            $this->table->add_row(  $index.'. ', 
                                    kos( $item->nama_kapal, ""), 
                                    kos( $item->nama_jenis_kapal, ""),
                                    kos( $item->nama_alat_tangkap, ""),
                                    kos( $item->nama_pelabuhan_pangkalan, ""),
                                    kos( $item->tgl_aktivitas_2, ""),
                                    kos( fmt_angka($item->jml_ikan), "-"),
                                    kos( fmt_angka($item->harga_pedagang), "-"),
                                    $kolom_jenis_ikan,
                                    kos( fmt_angka($item->jumlah_abk), "0"),
                                    kos( fmt_angka($item->jumlah_perbekalan), "-"),
                                    kos( fmt_angka($item->biaya_oprasional), "-"));
            $index++;
        }
    }
		

    $table_jasa = $this->table->generate();
    $this->table->clear();
?>

<div class="panel panel-default">
    <div class="panel-body">
            <?php  echo form_open('pencarian/operasional/index','id="form_filter" role="form" '); ?>
           <legend>Pencarian Data Operasional</legend>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                       <label for="">Nama Kapal</label>
                       <input type="text" name="nama_kapal" class="form-control" id="nama_kapal" placeholder="Nama Kapal" value="<?php echo kos($pre_filter["like0nama_kapal"],''); ?>">
                    </div>
                    <div class="form-group">
                       <label for="">Tipe Kapal</label>
                       <input type="text" name="tipe_kapal" id="tipe_kapal" placeholder="Input field">
                    </div>
                    <div class="form-group">
                       <label for="">Alat Tangkap</label>
                       <input type="text" name="alat_tangkap" id="alat_tangkap" placeholder="Input field">
                    </div>
                    <div class="form-group">
                       <label for="">Pelabuhan Pangkalan</label>
                       <input type="text" name="pelabuhan_pangkalan" id="pelabuhan_pangkalan" placeholder="Input field">
                    </div>
                    <div class="form-group">
                       <label for="">Kunjungan Ke Pelabuhan</label>
                       <input type="text" name="aktivitas" id="aktivitas" >
                    </div>                         
                </div>
                <div class="col-lg-6">
                   <div class="form-group">
                       <label for="">Tanggal Aktivitas</label>
                       <input type="text" name="tgl_aktivitas" class="form-control" id="tgl_aktivitas" >
                       <input type="hidden" name="tgl_aktivitas_bln" id="tgl_aktivitas_bln" >
                       <input type="hidden" name="tgl_aktivitas_thn" id="tgl_aktivitas_thn" >
                    </div>
                    <div class="form-group">
                       <label for="">Jenis Ikan Yang Didaratkan</label>
                       <input type="text" name="jenis_ikan" id="jenis_ikan" placeholder="Input field">
                    </div>
                    <div class="form-group">
                       <label for="">Jumlah ABK (Orang) </label>
                       <input type="text" name="jumlah_abk" id="jumlah_abk"  value="<?php echo kos($pre_filter["morethan0jumlah_abk"],''); ?>">
                    </div>
                    <div class="form-group">
                       <label for="">Jumlah BBM (liter/bulan) </label>
                       <input type="text" name="jumlah_perbekalan" id="jumlah_perbekalan"  value="<?php echo kos($pre_filter["morethan0jumlah_perbekalan"],''); ?>">
                    </div>
                    <div class="form-group">
                       <label for="">Biaya Operasional (Rp/Bulan) </label>
                       <input type="text" name="biaya_operasional" id="biaya_operasional"  value="<?php echo kos($pre_filter["morethan0biaya_oprasional"],''); ?>">
                    </div>                  
                </div>
            </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="btn btn-primary">Cari</button>
    </div>
    </form>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
    Total Pencarian: <?php echo fmt_angka($total_result); ?> data.
    </div>
    <div class="panel-body overflowed">
       <?php echo $table_jasa; ?>
    </div>
</div>

<?php 
  $opsi_aktivitas = array(
                        array('id' => 'masuk' , 'text' => 'Masuk'),
                        array('id' => 'keluar' , 'text' => 'Keluar'),
                    );

  $opsi_jumlah_abk = array(
                        array('id' => '0' , 'text' => 'Semua'),
                        array('id' => '1' , 'text' => '<= 5'),
                        array('id' => '2' , 'text' => '6 - 10'),
                        array('id' => '3' , 'text' => '11 - 20'),
                        array('id' => '4' , 'text' => '21 - 30'),
                        array('id' => '5' , 'text' => '< 30'),
                    );

  $opsi_jumlah_perbekalan = array(
                        array('id' => '0' , 'text' => 'Semua'),
                        array('id' => '1' , 'text' => '<= 100'),
                        array('id' => '2' , 'text' => '101 - 1000'),
                        array('id' => '3' , 'text' => '1001 - 25000'),
                        array('id' => '4' , 'text' => '> 25000')
                    );

  $opsi_biaya_operasional = array(
                        array('id' => '0' , 'text' => '<= 1 juta'),
                        array('id' => '1' , 'text' => '1 juta - 10 juta'),
                        array('id' => '2' , 'text' => '10 juta - 50 juta'),
                        array('id' => '3' , 'text' => '50 juta - 100 juta'),
                        array('id' => '4' , 'text' => '> 100juta')
                    );
 ?> 


<script>
    var opsi_tipe_kapal = <?php echo $opsi_tipe_kapal; ?>;
        opsi_tipe_kapal.push({id:"0",text:"Semua"});
    var opsi_alat_tangkap = <?php echo $opsi_alat_tangkap; ?>;
        opsi_alat_tangkap.push({id:"0",text:"Semua"});
    var opsi_pelabuhan = <?php echo $opsi_pelabuhan; ?>;
        opsi_pelabuhan.push({id:"0",text:"Semua"});
    var opsi_jenis_ikan = <?php echo $opsi_jenis_ikan; ?>;
        opsi_jenis_ikan.push({id:"0",text:"Semua"});

    var opsi_aktivitas = <?php echo json_encode($opsi_aktivitas); ?>;
    var opsi_jumlah_abk = <?php echo json_encode($opsi_jumlah_abk); ?>;
    var opsi_jumlah_perbekalan = <?php echo json_encode($opsi_jumlah_perbekalan); ?>;
    var opsi_biaya_operasional = <?php echo json_encode($opsi_biaya_operasional); ?>;






    $(document).ready(function(){

      $("#nama_kapal").val("<?php echo kos($pre_filter['nama_kapal']); ?>");

      $("#tipe_kapal").select2({ data: opsi_tipe_kapal, width: "100%" });
      $("#tipe_kapal").select2("val", "<?php echo kos($pre_filter['tipe_kapal'], '0'); ?>");

      $("#alat_tangkap").select2({ data: opsi_alat_tangkap, width: "100%" });
      $("#alat_tangkap").select2("val", "<?php echo kos($pre_filter['alat_tangkap'], '0'); ?>");


      $("#pelabuhan_pangkalan").select2({ data: opsi_pelabuhan, width: "100%" });
      $("#pelabuhan_pangkalan").select2("val", "<?php echo kos($pre_filter['pelabuhan_pangkalan'], '0'); ?>");

      $("#jenis_ikan").select2({ data: opsi_jenis_ikan, width: "100%" });
      $("#jenis_ikan").select2("val", "<?php echo kos($pre_filter['jenis_ikan'], '0'); ?>");

      $("#aktivitas").select2({ data: opsi_aktivitas, width: "100%" });
      $("#aktivitas").select2("val", "<?php echo kos($pre_filter['aktivitas'], 'masuk'); ?>");

      $("#jumlah_abk").select2({ data: opsi_jumlah_abk, width: "100%" });
      $("#jumlah_abk").select2("val","<?php echo kos($pre_filter['jumlah_abk'], '0'); ?>");

      $("#jumlah_perbekalan").select2({ data: opsi_jumlah_perbekalan, width: "100%" });
      $("#jumlah_perbekalan").select2("val", "<?php echo kos($pre_filter['jumlah_perbekalan'], '0'); ?>");

      $("#biaya_operasional").select2({ data: opsi_biaya_operasional, width: "100%" });
      $("#biaya_operasional").select2("val", "<?php echo kos($pre_filter['biaya_operasional'], '0'); ?>");


      // $("#aktivitas").attr("placeholder","Belum Berfungsi").prop("disabled",true);
      // $("#tgl_aktivitas").attr("placeholder","Belum Berfungsi").prop("disabled",true);
      


      $('#tgl_aktivitas').datepicker({                
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            maxDate: new Date(),
            defaultDate: new Date(),
            dateFormat: 'mm/yy',
            monthNamesShort: nama_bulan_short,
            onClose: function(dateText, inst) { 
                var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
                var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
                var newDate = new Date(year, month, 1);
                var valDate = $(this).val();
                var fmtDate = format_thnbln_to_str(newDate);
                $(this).datepicker('setDate', newDate);  
                $('#tgl_aktivitas').text(fmtDate);
                $('#tgl_aktivitas_bln').val((+month)+1);
                $('#tgl_aktivitas_thn').val(year);  
            },
            beforeShow: function (input, inst) {
                $('#ui-datepicker-div').addClass('datepick-bulan-tahun');
            }
        });

      $('#tgl_aktivitas').click(function() {
            $('#tgl_aktivitas').datepicker('show').datepicker('widget').show().position({
                                                my: 'right bottom',
                                                at: 'right bottom',
                                                of: this 
                                            });
      });
                

      $('.table_operasional').dataTable({
            "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
             "aoColumns":  [
                            { "sWidth": "8%" , "sClass": "text-center"},
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"},                 
                            { "sWidth": "8%" , "sClass": "text-center"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"},                 
                            { "sWidth": "8%" , "sClass": "text-center","sType": "formatted-num"}                 
                          ],
            "bFilter": true,
            "bAutoWidth": false,
            "bInfo": false,
            "bPaginate": true,
            "iDisplayLength": 100,
            "bSort": true
          });
      });
</script>