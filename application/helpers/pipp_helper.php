<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('vdump'))
{
  function vdump($variable, $isdie = FALSE)
  {
    echo '<pre>';
    var_dump($variable);
    echo '</pre>'; 
    if($isdie){
      die;
    }
  }
}

if (!function_exists('fmt_tgl'))
{
  function fmt_tgl($str_tanggal)
  {
    return date("d/m/Y", strtotime($str_tanggal));
  }
}

if (!function_exists('fmt_bulan_tahun'))
{
  function fmt_bulan_tahun($str_tanggal)
  {
    // var_dump('1-'.$str_tanggal);
    return date("M, Y", strtotime($str_tanggal.'-1'));
  }
}

if (!function_exists('fmt_angka'))
{
  function fmt_angka($angka, $divider = 0)
  {
    if($divider > 0)
    {
      $angka = $angka / $divider;
    }
    return number_format($angka, 0, ",",".");
  }
}

if (!function_exists('fmt_rupiah'))
{
  function fmt_rupiah($angka)
  {
    return number_format($angka, 2, ",",".");
  }
}

if (!function_exists('fmt_field_name'))
{
  function fmt_field_name($str_field_name)
  {
    $formatted = str_replace('_', ' ', $str_field_name);
    $formatted = ucfirst($formatted);
    return $formatted;
  }
}

if (!function_exists('kos'))
{
  function kos(&$variable, $replacement = '')
  {
    if(empty($variable))
    {
      return $replacement;
    }elseif (!isset($variable)) {
      return $replacement;
    }elseif( $variable === FALSE ){
      return $replacement;
    }else{
      return $variable;
    }
  }
}

if (!function_exists('subdays'))
{
  function subdays($tanggal, $hari)
  {
    $date = new DateTime($tanggal);
    $date->sub(new DateInterval('P'.$hari.'D'));
    return $date->format('Y-m-d');
  }
}

if (!function_exists('bulan_ind'))
{
  function bulan_ind($index =''){
    $mons = array(1 => "Januari",
            2 => "Pebruari",
            3 => "Maret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember");
    if(empty($index)) {
      $date = getdate();
      $month = $date['mon'];
    }else{
      $month = $index;
    }
    $month_name = $mons[$month];

    return $month_name; // Displays the current month
  }
}

if (!function_exists('istilah_tujuan'))
{
  function istilah_tujuan($istilah)
  { 
    $tujuan_berangkat = '';

      if($istilah=== 'angkut')
            {
                $tujuan_berangkat = 'Mengangkut Ikan';
            }elseif ($istilah === 'tangkap') {
                $tujuan_berangkat = 'Menangkap Ikan';
            }elseif ($istilah === 'docking') {
                $tujuan_berangkat = 'Docking';
            }
        return $tujuan_berangkat;
  }
}