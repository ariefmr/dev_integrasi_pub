/*
 * Global JS.
 * Kumpulan fungsi yang bisa dipake di semua halaman.
 * Fungsi Shortcut dari JKey
 * Fungsi Notifikasi dari pnotify
 */

$.pnotify.defaults.delay = 4000;

var sound_fx = { 
                woosh: new Audio('http://localhost/de/assets/third_party/sounds/Woosh-Mark_DiAngelo-4778593.mp3'),
                blop: new Audio('http://localhost/de/assets/third_party/sounds/Blop-Mark_DiAngelo-79054334.mp3')
                };



function loading_kirim(pesan) {
    var percent = 0;
    var notice = $.pnotify({
        title: pesan,
        text: 'Test',
        type: 'info',
        icon: 'picon picon-throbber',
        hide: true,
        closer: false,
        sticker: false,
        opacity: 1,
        shadow: false
    });

    return notice;
}

function istilah_tujuan(istilah)
{
    var new_istilah = '';
    switch(istilah)
    {
        case 'tangkap' : new_istilah = 'Menangkap Ikan';
            break;
        case 'angkut'  : new_istilah = 'Mengangkut Ikan';
            break;
        case 'docking' : new_istilah = 'Docking';
    }

    return new_istilah;
}

function format_date_to_str(objectDate)
{
  // Javasript date use month number start from 0. So January is 0, December is 11;
  return objectDate.getDate()+"/"+ (objectDate.getMonth() +1 )+"/"+objectDate.getFullYear();
}
