jQuery.extend( jQuery.fn.dataTableExt.oSort, {
"formatted-num-pre": function ( a ) {
    a = (a==="-") ? 0 : a.replace(".", "" );
    return parseFloat( a );
},

"formatted-num-asc": function ( a, b ) {
    return a - b;
},

"formatted-num-desc": function ( a, b ) {
    return b - a;
},
"formatted-currency-pre": function ( a ) {
    a = (a==="-") ? 0 : a.split(".").join("").replace(",",".");;
    return parseFloat( a );
},

"formatted-currency-asc": function ( a, b ) {
    return a - b;
},

"formatted-currency-desc": function ( a, b ) {
    return b - a;
}
} );